﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Flow;
using ChemMgmt.Nikon.History;
using ChemMgmt.Nikon.Ledger;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;
using TBL = ZyCoaG.Nikon.T_LED_WORK_HISTORY;

namespace ChemMgmt.Nikon.History
{
    public class LedgerWorkHistoryModel : TBL
    {
        public LedgerWorkHistoryModel() { }
    }

    public class LedgerWorkHistorySearchModel : LedgerWorkHistoryModel
    {
        public int? MaxSearchResult { get; set; }

        public LedgerWorkHistorySearchModel()
        {
            SearchHistoryList = new List<LedgerWorkHistorySearchModel>();
        }

        public IEnumerable<LedgerWorkHistorySearchModel> SearchHistoryList;

    }
}
namespace ZyCoaG.Nikon.History
{
    public class LedgerWorkHistoryDataInfo : DataInfoBase<TBL>, IDisposable
    {

        private IDictionary<string, string> userDic { get; set; }

        protected override string TableName { get; } = nameof(T_LED_WORK_HISTORY);

        protected override void CreateInsertText(TBL value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(TBL.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(TBL.FLOW_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.REGISTER_USER_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.HIERARCHY) + ",");
            builder.AppendLine("  " + nameof(TBL.APPROVAL_TARGET) + ",");
            builder.AppendLine("  " + nameof(TBL.APPROVAL_USER_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.SUBSITUTE_USER_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.ACTUAL_USER_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.RESULT) + ",");
            builder.AppendLine("  " + nameof(TBL.MEMO));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  @" + nameof(TBL.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  @" + nameof(TBL.FLOW_CD) + ",");
            builder.AppendLine("  @" + nameof(TBL.REGISTER_USER_CD) + ",");
            builder.AppendLine("  @" + nameof(TBL.HIERARCHY) + ",");
            builder.AppendLine("  @" + nameof(TBL.APPROVAL_TARGET) + ",");
            builder.AppendLine("  @" + nameof(TBL.APPROVAL_USER_CD) + ",");
            builder.AppendLine("  @" + nameof(TBL.SUBSITUTE_USER_CD) + ",");
            builder.AppendLine("  @" + nameof(TBL.ACTUAL_USER_CD) + ",");
            builder.AppendLine("  @" + nameof(TBL.RESULT) + ",");
            builder.AppendLine("  @" + nameof(TBL.MEMO));
            builder.AppendLine(" )");
            parameters.Add(nameof(TBL.LEDGER_FLOW_ID), value.LEDGER_FLOW_ID);
            parameters.Add(nameof(TBL.FLOW_CD), value.FLOW_CD);
            parameters.Add(nameof(TBL.REGISTER_USER_CD), value.REGISTER_USER_CD);
            parameters.Add(nameof(TBL.HIERARCHY), value.HIERARCHY);
            parameters.Add(nameof(TBL.APPROVAL_TARGET), value.APPROVAL_TARGET);
            parameters.Add(nameof(TBL.APPROVAL_USER_CD), value.APPROVAL_USER_CD);
            parameters.Add(nameof(TBL.SUBSITUTE_USER_CD), value.SUBSITUTE_USER_CD);
            parameters.Add(nameof(TBL.ACTUAL_USER_CD), value.ACTUAL_USER_CD);
            parameters.Add(nameof(TBL.RESULT), value.RESULT);
            parameters.Add(nameof(TBL.MEMO), value.MEMO);

            sql = builder.ToString();
        }
        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (TBL)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);
            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (TBL)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" " + TableName + "." + nameof(TBL.LEDGER_FLOW_ID) + " = @" + nameof(TBL.LEDGER_FLOW_ID));
                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
            }

            sql.AppendLine(")");

            return sql.ToString();
        }

        public List<TBL> GetBlankWorkflow(TBL condition, string USER_CD, int? appli_class, Mode? mode)
        {
            using (var w = new UserWorkflowDataInfo() { Context = Context })
            {
                bool modify = false;
                if (appli_class == (int)AppliClass.Edit || mode == Mode.Edit)
                {
                    // 新規ではなく変更
                    modify = true;
                }
                var result = w.GetBlankWorkflow(condition.LEDGER_FLOW_ID, USER_CD, modify,
                    mode == Mode.Abolition ? UserWorkflowDataInfo.HIERARCHY_NULL_Abolish : null).ToList();
                foreach (var r in result)
                {
                    r.APPROVAL_USER_NAME = userDic.GetValueToString(r.APPROVAL_USER_CD);
                }
                var first = result.First(x => x.HIERARCHY == -1);
                first.ACTUAL_USER_CD = USER_CD;
                first.ACTUAL_USER_NAME = userDic.GetValueToString(USER_CD);
                return result;
            }
        }

        public override List<TBL> GetSearchResult(TBL condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select * ");
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            sql.AppendLine(" order by LED_WORK_HISTORY_ID");

            StartTransaction();
            List<TBL> records = DbUtil.Select<TBL>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<TBL>();
            foreach (DataRow record in dt.Rows)
            {
                dataArray.Add(new TBL()
                {
                    LED_WORK_HISTORY_ID = OrgConvert.ToNullableInt32(record[nameof(TBL.LED_WORK_HISTORY_ID)]),
                    LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(TBL.LEDGER_FLOW_ID)]),
                    FLOW_CD = record[nameof(TBL.FLOW_CD)].ToString(),
                    REGISTER_USER_CD = record[nameof(TBL.REGISTER_USER_CD)].ToString(),
                    HIERARCHY = OrgConvert.ToNullableInt32(record[nameof(TBL.HIERARCHY)]),
                    APPROVAL_TARGET = record[nameof(TBL.APPROVAL_TARGET)].ToString(),
                    APPROVAL_USER_CD = record[nameof(TBL.APPROVAL_USER_CD)].ToString(),
                    APPROVAL_USER_NAME = userDic.GetValueToString(record[nameof(TBL.APPROVAL_USER_CD)].ToString()),
                    SUBSITUTE_USER_CD = record[nameof(TBL.SUBSITUTE_USER_CD)].ToString(),
                    SUBSITUTE_USER_NAME = userDic.GetValueToString(record[nameof(TBL.SUBSITUTE_USER_CD)].ToString()),
                    ACTUAL_USER_CD = record[nameof(TBL.ACTUAL_USER_CD)].ToString(),
                    ACTUAL_USER_NAME = userDic.GetValueToString(record[nameof(TBL.ACTUAL_USER_CD)].ToString()),
                    RESULT = OrgConvert.ToNullableInt32(record[nameof(TBL.RESULT)]),
                    MEMO = record[nameof(TBL.MEMO)].ToString(),
                    REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(TBL.REG_DATE)]),
                    UPD_DATE = OrgConvert.ToNullableDateTime(record[nameof(TBL.UPD_DATE)]),
                    DEL_DATE = OrgConvert.ToNullableDateTime(record[nameof(TBL.REG_DATE)]),
                    DEL_FLAG = OrgConvert.ToNullableInt32(record[nameof(TBL.DEL_FLAG)]),
                });
            }
            return dataArray.AsQueryable().ToList();
        }

        protected override void InitializeDictionary()
        {
            userDic = new UserMasterInfo().GetDictionary();
        }

        public override void EditData(TBL Value) { }

        //protected override void CreateInsertText(TBL value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(TBL.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  " + nameof(TBL.FLOW_CD) + ",");
        //    builder.AppendLine("  " + nameof(TBL.REGISTER_USER_CD) + ",");
        //    builder.AppendLine("  " + nameof(TBL.HIERARCHY) + ",");
        //    builder.AppendLine("  " + nameof(TBL.APPROVAL_TARGET) + ",");
        //    builder.AppendLine("  " + nameof(TBL.APPROVAL_USER_CD) + ",");
        //    builder.AppendLine("  " + nameof(TBL.SUBSITUTE_USER_CD) + ",");
        //    builder.AppendLine("  " + nameof(TBL.ACTUAL_USER_CD) + ",");
        //    builder.AppendLine("  " + nameof(TBL.RESULT) + ",");
        //    builder.AppendLine("  " + nameof(TBL.MEMO));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(TBL.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  :" + nameof(TBL.FLOW_CD) + ",");
        //    builder.AppendLine("  :" + nameof(TBL.REGISTER_USER_CD) + ",");
        //    builder.AppendLine("  :" + nameof(TBL.HIERARCHY) + ",");
        //    builder.AppendLine("  :" + nameof(TBL.APPROVAL_TARGET) + ",");
        //    builder.AppendLine("  :" + nameof(TBL.APPROVAL_USER_CD) + ",");
        //    builder.AppendLine("  :" + nameof(TBL.SUBSITUTE_USER_CD) + ",");
        //    builder.AppendLine("  :" + nameof(TBL.ACTUAL_USER_CD) + ",");
        //    builder.AppendLine("  :" + nameof(TBL.RESULT) + ",");
        //    builder.AppendLine("  :" + nameof(TBL.MEMO));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(TBL.LEDGER_FLOW_ID), value.LEDGER_FLOW_ID);
        //    parameters.Add(nameof(TBL.FLOW_CD), value.FLOW_CD);
        //    parameters.Add(nameof(TBL.REGISTER_USER_CD), value.REGISTER_USER_CD);
        //    parameters.Add(nameof(TBL.HIERARCHY), value.HIERARCHY);
        //    parameters.Add(nameof(TBL.APPROVAL_TARGET), value.APPROVAL_TARGET);
        //    parameters.Add(nameof(TBL.APPROVAL_USER_CD), value.APPROVAL_USER_CD);
        //    parameters.Add(nameof(TBL.SUBSITUTE_USER_CD), value.SUBSITUTE_USER_CD);
        //    parameters.Add(nameof(TBL.ACTUAL_USER_CD), value.ACTUAL_USER_CD);
        //    parameters.Add(nameof(TBL.RESULT), value.RESULT);
        //    parameters.Add(nameof(TBL.MEMO), value.MEMO);

        //    sql = builder.ToString();
        //}

        protected override void CreateUpdateText(TBL value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.ACTUAL_USER_CD) + " = :" + nameof(value.ACTUAL_USER_CD) + ",");
            builder.AppendLine(" " + nameof(value.RESULT) + " = :" + nameof(value.RESULT) + ",");
            builder.AppendLine(" " + nameof(value.MEMO) + " = :" + nameof(value.MEMO) + ",");
            builder.AppendLine(" " + nameof(value.UPD_DATE) + " = getdate()");
            builder.AppendLine("where ");
            builder.AppendLine(" LED_WORK_HISTORY_ID=:LED_WORK_HISTORY_ID");
            parameters.Add(nameof(value.ACTUAL_USER_CD), value.ACTUAL_USER_CD);
            parameters.Add(nameof(value.RESULT), OrgConvert.ToNullableInt32(value.RESULT));
            parameters.Add(nameof(value.MEMO), value.MEMO);
            parameters.Add(nameof(value.LED_WORK_HISTORY_ID), value.LED_WORK_HISTORY_ID);
            sql = builder.ToString();
        }

        /// <summary>
        /// ワークフローの変更を申請中のワークフローへ反映する
        /// </summary>
        public void UpdateFlowChange(string FLOW_CD)
        {
            using (var master = new WorkflowDataInfo())
            {
                var workflow = master.GetWorkflow(FLOW_CD);
                if (workflow == null)
                {
                    return;
                }
                try
                {
                   
                    for (int i = 1; i <= Convert.ToInt32(workflow.FLOW_HIERARCHY); i++)
                    {
                        DynamicParameters parameters;
                        string sql;
                        CreateFlowChangeText(workflow, i, out sql, out parameters);
                        DbUtil.ExecuteUpdate(sql, parameters);
                    }
                   
                }
                catch
                {
                    //Rollback();
                }
                finally
                {
                    //Close();
                }
            }
        }

        protected void CreateFlowChangeText(M_WORKFLOW worklfow, int hierarchy, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(TBL.APPROVAL_TARGET) + " = @" + nameof(TBL.APPROVAL_TARGET) + ",");
            builder.AppendLine(" " + nameof(TBL.APPROVAL_USER_CD) + " = @" + nameof(TBL.APPROVAL_USER_CD) + ",");
            builder.AppendLine(" " + nameof(TBL.SUBSITUTE_USER_CD) + " = @" + nameof(TBL.SUBSITUTE_USER_CD));
            builder.AppendLine("where ");
            builder.AppendLine(" " + nameof(TBL.DEL_FLAG) + " = @" + nameof(TBL.DEL_FLAG));
            builder.AppendLine(" and " + nameof(TBL.RESULT) + " is null");
            builder.AppendLine(" and " + nameof(TBL.FLOW_CD) + " = @" + nameof(TBL.FLOW_CD));
            builder.AppendLine(" and " + nameof(TBL.HIERARCHY) + " = @" + nameof(TBL.HIERARCHY));

            parameters.Add(nameof(TBL.APPROVAL_TARGET), worklfow.APPROVAL_TARGET[hierarchy]);
            parameters.Add(nameof(TBL.APPROVAL_USER_CD), worklfow.APPROVAL_USER_CD[hierarchy]);
            parameters.Add(nameof(TBL.SUBSITUTE_USER_CD), worklfow.SUBSITUTE_USER_CD[hierarchy]);
            parameters.Add(nameof(TBL.DEL_FLAG), 0);
            parameters.Add(nameof(TBL.FLOW_CD), worklfow.FLOW_CD);
            parameters.Add(nameof(TBL.HIERARCHY), hierarchy);
            sql = builder.ToString();
        }

        /// <summary>
        /// ユーザーマスターの変更を申請中のワークフローへ反映する
        /// </summary>
        /// <param name="USER_CD">変更したユーザーコード</param>
        public void UpdateUserChange(M_USER before, M_USER after)
        {
            using (var master = new UserMasterInfo())
            {
                try
                {
                    DynamicParameters parameters;
                    string sql;
                    CreateUserApproveUserChangeText(after, out sql, out parameters);
                    //StartTransaction();
                    DbUtil.ExecuteUpdate(sql, parameters);
                    //Commit();
                    if (before.FLOW_CD != after.FLOW_CD)
                    {
                        UpdateUserFlowChange(before, after);
                        UpdateFlowChange(after.FLOW_CD);
                    }
                }
                catch(Exception exe)
                {
                    throw exe;
                }
                finally
                {
                    //Close();
                }
            }
        }

        /// <summary>
        /// ワークフロー内のフローコードを変更する
        /// </summary>
        /// <param name="before"></param>
        /// <param name="after"></param>
        protected void UpdateUserFlowChange(M_USER before, M_USER after)
        {
            DynamicParameters parameters;
            string sql;
            CreateUserFlowCdChangeText(before, after, out sql, out parameters);
            DbUtil.ExecuteUpdate(sql, parameters);
        }

        protected void CreateUserFlowCdChangeText(M_USER before, M_USER after, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(TBL.FLOW_CD) + " = @" + "AFTER_" + nameof(TBL.FLOW_CD));
            builder.AppendLine("where ");
            builder.AppendLine(" " + nameof(TBL.DEL_FLAG) + " = @" + nameof(TBL.DEL_FLAG));
            builder.AppendLine(" and " + nameof(TBL.REGISTER_USER_CD) + " = @" + nameof(TBL.REGISTER_USER_CD));
            builder.AppendLine(" and " + nameof(TBL.FLOW_CD) + " = @" + "BEFORE_" + nameof(TBL.FLOW_CD));

            parameters.Add("AFTER_" + nameof(TBL.FLOW_CD), after.FLOW_CD);
            parameters.Add(nameof(TBL.DEL_FLAG), 0);
            parameters.Add(nameof(TBL.REGISTER_USER_CD), after.USER_CD);
            parameters.Add("BEFORE_" + nameof(TBL.FLOW_CD), before.FLOW_CD);

            sql = builder.ToString();
        }

        protected void CreateUserApproveUserChangeText(M_USER after, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(TBL.APPROVAL_USER_CD) + " = @" + nameof(TBL.APPROVAL_USER_CD) + ",");
            builder.AppendLine(" " + nameof(TBL.SUBSITUTE_USER_CD) + " = @" + nameof(TBL.SUBSITUTE_USER_CD));
            builder.AppendLine("where ");
            builder.AppendLine(" " + nameof(TBL.DEL_FLAG) + " = @" + nameof(TBL.DEL_FLAG));
            builder.AppendLine(" and " + nameof(TBL.RESULT) + " is null");
            builder.AppendLine(" and " + nameof(TBL.REGISTER_USER_CD) + " = @" + nameof(TBL.REGISTER_USER_CD));
            builder.AppendLine(" and " + nameof(TBL.HIERARCHY) + " = @" + nameof(TBL.HIERARCHY));
            parameters.Add(nameof(TBL.APPROVAL_USER_CD), after.APPROVER1);
            parameters.Add(nameof(TBL.SUBSITUTE_USER_CD), after.APPROVER2);
            parameters.Add(nameof(TBL.DEL_FLAG), 0);
            parameters.Add(nameof(TBL.REGISTER_USER_CD), after.USER_CD);
            parameters.Add(nameof(TBL.HIERARCHY), 0);
            sql = builder.ToString();
        }
        /// <summary>
        /// 申請登録
        /// </summary>
        /// <param name="LEDGER_FLOW_ID">フローID（申請番号）</param>
        /// <param name="USER_CD">ユーザーCD</param>
        public void Temporary(int? LEDGER_FLOW_ID, string USER_CD)
        {
            using (var w = new UserWorkflowDataInfo() { Context = Context })
            {
                var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
                if (list.Count(x => x.DEL_FLAG == 0) == 0)
                {
                    var blank = w.GetBlankWorkflow(LEDGER_FLOW_ID, USER_CD, false);
                    var first = blank.LastOrDefault(x => x.DEL_FLAG == 0 && x.HIERARCHY == -1);
                    add(blank);
                }
                else
                {
                    IsValid = true;
                }
            }
        }
        /// <summary>
        /// 申請登録
        /// </summary>
        /// <param name="LEDGER_FLOW_ID">フローID（申請番号）</param>
        /// <param name="USER_CD">ユーザーCD</param>
        /// <param name="modify">変更申請であるか</param>
        public void Application(int? LEDGER_FLOW_ID, string USER_CD, bool modify)
        {
            using (var w = new UserWorkflowDataInfo() { Context = Context })
            {
                var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
                if (list.Count(x => x.DEL_FLAG == 0) == 0)
                {
                    var blank = w.GetBlankWorkflow(LEDGER_FLOW_ID, USER_CD, modify);
                    var first = blank.LastOrDefault(x => x.DEL_FLAG == 0 && x.HIERARCHY == -1);
                    if (first != null)
                    {
                        first.ACTUAL_USER_CD = USER_CD;
                        first.RESULT = (int)WorkflowResult.Application;
                    }
                    Add(blank);
                }
                else
                {
                    var first = list.LastOrDefault(x => x.DEL_FLAG == 0 && x.HIERARCHY == -1);
                    if (first != null)
                    {
                        first.ACTUAL_USER_CD = USER_CD;
                        first.RESULT = (int)WorkflowResult.Application;
                    }
                    save(first);
                }
            }
        }

        /// <summary>
        /// 変更申請
        /// </summary>
        /// <param name="LEDGER_FLOW_ID">フローID（申請番号）</param>
        /// <param name="USER_CD">ユーザーCD</param>
        public void HenkoApplication(int? LEDGER_FLOW_ID, string USER_CD)
        {
            var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
            if (list.Count(x => x.DEL_FLAG == 0) > 0)
            {
                ApplicationOnWorkflow(LEDGER_FLOW_ID, USER_CD);
            }
            else
            {
                Application(LEDGER_FLOW_ID, USER_CD, true);
            }
        }

        /// <summary>
        /// 承認
        /// </summary>
        /// <param name="LEDGER_FLOW_ID">フローID（申請番号）</param>
        /// <param name="USER_CD">ユーザーCD</param>
        public void Approval(int? LEDGER_FLOW_ID, string USER_CD)
        {
            var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID }).ToList();
            var current = list.First(x => x.DEL_FLAG == 0 && x.RESULT == null);
            current.ACTUAL_USER_CD = USER_CD;
            current.RESULT = (int)WorkflowResult.Approval;
            save(current);
            if (list.Count(x => x.RESULT == null) == 0)
            {
                remove(list.Where(x => x.DEL_FLAG == 0));
            }
        }

        /// <summary>
        /// 差戻し後
        /// </summary>
        /// <param name="LEDGER_FLOW_ID">フローID（申請番号）</param>
        /// <param name="USER_CD">ユーザーCD</param>
        public void ApplicationOnWorkflow(int? LEDGER_FLOW_ID, string USER_CD)
        {
            var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
            var current = list.First(x => x.DEL_FLAG == 0 && x.RESULT == null);
            current.ACTUAL_USER_CD = USER_CD;
            current.RESULT = (int)WorkflowResult.Application;
            save(current);
            if (list.Count(x => x.RESULT == null) == 0)
            {
                remove(list.Where(x => x.DEL_FLAG == 0));
            }
        }

        /// <summary>
        /// 承認できるか
        /// </summary>
        /// <param name="LEDGER_FLOW_ID"></param>
        /// <param name="USER_CD"></param>
        /// <returns></returns>
        public bool CanApproval(int? LEDGER_FLOW_ID, string USER_CD)
        {
            if (LEDGER_FLOW_ID != null)
            {
                var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
                var current = list.First(x => x.DEL_FLAG == 0 && x.RESULT == null);
                return (current.APPROVAL_USER_CD == USER_CD || current.SUBSITUTE_USER_CD == USER_CD);
            }
            else
            {
                return true;
            }
        }

        /// <summary>
        /// 差戻
        /// </summary>
        /// <param name="LEDGER_FLOW_ID">フローID（申請番号）</param>
        /// <param name="USER_CD">ユーザーCD</param>
        /// <param name="MEMO">コメント</param>
        public void Remand(int? LEDGER_FLOW_ID, string USER_CD, string MEMO)
        {
            var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
            var first = list.LastOrDefault(x => x.DEL_FLAG == 0 && x.HIERARCHY == -1);
            var current = list.FirstOrDefault(x => x.DEL_FLAG == 0 && x.RESULT == null);
            if (current != null)
            {
                current.ACTUAL_USER_CD = USER_CD;
                current.RESULT = (int)WorkflowResult.Remand;
                current.MEMO = MEMO;
                save(current);
            }
            remove(list.Where(x => x.RESULT == null), true);
            remove(list.Where(x => x.RESULT != null && x.DEL_FLAG != (int)DEL_FLAG.Deleted));
            using (var w = new UserWorkflowDataInfo() { Context = Context })
            {
                var blank = w.GetBlankWorkflow(LEDGER_FLOW_ID, first.APPROVAL_USER_CD, false,
                    first.APPROVAL_TARGET);
                add(blank);
            }
        }
        /// <summary>
        /// 差し戻せるか
        /// </summary>
        /// <param name="LEDGER_FLOW_ID"></param>
        /// <param name="USER_CD"></param>
        /// <returns></returns>
        public bool CanRemand(int? LEDGER_FLOW_ID, string USER_CD)
        {
            return CanApproval(LEDGER_FLOW_ID, USER_CD);
        }
        /// <summary>
        /// 取戻
        /// </summary>
        /// <param name="LEDGER_FLOW_ID">フローID（申請番号）</param>
        /// <param name="USER_CD">ユーザーCD</param>
        public void Regained(int? LEDGER_FLOW_ID, string USER_CD)
        {
            var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
            var first = list.FirstOrDefault(x => x.DEL_FLAG == 0 && x.HIERARCHY == -1);
            var current = list.FirstOrDefault(x => x.DEL_FLAG == 0 && x.RESULT == null);
            if (current != null)
            {
                current.ACTUAL_USER_CD = USER_CD;
                current.RESULT = (int)WorkflowResult.Regained;
                save(current);
            }
            if (first != null)
            {
                remove(list.Where(x => x.RESULT == null), true);
                using (var w = new UserWorkflowDataInfo() { Context = Context })
                {
                    var blank = w.GetBlankWorkflow(LEDGER_FLOW_ID, first.APPROVAL_USER_CD, false,
                        first.APPROVAL_TARGET);
                    add(blank.Where(x => x.HIERARCHY >= current.HIERARCHY - 1));
                }
            }
        }
        /// <summary>
        /// 取り戻せるか
        /// </summary>
        /// <param name="LEDGER_FLOW_ID"></param>
        /// <param name="USER_CD"></param>
        /// <returns></returns>
        //public bool CanRegained(int? LEDGER_FLOW_ID, string USER_CD)
        //{
        //    var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
        //    var approve = list.Where(x => x.DEL_FLAG == 0 && x.RESULT != null).LastOrDefault();
        //    return approve != null &&
        //        (approve.APPROVAL_USER_CD == USER_CD || approve.SUBSITUTE_USER_CD == USER_CD) &&
        //        approve.RESULT != (int)workflowResult.Regained;
        //}

        /// <summary>
        /// 案件取下
        /// </summary>
        /// <param name="LEDGER_FLOW_ID">フローID（申請番号）</param>
        public void Withdraw(int? LEDGER_FLOW_ID)
        {
            var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
            remove(list.Where(x => x.DEL_FLAG == 0), true);
        }

        /// <summary>
        /// 廃止申請
        /// </summary>
        /// <param name="LEDGER_FLOW_ID">フローID（申請番号）</param>
        /// <param name="USER_CD">ユーザーCD</param>
        public void Abolition(int? LEDGER_FLOW_ID, string USER_CD)
        {
            var list = GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
            var first = list.FirstOrDefault(x => x.DEL_FLAG == 0 && x.HIERARCHY == -1);
            var current = list.FirstOrDefault(x => x.DEL_FLAG == 0 && x.RESULT == null);
            if (current != null)
            {
                current.ACTUAL_USER_CD = USER_CD;
                current.RESULT = (int)WorkflowResult.Application;
                save(current);
            }
            else if (first != null && (first.APPROVAL_USER_CD == USER_CD || first.SUBSITUTE_USER_CD == USER_CD))
            {
                remove(list.Where(x => x.RESULT == null), true);
                remove(list.Where(x => x.RESULT != null));
                using (var w = new UserWorkflowDataInfo() { Context = Context })
                {
                    var blank = w.GetBlankWorkflow(LEDGER_FLOW_ID, first.APPROVAL_USER_CD, false,
                        UserWorkflowDataInfo.HIERARCHY_NULL_Abolish);
                    var curre = blank.FirstOrDefault(x => x.HIERARCHY == -1);
                    curre.ACTUAL_USER_CD = USER_CD;
                    curre.RESULT = (int)WorkflowResult.Application;
                    add(blank);
                }
            }
            else
            {
                remove(list.Where(x => x.RESULT == null), true);
                using (var w = new UserWorkflowDataInfo() { Context = Context })
                {
                    var blank = w.GetBlankWorkflow(LEDGER_FLOW_ID, USER_CD, false,
                        UserWorkflowDataInfo.HIERARCHY_NULL_Abolish);
                    var curre = blank.FirstOrDefault(x => x.HIERARCHY == -1);
                    curre.ACTUAL_USER_CD = USER_CD;
                    curre.RESULT = (int)WorkflowResult.Application;
                    add(blank);
                }
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    userDic = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        protected override void CreateUpdateText(TBL value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.ACTUAL_USER_CD) + " = @" + nameof(value.ACTUAL_USER_CD) + ",");
            builder.AppendLine(" " + nameof(value.RESULT) + " = @" + nameof(value.RESULT) + ",");
            builder.AppendLine(" " + nameof(value.MEMO) + " = @" + nameof(value.MEMO) + ",");
            builder.AppendLine(" " + nameof(value.UPD_DATE) + " = getdate()");
            builder.AppendLine("where ");
            builder.AppendLine(" LED_WORK_HISTORY_ID=@LED_WORK_HISTORY_ID");
            parameters.Add(nameof(value.ACTUAL_USER_CD), value.ACTUAL_USER_CD);
            parameters.Add(nameof(value.RESULT), OrgConvert.ToNullableInt32(value.RESULT));
            parameters.Add(nameof(value.MEMO), value.MEMO);
            parameters.Add(nameof(value.LED_WORK_HISTORY_ID), value.LED_WORK_HISTORY_ID);
            sql = builder.ToString();
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override void CreateInsertText(TBL value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(TBL.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(TBL.FLOW_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.REGISTER_USER_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.HIERARCHY) + ",");
            builder.AppendLine("  " + nameof(TBL.APPROVAL_TARGET) + ",");
            builder.AppendLine("  " + nameof(TBL.APPROVAL_USER_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.SUBSITUTE_USER_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.ACTUAL_USER_CD) + ",");
            builder.AppendLine("  " + nameof(TBL.RESULT) + ",");
            builder.AppendLine("  " + nameof(TBL.MEMO));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(TBL.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  :" + nameof(TBL.FLOW_CD) + ",");
            builder.AppendLine("  :" + nameof(TBL.REGISTER_USER_CD) + ",");
            builder.AppendLine("  :" + nameof(TBL.HIERARCHY) + ",");
            builder.AppendLine("  :" + nameof(TBL.APPROVAL_TARGET) + ",");
            builder.AppendLine("  :" + nameof(TBL.APPROVAL_USER_CD) + ",");
            builder.AppendLine("  :" + nameof(TBL.SUBSITUTE_USER_CD) + ",");
            builder.AppendLine("  :" + nameof(TBL.ACTUAL_USER_CD) + ",");
            builder.AppendLine("  :" + nameof(TBL.RESULT) + ",");
            builder.AppendLine("  :" + nameof(TBL.MEMO));
            builder.AppendLine(" )");
            parameters.Add(nameof(TBL.LEDGER_FLOW_ID), value.LEDGER_FLOW_ID);
            parameters.Add(nameof(TBL.FLOW_CD), value.FLOW_CD);
            parameters.Add(nameof(TBL.REGISTER_USER_CD), value.REGISTER_USER_CD);
            parameters.Add(nameof(TBL.HIERARCHY), value.HIERARCHY);
            parameters.Add(nameof(TBL.APPROVAL_TARGET), value.APPROVAL_TARGET);
            parameters.Add(nameof(TBL.APPROVAL_USER_CD), value.APPROVAL_USER_CD);
            parameters.Add(nameof(TBL.SUBSITUTE_USER_CD), value.SUBSITUTE_USER_CD);
            parameters.Add(nameof(TBL.ACTUAL_USER_CD), value.ACTUAL_USER_CD);
            parameters.Add(nameof(TBL.RESULT), value.RESULT);
            parameters.Add(nameof(TBL.MEMO), value.MEMO);

            sql = builder.ToString();
        }
    }
}