﻿
using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Models;
using ChemMgmt.Nikon.Nikon;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Util;

namespace ZyCoaG.Nikon
{
    public class ChemUsageHistoryModel : T_STOCK
    {
        public ChemUsageHistoryModel() { }

        [Display(Name = "CAS#")]
        public string CAS_NO { get; set; }

        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        public int ACTION_ID { get; set; }

        public int? ACTION_TYPE_ID { get; set; }

        public string ACTION_TYPE_NAME { get; set; }

        public string USER_CD { get; set; }

        public int? ACTION_LOC_ID { get; set; }

        public string ACTION_LOC_NAME { get; set; }

        public int? USAGE_LOC_ID { get; set; }

        public string USAGE_LOC_NAME { get; set; }

        public long? WORKING_TIMES { get; set; }

        public decimal? ACTION_CUR_GROSS_WEIGHT_G { get; set; }

        /// <summary>
        /// 現在重量単位
        /// </summary>
        public string ACTION_CUR_GROSS_WEIGHT_G_UNITSIZE
        {
            get
            {
                if (ACTION_CUR_GROSS_WEIGHT_G.HasValue) return "g";
                return null;
            }
        }

        /// <summary>
        /// 現在重量（風袋込）表示名
        /// </summary>
        public string ACTION_CUR_GROSS_WEIGHT_G_NAME
        {
            get
            {
                return ACTION_CUR_GROSS_WEIGHT_G.ToString() + ACTION_CUR_GROSS_WEIGHT_G_UNITSIZE;
            }
        }

        public decimal? HISTORY_MEMO { get; set; }

        public string USER_NAME { get; set; }

        public string GROUP_ID { get; set; }

        public string GROUP_CD { get; set; }

        public string GROUP_NAME { get; set; }

        public DateTime? HISTORY_REG_DATE { get; set; }

        public string LOC_NAME { get; set; }

        public string GHSCAT_NAME { get; set; }

        public int? REG_TYPE_ID { get; set; }

        public string REG_TYPE_STRING { get; set; }

        public IEnumerable<int> REG_TYPE_ID_COLLECTION
        {
            get
            {
                if (REG_TYPE_STRING == null) yield break;
                foreach (var s in REG_TYPE_STRING?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string REG_TEXT { get; set; }

        public int ACTION_SEQ { get; set; }

        public int? ACTION_DEL_FLAG { get; set; }
    }

    public class ChemUsageHistorySearchModel : ChemUsageHistoryModel
    {
        public int PRODUCT_NAME_CONDITION { get; set; }

        public int USER_NAME_CONDITION { get; set; }

        public int BARCODE_CONDITION { get; set; }

        public IEnumerable<SelectListItem> LookUpBarcodeCondition { get; set; }

        public int REG_NO_CONDITION { get; set; }

        [Display(Name = "発生開始日")]
        public string OCCURRENCE_START_DATE { get; set; }

        [Display(Name = "発生終了日")]
        public string OCCURRENCE_END_DATE { get; set; }

        public int IncludesContained { get; set; } = (int)ChemSearchContained.Without;

        public string GROUP_SELECTION { get; set; }

        public string USER_SELECTION { get; set; }

        [Display(Name = "ユーザー")]
        public IEnumerable<string> USER_COLLECTION
        {
            get
            {
                if (USER_SELECTION == null) yield break;
                foreach (var s in USER_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return s;
                    }
                }
            }
        }

        [Display(Name = "部署名")]
        public IEnumerable<string> GROUP_COLLECTION
        {
            get
            {
                if (GROUP_SELECTION == null) yield break;
                foreach (var s in GROUP_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return s;
                    }
                }
            }
        }

        public string UserSelectionStyle
        {
            get
            {
                return USER_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string UserSelectionText
        {
            get
            {
                return USER_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string UserClickEvent { get; set; }

        public string LOCATION_SELECTION { get; set; }

        [Display(Name = "保管場所/使用場所")]
        public IEnumerable<int> LOCATION_COLLECTION
        {
            get
            {
                if (LOCATION_SELECTION == null) yield break;
                foreach (var s in LOCATION_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string LocationSelectionStyle
        {
            get
            {
                return LOCATION_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string LocationSelectionText
        {
            get
            {
                return LOCATION_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string LocationClickEvent { get; set; }

        public string REG_SELECTION { get; set; }

        [Display(Name = "法規制")]
        public IEnumerable<int> REG_COLLECTION
        {
            get
            {
                if (REG_SELECTION == null) yield break;
                foreach (var s in REG_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string RegulationSelectionStyle
        {
            get
            {
                return REG_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string RegulationSelectionText
        {
            get
            {
                return REG_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string RegulationClickEvent { get; set; }

        public string REFERENCE_AUTHORITY { get; set; }

        public string LOGIN_USER_CD { get; set; }

        //2019/04/01 TPE.Rin Add Start
        public string FIRE_SELECTION { get; set; }

        [Display(Name = "消防法")]
        public IEnumerable<int> FIRE_COLLECTION
        {
            get
            {
                if (FIRE_SELECTION == null) yield break;
                foreach (var s in FIRE_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string FireSelectionStyle
        {
            get
            {
                return FIRE_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string FireSelectionText
        {
            get
            {
                return FIRE_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string FireClickEvent { get; set; }

        public string OFFICE_SELECTION { get; set; }

        [Display(Name = "社内ルール")]
        public IEnumerable<int> OFFICE_COLLECTION
        {
            get
            {
                if (OFFICE_SELECTION == null) yield break;
                foreach (var s in FIRE_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string OfficeSelectionStyle
        {
            get
            {
                return OFFICE_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string OfficeSelectionText
        {
            get
            {
                return OFFICE_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string OfficeClickEvent { get; set; }

        public string GAS_SELECTION { get; set; }

        [Display(Name = "GHS分類")]
        public IEnumerable<int> GAS_COLLECTION
        {
            get
            {
                if (OFFICE_SELECTION == null) yield break;
                foreach (var s in GAS_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string GasSelectionStyle
        {
            get
            {
                return GAS_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string GasSelectionText
        {
            get
            {
                return GAS_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string GasClickEvent { get; set; }

        //2019/04/01 TPE.Rin Add End


    }

}

namespace ChemMgmt.Nikon.Nikon.History
{
    public class ChemUsageHistoryDataInfo : DataInfoBase<ChemUsageHistoryModel>
    {
        protected override string TableName { get; } = "T_ACTION_HISTORY";

        /// <summary>
        /// ユーザー辞書
        /// </summary>
        private IDictionary<string, string> userDic { get; set; }

        /// <summary>
        /// 部署辞書
        /// </summary>
        private IDictionary<string, string> groupDic { get; set; }

        /// <summary>
        /// 保管場所辞書
        /// </summary>
        private IDictionary<int?, string> locationDic { get; set; }

        /// <summary>
        /// 操作種類辞書
        /// </summary>
        private IDictionary<int?, string> actionTypeDic { get; set; }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (ChemUsageHistorySearchModel)dynamicCondition;
            var sql = new StringBuilder();
            sql.AppendLine("from");
            sql.AppendLine(" T_STOCK");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_CHEM");
            sql.AppendLine(" on");
            sql.AppendLine(" T_STOCK.CAT_CD = M_CHEM.CHEM_CD");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" T_ACTION_HISTORY");
            sql.AppendLine(" on");
            sql.AppendLine(" T_STOCK.STOCK_ID = T_ACTION_HISTORY.STOCK_ID");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_GHSCAT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_STOCK.CAT_CD = V_CHEM_GHSCAT.CHEM_CD");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_REGULATION");
            sql.AppendLine(" on");
            sql.AppendLine(" T_STOCK.CAT_CD = V_CHEM_REGULATION.CHEM_CD");
            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (ChemUsageHistorySearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" T_STOCK.STOCK_ID = T_STOCK.STOCK_ID");

            if (!string.IsNullOrWhiteSpace(condition.PRODUCT_NAME_JP))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_STOCK.CAT_CD in");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_ALIAS.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_ALIAS");
                sql.AppendLine("  where");
                switch ((SearchConditionType)condition.PRODUCT_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) = @CHEM_NAME");
                        break;
                }
                sql.AppendLine(" )");
                if ((SearchConditionType)condition.PRODUCT_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add("@CHEM_NAME", condition.PRODUCT_NAME_JP.ToUpper());
                }
                else
                {
                    parameters.Add("@CHEM_NAME", DbUtil.ConvertIntoEscapeChar(condition.PRODUCT_NAME_JP.ToUpper()));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.PRODUCT_NAME_JP)), condition.PRODUCT_NAME_CONDITION, condition.PRODUCT_NAME_JP);
            }


            if (!string.IsNullOrWhiteSpace(condition.BARCODE))
            {
                sql.AppendLine(" and");
                switch ((SearchConditionType)condition.BARCODE_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" T_STOCK.BARCODE like '%' + @BARCODE + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" T_STOCK.BARCODE like @BARCODE + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" T_STOCK.BARCODE like '%' + @BARCODE " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" T_STOCK.BARCODE = @BARCODE");
                        break;
                }
                if ((SearchConditionType)condition.BARCODE_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.BARCODE), condition.BARCODE);
                }
                else
                {
                    parameters.Add(nameof(condition.BARCODE), DbUtil.ConvertIntoEscapeChar(condition.BARCODE));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.BARCODE)), condition.BARCODE_CONDITION, condition.BARCODE);
            }

            if (!string.IsNullOrWhiteSpace(condition.REG_NO))
            {
                sql.AppendLine(" and");
                switch ((SearchConditionType)condition.REG_NO_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" T_STOCK.REG_NO like '%' + @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" T_STOCK.REG_NO like @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" T_STOCK.REG_NO like '%' + @REG_NO " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" T_STOCK.REG_NO = @REG_NO");
                        break;
                }
                if ((SearchConditionType)condition.REG_NO_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.REG_NO), condition.REG_NO);
                }
                else
                {
                    parameters.Add(nameof(condition.REG_NO), DbUtil.ConvertIntoEscapeChar(condition.REG_NO));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_NO)), condition.REG_NO_CONDITION, condition.REG_NO);
            }

            if (!string.IsNullOrWhiteSpace(condition.CAS_NO))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.CAS_NO = @CAS_NO");
                parameters.Add(nameof(condition.CAS_NO), condition.CAS_NO);

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CAS_NO)), condition.CAS_NO);
            }

            //if (condition.USER_COLLECTION.Count() > 0)
            //{
            //    sql.AppendLine(" and");
            //    sql.AppendLine(" T_ACTION_HISTORY.USER_CD in (" + DbUtil.CreateInOperator(condition.USER_COLLECTION.ToList(), nameof(condition.USER_CD), parameters) + ")");

            //    MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.USER_COLLECTION)), condition.USER_COLLECTION, userDic);
            //}

            if (condition.GROUP_COLLECTION.Count() > 0)
            {
                sql.AppendLine("            and T_ACTION_HISTORY.ACTION_GROUP_NAME in (" + DbUtil.CreateInOperator(condition.GROUP_COLLECTION.ToList(), nameof(condition.GROUP_NAME), parameters) + ")");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.GROUP_COLLECTION)), condition.GROUP_COLLECTION.ToList());
            }

            if (!string.IsNullOrWhiteSpace(condition.USER_NAME))
            {

                MakeConditionInLineOfCsv("作業者名", condition.USER_NAME_CONDITION, condition.USER_NAME);
            }

            if (!string.IsNullOrWhiteSpace(condition.OCCURRENCE_START_DATE))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_ACTION_HISTORY.REG_DATE >= @REG_DATE_START");
                parameters.Add("@REG_DATE_START", Convert.ToDateTime(condition.OCCURRENCE_START_DATE).Date);

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.OCCURRENCE_START_DATE)), Convert.ToDateTime(condition.OCCURRENCE_START_DATE).ToShortDateString());
            }

            if (!string.IsNullOrWhiteSpace(condition.OCCURRENCE_END_DATE))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_ACTION_HISTORY.REG_DATE <= @REG_DATE_END");
                parameters.Add("@REG_DATE_END", Convert.ToDateTime(condition.OCCURRENCE_END_DATE).Date.AddHours(23).AddMinutes(59).AddSeconds(59));

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.OCCURRENCE_END_DATE)), Convert.ToDateTime(condition.OCCURRENCE_END_DATE).ToShortDateString());
            }

            var regulationBasedInPharse = string.Empty;
            if (condition.REG_COLLECTION.Count() > 0)
            {
                regulationBasedInPharse = DbUtil.CreateInOperator(condition.REG_COLLECTION.ToList(), nameof(M_REGULATION.REG_TYPE_ID), parameters);
                sql.AppendLine(" and");
                sql.AppendLine(" T_STOCK.CAT_CD in ");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("  distinct(M_CHEM_REGULATION.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("  M_CHEM_REGULATION");
                sql.AppendLine("  where");
                sql.AppendLine("  M_CHEM_REGULATION.REG_TYPE_ID in (" + regulationBasedInPharse + ")");
                sql.AppendLine(" )");

                var regulationDic = new RegulationMasterInfo().GetDictionary();
                var ghsCategoryDic = new GhsCategoryMasterInfo().GetDictionary();
                foreach (var ghsCategory in ghsCategoryDic) regulationDic.Add(ghsCategory);
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_COLLECTION)),
                                         condition.REG_COLLECTION.Where(x => x != NikonConst.GhsCategoryTypeId &&
                                                                             x != NikonConst.RegulationTypeId), regulationDic);
            }

            if (condition.LOCATION_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" (");
                sql.AppendLine("  T_ACTION_HISTORY.LOC_ID in (" + DbUtil.CreateInOperator(condition.LOCATION_COLLECTION.ToList(), nameof(condition.ACTION_LOC_ID), parameters) + ")");
                sql.AppendLine("  or");
                sql.AppendLine("  T_ACTION_HISTORY.USAGE_LOC_ID in (" + DbUtil.CreateInOperator(condition.LOCATION_COLLECTION.ToList(), nameof(condition.USAGE_LOC_ID), parameters) + ")");
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.LOCATION_COLLECTION)),
                                         condition.LOCATION_COLLECTION.Where(x => x != NikonConst.StoringLocationTypeId &&
                                                                                  x != NikonConst.UsageLocationTypeId), locationDic);
            }

            // 含有物を検索します。
            var includesContainedDic = new IncludesContainedListMasterInfo().GetDictionary();
            if (condition.IncludesContained == (int)ChemSearchContained.With)
            {
                if (!string.IsNullOrWhiteSpace(condition.PRODUCT_NAME_JP) || !string.IsNullOrWhiteSpace(condition.CAS_NO))
                {
                    sql.AppendLine(" or");
                    sql.AppendLine(" T_STOCK.CAT_CD in");
                    sql.AppendLine(" (");
                    sql.AppendLine("  select");
                    sql.AppendLine("   distinct(M_CONTAINED.CHEM_CD)");
                    sql.AppendLine("  from");
                    sql.AppendLine("   M_CONTAINED");
                    sql.AppendLine("   inner join");
                    sql.AppendLine("   M_CHEM");
                    sql.AppendLine("   on");
                    sql.AppendLine("   M_CONTAINED.C_CHEM_CD = M_CHEM.CHEM_CD");
                    sql.AppendLine("   left outer join");
                    sql.AppendLine("   M_CHEM_ALIAS");
                    sql.AppendLine("   on");
                    sql.AppendLine("   M_CHEM.CHEM_CD = M_CHEM_ALIAS.CHEM_CD");
                    sql.AppendLine("   left outer join");
                    sql.AppendLine("   M_CHEM_REGULATION");
                    sql.AppendLine("   on");
                    sql.AppendLine("   M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
                    sql.AppendLine("  where");
                    sql.AppendLine("   M_CHEM.CHEM_CD = M_CHEM.CHEM_CD");

                    if (!string.IsNullOrWhiteSpace(condition.PRODUCT_NAME_JP))
                    {
                        sql.AppendLine("   and");
                        switch ((SearchConditionType)condition.PRODUCT_NAME_CONDITION)
                        {
                            case SearchConditionType.PartialMatching:
                                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.ForwardMatching:
                                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.BackwardMatching:
                                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.PerfectMatching:
                                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) = @CHEM_NAME");
                                break;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(condition.CAS_NO))
                    {
                        sql.AppendLine("   and");
                        sql.AppendLine("   M_CHEM.CAS_NO = @CAS_NO");
                    }

                    if (condition.REG_COLLECTION.Count() > 0)
                    {
                        sql.AppendLine("   and");
                        sql.AppendLine("   M_CHEM_REGULATION.REG_TYPE_ID in (" + regulationBasedInPharse + ")");
                    }

                    sql.AppendLine(" )");
                }
            }
            MakeConditionInLineOfCsv("含有物", includesContainedDic[condition.IncludesContained]);

            sql.AppendLine(")");

            if (condition.REFERENCE_AUTHORITY == Const.cREFERENCE_AUTHORITY_FLAG_NORMAL)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" (");
                sql.AppendLine("    T_STOCK.REG_NO in ");
                sql.AppendLine("    (");
                sql.AppendLine("        select distinct(ML.REG_NO) from T_MGMT_LEDGER ML");
                sql.AppendLine("        where ML.GROUP_CD in ");
                sql.AppendLine("        (");
                sql.AppendLine("            (select G1.GROUP_CD from M_USER U1 left outer join M_GROUP G1 on G1.GROUP_CD = U1.GROUP_CD where U1.USER_CD = @USER_CD), ");
                sql.AppendLine("            (select G2.GROUP_CD from M_GROUP G2 where G2.GROUP_CD = (select G5.P_GROUP_CD from M_GROUP G5 left outer join M_USER U5 on G5.GROUP_CD = U5.GROUP_CD where U5.USER_CD = @USER_CD)), ");
                sql.AppendLine("            (select G3.GROUP_CD from M_USER U3 left outer join M_GROUP G3 on U3.PREVIOUS_GROUP_CD = G3.GROUP_CD where U3.USER_CD = @USER_CD and G3.DEL_FLAG = 1), ");
                sql.AppendLine("            (select G4.GROUP_CD from M_GROUP G4 where G4.GROUP_CD = (select G6.P_GROUP_CD from M_USER U6 left outer join M_GROUP G6 on U6.PREVIOUS_GROUP_CD = G6.GROUP_CD where U6.USER_CD = @USER_CD and G6.DEL_FLAG = 1 )) ");
                sql.AppendLine("        ) ");
                sql.AppendLine("    )");
                sql.AppendLine("    or T_STOCK.REG_DATE <= '" + NikonConst.Phase2StartDate + "'");
                sql.AppendLine(" )");

                parameters.Add("@USER_CD", condition.LOGIN_USER_CD);
            }

            if (!string.IsNullOrWhiteSpace(condition.USER_NAME))
            {
                sql.AppendLine(" and");
                switch ((SearchConditionType)condition.USER_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" T_ACTION_HISTORY.ACTION_USER_NM like '%' + @USER_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" T_ACTION_HISTORY.ACTION_USER_NM like @USER_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" T_ACTION_HISTORY.ACTION_USER_NM like '%' + @USER_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" T_ACTION_HISTORY.ACTION_USER_NM = @USER_NAME");
                        break;
                }
                if ((SearchConditionType)condition.USER_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.USER_NAME), condition.USER_NAME);
                }
                else
                {
                    parameters.Add(nameof(condition.USER_NAME), DbUtil.ConvertIntoEscapeChar(condition.USER_NAME));
                }
            }

            sql.AppendLine(" and");
            sql.AppendLine(" T_STOCK.DEL_FLAG = @DEL_FLAG");
            parameters.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Undelete);

            sql.AppendLine(" and");
            sql.AppendLine(" T_STOCK.DEL_FLAG = @ACTION_DEL_FLAG");
            parameters.Add(nameof(condition.ACTION_DEL_FLAG), (int)DEL_FLAG.Undelete);

            return sql.ToString();
        }

        public override List<ChemUsageHistoryModel> GetSearchResult(ChemUsageHistoryModel condition)
        {
            throw new NotImplementedException();
        }

        public List<ChemUsageHistoryModel> GetSearchResult(ChemUsageHistorySearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;
            try
            {
                sql.AppendLine("select");
                sql.AppendLine(" T_STOCK.STOCK_ID,");
                sql.AppendLine(" T_STOCK.REG_NO,");
                sql.AppendLine(" M_CHEM.CHEM_NAME PRODUCT_NAME_JP,");
                sql.AppendLine(" M_CHEM.CAS_NO,");
                sql.AppendLine(" T_STOCK.BARCODE,");
                sql.AppendLine(" T_ACTION_HISTORY.REG_DATE,");
                sql.AppendLine(" T_ACTION_HISTORY.ACTION_TYPE_ID,");
                sql.AppendLine(" T_ACTION_HISTORY.USER_CD,");
                sql.AppendLine(" T_ACTION_HISTORY.LOC_ID ACTION_LOC_ID,");
                sql.AppendLine(" T_ACTION_HISTORY.LOC_NAME ACTION_LOC_NAME,");
                sql.AppendLine(" T_ACTION_HISTORY.USAGE_LOC_ID,");
                sql.AppendLine(" T_ACTION_HISTORY.USAGE_LOC_NAME,");
                sql.AppendLine(" T_ACTION_HISTORY.INTENDED_USE_ID,");
                sql.AppendLine(" T_ACTION_HISTORY.INTENDED_NM,");
                sql.AppendLine(" T_ACTION_HISTORY.INTENDED,");
                sql.AppendLine(" T_ACTION_HISTORY.WORKING_TIMES,");
                sql.AppendLine(" T_ACTION_HISTORY.CUR_GROSS_WEIGHT_G,");
                sql.AppendLine(" V_CHEM_GHSCAT.GHSCAT_ID,");
                sql.AppendLine(" V_CHEM_GHSCAT.GHSCAT_NAME,");
                sql.AppendLine(" V_CHEM_REGULATION.REG_TYPE_ID AS REG_TYPE_STRING,");
                sql.AppendLine(" V_CHEM_REGULATION.REG_TEXT,");
                sql.AppendLine(" T_ACTION_HISTORY.MEMO,");
                sql.AppendLine(" T_STOCK.DEL_FLAG,");
                sql.AppendLine(" T_ACTION_HISTORY.DEL_FLAG ACTION_DEL_FLAG,");
                sql.AppendLine(" T_ACTION_HISTORY.ACTION_USER_NM　USER_NAME,");
                sql.AppendLine(" T_ACTION_HISTORY.ACTION_GROUP_CD GROUP_CD,");
                sql.AppendLine(" T_ACTION_HISTORY.ACTION_GROUP_NAME GROUP_NAME");
                sql.AppendLine(FromWardCreate(condition));
                sql.AppendLine(WhereWardCreate(condition, out parameters));
                //StartTransaction();            
                List<ChemUsageHistoryModel> records = DbUtil.Select<ChemUsageHistoryModel>(sql.ToString(), parameters);

                var dataArray = new List<ChemUsageHistoryModel>();
                //foreach (var record in records)
                //{
                //    var data = new ChemUsageHistoryModel();
                //    data.STOCK_ID = record.STOCK_ID;
                //    data.REG_NO = record.REG_NO;
                //    data.PRODUCT_NAME_JP = record.PRODUCT_NAME_JP;
                //    data.CAS_NO = record.CAS_NO;
                //    data.BARCODE = record.BARCODE;
                //    data.REG_DATE = OrgConvert.ToNullableDateTime((record.REG_DATE).ToString());
                //    data.ACTION_TYPE_ID = OrgConvert.ToNullableInt32((record.ACTION_TYPE_ID).ToString());
                //    data.USER_CD = record.USER_CD;
                //    data.GROUP_ID = record.GROUP_CD;
                //    data.ACTION_LOC_ID = OrgConvert.ToNullableInt32((record.ACTION_LOC_ID).ToString());
                //    data.ACTION_LOC_NAME = record.ACTION_LOC_NAME.ToString();
                //    data.USAGE_LOC_ID = OrgConvert.ToNullableInt32((record.USAGE_LOC_ID).ToString());
                //    data.USAGE_LOC_NAME = record.USAGE_LOC_NAME;
                //    data.INTENDED_USE_ID = record.INTENDED_USE_ID;
                //    data.INTENDED_NM = record.INTENDED_NM;
                //    data.INTENDED = record.INTENDED;
                //    data.WORKING_TIMES = OrgConvert.ToNullableInt64((record.WORKING_TIMES).ToString());
                //    data.ACTION_CUR_GROSS_WEIGHT_G = OrgConvert.ToNullableDecimal((record.CUR_GROSS_WEIGHT_G).ToString());
                //    data.GHSCAT_NAME = record.GHSCAT_NAME;
                //    data.REG_TYPE_STRING = record.REG_TYPE_STRING;
                //    data.REG_TEXT = record.REG_TEXT;
                //    data.MEMO = record.MEMO;
                //    data.DEL_FLAG = OrgConvert.ToNullableInt32((record.DEL_FLAG).ToString());
                //    data.ACTION_DEL_FLAG = OrgConvert.ToNullableInt32((record.ACTION_DEL_FLAG).ToString());

                //    data.USER_NAME = record.USER_NAME;
                //    data.GROUP_NAME = record.GROUP_NAME;
                //    //EditData(data);
                //    dataArray.Add(data);
                //}

                var searchQuery = records.AsQueryable();

                // 動作順を割り当てます。
                foreach (var stockIdGroup in searchQuery.GroupBy(x => x.STOCK_ID))
                {
                    var sorted = stockIdGroup.OrderBy(x => x.REG_DATE);
                    var i = 1;
                    foreach (var actionSeq in stockIdGroup.OrderBy(x => x.REG_DATE))
                    {
                        actionSeq.ACTION_SEQ = i;
                        actionSeq.ACTION_TYPE_NAME = actionTypeDic.GetValueToString(actionSeq.ACTION_TYPE_ID);                      
                        i++;
                    }
                }

                searchQuery = searchQuery.OrderBy(x => x.PRODUCT_NAME_JP).ThenBy(x => x.BARCODE).ThenBy(x => x.STOCK_ID).ThenBy(x => x.REG_DATE);
                return searchQuery.ToList();
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return new List<ChemUsageHistoryModel>().ToList();
        }

        protected override void InitializeDictionary()
        {
            userDic = new UserMasterInfo().GetDictionary();
            groupDic = new GroupMasterInfo().GetDictionary();
            locationDic = new LocationMasterInfo().GetDictionary();
            actionTypeDic = new ActionTypeMasterInfo().GetDictionary();
        }

        public override void EditData(ChemUsageHistoryModel Value)
        {
            Value.ACTION_TYPE_NAME = actionTypeDic.GetValueToString(Value.ACTION_TYPE_ID);
        }

        protected override void CreateInsertText(ChemUsageHistoryModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            insertSql.AppendLine("insert into ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine(" (");
            insertSql.AppendLine(" " + nameof(ChemStockModel.STATUS_ID) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.OWNER) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.UNITSIZE) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.UNITSIZE_ID) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.PRODUCT_NAME_JP) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.MAKER_ID) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.MANAGE_WEIGHT) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.FIRST_LOC_ID) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.INI_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.BOTTLE_WEIGHT_G) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.CUR_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.CAT_CD) + ",");
            insertSql.AppendLine(" " + nameof(ChemStockModel.REG_NO));
            insertSql.AppendLine(" )");
            insertSql.AppendLine("values ");
            insertSql.AppendLine(" (");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.STATUS_ID) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.OWNER) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.UNITSIZE) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.UNITSIZE_ID) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.PRODUCT_NAME_JP) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.MAKER_ID) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.MANAGE_WEIGHT) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.FIRST_LOC_ID) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.INI_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.BOTTLE_WEIGHT_G) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.CUR_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.CAT_CD) + ",");
            insertSql.AppendLine(" :" + nameof(ChemStockModel.REG_NO));
            insertSql.AppendLine(" )");
            parameters.Add(nameof(ChemStockModel.STATUS_ID), (int)Const.cSTOCK_STATUS_ID.STOCK);
            parameters.Add(nameof(ChemStockModel.OWNER), value.OWNER);
            parameters.Add(nameof(ChemStockModel.UNITSIZE), value.COMPOUND_LIST_ID);
            parameters.Add(nameof(ChemStockModel.UNITSIZE_ID), value.CAT_CD);
            parameters.Add(nameof(ChemStockModel.PRODUCT_NAME_JP), value.UNITSIZE);
            parameters.Add(nameof(ChemStockModel.MAKER_ID), value.MAKER_ID);
            parameters.Add(nameof(ChemStockModel.MANAGE_WEIGHT), value.MANAGE_WEIGHT);
            parameters.Add(nameof(ChemStockModel.FIRST_LOC_ID), value.FIRST_LOC_ID);
            parameters.Add(nameof(ChemStockModel.INI_GROSS_WEIGHT_G), value.INI_GROSS_WEIGHT_G);
            parameters.Add(nameof(ChemStockModel.BOTTLE_WEIGHT_G), value.BOTTLE_WEIGHT_G);
            parameters.Add(nameof(ChemStockModel.CUR_GROSS_WEIGHT_G), value.CUR_GROSS_WEIGHT_G);
            parameters.Add(nameof(ChemStockModel.CAT_CD), value.CAT_CD);

            sql = insertSql.ToString();
        }

        protected override void CreateUpdateText(ChemUsageHistoryModel value, out string Sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(ChemUsageHistoryModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(ChemUsageHistoryModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}

