﻿using ChemMgmt.Nikon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Nikon.History
{
    public enum WorkflowResult
    {
        /// <summary>
        /// 申請
        /// </summary>
        Application,
        /// <summary>
        /// 承認
        /// </summary>
        Approval,
        /// <summary>
        /// 取戻
        /// </summary>
        Regained,
        /// <summary>
        /// 差戻
        /// </summary>
        Remand,
    };

    public class WorkflowHistoryModel : V_WORKFLOW_LEDGER_HISTORY
    {

        public WorkflowHistoryModel() { }

        /// <summary>
        /// 承認者
        /// </summary>
        public string APPROVAL_USER_NAME { get; set; }

        /// <summary>
        /// 結果
        /// </summary>
        public string RESULT_TEXT
        {
            get
            {
                switch (RESULT)
                {
                    case (int)WorkflowResult.Application:
                        return "申請";
                    case (int)WorkflowResult.Approval:
                        return "承認";
                    case (int)WorkflowResult.Remand:
                        return "差戻";
                    case (int)WorkflowResult.Regained:
                        return "取戻";
                    default:
                        return "";
                }
            }
        }
    }

    public class WorkflowHistorySearchModel : WorkflowHistoryModel
    {
        public int? MaxSearchResult { get; set; }

        public WorkflowHistorySearchModel()
        {
            SearchHistoryList = new List<WorkflowHistorySearchModel>();
        }

        public IEnumerable<WorkflowHistorySearchModel> SearchHistoryList;

    }
}