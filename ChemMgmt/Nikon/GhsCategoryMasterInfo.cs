﻿using ChemMgmt.Nikon.Models;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon
{
    public class GhsCategoryMasterInfo : IDisposable
    {
        protected DbContext _Context;
        public DbContext Context
        {
            protected get
            {
                return _Context;
            }
            set
            {
                IsInteriorTransaction = false;
                _Context = value;
            }
        }

        protected virtual bool IsInteriorTransaction { get; set; } = true;

        private IEnumerable<V_GHSCAT> dataArray { get; set; }

        public GhsCategoryMasterInfo()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" V_GHSCAT.GHSCAT_ID,");
            sql.AppendLine(" V_GHSCAT.GHSCAT_NAME,");
            sql.AppendLine(" V_GHSCAT.GHSCAT_NAME_BASE,");
            sql.AppendLine(" V_GHSCAT.GHSCAT_NAME_LAST,");
            sql.AppendLine(" V_GHSCAT.GHSCAT_ICON_PATH,");
            sql.AppendLine(" V_GHSCAT.P_GHSCAT_ID,");
            sql.AppendLine(" V_GHSCAT.SORT_ORDER,");
            sql.AppendLine(" V_GHSCAT.DEL_FLAG,");
            sql.AppendLine("N'手入力' as INPUT_FLAG");    //2019/03/12 TPE.Rin Add
            sql.AppendLine("from");
            sql.AppendLine(" V_GHSCAT");

            var parameters = new DynamicParameters();

            //StartTransaction();
            List<V_GHSCAT> records = DbUtil.Select<V_GHSCAT>(sql.ToString(), parameters);
            //Close();

            var dataArray = new List<V_GHSCAT>();
            foreach (var record in records)
            {
                var data = new V_GHSCAT();
                data.GHSCAT_ID = Convert.ToInt32(record.GHSCAT_ID.ToString());
                data.GHSCAT_NAME = record.GHSCAT_NAME;
                data.GHSCAT_NAME_BASE = record.GHSCAT_NAME_BASE;
                data.GHSCAT_NAME_LAST = record.GHSCAT_NAME_LAST;
                if (record.GHSCAT_ICON_PATH != null)
                {
                    data.GHSCAT_ICON_PATH = record.GHSCAT_ICON_PATH;
                }            

                data.P_GHSCAT_ID = OrgConvert.ToNullableInt32(record.P_GHSCAT_ID.ToString());
                data.SORT_ORDER = OrgConvert.ToNullableInt32(record.SORT_ORDER.ToString());
                data.DEL_FLAG = OrgConvert.ToNullableInt32(record.DEL_FLAG.ToString());
                data.INPUT_FLAG = record.INPUT_FLAG.ToString();  //2019/03/12 TPE.Rin Add
                dataArray.Add(data);
            }
            this.dataArray = dataArray;
        }

        /// <summary>
        /// <para>変換辞書を取得します。</para>
        /// <para>重複したデータが存在した場合、後のデータが優先されます。</para>
        /// <para>基本機能→各社用の用語などのために後のデータを優先しています。</para>
        /// </summary>
        /// <returns>変換辞書を<see cref="IDictionary{T,string}"/>で返します。</returns>
        public virtual IDictionary<int?, string> GetDictionary()
        {
            var dic = new Dictionary<int?, string>();
            foreach (var data in dataArray) dic.Add(data.GHSCAT_ID, data.GHSCAT_NAME);
            return dic;
        }

        public IEnumerable<int> GetIdsBelongInChem(string chemCode)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.REG_TYPE_ID));
            sql.AppendLine("from");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION));
            sql.AppendLine("where");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CHEM_CD) + " = @" + nameof(M_CHEM_REGULATION.CHEM_CD));
            sql.AppendLine(" and");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CLASS_ID) + " = @" + nameof(M_CHEM_REGULATION.CLASS_ID));

            var parameters = new DynamicParameters();
            parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);
            parameters.Add(nameof(M_CHEM_REGULATION.CLASS_ID), (int)ClassIdType.GhsCategory);

            //StartTransaction();
            List<M_CHEM_REGULATION> records = DbUtil.Select<M_CHEM_REGULATION>(sql.ToString(), parameters);
            //Close();

            var ids = new List<int>();
            foreach (var record in records)
            {
                ids.Add(Convert.ToInt32(record.REG_TYPE_ID.ToString()));
            }
            return ids;
        }
        public Dictionary<int, int> GetIdsInputFlg(string chemCode)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.REG_TYPE_ID));
            sql.AppendLine(" ,isnull(" + nameof(M_CHEM_REGULATION.INPUT_FLAG) + ",'0') AS INPUT_FLAG ");
            sql.AppendLine("from");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION));
            sql.AppendLine("where");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CHEM_CD) + " = @" + nameof(M_CHEM_REGULATION.CHEM_CD));
            sql.AppendLine(" and");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CLASS_ID) + " = @" + nameof(M_CHEM_REGULATION.CLASS_ID));

            var parameters = new DynamicParameters();
            parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);
            parameters.Add(nameof(M_CHEM_REGULATION.CLASS_ID), (int)ClassIdType.GhsCategory);

            //StartTransaction();
            List<M_CHEM_REGULATION> records = DbUtil.Select<M_CHEM_REGULATION>(sql.ToString(), parameters);
            //Close();

            var ids = new Dictionary<int, int>();
            if (records.Count > 0)
            {
                foreach (var record in records)
                {
                    //ids.Add(Convert.ToInt32(record[nameof(M_CHEM_REGULATION.REG_TYPE_ID)].ToString()), Convert.ToInt32(record[nameof(M_CHEM_REGULATION.INPUT_FLAG)].ToString()));
                    ids.Add(Convert.ToInt32(record.REG_TYPE_ID.ToString()), Convert.ToInt32(record.INPUT_FLAG.ToString()));
                }
            }
            return ids;
        }

        public IEnumerable<V_GHSCAT> GetSelectedItems(IEnumerable<int> ids)
        {
            return dataArray.Where(x => ids.Contains(x.GHSCAT_ID)).OrderBy(x => x.SORT_ORDER).ThenBy(x => x.GHSCAT_ID);
        }

        public V_GHSCAT GetSelectedOneItems(int ids)
        {
            V_GHSCAT ghscat = new V_GHSCAT();
            foreach (V_GHSCAT data in dataArray)
            {
                if (data.GHSCAT_ID == ids)
                {
                    ghscat = data;
                }

            }
            return ghscat;
        }


        protected void StartTransaction()
        {
            if (_Context == null)
            {
                _Context = DbUtil.Open(true);
            }
        }

        protected void Close()
        {
            if (IsInteriorTransaction)
            {
                _Context.Close();
                _Context = null;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    dataArray = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~GhsCategoryInfo() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}