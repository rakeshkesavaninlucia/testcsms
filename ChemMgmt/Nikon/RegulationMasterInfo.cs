﻿using ChemMgmt.Models;
using ChemMgmt.Nikon.Models;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon
{
    public class RegulationMasterInfo :ChemMgmt.Classes.RegulationMasterInfo
    {
        public IEnumerable<int> GetIdsBelongInChem(string chemCode)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.REG_TYPE_ID));
            sql.AppendLine("from");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION));
            sql.AppendLine("where");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CHEM_CD) + " = @" + nameof(M_CHEM_REGULATION.CHEM_CD));
            //2019/05/18 TPE.Rin Add Start
            sql.AppendLine(" and");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CLASS_ID) + " = @" + nameof(M_CHEM_REGULATION.CLASS_ID));
            //2019/05/18 TPE.Rin Add End


            var parameters = new DynamicParameters();
            parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);
            //2019/05/18 TPE.Rin Add Start
            parameters.Add(nameof(M_CHEM_REGULATION.CLASS_ID), (int)ClassIdType.Regulation);
            //2019/05/18 TPE.Rin Add End


            //StartTransaction();
            List<V_REGULATION> records = DbUtil.Select<V_REGULATION>(sql.ToString(), parameters);
            //Close();

            var ids = new List<int>();
            foreach (var record in records)
            {
                ids.Add(Convert.ToInt32(record.REG_TYPE_ID.ToString()));
            }
            return ids;
        }

        public Dictionary<int, int> GetIdsInputFlg(string chemCode)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.REG_TYPE_ID));
            sql.AppendLine(" ,isnull(" + nameof(M_CHEM_REGULATION.INPUT_FLAG) + ",'0') AS INPUT_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION));
            sql.AppendLine("where");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CHEM_CD) + " = @" + nameof(M_CHEM_REGULATION.CHEM_CD));
            sql.AppendLine(" and");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CLASS_ID) + " = @" + nameof(M_CHEM_REGULATION.CLASS_ID));

            var parameters = new DynamicParameters();
            parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);
            parameters.Add(nameof(M_CHEM_REGULATION.CLASS_ID), (int)ClassIdType.Regulation);

            //StartTransaction();
            List<M_CHEM_REGULATION> records = DbUtil.Select<M_CHEM_REGULATION>(sql.ToString(), parameters);
            //Close();

            var ids = new Dictionary<int, int>();
            if (records.Count > 0)
            {
                foreach (var record in records)
                {
                    //ids.Add(Convert.ToInt32(record[nameof(M_CHEM_REGULATION.REG_TYPE_ID)].ToString()), Convert.ToInt32(record[nameof(M_CHEM_REGULATION.INPUT_FLAG)].ToString()));
                    ids.Add(Convert.ToInt32(record.REG_TYPE_ID), Convert.ToInt32(record.INPUT_FLAG));
                }
            }
            return ids;
        }

        ///// <summary>
        ///// 化学物質(単体、混合物含む)の法規制IDを取得します。
        ///// </summary>
        //public IEnumerable<int> LedgerGetIdsBelongInChem(string chemCode)
        //{
        //    string strWCHEM = "V_WCHEM";
        //    var sql = new StringBuilder();
        //    sql.AppendLine("select");
        //    sql.AppendLine("distinct");
        //    sql.AppendLine(nameof(V_CAS_REGULATION) + ".REG_TYPE_ID");
        //    sql.AppendLine("from");
        //    sql.AppendLine(strWCHEM);
        //    sql.AppendLine("inner join");
        //    sql.AppendLine(nameof(V_CAS_REGULATION));
        //    sql.AppendLine("on");
        //    sql.AppendLine(strWCHEM + ".CAS_NO = " + nameof(V_CAS_REGULATION) + ".CASNO");
        //    sql.AppendLine("where " + strWCHEM + ".CHEM_CD = @CHEM_CD");
        //    sql.AppendLine("order by " + nameof(V_CAS_REGULATION) + ".REG_TYPE_ID");

        //    var parameters = new Hashtable();
        //    parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);

        //    StartTransaction();
        //    DataTable records = DbUtil.select(_Context, sql.ToString(), parameters);
        //    Close();

        //    var ids = new List<int>();
        //    foreach (DataRow record in records.Rows)
        //    {
        //        ids.Add(Convert.ToInt32(record[nameof(V_REGULATION.REG_TYPE_ID)].ToString()));
        //    }
        //    return ids;
        //}
    }
}
