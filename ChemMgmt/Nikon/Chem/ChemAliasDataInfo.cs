﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon.Models;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Chem
{
    public class ChemAliasModel : M_CHEM_ALIAS
    {
        public ChemAliasModel() { }

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }
    }

    public class ChemAliasSearchModel : ChemAliasModel
    {

    }
}

namespace ChemMgmt.Nikon.Chem
{
    public class ChemAliasDataInfo : DataInfoBase<ChemAliasModel>, IDisposable
    {
        protected override string TableName { get; } = nameof(M_CHEM_ALIAS);

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            throw new NotImplementedException();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (ChemAliasModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine(" M_CHEM_ALIAS.CHEM_CD = M_CHEM_ALIAS.CHEM_CD");

            if (!string.IsNullOrWhiteSpace(condition.CHEM_CD))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM_ALIAS.CHEM_CD = @CHEM_CD");
                parameters.Add(nameof(condition.CHEM_CD), condition.CHEM_CD);
            }

            return sql.ToString();
        }

        public override List<ChemAliasModel> GetSearchResult(ChemAliasModel condition)
        {
            var sql = new StringBuilder();
            //Hashtable parameters = null;
            DynamicParameters parameters = new DynamicParameters();

            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(ChemAliasModel.CHEM_CD) + ",");
            sql.AppendLine(" " + nameof(ChemAliasModel.CHEM_NAME_SEQ) + ",");
            sql.AppendLine(" " + nameof(ChemAliasModel.CHEM_NAME));
            sql.AppendLine("from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            List<ChemAliasModel> records = DbUtil.Select<ChemAliasModel>(sql.ToString(), parameters);

            var dataArray = new List<ChemAliasModel>();
            foreach (var record in records)
            {
                var data = new ChemAliasModel();
                data.CHEM_CD = record.CHEM_CD.ToString();
                data.CHEM_NAME_SEQ = Convert.ToInt32(record.CHEM_NAME_SEQ);
                data.CHEM_NAME = record.CHEM_NAME.ToString();

                dataArray.Add(data);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.CHEM_CD).OrderBy(x => x.CHEM_NAME_SEQ).ThenBy(x => x.CHEM_NAME);   //2018/09/21 TPE.Kometani Upd

            return searchQuery.ToList();
        }

        protected override void InitializeDictionary() { }

        public override void EditData(ChemAliasModel Value)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(ChemAliasModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            insertSql.AppendLine("insert into ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  " + nameof(M_CHEM_ALIAS.CHEM_CD) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_ALIAS.CHEM_NAME_SEQ) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_ALIAS.CHEM_NAME));
            insertSql.AppendLine(" )");
            insertSql.AppendLine("values ");
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  @" + nameof(M_CHEM_ALIAS.CHEM_CD) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_ALIAS.CHEM_NAME_SEQ) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_ALIAS.CHEM_NAME));
            insertSql.AppendLine(" )");
            parameters.Add(nameof(M_CHEM_ALIAS.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(M_CHEM_ALIAS.CHEM_NAME_SEQ), value.CHEM_NAME_SEQ);
            parameters.Add(nameof(M_CHEM_ALIAS.CHEM_NAME), value.CHEM_NAME);

            sql = insertSql.ToString();
        }

        protected override void CreateUpdateText(ChemAliasModel value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }
        //2018/08/17 TPE.Sugimoto Add Sta
        public void IsUsedChemAliasName(ChemAliasModel value, ChemModel chemvalue)
        {
            var sql = new StringBuilder();
            var parameters = new DynamicParameters();

            sql.AppendLine("select");
            sql.AppendLine(" A.CHEM_NAME");
            sql.AppendLine("from");
            sql.AppendLine(" (select CHEM_CD,CHEM_NAME ");
            sql.AppendLine("from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("where");
            sql.AppendLine(" not CHEM_NAME_SEQ = 0 and ");
            if (!string.IsNullOrWhiteSpace(value.CHEM_CD))
            {
                // 2018/12/12 Upd Start TPE Kometani
                //sql.AppendLine(" not(" + getKeyWhere(value) + ")");
                //sql.AppendLine(" and");
                //var keyColumns = getKeyProperty(value);
                //parameters = keyColumns.ToHashtable();
                sql.AppendLine(" not " + nameof(value.CHEM_CD) + " = @" + nameof(chemvalue.CHEM_CD));
                sql.AppendLine(" and");
                parameters.Add(nameof(value.CHEM_CD), value.CHEM_CD);
                // 2018/12/12 Upd End TPE Kometani
            }
            // 20180723 FJ)Shinagawa Add Start ->
            if (!string.IsNullOrWhiteSpace(chemvalue.REF_CHEM_CD))
            {
                // 2018/12/12 Upd Start TPE Kometani
                //sql.AppendLine(" not(" + nameof(value.CHEM_CD) + " = @" + nameof(chemvalue.REF_CHEM_CD));
                //sql.AppendLine(" and " + nameof(value.CHEM_NAME_SEQ) + " = @" + nameof(value.CHEM_NAME_SEQ) + ")");
                //parameters.Add(nameof(chemvalue.REF_CHEM_CD), chemvalue.REF_CHEM_CD);
                //if (!parameters.ContainsKey(nameof(value.CHEM_NAME_SEQ))) parameters.Add(nameof(value.CHEM_NAME_SEQ), value.CHEM_NAME_SEQ);
                sql.AppendLine(" not " + nameof(value.CHEM_CD) + " = @" + nameof(chemvalue.REF_CHEM_CD));
                sql.AppendLine(" and");
                parameters.Add(nameof(chemvalue.REF_CHEM_CD), chemvalue.REF_CHEM_CD);
                // 2018/12/12 Upd End TPE Kometani
            }
            // 20180723 FJ)Shinagawa Add End <-
            sql.AppendLine(" REPLACE(" + nameof(value.CHEM_NAME) + ",' ','') = @" + nameof(value.CHEM_NAME) + " COLLATE Japanese_CI_AS_KS ) A ");   //2018/09/20 TPE.Kometani Upd
            sql.AppendLine("LEFT JOIN M_CHEM ");
            sql.AppendLine("ON A.CHEM_CD = M_CHEM.CHEM_CD ");
            //sql.AppendLine(" and");
            sql.AppendLine("where not( M_CHEM." + nameof(value.DEL_FLAG) + " = @" + nameof(value.DEL_FLAG) + ")");
            
            //string Paramvalue = parameters.Get<string>(value.CHEM_NAME).ToString();
            //if (!Paramvalue.Contains(value.CHEM_NAME))
            //{
                parameters.Add(nameof(value.CHEM_NAME), value.CHEM_NAME.Replace(" ", "").Replace("　", ""));
            //}

            parameters.Add(nameof(value.DEL_FLAG), (int)DEL_FLAG.Temporary);


            List<ChemAliasModel> records = DbUtil.Select<ChemAliasModel>(sql.ToString(), parameters);

            if (records.Count > 0) //return true;
            {                        //return false;
                IsValid = false;
                var rowNumberString = string.Empty;
                ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotUnique, "化学物質別名", value.CHEM_NAME));
            }
        }

        protected override void CreateInsertText(ChemAliasModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(ChemAliasModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
        //2018/08/17 TPE.Sugimoto Add End
    }
}