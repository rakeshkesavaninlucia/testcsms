﻿using ChemMgmt.Classes;

using ChemMgmt.Nikon.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Chem
{
    public class ChemRegulationDataInfo : DataInfoBase<M_CHEM_REGULATION>, IDisposable
    {
        protected override string TableName { get; } = nameof(M_CHEM_REGULATION);

        protected virtual ClassIdType ClassId { get; set; }

        public ChemRegulationDataInfo(ClassIdType classId)
        {
            ClassId = classId;
        }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var sql = new StringBuilder();

            sql.AppendLine("from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine(" inner join");
            sql.AppendLine(" M_REGULATION");
            sql.AppendLine(" on");
            sql.AppendLine(" M_CHEM_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID");

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (M_CHEM_REGULATION)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine(" M_CHEM_REGULATION.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");

            if (!string.IsNullOrWhiteSpace(condition.CLASS_ID.ToString()))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM_REGULATION.CLASS_ID = @CLASS_ID");
                parameters.Add(nameof(condition.CLASS_ID), condition.CLASS_ID);
            }

            if (!string.IsNullOrWhiteSpace(condition.CHEM_CD))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM_REGULATION.CHEM_CD = @CHEM_CD");
                parameters.Add(nameof(condition.CHEM_CD), condition.CHEM_CD);
            }

            return sql.ToString();
        }

        public override List<M_CHEM_REGULATION> GetSearchResult(M_CHEM_REGULATION condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION) + "." + nameof(M_CHEM_REGULATION.CHEM_CD) + ",");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION) + "." + nameof(M_CHEM_REGULATION.REG_TYPE_ID) + ",");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION) + "." + nameof(M_CHEM_REGULATION.CLASS_ID) + ",");
            sql.AppendLine(" " + nameof(M_REGULATION) + "." + nameof(M_CHEM_REGULATION.SORT_ORDER));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            
            List<M_CHEM_REGULATION> records = DbUtil.Select<M_CHEM_REGULATION>(sql.ToString(), parameters);

            var dataArray = new List<M_CHEM_REGULATION>();
            foreach (var record in records)
            {
                var data = new M_CHEM_REGULATION();
                data.CHEM_CD = record.CHEM_CD.ToString();
                data.REG_TYPE_ID = Convert.ToInt32(record.REG_TYPE_ID.ToString());
                data.CLASS_ID = OrgConvert.ToNullableInt32(record.CLASS_ID.ToString());
                data.SORT_ORDER = OrgConvert.ToNullableInt32(record.SORT_ORDER.ToString());

                dataArray.Add(data);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.CHEM_CD).ThenBy(x => x.SORT_ORDER);

            return searchQuery.ToList();
        }

        protected override void InitializeDictionary() { }

        public override void EditData(M_CHEM_REGULATION Value)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(M_CHEM_REGULATION value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            insertSql.AppendLine("insert into ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  " + nameof(M_CHEM_REGULATION.REG_TYPE_ID) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_REGULATION.CHEM_CD) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_REGULATION.CLASS_ID) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_REGULATION.INPUT_FLAG));    //20190321 TPE.Rin Add
            insertSql.AppendLine(" )");
            insertSql.AppendLine("values ");
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  @" + nameof(M_CHEM_REGULATION.REG_TYPE_ID) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_REGULATION.CHEM_CD) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_REGULATION.CLASS_ID) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_REGULATION.INPUT_FLAG));    //20190321 TPE.Rin Add
            insertSql.AppendLine(" )");
            parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(M_CHEM_REGULATION.REG_TYPE_ID), value.REG_TYPE_ID);
            parameters.Add(nameof(M_CHEM_REGULATION.CLASS_ID), (int)ClassId);
            parameters.Add(nameof(M_CHEM_REGULATION.INPUT_FLAG), value.INPUT_FLAG);

            sql = insertSql.ToString();
        }

        protected override void CreateUpdateText(M_CHEM_REGULATION value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(M_CHEM_REGULATION value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(M_CHEM_REGULATION value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}