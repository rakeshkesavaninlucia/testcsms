﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon.Chem;
using ChemMgmt.Nikon.Models;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Chem
{

    public class ChemContainedModel : M_CONTAINED
    {

        private int _Mode { get; set; }

        public ChemContainedModel() { }

        public ChemContainedModel(ChemModel value)
        {
            C_CHEM_CD = value.CHEM_CD;
            CHEM_NAME = value.CHEM_NAME;
            CAS_NO = value.CAS_NO;
            DENSITY = value.DENSITY;
        }

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

        /// <summary>
        /// 成分
        /// </summary>
        public string CHEM_NAME { get; set; }

        /// <summary>
        /// CAS#
        /// </summary>
        public string CAS_NO { get; set; }

        /// <summary>
        /// 比重
        /// </summary>
        public decimal? DENSITY { get; set; }
    }

    public class ChemContainedSearchModel : ChemContainedModel
    {
        public ChemContainedSearchModel()
        {
            ChildChemCodes = new List<string>();
        }

        public int CHEM_NAME_CONDITION { get; set; }

        public IEnumerable<string> ChildChemCodes { get; set; }
    }
}

namespace ZyCoaG.Nikon.Chem
{
    public class ChemContainedDataInfo : DataInfoBase<ChemContainedModel>, IDisposable
    {
        protected override string TableName { get; } = nameof(M_CONTAINED);

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (ChemContainedSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            sql.AppendLine("from");
            sql.AppendLine(" M_CHEM");
            sql.AppendLine(" inner join");
            sql.AppendLine(" M_CONTAINED");
            sql.AppendLine(" on");
            sql.AppendLine(" M_CHEM.CHEM_CD =M_CONTAINED.C_CHEM_CD");
            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (ChemContainedSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine(" M_CONTAINED.CHEM_CD =M_CONTAINED.CHEM_CD");

            if (!string.IsNullOrWhiteSpace(condition.CHEM_NAME))
            {
                sql.AppendLine(" and");
                switch ((SearchConditionType)condition.CHEM_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" M_CHEM.CHEM_NAME like '%' || @CHEM_NAME || '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" M_CHEM.CHEM_NAME like @CHEM_NAME || '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" M_CHEM.CHEM_NAME like '%' || @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" M_CHEM.CHEM_NAME =@CHEM_NAME");
                        break;
                }
                if ((SearchConditionType)condition.CHEM_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.CHEM_NAME), condition.CHEM_NAME);
                }
                else
                {
                    parameters.Add(nameof(condition.CHEM_NAME), DbUtil.ConvertIntoEscapeChar(condition.CHEM_NAME));
                }
            }

            if (!string.IsNullOrWhiteSpace(condition.CHEM_CD))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CONTAINED.CHEM_CD=@CHEM_CD");
                parameters.Add(nameof(condition.CHEM_CD), condition.CHEM_CD);
            }

            if (!string.IsNullOrWhiteSpace(condition.CAS_NO))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.CAS_NO =@CAS_NO");
                parameters.Add(nameof(condition.CAS_NO), condition.CAS_NO);
            }

            return sql.ToString();
        }

        public override List<ChemContainedModel> GetSearchResult(ChemContainedModel condition)
        {
            throw new NotImplementedException();
        }

        //public IQueryable<ChemContainedModel> GetSearchResult(ChemContainedSearchModel condition)
        //{
        //    var sql = new StringBuilder();
        //    DynamicParameters parameters = null;

        //    sql.AppendLine("select");
        //    sql.AppendLine(" M_CONTAINED.CHEM_CD,");
        //    sql.AppendLine(" M_CHEM.CHEM_CD C_CHEM_CD,");
        //    sql.AppendLine(" M_CHEM.CHEM_NAME,");
        //    sql.AppendLine(" M_CHEM.DENSITY,");
        //    sql.AppendLine(" M_CHEM.CAS_NO,");
        //    sql.AppendLine(" M_CONTAINED.MIN_CONTAINED_RATE,");
        //    sql.AppendLine(" M_CONTAINED.MAX_CONTAINED_RATE,");
        //    sql.AppendLine(" M_CHEM.DEL_FLAG");
        //    sql.AppendLine(FromWardCreate(condition));
        //    sql.AppendLine(WhereWardCreate(condition, out parameters));

        //    List<ChemContainedModel> records = DbUtil.Select<ChemContainedModel>(sql.ToString(), parameters);
        //    var dataArray = new List<ChemContainedModel>();
        //    foreach (var record in records)
        //    {
        //        var data = new ChemContainedModel();
        //        data.CHEM_CD = record.CHEM_CD.ToString();
        //        data.C_CHEM_CD = record.C_CHEM_CD.ToString();
        //        data.CHEM_NAME = record.CHEM_NAME.ToString();
        //        data.DENSITY = OrgConvert.ToNullableDecimal(record.DENSITY.ToString());if(record.CAS_NO!=null)
        //        {
        //            data.CAS_NO = record.CAS_NO.ToString();
        //        }

        //        data.MIN_CONTAINED_RATE = OrgConvert.ToNullableDecimal(record.MIN_CONTAINED_RATE.ToString());
        //        data.MinContainedRate = data.MIN_CONTAINED_RATE.ToString();
        //        data.MAX_CONTAINED_RATE = OrgConvert.ToNullableDecimal(record.MAX_CONTAINED_RATE.ToString());
        //        data.MaxContainedRate = data.MAX_CONTAINED_RATE.ToString();
        //        data.DEL_FLAG = OrgConvert.ToNullableInt32(record.DEL_FLAG.ToString());
        //        dataArray.Add(data);
        //    }

        //    var searchQuery = dataArray.AsQueryable();
        //    searchQuery = searchQuery.OrderBy(x => x.CHEM_CD).ThenByDescending(x => x.MIN_CONTAINED_RATE)
        //                             .ThenByDescending(x => x.MAX_CONTAINED_RATE).ThenBy(x => x.CHEM_NAME);

        //    return searchQuery;
        //}


        public IQueryable<ChemContainedModel> GetSearchResult(ChemContainedSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            sql.AppendLine(" M_CONTAINED.CHEM_CD,");
            sql.AppendLine(" M_CHEM.CHEM_CD C_CHEM_CD,");
            sql.AppendLine(" M_CHEM.CHEM_NAME,");
            sql.AppendLine(" M_CHEM.DENSITY,");
            sql.AppendLine(" M_CHEM.CAS_NO,");
            sql.AppendLine(" M_CONTAINED.MIN_CONTAINED_RATE,");
            sql.AppendLine(" M_CONTAINED.MAX_CONTAINED_RATE,");
            sql.AppendLine(" M_CHEM.DEL_FLAG");
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));

            List<ChemContainedModel> records = DbUtil.Select<ChemContainedModel>(sql.ToString(), parameters);
            var dataArray = new List<ChemContainedModel>();
            int UniqueRowId = 1;
            foreach (var record in records)
            {
                var data = new ChemContainedModel();
                data.CHEM_CD = record.CHEM_CD.ToString();
                data.C_CHEM_CD = record.C_CHEM_CD.ToString();
                data.CHEM_NAME = record.CHEM_NAME.ToString();
                data.DENSITY = OrgConvert.ToNullableDecimal(record.DENSITY.ToString()); if (record.CAS_NO != null)
                {
                    data.CAS_NO = record.CAS_NO.ToString();
                }

                data.MIN_CONTAINED_RATE = OrgConvert.ToNullableDecimal(record.MIN_CONTAINED_RATE.ToString());
                data.MinContainedRate = data.MIN_CONTAINED_RATE.ToString();
                data.MAX_CONTAINED_RATE = OrgConvert.ToNullableDecimal(record.MAX_CONTAINED_RATE.ToString());
                data.MaxContainedRate = data.MAX_CONTAINED_RATE.ToString();
                data.DEL_FLAG = OrgConvert.ToNullableInt32(record.DEL_FLAG.ToString());
                data.UniqueRowId = UniqueRowId;
                UniqueRowId++;
                dataArray.Add(data);

            }

            var searchQuery = dataArray.AsQueryable();
            searchQuery = searchQuery.OrderBy(x => x.CHEM_CD).ThenByDescending(x => x.MIN_CONTAINED_RATE)
                                     .ThenByDescending(x => x.MAX_CONTAINED_RATE).ThenBy(x => x.CHEM_NAME);

            return searchQuery;
        }

        protected override void InitializeDictionary() { }

        public override void EditData(ChemContainedModel Value)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(ChemContainedModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();          

            insertSql.AppendLine("insert into ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  " + nameof(M_CONTAINED.CHEM_CD) + ",");
            insertSql.AppendLine("  " + nameof(M_CONTAINED.C_CHEM_CD) + ",");
            insertSql.AppendLine("  " + nameof(M_CONTAINED.MIN_CONTAINED_RATE) + ",");
            insertSql.AppendLine("  " + nameof(M_CONTAINED.MAX_CONTAINED_RATE));
            insertSql.AppendLine(" )");
            insertSql.AppendLine("values ");
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  @" + nameof(M_CONTAINED.CHEM_CD) + ",");
            insertSql.AppendLine("  @" + nameof(M_CONTAINED.C_CHEM_CD) + ",");
            insertSql.AppendLine("  @" + nameof(M_CONTAINED.MIN_CONTAINED_RATE) + ",");
            insertSql.AppendLine("  @" + nameof(M_CONTAINED.MAX_CONTAINED_RATE));
            insertSql.AppendLine(" )");
            parameters.Add(nameof(M_CONTAINED.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(M_CONTAINED.C_CHEM_CD), value.C_CHEM_CD);
            parameters.Add(nameof(M_CONTAINED.MIN_CONTAINED_RATE), OrgConvert.ToNullableDecimal(value.MinContainedRate));
            parameters.Add(nameof(M_CONTAINED.MAX_CONTAINED_RATE), OrgConvert.ToNullableDecimal(value.MaxContainedRate));

            sql = insertSql.ToString();
        }

        protected override void CreateUpdateText(ChemContainedModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();          

            insertSql.AppendLine("update ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine("set ");
            insertSql.AppendLine(" " + nameof(M_CONTAINED.CHEM_CD) + " = @" + nameof(M_CONTAINED.CHEM_CD) + ",");
            insertSql.AppendLine(" " + nameof(M_CONTAINED.C_CHEM_CD) + " = @" + nameof(M_CONTAINED.C_CHEM_CD) + ",");
            insertSql.AppendLine(" " + nameof(M_CONTAINED.MIN_CONTAINED_RATE) + " = @" + nameof(M_CONTAINED.MIN_CONTAINED_RATE) + ",");
            insertSql.AppendLine(" " + nameof(M_CONTAINED.MAX_CONTAINED_RATE) + " = @" + nameof(M_CONTAINED.MAX_CONTAINED_RATE));
            insertSql.AppendLine("where ");
            insertSql.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(M_CONTAINED.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(M_CONTAINED.C_CHEM_CD), value.C_CHEM_CD);
            parameters.Add(nameof(M_CONTAINED.MIN_CONTAINED_RATE), OrgConvert.ToNullableDecimal(value.MinContainedRate));
            parameters.Add(nameof(M_CONTAINED.MAX_CONTAINED_RATE), OrgConvert.ToNullableDecimal(value.MaxContainedRate));

            sql = insertSql.ToString();
        }

        protected override void CreateInsertText(ChemContainedModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(ChemContainedModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}