﻿using ChemMgmt.Models;
using ChemMgmt.Classes;
using ChemMgmt.Nikon.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using ZyCoaG;
using ZyCoaG.Util;
using ZyCoaG.Nikon;
using ZyCoaG.Classes.util;
using ChemMgmt.Classes.Extensions;
using ZyCoaG.Extensions;

namespace ChemMgmt.Nikon
{
    public class ChemModel : M_CHEM
    {

        public int _Mode { get; set; }
        /// <summary>
        /// 編集モード
        /// </summary>
        public Mode Mode
        {
            get
            {
                Mode resultMode;
                if (Mode.TryParse(_Mode.ToString(), out resultMode)) return resultMode;
                return default(Mode);
            }
            set
            {
                _Mode = (int)value;
            }
        }

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

        /// <summary>
        /// 法規制
        /// </summary>
        public IEnumerable<int> GHSCAT_ID { get; set; }

        /// <summary>
        /// GHS分類
        /// </summary>
        public string GHSCAT_NAME { get; set; }

        /// <summary>
        /// 法規制
        /// </summary>
        public int REG_TYPE_ID { get; set; }

        /// <summary>
        /// 法規制名
        /// </summary>
        public string REG_TEXT { get; set; }

        /// <summary>
        /// メーカーID
        /// </summary>
        public int? MAKER_ID { get; set; }

        /// <summary>
        /// メーカー名
        /// </summary>
        [Display(Name = "メーカー名")]
        public string MAKER_NAME { get; set; }

        public int MAKER_NAME_CONDITION { get; set; }
        /// <summary>
        /// メーカー名削除フラグ
        /// </summary>
        public bool MAKER_NAME_DEL_FLAG { get; set; }

        /// <summary>
        /// 商品シーケンス
        /// </summary>
        public int? PRODUCT_SEQ { get; set; }

        /// <summary>
        /// 商品名
        /// </summary>
        public string PRODUCT_NAME { get; set; }

        /// <summary>
        /// 容量
        /// </summary>
        public string UNITSIZE { get; set; }

        /// <summary>
        /// 単位ID
        /// </summary>
        public int? UNITSIZE_ID { get; set; }

        /// <summary>
        /// 単位名
        /// </summary>
        [Display(Name = "容量単位")]
        public string UNITSIZE_NAME { get; set; }

        /// <summary>
        /// 単位削除フラグ
        /// </summary>
        public bool UNITSIZE_NAME_DEL_FLAG { get; set; }

        private string _PRODUCT_BARCODE { get; set; }

        /// <summary>
        /// 商品バーコード
        /// </summary>
        [Display(Name = "商品バーコード")]
        public string PRODUCT_BARCODE
        {
            get
            {
                return _PRODUCT_BARCODE;
            }
            set
            {
                _PRODUCT_BARCODE = value?.ToUpper();
            }
        }

        //2018/8/17 Add FJ

        /// <summary>
        /// SDS最終更新日
        /// </summary>
        public string SDS_UPD_DATE_DISPLAY
        {
            get
            {
                if (SDS_UPD_DATE2 != null)
                {
                    return SDS_UPD_DATE2.Value.ToShortDateString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        /// <summary>
        /// SDSリンクURL
        /// </summary>
        [Display(Name = "SDS (URL)")]
        public string SDS_URL2 { get; set; }

        /// <summary>
        /// SDS更新日
        /// </summary>
        [DataType(DataType.Date), DisplayFormat(DataFormatString = @"{0:dd\-MM\-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime? SDS_UPD_DATE2 { get; set; }

        /// <summary>
        /// SDS更新日
        /// </summary>
        public string SdsUpdateDate2 { get; set; }
        public string SDS_UPD_DATE3 { get; set; }

        //-------------------------------------------------------

        /// <summary>
        /// SDSリンクURL
        /// </summary>
        [Display(Name = "SDS (URL)")]
        public string SDS_URL { get; set; }

        /// <summary>
        /// SDS更新日
        /// </summary>
        public string SDS_UPD_DATE { get; set; }

        /// <summary>
        /// SDS更新日
        /// </summary>
        public string SdsUpdateDate { get; set; }
        
        /// <summary>
        /// SDS_MIMETYPE
        /// </summary>
        public string SDS_MIMETYPE { get; set; }

        /// <summary>
        /// SDS
        /// </summary>
        [Display(Name = "SDS (PDF)")]
        public Byte[] SDS { get; set; } = new Byte[0];

        /// <summary>
        /// CHEM_SDS_FILE
        /// </summary>
        public string CHEM_SDS_FILE { get; set; }

        /// <summary>
        /// SDS_FILENAME
        /// </summary>
        public string SDS_FILENAME { get; set; }

        /// <summary>
        /// CHEM_SDS_URL
        /// </summary>
        public string CHEM_SDS_URL { get; set; }
        //2018/08/09 TPE.Sugimoto Add End

        /// <summary>
        /// 鍵管理名
        /// </summary>
        public string KEY_MGMT_NAME { get; set; }

        /// <summary>
        /// 重量管理名
        /// </summary>
        public string WEIGHT_MGMT_NAME { get; set; }

        /// <summary>
        /// 形状名
        /// </summary>
        public string FIGURE_NAME { get; set; }

        /// <summary>
        /// ユニークコード
        /// </summary>
        public string CODE
        {
            get
            {
                return CHEM_CD + SystemConst.CodeDelimiter + PRODUCT_SEQ.ToString();
            }
        }

        /// <summary>
        /// 容量+単位名
        /// </summary>
        public string UNITSIZE_NAME_JOIN
        {
            get
            {
                return UNITSIZE + UNITSIZE_NAME;
            }
        }

        /// <summary>
        /// 登録者
        /// </summary>
        public string REG_USER_NAME { get; set; }

        /// <summary>
        /// 更新者
        /// </summary>
        public string UPD_USER_NAME { get; set; }

        /// <summary>
        /// 削除者
        /// </summary>
        public string DEL_USER_NAME { get; set; }

        /// <summary>
        /// 状態名
        /// </summary>
        public string USAGE_STATUS_NAME { get; set; }

        /// <summary>
        /// 最終更新者
        /// </summary>
        public string UPD_USER_NAME_DISPLAY
        {
            get
            {
                if (!string.IsNullOrEmpty(REG_USER_CD))
                {
                    return REG_USER_CD;
                }
                if (!string.IsNullOrEmpty(UPD_USER_CD))
                {
                    return UPD_USER_CD;
                }
                else if (!string.IsNullOrEmpty(REG_USER_CD))
                {
                    return REG_USER_CD;
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        //public string UPD_DATE_DISPLAY { get; set; }
        /// <summary>
        /// 最終更新日
        /// </summary>
        public string UPD_DATE_DISPLAY
        {
            get
            {
                if (DEL_DATE.HasValue)
                {
                    return DEL_DATE.Value.ToShortDateString();
                }
                if (UPD_DATE.HasValue)
                {
                    return UPD_DATE.Value.ToShortDateString();
                }
                else if (REG_DATE.HasValue)
                {
                    return REG_DATE.Value.ToShortDateString();
                }
                else
                {
                    return string.Empty;
                }
            }
            set
            {

            }
        }

        ///// <summary>
        ///// 削除されているか
        ///// </summary>
        public bool IsShowDeleted
        {
            get
            {
                if (DEL_FLAG == (int)ZyCoaG.DEL_FLAG.Deleted) return true;
                return false;
            }
        }

        //// 20180723 FJ)Shinagawa Add Start ->
        ///// <summary>
        ///// 保存中（新規）状態か
        ///// </summary>
        public bool IsShowTemporaryRegister
        {
            get
            {
                if (DEL_FLAG == (int)ZyCoaG.DEL_FLAG.Temporary && REF_CHEM_CD == "") return true;
                return false;
            }
        }

        ///// <summary>
        ///// 保存中（更新）状態か
        ///// </summary>
        public bool IsShowTemporaryUpdate
        {
            get
            {
                if (DEL_FLAG == (int)ZyCoaG.DEL_FLAG.Temporary && REF_CHEM_CD != "") return true;
                return false;
            }
        }
        // 20180723 FJ)Shinagawa Add End <-

        /// <summary>
        /// 化学物質コードが読取のみか
        /// </summary>
        public bool IsChemCodeReadOnly
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Copy:
                        return false;
                    default:
                        return true;
                }
            }
        }

        /// <summary>
        /// 項目が読取のみか
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                    case Mode.TemporaryNew:  // 20180723 FJ)Shinagawa Add
                    case Mode.TemporaryEdit: // 20180723 FJ)Shinagawa Add
                        return false;
                    default:
                        return true;
                }
            }
        }

        /// <summary>
        /// ボタンを表示または有効にするか
        /// </summary>
        public bool IsButtonVisibleOrEnabled
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                    case Mode.TemporaryNew:  // 20180723 FJ)Shinagawa Add
                    case Mode.TemporaryEdit: // 20180723 FJ)Shinagawa Add
                        return true;
                    default:
                        return false;
                }
            }
        }
        public string REG_NO { get; set; }

        //2019/04/02 TPE.Rin Add Start
        /// <summary>
        /// 法規制名略称
        /// </summary>
        public string REG_ACRONYM { get; set; }
        /// <summary>
        /// GHS分類略称
        /// </summary>
        public string GHS_ACRONYM { get; set; }
        /// <summary>
        /// 消防法略称
        /// </summary>
        public string FIRE_ACRONYM { get; set; }
        /// <summary>
        /// 社内ルール略称
        /// </summary>
        public string OFFICE_ACRONYM { get; set; }
        /// <summary>
        /// 特殊健康診断
        /// </summary>
        public string EXAMINATION { get; set; }
        /// <summary>
        /// 作業環境測定
        /// </summary>
        public string MEASUREMENT { get; set; }
        //2019/04/02 TPE.Rin Add End

        //2019/04/26 TPE.Rin Add Start
        /// <summary>
        /// 消防法
        /// </summary>
        public string FIRECAT_NAME { get; set; }

        /// <summary>
        /// 社内ルール
        /// </summary>
        public string OFFICECAT_NAME { get; set; }
        //2019/04/26 TPE.Rin Add End

    }

    public class ChemSearchModel : ChemModel
    {

        public IQueryable<ChemSearchModel> MyProperty1 { get; set; }
        public int USAGE_STATUS { get; set; } = (int)Classes.UsageStatusCondtionType.Using;
        public int IncludesContained { get; set; } = (int)ChemSearchContained.Without;
        public int? MaxSearchResult { get; set; }

        public string hiddenbusyCheckid { get; set; }

        public int? hdnmode { get; set; }

        // public IEnumerable<SelectListItem> ddlAdminFlag { get; set; }
        // public IEnumerable<SelectListItem> ddlFlag { get; set; }

        public string FIGURE1 { get; set; }


        //public int? FIGURE { get; set; }
        private int _SearchMode { get; set; }

        //public ContainedSearchMode SearchMode
        //{
        //    get
        //    {
        //        ContainedSearchMode resultMode;
        //        if (ContainedSearchMode.TryParse(_SearchMode.ToString(), out resultMode)) return resultMode;
        //        return default(ContainedSearchMode);
        //    }
        //    set
        //    {
        //        _SearchMode = (int)value;
        //    }
        //}

        /// <summary>
        /// 部署コード
        /// </summary>
        public string BusyoCheck { get; set; }

        public ChemSearchModel()
        {
            SearchProductList = new List<ChemModel>();
        }
        public ChemSearchModel(ChemModel val)
        {
            CHEM_CD = val.CHEM_CD;
        }

        public int CHEM_SEARCH_TARGET { get; set; }

        public int CHEM_CD_CONDITION { get; set; }

        public int CAS_NO_CONDITION { get; set; }

        public int CHEM_NAME_CONDITION { get; set; }

        public int MAKER_NAME_CONDITION { get; set; }

        //2019/02/06 Rin Add Start
        public int PRODUCT_NAME_CONDITION { get; set; }

        //2019/02/06 Rin Add END

        ////2019/04/02 TPE.Rin Add Start
        public int Examination_FLAG { get; set; } = (int)ExaminationList.All;

        public int Measurement_FLAG { get; set; } = (int)MeasurementList.All;
        ////2019/04/02 TPE.Rin Add End

        public IEnumerable<ChemModel> SearchProductList { get; set; }

        public bool IsOnlyProduct { get; set; }

        public bool IsScanned { get; set; } = false;

        public bool IsAdmin { get; set; } = false;

        public bool IsAdminScanned
        {
            get
            {
                return IsAdmin && IsScanned ? true : false;
            }
        }

        public bool IsProductScanned
        {
            get
            {
                return IsScanned && CHEM_SEARCH_TARGET == (int)ChemSearchTarget.Product && USAGE_STATUS == (int)Classes.UsageStatusCondtionType.Using ? true : false;
            }
        }

        public bool IsAccepted { get; set; } = false;

        public string REG_SELECTION { get; set; }

        [Display(Name = "法規制")]
        public IEnumerable<int> REG_COLLECTION
        {
            get
            {
                if (REG_SELECTION == null) yield break;
                foreach (var s in REG_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string RegulationSelectionStyle
        {
            get
            {
                return REG_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string RegulationSelectionText
        {
            get
            {
                return REG_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
            set
            {

            }
        }

        public string RegulationClickEvent { get; set; }

        //2019/04/01 TPE.Rin Add Start
        public string FIRE_SELECTION { get; set; }

        [Display(Name = "消防法")]
        public IEnumerable<int> FIRE_COLLECTION
        {
            get
            {
                if (FIRE_SELECTION == null) yield break;
                foreach (var s in FIRE_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string FireSelectionStyle
        {
            get
            {
                return FIRE_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string FireSelectionText
        {
            get
            {
                return FIRE_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string OFFICE_SELECTION { get; set; }

        [Display(Name = "社内ルール")]
        public IEnumerable<int> OFFICE_COLLECTION
        {
            get
            {
                if (OFFICE_SELECTION == null) yield break;
                foreach (var s in OFFICE_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string OfficeSelectionStyle
        {
            get
            {
                return OFFICE_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string OfficeSelectionText
        {
            get
            {
                return OFFICE_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string GAS_SELECTION { get; set; }

        [Display(Name = "GHS分類")]
        public IEnumerable<int> GAS_COLLECTION
        {
            get
            {
                if (GAS_SELECTION == null) yield break;
                foreach (var s in GAS_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string GaseSelectionStyle
        {
            get
            {
                return GAS_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string GasSelectionText
        {
            get
            {
                return GAS_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        //2019/04/01 TPE.Rin Add End

        //2019/02/09 TPE.Rin Add Start
        public string GasClickEvent { get; set; }
        public string FireClickEvent { get; set; }
        public string OfficeClickEvent { get; set; }
        //2019/02/09 TPE.Rin Add End

        public string REFERENCE_AUTHORITY { get; set; }

        public string LOGIN_USER_CD { get; set; }

        //2019/04/01 TPE.Rin Add Start
        //public string EXAMINATION_FLAG { get; set; }

        //public string MEASUREMENT_FLAG { get; set; }
        //2019/04/01 TPE.Rin Add End
    }

    public class ChemDataInfo : DataInfoBase<ChemModel>
    {
        protected override string TableName { get; } = nameof(M_CHEM);

        /// <summary>
        /// 鍵管理辞書
        /// </summary>
        private IDictionary<int?, string> keyManagementDic { get; set; }

        /// <summary>
        /// 重量管理辞書
        /// </summary>
        private IDictionary<int?, string> weightManagementDic { get; set; }

        /// <summary>
        /// 形状辞書
        /// </summary>
        private IDictionary<int?, string> figureDic { get; set; }

        /// <summary>
        /// メーカー辞書
        /// </summary>
        private IDictionary<int?, string> makerDic { get; set; }

        /// <summary>
        /// メーカー選択リスト
        /// </summary>
        private IEnumerable<CommonDataModel<int?>> makerSelectList { get; set; }

        /// <summary>
        /// 単位辞書
        /// </summary>
        private IDictionary<int?, string> unitsizeDic { get; set; }

        /// <summary>
        /// 単位選択リスト
        /// </summary>
        private IEnumerable<CommonDataModel<int?>> unitsizeSelectList { get; set; }

        /// <summary>
        /// ユーザー辞書
        /// </summary>
        private IDictionary<string, string> userDic { get; set; }

        /// <summary>
        /// 法規制辞書
        /// </summary>
        private IDictionary<int?, string> regulationDic { get; set; }

        /// <summary>
        /// 状態辞書
        /// </summary>
        private IDictionary<int?, string> usageStatusDic { get; set; }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (ChemSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            sql.AppendLine("from");
            sql.AppendLine(" M_CHEM");
            var chemSearchTargetName = string.Empty;
            if (condition.CHEM_SEARCH_TARGET == (int)ChemSearchTarget.Product)
            {
                if (condition.IsOnlyProduct)
                {
                    sql.AppendLine(" inner join");
                }
                else
                {
                    sql.AppendLine(" left outer join");
                }
                sql.AppendLine(" M_CHEM_PRODUCT");
                chemSearchTargetName = "商品";
            }
            else
            {
                sql.AppendLine(" left outer join (select * from M_CHEM_PRODUCT");

                if (condition.CHEM_SEARCH_TARGET == (int)ChemSearchTarget.Chem)
                {
                    sql.AppendLine("  where M_CHEM_PRODUCT.CHEM_CD is null");
                }

                sql.AppendLine(" ) M_CHEM_PRODUCT");
                chemSearchTargetName = "化学物質";
            }
            // MakeConditionInLineOfCsv("検索対象", chemSearchTargetName);
            //2019/05/28 TPE.Rin Add
            //ZC11010.chemSearchFlg = "OFF";//                                  NO USE

            sql.AppendLine(" on M_CHEM.CHEM_CD = M_CHEM_PRODUCT.CHEM_CD left outer join V_CHEM_REGULATION on M_CHEM.CHEM_CD = V_CHEM_REGULATION.CHEM_CD");

            //2019/05/29 TPE.Rin Del Start
            //2019/05/15 TPE.rin Add Start
            //特殊健康診断
            var examinationFLAGInPharse = string.Empty;
            List<int> examinationstatus = new List<int>();
            switch (condition.Examination_FLAG)
            {
                case (int)ExaminationList.Higaitou:
                    sql.AppendLine("   and V_CHEM_REGULATION.EXAMINATION like ('%0%')");
                    break;
                case (int)ExaminationList.Gaitou:
                    sql.AppendLine("   and V_CHEM_REGULATION.EXAMINATION like ('%1%')");
                    break;
                case (int)ExaminationList.All:
                    sql.AppendLine("   and( V_CHEM_REGULATION.EXAMINATION like ('%0%') or V_CHEM_REGULATION.EXAMINATION like ('%1%'))");
                    break;
            }

            //作業環境測定
            var measurementFLAGInPharse = string.Empty;
            List<int> measurement = new List<int>();
            switch (condition.Measurement_FLAG)
            {
                case (int)MeasurementList.Higaitou:
                    sql.AppendLine("   and V_CHEM_REGULATION.MEASUREMENT like ('%0%')");
                    break;
                case (int)MeasurementList.Gaitou:
                    sql.AppendLine("   and V_CHEM_REGULATION.MEASUREMENT like ('%1%')");
                    break;
                case (int)MeasurementList.All:
                    sql.AppendLine("   and( V_CHEM_REGULATION.MEASUREMENT like ('%0%') or V_CHEM_REGULATION.MEASUREMENT like ('%1%'))");
                    break;
            }
            //2019/05/15 TPE.rin Add End
            //2019/05/29 TPE.Rin Del End

            sql.AppendLine(" left outer join V_CHEM_GHSCAT on M_CHEM.CHEM_CD = V_CHEM_GHSCAT.CHEM_CD left outer join V_CHEM_FIRE on M_CHEM.CHEM_CD = V_CHEM_FIRE.CHEM_CD ");
            sql.AppendLine(" left outer join V_CHEM_OFFICE on M_CHEM.CHEM_CD = V_CHEM_OFFICE.CHEM_CD left outer join M_MAKER on M_CHEM_PRODUCT.MAKER_ID = M_MAKER.MAKER_ID ");
            return sql.ToString();
        }

        //removed hastable parameter
        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters param)
        {
            var condition = (ChemSearchModel)dynamicCondition;
            //parameters = new Hashtable();
            param = new DynamicParameters();
            StringBuilder sql = new StringBuilder();
            Csv = string.Empty;

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" M_CHEM.CHEM_CD = M_CHEM.CHEM_CD");

            //2019/04/01 TPE.Rin Add Start
            //分類
            if (!string.IsNullOrWhiteSpace(condition.CHEM_CAT))  //added to string();
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.CHEM_CAT = @CHEM_CAT");

                //2019/05/23 TPE.Rin Mod Start
                if (condition.CHEM_CAT.ToString() == "1")
                {
                    param.Add(nameof(condition.CHEM_CAT), "単体");
                }
                else if (condition.CHEM_CAT.ToString() == "2")
                {
                    param.Add(nameof(condition.CHEM_CAT), "混合物");
                }
                else
                {
                    param.Add(nameof(condition.CHEM_CAT), condition.CHEM_CAT);
                }
                //2019/05/23 TPE.Rin Mod End
            }           

            //検索対処：商品
            if (!string.IsNullOrWhiteSpace(condition.PRODUCT_NAME))
            {
                condition.PRODUCT_NAME = condition.PRODUCT_NAME.Trim();
                sql.AppendLine(" and");
                //2019/06/25 TPE.Rin Mod Start
                if (condition.CHEM_SEARCH_TARGET == (int)ChemSearchTarget.Product)
                {
                    sql.AppendLine(" M_CHEM.CHEM_CD + CONVERT(VARCHAR,M_CHEM_PRODUCT.PRODUCT_SEQ) in");

                }
                else
                {
                    sql.AppendLine(" M_CHEM.CHEM_CD in");
                }
                //2019/06/25 TPE.Rin Mod End
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                //2019/06/25 TPE.Rin Mod Start
                if (condition.CHEM_SEARCH_TARGET == (int)ChemSearchTarget.Product)
                {
                    sql.AppendLine("   M_CHEM_PRODUCT.CHEM_CD + CONVERT(VARCHAR,M_CHEM_PRODUCT.PRODUCT_SEQ)");
                }
                else
                {
                    sql.AppendLine("   distinct (M_CHEM_PRODUCT.CHEM_CD)");
                }
                //2019/06/25 TPE.Rin Mod End
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_PRODUCT");
                sql.AppendLine("  where");
                switch ((SearchConditionType)condition.PRODUCT_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("   upper(M_CHEM_PRODUCT.PRODUCT_NAME) like '%' + @PRODUCT_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("   upper(M_CHEM_PRODUCT.PRODUCT_NAME) like @PRODUCT_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("   upper(M_CHEM_PRODUCT.PRODUCT_NAME) like '%' + @PRODUCT_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("   upper(M_CHEM_PRODUCT.PRODUCT_NAME) = @PRODUCT_NAME");
                        break;
                }
                sql.AppendLine(" )");
                if ((SearchConditionType)condition.PRODUCT_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    param.Add(nameof(condition.PRODUCT_NAME), condition.PRODUCT_NAME.ToUpper());
                }
                else
                {
                    param.Add(nameof(condition.PRODUCT_NAME), DbUtil.ConvertIntoEscapeChar(condition.PRODUCT_NAME.ToUpper()));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.PRODUCT_NAME)), condition.PRODUCT_NAME_CONDITION, condition.PRODUCT_NAME);
            }

            //検索対処：化学物質
            if (!string.IsNullOrWhiteSpace(condition.CHEM_NAME))
            {
                condition.CHEM_NAME = condition.CHEM_NAME.Trim();
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.CHEM_CD in");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_ALIAS.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_ALIAS");
                sql.AppendLine("  where");
                switch ((SearchConditionType)condition.CHEM_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) = @CHEM_NAME");
                        break;
                }
                sql.AppendLine(" )");
                if ((SearchConditionType)condition.CHEM_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    param.Add(nameof(condition.CHEM_NAME), condition.CHEM_NAME.ToUpper());
                }
                else
                {
                    param.Add(nameof(condition.CHEM_NAME), DbUtil.ConvertIntoEscapeChar(condition.CHEM_NAME.ToUpper()));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_NAME)), condition.CHEM_NAME_CONDITION, condition.CHEM_NAME);
            }

            if (!string.IsNullOrWhiteSpace(condition.MAKER_NAME))
            {
                sql.AppendLine(" and");
                switch ((SearchConditionType)condition.MAKER_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" M_MAKER.MAKER_NAME like '%' + @MAKER_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" M_MAKER.MAKER_NAME like @MAKER_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" M_MAKER.MAKER_NAME like '%' + @MAKER_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" M_MAKER.MAKER_NAME = @MAKER_NAME");
                        break;
                }
                if ((SearchConditionType)condition.MAKER_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    param.Add(nameof(condition.MAKER_NAME), condition.MAKER_NAME);
                }
                else
                {
                    param.Add(nameof(condition.MAKER_NAME), DbUtil.ConvertIntoEscapeChar(condition.MAKER_NAME));
                }

               MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.MAKER_NAME)), condition.MAKER_NAME_CONDITION, condition.MAKER_NAME);
            }

            if (!string.IsNullOrWhiteSpace(condition.CHEM_CD))
            {
                condition.CHEM_CD = condition.CHEM_CD.Trim();
                sql.AppendLine(" and");
                switch ((SearchConditionType)condition.CHEM_CD_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" M_CHEM.CHEM_CD like '%' + @CHEM_CD + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" M_CHEM.CHEM_CD like @CHEM_CD + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" M_CHEM.CHEM_CD like '%' + @CHEM_CD " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" M_CHEM.CHEM_CD = @CHEM_CD");
                        break;
                }
                if ((SearchConditionType)condition.CHEM_CD_CONDITION == SearchConditionType.PerfectMatching)
                {
                    param.Add(nameof(condition.CHEM_CD), condition.CHEM_CD);
                }
                else
                {
                    param.Add(nameof(condition.CHEM_CD), DbUtil.ConvertIntoEscapeChar(condition.CHEM_CD));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_CD)), condition.CHEM_CD_CONDITION, condition.CHEM_CD);
            }

            //CAS No
            if (!string.IsNullOrEmpty(condition.CAS_NO))
            {
                condition.CAS_NO = condition.CAS_NO.Trim();
                sql.AppendLine(" and");
                switch ((SearchConditionType)condition.CAS_NO_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO like '%' + @CAS_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO like @CAS_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO like '%' + @CAS_NO " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO = @CAS_NO");
                        break;
                }
                if ((SearchConditionType)condition.CAS_NO_CONDITION == SearchConditionType.PerfectMatching)
                {
                    param.Add(nameof(condition.CAS_NO), condition.CAS_NO);
                }
                else
                {
                    param.Add(nameof(condition.CAS_NO), DbUtil.ConvertIntoEscapeChar(condition.CAS_NO));
                }
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CAS_NO)), condition.CAS_NO_CONDITION, condition.CAS_NO);
            }

            //2019/04/01 TPE.Rin Add Start
            //形状
            if (!string.IsNullOrEmpty(condition.FIGURE.ToString()))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.FIGURE = @FIGURE");
                param.Add(nameof(condition.FIGURE), condition.FIGURE);
            }

            //鍵管理
            if (!string.IsNullOrEmpty(condition.KEY_MGMT.ToString()))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.KEY_MGMT = @KEY_MGMT");
                param.Add(nameof(condition.KEY_MGMT), condition.KEY_MGMT);
            }

            //重量管理
            if (!string.IsNullOrEmpty(condition.WEIGHT_MGMT.ToString()))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.WEIGHT_MGMT = @WEIGHT_MGMT");
                param.Add(nameof(condition.WEIGHT_MGMT), condition.WEIGHT_MGMT);
            }
            //2019/04/01 TPE.Rin Add End



            var regulationBasedInPharse = string.Empty;
            if (condition.REG_COLLECTION.Count() > 0)
            {
                regulationBasedInPharse = DbUtil.CreateInOperator(condition.REG_COLLECTION.ToList(), nameof(M_REGULATION.REG_TYPE_ID), param);
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.CHEM_CD in ");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_REGULATION.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_REGULATION");
                sql.AppendLine("  where");
                sql.AppendLine("   M_CHEM_REGULATION.REG_TYPE_ID in (" + regulationBasedInPharse + ")");
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_COLLECTION)),
                                         condition.REG_COLLECTION.Where(x => x != NikonConst.GhsCategoryTypeId &&
                                                                             x != NikonConst.RegulationTypeId), regulationDic);
            }

            //2019/04/01 TPE.Rin Add Start
            var fireBasedInPharse = string.Empty;
            if (condition.FIRE_COLLECTION.Count() > 0)
            {
                fireBasedInPharse = DbUtil.CreateInOperator(condition.FIRE_COLLECTION.ToList(), nameof(M_REGULATION.REG_TYPE_ID), param);
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.CHEM_CD in ");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_REGULATION.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_REGULATION");
                sql.AppendLine("  where");
                sql.AppendLine("   M_CHEM_REGULATION.REG_TYPE_ID in (" + fireBasedInPharse + ")");
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.FIRE_COLLECTION)),
                                         condition.FIRE_COLLECTION.Where(x => x != NikonConst.GhsCategoryTypeId &&
                                                                             x != NikonConst.RegulationTypeId), regulationDic);
            }

            var officeBasedInPharse = string.Empty;
            if (condition.OFFICE_COLLECTION.Count() > 0)
            {
                officeBasedInPharse = DbUtil.CreateInOperator(condition.OFFICE_COLLECTION.ToList(), nameof(M_REGULATION.REG_TYPE_ID), param);
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.CHEM_CD in ");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_REGULATION.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_REGULATION");
                sql.AppendLine("  where");
                sql.AppendLine("   M_CHEM_REGULATION.REG_TYPE_ID in (" + officeBasedInPharse + ")");
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.OFFICE_COLLECTION)),
                                         condition.OFFICE_COLLECTION.Where(x => x != NikonConst.GhsCategoryTypeId &&
                                                                             x != NikonConst.RegulationTypeId), regulationDic);
            }

            var gasBasedInPharse = string.Empty;
            if (condition.GAS_COLLECTION.Count() > 0)
            {
                gasBasedInPharse = DbUtil.CreateInOperator(condition.GAS_COLLECTION.ToList(), nameof(M_REGULATION.REG_TYPE_ID), param);
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM.CHEM_CD in ");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_REGULATION.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_REGULATION");
                sql.AppendLine("  where");
                sql.AppendLine("   M_CHEM_REGULATION.REG_TYPE_ID in (" + gasBasedInPharse + ")");
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.GAS_COLLECTION)),
                                         condition.GAS_COLLECTION.Where(x => x != NikonConst.GhsCategoryTypeId &&
                                                                             x != NikonConst.RegulationTypeId), regulationDic);
            }

            //2019/04/01 TPE.Rin Add End

            if (!string.IsNullOrWhiteSpace(condition.PRODUCT_BARCODE))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_BARCODE = @PRODUCT_BARCODE");
                param.Add(nameof(condition.PRODUCT_BARCODE), condition.PRODUCT_BARCODE);

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.PRODUCT_BARCODE)), condition.PRODUCT_BARCODE);
            }

            // 含有物を検索します。
            var includesContainedDic = new IncludesContainedListMasterInfo().GetDictionary();
            if (condition.IncludesContained == (int)ChemSearchContained.With)
            {
                if (!string.IsNullOrWhiteSpace(condition.CHEM_NAME) || !string.IsNullOrWhiteSpace(condition.MAKER_NAME) ||
                    !string.IsNullOrWhiteSpace(condition.CHEM_CD) || !string.IsNullOrWhiteSpace(condition.CAS_NO) ||
                    condition.REG_COLLECTION.Count() > 0)
                {
                    sql.AppendLine(" or");
                    sql.AppendLine(" M_CHEM.CHEM_CD in");
                    sql.AppendLine(" (");
                    sql.AppendLine("  select");
                    sql.AppendLine("   distinct(M_CONTAINED.CHEM_CD)");
                    sql.AppendLine("  from");
                    sql.AppendLine("   M_CONTAINED");
                    sql.AppendLine("   inner join");
                    sql.AppendLine("   M_CHEM");
                    sql.AppendLine("   on");
                    sql.AppendLine("   M_CONTAINED.C_CHEM_CD = M_CHEM.CHEM_CD");
                    sql.AppendLine("   left outer join");
                    sql.AppendLine("   M_CHEM_ALIAS");
                    sql.AppendLine("   on");
                    sql.AppendLine("   M_CHEM.CHEM_CD = M_CHEM_ALIAS.CHEM_CD");
                    if (condition.CHEM_SEARCH_TARGET == (int)ChemSearchTarget.Product)
                    {
                        if (condition.IsOnlyProduct)
                        {
                            sql.AppendLine("   inner join");
                        }
                        else
                        {
                            sql.AppendLine("   left outer join");
                        }
                        sql.AppendLine("   M_CHEM_PRODUCT");
                    }
                    else
                    {
                        sql.AppendLine("   left outer join");
                        sql.AppendLine("   (");
                        sql.AppendLine("    select");
                        sql.AppendLine("     *");
                        sql.AppendLine("    from");
                        sql.AppendLine("     M_CHEM_PRODUCT");
                        sql.AppendLine("    where");
                        sql.AppendLine("     M_CHEM_PRODUCT.CHEM_CD is null");
                        sql.AppendLine("   ) M_CHEM_PRODUCT");
                    }
                    sql.AppendLine("   on");
                    sql.AppendLine("   M_CHEM.CHEM_CD = M_CHEM_PRODUCT.CHEM_CD");
                    sql.AppendLine("   left outer join");
                    sql.AppendLine("   M_MAKER");
                    sql.AppendLine("   on");
                    sql.AppendLine("   M_CHEM_PRODUCT.MAKER_ID = M_MAKER.MAKER_ID");                  

                    sql.AppendLine("  where");
                    sql.AppendLine("   M_CHEM.CHEM_CD = M_CHEM.CHEM_CD");

                    if (!string.IsNullOrWhiteSpace(condition.CHEM_NAME))
                    {
                        sql.AppendLine("   and");
                        switch ((SearchConditionType)condition.CHEM_NAME_CONDITION)
                        {
                            case SearchConditionType.PartialMatching:
                                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.ForwardMatching:
                                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.BackwardMatching:
                                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.PerfectMatching:
                                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) = @CHEM_NAME");
                                break;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(condition.MAKER_NAME))
                    {
                        sql.AppendLine("   and");
                        switch ((SearchConditionType)condition.MAKER_NAME_CONDITION)
                        {
                            case SearchConditionType.PartialMatching:
                                sql.AppendLine("   M_MAKER.MAKER_NAME like '%' + @MAKER_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.ForwardMatching:
                                sql.AppendLine("   M_MAKER.MAKER_NAME like @MAKER_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.BackwardMatching:
                                sql.AppendLine("   M_MAKER.MAKER_NAME like '%' + @MAKER_NAME " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.PerfectMatching:
                                sql.AppendLine("   M_MAKER.MAKER_NAME = @MAKER_NAME");
                                break;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(condition.CHEM_CD))
                    {
                        sql.AppendLine("   and");
                        switch ((SearchConditionType)condition.CHEM_CD_CONDITION)
                        {
                            case SearchConditionType.PartialMatching:
                                sql.AppendLine("   M_CHEM.CHEM_CD like '%' + @CHEM_CD + '%' " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.ForwardMatching:
                                sql.AppendLine("   M_CHEM.CHEM_CD like @CHEM_CD + '%' " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.BackwardMatching:
                                sql.AppendLine("   M_CHEM.CHEM_CD like '%' + @CHEM_CD " + DbUtil.LikeEscapeInfoAdd());
                                break;
                            case SearchConditionType.PerfectMatching:
                                sql.AppendLine("   M_CHEM.CHEM_CD = @CHEM_CD");
                                break;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(condition.CAS_NO))
                    {
                        sql.AppendLine("   and");
                        sql.AppendLine("   M_CHEM.CAS_NO = @CAS_NO");
                    }

                    if (condition.REG_COLLECTION.Count() > 0)
                    {
                        sql.AppendLine("   and");
                        sql.AppendLine("   M_CHEM.CHEM_CD in ");
                        sql.AppendLine("   (");
                        sql.AppendLine("    select");
                        sql.AppendLine("     distinct(M_CHEM_REGULATION.CHEM_CD)");
                        sql.AppendLine("    from");
                        sql.AppendLine("     M_CHEM_REGULATION");
                        sql.AppendLine("    where");
                        sql.AppendLine("     M_CHEM_REGULATION.REG_TYPE_ID in (" + regulationBasedInPharse + ")");
                        sql.AppendLine("   )");
                    }

                    sql.AppendLine(" )");
                }

            }
            MakeConditionInLineOfCsv("含有物", includesContainedDic[condition.IncludesContained]);

            sql.AppendLine(")");

            switch (condition.USAGE_STATUS)
            {
                case (int)Classes.UsageStatusCondtionType.Using:
                    sql.AppendLine(" and");
                    sql.AppendLine(" M_CHEM.DEL_FLAG = @DEL_FLAG");
                    param.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Undelete);
                    break;
                case (int)Classes.UsageStatusCondtionType.Deleted:
                    sql.AppendLine(" and");
                    sql.AppendLine(" M_CHEM.DEL_FLAG = @DEL_FLAG");
                    param.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Deleted);
                    var dic = new IncludesContainedListMasterInfo().GetDictionary();
                    break;
                // 20180723 FJ)Shinagawa Add Start ->
                case (int)Classes.UsageStatusCondtionType.Temporary:
                    sql.AppendLine(" and");
                    sql.AppendLine(" M_CHEM.DEL_FLAG = @DEL_FLAG");
                    param.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Temporary);
                    break;
                    // 20180723 FJ)Shinagawa Add End <-
            }
            MakeConditionInLineOfCsv("状態", usageStatusDic[condition.USAGE_STATUS]);

            var examinationFLAGInPharse = string.Empty;
            List<int> examinationstatus = new List<int>();
            switch (condition.Examination_FLAG)
            {
                case (int)ExaminationList.Higaitou:
                    //sql.AppendLine("   and isnull(V_CHEM_REGULATION.EXAMINATION,'0') like ('%0%')");
                    sql.AppendLine("   and isnull(V_CHEM_REGULATION.EXAMINATION,'0') = '0'");
                    break;
                case (int)ExaminationList.Gaitou:
                    //sql.AppendLine("   and isnull(V_CHEM_REGULATION.EXAMINATION,'0') like ('%1%')");
                    sql.AppendLine("   and isnull(V_CHEM_REGULATION.EXAMINATION,'0') = '1'");
                    break;
                case (int)ExaminationList.All:
                    //sql.AppendLine("   and( isnull(V_CHEM_REGULATION.EXAMINATION,'0') like ('%0%') or isnull(V_CHEM_REGULATION.EXAMINATION,'0') like ('%1%'))");
                    sql.AppendLine("   and( isnull(V_CHEM_REGULATION.EXAMINATION,'0') = '0' or isnull(V_CHEM_REGULATION.EXAMINATION,'0') = '1')");
                    break;
            }

            //作業環境測定
            var measurementFLAGInPharse = string.Empty;
            List<int> measurement = new List<int>();
            switch (condition.Measurement_FLAG)
            {
                case (int)MeasurementList.Higaitou:
                    //sql.AppendLine("   and isnull(V_CHEM_REGULATION.MEASUREMENT,'0') like ('%0%')");
                    sql.AppendLine("   and isnull(V_CHEM_REGULATION.MEASUREMENT,'0') = '0'");
                    break;
                case (int)MeasurementList.Gaitou:
                    //sql.AppendLine("   and isnull(V_CHEM_REGULATION.MEASUREMENT,'0') like ('%1%')");
                    sql.AppendLine("   and isnull(V_CHEM_REGULATION.MEASUREMENT,'0') = '1'");
                    break;
                case (int)MeasurementList.All:
                    //sql.AppendLine("   and( isnull(V_CHEM_REGULATION.MEASUREMENT,'0') like ('%0%') or isnull(V_CHEM_REGULATION.MEASUREMENT,'0') like ('%1%'))");
                    sql.AppendLine("   and( isnull(V_CHEM_REGULATION.MEASUREMENT,'0') = '0' or isnull(V_CHEM_REGULATION.MEASUREMENT,'0') = '1')");
                    break;
            }

            return sql.ToString();
        }




        public List<CsmsCommonInfo> GetSearchResult(ChemSearchModel condition)
        {
            StringBuilder sql = new StringBuilder();
            DynamicParameters param = null;

            sql.AppendLine("select");
            //if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            //{
            //    sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            //}
            sql.AppendLine(" M_CHEM.CHEM_CD,M_CHEM.CHEM_NAME,M_CHEM.CHEM_CAT,M_CHEM.DENSITY,M_CHEM.FLASH_POINT,M_CHEM.EXPIRATION_DATE,M_CHEM.KEY_MGMT,M_CHEM.WEIGHT_MGMT,M_CHEM.FIGURE,");
            sql.AppendLine(" M_CHEM.CAS_NO,M_CHEM.MEMO,M_CHEM.OPTIONAL1, M_CHEM.OPTIONAL2,M_CHEM.OPTIONAL3,M_CHEM.OPTIONAL4, M_CHEM.OPTIONAL5,M_CHEM.REG_USER_CD,M_CHEM.UPD_USER_CD,M_CHEM.DEL_USER_CD,");
            sql.AppendLine(" M_CHEM.REG_DATE,M_CHEM.UPD_DATE,M_CHEM.DEL_FLAG,M_CHEM.DEL_DATE,M_CHEM.SDS,M_CHEM.SDS_MIMETYPE,M_CHEM.SDS_FILENAME,M_CHEM.SDS_URL AS SDS_URL2,M_CHEM.SDS_UPD_DATE SDS_UPD_DATE2,M_CHEM_PRODUCT.UNITSIZE_ID,M_CHEM_PRODUCT.MAKER_ID, ");
            sql.AppendLine(" V_CHEM_GHSCAT.GHS_ICON_PATH,V_CHEM_REGULATION.REG_ACRONYM,V_CHEM_FIRE.FIRE_ACRONYM,V_CHEM_OFFICE.OFFICE_ACRONYM,");
            sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_SEQ,M_CHEM_PRODUCT.PRODUCT_NAME,M_CHEM_PRODUCT.UNITSIZE,M_CHEM_PRODUCT.PRODUCT_BARCODE,");   
            sql.AppendLine(" M_CHEM_PRODUCT.SDS_URL,M_CHEM_PRODUCT.SDS_UPD_DATE,M_CHEM.REF_CHEM_CD,M_CHEM.FIRE_FLAG,");
            sql.AppendLine("(CASE WHEN V_CHEM_REGULATION.EXAMINATION=NULL THEN '非該当' WHEN V_CHEM_REGULATION.EXAMINATION='1' THEN '該当' ELSE '非該当' END) AS EXAMINATION,");
            sql.AppendLine("(CASE WHEN V_CHEM_REGULATION.MEASUREMENT = NULL THEN '非該当' WHEN V_CHEM_REGULATION.MEASUREMENT = '1' THEN '該当' ELSE '非該当' END) AS MEASUREMENT,");

            //REGULATION
            sql.AppendLine("STUFF((select ','+CONVERT(VARCHAR,M_REGULATION.REG_ACRONYM_TEXT) from M_CHEM_REGULATION,M_REGULATION where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD ");
            sql.AppendLine("AND M_CHEM_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID AND M_CHEM_REGULATION.CLASS_ID = 1  order by M_CHEM_REGULATION.CHEM_CD FOR XML PATH('')), 1, 1, '') AS 'REGULATION_Text',");

            //REGULATION_Division
            sql.AppendLine(" STUFF((select ','+CONVERT(VARCHAR,IIF(M_CHEM_REGULATION.INPUT_FLAG = 0,'自動入力','手入力')) from M_CHEM_REGULATION where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD AND M_CHEM_REGULATION.CLASS_ID ");
            sql.AppendLine("= 1  order by M_CHEM_REGULATION.CHEM_CD FOR XML PATH('')), 1, 1, '') AS 'REGULATION_Division_Text',");

            //Fire
            sql.AppendLine("STUFF((select ','+CONVERT(VARCHAR,M_REGULATION.REG_ACRONYM_TEXT) from M_CHEM_REGULATION,M_REGULATION");
            sql.AppendLine("where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD AND M_CHEM_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID AND M_CHEM_REGULATION.CLASS_ID = 3 ");
            sql.AppendLine("order by M_CHEM_REGULATION.CHEM_CD FOR XML PATH('')), 1, 1, '') AS 'Fire_Text',");

            //Fire_FLAG
            sql.AppendLine("STUFF((select ','+CONVERT(VARCHAR,IIF(M_CHEM_REGULATION.INPUT_FLAG = 0,'自動入力','手入力')) from M_CHEM_REGULATION where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD AND M_CHEM_REGULATION.");
            sql.AppendLine("CLASS_ID = 3  order by M_CHEM_REGULATION.CHEM_CD FOR XML PATH('')), 1, 1, '') AS 'Fire_Division', CONVERT(VARCHAR,IIF(M_CHEM.FIRE_FLAG = 0,'該当','非該当')) AS 'Fire_FLAG_Text',");

            //Office
            sql.AppendLine("STUFF((select ','+CONVERT(VARCHAR,M_REGULATION.REG_ACRONYM_TEXT) from M_CHEM_REGULATION,M_REGULATION where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD AND M_CHEM_REGULATION.REG_TYPE_ID =");
            sql.AppendLine("M_REGULATION.REG_TYPE_ID AND M_CHEM_REGULATION.CLASS_ID = 4  order by M_CHEM_REGULATION.CHEM_CD FOR XML PATH('')), 1, 1, '') AS 'Office_Text',");
            //Office_Division
            sql.AppendLine("STUFF((select ','+CONVERT(VARCHAR,IIF(M_CHEM_REGULATION.INPUT_FLAG = 0,'自動入力','手入力')) from M_CHEM_REGULATION where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
            sql.AppendLine("AND M_CHEM_REGULATION.CLASS_ID = 4  order by M_CHEM_REGULATION.CHEM_CD FOR XML PATH('')), 1, 1, '') AS 'Office_Division_Text',");
            //GHS_TEXT
            sql.AppendLine("STUFF((select ','+CONVERT(VARCHAR,M_REGULATION.REG_ACRONYM_TEXT) from M_CHEM_REGULATION,M_REGULATION where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD AND M_CHEM_REGULATION.REG_TYPE_ID =");
            sql.AppendLine("M_REGULATION.REG_TYPE_ID AND M_CHEM_REGULATION.CLASS_ID = 2  order by M_CHEM_REGULATION.CHEM_CD FOR XML PATH('')), 1, 1, '') AS 'GHS_TEXT',");
            //GHS_Division_Text
            sql.AppendLine("STUFF((select ','+CONVERT(VARCHAR,IIF(M_CHEM_REGULATION.INPUT_FLAG = 0,'自動入力','手入力')) from M_CHEM_REGULATION where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD AND M_CHEM_REGULATION.CLASS_ID = 2  order by M_CHEM_REGULATION.");
            sql.AppendLine("CHEM_CD FOR XML PATH('')), 1, 1, '') AS 'GHS_Division_Text',");



            sql.AppendLine("(CASE WHEN ISNULL((select M_MAKER.DEL_FLAG FROM M_MAKER WHERE MAKER_ID = M_CHEM_PRODUCT.MAKER_ID),0) = 0 THEN 0  ELSE 1 END) MAKER_NAME_DEL_FLAG , ");
            sql.AppendLine("(CASE  WHEN  ISNULL((select M_UNITSIZE.DEL_FLAG FROM M_UNITSIZE WHERE UNITSIZE_ID = M_CHEM_PRODUCT.UNITSIZE_ID),0) = 0 THEN 0 ELSE 1 END) AS UNITSIZE_NAME_DEL_FLAG, ");
            sql.AppendLine(" (select M_UNITSIZE.UNIT_NAME FROM M_UNITSIZE WHERE UNITSIZE_ID = M_CHEM_PRODUCT.UNITSIZE_ID) AS UNITSIZE_NAME,");
            sql.AppendLine("(SELECT DISPLAY_VALUE FROM M_COMMON WHERE TABLE_NAME = 'M_KEY_MGMT' AND KEY_VALUE = M_CHEM.KEY_MGMT) AS KEY_MGMT_NAME,");
            sql.AppendLine("(SELECT DISPLAY_VALUE FROM M_COMMON WHERE TABLE_NAME = 'M_FIGURE' AND KEY_VALUE = M_CHEM.FIGURE) AS FIGURE_NAME,");
            sql.AppendLine("(SELECT  MAKER_NAME FROM M_MAKER WHERE MAKER_ID = M_CHEM_PRODUCT.MAKER_ID) AS MAKER_NAME,");
            sql.AppendLine("(SELECT DISPLAY_VALUE FROM M_COMMON WHERE TABLE_NAME = 'M_WEIGHT_MGMT' AND KEY_VALUE = M_CHEM.WEIGHT_MGMT) AS WEIGHT_MGMT_NAME,");
            sql.AppendLine("(SELECT TOP 1 USER_NAME FROM M_USER WHERE USER_CD = M_CHEM.REG_USER_CD) AS REG_USER_CD,");
            sql.AppendLine("(SELECT TOP 1 USER_NAME FROM M_USER WHERE USER_CD = M_CHEM.UPD_USER_CD) AS UPD_USER_CD,");
            sql.AppendLine("(SELECT TOP 1 USER_NAME FROM M_USER WHERE USER_CD = M_CHEM.DEL_USER_CD) AS DEL_USER_CD,");
            //sql.AppendLine("(select convert(varchar, M_CHEM.SDS_UPD_DATE, 105)) AS SDS_UPD_DATE2, ");
            sql.AppendLine("(CASE WHEN M_CHEM.DEL_FLAG=0 THEN '使用中' WHEN M_CHEM.DEL_FLAG=1 THEN '削除済み' WHEN M_CHEM.DEL_FLAG=2 THEN '保存中' WHEN M_CHEM.DEL_FLAG=3 THEN '全て' END) AS USAGE_STATUS_NAME ");

            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out param));
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            if (condition.CHEM_SEARCH_TARGET == (int)ChemSearchTarget.Chem)
            {
                sql.AppendLine(" ");
            }
            sql.AppendLine(" order by  CHEM_CD asc");
            List<CsmsCommonInfo> records = DbUtil.Select<CsmsCommonInfo>(sql.ToString(), param);

            makerSelectList = new MakerMasterInfo().GetSelectList();
            if (records.Count > 0)
            {
                records[0].MAKER_NAME_DEL_FLAG = makerSelectList.Any(x => x.Id == records[0].MAKER_ID);
                records[0].UNITSIZE_NAME_DEL_FLAG = unitsizeSelectList.Any(x => x.Id == records[0].UNITSIZE_ID);
                if (records[0].UNITSIZE!=null)
                {
                    records[0].UNITSIZE = DecimalExtension.DecimalZeroSuppress(Convert.ToDecimal(records[0].UNITSIZE)).ToString();
                }               
            }

            if (condition.CHEM_CD == NikonConst.UnregisteredChemCd)
            {
                var chem = new CsmsCommonInfo();
                chem.CHEM_CD = NikonConst.UnregisteredChemCd;
                chem.CHEM_NAME = NikonConst.UnregisteredChemName;
                records.Add(chem);
            }

            var searchQuery = records.AsQueryable();

            if (condition.SearchProductList.Count() > 0)
            {
                var productList = new List<string>();
                foreach (var searchProduct in condition.SearchProductList)
                {
                    productList.Add(searchProduct.CHEM_CD + SystemConst.CodeDelimiter + searchProduct.PRODUCT_SEQ.ToString());
                }
                searchQuery = searchQuery.Where(x => productList.Contains(x.CODE));
            }
            searchQuery.OrderBy(x => x.CHEM_NAME).ThenBy(x => x.MAKER_NAME).ThenBy(x => x.UNITSIZE).ThenBy(x => x.CHEM_CD);

            return searchQuery.ToList();
        }


        protected override void InitializeDictionary()
        {
            keyManagementDic = new CommonMasterInfo(NikonCommonMasterName.KEY_MGMT).GetDictionary();
            weightManagementDic = new CommonMasterInfo(NikonCommonMasterName.WEIGHT_MGMT).GetDictionary();
            figureDic = new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetDictionary();
            makerDic = new MakerMasterInfo().GetDictionary();
            makerSelectList = new MakerMasterInfo().GetSelectList();
            unitsizeDic = new UnitSizeMasterInfo().GetDictionary();
            unitsizeSelectList = new UnitSizeMasterInfo().GetSelectList();
            userDic = new UserMasterInfo().GetDictionary();
            usageStatusDic = new Classes.UsageStatusListMasterInfo().GetDictionary();
            regulationDic = new RegulationMasterInfo().GetDictionary();
            var ghsCategoryDic = new GhsCategoryMasterInfo().GetDictionary();
            foreach (var ghsCategory in ghsCategoryDic) regulationDic.Add(ghsCategory);
        }



        protected override void CreateInsertText(ChemModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            //value.CHEM_CD = GetNewChemCode(); // 20180723 FJ)Shinagawa Del

            insertSql.AppendLine("insert into ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  " + nameof(value.CHEM_CD) + ",");
            insertSql.AppendLine("  " + nameof(value.CHEM_NAME) + ",");
            insertSql.AppendLine("  " + nameof(value.CHEM_CAT) + ",");
            insertSql.AppendLine("  " + nameof(value.DENSITY) + ",");
            insertSql.AppendLine("  " + nameof(value.FLASH_POINT) + ",");
            insertSql.AppendLine("  " + nameof(value.EXPIRATION_DATE) + ",");
            insertSql.AppendLine("  " + nameof(value.KEY_MGMT) + ",");
            insertSql.AppendLine("  " + nameof(value.WEIGHT_MGMT) + ",");
            insertSql.AppendLine("  " + nameof(value.FIGURE) + ",");
            insertSql.AppendLine("  " + nameof(value.CAS_NO) + ",");
            insertSql.AppendLine("  " + nameof(value.MEMO) + ",");
            insertSql.AppendLine("  " + nameof(value.OPTIONAL1) + ",");
            insertSql.AppendLine("  " + nameof(value.OPTIONAL2) + ",");
            insertSql.AppendLine("  " + nameof(value.OPTIONAL3) + ",");
            insertSql.AppendLine("  " + nameof(value.OPTIONAL4) + ",");
            insertSql.AppendLine("  " + nameof(value.OPTIONAL5) + ",");
            //2018/8/17 Add FJ
            if (value.SDS != null)
            {
                insertSql.AppendLine("  " + nameof(value.SDS) + ",");
                insertSql.AppendLine("  " + nameof(value.SDS_MIMETYPE) + ",");
                insertSql.AppendLine("  " + nameof(value.SDS_FILENAME) + ",");
            }
            insertSql.AppendLine("  " + nameof(value.SDS_URL) + ",");
            //-------------------------------------------------------------------------
            insertSql.AppendLine("  " + nameof(value.REG_USER_CD) + ",");
            insertSql.AppendLine("  " + nameof(value.DEL_FLAG) + ","); // 20180723 FJ)Shinagawa Add
            insertSql.AppendLine("  " + nameof(value.REF_CHEM_CD) + ",");    // 20180723 FJ)Shinagawa Add
            insertSql.AppendLine("  " + nameof(value.FIRE_FLAG));    // 20190321 TPE.Rin Add
            insertSql.AppendLine(" )");
            insertSql.AppendLine("values ");
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  @" + nameof(value.CHEM_CD) + ",");
            insertSql.AppendLine("  @" + nameof(value.CHEM_NAME) + ",");
            insertSql.AppendLine("  @" + nameof(value.CHEM_CAT) + ",");
            insertSql.AppendLine("  @" + nameof(value.DENSITY) + ",");
            insertSql.AppendLine("  @" + nameof(value.FLASH_POINT) + ",");
            insertSql.AppendLine("  @" + nameof(value.EXPIRATION_DATE) + ",");
            insertSql.AppendLine("  @" + nameof(value.KEY_MGMT) + ",");
            insertSql.AppendLine("  @" + nameof(value.WEIGHT_MGMT) + ",");
            insertSql.AppendLine("  @" + nameof(value.FIGURE) + ",");
            insertSql.AppendLine("  @" + nameof(value.CAS_NO) + ",");
            insertSql.AppendLine("  @" + nameof(value.MEMO) + ",");
            insertSql.AppendLine("  @" + nameof(value.OPTIONAL1) + ",");
            insertSql.AppendLine("  @" + nameof(value.OPTIONAL2) + ",");
            insertSql.AppendLine("  @" + nameof(value.OPTIONAL3) + ",");
            insertSql.AppendLine("  @" + nameof(value.OPTIONAL4) + ",");
            insertSql.AppendLine("  @" + nameof(value.OPTIONAL5) + ",");
            //2018/8/17 Add FJ
            if (value.SDS != null)
            {
                insertSql.AppendLine("  @" + nameof(value.SDS) + ",");
                insertSql.AppendLine("  @" + nameof(value.SDS_MIMETYPE) + ",");
                insertSql.AppendLine("  @" + nameof(value.SDS_FILENAME) + ",");
            }
            insertSql.AppendLine("  @" + nameof(value.SDS_URL2) + ",");
            //-------------------------------------------------------------------------
            insertSql.AppendLine("  @" + nameof(value.REG_USER_CD) + ",");
            insertSql.AppendLine("  @" + nameof(value.DEL_FLAG) + ","); // 20180723 FJ)Shinagawa Add
            insertSql.AppendLine("  @" + nameof(value.REF_CHEM_CD) + ",");    // 20180723 FJ)Shinagawa Add
            insertSql.AppendLine("  @" + nameof(value.FIRE_FLAG));    // 20190321 TPE.Rin Add
            insertSql.AppendLine(" )");
            parameters.Add(nameof(value.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(value.CHEM_NAME), value.CHEM_NAME);
            parameters.Add(nameof(value.CHEM_CAT), value.CHEM_CAT);
            parameters.Add(nameof(value.DENSITY), OrgConvert.ToNullableDecimal(value.DENSITY));
            parameters.Add(nameof(value.FLASH_POINT), OrgConvert.ToNullableDecimal(value.FLASH_POINT));
            parameters.Add(nameof(value.EXPIRATION_DATE), value.EXPIRATION_DATE);
            parameters.Add(nameof(value.KEY_MGMT), value.KEY_MGMT);
            parameters.Add(nameof(value.WEIGHT_MGMT), value.WEIGHT_MGMT);
            parameters.Add(nameof(value.FIGURE), value.FIGURE);
            parameters.Add(nameof(value.CAS_NO), value.CAS_NO);
            parameters.Add(nameof(value.OPTIONAL1), value.OPTIONAL1);
            parameters.Add(nameof(value.OPTIONAL2), value.OPTIONAL2);
            parameters.Add(nameof(value.OPTIONAL3), value.OPTIONAL3);
            parameters.Add(nameof(value.OPTIONAL4), value.OPTIONAL4);
            parameters.Add(nameof(value.OPTIONAL5), value.OPTIONAL5);
            //2018/8/17 Add FJ
            if (value.SDS != null)
            {
                parameters.Add(nameof(value.SDS), value.SDS);
                parameters.Add(nameof(value.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(value.SDS_FILENAME), value.SDS_FILENAME);

            }
            parameters.Add(nameof(value.SDS_URL2), value.SDS_URL2);
            //--------------------------------------------------------------------------------
            parameters.Add(nameof(value.MEMO), value.MEMO);
            parameters.Add(nameof(value.REG_USER_CD), value.REG_USER_CD);
            parameters.Add(nameof(value.DEL_FLAG), value.DEL_FLAG);       // 20180723 FJ)Shinagawa Add
            parameters.Add(nameof(value.REF_CHEM_CD), value.REF_CHEM_CD); // 20180723 FJ)Shinagawa Add
            var fireflag = 0;
            if (value.FIRE_FLAG == true)
            {
                fireflag = 1;
            }          

            parameters.Add(nameof(value.FIRE_FLAG), fireflag); // 20190321 TPE.Rin Add

            sql = insertSql.ToString();
        }



        public void IsUsedChemName(ChemModel value)
        {
            var sql = new StringBuilder();
            var parameters = new DynamicParameters();
            var keyColumns = getKeyProperty(value);

            sql.AppendLine("select");
            sql.AppendLine("CHEM_NAME");
            sql.AppendLine(",DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("where");
            if (!string.IsNullOrWhiteSpace(value.CHEM_CD))
            {
                sql.AppendLine(" not(" + getKeyWhere(value) + ")");
                sql.AppendLine(" and");



                foreach (var proerties in keyColumns)
                {
                    parameters.Add(proerties.Key, proerties.Value);
                }

                //parameters = keyColumns.ToHashtable();
            }
            if (!string.IsNullOrWhiteSpace(value.REF_CHEM_CD))
            {
                sql.AppendLine(" not(" + nameof(value.REF_CHEM_CD) + " = @" + nameof(value.REF_CHEM_CD) + ")");
                sql.AppendLine(" and");
                parameters.Add(nameof(value.REF_CHEM_CD), value.REF_CHEM_CD);
            }
            sql.AppendLine(" REPLACE(" + nameof(value.CHEM_NAME) + ",' ','') = @" + nameof(value.CHEM_NAME));
            sql.AppendLine(" and");
            sql.AppendLine(" not(" + nameof(value.DEL_FLAG) + " = @" + nameof(value.DEL_FLAG) + ")");
            //string paramkey= keyColumns.Keys



            //if (parameters.Equals(nameof(value.CHEM_NAME)))

            //{
            parameters.Add(nameof(value.CHEM_NAME), value.CHEM_NAME.Replace(" ", "").Replace("　", ""));
            //}
            parameters.Add(nameof(value.DEL_FLAG), (int)DEL_FLAG.Temporary);

            List<ChemModel> records = DbUtil.Select<ChemModel>(sql.ToString(), parameters);

            if (records.Count > 0) //return true;
            {
                //return false;
                foreach (var e in records)
                {

                    if (Convert.ToInt64(e.DEL_FLAG) == (int)DEL_FLAG.Deleted)
                    {
                        IsValid = false;
                        var rowNumberString = string.Empty;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotUnique_RemoveData, "化学物質名", value.CHEM_NAME));
                    }
                    else
                    {
                        IsValid = false;
                        var rowNumberString = string.Empty;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotUnique, "化学物質名", value.CHEM_NAME));
                    }
                }
            }
        }


        public bool IsUsedCasNumber(ChemModel value)
        {

            var sql = new StringBuilder();
            var parameters = new DynamicParameters();

            sql.AppendLine("select");
            sql.AppendLine("CAS_NO");
            sql.AppendLine("from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("where");
            if (!string.IsNullOrWhiteSpace(value.CHEM_CD))
            {
                sql.AppendLine(" not(" + getKeyWhere(value) + ")");
                sql.AppendLine(" and");
                var keyColumns = getKeyProperty(value);
                foreach (var proerties in keyColumns)
                {
                    parameters.Add(proerties.Key, proerties.Value);
                }
                //parameters = keyColumns.ToHashtable();
            }
            // 20180723 FJ)Shinagawa Add Start ->
            if (!string.IsNullOrWhiteSpace(value.REF_CHEM_CD))
            {
                sql.AppendLine(" not(" + nameof(value.REF_CHEM_CD) + " = @" + nameof(value.REF_CHEM_CD) + ")");
                sql.AppendLine(" and");
                parameters.Add(nameof(value.REF_CHEM_CD), value.REF_CHEM_CD);
            }
            // 20180723 FJ)Shinagawa Add End <-
            sql.AppendLine(" " + nameof(value.CAS_NO) + " = @" + nameof(value.CAS_NO));
            sql.AppendLine(" and");
            sql.AppendLine(" " + nameof(value.DEL_FLAG) + " = @" + nameof(value.DEL_FLAG));

            /*if (parameters.Equals(nameof(value.CAS_NO)))*/
            parameters.Add(nameof(value.CAS_NO), value.CAS_NO);
            parameters.Add(nameof(value.DEL_FLAG), (int)DEL_FLAG.Undelete);

            List<ChemModel> records = DbUtil.Select<ChemModel>(sql.ToString(), parameters);

            if (records.Count > 0) return true;
            return false;
        }

        public string GetNewChemCode() // 20180723 FJ)Shinagawa Mod
        {
            var sql = string.Empty;
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
            {
                sql = "select CHEMCODE_SEQ.nextval as CHEMCODE_SEQ from dual";
            }
            if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
            {
                sql = "select next value for CHEMCODE_SEQ";
            }
            List<string> newChemCodeTable = DbUtil.Select<string>(sql, null);
            var row = newChemCodeTable[0];
            return NikonConst.ChemCodePrefix + Convert.ToInt32(row.ToString()).ToString("D8");
        }

        // 20180723 FJ)Shinagawa Add Start ->
        public string GetNewRefChemCode()
        {
            var sql = string.Empty;
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
            {
                sql = "select REFCHEMCODE_SEQ.nextval as REFCHEMCODE_SEQ from dual";
            }
            if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
            {
                sql = "select next value for REFCHEMCODE_SEQ";
            }
            List<string> newChemCodeTable = DbUtil.Select<string>(sql, null);
            var row = newChemCodeTable[0];
            return NikonConst.RefChemCodePrefix + Convert.ToInt32(row.ToString()).ToString("D8");
        }

        protected override void CreateUpdateText(ChemModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            insertSql.AppendLine("update ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine("set ");
            insertSql.AppendLine(" " + nameof(value.CHEM_NAME) + " = @" + nameof(value.CHEM_NAME) + ",");
            insertSql.AppendLine(" " + nameof(value.CHEM_CAT) + " = @" + nameof(value.CHEM_CAT) + ",");
            insertSql.AppendLine(" " + nameof(value.DENSITY) + " = @" + nameof(value.DENSITY) + ",");
            insertSql.AppendLine(" " + nameof(value.FLASH_POINT) + " = @" + nameof(value.FLASH_POINT) + ",");
            insertSql.AppendLine(" " + nameof(value.EXPIRATION_DATE) + " = @" + nameof(value.EXPIRATION_DATE) + ",");
            insertSql.AppendLine(" " + nameof(value.KEY_MGMT) + " = @" + nameof(value.KEY_MGMT) + ",");
            insertSql.AppendLine(" " + nameof(value.WEIGHT_MGMT) + " = @" + nameof(value.WEIGHT_MGMT) + ",");
            insertSql.AppendLine(" " + nameof(value.FIGURE) + " = @" + nameof(value.FIGURE) + ",");
            insertSql.AppendLine(" " + nameof(value.CAS_NO) + " = @" + nameof(value.CAS_NO) + ",");
            insertSql.AppendLine(" " + nameof(value.MEMO) + " = @" + nameof(value.MEMO) + ",");
            insertSql.AppendLine(" " + nameof(value.OPTIONAL1) + " = @" + nameof(value.OPTIONAL1) + ",");
            insertSql.AppendLine(" " + nameof(value.OPTIONAL2) + " = @" + nameof(value.OPTIONAL2) + ",");
            insertSql.AppendLine(" " + nameof(value.OPTIONAL3) + " = @" + nameof(value.OPTIONAL3) + ",");
            insertSql.AppendLine(" " + nameof(value.OPTIONAL4) + " = @" + nameof(value.OPTIONAL4) + ",");
            insertSql.AppendLine(" " + nameof(value.OPTIONAL5) + " = @" + nameof(value.OPTIONAL5) + ",");
            //2018/8/17 Add FJ
            if (value.SDS_FILENAME != null)
            {
                insertSql.AppendLine(" " + nameof(value.SDS) + " = @" + nameof(value.SDS) + ",");
                insertSql.AppendLine(" " + nameof(value.SDS_MIMETYPE) + " = @" + nameof(value.SDS_MIMETYPE) + ",");
                insertSql.AppendLine(" " + nameof(value.SDS_FILENAME) + " = @" + nameof(value.SDS_FILENAME) + ",");
            }
            insertSql.AppendLine(" " + nameof(value.SDS_URL) + " = @" + nameof(value.SDS_URL2) + ",");
            //---------------------------------------------------------------------------------
            insertSql.AppendLine(" " + nameof(value.FIRE_FLAG) + " = @" + nameof(value.FIRE_FLAG) + ",");   //2019/05/10 TPE.Rin Add
            insertSql.AppendLine(" " + nameof(value.UPD_USER_CD) + " = @" + nameof(value.UPD_USER_CD));
            insertSql.AppendLine("where ");
            insertSql.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(value.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(value.CHEM_NAME), value.CHEM_NAME);
            parameters.Add(nameof(value.CHEM_CAT), value.CHEM_CAT);
            parameters.Add(nameof(value.DENSITY), OrgConvert.ToNullableDecimal(value.DENSITY));
            parameters.Add(nameof(value.FLASH_POINT), OrgConvert.ToNullableDecimal(value.FLASH_POINT));
            parameters.Add(nameof(value.EXPIRATION_DATE), value.EXPIRATION_DATE);
            parameters.Add(nameof(value.KEY_MGMT), value.KEY_MGMT);
            parameters.Add(nameof(value.WEIGHT_MGMT), value.WEIGHT_MGMT);
            parameters.Add(nameof(value.FIGURE), value.FIGURE);
            parameters.Add(nameof(value.CAS_NO), value.CAS_NO);
            parameters.Add(nameof(value.MEMO), value.MEMO);
            parameters.Add(nameof(value.OPTIONAL1), value.OPTIONAL1);
            parameters.Add(nameof(value.OPTIONAL2), value.OPTIONAL2);
            parameters.Add(nameof(value.OPTIONAL3), value.OPTIONAL3);
            parameters.Add(nameof(value.OPTIONAL4), value.OPTIONAL4);
            parameters.Add(nameof(value.OPTIONAL5), value.OPTIONAL5);
            //2018/8/17 Add FJ
            if (value.SDS_FILENAME != null)
            {
                parameters.Add(nameof(value.SDS), value.SDS);
                parameters.Add(nameof(value.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(value.SDS_FILENAME), value.SDS_FILENAME);
            }
            parameters.Add(nameof(value.SDS_URL2), value.SDS_URL2);
            //----------------------------------------------------------------------
            //2019/05/10 TPE.Rin Add Start
            var fireflag = 0;
            if (value.FIRE_FLAG == true)
            {
                fireflag = 1;
            }
            parameters.Add(nameof(value.FIRE_FLAG), fireflag);
            //2019/05/10 TPE.Rin Add End

            parameters.Add(nameof(value.UPD_USER_CD), value.UPD_USER_CD);

            sql = insertSql.ToString();
        }

        protected override string ValidateKeyData(ChemModel value, UpdateType updateType, int? rowNumber = default(int?))
        {
            // 20180723 FJ)Shinagawa Mod Start ->
            //if (IsLedgerExists(new ChemSearchModel(value)) && updateType == UpdateType.Delete)
            //{
            //    IsValid = false;
            //    ErrorMessages.Add(string.Format(WarningMessages.RegistedInLedger, value.CHEM_CD,"削除"));
            //}

            if ((IsLedgerExists(new ChemSearchModel(value)) || IsLedgerWorkExists(new ChemSearchModel(value)))
                && updateType == UpdateType.Delete)
            {
                IsValid = false;
                ErrorMessages.Add(string.Format(WarningMessages.RegistedInLedger, value.CHEM_CD, "削除"));
            }
            // 20180723 FJ)Shinagawa Mod End <-

            //2019/02/18 TPE.Sugimoto Add Sta
            if (IsContainExists(new ChemSearchModel(value))
                && updateType == UpdateType.Delete)
            {
                IsValid = false;
                ErrorMessages.Add(string.Format(WarningMessages.UsedContainedError, value.CHEM_NAME));
            }
            //2019/02/18 TPE.Sugimoto Add End

            base.ValidateKeyData(value, updateType, rowNumber);
            return null;
        }

        //protected override void Delete(ChemModel value)
        //{
        //    var sql = new StringBuilder();

        //    sql.AppendLine("update");
        //    sql.AppendLine(" " + TableName);
        //    sql.AppendLine("set");
        //    sql.AppendLine(" " + nameof(value.DEL_USER_CD) + " = @" + nameof(value.DEL_USER_CD) + ",");
        //    sql.AppendLine(" " + nameof(value.DEL_FLAG) + " = " + ((int)DEL_FLAG.Deleted).ToString());
        //    sql.AppendLine("where");
        //    sql.AppendLine(getKeyWhere(value));

        //    var keyColumns = getKeyProperty(value);
        //    var parameters = keyColumns.ToHashtable();
        //    parameters.Add(nameof(value.DEL_USER_CD), value.DEL_USER_CD);

        //    DbUtil.ExecuteUpdate(_Context, sql.ToString(), parameters);
        //}

        //protected override void UndoDeleted(ChemModel value)
        //{
        //    var sql = new StringBuilder();

        //    sql.AppendLine("update");
        //    sql.AppendLine(" " + TableName);
        //    sql.AppendLine("set");
        //    sql.AppendLine(" " + nameof(value.DEL_USER_CD) + " = null,");
        //    sql.AppendLine(" " + nameof(value.UPD_USER_CD) + " = @" + nameof(value.UPD_USER_CD) + ",");
        //    sql.AppendLine(" " + nameof(value.DEL_FLAG) + " = " + ((int)DEL_FLAG.Undelete).ToString() + ",");
        //    sql.AppendLine(" " + nameof(value.DEL_DATE) + " = null");
        //    sql.AppendLine("where");
        //    sql.AppendLine(getKeyWhere(value));

        //    var keyColumns = getKeyProperty(value);
        //    var parameters = keyColumns.ToHashtable();
        //    parameters.Add(nameof(value.UPD_USER_CD), value.UPD_USER_CD);

        //    DbUtil.ExecuteUpdate(_Context, sql.ToString(), parameters);
        //}
        
        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    keyManagementDic = null;
                    weightManagementDic = null;
                    figureDic = null;
                    makerDic = null;
                    unitsizeDic = null;
                    userDic = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        public bool IsLedgerExists(ChemSearchModel value)
        {
            //StartTransaction();

            var sql = new StringBuilder();
            var parameters = new DynamicParameters();

            sql.AppendLine("select T_MGMT_LEDGER.REG_NO from ");
            sql.AppendLine(" " + TableName);
            sql.AppendLine(" left outer join T_MGMT_LEDGER on " + TableName + "." + nameof(value.CHEM_CD) + " = T_MGMT_LEDGER." + nameof(value.CHEM_CD) + " ");
            sql.AppendLine("where");
            sql.AppendLine(" " + TableName + "." + nameof(value.CHEM_CD) + " = @" + nameof(value.CHEM_CD));
            sql.AppendLine("and ");
            sql.AppendLine(" T_MGMT_LEDGER.APPLI_CLASS <> 3 ");

            parameters.Add(nameof(value.CHEM_CD), value.CHEM_CD);

            List<string> records = DbUtil.Select<string>(sql.ToString(), parameters);

            if (records.Count > 0) return true;
            return false;
        }

        public bool IsLedgerWorkExists(ChemSearchModel value)
        {
            //StartTransaction();
            var sql = new StringBuilder();
            DynamicParameters parameters = new DynamicParameters();

            sql.AppendLine("select T_LEDGER_WORK.LEDGER_FLOW_ID from ");
            sql.AppendLine(" " + TableName);
            sql.AppendLine(" left outer join T_LEDGER_WORK on " + TableName + "." + nameof(value.CHEM_CD) + " = T_LEDGER_WORK." + nameof(value.CHEM_CD) + " ");
            sql.AppendLine("where");
            sql.AppendLine(" " + TableName + "." + nameof(value.CHEM_CD) + " = @" + nameof(value.CHEM_CD));
            sql.AppendLine("and ");
            sql.AppendLine(" T_LEDGER_WORK.APPLI_CLASS = 1 ");

            parameters.Add(nameof(value.CHEM_CD), value.CHEM_CD);

            List<string> records = DbUtil.Select<string>(sql.ToString(), parameters);

            if (records.Count > 0) return true;
            return false;
        }
        // 20180723 FJ)Shinagawa Add End <-

        //2019/02/18 TPE.Sugimoto Add Sta
        public bool IsContainExists(ChemSearchModel value)
        {
            //StartTransaction();

            var sql = new StringBuilder();
            DynamicParameters parameters = new DynamicParameters();

            sql.AppendLine("select M_CHEM.CHEM_CD from M_CHEM,M_CONTAINED where");
            sql.AppendLine(" M_CONTAINED.C_CHEM_CD" + " = @" + nameof(value.CHEM_CD));
            sql.AppendLine("and M_CHEM.CHEM_CD = M_CONTAINED.CHEM_CD and M_CHEM.DEL_FLAG in (0,2) ");
            parameters.Add(nameof(value.CHEM_CD), value.CHEM_CD);
            List<string> records = DbUtil.Select<string>(sql.ToString(), parameters);

            if (records.Count > 0) return true;
            return false;
        }
        public bool IsLedgerMgmtGroup(ChemSearchModel value)
        {
            string no;
            return IsLedgerMgmtGroup(value, out no);
        }
        public bool IsLedgerMgmtGroup(ChemSearchModel value, out string no)
        {

            var sql = new StringBuilder();
            var parameters = new DynamicParameters();

            sql.AppendLine("select T_MGMT_LEDGER.REG_NO from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine(" left outer join T_MGMT_LEDGER on " + TableName + "." + nameof(value.CHEM_CD) + " = T_MGMT_LEDGER." + nameof(value.CHEM_CD) + " ");
            sql.AppendLine("where T_MGMT_LEDGER.GROUP_CD in ( ");
            sql.AppendLine("    (select G1.GROUP_CD from M_USER U1 left outer join M_GROUP G1 on G1.GROUP_CD = U1.GROUP_CD where U1.USER_CD = @" + nameof(value.LOGIN_USER_CD) + " ), ");
            sql.AppendLine("    (select G2.GROUP_CD from M_GROUP G2 where G2.GROUP_CD = (select G5.P_GROUP_CD from M_GROUP G5 left outer join M_USER U5 on G5.GROUP_CD = U5.GROUP_CD where U5.USER_CD = @" + nameof(value.LOGIN_USER_CD) + " )),  ");
            sql.AppendLine("    (select G3.GROUP_CD from M_USER U3 left outer join M_GROUP G3 on U3.PREVIOUS_GROUP_CD = G3.GROUP_CD where U3.USER_CD = @" + nameof(value.LOGIN_USER_CD) + " and G3.DEL_FLAG = 1),  ");
            sql.AppendLine("    (select G4.GROUP_CD from M_GROUP G4 where G4.GROUP_CD = (select G6.P_GROUP_CD from M_USER U6 left outer join M_GROUP G6 on U6.PREVIOUS_GROUP_CD = G6.GROUP_CD where U6.USER_CD = @" + nameof(value.LOGIN_USER_CD) + " and G6.DEL_FLAG = 1 ))");
            sql.AppendLine(") ");
            sql.AppendLine("and ");
            sql.AppendLine(" T_MGMT_LEDGER.APPLI_CLASS <> 3 ");
            sql.AppendLine("and ");
            sql.AppendLine(" " + TableName + "." + nameof(value.CHEM_CD) + " = @" + nameof(value.CHEM_CD));

            parameters.Add(nameof(value.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(value.LOGIN_USER_CD), value.LOGIN_USER_CD);

            List<string> records = DbUtil.Select<string>(sql.ToString(), parameters);

            if (records.Count > 0)
            {
                no = records[0].ToString();
                return true;
            }
            no = string.Empty;
            return false;
        }

        public void ChemDeletePhysical(ChemModel value)
        {
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("Delete from M_CHEM WHERE CHEM_CD = @CHEM_CD");
            sql.AppendLine("Delete from M_CHEM_PRODUCT where  CHEM_CD = @CHEM_CD");
            sql.AppendLine("Delete from M_CHEM_ALIAS where  CHEM_CD = @CHEM_CD");
            sql.AppendLine("Delete from M_CONTAINED where  CHEM_CD = @CHEM_CD");
            sql.AppendLine("Delete from M_CHEM_REGULATION where  CHEM_CD = @CHEM_CD");
            var keyColumns = getKeyProperty(value);
            DynamicParameters dapperparameter = new DynamicParameters();
            var parameters = keyColumns;
            foreach (var parameter in parameters)
            {
                if (parameter.Key.Contains("CHEM_CD"))
                {
                    dapperparameter.Add("@CHEM_CD", parameter.Value);
                }
            }

            DbUtil.ExecuteUpdate(sql.ToString(), dapperparameter);
        }

        public bool getLocationListInLedger(ChemSearchModel value, out DataTable data)
        {
            // StartTransaction();

            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" T_MGMT_LED_LOC.LOC_ID,V_LOCATION.LOC_BARCODE,V_LOCATION.LOC_NAME");
            sql.AppendLine("from");
            sql.AppendLine(" T_MGMT_LED_LOC inner join V_LOCATION on T_MGMT_LED_LOC.LOC_ID = V_LOCATION.LOC_ID");
            sql.AppendLine("where");
            sql.AppendLine(" REG_NO = @" + nameof(value.REG_NO));
            sql.AppendLine("order by");
            sql.AppendLine(" LOC_SEQ");

            parameters.Add(nameof(value.REG_NO), value.REG_NO);

            data = DbUtil.Select(_Context, sql.ToString(), parameters);

            if (data.Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        public override List<ChemModel> GetSearchResult(ChemModel condition)
        {
            throw new NotImplementedException();
        }

        public override void EditData(ChemModel Value)
        {
            Value.KEY_MGMT_NAME = keyManagementDic.GetValueToString(Value.KEY_MGMT);
            Value.WEIGHT_MGMT_NAME = weightManagementDic.GetValueToString(Value.WEIGHT_MGMT);
            Value.FIGURE_NAME = figureDic.GetValueToString(Value.FIGURE);
            Value.MAKER_NAME = makerDic.GetValueToString(Value.MAKER_ID);
            Value.MAKER_NAME_DEL_FLAG = makerSelectList.Any(x => x.Id == Value.MAKER_ID);
            Value.UNITSIZE_NAME = unitsizeDic.GetValueToString(Value.UNITSIZE_ID);
            Value.UNITSIZE_NAME_DEL_FLAG = unitsizeSelectList.Any(x => x.Id == Value.UNITSIZE_ID);
            Value.REG_USER_NAME = userDic.GetValueToString(Value.REG_USER_CD);
            Value.UPD_USER_NAME = userDic.GetValueToString(Value.UPD_USER_CD);
            Value.DEL_USER_NAME = userDic.GetValueToString(Value.UPD_USER_CD);
            Value.USAGE_STATUS_NAME = usageStatusDic.GetValueToString(Value.DEL_FLAG);
        }

        protected override void CreateInsertText(ChemModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(ChemModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }

}


