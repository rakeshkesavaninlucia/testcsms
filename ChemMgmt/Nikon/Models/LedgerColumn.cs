﻿using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;
using ZyCoaG.Nikon;

namespace ChemMgmt.Nikon.Models
{
    public class LedgerColumn : SystemColumn
    {
        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Display(Name = "最終フローID")]
        public int? RECENT_LEDGER_FLOW_ID { get; set; }

        [Display(Name = "フローコード")]
        public string FLOW_CD { get; set; }

        [Display(Name = "現在フロー階層")]
        public int? HIERARCHY { get; set; }

        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Display(Name = "申請区分")]
        public int? APPLI_CLASS { get; set; }

        [Display(Name = "部署コード")]
        public string GROUP_CD { get; set; }

        [Display(Name = "変更・廃止の事由")]
        public int? REASON_ID { get; set; }

        [Display(Name = "変更・廃止の事由(その他）")]
        public string OTHER_REASON { get; set; }

        [Display(Name = "化学物質コード")]
        public string CHEM_CD { get; set; }

        [Display(Name = "メーカー名")]
        public string MAKER_NAME { get; set; }

        [Display(Name = "その他該当法令")]
        public string OTHER_REGULATION { get; set; }

        [Display(Name = "SDS (PDF)")]
        public Byte[] SDS { get; set; } = new Byte[0];
        public string SDS_MIMETYPE { get; set; }
        public string SDS_FILENAME { get; set; }

        [Display(Name = "SDS (URL)")]
        public string SDS_URL { get; set; }

        [Display(Name = "未登録化学物質情報")]
        public Byte[] UNREGISTERED_CHEM { get; set; } = new Byte[0];
        public int hdnUNREGISTERED_CHEM { get; set; }
        
        public string UNREGISTERED_CHEM_MIMETYPE { get; set; }
        public string UNREGISTERED_CHEM_FILENAME { get; set; }

        [Display(Name = "備考")]
        public string MEMO { get; set; }

        [Display(Name = "一時保存フラグ")]
        public int? TEMP_SAVED_FLAG { get; set; } = 0;

        public static explicit operator T_LEDGER_WORK(LedgerColumn model)
        {
            return new T_LEDGER_WORK()
            {
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                FLOW_CD = model.FLOW_CD,
                HIERARCHY = model.HIERARCHY,
                REG_NO = model.REG_NO,
                APPLI_CLASS = model.APPLI_CLASS,
                GROUP_CD = model.GROUP_CD,
                REASON_ID = model.REASON_ID,
                OTHER_REASON = model.OTHER_REASON,
                CHEM_CD = model.CHEM_CD,
                MAKER_NAME = model.MAKER_NAME,
                OTHER_REGULATION = model.OTHER_REGULATION,
                SDS = model.SDS,
                SDS_MIMETYPE = model.SDS_MIMETYPE,
                SDS_URL = model.SDS_URL,
                UNREGISTERED_CHEM = model.UNREGISTERED_CHEM,
                UNREGISTERED_CHEM_MIMETYPE = model.UNREGISTERED_CHEM_MIMETYPE,
                MEMO = model.MEMO,
                TEMP_SAVED_FLAG = model.TEMP_SAVED_FLAG,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
                UPD_DATE = model.UPD_DATE,
                DEL_DATE = model.DEL_DATE,
                DEL_FLAG = model.DEL_FLAG,
            };
        }

        public static explicit operator T_LEDGER_HISTORY(LedgerColumn model)
        {
            return new T_LEDGER_HISTORY()
            {
                LEDGER_HISTORY_ID = model.LEDGER_HISTORY_ID,
                FLOW_CD = model.FLOW_CD,
                HIERARCHY = model.HIERARCHY,
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                REG_NO = model.REG_NO,
                APPLI_CLASS = model.APPLI_CLASS,
                GROUP_CD = model.GROUP_CD,
                REASON_ID = model.REASON_ID,
                OTHER_REASON = model.OTHER_REASON,
                CHEM_CD = model.CHEM_CD,
                MAKER_NAME = model.MAKER_NAME,
                OTHER_REGULATION = model.OTHER_REGULATION,
                SDS = model.SDS,
                SDS_MIMETYPE = model.SDS_MIMETYPE,
                SDS_FILENAME = model.SDS_FILENAME,
                SDS_URL = model.SDS_URL,
                UNREGISTERED_CHEM = model.UNREGISTERED_CHEM,
                UNREGISTERED_CHEM_MIMETYPE = model.UNREGISTERED_CHEM_MIMETYPE,
                UNREGISTERED_CHEM_FILENAME = model.UNREGISTERED_CHEM_FILENAME,
                MEMO = model.MEMO,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
                UPD_DATE = model.UPD_DATE,
                DEL_DATE = model.DEL_DATE,
                DEL_FLAG = model.DEL_FLAG,
            };
        }

        public static explicit operator T_MGMT_LEDGER(LedgerColumn model)
        {
            return new T_MGMT_LEDGER()
            {
                REG_NO = model.REG_NO,
                RECENT_LEDGER_FLOW_ID = model.RECENT_LEDGER_FLOW_ID,
                GROUP_CD = model.GROUP_CD,
                REASON_ID = model.REASON_ID,
                OTHER_REASON = model.OTHER_REASON,
                CHEM_CD = model.CHEM_CD,
                MAKER_NAME = model.MAKER_NAME,
                OTHER_REGULATION = model.OTHER_REGULATION,
                SDS = model.SDS,
                SDS_MIMETYPE = model.SDS_MIMETYPE,
                SDS_URL = model.SDS_URL,
                UNREGISTERED_CHEM = model.UNREGISTERED_CHEM,
                UNREGISTERED_CHEM_MIMETYPE = model.UNREGISTERED_CHEM_MIMETYPE,
                MEMO = model.MEMO,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
                UPD_DATE = model.UPD_DATE,
                DEL_DATE = model.DEL_DATE,
                DEL_FLAG = model.DEL_FLAG,
            };
        }
    }
}