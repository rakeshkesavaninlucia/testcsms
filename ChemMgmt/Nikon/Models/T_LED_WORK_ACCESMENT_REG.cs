﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class T_LED_WORK_ACCESMENT_REG : SystemColumn
    {
        [Key]
        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Key]
        [Display(Name = "使用場所シーケンス（連番）")]
        public int? USAGE_LOC_SEQ { get; set; }

        [Key]
        [Display(Name = "管理台帳法規制ID")]
        public int? LED_ACCESMENT_REG_ID { get; set; }

        [Display(Name = "法規制種類ID")]
        public int? REG_TYPE_ID { get; set; }

        [Display(Name = "法規制・注意事項情報")]
        public string REG_TEXT { get; set; }

        [Display(Name = "適用法令詳細")]
        public string REG_TEXT_LAST { get; set; }

        [Display(Name = "法規制・注意事項用アイコンのファイル名")]
        public string REG_ICON_PATH { get; set; }

        [Display(Name = "CAS")]
        public string CAS_NO { get; set; }

        [Display(Name = "指定数量")]
        public decimal FIRE_SIZE { get; set; }

        [Display(Name = "作業記録管理")]
        public int? WORKTIME_MGMT { get; set; }
    }
}