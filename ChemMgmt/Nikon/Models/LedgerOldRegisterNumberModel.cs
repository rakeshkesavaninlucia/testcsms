﻿using ChemMgmt.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class LedgerOldRegisterNumberModel : LedgerOldRegisterNumberColumn
    {
        public int _Mode { get; set; }

        public Mode Mode
        {
            get
            {
                Mode resultMode;
                if (Mode.TryParse(_Mode.ToString(), out resultMode)) return resultMode;
                return default(Mode);
            }
            set
            {
                _Mode = (int)value;
            }
        }

        public LedgerOldRegisterNumberModel() { }

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

    }

    public class LedgerOldRegisterNumberSearchModel : LedgerOldRegisterNumberModel
    {
        public int? MaxSearchResult { get; set; }

        public LedgerOldRegisterNumberSearchModel()
        {
            SearchOldRegisterNumbers = new List<LedgerOldRegisterNumberModel>();
        }

        public IEnumerable<LedgerOldRegisterNumberModel> SearchOldRegisterNumbers;

        /// <summary>
        /// 項目が読取のみか
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                        return false;
                    default:
                        return true;
                }
            }
        }

        public bool IsButtonVisibleOrEnabled
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}