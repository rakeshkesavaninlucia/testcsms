﻿using ChemMgmt.Models;
using ChemMgmt.Nikon.Models;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon
{
    public class LedRegulationMasterInfo : Classes.LedRegulationMasterInfo
    {

        //public IEnumerable<int> GetIdsBelongInChem(string chemCode)
        //{
        //    var sql = new StringBuilder();
        //    sql.AppendLine("select");
        //    sql.AppendLine(" " + nameof(M_CHEM_REGULATION.REG_TYPE_ID));
        //    sql.AppendLine("from");
        //    sql.AppendLine(" " + nameof(M_CHEM_REGULATION));
        //    sql.AppendLine("where");
        //    sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CHEM_CD) + " = :" + nameof(M_CHEM_REGULATION.CHEM_CD));

        //    var parameters = new Hashtable();
        //    parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);

        //    StartTransaction();
        //    DataTable records = DbUtil.select(_Context, sql.ToString(), parameters);
        //    Close();

        //    var ids = new List<int>();
        //    foreach (DataRow record in records.Rows)
        //    {
        //        ids.Add(Convert.ToInt32(record[nameof(V_REGULATION.REG_TYPE_ID)].ToString()));
        //    }
        //    return ids;
        //}      

        public Dictionary<int, int> GetIdsInputFlg(string chemCode)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.REG_TYPE_ID));
            sql.AppendLine(" ,isnull(" + nameof(M_CHEM_REGULATION.INPUT_FLAG) + ",'0') AS INPUT_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION));
            sql.AppendLine("where");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CHEM_CD) + " = @" + nameof(M_CHEM_REGULATION.CHEM_CD));

            var parameters = new DynamicParameters();
            parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);

            StartTransaction();
            List<M_CHEM_REGULATION> records = DbUtil.Select<M_CHEM_REGULATION>(sql.ToString(), parameters);
            Close();

            var ids = new Dictionary<int, int>();
            if (records.Count > 0)
            {
                foreach (var record in records)
                {
                    //ids.Add(Convert.ToInt32(record.ItemArray[0].ToString()), Convert.ToInt32(record.ItemArray[1].ToString()));
                    ids.Add(Convert.ToInt32(record.REG_TYPE_ID), Convert.ToInt32(record.INPUT_FLAG));
                }
            }
            return ids;
        }

        /// <summary>
        /// 化学物質(単体、混合物含む)の法規制IDを取得します。
        /// </summary>
        public IEnumerable<int> LedgerGetIdsBelongInChem(string chemCode)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select ");
            sql.AppendLine("distinct ");
            sql.AppendLine(nameof(M_CHEM_REGULATION) + ".REG_TYPE_ID, ");
            sql.AppendLine(nameof(M_REGULATION) + ".P_REG_TYPE_ID, ");
            sql.AppendLine(nameof(M_REGULATION) + ".SORT_ORDER ");
            sql.AppendLine("from ");
            sql.AppendLine(nameof(M_CHEM_REGULATION));
            sql.AppendLine(" inner join ");
            sql.AppendLine(nameof(M_REGULATION));
            sql.AppendLine(" on ");
            sql.AppendLine(nameof(M_CHEM_REGULATION) + ".REG_TYPE_ID =" + nameof(M_REGULATION) + ".REG_TYPE_ID ");
            sql.AppendLine(" where ");
            sql.AppendLine(nameof(M_CHEM_REGULATION) + ".CHEM_CD = @CHEM_CD ");
            sql.AppendLine("union ");
            sql.AppendLine("select ");
            sql.AppendLine("distinct ");
            sql.AppendLine(nameof(M_CHEM_REGULATION) + ".REG_TYPE_ID, ");
            sql.AppendLine(nameof(M_REGULATION) + ".P_REG_TYPE_ID, ");
            sql.AppendLine(nameof(M_REGULATION) + ".SORT_ORDER ");
            sql.AppendLine("from ");
            sql.AppendLine(nameof(M_CHEM_REGULATION));
            sql.AppendLine(" inner join ");
            sql.AppendLine(nameof(M_REGULATION));
            sql.AppendLine(" on ");
            sql.AppendLine(nameof(M_CHEM_REGULATION) + ".REG_TYPE_ID =" + nameof(M_REGULATION) + ".REG_TYPE_ID ");
            sql.AppendLine(" where ");
            sql.AppendLine(nameof(M_CHEM_REGULATION) + ".CHEM_CD in (select C_CHEM_CD from V_WCHEM where CHEM_CD = @CHEM_CD) and ");
            sql.AppendLine(nameof(M_CHEM_REGULATION) + ".CLASS_ID <> @CLASS_ID ");
            sql.AppendLine(" order by " + nameof(M_REGULATION) + ".P_REG_TYPE_ID, ");
            sql.AppendLine(nameof(M_REGULATION) + ".SORT_ORDER ");

            var parameters = new DynamicParameters();
            parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);
            parameters.Add(nameof(M_CHEM_REGULATION.CLASS_ID), (int)ClassIdType.Fire);

            StartTransaction();
            List<M_REGULATION> records = DbUtil.Select<M_REGULATION>(sql.ToString(), parameters);
            //DataTable dt = ConvertToDataTable(records);
            Close();

            var ids = new List<int>();
            foreach (M_REGULATION record in records)
            {
                ids.Add(Convert.ToInt32(record.REG_TYPE_ID));
            }
            return ids;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
    }
}