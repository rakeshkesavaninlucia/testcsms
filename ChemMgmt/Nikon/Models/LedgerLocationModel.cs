﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Nikon
{
    public class LedgerLocationModel : LedgerLocationColumn
    {
        public LedgerLocationModel() { }

        /// <summary>
        /// 行識別ID
        /// </summary>
        public string RowId { get; set; } = Guid.NewGuid().ToString();

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

        /// <summary>
        /// 保管場所名
        /// </summary>
        public string LOC_NAME { get; set; }

        /// <summary>
        /// 保管場所危険物フラグ
        /// </summary>
        public bool FLG_LOC_HAZARD { get; set; }



    }

    public class LedgerLocationSearchModel : LedgerLocationColumn
    {
        public int? MaxSearchResult { get; set; }

        public LedgerLocationSearchModel()
        {
            SearchLocationList = new List<LedgerLocationSearchModel>();
        }

        public IEnumerable<LedgerLocationSearchModel> SearchLocationList;
    }
}