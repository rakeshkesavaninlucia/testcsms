﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class T_LED_HISTORY_RISK_REDUCE : SystemColumn
    {
        [Key]
        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Key]
        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Key]
        [Display(Name = "使用場所シーケンス（連番）")]
        public int? USAGE_LOC_SEQ { get; set; }

        [Key]
        [Display(Name = "管理台帳リスク低減策ID")]
        public int? LED_RISK_REDUCE_ID { get; set; }

        [Display(Name = "リスク低減策タイプ")]
        public int? LED_RISK_REDUCE_TYPE { get; set; }

        [Display(Name = "リスク低減策フラグ")]
        public bool LED_RISK_REDUCE_FLAG { get; set; }
    }
}