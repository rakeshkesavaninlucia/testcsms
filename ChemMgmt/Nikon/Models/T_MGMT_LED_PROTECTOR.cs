﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class T_MGMT_LED_PROTECTOR : SystemColumn
    {

        [Key]
        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Key]
        [Display(Name = "使用場所シーケンス（連番）")]
        public int? USAGE_LOC_SEQ { get; set; }

        [Key]
        [Display(Name = "保護具シーケンス（連番）")]
        public int? PROTECTOR_SEQ { get; set; }

        [Display(Name = "保護具ID")]
        [Required]
        public int? PROTECTOR_ID { get; set; }

    }
}