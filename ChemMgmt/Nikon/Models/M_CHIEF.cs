﻿using System.ComponentModel.DataAnnotations;


namespace ZyCoaG.Nikon
{
    public class M_CHIEF : SystemColumn
    {

        /// <summary>
        /// ユーザーコード
        /// </summary>
        [Display(Name = "NID")]
        [StringLength(30)]
        public string USER_CD { get; set; }

        [Display(Name = "氏名")]
        public string USER_NAME { get; set; }
    }
}