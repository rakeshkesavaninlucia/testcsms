﻿using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class V_GHSCAT : M_GHSCAT
    {
        /// <summary>
        /// GHS分類名(カテゴリ)
        /// </summary>
        public string GHSCAT_NAME_BASE { get; set; }

        /// <summary>
        /// GHS分類名(名称)
        /// </summary>
        public string GHSCAT_NAME_LAST { get; set; }

        /// <summary>
        /// アイコンURL
        /// </summary>
        public string IconUrl
        {
            get
            {
                return !string.IsNullOrWhiteSpace(GHSCAT_ICON_PATH) ? "../" + SystemConst.RegulationImageFolderName + "/" + GHSCAT_ICON_PATH : null;
            }
        }

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

        public string INPUT_FLAG { get; set; }
    }
}