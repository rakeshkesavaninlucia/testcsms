﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class LedgerUsageLocationColumn : SystemColumn
    {
        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Display(Name = "使用場所シーケンス（連番）")]
        public int? USAGE_LOC_SEQ { get; set; }

        [Display(Name = "場所ID")]
        public int? LOC_ID { get; set; }

        [Display(Name = "最大使用量")]
        public string AMOUNT { get; set; }

        [Display(Name = "単位")]
        public int? UNITSIZE_ID { get; set; }

        [Display(Name = "労働安全衛生法の用途")]
        public int? ISHA_USAGE { get; set; }

        [Display(Name = "労働安全衛生法の用途(その他)")]
        public string ISHA_OTHER_USAGE { get; set; }

        [Display(Name = "化学物質取扱責任者")]
        public string CHEM_OPERATION_CHIEF { get; set; }

        [Display(Name = "小分け容器の有無")]
        public int? SUB_CONTAINER { get; set; }

        [Display(Name = "装置への投入")]
        public string ENTRY_DEVICE { get; set; }

        [Display(Name = "保護具(その他)")]
        public string OTHER_PROTECTOR { get; set; }

        [Display(Name = "措置前点数")]
        public decimal? MEASURES_BEFORE_SCORE { get; set; }

        [Display(Name = "措置前ランク")]
        public int? MEASURES_BEFORE_RANK { get; set; }

        [Display(Name = "措置後点数")]
        public decimal? MEASURES_AFTER_SCORE { get; set; }

        [Display(Name = "措置後ランク")]
        public int? MEASURES_AFTER_RANK { get; set; }

        [Display(Name = "職場の取扱量")]
        public int? REG_TRANSACTION_VOLUME { get; set; }

        [Display(Name = "作業頻度")]
        public int? REG_WORK_FREQUENCY { get; set; }

        [Display(Name = "災害発生の可能性")]
        public int? REG_DISASTER_POSSIBILITY { get; set; }

        [Display(Name = "リスクアセスメント済フラグ")]
        public Boolean FLG_RISK_ASSESSMENT { get; set; }

        [Display(Name = "CAS")]
        public string CAS_NO { get; set; }

        public static explicit operator T_LED_WORK_USAGE_LOC(LedgerUsageLocationColumn model)
        {
            return new T_LED_WORK_USAGE_LOC()
            {
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                LOC_ID = model.LOC_ID,
                AMOUNT = model.AMOUNT,
                CAS_NO = model.CAS_NO,
                UNITSIZE_ID = model.UNITSIZE_ID,
                ISHA_USAGE = model.ISHA_USAGE,
                ISHA_OTHER_USAGE = model.ISHA_OTHER_USAGE,
                CHEM_OPERATION_CHIEF = model.CHEM_OPERATION_CHIEF,
                SUB_CONTAINER = model.SUB_CONTAINER,
                ENTRY_DEVICE = model.ENTRY_DEVICE,
                OTHER_PROTECTOR = model.OTHER_PROTECTOR,
                MEASURES_BEFORE_SCORE = model.MEASURES_BEFORE_SCORE,
                MEASURES_BEFORE_RANK = model.MEASURES_BEFORE_RANK,
                MEASURES_AFTER_SCORE = model.MEASURES_AFTER_SCORE,
                MEASURES_AFTER_RANK = model.MEASURES_AFTER_RANK,
                REG_TRANSACTION_VOLUME = model.REG_TRANSACTION_VOLUME,
                REG_WORK_FREQUENCY = model.REG_WORK_FREQUENCY,
                REG_DISASTER_POSSIBILITY = model.REG_DISASTER_POSSIBILITY,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LED_HISTORY_USAGE_LOC(LedgerUsageLocationColumn model)
        {
            return new T_LED_HISTORY_USAGE_LOC()
            {
                LEDGER_HISTORY_ID = model.LEDGER_HISTORY_ID,
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                LOC_ID = model.LOC_ID,
                AMOUNT = model.AMOUNT,
                CAS_NO = model.CAS_NO,
                UNITSIZE_ID = model.UNITSIZE_ID,
                ISHA_USAGE = model.ISHA_USAGE,
                ISHA_OTHER_USAGE = model.ISHA_OTHER_USAGE,
                CHEM_OPERATION_CHIEF = model.CHEM_OPERATION_CHIEF,
                SUB_CONTAINER = model.SUB_CONTAINER,
                ENTRY_DEVICE = model.ENTRY_DEVICE,
                OTHER_PROTECTOR = model.OTHER_PROTECTOR,
                MEASURES_BEFORE_SCORE = model.MEASURES_BEFORE_SCORE,
                MEASURES_BEFORE_RANK = model.MEASURES_BEFORE_RANK,
                MEASURES_AFTER_SCORE = model.MEASURES_AFTER_SCORE,
                MEASURES_AFTER_RANK = model.MEASURES_AFTER_RANK,
                REG_TRANSACTION_VOLUME = model.REG_TRANSACTION_VOLUME,
                REG_WORK_FREQUENCY = model.REG_WORK_FREQUENCY,
                REG_DISASTER_POSSIBILITY = model.REG_DISASTER_POSSIBILITY,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_MGMT_LED_USAGE_LOC(LedgerUsageLocationColumn model)
        {
            return new T_MGMT_LED_USAGE_LOC()
            {
                REG_NO = model.REG_NO,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                LOC_ID = model.LOC_ID,
                AMOUNT = model.AMOUNT,
                CAS_NO = model.CAS_NO,
                UNITSIZE_ID = model.UNITSIZE_ID,
                ISHA_USAGE = model.ISHA_USAGE,
                ISHA_OTHER_USAGE = model.ISHA_OTHER_USAGE,
                CHEM_OPERATION_CHIEF = model.CHEM_OPERATION_CHIEF,
                SUB_CONTAINER = model.SUB_CONTAINER,
                ENTRY_DEVICE = model.ENTRY_DEVICE,
                OTHER_PROTECTOR = model.OTHER_PROTECTOR,
                MEASURES_BEFORE_SCORE = model.MEASURES_BEFORE_SCORE,
                MEASURES_BEFORE_RANK = model.MEASURES_BEFORE_RANK,
                MEASURES_AFTER_SCORE = model.MEASURES_AFTER_SCORE,
                MEASURES_AFTER_RANK = model.MEASURES_AFTER_RANK,
                REG_TRANSACTION_VOLUME = model.REG_TRANSACTION_VOLUME,
                REG_WORK_FREQUENCY = model.REG_WORK_FREQUENCY,
                REG_DISASTER_POSSIBILITY = model.REG_DISASTER_POSSIBILITY,
                REG_DATE = model.REG_DATE,
            };
        }
    }
}