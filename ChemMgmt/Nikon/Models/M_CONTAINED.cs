﻿using ZyCoaG;
using System.ComponentModel.DataAnnotations;
using ChemMgmt.Classes;

namespace ChemMgmt.Nikon.Models
{
    /// <summary>
    /// 含有物質マスター
    /// </summary>
    public class M_CONTAINED : SystemColumn
    {
        /// <summary>
        /// 化学物質コード
        /// </summary>
        [Key]
        public string CHEM_CD { get; set; }

        /// <summary>
        /// 子化学物質コード
        /// </summary>
        [Key]
        public string C_CHEM_CD { get; set; }

        /// <summary>
        /// 含有率
        /// </summary>
        public decimal? MIN_CONTAINED_RATE { get; set; }

        /// <summary>
        /// 含有率下限
        /// </summary>
        [Display(Name = "含有率下限(%)")]
        [Halfwidth]
        [Number]
        [NumberLength(3, 3)]
        [Range(0, 100)]
        [SmallCompare(nameof(MaxContainedRate))]
        public string MinContainedRate { get; set; }

        /// <summary>
        /// 含有率上限
        /// </summary>
        public decimal? MAX_CONTAINED_RATE { get; set; }

        /// <summary>
        /// 含有率上限
        /// </summary>
        [Display(Name = "含有率上限(%)")]
        [Halfwidth]
        [Number]
        [NumberLength(3, 3)]
        [Range(0, 100)]
        public string MaxContainedRate { get; set; }

        public int UniqueRowId { get; set; }
    }
}