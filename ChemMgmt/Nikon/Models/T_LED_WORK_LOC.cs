﻿using ChemMgmt.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class T_LED_WORK_LOC : SystemColumn
    {

        [Key]
        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Key]
        [Display(Name = "保管場所シーケンス（連番）")]
        public int? LOC_SEQ { get; set; }

        [Display(Name = "場所ID")]
        [Required]
        public int? LOC_ID { get; set; }

        [Display(Name = "最大保管量")]
        [NumberLength(8, 3)]
        [RegularNumber]
        [Required]
        public string AMOUNT { get; set; }

        [Display(Name = "単位")]
        [Required]
        public int? UNITSIZE_ID { get; set; }

    }
}