﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class T_MGMT_LEDGER : SystemColumn
    {
        [Key]
        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Display(Name = "申請区分")]
        public int? APPLI_CLASS { get; set; }

        [Display(Name = "最終フローID")]
        public int? RECENT_LEDGER_FLOW_ID { get; set; }

        [Display(Name = "部署コード")]
        [Required]
        public string GROUP_CD { get; set; }

        [Display(Name = "変更・廃止の事由")]
        public int? REASON_ID { get; set; }

        [Display(Name = "変更・廃止の事由(その他）")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string OTHER_REASON { get; set; }

        [Display(Name = "化学物質コード")]
        [StringLength(10)]
        [Required]
        [ExcludingProhibition]
        public string CHEM_CD { get; set; }

        [Display(Name = "メーカー名")]
        [StringLength(255)]
        [ExcludingProhibition]
        public string MAKER_NAME { get; set; }

        [Display(Name = "その他該当法令")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string OTHER_REGULATION { get; set; }

        [Display(Name = "SDS (PDF)")]
        public Byte[] SDS { get; set; }
        public string SDS_MIMETYPE { get; set; }
        public string SDS_FILENAME { get; set; }

        [Display(Name = "SDS (URL)")]
        public string SDS_URL { get; set; }

        [Display(Name = "未登録化学物質情報")]
        public Byte[] UNREGISTERED_CHEM { get; set; }
        public string UNREGISTERED_CHEM_MIMETYPE { get; set; }
        public string UNREGISTERED_CHEM_FILENAME { get; set; }

        [Display(Name = "備考")]
        [StringLength(1000)]
        [ExcludingProhibition]
        public string MEMO { get; set; }
    }
}