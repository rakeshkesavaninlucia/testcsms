﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class V_APPLICABLE_LAW : SystemColumn
    {
        [Key]
        [Display(Name = "法規制種類ID")]
        public int? REG_TYPE_ID { get; set; }

        [Display(Name = "法規制・注意事項情報")]
        public string REG_TEXT { get; set; }

        [Display(Name = "適用法令")]
        public string REG_TEXT_BASE { get; set; }

        [Display(Name = "適用法令詳細")]
        public string REG_TEXT_LAST { get; set; }

        [Display(Name = "CAS")]
        public string CAS_NO { get; set; }

        [Display(Name = "点数")]
        public int? MARKS { get; set; }

        [Display(Name = "使用条件")]
        [Required]
        public int? ISHA_USAGE { get; set; }

        [Display(Name = "指定数量")]
        public decimal FIRE_SIZE { get; set; }

        [Display(Name = "リスクアセスメント対象")]
        [Required]
        public int? RA_FLAG { get; set; }
    }
}