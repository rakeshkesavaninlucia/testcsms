﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class T_LEDGER_WORK_REGULATION : SystemColumn
    {
        [Key]
        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Key]
        [Display(Name = "管理台帳法規制ID")]
        public int? LED_REGULATION_ID { get; set; }

        [Display(Name = "法規制種類ID")]
        public int? REG_TYPE_ID { get; set; }

        [Display(Name = "法規制・注意事項情報")]
        public string REG_TEXT { get; set; }

        //[Display(Name = "CASリスト")]
        //public string CAS_NO_LIST { get; set; }
    }
}