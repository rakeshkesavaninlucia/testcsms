﻿using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class V_WORKFLOW_LEDGER_HISTORY : SystemColumn
    {

        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Display(Name = "結果")]
        public int? RESULT { get; set; }

        [Display(Name = "階層")]
        public int? HIERARCHY { get; set; }

        [Display(Name = "コメント")]
        public string MEMO { get; set; }

        [Display(Name = "承認者")]
        public string APPROVAL_USER_CD { get; set; }

        [Display(Name = "承認目的")]
        public string APPROVAL_TARGET { get; set; }

    }
}