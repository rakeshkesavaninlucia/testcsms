﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class T_LED_HISTORY_PROTECTOR : SystemColumn
    {
        [Key]
        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Key]
        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Key]
        [Display(Name = "使用場所シーケンス（連番）")]
        public int? USAGE_LOC_SEQ { get; set; }

        [Key]
        [Display(Name = "保護具シーケンス（連番）")]
        public int? PROTECTOR_SEQ { get; set; }

        [Display(Name = "保護具ID")]
        [Required]
        public int? PROTECTOR_ID { get; set; }

    }
}