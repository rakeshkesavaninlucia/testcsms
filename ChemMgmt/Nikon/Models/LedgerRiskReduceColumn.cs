﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class LedgerRiskReduceColumn : SystemColumn
    {
        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Display(Name = "使用場所シーケンス（連番）")]
        public int? USAGE_LOC_SEQ { get; set; }

        [Display(Name = "管理台帳リスク低減策ID")]
        public int? LED_RISK_REDUCE_ID { get; set; }

        [Display(Name = "リスク低減策タイプ")]
        public int? LED_RISK_REDUCE_TYPE { get; set; }

        [Display(Name = "リスク低減策フラグ")]
        public bool LED_RISK_REDUCE_FLAG { get; set; }

        public static explicit operator T_LED_WORK_RISK_REDUCE(LedgerRiskReduceColumn model)
        {
            return new T_LED_WORK_RISK_REDUCE()
            {
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                LED_RISK_REDUCE_ID = model.LED_RISK_REDUCE_ID,
                LED_RISK_REDUCE_TYPE = model.LED_RISK_REDUCE_TYPE,
                LED_RISK_REDUCE_FLAG = model.LED_RISK_REDUCE_FLAG,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LED_HISTORY_RISK_REDUCE(LedgerRiskReduceColumn model)
        {
            return new T_LED_HISTORY_RISK_REDUCE()
            {
                LEDGER_HISTORY_ID = model.LEDGER_HISTORY_ID,
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                LED_RISK_REDUCE_ID = model.LED_RISK_REDUCE_ID,
                LED_RISK_REDUCE_TYPE = model.LED_RISK_REDUCE_TYPE,
                LED_RISK_REDUCE_FLAG = model.LED_RISK_REDUCE_FLAG,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LED_RISK_REDUCE(LedgerRiskReduceColumn model)
        {
            return new T_LED_RISK_REDUCE()
            {
                REG_NO = model.REG_NO,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                LED_RISK_REDUCE_ID = model.LED_RISK_REDUCE_ID,
                LED_RISK_REDUCE_TYPE = model.LED_RISK_REDUCE_TYPE,
                LED_RISK_REDUCE_FLAG = model.LED_RISK_REDUCE_FLAG,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
            };
        }
    }
}