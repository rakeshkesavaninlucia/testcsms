﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class LedgerLocationColumn : SystemColumn
    {
        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Display(Name = "保管場所シーケンス（連番）")]
        public int? LOC_SEQ { get; set; }

        [Display(Name = "場所ID")]
        public int? LOC_ID { get; set; }

        [Display(Name = "最大保管量")]
        public string AMOUNT { get; set; }

        [Display(Name = "単位")]
        public int? UNITSIZE_ID { get; set; }

        public static explicit operator T_LED_WORK_LOC(LedgerLocationColumn model)
        {
            return new T_LED_WORK_LOC()
            {
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                LOC_SEQ = model.LOC_SEQ,
                LOC_ID = model.LOC_ID,
                AMOUNT = model.AMOUNT,
                UNITSIZE_ID = model.UNITSIZE_ID,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LED_HISTORY_LOC(LedgerLocationColumn model)
        {
            return new T_LED_HISTORY_LOC()
            {
                LEDGER_HISTORY_ID = model.LEDGER_HISTORY_ID,
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                LOC_SEQ = model.LOC_SEQ,
                LOC_ID = model.LOC_ID,
                AMOUNT = model.AMOUNT,
                UNITSIZE_ID = model.UNITSIZE_ID,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_MGMT_LED_LOC(LedgerLocationColumn model)
        {
            return new T_MGMT_LED_LOC()
            {
                REG_NO = model.REG_NO,
                LOC_SEQ = model.LOC_SEQ,
                LOC_ID = model.LOC_ID,
                AMOUNT = model.AMOUNT,
                UNITSIZE_ID = model.UNITSIZE_ID,
                REG_DATE = model.REG_DATE,
            };
        }
    }
}