﻿using ChemMgmt.Classes;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon
{
    public class WorkflowDataInfo : DataInfoBase<M_WORKFLOW>
    {
        protected override string TableName
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override void EditData(M_WORKFLOW Value)
        {
            throw new NotImplementedException();
        }

        public override List<M_WORKFLOW> GetSearchResult(M_WORKFLOW condition)
        {
            throw new NotImplementedException();
        }

        //public override IQueryable<M_WORKFLOW> GetSearchResult(M_WORKFLOW condition)
        //{
        //    throw new NotImplementedException();
        //}

        /// <summary>
        /// ワークフローを取得する
        /// </summary>
        /// <param name="FLOW_CD">フローコード</param>
        /// <returns>ワークフロー</returns>
        public M_WORKFLOW GetWorkflow(string FLOW_CD)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select * from " + nameof(M_WORKFLOW));
            sql.AppendLine(" where " + nameof(M_WORKFLOW.FLOW_CD) + " = @" + nameof(M_USER.FLOW_CD));
            var parameters = new DynamicParameters();
            parameters.Add(nameof(M_WORKFLOW.FLOW_CD), FLOW_CD);
            //var context = Classes.util.Open(true);
            List<M_WORKFLOW> records = DbUtil.Select<M_WORKFLOW>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            //context.Close();
            foreach (DataRow record in dt.Rows)
            {
                var result = new M_WORKFLOW();
                result.SetDataRow(record);
                return result;
            }
            return null;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }


        protected override void CreateInsertText(M_WORKFLOW value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }
       

        protected override void CreateUpdateText(M_WORKFLOW value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            throw new NotImplementedException();
        }

        protected override void InitializeDictionary()
        {
        }

        

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters param)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(M_WORKFLOW value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(M_WORKFLOW value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}