﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class M_ISHA_USAGE : SystemColumn
    {
        /// <summary>
        /// 用途ID
        /// </summary>
        public int ISHA_USAGE_ID { get; set; }

        /// <summary>
        /// 用途
        /// </summary>
        public string ISHA_USAGE_TEXT { get; set; }


        
        /// <summary>
        /// 削除フラグ
        /// </summary>
        public int? ISHA_USAGE_ORDER { get; set; } = 0;
    }
}