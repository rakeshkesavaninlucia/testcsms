﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    /// <summary>
    /// 化学物質別名マスター
    /// </summary>
    public class M_CHEM_ALIAS : SystemColumn
    {
        /// <summary>
        /// 化学物質コード
        /// </summary>
        [Key]
        public string CHEM_CD { get; set; }

        /// <summary>
        /// 商品シーケンス
        /// </summary>
        [Key]
        public int CHEM_NAME_SEQ { get; set; }

        /// <summary>
        /// 化学物質名
        /// </summary>
        [Display(Name = "化学物質別名")]
        [StringLength(255)]
        [Required]
        [ExcludingProhibition]
        public string CHEM_NAME { get; set; }
    }
}