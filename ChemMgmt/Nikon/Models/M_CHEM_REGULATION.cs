﻿using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public enum ClassIdType
    {
        /// <summary>
        /// 法規制
        /// </summary>
        Regulation = 1,

        /// <summary>
        /// GHS分類
        /// </summary>
        GhsCategory = 2,

        //2019/03/11 TPE.Rin Add
        /// <summary>
        /// 消防法
        /// </summary>
        Fire = 3,

        /// <summary>
        /// 社内ルール
        /// </summary>
        Office = 4

    }
    /// <summary>
    /// 化学物質法規制マスター
    /// </summary>
    public class M_CHEM_REGULATION : SystemColumn
    {
        public M_CHEM_REGULATION() { }

        public M_CHEM_REGULATION(string chemCode, int regulationTypeId)
        {
            CHEM_CD = chemCode;
            REG_TYPE_ID = regulationTypeId;
        }

        public M_CHEM_REGULATION(string chemCode, M_REGULATION regulation)
        {
            CHEM_CD = chemCode;
            REG_TYPE_ID = regulation.REG_TYPE_ID;
            SORT_ORDER = regulation.SORT_ORDER;
        }

        public M_CHEM_REGULATION(string chemCode, V_REGULATION regulation)
        {
            CHEM_CD = chemCode;
            REG_TYPE_ID = regulation.REG_TYPE_ID;
            SORT_ORDER = regulation.SORT_ORDER;
        }

        //2019/04/05 TPE.Rin Add Start
        //手入力区分が増えた
        public M_CHEM_REGULATION(string chemCode, int regulationTypeId, string inputflag)
        {
            CHEM_CD = chemCode;
            REG_TYPE_ID = regulationTypeId;
            INPUT_FLAG = inputflag;
        }
        //2019/04/05 TPE.Rin Add End


        /// <summary>
        /// 化学物質コード
        /// </summary>
        [Key]
        public string CHEM_CD { get; set; }

        /// <summary>
        /// 法規制種類ID
        /// </summary>
        [Key]
        public int REG_TYPE_ID { get; set; }

        /// <summary>
        /// クラスID
        /// </summary>
        public int? CLASS_ID { get; set; }

        /// <summary>
        /// 表示順
        /// </summary>
        public int? SORT_ORDER { get; set; }

        //2019/03/18 TPE.Rin Add Start
        /// <summary>
        /// 手入力フラグ
        /// </summary>
        public string INPUT_FLAG { get; set; }
        //2019/03/18 TPE.Rin Add End
    }
}