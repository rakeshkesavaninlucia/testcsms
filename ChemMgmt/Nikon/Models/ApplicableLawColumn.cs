﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class ApplicableLawColumn : SystemColumn
    {
        [Display(Name = "法規制種類ID")]
        public int? REG_TYPE_ID { get; set; }

        [Display(Name = "法規制・注意事項情報")]
        public string REG_TEXT { get; set; }

        [Display(Name = "適用法令")]
        public string REG_TEXT_BASE { get; set; }

        [Display(Name = "適用法令詳細")]
        public string REG_TEXT_LAST { get; set; }

        [Display(Name = "法規制・注意事項用アイコンのファイル名")]
        public string REG_ICON_PATH { get; set; }

        [Display(Name = "CAS")]
        public string CAS_NO { get; set; }

        [Display(Name = "使用条件")]
        public int? ISHA_USAGE { get; set; }

        [Display(Name = "指定数量")]
        public decimal FIRE_SIZE { get; set; }

        [Display(Name = "作業記録管理")]
        public int? WORKTIME_MGMT { get; set; }

        [Display(Name = "点数")]
        public int? MARKS { get; set; }

        [Display(Name = "リスクアセスメント対象")]
        public int? RA_FLAG { get; set; }

        public static explicit operator V_APPLICABLE_LAW(ApplicableLawColumn model)
        {
            return new V_APPLICABLE_LAW()
            {
                REG_TYPE_ID = model.REG_TYPE_ID,
                REG_TEXT = model.REG_TEXT,
                REG_TEXT_BASE = model.REG_TEXT_BASE,
                REG_TEXT_LAST = model.REG_TEXT_LAST,
                CAS_NO = model.CAS_NO,
                ISHA_USAGE = model.ISHA_USAGE,
                MARKS = model.MARKS,
                RA_FLAG = model.RA_FLAG,
            };
        }
    }
}