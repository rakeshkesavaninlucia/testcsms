﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    /// <summary>
    /// 化学物質マスター
    /// </summary>
    public class M_CHEM : SystemColumn
    {
        /// <summary>
        /// 化学物質コード
        /// </summary>
        [Key]
        [Display(Name = "化学物質コード")]
        public string CHEM_CD { get; set; }

        /// <summary>
        /// 化学物質名
        /// </summary>
        [Display(Name = "化学物質名")]
        [StringLength(255)]
        [Required]
        [ExcludingProhibition]
        [ChemUnique]//2018/08/08 TPE.Sugimoto Add
        public string CHEM_NAME { get; set; }

        /// <summary>
        /// 分類
        /// </summary>
        [Display(Name = "分類")]
        [StringLength(100)]
        [Required]
        [ExcludingProhibition]
        public string CHEM_CAT { get; set; }

        /// <summary>
        /// 密度
        /// </summary>
        public decimal? DENSITY { get; set; }

        /// <summary>
        /// 密度
        /// </summary>
        [Display(Name = "比重")]
        [Halfwidth]
        [Required]
        [RegularNumber]
        [NumberLength(4, 3)]
        public string DensityString { get; set; }

        /// <summary>
        /// 引火点
        /// </summary>
        public decimal? FLASH_POINT { get; set; }

        /// <summary>
        /// 密度
        /// </summary>
        [Display(Name = "引火点")]
        [Halfwidth]
        [Number]
        [NumberLength(3, 1)]
        public string FlashPointString { get; set; }

        /// <summary>
        /// 開封後有効期限
        /// </summary>
        [Display(Name = "開封後有効期限")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string EXPIRATION_DATE { get; set; }

        /// <summary>
        /// 鍵管理
        /// </summary>
        [Display(Name = "鍵管理")]
        [Required]
        public int? KEY_MGMT { get; set; }

        /// <summary>
        /// 重量管理
        /// </summary>
        [Display(Name = "重量管理")]
        [Required]
        public int? WEIGHT_MGMT { get; set; }

        /// <summary>
        /// 形状
        /// </summary>
        [Display(Name = "形状")]
        [Required]
        public int? FIGURE { get; set; }

        /// <summary>
        /// CAS#
        /// </summary>
        [Display(Name = "CAS#")]
        [StringLength(60)]
        [Halfwidth]
        [ExcludingProhibition]
        public string CAS_NO { get; set; }

        /// <summary>
        /// 備考
        /// </summary>
        [Display(Name = "備考")]
        [StringLength(1000)]
        [ExcludingProhibition]
        public string MEMO { get; set; }

        /// <summary>
        /// 任意項目1
        /// </summary>
        [Display(Name = "任意項目1")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string OPTIONAL1 { get; set; }

        /// <summary>
        /// 任意項目2
        /// </summary>
        [Display(Name = "任意項目2")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string OPTIONAL2 { get; set; }

        /// <summary>
        /// 任意項目3
        /// </summary>
        [Display(Name = "任意項目3")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string OPTIONAL3 { get; set; }

        /// <summary>
        /// 任意項目4
        /// </summary>
        [Display(Name = "任意項目4")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string OPTIONAL4 { get; set; }

        /// <summary>
        /// 任意項目5
        /// </summary>
        [Display(Name = "任意項目5")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string OPTIONAL5 { get; set; }

        // 20180723 FJ)Shinagawa Add Start ->
        /// <summary>
        /// 変更中化学物質コード
        /// </summary>
        [Display(Name = "変更中化学物質コード")]
        public string REF_CHEM_CD { get; set; }
        // 20180723 FJ)Shinagawa Add End <-

        //2019/03/21 TPE.Rin Add Start
        /// <summary>
        /// 消防法非該当フラグ
        /// </summary>
        [Display(Name = "消防法非該当フラグ")]
        public bool FIRE_FLAG { get; set; }

        public string FIREFLAG { get; set; }

        [StringLength(100)]
        public string REGULATION { get; set; }

        [StringLength(100)]
        public string REGULATION_Division { get; set; }

        [StringLength(100)]
        public string Fire { get; set; }
        [StringLength(100)]
        public string Fire_Division { get; set; }

        [StringLength(100)]
        public string Office { get; set; }
        [StringLength(100)]
        public string Office_Division { get; set; }
        [StringLength(100)]
        public string GHS { get; set; }
        [StringLength(100)]
        public string GHS_Division { get; set; }

    }
}