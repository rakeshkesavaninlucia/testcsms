﻿using System.ComponentModel.DataAnnotations;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class T_MGMT_LED_REG_OLD_NO : SystemColumn
    {
        [Key]
        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Key]
        [Display(Name = "旧安全衛生番号シーケンス（連番）")]
        public int? REG_OLD_NO_SEQ { get; set; }

        [Display(Name = "旧安全衛生番号")]
        public string REG_OLD_NO { get; set; }

    }
}