﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Models
{
    public class LedgerAccesmentRegColumn : SystemColumn
    {
        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Display(Name = "使用場所シーケンス（連番）")]
        public int? USAGE_LOC_SEQ { get; set; }

        [Display(Name = "管理台帳法規制ID")]
        public int? LED_ACCESMENT_REG_ID { get; set; }

        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Display(Name = "CAS")]
        public string CAS_NO { get; set; }

        [Display(Name = "法規制種類ID")]
        public int? REG_TYPE_ID { get; set; }

        [Display(Name = "法規制・注意事項情報")]
        public string REG_TEXT { get; set; }

        [Display(Name = "適用法令詳細")]
        public string REG_TEXT_LAST { get; set; }

        [Display(Name = "法規制・注意事項用アイコンのファイル名")]
        public string REG_ICON_PATH { get; set; }

        [Display(Name = "指定数量")]
        public decimal FIRE_SIZE { get; set; }

        [Display(Name = "作業記録管理")]
        public int? WORKTIME_MGMT { get; set; }

        public static explicit operator T_LED_WORK_ACCESMENT_REG(LedgerAccesmentRegColumn model)
        {
            return new T_LED_WORK_ACCESMENT_REG()
            {
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                LED_ACCESMENT_REG_ID = model.LED_ACCESMENT_REG_ID,
                REG_TYPE_ID = model.REG_TYPE_ID,
                REG_TEXT = model.REG_TEXT,
                REG_TEXT_LAST = model.REG_TEXT_LAST,
                REG_ICON_PATH = model.REG_ICON_PATH,
                CAS_NO = model.CAS_NO,
                FIRE_SIZE = model.FIRE_SIZE,
                WORKTIME_MGMT = model.WORKTIME_MGMT,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LED_HISTORY_ACCESMENT_REG(LedgerAccesmentRegColumn model)
        {
            return new T_LED_HISTORY_ACCESMENT_REG()
            {
                LEDGER_HISTORY_ID = model.LEDGER_HISTORY_ID,
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                LED_ACCESMENT_REG_ID = model.LED_ACCESMENT_REG_ID,
                REG_TYPE_ID = model.REG_TYPE_ID,
                REG_TEXT = model.REG_TEXT,
                REG_TEXT_LAST = model.REG_TEXT_LAST,
                REG_ICON_PATH = model.REG_ICON_PATH,
                CAS_NO = model.CAS_NO,
                FIRE_SIZE = model.FIRE_SIZE,
                WORKTIME_MGMT = model.WORKTIME_MGMT,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LED_ACCESMENT_REG(LedgerAccesmentRegColumn model)
        {
            return new T_LED_ACCESMENT_REG()
            {
                REG_NO = model.REG_NO,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                LED_ACCESMENT_REG_ID = model.LED_ACCESMENT_REG_ID,
                REG_TYPE_ID = model.REG_TYPE_ID,
                REG_TEXT = model.REG_TEXT,
                REG_TEXT_LAST = model.REG_TEXT_LAST,
                REG_ICON_PATH = model.REG_ICON_PATH,
                CAS_NO = model.CAS_NO,
                FIRE_SIZE = model.FIRE_SIZE,
                WORKTIME_MGMT = model.WORKTIME_MGMT,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
            };
        }
    }
}