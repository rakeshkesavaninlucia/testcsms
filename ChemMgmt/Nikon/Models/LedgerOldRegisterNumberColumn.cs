﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class LedgerOldRegisterNumberColumn : SystemColumn
    {
        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Display(Name = "旧安全衛生番号シーケンス（連番）")]
        public int? REG_OLD_NO_SEQ { get; set; }

        [Display(Name = "旧安全衛生番号")]
        public string REG_OLD_NO { get; set; }

        public static explicit operator T_LED_WORK_REG_OLD_NO(LedgerOldRegisterNumberColumn model)
        {
            return new T_LED_WORK_REG_OLD_NO()
            {
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                REG_OLD_NO_SEQ = model.REG_OLD_NO_SEQ,
                REG_OLD_NO = model.REG_OLD_NO,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LED_HISTORY_REG_OLD_NO(LedgerOldRegisterNumberColumn model)
        {
            return new T_LED_HISTORY_REG_OLD_NO()
            {
                LEDGER_HISTORY_ID = model.LEDGER_HISTORY_ID,
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                REG_OLD_NO_SEQ = model.REG_OLD_NO_SEQ,
                REG_OLD_NO = model.REG_OLD_NO,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_MGMT_LED_REG_OLD_NO(LedgerOldRegisterNumberColumn model)
        {
            return new T_MGMT_LED_REG_OLD_NO()
            {
                REG_NO = model.REG_NO,
                REG_OLD_NO_SEQ = model.REG_OLD_NO_SEQ,
                REG_OLD_NO = model.REG_OLD_NO,
                REG_DATE = model.REG_DATE,
            };
        }

    }
}