﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class T_LED_HISTORY_REG_OLD_NO : SystemColumn
    {
        [Key]
        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Key]
        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Key]
        [Display(Name = "旧安全衛生番号シーケンス（連番）")]
        public int? REG_OLD_NO_SEQ { get; set; }

        [Display(Name = "旧安全衛生番号")]
        public string REG_OLD_NO { get; set; }

    }
}