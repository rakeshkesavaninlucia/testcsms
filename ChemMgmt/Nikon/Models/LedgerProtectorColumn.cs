﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class LedgerProtectorColumn : SystemColumn
    {
        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Display(Name = "使用場所シーケンス（連番）")]
        public int? USAGE_LOC_SEQ { get; set; }

        [Display(Name = "保護具シーケンス（連番）")]
        public int? PROTECTOR_SEQ { get; set; }

        [Display(Name = "保護具ID")]
        public int? PROTECTOR_ID { get; set; }

        public static explicit operator T_LED_WORK_PROTECTOR(LedgerProtectorColumn model)
        {
            return new T_LED_WORK_PROTECTOR()
            {
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                PROTECTOR_SEQ = model.PROTECTOR_SEQ,
                PROTECTOR_ID = model.PROTECTOR_ID,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LED_HISTORY_PROTECTOR(LedgerProtectorColumn model)
        {
            return new T_LED_HISTORY_PROTECTOR()
            {
                LEDGER_HISTORY_ID = model.LEDGER_HISTORY_ID,
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                PROTECTOR_SEQ = model.PROTECTOR_SEQ,
                PROTECTOR_ID = model.PROTECTOR_ID,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_MGMT_LED_PROTECTOR(LedgerProtectorColumn model)
        {
            return new T_MGMT_LED_PROTECTOR()
            {
                REG_NO = model.REG_NO,
                USAGE_LOC_SEQ = model.USAGE_LOC_SEQ,
                PROTECTOR_SEQ = model.PROTECTOR_SEQ,
                PROTECTOR_ID = model.PROTECTOR_ID,
                REG_DATE = model.REG_DATE,
            };
        }
    }
}