﻿using ChemMgmt.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class LedgerRegulationColumn : M_REGULATION
    {
        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Key]
        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        [Display(Name = "履歴ID")]
        public int? LEDGER_HISTORY_ID { get; set; }

        [Display(Name = "管理台帳法規制ID")]
        public int? LED_REGULATION_ID { get; set; }

        //[Display(Name = "CASリスト")]
        //public string CAS_NO_LIST { get; set; }

        [Display(Name = "使用場所ID")]
        public int LOC_ID { get; set; }

        /// <summary>
        /// 法規制名(カテゴリ)
        /// </summary>
        public string REG_TEXT_BASE { get; set; }

        /// <summary>
        /// 法規制名(名称)
        /// </summary>
        public string REG_TEXT_LAST { get; set; }

        /// <summary>
        /// アイコンURL
        /// </summary>
        public string IconUrl
        {
            get
            {
                return !string.IsNullOrWhiteSpace(REG_ICON_PATH) ? "../" + SystemConst.RegulationImageFolderName + "/" + REG_ICON_PATH : null;
            }
        }

        public static explicit operator T_LEDGER_WORK_REGULATION(LedgerRegulationColumn model)
        {
            return new T_LEDGER_WORK_REGULATION()
            {
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                LED_REGULATION_ID = model.LED_REGULATION_ID,
                REG_TYPE_ID = model.REG_TYPE_ID,
                REG_TEXT = model.REG_TEXT,
                //CAS_NO_LIST = model.CAS_NO_LIST,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LEDGER_HISTORY_REGULATION(LedgerRegulationColumn model)
        {
            return new T_LEDGER_HISTORY_REGULATION()
            {
                LEDGER_HISTORY_ID = model.LEDGER_HISTORY_ID,
                LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                LED_REGULATION_ID = model.LED_REGULATION_ID,
                REG_TYPE_ID = model.REG_TYPE_ID,
                REG_TEXT = model.REG_TEXT,
                //CAS_NO_LIST = model.CAS_NO_LIST,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
            };
        }

        public static explicit operator T_LED_REGULATION(LedgerRegulationColumn model)
        {
            return new T_LED_REGULATION()
            {
                REG_NO = model.REG_NO,
                LED_REGULATION_ID = model.LED_REGULATION_ID,
                REG_TYPE_ID = model.REG_TYPE_ID,
                REG_TEXT = model.REG_TEXT,
                //CAS_NO_LIST = model.CAS_NO_LIST,
                REG_USER_CD = model.REG_USER_CD,
                REG_DATE = model.REG_DATE,
            };
        }
    }
}