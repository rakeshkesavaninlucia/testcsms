﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon.History;
using System;
using System.ComponentModel.DataAnnotations;

namespace ZyCoaG.Nikon
{
    public class T_LED_WORK_HISTORY : SystemColumn
    {
        /// <summary>
        /// 履歴ID"
        /// </summary>
        [Key]
        [Display(Name = "履歴ID")]
        public int? LED_WORK_HISTORY_ID { get; set; }

        /// <summary>
        /// フローID（申請番号）
        /// </summary>
        [Display(Name = "フローID（申請番号）")]
        public int? LEDGER_FLOW_ID { get; set; }

        public int FLOW_HIERARCHY { get; set; }

        /// <summary>
        /// フローコード
        /// </summary>
        [Display(Name = "フローコード")]
        public string FLOW_CD { get; set; }

        /// <summary>
        /// 申請者コード
        /// </summary>
        [Display(Name = "申請者コード")]
        public string REGISTER_USER_CD { get; set; }

        /// <summary>
        /// フロー階層
        /// </summary>
        [Display(Name = "フロー階層")]
        public int? HIERARCHY { get; set; }

        /// <summary>
        /// 承認目的
        /// </summary>
        [Display(Name = "承認目的")]
        public string APPROVAL_TARGET { get; set; }

        /// <summary>
        /// 承認者コード
        /// </summary>
        [Display(Name = "承認者コード")]
        public string APPROVAL_USER_CD { get; set; }

        /// <summary>
        /// 承認者名
        /// </summary>
        [Display(Name = "承認者名")]
        public string APPROVAL_USER_NAME { get; set; }

        /// <summary>
        /// 代行者コード
        /// </summary>
        [Display(Name = "代行者コード")]
        public string SUBSITUTE_USER_CD { get; set; }

        /// <summary>
        /// 代行者名
        /// </summary>
        [Display(Name = "代行者名")]
        public string SUBSITUTE_USER_NAME { get; set; }

        /// <summary>
        /// 操作者コード
        /// </summary>
        [Display(Name = "操作者コード")]
        public string ACTUAL_USER_CD { get; set; }

        /// <summary>
        /// 操作者名
        /// </summary>
        [Display(Name = "操作者名")]
        public string ACTUAL_USER_NAME { get; set; }

        /// <summary>
        /// ワークフロー操作者コード
        /// </summary>
        [Display(Name = "ワークフロー操作者コード")]
        public string WORKFLOW_USER_CD
        {
            get
            {
                return string.IsNullOrEmpty(ACTUAL_USER_CD) ? APPROVAL_USER_CD : ACTUAL_USER_CD;
            }
        }

        /// <summary>
        /// ワークフロー操作者名
        /// </summary>
        [Display(Name = "ワークフロー操作者名")]
        public string WORKFLOW_USER_NAME
        {
            get
            {
                return string.IsNullOrEmpty(ACTUAL_USER_CD) ? APPROVAL_USER_NAME : ACTUAL_USER_NAME;
            }
        }

        /// <summary>
        /// 結果
        /// </summary>
        [Display(Name = "結果")]
        public int? RESULT { get; set; }

        /// <summary>
        /// 結果
        /// </summary>
        [Display(Name = "結果")]
        public string RESULT_TEXT
        {
            get
            {
                switch (RESULT)
                {
                    case (int)WorkflowResult.Application:
                        return "申請";
                    case (int)WorkflowResult.Approval:
                        return "承認";
                    case (int)WorkflowResult.Remand:
                        return "差戻";
                    case (int)WorkflowResult.Regained:
                        return "取戻";
                    default:
                        return "";
                }
            }
        }

        /// <summary>
        /// コメント
        /// </summary>
        [Display(Name = "コメント")]
        [StringLength(1000)]
        [ExcludingProhibition]
        public string MEMO { get; set; } = " ";

        /// <summary>
        /// 操作日時
        /// </summary>
        [Display(Name = "操作日時")]
        public DateTime? ACTUAL_DATE
        {
            get
            {
                if (RESULT == null)
                {
                    return null;
                }
                else if (RESULT == (int)WorkflowResult.Application && string.IsNullOrWhiteSpace(UPD_DATE.ToString()))
                {
                    return REG_DATE;
                }
                else
                {
                    return UPD_DATE;
                }
            }
        }
    }
}