﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace ZyCoaG.Nikon
{
    public class V_CAS_REGULATION
    {
        /// <summary>
        /// 法規制種類ID
        /// </summary>
        public int? REG_TYPE_ID { get; set; }

        /// <summary>
        /// 行番号
        /// </summary>
        public int? ROW_ID { get; set; }

        /// <summary>
        /// CAS#
        /// </summary>
        [StringLength(60)]
        public string casno { get; set; }

        /// <summary>
        /// 最大値
        /// </summary>
        public decimal? maximum { get; set; }

        /// <summary>
        /// 最小値
        /// </summary>
        public decimal? minimum { get; set; }

        /// <summary>
        /// 形状
        /// </summary>
        public string shape { get; set; }

        /// <summary>
        /// 形状のキー
        /// </summary>
        public string shapekey { get; set; }

        /// <summary>
        /// 使用条件
        /// </summary>
        public string terms { get; set; }

        /// <summary>
        /// 使用条件のキー
        /// </summary>
        public string termskey { get; set; }

    }
}