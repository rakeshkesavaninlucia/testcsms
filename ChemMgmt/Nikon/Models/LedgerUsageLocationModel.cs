﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon.Ledger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public class LedgerUsageLocationModel : LedgerUsageLocationColumn
    {
        public int _Mode { get; set; }

        public Mode Mode
        {
            get
            {
                Mode resultMode;
                if (Mode.TryParse(_Mode.ToString(), out resultMode)) return resultMode;
                return default(Mode);
            }
            set
            {
                _Mode = (int)value;
            }
        }

        public LedgerUsageLocationModel() { }

        /// <summary>
        /// その他の保護具のIDを定義します。
        /// </summary>
        public static int OTHER_PROTECTOR_ID { get; } = 6;

        /// <summary>
        /// その他の変更・廃止の事由IDを定義します。
        /// </summary>
        public static int OTHER_ISHA_USAGE { get; } = 18;

        /// <summary>
        /// 行識別ID
        /// </summary>
        public string RowId { get; set; } = Guid.NewGuid().ToString();

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

        public string LOC_NAME { get; set; }

        public string UNITSIZE
        {
            get
            {
                return new UnitSizeMasterInfo().GetSelectList().FirstOrDefault(x => x.Id == UNITSIZE_ID)?.Name;
            }
        }

        public List<LedgerProtectorModel> objLedgerProtectorModel { get; set; }

        public List<LedgerRiskReduceModel> objLedgerRisk { get; set; }

        public IEnumerable<SelectListItem> LookupChemNameCondition { get; set; }
        public IEnumerable<SelectListItem> LookupProductName { get; set; }
        public IEnumerable<SelectListItem> LookupCas { get; set; }
        public IEnumerable<SelectListItem> LookupRegistrationNo { get; set; }
        public IEnumerable<SelectListItem> LookupState { get; set; }
        public IEnumerable<SelectListItem> LookupReason { get; set; }
        public IEnumerable<SelectListItem> LookupREG_DISASTER_POSSIBILITY { get; set; }
        public IEnumerable<SelectListItem> LookupREG_WORK_FREQUENCY { get; set; }
        public IEnumerable<SelectListItem> LookupREG_TRANSACTION_VOLUME { get; set; }
        public IEnumerable<SelectListItem> LookupSUB_CONTAINER { get; set; }
        public IEnumerable<SelectListItem> LookupUnitSizeMasterInfo { get; set; }
        public IEnumerable<SelectListItem> LookupUsageMasterInfo { get; set; }
        public IEnumerable<SelectListItem> LookupManufacturerName { get; set; }

        public string MEASURES_BEFORE_SCORE_UNIT { get; set; }
        public string MEASURES_BEFORE_RANK_DISP { get; set; }
        public string MEASURES_AFTER_RANK_DISP { get; set; }
        public string MEASURES_AFTER_SCORE_UNIT { get; set; }
        public bool FLG_UNREGISTERED_CHEM { get; set; }

        public string StatusId { get; set; }
        public string hiddenRegulationIds { get; set; }
        public string managementgroupIds { get; set; }
        public string hiddenLocationIds { get; set; }
        public string hiddenusageIds { get; set; }
        public string hiddenstoragelocationIds { get; set; }
        public string hiddengroupIds { get; set; }

        public string AMOUNTWITHUNITSIZE
        {
            get
            {
                return AMOUNT + UNITSIZE;
            }
        }

        public string CHEM_OPERATION_CHIEF_NAME { get; set; }

        //2018/08/13 TPE Add
        public string CHEM_OPERATION_CHIEF_CD { get; set; }

        public string PROTECTOR_TEXT
        {
            get
            {
                if (PROTECTOR_COLLECTION == null)
                {
                    return "";
                }
                else
                {
                    return string.Join(", ", PROTECTOR_COLLECTION.Where(x => x.CHECK).Select(x => x.PROTECTOR_NAME));
                }
            }
        }

        public string PROTECTOR_COUNT
        {
            get
            {
                if (PROTECTOR_COLLECTION != null)
                {
                    return PROTECTOR_COLLECTION.Where(x => x.CHECK).Count().ToString();
                }
                else
                {
                    return string.Empty;
                }
            }
        }

        public string ISHA_USAGE_TEXT { get; set; }

        public string SUB_CONTAINER_TEXT { get; set; }

        public string MEASURES_BEFORE_RANK_TEXT { get; set; }

        public string MEASURES_AFTER_RANK_TEXT { get; set; }

        public IEnumerable<LedgerProtectorModel> PROTECTOR_COLLECTION { get; set; } = new List<LedgerProtectorModel>();

        public string REG_TRANSACTION_VOLUME_TEXT { get; set; }

        public string REG_WORK_FREQUENCY_TEXT { get; set; }

        public string REG_DISASTER_POSSIBILITY_TEXT { get; set; }

        public IEnumerable<LedgerRiskReduceModel> RISK_REDUCE_COLLECTION { get; set; } = new List<LedgerRiskReduceModel>();

        public IEnumerable<LedgerAccesmentRegModel> ACCESMENT_REG_COLLECTION { get; set; } = new List<LedgerAccesmentRegModel>();

        /// <summary>
        /// 削除されているか
        /// </summary>
        public bool IsShowDeleted
        {
            get
            {
                if (DEL_FLAG == (int)ZyCoaG.DEL_FLAG.Deleted) return true;
                return false;
            }
        }

        /// <summary>
        /// 労働安全衛生法の用途が読取のみか
        /// </summary>
        public bool IsOtherIshaUsageReadonly
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                        return ISHA_USAGE != OTHER_ISHA_USAGE;
                    default:
                        return true;
                }
            }
        }
    }

    public class LedgerUsageLocationSearchModel : LedgerUsageLocationModel
    {
        public int? MaxSearchResult { get; set; }

        public LedgerUsageLocationSearchModel()
        {
            SearchLocationList = new List<LedgerUsageLocationModel>();
        }

        public IEnumerable<LedgerUsageLocationModel> SearchLocationList;

    }
}