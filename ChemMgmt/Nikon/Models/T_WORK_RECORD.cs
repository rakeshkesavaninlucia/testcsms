﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZyCoaG.Nikon
{
    public class T_WORK_RECORD : SystemColumn
    {
        public long WORK_RECORD_ID { get; set; }

        public int? ACTION_ID { get; set; }

        public DateTime? ISSUE_DATE { get; set; }

        public DateTime? RECEIPT_DATE { get; set; }

        [Display(Name = "瓶番号")]
        public string BARCODE { get; set; }

        public int? EXPOSURE_WORK_USE { get; set; }

        public int? EXPOSURE_WORK_TYPE { get; set; }

        public int? EXPOSURE_WORK_TEMPERATURE { get; set; }

        public decimal? UNITSIZE { get; set; }

        public int? UNITSIZE_ID { get; set; }

        public string NOTICES { get; set; }

        public string REASON_FOR_UPDATE { get; set; }

        public ICollection<M_USER> Workers { get; set; }
    }
}