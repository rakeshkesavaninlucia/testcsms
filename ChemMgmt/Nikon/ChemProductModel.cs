﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;
using Dapper;
using System.Text;
using COE.Common;
using System.Web.Mvc;
using System.Collections;

namespace ChemMgmt.Nikon
{
    public class ChemProductModel : M_CHEM_PRODUCT
    {
        
        public int _Mode { get; set; }
        /// <summary>
        /// 編集モード
        /// </summary>
        public Mode Mode
        {
            get
            {
                Mode resultMode;
                if (Mode.TryParse(_Mode.ToString(), out resultMode)) return resultMode;
                return default(Mode);
            }
            set
            {
                _Mode = (int)value;
            }
        }

        public ChemProductModel() {

        }
       

        public ChemProductModel(ChemModel value)
        {
            CHEM_CD = value.CHEM_CD;
            PRODUCT_SEQ = (int)value.PRODUCT_SEQ;
            PRODUCT_NAME = value.PRODUCT_NAME;
            MAKER_ID = value.MAKER_ID;
            UNITSIZE = value.UNITSIZE;
            UNITSIZE_ID = value.UNITSIZE_ID;
            PRODUCT_BARCODE = value.PRODUCT_BARCODE;
            SDS_URL = value.SDS_URL;
            if (!string.IsNullOrEmpty(value.SDS_UPD_DATE))
            {
                SDS_UPD_DATE = Convert.ToDateTime(value.SDS_UPD_DATE);
            }
            SdsUpdateDate = value.SdsUpdateDate;
        }

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }


        /// <summary>
        /// ボタンを表示または有効にするか
        /// </summary>
        public bool IsButtonVisibleOrEnabled
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }

    public class ChemProductSearchModel : ChemProductModel
    {

    }
}

namespace ZyCoaG.Nikon.Chem
{
    public class ChemProductDataInfo : DataInfoBase<ChemProductModel>, IDisposable
    {
        protected override string TableName { get; } = nameof(M_CHEM_PRODUCT);

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            throw new NotImplementedException();
        }

        protected override string WhereWardCreate(dynamic dynamiCondition, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        public override List<ChemProductModel> GetSearchResult(ChemProductModel condition)
        {
            throw new NotImplementedException();
        }

        protected override void InitializeDictionary() { }

        public override void EditData(ChemProductModel Value)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(ChemProductModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            insertSql.AppendLine("insert into ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  " + nameof(M_CHEM_PRODUCT.CHEM_CD) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_PRODUCT.PRODUCT_SEQ) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_PRODUCT.PRODUCT_NAME) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_PRODUCT.MAKER_ID) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_PRODUCT.UNITSIZE) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_PRODUCT.UNITSIZE_ID) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_PRODUCT.PRODUCT_BARCODE) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_PRODUCT.SDS_URL) + ",");
            insertSql.AppendLine("  " + nameof(M_CHEM_PRODUCT.SDS_UPD_DATE));
            insertSql.AppendLine(" )");
            insertSql.AppendLine("values ");
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  @" + nameof(M_CHEM_PRODUCT.CHEM_CD) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_PRODUCT.PRODUCT_SEQ) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_PRODUCT.PRODUCT_NAME) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_PRODUCT.MAKER_ID) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_PRODUCT.UNITSIZE) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_PRODUCT.UNITSIZE_ID) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_PRODUCT.PRODUCT_BARCODE) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_PRODUCT.SDS_URL) + ",");
            insertSql.AppendLine("  @" + nameof(M_CHEM_PRODUCT.SDS_UPD_DATE));
            insertSql.AppendLine(" )");
            parameters.Add(nameof(M_CHEM_PRODUCT.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(M_CHEM_PRODUCT.PRODUCT_SEQ), value.PRODUCT_SEQ);
            parameters.Add(nameof(M_CHEM_PRODUCT.PRODUCT_NAME), value.PRODUCT_NAME);
            parameters.Add(nameof(M_CHEM_PRODUCT.MAKER_ID), value.MAKER_ID);
            parameters.Add(nameof(M_CHEM_PRODUCT.UNITSIZE), value.UNITSIZE);
            parameters.Add(nameof(M_CHEM_PRODUCT.UNITSIZE_ID), value.UNITSIZE_ID);
            parameters.Add(nameof(M_CHEM_PRODUCT.PRODUCT_BARCODE), value.PRODUCT_BARCODE);
            parameters.Add(nameof(M_CHEM_PRODUCT.SDS_URL), value.SDS_URL);
            parameters.Add(nameof(M_CHEM_PRODUCT.SDS_UPD_DATE), OrgConvert.ToNullableDateTime(value.SDS_UPD_DATE));

            sql = insertSql.ToString();
        }

        protected override void CreateUpdateText(ChemProductModel value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(ChemProductModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(ChemProductModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}