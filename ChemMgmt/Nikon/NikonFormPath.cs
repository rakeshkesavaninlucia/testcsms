﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.Nikon
{
    /// <summary>
    /// ニコン用画面パスを定義します。
    /// </summary>
    public static class NikonFormPath
    {
        /// <summary>
        /// 化学物質マスター検索
        /// </summary>
        public static string ChemSearch { get; } = "~/ZC110/ZC11010";

        /// <summary>
        /// 化学物質マスターメンテナンス
        /// </summary>
        public static string ChemMaintenance { get; } = "~/ZC110/ZC11020";

        /// <summary>
        /// 化学物質マスターメンテナンス(ポップアップ)
        /// </summary>
        public static string ChemMaintenancePopup { get; } = "../ZC110/ZC11020";

        /// <summary>
        /// 含有物検索
        /// </summary>
        public static string ContainedSearchPopup { get; } = "../ZC110/ZC11021";

        /// <summary>
        /// 商品受入
        /// </summary>
        public static string ChemAccept { get; } = "~/ZC120/ZC12010";

        /// <summary>
        /// 使用履歴（複合）
        /// </summary>
        public static string UsageHistoryComplex { get; } = "~/ZC130/ZC13010";

        /// <summary>
        /// 管理台帳検索
        /// </summary>
        public static string LedgerSearch { get; } = "~/ZC140/ZC14010";

        /// <summary>
        /// 管理台帳登録申請
        /// </summary>
        public static string LedgerMaintenance { get; } = "ZC14020";


        /// </summary>
        public static string LedgerMaintenancePopup { get; } = "../ZC140/ZC14020";

        /// <summary>
        /// 使用場所明細
        /// </summary>
        public static string UsageLocationPopup { get; } = "../ZC140/ZC14021";

        /// <summary>
        /// 案件一覧
        /// </summary>
        public static string LedgerProject { get; } = "~/ZC140/ZC14030";

        /// <summary>
        /// 管理台帳一括変更
        /// </summary>
        public static string LedgerBulkUpdate { get; } = "~/ZC140/ZC14040";

        /// <summary>
        /// 管理台帳変更履歴
        /// </summary>
        public static string LedgerHistory { get; } = "ZC14050";

        /// <summary>
        /// 保管場所一括変更
        /// </summary>
        public static string BulkChangeStoringLocation { get; } = "~/ZC020/ZC02004";

        /// <summary>
        /// 作業記録検索
        /// </summary>
        public static string WorkRecordSearch { get; } = "~/ZC130/ZC13020";

        /// <summary>
        /// 危険物集計情報参照
        /// </summary>
        public static string HazardTotal { get; } = "~/ZC130/ZC13030";

    }
}