﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Nikon
{
    public static class NikonCommonMasterName
    {
        /// <summary>
        /// 鍵管理
        /// </summary>
        public const string KEY_MGMT = "M_KEY_MGMT";

        /// <summary>
        /// 重量管理
        /// </summary>
        public const string WEIGHT_MGMT = "M_WEIGHT_MGMT";

        public const string WORKTIME_MGMT = "M_WORKTIME_MGMT";

        /// <summary>
        /// 形状
        /// </summary>
        public const string FIGURE = "M_FIGURE";

        /// <summary>
        /// 変更・廃止の事由
        /// </summary>
        public const string REASON = "M_REASON";

        /// <summary>
        /// 労働安全衛生法上の用途
        /// </summary>
        public const string ISHA_USAGE = "M_ISHA_USAGE";

        /// <summary>
        /// 小分け容器の有無
        /// </summary>
        public const string SUB_CONTAINER = "M_SUB_CONTAINER";

        /// <summary>
        /// 保護具
        /// </summary>
        public const string PROTECTOR = "M_PROTECTOR";

        /// <summary>
        /// リスクアセスメントランク
        /// </summary>
        public const string RANK = "M_RISK_ASSESSMENT";

        //2019/02/06 Rin Add
        public const string CHEM_CAT = "M_CHEM_CAT";

        /// <summary>
        /// 職場の取扱量
        /// </summary>
        public const string REG_TRANSACTION_VOLUME = "T_LED_USAGE_LOC1";

        /// <summary>
        /// 作業頻度
        /// </summary>
        public const string REG_WORK_FREQUENCY = "T_LED_USAGE_LOC2";

        /// <summary>
        /// 災害発生の可能性
        /// </summary>
        public const string REG_DISASTER_POSSIBILITY = "T_LED_USAGE_LOC3";

        /// <summary>
        /// 職場の取扱量_値
        /// </summary>
        public const string REG_TRANSACTION_VOLUME_VALUE = "T_LED_USAGE_LOC1_VALUE";

        /// <summary>
        /// 作業頻度_値
        /// </summary>
        public const string REG_WORK_FREQUENCY_VALUE = "T_LED_USAGE_LOC2_VALUE";

        /// <summary>
        /// 災害発生の可能性_値
        /// </summary>
        public const string REG_DISASTER_POSSIBILITY_VALUE = "T_LED_USAGE_LOC3_VALUE";

        /// <summary>
        /// リスク低減策
        /// </summary>
        public const string RISK_REDUCE = "T_LED_RISK_REDUCE";

        /// <summary>
        /// リスク低減策_値
        /// </summary>
        public const string RISK_REDUCE_VALUE = "T_LED_RISK_REDUCE_VALUE";

        /// <summary>
        /// リスクアセスメント結果適用法令
        /// </summary>
        public const string APPLICABLE_LAW = "V_APPLICABLE_LAW";
    }
}