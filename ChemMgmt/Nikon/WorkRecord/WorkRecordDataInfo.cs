﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon
{
    public class WorkRecordModel : T_WORK_RECORD
    {
        public WorkRecordModel() { }

        public string UPD_TYPE { get; set; }

        [Display(Name = "“o˜^”Ô†")]
        public string REG_NO { get; set; }

        public ICollection<string> OldRegistNumbers { get; set; }

        public string REG_OLD_NO
        {
            get
            {
                return OldRegistNumbers.Count == 0 ? string.Empty : OldRegistNumbers.Aggregate((x, y) => $"{x}{SystemConst.ValuesDelimiter}{y}");
            }
        }

        public string CHEM_CD { get; set; }

        public string CHEM_NAME { get; set; }

        [Display(Name = "CAS#")]
        public string CAS_NO { get; set; }

        public string WORKER_USER_NM { get; set; }

        public string WORKER_USER_CD { get; set; }

        public string GROUP_NAME { get; set; }   // 2018/12/11 Add TPE Kometani

        public string WORKER_GROUP_CD { get; set; } // 2018/12/11 Add TPE Kometani

        public int CLASS_ID { get; set; }

        public int? USAGE_LOC_ID { get; set; }

        public string USAGE_LOC_NAME { get; set; }

        public string INTENDED_NM { get; set; }

        public string INTENDED { get; set; }

        public string EXPOSURE_WORK_USE_NAME { get; set; }

        public string EXPOSURE_WORK_TYPE_NAME { get; set; }

        public string EXPOSURE_WORK_TEMPERATURE_NAME { get; set; }

        public int? WORKING_TIMES { get; set; }

        public string UNIT_NAME { get; set; }

        public string C_CHEM_CD { get; set; }

        public string CONTAINED_NAME { get; set; }

        public decimal? MIN_CONTAINED_RATE { get; set; }

        public decimal? MAX_CONTAINED_RATE { get; set; }

        public string UPD_USER_NM { get; set; }

        public int? REG_TYPE_ID { get; set; }

        public string REG_TYPE_STRING { get; set; }

        public IEnumerable<int> REG_TYPE_ID_COLLECTION
        {
            get
            {
                if (REG_TYPE_STRING == null) yield break;
                foreach (var s in REG_TYPE_STRING?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string REG_TEXT { get; set; }

        public string MEMO { get; set; }
    }

    public class WorkRecordSearchModel : WorkRecordModel
    {
        public int? MaxSearchResult { get; set; }

        [Display(Name = "‰»Šw•¨Ž¿–¼")]
        public string PRODUCT_NAME_JP { get; set; }

        public int PRODUCT_NAME_CONDITION { get; set; }

        public int USER_NAME_CONDITION { get; set; }

        public int BARCODE_CONDITION { get; set; }

        public int REG_NO_CONDITION { get; set; }

        [Display(Name = "oŒÉŠJŽn“ú")]
        public string ISSUE_START_DATE { get; set; }

        [Display(Name = "oŒÉI—¹“ú")]
        public string ISSUE_END_DATE { get; set; }

        [Display(Name = "“üŒÉŠJŽn“ú")]
        public string RECEIPT_START_DATE { get; set; }

        [Display(Name = "“üŒÉI—¹“ú")]
        public string RECEIPT_END_DATE { get; set; }

        public int IncludesContained { get; set; } = (int)ChemSearchContained.Without;

        [Display(Name = "ì‹ÆŽÒ–¼")]
        public string USER_NAME { get; set; }

        public string GROUP_SELECTION { get; set; }

        public string USER_SELECTION { get; set; }

        [Display(Name = "ƒ†[ƒU[")]
        public IEnumerable<string> USER_COLLECTION
        {
            get
            {
                if (USER_SELECTION == null) yield break;
                foreach (var s in USER_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return s;
                    }
                }
            }
        }

        [Display(Name = "•”–¼")]
        public IEnumerable<string> GROUP_COLLECTION
        {
            get
            {
                if (GROUP_SELECTION == null) yield break;
                foreach (var s in GROUP_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return s;
                    }
                }
            }
        }

        public string UserSelectionStyle
        {
            get
            {
                return USER_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string UserSelectionText
        {
            get
            {
                return USER_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string UserClickEvent { get; set; }

        public string LOCATION_SELECTION { get; set; }

        [Display(Name = "Žg—pêŠ")]
        public IEnumerable<int> LOCATION_COLLECTION
        {
            get
            {
                if (LOCATION_SELECTION == null) yield break;
                foreach (var s in LOCATION_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string LocationSelectionStyle
        {
            get
            {
                return LOCATION_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string LocationSelectionText
        {
            get
            {
                return LOCATION_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string LocationClickEvent { get; set; }

        public string REG_SELECTION { get; set; }

        [Display(Name = "–@‹K§")]
        public IEnumerable<int> REG_COLLECTION
        {
            get
            {
                if (REG_SELECTION == null) yield break;
                foreach (var s in REG_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string RegulationSelectionStyle
        {
            get
            {
                return REG_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string RegulationSelectionText
        {
            get
            {
                return REG_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string RegulationClickEvent { get; set; }

        public string REFERENCE_AUTHORITY { get; set; }

        public string LOGIN_USER_CD { get; set; }
        public string hdnRegulationIds { get; set; }

        public string hdnLocationIds { get; set; }

        public string hdnUsedIds { get; set; }
        public string hdnGroupIds { get; set; }


        public bool IsScanned { get; set; } = false;
    }
}

namespace ZyCoaG.Nikon.WorkRecord
{
    public class WorkRecordDataInfo : DataInfoBase<WorkRecordModel>
    {
        protected override string TableName { get; } = "T_WORK_RECORD";

        /// <summary>
        /// ƒ†[ƒU[Ž«‘
        /// </summary>
        private IDictionary<string, List<string>> OldRegistNumbers { get; set; }

        /// <summary>
        /// ƒ†[ƒU[Ž«‘
        /// </summary>
        private IDictionary<string, string> Users { get; set; }

        /// <summary>
        /// •ÛŠÇêŠŽ«‘
        /// </summary>
        private IDictionary<int?, string> Locations { get; set; }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (WorkRecordSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            sql.AppendLine("from  T_WORK_RECORD inner join  T_WORKER on     T_WORK_RECORD.WORK_RECORD_ID = T_WORKER.WORK_RECORD_ID inner join T_ACTION_HISTORY on T_WORK_RECORD.ACTION_ID = T_ACTION_HISTORY.ACTION_ID ");
            sql.AppendLine("	 left outer join(	select    A.ACTION_ID , min(B.ACTION_ID) B_ACTION_ID, min(B.WORKING_TIMES) WORKING_TIMES from T_ACTION_HISTORY A left outer join T_ACTION_HISTORY B on A.BARCODE = B.BARCODE ");
            sql.AppendLine("     where   A.ACTION_TYPE_ID in (2,6) and B.ACTION_TYPE_ID in (3,4,5) and A.ACTION_ID < B.ACTION_ID and A.ACTION_TYPE_ID < B.ACTION_TYPE_ID group by A.ACTION_ID ) T_ACTION_HISTORY_B");
            sql.AppendLine("     on T_ACTION_HISTORY.ACTION_ID = T_ACTION_HISTORY_B.ACTION_ID inner join T_STOCK on T_WORK_RECORD.BARCODE = T_STOCK.BARCODE left outer join T_MGMT_LEDGER on T_STOCK.REG_NO = T_MGMT_LEDGER.REG_NO");
            sql.AppendLine("     inner join  (select   M_CHEM.CHEM_CD , M_CHEM.CHEM_CD as C_CHEM_CD, M_CHEM.CHEM_NAME, M_CHEM.CAS_NO, 100 as MIN_CONTAINED_RATE, 100 as MAX_CONTAINED_RATE from M_CHEM ");
            if (condition.IncludesContained == (int)ChemSearchContained.With && (!string.IsNullOrWhiteSpace(condition.PRODUCT_NAME_JP) || !string.IsNullOrWhiteSpace(condition.CAS_NO)))
            {
                sql.AppendLine("   union select M_CONTAINED.CHEM_CD, M_CONTAINED.C_CHEM_CD, M_CHEM.CHEM_NAME, M_CHEM.CAS_NO, M_CONTAINED.MIN_CONTAINED_RATE, M_CONTAINED.MAX_CONTAINED_RATE from M_CONTAINED");
                sql.AppendLine("   inner join M_CHEM on M_CONTAINED.C_CHEM_CD = M_CHEM.CHEM_NAME");
            }
            sql.AppendLine("     ) M_CHEM_ALL on T_STOCK.CAT_CD = M_CHEM_ALL.CHEM_CD inner join M_CHEM on T_STOCK.CAT_CD = M_CHEM.CHEM_CD left outer join V_CHEM_REGULATION on M_CHEM_ALL.CHEM_CD = V_CHEM_REGULATION.CHEM_CD ");
            sql.AppendLine("     left outer join V_GROUP     on     T_WORKER.WORKER_GROUP_CD = V_GROUP.GROUP_CD left outer join M_EXPOSURE_WORK M_EXPOSURE_WORK_USE on T_WORK_RECORD.EXPOSURE_WORK_USE = M_EXPOSURE_WORK_USE.EXPOSURE_WORK_ID ");
            sql.AppendLine("     left outer join M_EXPOSURE_WORK M_EXPOSURE_WORK_TYPE on T_WORK_RECORD.EXPOSURE_WORK_TYPE = M_EXPOSURE_WORK_TYPE.EXPOSURE_WORK_ID left outer join M_EXPOSURE_WORK M_EXPOSURE_WORK_TEMPERATURE");
            sql.AppendLine("     on     T_WORK_RECORD.EXPOSURE_WORK_TEMPERATURE = M_EXPOSURE_WORK_TEMPERATURE.EXPOSURE_WORK_ID left outer join M_UNITSIZE on T_WORK_RECORD.UNITSIZE_ID = M_UNITSIZE.UNITSIZE_ID ");
            sql.AppendLine("     left outer join M_USER M_UPD_USER on T_WORK_RECORD.UPD_USER_CD = M_UPD_USER.USER_CD");
            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (WorkRecordSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();
            Csv = string.Empty;

            sql.AppendLine("where (T_WORK_RECORD.WORK_RECORD_ID = T_WORK_RECORD.WORK_RECORD_ID");
            if (!string.IsNullOrWhiteSpace(condition.PRODUCT_NAME_JP))
            {
                sql.AppendLine("  and M_CHEM_ALL.CHEM_CD in (select distinct(M_CHEM_ALIAS.CHEM_CD) from M_CHEM_ALIAS where ");
                switch ((SearchConditionType)condition.PRODUCT_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("                                              upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("                                              upper(M_CHEM_ALIAS.CHEM_NAME) like @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("                                              upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("                                              upper(M_CHEM_ALIAS.CHEM_NAME) = @CHEM_NAME");
                        break;
                }
                sql.AppendLine("                              )");
                if ((SearchConditionType)condition.PRODUCT_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add("@CHEM_NAME", condition.PRODUCT_NAME_JP.ToUpper());
                }
                else
                {
                    parameters.Add("@CHEM_NAME", DbUtil.ConvertIntoEscapeChar(condition.PRODUCT_NAME_JP.ToUpper()));
                }
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.PRODUCT_NAME_JP)), condition.PRODUCT_NAME_CONDITION, condition.PRODUCT_NAME_JP);
            }

            if (!string.IsNullOrWhiteSpace(condition.BARCODE))
            {
                sql.AppendLine("            and");
                switch ((SearchConditionType)condition.BARCODE_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("                                          T_WORK_RECORD.BARCODE like '%' + @BARCODE + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("                                          T_WORK_RECORD.BARCODE like @BARCODE + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("                                          T_WORK_RECORD.BARCODE like '%' + @BARCODE " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("                                          T_WORK_RECORD.BARCODE = @BARCODE");
                        break;
                }
                if ((SearchConditionType)condition.BARCODE_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.BARCODE), condition.BARCODE);
                }
                else
                {
                    parameters.Add(nameof(condition.BARCODE), DbUtil.ConvertIntoEscapeChar(condition.BARCODE));
                }
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.BARCODE)), condition.BARCODE_CONDITION, condition.BARCODE);
            }

            if (!string.IsNullOrWhiteSpace(condition.REG_NO))
            {
                sql.AppendLine("            and");
                switch ((SearchConditionType)condition.REG_NO_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("                                          T_STOCK.REG_NO like '%' + @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("                                          T_STOCK.REG_NO like @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("                                          T_STOCK.REG_NO like '%' + @REG_NO " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("                                          T_STOCK.REG_NO = @REG_NO");
                        break;
                }
                if ((SearchConditionType)condition.REG_NO_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.REG_NO), condition.REG_NO);
                }
                else
                {
                    parameters.Add(nameof(condition.REG_NO), DbUtil.ConvertIntoEscapeChar(condition.REG_NO));
                }
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_NO)), condition.REG_NO_CONDITION, condition.REG_NO);
            }

            if (!string.IsNullOrWhiteSpace(condition.CAS_NO))
            {
                sql.AppendLine("            and M_CHEM.CAS_NO = @CAS_NO");
                parameters.Add(nameof(condition.CAS_NO), condition.CAS_NO);
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CAS_NO)), condition.CAS_NO);
            }


            if (!string.IsNullOrWhiteSpace(condition.hdnGroupIds))
            {
                List<string> TagIds = condition.hdnGroupIds.Split(',').ToList();
                sql.AppendLine("            and T_WORKER.WORKER_GROUP_NAME in (" + DbUtil.CreateInOperator(TagIds.ToList(), nameof(condition.GROUP_NAME), parameters) + ")");
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.GROUP_COLLECTION)), condition.hdnGroupIds.ToList());
            }

            if (!string.IsNullOrWhiteSpace(condition.USER_NAME))
            {
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.USER_NAME)), condition.USER_NAME_CONDITION, condition.USER_NAME);
            }

            if (!string.IsNullOrWhiteSpace(condition.ISSUE_START_DATE))
            {
                sql.AppendLine("            and T_WORK_RECORD.ISSUE_DATE >= @ISSUE_START_DATE");
                parameters.Add(nameof(condition.ISSUE_START_DATE), Convert.ToDateTime(condition.ISSUE_START_DATE).Date);
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.ISSUE_START_DATE)), Convert.ToDateTime(condition.ISSUE_START_DATE).ToShortDateString());
            }

            if (!string.IsNullOrWhiteSpace(condition.ISSUE_END_DATE))
            {
                sql.AppendLine("            and T_WORK_RECORD.ISSUE_DATE <= @ISSUE_END_DATE");
                parameters.Add(nameof(condition.ISSUE_END_DATE), Convert.ToDateTime(condition.ISSUE_END_DATE).Date.AddHours(23).AddMinutes(59).AddSeconds(59));
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.ISSUE_END_DATE)), Convert.ToDateTime(condition.ISSUE_END_DATE).ToShortDateString());
            }

            if (!string.IsNullOrWhiteSpace(condition.RECEIPT_START_DATE))
            {
                sql.AppendLine("            and T_WORK_RECORD.RECEIPT_DATE >= @RECEIPT_START_DATE");
                parameters.Add(nameof(condition.RECEIPT_START_DATE), Convert.ToDateTime(condition.RECEIPT_START_DATE).Date);
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.RECEIPT_START_DATE)), Convert.ToDateTime(condition.RECEIPT_START_DATE).ToShortDateString());
            }

            if (!string.IsNullOrWhiteSpace(condition.RECEIPT_END_DATE))
            {
                sql.AppendLine("            and T_WORK_RECORD.RECEIPT_DATE <= @RECEIPT_END_DATE");
                parameters.Add(nameof(condition.RECEIPT_END_DATE), Convert.ToDateTime(condition.RECEIPT_END_DATE).Date.AddHours(23).AddMinutes(59).AddSeconds(59));
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.RECEIPT_END_DATE)), Convert.ToDateTime(condition.RECEIPT_END_DATE).ToShortDateString());
            }

            var regulationBasedInPharse = string.Empty;
            if (!string.IsNullOrWhiteSpace(condition.hdnRegulationIds))
            {
                List<string> TagIds = condition.hdnRegulationIds.Split(',').ToList();
                regulationBasedInPharse = DbUtil.CreateInOperator(TagIds.ToList(), nameof(M_REGULATION.REG_TYPE_ID), parameters);
                sql.AppendLine("            and M_CHEM_ALL.CHEM_CD in ( select distinct(M_CHEM_REGULATION.CHEM_CD)");
                sql.AppendLine("                                        from M_CHEM_REGULATION");
                sql.AppendLine("                                        where M_CHEM_REGULATION.REG_TYPE_ID in (" + regulationBasedInPharse + ") )");

                //var regulationDic = new ChemMgmt.Classes.RegulationMasterInfo().GetDictionary();
                //var ghsCategoryDic = new GhsCategoryMasterInfo().GetDictionary();
                //foreach (var ghsCategory in ghsCategoryDic) regulationDic.Add(ghsCategory);
                //MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_COLLECTION)),
                //                         condition.REG_COLLECTION.Where(x => x != NikonConst.GhsCategoryTypeId &&
                //                                                             x != NikonConst.RegulationTypeId), regulationDic);
            }

            if (!string.IsNullOrWhiteSpace(condition.hdnLocationIds))
            {
                List<string> TagIds = condition.hdnLocationIds.Split(',').ToList();
                sql.AppendLine("            and T_ACTION_HISTORY.USAGE_LOC_ID in (" + DbUtil.CreateInOperator(TagIds.ToList(), nameof(condition.USAGE_LOC_ID), parameters) + ")");

                //MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.LOCATION_COLLECTION)),
                //                         condition.LOCATION_COLLECTION.Where(x => x != NikonConst.StoringLocationTypeId &&
                //                                                                  x != NikonConst.UsageLocationTypeId), Locations);
            }
            var includesContainedDic = new IncludesContainedListMasterInfo().GetDictionary();
            MakeConditionInLineOfCsv("ŠÜ—L•¨", includesContainedDic[condition.IncludesContained]);

            sql.AppendLine(" and M_CHEM_ALL.CHEM_CD in (select distinct(M_CHEM_REGULATION.CHEM_CD) from M_CHEM_REGULATION,M_REGULATION ");
            sql.AppendLine(" WHERE M_CHEM_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID AND M_REGULATION.WORKTIME_MGMT = 2) ");
            sql.AppendLine("          )");
            if (condition.REFERENCE_AUTHORITY == Const.cREFERENCE_AUTHORITY_FLAG_NORMAL)
            {
                sql.AppendLine("      and (T_STOCK.REG_NO in (select distinct(T_MGMT_LEDGER.REG_NO) from T_MGMT_LEDGER where T_MGMT_LEDGER.GROUP_CD in ((select G1.GROUP_CD ");
                sql.AppendLine("      from M_USER U1 left outer join M_GROUP G1 on G1.GROUP_CD = U1.GROUP_CD where U1.USER_CD = @USER_CD)");
                sql.AppendLine("      , (select G2.GROUP_CD from M_GROUP G2 where G2.GROUP_CD = ( select G5.P_GROUP_CD from  M_GROUP G5");
                sql.AppendLine("       left outer join M_USER U5 on G5.GROUP_CD = U5.GROUP_CD where U5.USER_CD = @USER_CD ))");
                sql.AppendLine("       ,( select G3.GROUP_CD from  M_USER U3 left outer join M_GROUP G3 on U3.PREVIOUS_GROUP_CD = G3.GROUP_CD where U3.USER_CD = @USER_CD");
                sql.AppendLine("        and G3.DEL_FLAG = 1 ) , (select G4.GROUP_CD from M_GROUP G4 where G4.GROUP_CD = ( select G6.P_GROUP_CD from M_USER U6 ");
                sql.AppendLine("        left outer join M_GROUP G6 on U6.PREVIOUS_GROUP_CD = G6.GROUP_CD where U6.USER_CD = @USER_CD and G6.DEL_FLAG = 1 ))) and T_MGMT_LEDGER.DEL_FLAG <> 1 ) or T_STOCK.REG_DATE <= '" + NikonConst.Phase2StartDate + "')");
                parameters.Add("@USER_CD", condition.LOGIN_USER_CD);
            }

            if (!string.IsNullOrWhiteSpace(condition.USER_NAME))
            {
                sql.AppendLine("            and");
                switch ((SearchConditionType)condition.USER_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("                                          T_WORKER.WORKER_USER_NM like '%' + @USER_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("                                          T_WORKER.WORKER_USER_NM like @USER_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("                                          T_WORKER.WORKER_USER_NM like '%' + @USER_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("                                          T_WORKER.WORKER_USER_NM = @USER_NAME");
                        break;
                }
                if ((SearchConditionType)condition.USER_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.USER_NAME), condition.USER_NAME);
                }
                else
                {
                    parameters.Add(nameof(condition.USER_NAME), DbUtil.ConvertIntoEscapeChar(condition.USER_NAME));
                }
            }

            sql.AppendLine(" and T_WORK_RECORD.DEL_FLAG = @DEL_FLAG");
            parameters.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Undelete);

            return sql.ToString();
        }

        public override List<WorkRecordModel> GetSearchResult(WorkRecordModel condition)
        {
            throw new NotImplementedException();
        }

        public List<WorkRecordModel> GetSearchResult(WorkRecordSearchModel condition)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select   T_STOCK.REG_NO, M_CHEM.CHEM_CD, M_CHEM.CHEM_NAME, M_CHEM.CAS_NO,T_WORK_RECORD.BARCODE, T_WORK_RECORD.ISSUE_DATE, T_WORK_RECORD.RECEIPT_DATE , T_WORKER.WORKER_USER_NM as WORKER_USER_NM, T_WORKER.WORKER_USER_CD");
            sql.AppendLine("       , T_WORKER.WORKER_GROUP_NAME as GROUP_NAME , T_WORKER.WORKER_GROUP_CD, T_WORKER.CLASS_ID, T_ACTION_HISTORY.USAGE_LOC_NAME, T_ACTION_HISTORY.INTENDED_NM , T_ACTION_HISTORY.INTENDED, M_EXPOSURE_WORK_USE.EXPOSURE_WORK_NAME as EXPOSURE_WORK_USE_NAME");
            sql.AppendLine("       , M_EXPOSURE_WORK_TYPE.EXPOSURE_WORK_NAME as EXPOSURE_WORK_TYPE_NAME, M_EXPOSURE_WORK_TEMPERATURE.EXPOSURE_WORK_NAME as EXPOSURE_WORK_TEMPERATURE_NAME, T_ACTION_HISTORY_B.WORKING_TIMES, T_WORK_RECORD.UNITSIZE");
            sql.AppendLine("       , M_UNITSIZE.UNIT_NAME, M_CHEM_ALL.C_CHEM_CD as C_CHEM_CD, M_CHEM_ALL.CHEM_NAME as CONTAINED_NAME, M_CHEM_ALL.MIN_CONTAINED_RATE, M_CHEM_ALL.MAX_CONTAINED_RATE, T_WORK_RECORD.NOTICES, V_CHEM_REGULATION.REG_TEXT, M_UPD_USER.USER_NAME as UPD_USER_NM");
            sql.AppendLine("       , T_WORK_RECORD.UPD_DATE, T_WORK_RECORD.REASON_FOR_UPDATE, T_ACTION_HISTORY.MEMO");
            sql.AppendLine(FromWardCreate(condition));
            DynamicParameters parameters = null;
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            List<WorkRecordModel> records = DbUtil.Select<WorkRecordModel>(sql.ToString(), parameters);
            var dataArray = new List<WorkRecordModel>();
            foreach (var record in records)
            {
                var data = new WorkRecordModel();
                data.REG_NO = record.REG_NO;
                data.CHEM_CD = record.CHEM_CD;
                data.CHEM_NAME = record.CHEM_NAME;
                data.CAS_NO = record.CAS_NO;
                data.BARCODE = record.BARCODE;
                data.ISSUE_DATE = OrgConvert.ToNullableDateTime(record.ISSUE_DATE);
                data.RECEIPT_DATE = OrgConvert.ToNullableDateTime(record.RECEIPT_DATE);
                data.WORKER_USER_NM = record.WORKER_USER_NM;
                data.WORKER_USER_CD = record.WORKER_USER_CD;
                data.GROUP_NAME = record.GROUP_NAME;
                data.WORKER_GROUP_CD = record.WORKER_GROUP_CD;
                data.CLASS_ID = Convert.ToInt32(record.CLASS_ID);
                data.USAGE_LOC_NAME = record.USAGE_LOC_NAME;
                data.INTENDED_NM = record.INTENDED_NM;
                data.INTENDED = record.INTENDED;
                data.EXPOSURE_WORK_USE_NAME = record.EXPOSURE_WORK_USE_NAME;
                data.EXPOSURE_WORK_TYPE_NAME = record.EXPOSURE_WORK_TYPE_NAME;
                data.EXPOSURE_WORK_TEMPERATURE_NAME = record.EXPOSURE_WORK_TEMPERATURE_NAME;
                data.WORKING_TIMES = OrgConvert.ToNullableInt32(record.WORKING_TIMES);
                data.UNITSIZE = OrgConvert.ToNullableDecimal(record.UNITSIZE);
                data.UNIT_NAME = record.UNIT_NAME;
                data.C_CHEM_CD = record.C_CHEM_CD;
                data.CONTAINED_NAME = record.CONTAINED_NAME;
                data.MIN_CONTAINED_RATE = OrgConvert.ToNullableDecimal(record.MIN_CONTAINED_RATE);
                data.MAX_CONTAINED_RATE = OrgConvert.ToNullableDecimal(record.MAX_CONTAINED_RATE);
                data.NOTICES = record.NOTICES;
                data.REG_TEXT = record.REG_TEXT;
                data.UPD_USER_NM = record.UPD_USER_NM;
                data.REASON_FOR_UPDATE = record.REASON_FOR_UPDATE;
                data.MEMO = record.MEMO;
                data.UPD_DATE = OrgConvert.ToNullableDateTime(record.UPD_DATE);
                if (data.CHEM_CD == data.C_CHEM_CD)
                {
                    data.CONTAINED_NAME = string.Empty;
                    data.MIN_CONTAINED_RATE = null;
                    data.MAX_CONTAINED_RATE = null;
                }
                dataArray.Add(data);
            }

            SetOldRegistNumbers(dataArray.Select(x => x.REG_NO).Distinct());
            dataArray.ForEach(x => EditData(x));

            return dataArray.OrderBy(x => x.REG_NO).ThenBy(x => x.BARCODE).ThenBy(x => x.WORKER_USER_CD).ToList();
        }

        protected override void InitializeDictionary()
        {
            Users = new UserMasterInfo().GetDictionary();
            Locations = new LocationMasterInfo().GetDictionary();
        }

        private void SetOldRegistNumbers(IEnumerable<string> registNumbers)
        {
            if (registNumbers.Count() == 0) return;

            var sql = new StringBuilder();
            sql.AppendLine(" select   T_MGMT_LED_REG_OLD_NO.REG_NO, T_MGMT_LED_REG_OLD_NO.REG_OLD_NO from T_MGMT_LED_REG_OLD_NO");
            sql.AppendLine($"where REG_NO in ({registNumbers.Select(x => $"'{registNumbers}'").Aggregate((x, y) => $"{x},{y}")})");
            DataTable records = DbUtil.Select(sql.ToString());
            OldRegistNumbers = new Dictionary<string, List<string>>();
            foreach (DataRow record in records.Rows)
            {
                var regNo = record["REG_NO"].ToString();
                var oldRegNo = record["REG_OLD_NO"].ToString();
                if (OldRegistNumbers.ContainsKey(regNo)) OldRegistNumbers[regNo].Add(oldRegNo);
                else OldRegistNumbers.Add(regNo, new List<string> { oldRegNo });
            }
        }

        public override void EditData(WorkRecordModel value)
        {
            if (OldRegistNumbers.ContainsKey(value.REG_NO)) value.OldRegistNumbers = OldRegistNumbers[value.REG_NO];
            else value.OldRegistNumbers = new string[] { };
            if (value.CLASS_ID == 0) value.NOTICES = string.Empty;
            if (!string.IsNullOrWhiteSpace(value.REASON_FOR_UPDATE)) value.UPD_TYPE = "C³";
        }

        protected override void CreateInsertText(WorkRecordModel value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(WorkRecordModel value, out string Sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(WorkRecordModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(WorkRecordModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}