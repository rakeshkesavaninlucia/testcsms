﻿using ChemMgmt.Models;
using ChemMgmt.Nikon.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ZyCoaG.Nikon
{
    public class FireMasterInfo : IDisposable
    {
        protected DbContext _Context;
        public DbContext Context
        {
            protected get
            {
                return _Context;
            }
            set
            {
                IsInteriorTransaction = false;
                _Context = value;
            }
        }

        protected virtual bool IsInteriorTransaction { get; set; } = true;

        private IEnumerable<V_FIRE> dataArray { get; set; }

        public FireMasterInfo()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" V_FIRE.FIRE_ID,");
            sql.AppendLine(" V_FIRE.FIRE_NAME,");
            sql.AppendLine(" V_FIRE.FIRE_NAME_BASE,");
            sql.AppendLine(" V_FIRE.FIRE_NAME_LAST,");
            sql.AppendLine(" V_FIRE.FIRE_ICON_PATH,");
            sql.AppendLine(" V_FIRE.P_FIRE_ID,");
            sql.AppendLine(" V_FIRE.SORT_ORDER,");
            sql.AppendLine(" V_FIRE.DEL_FLAG,");
            sql.AppendLine("N'手入力' as INPUT_FLAG");    //2019/03/12 TPE.Rin Add
            sql.AppendLine("from");
            sql.AppendLine(" V_FIRE");

            var parameters = new DynamicParameters();

            //StartTransaction();
            List<V_FIRE> records = DbUtil.Select<V_FIRE>(sql.ToString(), parameters);
            //Close();

            var dataArray = new List<V_FIRE>();
            foreach (var record in records)
            {
                var data = new V_FIRE();
                data.FIRE_ID = Convert.ToInt32(record.FIRE_ID.ToString());
                data.FIRE_NAME = record.FIRE_NAME;
                data.FIRE_NAME_BASE = record.FIRE_NAME_BASE;
                data.FIRE_NAME_LAST = record.FIRE_NAME_LAST;
                if (record.FIRE_ICON_PATH != null)
                {
                    data.FIRE_ICON_PATH = record.FIRE_ICON_PATH;
                }
                data.P_FIRE_ID = OrgConvert.ToNullableInt32(record.P_FIRE_ID.ToString());
                data.SORT_ORDER = OrgConvert.ToNullableInt32(record.SORT_ORDER.ToString());
                data.DEL_FLAG = OrgConvert.ToNullableInt32(record.DEL_FLAG.ToString());
                data.INPUT_FLAG = record.INPUT_FLAG.ToString();  //2019/03/12 TPE.Rin Add
                dataArray.Add(data);
            }
            this.dataArray = dataArray;
        }

        /// <summary>
        /// <para>変換辞書を取得します。</para>
        /// <para>重複したデータが存在した場合、後のデータが優先されます。</para>
        /// <para>基本機能→各社用の用語などのために後のデータを優先しています。</para>
        /// </summary>
        /// <returns>変換辞書を<see cref="IDictionary{T,string}"/>で返します。</returns>
        public virtual IDictionary<int?, string> GetDictionary()
        {
            var dic = new Dictionary<int?, string>();
            foreach (var data in dataArray) dic.Add(data.FIRE_ID, data.FIRE_NAME);
            return dic;
        }

        public IEnumerable<int> GetIdsBelongInChem(string chemCode)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.REG_TYPE_ID));
            sql.AppendLine("from");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION));
            sql.AppendLine("where");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CHEM_CD) + " = @" + nameof(M_CHEM_REGULATION.CHEM_CD));
            sql.AppendLine(" and");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CLASS_ID) + " = @" + nameof(M_CHEM_REGULATION.CLASS_ID));

            var parameters = new DynamicParameters();
            parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);
            parameters.Add(nameof(M_CHEM_REGULATION.CLASS_ID), (int)ClassIdType.Fire);

            //StartTransaction();
            List< M_CHEM_REGULATION> records = DbUtil.Select<M_CHEM_REGULATION>(sql.ToString(), parameters);
            //Close();

            var ids = new List<int>();
            foreach (var record in records)
            {
                ids.Add(Convert.ToInt32(record.REG_TYPE_ID.ToString()));
            }
            return ids;
        }

        public Dictionary<int, int> GetIdsInputFlg(string chemCode)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.REG_TYPE_ID));
            sql.AppendLine(" ,isnull(" + nameof(M_CHEM_REGULATION.INPUT_FLAG) + ",'0') AS INPUT_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION));
            sql.AppendLine("where");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CHEM_CD) + " = @" + nameof(M_CHEM_REGULATION.CHEM_CD));
            sql.AppendLine(" and");
            sql.AppendLine(" " + nameof(M_CHEM_REGULATION.CLASS_ID) + " = @" + nameof(M_CHEM_REGULATION.CLASS_ID));

            var parameters = new DynamicParameters();
            parameters.Add(nameof(M_CHEM_REGULATION.CHEM_CD), chemCode);
            parameters.Add(nameof(M_CHEM_REGULATION.CLASS_ID), (int)ClassIdType.Fire);

            //StartTransaction();
            List< M_CHEM_REGULATION> records = DbUtil.Select<M_CHEM_REGULATION>(sql.ToString(), parameters);
            //Close();

            var ids = new Dictionary<int, int>();
            if (records.Count > 0)
            {
                foreach (var record in records)
                {
                    ids.Add(Convert.ToInt32(record.REG_TYPE_ID.ToString()), Convert.ToInt32(record.INPUT_FLAG.ToString()));
                }
            }
            return ids;
        }


        public IEnumerable<V_FIRE> GetSelectedItems(IEnumerable<int> ids)
        {
            return dataArray.Where(x => ids.Contains(x.FIRE_ID)).OrderBy(x => x.SORT_ORDER).ThenBy(x => x.FIRE_ID);
        }
        public V_FIRE GetSelectedOneItems(int ids)
        {
            V_FIRE fire = new V_FIRE();
            foreach (V_FIRE data in dataArray)
            {
                if (data.FIRE_ID == ids)
                {
                    fire = data;
                }

            }
            return fire;
        }

        //protected void StartTransaction()
        //{
        //    if (_Context == null)
        //    {
        //        _Context = DbUtil.Open(true);
        //    }
        //}

        //protected void Close()
        //{
        //    if (IsInteriorTransaction)
        //    {
        //        _Context.Close();
        //        _Context = null;
        //    }
        //}

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    dataArray = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~FIREegoryInfo() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}