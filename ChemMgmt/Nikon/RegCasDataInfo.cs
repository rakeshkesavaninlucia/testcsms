﻿using COE.Common;
using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Threading.Tasks;
using ZyCoaG.Util;

namespace ZyCoaG.Nikon
{
    public class CasRegModel : V_CAS_REGULATION
    {
        public CasRegModel() { }

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

    }

    public class CasRegSearchModel : CasRegModel
    {

    }
}