﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerAccesmentRegModel : LedgerAccesmentRegColumn
    {
        public int _Mode { get; set; }

        public Mode Mode
        {
            get
            {
                Mode resultMode;
                if (Mode.TryParse(_Mode.ToString(), out resultMode)) return resultMode;
                return default(Mode);
            }
            set
            {
                _Mode = (int)value;
            }
        }

        public LedgerAccesmentRegModel() { }
    }

    public class LedgerAccesmentRegSearchModel : LedgerAccesmentRegModel
    {
        public int? MaxSearchResult { get; set; }

        public LedgerAccesmentRegSearchModel()
        {
            SearchOldRegisterNumbers = new List<LedgerAccesmentRegModel>();
        }

        public IEnumerable<LedgerAccesmentRegModel> SearchOldRegisterNumbers;

        /// <summary>
        /// 項目が読取のみか
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                        return false;
                    default:
                        return true;
                }
            }
        }

        public bool IsButtonVisibleOrEnabled
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}