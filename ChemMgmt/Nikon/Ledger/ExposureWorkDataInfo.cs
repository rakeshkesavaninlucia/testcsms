﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ZyCoaG.Nikon.Ledger
{
    /// <summary>
    /// ばく露作業の種類
    /// </summary>
    public enum ExposureClassId
    {
        WorkUse = 0,
        WorkType,
        UseTemperature
    }

    public class ExposureWorkDataInfo : CommonDataModelMasterInfoBase<int?>
    {
        private ExposureClassId exposureClassId { get; set; }

        /// <summary>
        /// データを全て取得します。
        /// </summary>
        /// <returns>データを<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" EXPOSURE_WORK_ID,");
            sql.AppendLine(" EXPOSURE_WORK_NAME,");
            sql.AppendLine(" DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" M_EXPOSURE_WORK");

            var context = DbUtil.Open(true);
            var records = DbUtil.Select(context, sql.ToString(), new Hashtable());
            context.Close();

            var collection = new List<CommonDataModel<int?>>();
            foreach (DataRow record in records.Rows)
            {
                collection.Add(new CommonDataModel<int?>
                {
                    Id = OrgConvert.ToNullableInt32(record["EXPOSURE_WORK_ID"].ToString()),
                    Name = record["EXPOSURE_WORK_NAME"].ToString(),
                    IsDeleted = Convert.ToInt32(record["DEL_FLAG"].ToString()).ToBool()
                });
            };
            return collection;
        }

        public ExposureWorkDataInfo(ExposureClassId exposureClassId)
        {
            this.exposureClassId = exposureClassId;
            getExposureWorkMaster();
        }

        private void getExposureWorkMaster()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" EXPOSURE_WORK_ID,");
            sql.AppendLine(" EXPOSURE_WORK_NAME,");
            sql.AppendLine(" DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" M_EXPOSURE_WORK");
            switch (exposureClassId)
            {
                case (ExposureClassId.WorkUse):
                    sql.AppendLine("where");
                    sql.AppendLine(" CLASS_ID = 0");
                    break;
                case (ExposureClassId.WorkType):
                    sql.AppendLine("where");
                    sql.AppendLine(" CLASS_ID = 1");
                    break;
                case (ExposureClassId.UseTemperature):
                    sql.AppendLine("where");
                    sql.AppendLine(" CLASS_ID = 2");
                    break;
            }
            var context = DbUtil.Open(true);
            var records = DbUtil.Select(context, sql.ToString(), new Hashtable());
            context.Close();

            var collection = new List<CommonDataModel<int?>>();
            foreach (DataRow record in records.Rows)
            {
                collection.Add(new CommonDataModel<int?>
                {
                    Id = OrgConvert.ToNullableInt32(record["EXPOSURE_WORK_ID"].ToString()),
                    Name = record["EXPOSURE_WORK_NAME"].ToString(),
                    IsDeleted = Convert.ToInt32(record["DEL_FLAG"].ToString()).ToBool()
                });
            };
            DataArray = collection;
        }
    }
}