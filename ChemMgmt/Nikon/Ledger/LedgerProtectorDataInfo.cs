﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerProtectorDataInfo : DataInfoBase<T_MGMT_LED_PROTECTOR>, IDisposable
    {
        protected override string TableName { get; } = nameof(T_MGMT_LED_PROTECTOR);

        private IDictionary<int?, string> protectorDic { get; set; }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerProtectorSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);

            return sql.ToString();
        }

        //protected override void CreateInsertText(T_MGMT_LED_PROTECTOR value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.REG_NO) + ",");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ) + ",");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ) + ",");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.REG_NO) + ",");
        //    builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ) + ",");
        //    builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ) + ",");
        //    builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(T_MGMT_LED_PROTECTOR.REG_NO), value.REG_NO);
        //    parameters.Add(nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
        //    parameters.Add(nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ), OrgConvert.ToNullableInt32(value.PROTECTOR_SEQ));
        //    parameters.Add(nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID), OrgConvert.ToNullableInt32(value.PROTECTOR_ID));

        //    sql = builder.ToString();
        //}
        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerProtectorSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.REG_NO != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LED_PROTECTOR.REG_NO = @REG_NO");
                parameters.Add(nameof(condition.REG_NO), condition.REG_NO);
            }

            if (condition.USAGE_LOC_SEQ != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ = @USAGE_LOC_SEQ");
                parameters.Add(nameof(condition.USAGE_LOC_SEQ), condition.USAGE_LOC_SEQ);
            }

            sql.AppendLine(")");

            return sql.ToString();
        }

        public IQueryable<LedgerProtectorModel> GetSearchResult(LedgerProtectorSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_PROTECTOR.REG_NO) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_PROTECTOR.REG_DATE));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<T_MGMT_LED_PROTECTOR> records = DbUtil.Select<T_MGMT_LED_PROTECTOR>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<LedgerProtectorModel>();

            foreach (DataRow record in dt.Rows)
            {
                var model = new LedgerProtectorModel();
                model.REG_NO = record[nameof(model.REG_NO)].ToString();
                model.USAGE_LOC_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.USAGE_LOC_SEQ)]);
                model.PROTECTOR_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.PROTECTOR_SEQ)]);
                model.PROTECTOR_ID = OrgConvert.ToNullableInt32(record[nameof(model.PROTECTOR_ID)]);
                model.PROTECTOR_NAME = protectorDic.GetValueToString(model.PROTECTOR_ID);
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
                model.CHECK = true;
                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.REG_NO)
                .ThenBy(x => x.USAGE_LOC_SEQ).ThenBy(x => x.PROTECTOR_SEQ);

            return searchQuery;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override void InitializeDictionary()
        {
            protectorDic = new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetDictionary();
        }

        public override void EditData(T_MGMT_LED_PROTECTOR Value) { }

        protected override void CreateInsertText(T_MGMT_LED_PROTECTOR value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.REG_NO) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_MGMT_LED_PROTECTOR.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
            parameters.Add(nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ), OrgConvert.ToNullableInt32(value.PROTECTOR_SEQ));
            parameters.Add(nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID), OrgConvert.ToNullableInt32(value.PROTECTOR_ID));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_MGMT_LED_PROTECTOR value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.PROTECTOR_ID) + " = :" + nameof(value.PROTECTOR_ID) + ",");
            builder.AppendLine("where ");
            builder.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(value.REG_NO), OrgConvert.ToNullableInt32(value.REG_NO));
            parameters.Add(nameof(value.PROTECTOR_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
            parameters.Add(nameof(value.PROTECTOR_SEQ), OrgConvert.ToNullableInt32(value.PROTECTOR_SEQ));
            parameters.Add(nameof(value.PROTECTOR_ID), OrgConvert.ToNullableInt32(value.PROTECTOR_ID));

            sql = builder.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    protectorDic = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        public override List<T_MGMT_LED_PROTECTOR> GetSearchResult(T_MGMT_LED_PROTECTOR condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_MGMT_LED_PROTECTOR value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.REG_NO) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_MGMT_LED_PROTECTOR.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
            parameters.Add(nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_SEQ), OrgConvert.ToNullableInt32(value.PROTECTOR_SEQ));
            parameters.Add(nameof(T_MGMT_LED_PROTECTOR.PROTECTOR_ID), OrgConvert.ToNullableInt32(value.PROTECTOR_ID));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_MGMT_LED_PROTECTOR value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}