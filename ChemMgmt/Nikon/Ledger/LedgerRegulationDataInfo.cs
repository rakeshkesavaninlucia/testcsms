﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon
{
    public class LedgerRegulationDataInfo : DataInfoBase<T_LED_REGULATION>, IDisposable
    {
        private IDictionary<int?, string> regulationDic { get; set; }
        private IDictionary<string, string> userDic { get; set; }
        protected override string TableName { get; } = nameof(T_LED_REGULATION);

        public override void EditData(T_LED_REGULATION Value)
        {
            throw new NotImplementedException();
        }

        public override List<T_LED_REGULATION> GetSearchResult(T_LED_REGULATION condition)
        {
            throw new NotImplementedException();
        }

        //protected override void CreateInsertText(T_LED_REGULATION value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_LED_REGULATION.REG_NO) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_REGULATION.LED_REGULATION_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_REGULATION.REG_TYPE_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_REGULATION.REG_USER_CD));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_LED_REGULATION.REG_NO) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_REGULATION.LED_REGULATION_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_REGULATION.REG_TYPE_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_REGULATION.REG_USER_CD));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(value.REG_NO), value.REG_NO.ToString());
        //    parameters.Add(nameof(T_LED_REGULATION.LED_REGULATION_ID), OrgConvert.ToNullableInt32(value.LED_REGULATION_ID));
        //    parameters.Add(nameof(T_LED_REGULATION.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
        //    parameters.Add(nameof(T_LED_REGULATION.REG_USER_CD), Convert.ToString(value.REG_USER_CD));

        //    sql = builder.ToString();
        //}
        public IQueryable<LedgerRegulationModel> GetSearchResult(LedgerRegulationSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_LED_REGULATION.REG_NO) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_REGULATION.LED_REGULATION_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_REGULATION.REG_TYPE_ID) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT_LAST) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_ICON_PATH) + ",");
            sql.AppendLine(nameof(M_REGULATION) + "." + nameof(M_REGULATION.WORKTIME_MGMT) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_REGULATION.REG_USER_CD) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_REGULATION.REG_DATE));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<T_LED_REGULATION> records = DbUtil.Select<T_LED_REGULATION>(sql.ToString(), parameters);

            var dataArray = new List<LedgerRegulationModel>();

            foreach (var record in records)
            {
                var model = new LedgerRegulationModel();
                model.REG_NO = Convert.ToString(record.REG_NO);
                model.LED_REGULATION_ID = OrgConvert.ToNullableInt32(record.LED_REGULATION_ID);
                model.REG_TYPE_ID = Convert.ToInt32(record.REG_TYPE_ID);
                model.REG_TEXT = regulationDic.GetValueToString(model.REG_TYPE_ID);
                model.REG_TEXT_LAST = Convert.ToString(record.REG_TEXT_LAST);
                model.REG_ICON_PATH = Convert.ToString(record.REG_ICON_PATH);
                model.WORKTIME_MGMT = OrgConvert.ToNullableInt32(record.WORKTIME_MGMT);
                model.REG_USER_CD = Convert.ToString(record.REG_USER_CD);
                model.REG_DATE = OrgConvert.ToNullableDateTime(record.REG_DATE);

                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.REG_NO).ThenBy(x => x.LED_REGULATION_ID);

            return searchQuery;
        }
        public IQueryable<LedgerRegulationModel> GetSearchResultFromChem(string strQuery)
        {
            var sql = new StringBuilder();
            DynamicParameters param = null;

            sql.AppendLine("select");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TYPE_ID) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT_LAST) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_ICON_PATH) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.CLASS_ID) + ",");
            sql.AppendLine(nameof(M_REGULATION) + "." + nameof(M_REGULATION.WORKTIME_MGMT) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.P_REG_TYPE_ID) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.SORT_ORDER));
            sql.AppendLine(" from V_REGULATION_ALL ");
            sql.AppendLine(" inner join " + nameof(M_REGULATION));
            sql.AppendLine(" on ");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TYPE_ID));
            sql.AppendLine(" = ");
            sql.AppendLine(nameof(M_REGULATION) + "." + nameof(M_REGULATION.REG_TYPE_ID));
            sql.AppendLine(" where ");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TYPE_ID));
            sql.AppendLine(" in (");
            sql.AppendLine(strQuery);
            sql.AppendLine(")");
            sql.AppendLine(" and M_REGULATION.DEL_FLAG = 0");
            sql.AppendLine(" order by V_REGULATION_ALL.P_REG_TYPE_ID, V_REGULATION_ALL.SORT_ORDER");

            //StartTransaction();
            List<V_REGULATION> records = DbUtil.Select<V_REGULATION>(sql.ToString(), param);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<LedgerRegulationModel>();

            //var sqlCas = new StringBuilder();
            //sqlCas.AppendLine("select");
            //sqlCas.AppendLine("distinct");
            //sqlCas.AppendLine("CASNO");
            //sqlCas.AppendLine("from V_CAS_REGULATION where REG_TYPE_ID = :REG_TYPE_ID");

            foreach (DataRow record in dt.Rows)
            {
                var model = new LedgerRegulationModel();
                model.REG_TYPE_ID = Convert.ToInt32(record[nameof(model.REG_TYPE_ID)]);
                model.REG_TEXT = regulationDic.GetValueToString(model.REG_TYPE_ID);
                model.REG_TEXT_LAST = record[nameof(model.REG_TEXT_LAST)].ToString();
                model.REG_ICON_PATH = record[nameof(model.REG_ICON_PATH)].ToString();
                model.WORKTIME_MGMT = OrgConvert.ToNullableInt32(record[nameof(model.WORKTIME_MGMT)]);
                model.CLASS_ID = Convert.ToInt32(record[nameof(model.CLASS_ID)]);

                //Hashtable parametersCas = new Hashtable();
                //parametersCas.Add(nameof(M_REGULATION.REG_TYPE_ID), model.REG_TYPE_ID);
                //DataTable recordsCas = DbUtil.select(Context, sqlCas.ToString(), parametersCas);
                //string caslists = null;
                //foreach (DataRow recordCas in recordsCas.Rows)
                //{
                //    if (caslists != null)
                //    {
                //        caslists += ",";
                //    }
                //    caslists += recordCas["CASNO"].ToString();
                //}
                //model.CAS_NO_LIST = caslists;

                dataArray.Add(model);
            }

            return dataArray.AsQueryable();
        }
        protected override void InitializeDictionary()
        {
            regulationDic = new LedRegulationMasterInfo().GetDictionary();
            userDic = new UserMasterInfo().GetDictionary();
        }
        public IQueryable<LedgerRegulationModel> GetResultFromRegulation(string strtype, string keyid)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = new DynamicParameters();

            if (strtype == "T_LED_REGULATION")
            {
                sql.AppendLine("select");
                sql.AppendLine(" REG_TYPE_ID from T_LED_REGULATION where REG_NO = @REG_NO");
                parameters.Add("REG_NO", keyid);
            }
            if (strtype == "T_LEDGER_WORK_REGULATION")
            {
                sql.AppendLine("select");
                sql.AppendLine(" REG_TYPE_ID from T_LEDGER_WORK_REGULATION where LEDGER_FLOW_ID = @LEDGER_FLOW_ID");
                parameters.Add("LEDGER_FLOW_ID", keyid);
            }

            //StartTransaction();
            List<LedgerRegulationModel> records = DbUtil.Select<LedgerRegulationModel>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            if (records.Count != 0)
            {
                string arrRegTypeIds = "";
                foreach (DataRow record in dt.Rows)
                {
                    if (arrRegTypeIds != "")
                    {
                        arrRegTypeIds += ",";
                    }
                    arrRegTypeIds += record["REG_TYPE_ID"].ToString();
                }
                return GetSearchResultFromChem(arrRegTypeIds);
            }
            else
            {
                var dataArray = new List<LedgerRegulationModel>();
                return dataArray.AsQueryable();
            }
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        public IQueryable<LedgerRegulationModel> GetRegTypeIds(string strQuery, string strIshaUsages)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;
            sql.AppendLine("select");
            sql.AppendLine(" distinct " + nameof(M_CHEM_REGULATION.REG_TYPE_ID));
            sql.AppendLine(" from " + nameof(M_CHEM_REGULATION));
            sql.AppendLine(" where ");
            sql.AppendLine(nameof(M_CHEM_REGULATION.REG_TYPE_ID) + " not in (");
            sql.AppendLine("select REG_TYPE_ID from V_CAS_REGULATION)");
            sql.AppendLine(" and REG_TYPE_ID in(");
            sql.AppendLine(strQuery);
            sql.AppendLine(")");

            if (strIshaUsages != "")
            {
                sql.AppendLine(" union ");
                sql.AppendLine(" select distinct REG_TYPE_ID from V_APPLICABLE_LAW ");
                sql.AppendLine(" where (ISHA_USAGE in ( ");
                sql.AppendLine(strIshaUsages);
                sql.AppendLine(" ) or ISHA_USAGE is null) ");
                sql.AppendLine(" and REG_TYPE_ID in(");
                sql.AppendLine(strQuery);
                sql.AppendLine(")");
            }

            //StartTransaction();
            List<LedgerRegulationModel> records = DbUtil.Select<LedgerRegulationModel>(sql.ToString(), parameters);
            string strRegTypeIds = "";
            foreach (LedgerRegulationModel record in records)
            {
                if (strRegTypeIds != "")
                {
                    strRegTypeIds += ",";
                }

                strRegTypeIds += record.REG_TYPE_ID.ToString();
            }

            if (strRegTypeIds != "")
            {
                return GetSearchResultFromChem(strRegTypeIds);
            }
            else
            {
                var dataArray = new List<LedgerRegulationModel>();
                return dataArray.AsQueryable();
            }
        }

        protected override void CreateInsertText(T_LED_REGULATION value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(T_LED_REGULATION value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerRegulationSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);
            sql.AppendLine("inner join ");
            sql.AppendLine("V_REGULATION_ALL on T_LED_REGULATION.REG_TYPE_ID = V_REGULATION_ALL.REG_TYPE_ID");
            sql.AppendLine("inner join M_REGULATION on T_LED_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID");
            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerRegulationSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.REG_NO != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LED_REGULATION.REG_NO = @REG_NO");
                parameters.Add(nameof(condition.REG_NO), condition.REG_NO);
            }

            sql.AppendLine(")");

            return sql.ToString();
        }

        protected override void CreateInsertText(T_LED_REGULATION value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LED_REGULATION.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_LED_REGULATION.LED_REGULATION_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_REGULATION.REG_TYPE_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_REGULATION.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_LED_REGULATION.REG_NO) + ",");
            builder.AppendLine("  :" + nameof(T_LED_REGULATION.LED_REGULATION_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_REGULATION.REG_TYPE_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_REGULATION.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(value.REG_NO), value.REG_NO.ToString());
            parameters.Add(nameof(T_LED_REGULATION.LED_REGULATION_ID), OrgConvert.ToNullableInt32(value.LED_REGULATION_ID));
            parameters.Add(nameof(T_LED_REGULATION.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
            parameters.Add(nameof(T_LED_REGULATION.REG_USER_CD), Convert.ToString(value.REG_USER_CD));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LED_REGULATION value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}