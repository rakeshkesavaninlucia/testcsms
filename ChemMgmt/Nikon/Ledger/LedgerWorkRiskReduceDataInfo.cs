﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon.Models;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerWorkRiskReduceDataInfo : DataInfoBase<T_LED_WORK_RISK_REDUCE>, IDisposable
    {
        protected override string TableName { get; } = nameof(T_LED_WORK_RISK_REDUCE);

        protected override void CreateInsertText(T_LED_WORK_RISK_REDUCE value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_TYPE) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  @" + nameof(T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_TYPE) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_RISK_REDUCE.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
            parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_ID), OrgConvert.ToNullableInt32(value.LED_RISK_REDUCE_ID));
            parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_TYPE), OrgConvert.ToNullableInt32(value.LED_RISK_REDUCE_TYPE));
            parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG), value.LED_RISK_REDUCE_FLAG);
            parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.REG_USER_CD), value.REG_USER_CD);

            sql = builder.ToString();
        }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerRiskReduceSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerRiskReduceSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID = @LEDGER_FLOW_ID");
                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
            }

            if (condition.USAGE_LOC_SEQ != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ = @USAGE_LOC_SEQ");
                parameters.Add(nameof(condition.USAGE_LOC_SEQ), condition.USAGE_LOC_SEQ);
            }

            sql.AppendLine(")");

            return sql.ToString();
        }

        //protected override string WhereWardCreate(dynamic dynamicCondition, out Hashtable parameters)
        //{
        //    var condition = (LedgerRiskReduceSearchModel)dynamicCondition;
        //    parameters = new Hashtable();
        //    var sql = new StringBuilder();

        //    sql.AppendLine("where");
        //    sql.AppendLine("(");
        //    sql.AppendLine(" 1 = 1");

        //    if (condition.LEDGER_FLOW_ID != null)
        //    {
        //        sql.AppendLine(" and");
        //        sql.AppendLine(" T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID = :LEDGER_FLOW_ID");
        //        parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
        //    }

        //    if (condition.USAGE_LOC_SEQ != null)
        //    {
        //        sql.AppendLine(" and");
        //        sql.AppendLine(" T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ = :USAGE_LOC_SEQ");
        //        parameters.Add(nameof(condition.USAGE_LOC_SEQ), condition.USAGE_LOC_SEQ);
        //    }

        //    sql.AppendLine(")");

        //    return sql.ToString();
        //}

        //public override IQueryable<T_LED_WORK_RISK_REDUCE> GetSearchResult(T_LED_WORK_RISK_REDUCE condition)
        //{
        //    throw new NotImplementedException();
        //}

        //public IQueryable<LedgerRiskReduceModel> GetSearchResult(LedgerRiskReduceSearchModel condition)
        //{
        //    var sql = new StringBuilder();
        //    Hashtable parameters = null;

        //    sql.AppendLine("select");
        //    if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
        //    {
        //        sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
        //    }
        //    sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_ID) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_TYPE) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.REG_DATE));
        //    sql.AppendLine(FromWardCreate(condition));
        //    sql.AppendLine(WhereWardCreate(condition, out parameters));
        //    if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
        //    {
        //        sql.AppendLine(" and");
        //        sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
        //    }

        //    //StartTransaction();
        //    DataTable records = DbUtil.select(Context, sql.ToString(), parameters);

        //    var dataArray = new List<LedgerRiskReduceModel>();

        //    foreach (DataRow record in records.Rows)
        //    {
        //        var model = new LedgerRiskReduceModel();
        //        model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)]);
        //        model.USAGE_LOC_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.USAGE_LOC_SEQ)]);
        //        model.LED_RISK_REDUCE_ID = OrgConvert.ToNullableInt32(record[nameof(model.LED_RISK_REDUCE_ID)]);
        //        model.LED_RISK_REDUCE_TYPE = OrgConvert.ToNullableInt32(record[nameof(model.LED_RISK_REDUCE_TYPE)]);
        //        model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);

        //        if (Convert.ToInt32(record[nameof(model.LED_RISK_REDUCE_FLAG)]) == 1)
        //        {
        //            model.LED_RISK_REDUCE_FLAG = true;
        //        }
        //        else
        //        {
        //            model.LED_RISK_REDUCE_FLAG = false;
        //        }
        //        dataArray.Add(model);
        //    }

        //    var searchQuery = dataArray.AsQueryable();

        //    searchQuery = searchQuery.OrderBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.USAGE_LOC_SEQ).ThenBy(x => x.LED_RISK_REDUCE_ID);

        //    return searchQuery;
        //}

        protected override void InitializeDictionary()
        {
        }

        public override void EditData(T_LED_WORK_RISK_REDUCE Value) { }

        //protected override void CreateInsertText(T_LED_WORK_RISK_REDUCE value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_TYPE) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_RISK_REDUCE.REG_USER_CD));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_TYPE) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_RISK_REDUCE.REG_USER_CD));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
        //    parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
        //    parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_ID), OrgConvert.ToNullableInt32(value.LED_RISK_REDUCE_ID));
        //    parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_TYPE), OrgConvert.ToNullableInt32(value.LED_RISK_REDUCE_TYPE));
        //    parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG), value.LED_RISK_REDUCE_FLAG);
        //    parameters.Add(nameof(T_LED_WORK_RISK_REDUCE.REG_USER_CD), value.REG_USER_CD);

        //    sql = builder.ToString();
        //}

        protected override void CreateUpdateText(T_LED_WORK_RISK_REDUCE value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.LED_RISK_REDUCE_TYPE) + " = :" + nameof(value.LED_RISK_REDUCE_TYPE) + ",");
            builder.AppendLine(" " + nameof(value.LED_RISK_REDUCE_FLAG) + " = :" + nameof(value.LED_RISK_REDUCE_FLAG));
            builder.AppendLine("where ");
            builder.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(value.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(value.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
            parameters.Add(nameof(value.LED_RISK_REDUCE_ID), OrgConvert.ToNullableInt32(value.LED_RISK_REDUCE_ID));

            sql = builder.ToString();
        }

        /// <summary>
        /// 保存ロジック
        /// </summary>
        /// <param name="ledger">LedgerModel</param>
        /// <param name="usageLocation">LedgerUsageLocationModel</param>
        public void Save(LedgerModel ledger, LedgerUsageLocationModel usageLocation)
        {
            var condition = new LedgerRiskReduceSearchModel();
            condition.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
            condition.USAGE_LOC_SEQ = usageLocation.USAGE_LOC_SEQ;
            var deleteDataArray = GetSearchResult(condition);
            Remove(deleteDataArray.Select(x => (T_LED_WORK_RISK_REDUCE)x), true);

            var riskreduces = usageLocation.RISK_REDUCE_COLLECTION != null ?
                usageLocation.RISK_REDUCE_COLLECTION : Enumerable.Empty<LedgerRiskReduceModel>();
            var j = 1;
            foreach (var riskreduce in riskreduces)
            {
                riskreduce.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                riskreduce.USAGE_LOC_SEQ = usageLocation.USAGE_LOC_SEQ;
                riskreduce.LED_RISK_REDUCE_ID = j;
                j++;
            }
            Add(riskreduces.Select(x => (T_LED_WORK_RISK_REDUCE)x));
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }       

        protected override void CreateUpdateText(T_LED_WORK_RISK_REDUCE value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        public override List<T_LED_WORK_RISK_REDUCE> GetSearchResult(T_LED_WORK_RISK_REDUCE condition)
        {
            throw new NotImplementedException();
        }

        public IQueryable<LedgerRiskReduceModel> GetSearchResult(LedgerRiskReduceSearchModel condition)
        {
            try
            {
                var sql = new StringBuilder();
                DynamicParameters parameters = null;

                sql.AppendLine("select");
                if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
                {
                    sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
                }
                sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID) + ",");
                sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ) + ",");
                sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_ID) + ","); 
                sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_TYPE) + ",");
                sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG) + " AS REAL,");
                sql.AppendLine(TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.REG_DATE)+",");
                sql.AppendLine("(CASE WHEN "+TableName + "." + nameof(T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG)+ "=1 THEN true ELSE false END) LED_RISK_REDUCE_FLAG ");
                sql.AppendLine(FromWardCreate(condition));
                sql.AppendLine(WhereWardCreate(condition, out parameters));
                if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
                {
                    sql.AppendLine(" and");
                    sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
                }

                //StartTransaction();
                List<LedgerRiskReduceModel> records = DbUtil.Select<LedgerRiskReduceModel>(sql.ToString(), parameters);
                //DataTable dt = ConvertToDataTable(records);
                //var dataArray = new List<LedgerRiskReduceModel>();

                //foreach (DataRow record in dt.Rows)
                //{
                //    var model = new LedgerRiskReduceModel();
                //    model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)]);
                //    model.USAGE_LOC_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.USAGE_LOC_SEQ)]);
                //    model.LED_RISK_REDUCE_ID = OrgConvert.ToNullableInt32(record[nameof(model.LED_RISK_REDUCE_ID)]);
                //    model.LED_RISK_REDUCE_TYPE = OrgConvert.ToNullableInt32(record[nameof(model.LED_RISK_REDUCE_TYPE)]);
                //    model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);

                //    if (Convert.ToInt32(record[nameof(model.LED_RISK_REDUCE_FLAG)]) == 1)
                //    {
                //        model.LED_RISK_REDUCE_FLAG = true;
                //    }
                //    else
                //    {
                //        model.LED_RISK_REDUCE_FLAG = false;
                //    }
                //    dataArray.Add(model);
                //}

                var searchQuery = records.AsQueryable();

                searchQuery = searchQuery.OrderBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.USAGE_LOC_SEQ).ThenBy(x => x.LED_RISK_REDUCE_ID);

                return searchQuery;
            }
            catch (Exception ex)
            {

                throw ex;
            }
           
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override void CreateInsertText(T_LED_WORK_RISK_REDUCE value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        
    }
}