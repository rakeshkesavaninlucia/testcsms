﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerHistoryDataInfo : DataInfoBase<T_LEDGER_HISTORY>
    {
        protected override string TableName { get; } = nameof(T_LEDGER_HISTORY);

        private IDictionary<int?, string> reasonDic { get; set; }

        private IDictionary<string, string> userDic { get; set; }

        private IDictionary<string, string> groupDic { get; set; }

        private IDictionary<int?, string> figureDic { get; set; }

        private IDictionary<int?, string> unitsizeDic { get; set; }

        private IEnumerable<CommonDataModel<int?>> unitsizeSelectList { get; set; }

        private IDictionary<int?, string> locationDic { get; set; }

        private IDictionary<int?, string> ishaUsageDic { get; set; }

        private IDictionary<int?, string> subcontainerDic { get; set; }

        private IDictionary<int?, string> rankDic { get; set; }

        private IDictionary<int?, string> protectorDic { get; set; }

        private IDictionary<int?, string> ledgerStatusDic { get; set; }

        private IDictionary<int?, string> regulationDic { get; set; }

        private IDictionary<int?, string> riskreduceDic { get; set; }
        
        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);

            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_CHEM");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_HISTORY.CHEM_CD = M_CHEM.CHEM_CD");

            sql.AppendLine("  left join");
            sql.AppendLine(" (");
            sql.AppendLine("select * from M_CHEM_PRODUCT t1");
            sql.AppendLine("where not exists(");
            sql.AppendLine("select");
            sql.AppendLine("  1");
            sql.AppendLine(" from M_CHEM_PRODUCT t2");
            sql.AppendLine("where");
            sql.AppendLine("  t1.CHEM_CD = t2.CHEM_CD");
            sql.AppendLine("  and");
            sql.AppendLine("  t1.PRODUCT_SEQ > t2.PRODUCT_SEQ)");
            sql.AppendLine(") M_CHEM_PRODUCT");
            sql.AppendLine(" on");
            sql.AppendLine(" M_CHEM.CHEM_CD = M_CHEM_PRODUCT.CHEM_CD");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select CHEM_CD, COUNT(1) CHEM_PRODUCT_COUNT from M_CHEM_PRODUCT group by CHEM_CD) CHEM_PRODUCT_COUNT");
            sql.AppendLine("on");
            sql.AppendLine(" M_CHEM.CHEM_CD = CHEM_PRODUCT_COUNT.CHEM_CD");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select LEDGER_FLOW_ID, COUNT(1) REG_OLD_NO_COUNT from T_LED_HISTORY_REG_OLD_NO group by LEDGER_FLOW_ID) COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_FLOW_ID = COUNT.LEDGER_FLOW_ID");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_HISTORY_LED_REGULATION");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID = V_HISTORY_LED_REGULATION.LEDGER_HISTORY_ID");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_USER");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_HISTORY.REG_USER_CD = M_USER.USER_CD");

            sql.AppendLine(" left outer join");
            sql.AppendLine("(");
            sql.AppendLine(" select LEDGER_HISTORY_ID, REG_OLD_NO from T_LED_HISTORY_REG_OLD_NO O1 where REG_OLD_NO_SEQ = ");
            sql.AppendLine(" (");
            sql.AppendLine("  select MIN(REG_OLD_NO_SEQ) from T_LED_HISTORY_REG_OLD_NO O2 where O1.LEDGER_HISTORY_ID = O2.LEDGER_HISTORY_ID");
            sql.AppendLine(" )");
            sql.AppendLine(") OLD_NO");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID = OLD_NO.LEDGER_HISTORY_ID");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_WORKFLOW");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_HISTORY.FLOW_CD = M_WORKFLOW.FLOW_CD");

            if (condition.REGULATION_BASE)
            {
                sql.AppendLine(" left join T_LEDGER_HISTORY_REGULATION on T_LEDGER_HISTORY.LEDGER_HISTORY_ID = T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID");
            }
            else
            {
                sql.AppendLine(" left join");
                sql.AppendLine(" (");
                sql.AppendLine(" select * from T_LEDGER_HISTORY_REGULATION t1 ");
                sql.AppendLine(" where not exists (");
                sql.AppendLine("  select ");
                sql.AppendLine("   1");
                sql.AppendLine("  from T_LEDGER_HISTORY_REGULATION t2 ");
                sql.AppendLine("  where ");
                sql.AppendLine("   t1.LEDGER_HISTORY_ID = t2.LEDGER_HISTORY_ID ");
                sql.AppendLine("   and ");
                sql.AppendLine("   t1.LED_REGULATION_ID > t2.LED_REGULATION_ID ");
                sql.AppendLine("  group by LEDGER_HISTORY_ID)");
                sql.AppendLine(" ) T_LEDGER_HISTORY_REGULATION");
                sql.AppendLine(" on");

                sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID = T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID");
                sql.AppendLine(" left outer join");
                sql.AppendLine(" (select LEDGER_HISTORY_ID, COUNT(1) REGULATION_COUNT from T_LEDGER_HISTORY_REGULATION group by LEDGER_HISTORY_ID) REGULATION_COUNT");
                sql.AppendLine(" on");
                sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID = REGULATION_COUNT.LEDGER_HISTORY_ID");
            }

            if (condition.LOC_BASE)
            {
                sql.AppendLine(" left join T_LED_HISTORY_LOC on T_LEDGER_HISTORY.LEDGER_HISTORY_ID = T_LED_HISTORY_LOC.LEDGER_HISTORY_ID");
            }
            else
            {
                sql.AppendLine(" left join");
                sql.AppendLine(" (");
                sql.AppendLine(" select * from T_LED_HISTORY_LOC t1 ");
                sql.AppendLine(" where not exists (");
                sql.AppendLine("  select ");
                sql.AppendLine("   1");
                sql.AppendLine("  from T_LED_HISTORY_LOC t2 ");
                sql.AppendLine("  where ");
                sql.AppendLine("   t1.LEDGER_HISTORY_ID = t2.LEDGER_HISTORY_ID ");
                sql.AppendLine("   and ");
                sql.AppendLine("   t1.LOC_SEQ > t2.LOC_SEQ ");
                sql.AppendLine("  group by LEDGER_HISTORY_ID)");
                sql.AppendLine(" ) T_LED_HISTORY_LOC");
                sql.AppendLine(" on");
                sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID = T_LED_HISTORY_LOC.LEDGER_HISTORY_ID");

                sql.AppendLine(" left outer join");
                sql.AppendLine(" (select LEDGER_HISTORY_ID, COUNT(1) LOC_COUNT from T_LED_HISTORY_LOC group by LEDGER_HISTORY_ID) LOC_COUNT");
                sql.AppendLine(" on");
                sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID = LOC_COUNT.LEDGER_HISTORY_ID");
            }

            if (condition.USAGE_LOC_BASE)
            {
                sql.AppendLine(" left join T_LED_HISTORY_USAGE_LOC on T_LEDGER_HISTORY.LEDGER_HISTORY_ID = T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID");
            }
            else
            {
                sql.AppendLine(" left join");
                sql.AppendLine(" (");
                sql.AppendLine(" select * from T_LED_HISTORY_USAGE_LOC t1 ");
                sql.AppendLine(" where not exists (");
                sql.AppendLine("  select ");
                sql.AppendLine("   1");
                sql.AppendLine("  from T_LED_HISTORY_USAGE_LOC t2");
                sql.AppendLine("  where ");
                sql.AppendLine("   t1.LEDGER_FLOW_ID = t2.LEDGER_FLOW_ID ");
                sql.AppendLine("   and");
                sql.AppendLine("   t1.USAGE_LOC_SEQ > t2.USAGE_LOC_SEQ)");
                sql.AppendLine(" ) T_LED_HISTORY_USAGE_LOC");
                sql.AppendLine(" on");
                sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID = T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID");

                sql.AppendLine(" left outer join");
                sql.AppendLine(" (select LEDGER_HISTORY_ID, COUNT(1) USAGE_LOC_COUNT from T_LED_HISTORY_USAGE_LOC group by LEDGER_HISTORY_ID) USAGE_LOC_COUNT");
                sql.AppendLine(" on");
                sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID = USAGE_LOC_COUNT.LEDGER_HISTORY_ID");
            }

            sql.AppendLine(" left join");
            sql.AppendLine(" (");
            sql.AppendLine(" select * from T_LED_HISTORY_PROTECTOR t1 ");
            sql.AppendLine(" where not exists (");
            sql.AppendLine("  select ");
            sql.AppendLine("   1");
            sql.AppendLine("  from T_LED_HISTORY_PROTECTOR t2");
            sql.AppendLine("  where ");
            sql.AppendLine("   t1.LEDGER_HISTORY_ID = t2.LEDGER_HISTORY_ID ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.USAGE_LOC_SEQ = t2.USAGE_LOC_SEQ ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.PROTECTOR_SEQ > t2.PROTECTOR_SEQ)");
            sql.AppendLine(" ) T_LED_HISTORY_PROTECTOR");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID = T_LED_HISTORY_PROTECTOR.LEDGER_HISTORY_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ = T_LED_HISTORY_PROTECTOR.USAGE_LOC_SEQ");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select LEDGER_HISTORY_ID, USAGE_LOC_SEQ, COUNT(1) PROTECTOR_COUNT from T_LED_HISTORY_PROTECTOR group by LEDGER_HISTORY_ID, USAGE_LOC_SEQ) PROTECTOR_COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID = PROTECTOR_COUNT.LEDGER_HISTORY_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ = PROTECTOR_COUNT.USAGE_LOC_SEQ");

            sql.AppendLine(" left join ");
            sql.AppendLine(" ( ");
            sql.AppendLine(" select * from T_LED_HISTORY_ACCESMENT_REG t1 ");
            sql.AppendLine(" where not exists ( ");
            sql.AppendLine("  select ");
            sql.AppendLine("   1 ");
            sql.AppendLine("  from T_LED_HISTORY_ACCESMENT_REG t2 ");
            sql.AppendLine("  where ");
            sql.AppendLine("   t1.LEDGER_HISTORY_ID = t2.LEDGER_HISTORY_ID ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.USAGE_LOC_SEQ = t2.USAGE_LOC_SEQ ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.LED_ACCESMENT_REG_ID > t2.LED_ACCESMENT_REG_ID) ");
            sql.AppendLine(" ) T_LED_HISTORY_ACCESMENT_REG ");
            sql.AppendLine(" on ");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID = T_LED_HISTORY_ACCESMENT_REG.LEDGER_HISTORY_ID ");
            sql.AppendLine(" and ");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ = T_LED_HISTORY_ACCESMENT_REG.USAGE_LOC_SEQ ");
            sql.AppendLine(" left outer join ");
            sql.AppendLine(" (select LEDGER_HISTORY_ID, USAGE_LOC_SEQ, COUNT(1) ACCESMENT_REG_COUNT from T_LED_HISTORY_ACCESMENT_REG group by LEDGER_HISTORY_ID, USAGE_LOC_SEQ) ACCESMENT_REG_COUNT ");
            sql.AppendLine(" on ");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID = ACCESMENT_REG_COUNT.LEDGER_HISTORY_ID ");
            sql.AppendLine(" and ");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ = ACCESMENT_REG_COUNT.USAGE_LOC_SEQ ");

            sql.AppendLine(" left join");
            sql.AppendLine(" (");
            sql.AppendLine(" select * from T_LED_HISTORY_RISK_REDUCE t1 ");
            sql.AppendLine(" where not exists (");
            sql.AppendLine("  select ");
            sql.AppendLine("   1");
            sql.AppendLine("  from T_LED_HISTORY_RISK_REDUCE t2");
            sql.AppendLine("  where ");
            sql.AppendLine("   t1.LEDGER_HISTORY_ID = t2.LEDGER_HISTORY_ID ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.USAGE_LOC_SEQ = t2.USAGE_LOC_SEQ ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.LED_RISK_REDUCE_ID > t2.LED_RISK_REDUCE_ID)");
            sql.AppendLine(" ) T_LED_HISTORY_RISK_REDUCE");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID = T_LED_HISTORY_RISK_REDUCE.LEDGER_HISTORY_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ = T_LED_HISTORY_RISK_REDUCE.USAGE_LOC_SEQ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select LEDGER_HISTORY_ID, USAGE_LOC_SEQ, COUNT(1) RISK_REDUCE_COUNT from T_LED_HISTORY_RISK_REDUCE group by LEDGER_HISTORY_ID, USAGE_LOC_SEQ) RISK_REDUCE_COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID = RISK_REDUCE_COUNT.LEDGER_HISTORY_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ = RISK_REDUCE_COUNT.USAGE_LOC_SEQ");

            //2018/08/14 TPE Add Start
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select");
            sql.AppendLine("  LEDGER_HISTORY_ID,");
            sql.AppendLine("  USAGE_LOC_SEQ,");
            sql.AppendLine("  (select M_CHIEF.USER_CD + ';'");
            sql.AppendLine("   from M_CHIEF,T_LED_HISTORY_USAGE_LOC");
            sql.AppendLine("   where M_CHIEF.LOC_ID = T_LED_HISTORY_USAGE_LOC.LOC_ID");
            sql.AppendLine("   and A.LOC_ID = T_LED_HISTORY_USAGE_LOC.LOC_ID");

            //2018/10/05 Rin Add Start
            sql.AppendLine("   and A.LEDGER_HISTORY_ID = T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID");
            sql.AppendLine("   and A.LEDGER_FLOW_ID = T_LED_HISTORY_USAGE_LOC.LEDGER_FLOW_ID");
            sql.AppendLine("   and A.USAGE_LOC_SEQ = T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ");
            //2018/10/05 Rin Add End

            sql.AppendLine("   order by M_CHIEF.USER_CD");
            sql.AppendLine("   for xml path('')");
            sql.AppendLine("   ) as CHEM_OPERATION_CHIEF,");
            sql.AppendLine("   (select M_USER.USER_NAME + ';'");
            sql.AppendLine("   from M_CHIEF,T_LED_HISTORY_USAGE_LOC,M_USER");
            sql.AppendLine("   where M_CHIEF.LOC_ID = T_LED_HISTORY_USAGE_LOC.LOC_ID");
            sql.AppendLine("   and M_CHIEF.USER_CD = M_USER.USER_CD");
            sql.AppendLine("   and A.LOC_ID = T_LED_HISTORY_USAGE_LOC.LOC_ID");

            //2018/10/05 Rin Add Start
            sql.AppendLine("   and A.LEDGER_HISTORY_ID = T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID");
            sql.AppendLine("   and A.LEDGER_FLOW_ID = T_LED_HISTORY_USAGE_LOC.LEDGER_FLOW_ID");
            sql.AppendLine("   and A.USAGE_LOC_SEQ = T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ");
            //2018/10/05 Rin Add End

            sql.AppendLine("   order by M_CHIEF.USER_CD");
            sql.AppendLine("   for xml path('')");
            sql.AppendLine("  ) as CHEM_OPERATION_CHIEF_NAME");
            sql.AppendLine(" from");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC A");
            sql.AppendLine(" ) U");
            sql.AppendLine(" on");
            sql.AppendLine(" U.LEDGER_HISTORY_ID = T_LED_HISTORY_USAGE_LOC.LEDGER_HISTORY_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" U.USAGE_LOC_SEQ = T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ");
            //2018/08/14 TPE Add End


            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.LEDGER_HISTORY_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID = @LEDGER_HISTORY_ID");
                parameters.Add(nameof(condition.LEDGER_HISTORY_ID), condition.LEDGER_HISTORY_ID);

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.LEDGER_HISTORY_ID)), condition.LEDGER_HISTORY_ID.ToString());
            }

            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_FLOW_ID = @LEDGER_FLOW_ID");
                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.LEDGER_FLOW_ID)), condition.LEDGER_FLOW_ID.ToString());
            }

            if (condition.REG_NO != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY.REG_NO = @REG_NO");
                parameters.Add(nameof(condition.REG_NO), condition.REG_NO);

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_NO)), condition.REG_NO);
            }

            if (condition.REG_NO_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY.REG_NO in");
                sql.AppendLine(" (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.REG_NO_COLLECTION.ToList(), nameof(T_MGMT_LEDGER.REG_NO), parameters));
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_NO_COLLECTION)), string.Join(",", condition.REG_NO_COLLECTION));
            }

            if (condition.LEDGER_FLOW_ID_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_FLOW_ID in");
                sql.AppendLine(" (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.LEDGER_FLOW_ID_COLLECTION.ToList(), nameof(T_LEDGER_HISTORY.LEDGER_FLOW_ID), parameters));
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.LEDGER_FLOW_ID_COLLECTION)), string.Join(",", condition.LEDGER_FLOW_ID_COLLECTION));
            }

            if (!string.IsNullOrWhiteSpace(condition.CHEM_NAME))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY.CHEM_CD in");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_ALIAS.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_ALIAS");
                sql.AppendLine("  where");
                switch ((SearchConditionType)condition.CHEM_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) = @CHEM_NAME");
                        break;
                }
                sql.AppendLine(" )");
                if ((SearchConditionType)condition.CHEM_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.CHEM_NAME), condition.CHEM_NAME.ToUpper());
                }
                else
                {
                    parameters.Add(nameof(condition.CHEM_NAME), DbUtil.ConvertIntoEscapeChar(condition.CHEM_NAME.ToUpper()));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_NAME)), condition.CHEM_NAME_CONDITION, condition.CHEM_NAME);
            }

            // 商品名
            if (!string.IsNullOrEmpty(condition.PRODUCT_NAME))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY.CHEM_CD in");
                sql.AppendLine(" ( select M_CHEM_PRODUCT.CHEM_CD from M_CHEM_PRODUCT where");
                switch ((SearchConditionType)condition.PRODUCT_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME like '%' + @PRODUCT_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME like :PRODUCT_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME like '%' + @PRODUCT_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME = @PRODUCT_NAME");
                        break;
                }
                sql.AppendLine(")");
                if ((SearchConditionType)condition.PRODUCT_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.PRODUCT_NAME), condition.PRODUCT_NAME);
                }
                else
                {
                    parameters.Add(nameof(condition.PRODUCT_NAME), DbUtil.ConvertIntoEscapeChar(condition.PRODUCT_NAME));
                }
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.PRODUCT_NAME)), condition.PRODUCT_NAME_CONDITION, condition.PRODUCT_NAME);
            }

            if (!string.IsNullOrEmpty(condition.CAS_NO))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY.CAS_NO = @CAS_NO");
                parameters.Add(nameof(condition.CAS_NO), condition.CAS_NO);

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CAS_NO)), condition.CAS_NO);
            }

            // 化学物質コード
            if (!string.IsNullOrEmpty(condition.CHEM_CD))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY.CHEM_CD in");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_ALIAS.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_ALIAS");
                sql.AppendLine("  where");
                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_CD) =@CHEM_CD");
                sql.AppendLine(" )");

                parameters.Add(nameof(condition.CHEM_CD), condition.CHEM_CD.ToUpper());

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_CD)), condition.CHEM_CD);
            }

            if (!string.IsNullOrWhiteSpace(condition.FLOW_USER_CD))
            {
                if (condition.FLOW_SEARCH_TARGET == (int)FlowSearchTarget.Await)
                {
                    sql.AppendLine(" and ");
                    sql.AppendLine(" T_LEDGER_HISTORY.FLOW_CD IS NOT NULL");
                    sql.AppendLine(" and exists");
                    sql.AppendLine(" (");
                    sql.AppendLine("  select");
                    sql.AppendLine("   1");
                    sql.AppendLine("  from");
                    sql.AppendLine("   V_WORKFLOW ");
                    sql.AppendLine("  where");
                    sql.AppendLine("   V_WORKFLOW.FLOW_CD = T_LEDGER_HISTORY.FLOW_CD");
                    sql.AppendLine("   and");
                    sql.AppendLine("   V_WORKFLOW.HIERARCHY = (T_LEDGER_HISTORY.HIERARCHY + 1)");
                    sql.AppendLine("   and");
                    sql.AppendLine("   (");
                    sql.AppendLine("    V_WORKFLOW.APPROVAL_USER_CD = @FLOW_USER_CD");
                    sql.AppendLine("    or");
                    sql.AppendLine("    V_WORKFLOW.SUBSITUTE_USER_CD = @FLOW_USER_CD");
                    sql.AppendLine("   )");
                    sql.AppendLine(" )");
                    sql.AppendLine(" or");
                    sql.AppendLine(" (");
                    sql.AppendLine("  T_LEDGER_HISTORY.HIERARCHY IS NULL");
                    sql.AppendLine("  and exists");
                    sql.AppendLine("  (");
                    sql.AppendLine("   select");
                    sql.AppendLine("    1");
                    sql.AppendLine("   from");
                    sql.AppendLine("    M_USER");
                    sql.AppendLine("   where");
                    sql.AppendLine("    M_USER.USER_CD = T_LEDGER_HISTORY.REG_USER_CD");
                    sql.AppendLine("    and");
                    sql.AppendLine("    (");
                    sql.AppendLine("     M_USER.APPROVER1 = @FLOW_USER_CD");
                    sql.AppendLine("     or");
                    sql.AppendLine("     M_USER.APPROVER2 = @FLOW_USER_CD");
                    sql.AppendLine("    )");
                    sql.AppendLine("  )");
                    sql.AppendLine(" )");
                    parameters.Add(nameof(condition.FLOW_USER_CD), DbUtil.ConvertIntoEscapeChar(condition.FLOW_USER_CD));

                    MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.FLOW_USER_CD)), condition.FLOW_USER_CD);
                }
                else
                {
                    sql.AppendLine(" and exists");
                    sql.AppendLine(" (");
                    sql.AppendLine("  select");
                    sql.AppendLine("   1");
                    sql.AppendLine("  from");
                    sql.AppendLine("   V_WORKFLOW ");
                    sql.AppendLine("  where");
                    sql.AppendLine("   V_WORKFLOW.FLOW_CD = T_LEDGER_HISTORY.FLOW_CD");
                    sql.AppendLine("   and");
                    sql.AppendLine("   (");
                    sql.AppendLine("    V_WORKFLOW.APPROVAL_USER_CD = @FLOW_USER_CD");
                    sql.AppendLine("    or");
                    sql.AppendLine("    V_WORKFLOW.SUBSITUTE_USER_CD = @FLOW_USER_CD");
                    sql.AppendLine("   )");
                    sql.AppendLine("  )");
                    parameters.Add(nameof(condition.FLOW_USER_CD), DbUtil.ConvertIntoEscapeChar(condition.FLOW_USER_CD));

                    MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.FLOW_USER_CD)), condition.FLOW_USER_CD);
                }
            }
            sql.AppendLine(" )");

            return sql.ToString();
        }

        public IQueryable<LedgerCommonInfo> GetSearchResult(LedgerSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_HISTORY_ID,");
            sql.AppendLine(" T_LEDGER_HISTORY.LEDGER_FLOW_ID,");
            sql.AppendLine(" T_LEDGER_HISTORY.FLOW_CD,");
            sql.AppendLine(" T_LEDGER_HISTORY.HIERARCHY,");
            sql.AppendLine(" T_LEDGER_HISTORY.REG_NO,");
            sql.AppendLine(" T_LEDGER_HISTORY.APPLI_CLASS,");
            sql.AppendLine(" T_LEDGER_HISTORY.GROUP_CD,");
            sql.AppendLine(" T_LEDGER_HISTORY.REASON_ID,");
            sql.AppendLine(" T_LEDGER_HISTORY.OTHER_REASON,");
            sql.AppendLine(" T_LEDGER_HISTORY.CHEM_CD,");
            sql.AppendLine(" T_LEDGER_HISTORY.MAKER_NAME,");
            sql.AppendLine(" M_CHEM.CHEM_NAME,");
            sql.AppendLine(" M_CHEM.CAS_NO,");
            sql.AppendLine(" M_CHEM.FIGURE,");
            sql.AppendLine(" M_CHEM.CHEM_CAT,");
            sql.AppendLine(" V_HISTORY_LED_REGULATION.REG_TYPE_ID ARRAY_REG_TYPE_IDS,");
            sql.AppendLine(" M_USER.APPROVER1,");
            sql.AppendLine(" M_USER.APPROVER2,");
            sql.AppendLine(" T_LEDGER_HISTORY.OTHER_REGULATION,");
            sql.AppendLine(" T_LEDGER_HISTORY.SDS,");
            sql.AppendLine(" T_LEDGER_HISTORY.SDS_MIMETYPE,");
            sql.AppendLine(" T_LEDGER_HISTORY.SDS_FILENAME,");
            sql.AppendLine(" T_LEDGER_HISTORY.SDS_URL,");
            sql.AppendLine(" T_LEDGER_HISTORY.UNREGISTERED_CHEM,");
            sql.AppendLine(" T_LEDGER_HISTORY.UNREGISTERED_CHEM_MIMETYPE,");
            sql.AppendLine(" T_LEDGER_HISTORY.UNREGISTERED_CHEM_FILENAME,");
            sql.AppendLine(" T_LEDGER_HISTORY.MEMO,");
            sql.AppendLine(" T_LEDGER_HISTORY.REG_DATE,");
            sql.AppendLine(" T_LEDGER_HISTORY.UPD_DATE,");
            sql.AppendLine(" T_LEDGER_HISTORY.DEL_DATE,");
            sql.AppendLine(" T_LEDGER_HISTORY.DEL_FLAG,");
            sql.AppendLine(" REG_OLD_NO_COUNT, ");
            sql.AppendLine(" REG_OLD_NO,");
            sql.AppendLine(" CHEM_PRODUCT_COUNT, ");
            sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME,");

            sql.AppendLine(" case ");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY IS NULL THEN M_USER.APPROVER1");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 0 THEN M_WORKFLOW.APPROVAL_USER_CD1");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 1 THEN M_WORKFLOW.APPROVAL_USER_CD2");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 2 THEN M_WORKFLOW.APPROVAL_USER_CD3");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 3 THEN M_WORKFLOW.APPROVAL_USER_CD4");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 4 THEN M_WORKFLOW.APPROVAL_USER_CD5");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 5 THEN M_WORKFLOW.APPROVAL_USER_CD6");
            sql.AppendLine(" end APPROVAL_USER_CD,");

            sql.AppendLine(" case ");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY IS NULL THEN M_USER.APPROVER2");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 0 THEN M_WORKFLOW.SUBSITUTE_USER_CD1");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 1 THEN M_WORKFLOW.SUBSITUTE_USER_CD2");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 2 THEN M_WORKFLOW.SUBSITUTE_USER_CD3");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 3 THEN M_WORKFLOW.SUBSITUTE_USER_CD4");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 4 THEN M_WORKFLOW.SUBSITUTE_USER_CD5");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 5 THEN M_WORKFLOW.SUBSITUTE_USER_CD6");
            sql.AppendLine(" end SUBSITUTE_USER_CD,");

            sql.AppendLine(" case ");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 0 THEN '上長承認'");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 1 THEN M_WORKFLOW.APPROVAL_TARGET1");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 2 THEN M_WORKFLOW.APPROVAL_TARGET2");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 3 THEN M_WORKFLOW.APPROVAL_TARGET3");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 4 THEN M_WORKFLOW.APPROVAL_TARGET4");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 5 THEN M_WORKFLOW.APPROVAL_TARGET5");
            sql.AppendLine("  when T_LEDGER_HISTORY.HIERARCHY = 6 THEN M_WORKFLOW.APPROVAL_TARGET6");
            sql.AppendLine(" end APPROVAL_TARGET,");
            sql.AppendLine(" M_WORKFLOW.FLOW_HIERARCHY,");
            sql.AppendLine(" T_LEDGER_HISTORY.REG_USER_CD,");

            sql.AppendLine(" T_LEDGER_HISTORY_REGULATION.REG_TYPE_ID,");
            sql.AppendLine(" REGULATION_COUNT,");

            sql.AppendLine(" T_LED_HISTORY_LOC.LOC_SEQ,");
            sql.AppendLine(" T_LED_HISTORY_LOC.LOC_ID,");
            if (!condition.LOC_BASE)
            {
                sql.AppendLine(" LOC_COUNT,");
            }
            sql.AppendLine(" T_LED_HISTORY_LOC.AMOUNT,");
            sql.AppendLine(" T_LED_HISTORY_LOC.UNITSIZE_ID,");

            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.USAGE_LOC_SEQ,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.LOC_ID USAGE_LOC_ID,");
            if (!condition.USAGE_LOC_BASE)
            {
                sql.AppendLine(" USAGE_LOC_COUNT,");
            }
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.AMOUNT USAGE_LOC_AMOUNT,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.UNITSIZE_ID USAGE_LOC_UNITSIZE_ID,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.ISHA_USAGE,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.ISHA_OTHER_USAGE,");

            //2018/08/14 TPE Delete Start
            //sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.CHEM_OPERATION_CHIEF,");
            //2018/08/14 TPE Delete End

            //2018/08/14 TPE Add Start
            sql.AppendLine(" case when len(U.CHEM_OPERATION_CHIEF) > 0 then");
            sql.AppendLine("  left(U.CHEM_OPERATION_CHIEF,charindex(';',U.CHEM_OPERATION_CHIEF,len(U.CHEM_OPERATION_CHIEF)) -1)");
            sql.AppendLine(" else");
            sql.AppendLine("  null");
            sql.AppendLine(" end CHEM_OPERATION_CHIEF,");
            sql.AppendLine(" case when len(U.CHEM_OPERATION_CHIEF_NAME) > 0 then");
            sql.AppendLine("  left(U.CHEM_OPERATION_CHIEF_NAME,charindex(';',U.CHEM_OPERATION_CHIEF_NAME,len(U.CHEM_OPERATION_CHIEF_NAME)) -1)");
            sql.AppendLine(" else");
            sql.AppendLine("  null");
            sql.AppendLine(" end CHEM_OPERATION_CHIEF_NAME,");
            //2018/08/14 TPE Add End

            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.SUB_CONTAINER,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.ENTRY_DEVICE,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.OTHER_PROTECTOR,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.MEASURES_BEFORE_SCORE,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.MEASURES_BEFORE_RANK,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.MEASURES_AFTER_SCORE,");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC.MEASURES_AFTER_RANK,");
            sql.AppendLine(" T_LED_HISTORY_PROTECTOR.PROTECTOR_ID,");
            sql.AppendLine(" PROTECTOR_COUNT,");

            sql.AppendLine(" T_LED_HISTORY_ACCESMENT_REG.REG_TYPE_ID,");
            sql.AppendLine(" ACCESMENT_REG_COUNT,");

            sql.AppendLine(" T_LED_HISTORY_RISK_REDUCE.LED_RISK_REDUCE_TYPE,");
            sql.AppendLine(" T_LED_HISTORY_RISK_REDUCE.LED_RISK_REDUCE_FLAG AS REAL,");
            sql.AppendLine(" RISK_REDUCE_COUNT");

            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<LedgerSearchModel> records = DbUtil.Select<LedgerSearchModel>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<LedgerCommonInfo>();

            foreach (DataRow record in dt.Rows)
            {
                var model = new LedgerCommonInfo();
                model.TableType = TableType.History;
                model.LEDGER_HISTORY_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_HISTORY_ID)].ToString());
                model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)].ToString());
                model.FLOW_CD = record[nameof(model.FLOW_CD)].ToString();
                model.HIERARCHY = OrgConvert.ToNullableInt32(record[nameof(model.HIERARCHY)].ToString());
                model.REG_NO = record[nameof(model.REG_NO)].ToString();
                model.APPLI_CLASS = OrgConvert.ToNullableInt32(record[nameof(model.APPLI_CLASS)].ToString());
                model.GROUP_CD = record[nameof(model.GROUP_CD)].ToString();
                model.GROUP_NAME = groupDic.GetValueToString(model.GROUP_CD);
                model.REASON_ID = OrgConvert.ToNullableInt32(record[nameof(model.REASON_ID)].ToString());
                model.REASON = reasonDic.GetValueToString(model.REASON_ID);
                model.OTHER_REASON = record[nameof(model.OTHER_REASON)].ToString();
                model.CHEM_CD = record[nameof(model.CHEM_CD)].ToString();
                model.MAKER_NAME = record[nameof(model.MAKER_NAME)].ToString();
                if (model.CHEM_CD == NikonConst.UnregisteredChemCd)
                {
                    model.CHEM_NAME = NikonConst.UnregisteredChemName;
                }
                else
                {
                    model.CHEM_NAME = record[nameof(model.CHEM_NAME)].ToString();
                }
                model.CAS_NO = record[nameof(model.CAS_NO)].ToString();
                model.FIGURE = OrgConvert.ToNullableInt32(record[nameof(model.FIGURE)].ToString());
                model.FIGURE_NAME = figureDic.GetValueToString(model.FIGURE);
                model.CHEM_CAT = record[nameof(model.CHEM_CAT)].ToString();
                model.OTHER_REGULATION = record[nameof(model.OTHER_REGULATION)].ToString();
                if (record[nameof(model.SDS)] != DBNull.Value)
                {
                    model.SDS = (byte[])record[nameof(model.SDS)];
                    model.SDS_MIMETYPE = record[nameof(model.SDS_MIMETYPE)].ToString();
                    model.SDS_FILENAME = record[nameof(model.SDS_FILENAME)].ToString();
                }
                model.SDS_URL = record[nameof(model.SDS_URL)].ToString();
                if (record[nameof(model.UNREGISTERED_CHEM)] != DBNull.Value)
                {
                    model.UNREGISTERED_CHEM = (byte[])record[nameof(model.UNREGISTERED_CHEM)];
                    model.UNREGISTERED_CHEM_MIMETYPE = record[nameof(model.UNREGISTERED_CHEM_MIMETYPE)].ToString();
                    model.UNREGISTERED_CHEM_FILENAME = record[nameof(model.UNREGISTERED_CHEM_FILENAME)].ToString();
                }
                model.MEMO = record[nameof(model.MEMO)].ToString();
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)].ToString());
                model.UPD_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.UPD_DATE)].ToString());
                model.DEL_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.DEL_DATE)].ToString());
                model.DEL_FLAG = OrgConvert.ToNullableInt32(record[nameof(model.DEL_FLAG)].ToString());
                model.REG_OLD_NO_COUNT = OrgConvert.ToNullableInt32(record[nameof(model.REG_OLD_NO_COUNT)].ToString());
                model.REG_OLD_NO = record[nameof(model.REG_OLD_NO)].ToString();
                model.CHEM_PRODUCT_COUNT = OrgConvert.ToNullableInt32(record[nameof(model.CHEM_PRODUCT_COUNT)].ToString());
                model.PRODUCT_NAME = record[nameof(model.PRODUCT_NAME)].ToString();
                model.APPROVAL_TARGET = record[nameof(model.APPROVAL_TARGET)].ToString();
                model.APPROVAL_USER_CD = record[nameof(model.APPROVAL_USER_CD)].ToString();
                model.SUBSITUTE_USER_CD = record[nameof(model.SUBSITUTE_USER_CD)].ToString();
                model.FLOW_HIERARCHY = record[nameof(model.FLOW_HIERARCHY)].ToString();
                model.REG_USER_CD = record[nameof(model.REG_USER_CD)].ToString();
                model.APPLICANT = userDic.GetValueToString(model.REG_USER_CD);
                model.APPROVER1 = record[nameof(model.APPROVER1)].ToString();
                model.APPROVER2 = record[nameof(model.APPROVER2)].ToString();
                model.NID = model.REG_USER_CD;

                model.REG_TYPE_ID = OrgConvert.ToNullableInt32(record[nameof(model.REG_TYPE_ID)].ToString());
                model.ARRAY_REG_TYPE_IDS = record[nameof(model.ARRAY_REG_TYPE_IDS)].ToString();
                if (!condition.REGULATION_BASE)
                {
                    model.REGULATION_COUNT = OrgConvert.ToNullableInt32(record[nameof(model.REGULATION_COUNT)]);
                }
                if (model.ARRAY_REG_TYPE_IDS != "")
                {
                    var sqlReg = new StringBuilder();
                    sqlReg.AppendLine("select ");
                    sqlReg.AppendLine("V_REGULATION_ALL.REG_TEXT");
                    sqlReg.AppendLine("from ");
                    sqlReg.AppendLine("V_REGULATION_ALL where V_REGULATION_ALL.REG_TYPE_ID in (");
                    sqlReg.AppendLine(model.ARRAY_REG_TYPE_IDS);
                    sqlReg.AppendLine(") ");
                    sqlReg.AppendLine("and V_REGULATION_ALL.DEL_FLAG = 0 ");

                    List<V_REGULATION> recordsReg = DbUtil.Select<V_REGULATION>(sqlReg.ToString(),parameters);
                    //DataTable dt1 = ConvertToDataTable(records);
                    string strRegText = "";
                    foreach (var data in recordsReg)
                    {
                        if (strRegText != "")
                        {
                            strRegText += ",";
                        }
                        strRegText += data.REG_TEXT.ToString();
                    }
                    model.REG_TEXT = strRegText;
                }

                model.LOC_ID = OrgConvert.ToNullableInt32(record[nameof(model.LOC_ID)].ToString());
                if (!condition.LOC_BASE)
                {
                    model.LOC_COUNT = OrgConvert.ToNullableInt32(record[nameof(model.LOC_COUNT)]);
                }
                model.LOC_NAME = locationDic.GetValueToString(model.LOC_ID);
                model.AMOUNT = record[nameof(model.AMOUNT)].ToString();
                model.UNITSIZE_ID = OrgConvert.ToNullableInt32(record[nameof(model.UNITSIZE_ID)]);

                model.USAGE_LOC_ID = OrgConvert.ToNullableInt32(record[nameof(model.USAGE_LOC_ID)].ToString());
                if (!condition.USAGE_LOC_BASE)
                {
                    model.USAGE_LOC_COUNT = OrgConvert.ToNullableInt32(record[nameof(model.USAGE_LOC_COUNT)]);
                }
                model.USAGE_LOC_NAME = locationDic.GetValueToString(model.USAGE_LOC_ID);
                model.USAGE_LOC_AMOUNT = record[nameof(model.USAGE_LOC_AMOUNT)].ToString();
                model.USAGE_LOC_UNITSIZE_ID = OrgConvert.ToNullableInt32(record[nameof(model.USAGE_LOC_UNITSIZE_ID)]);
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
                model.ISHA_USAGE = OrgConvert.ToNullableInt32(record[nameof(model.ISHA_USAGE)]);
                model.ISHA_USAGE_TEXT = ishaUsageDic.GetValueToString(model.ISHA_USAGE);
                model.ISHA_OTHER_USAGE = record[nameof(model.ISHA_OTHER_USAGE)].ToString();
                model.CHEM_OPERATION_CHIEF = record[nameof(model.CHEM_OPERATION_CHIEF)].ToString();

                //2018/08/14 TPE Delete Start
                //model.CHEM_OPERATION_CHIEF_NAME = userDic.GetValueToString(model.CHEM_OPERATION_CHIEF);
                //2018/08/14 TPE Delete End

                //2018/08/14 TPE Add Start
                model.CHEM_OPERATION_CHIEF_NAME = record[nameof(model.CHEM_OPERATION_CHIEF_NAME)].ToString();
                //2018/08/14 TPE Add End

                model.SUB_CONTAINER = OrgConvert.ToNullableInt32(record[nameof(model.SUB_CONTAINER)]);
                model.SUB_CONTAINER_TEXT = subcontainerDic.GetValueToString(model.SUB_CONTAINER);
                model.ENTRY_DEVICE = record[nameof(model.ENTRY_DEVICE)].ToString();
                model.OTHER_PROTECTOR = record[nameof(model.OTHER_PROTECTOR)].ToString();
                model.MEASURES_BEFORE_SCORE = OrgConvert.ToNullableDecimal(record[nameof(model.MEASURES_BEFORE_SCORE)]);
                model.MEASURES_BEFORE_RANK = OrgConvert.ToNullableInt32(record[nameof(model.MEASURES_BEFORE_RANK)]);
                model.MEASURES_BEFORE_RANK_TEXT = rankDic.GetValueToString(model.MEASURES_BEFORE_RANK);
                model.MEASURES_AFTER_SCORE = OrgConvert.ToNullableDecimal(record[nameof(model.MEASURES_AFTER_SCORE)]);
                model.MEASURES_AFTER_RANK = OrgConvert.ToNullableInt32(record[nameof(model.MEASURES_AFTER_RANK)]);
                model.MEASURES_AFTER_RANK_TEXT = rankDic.GetValueToString(model.MEASURES_AFTER_RANK);
                model.PROTECTOR_ID = OrgConvert.ToNullableInt32(record[nameof(model.PROTECTOR_ID)]);
                model.PROTECTOR_NAME = protectorDic.GetValueToString(model.PROTECTOR_ID);
                model.PROTECTOR_COUNT = OrgConvert.ToNullableInt32(record[nameof(model.PROTECTOR_COUNT)]);

                model.LED_RISK_REDUCE_TYPE = OrgConvert.ToNullableInt32(record[nameof(model.LED_RISK_REDUCE_TYPE)]);
                if (model.LED_RISK_REDUCE_TYPE != null)
                {
                    if (Convert.ToInt32(record[nameof(model.LED_RISK_REDUCE_FLAG)]) == 1)
                    {
                        model.LED_RISK_REDUCE_FLAG = true;
                    }
                    else
                    {
                        model.LED_RISK_REDUCE_FLAG = false;
                    }
                }
                model.RISK_REDUCE_COUNT = OrgConvert.ToNullableInt32(record[nameof(model.RISK_REDUCE_COUNT)]);

                if (string.IsNullOrWhiteSpace(model.REG_NO))
                {
                    model.Status = LedgerStatusConditionType.Applying;
                }
                else if (model.APPLI_CLASS == (int)AppliClass.Stopped)
                {
                    model.Status = LedgerStatusConditionType.Abolished;
                }
                else
                {
                    model.Status = LedgerStatusConditionType.Using;
                }
                model.STATUS_TEXT = ledgerStatusDic.GetValueToString(model._Status);
                using (var regulationInfo = new LedRegulationMasterInfo())
                {
                    var regulations = regulationInfo.GetSelectedItems(model.REG_TYPE_ID_COLLECTION).AsQueryable();
                    model.REQUIRED_CHEM = regulations.Any(x => x.WORKTIME_MGMT == 1) ? "該当" : "非該当";
                    var hazmatText = regulations.FirstOrDefault(x => x.REG_TEXT.IndexOf("消防法") != -1)?.REG_TEXT;
                    model.HAZMAT_TYPE = hazmatText ?? "非該当";
                }
                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            if (condition.SearchLedgerList.Count() > 0)
            {
            }

            searchQuery.OrderBy(x => x.LEDGER_FLOW_ID);

            return searchQuery;
        }

        protected override void InitializeDictionary()
        {
            reasonDic = new CommonMasterInfo(NikonCommonMasterName.REASON).GetDictionary();
            userDic = new UserMasterInfo().GetDictionary();
            groupDic = new GroupMasterInfo().GetDictionary();
            figureDic = new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetDictionary();
            unitsizeDic = new UnitSizeMasterInfo().GetDictionary();
            unitsizeSelectList = new UnitSizeMasterInfo().GetSelectList();
            locationDic = new LocationMasterInfo().GetDictionary();
            ishaUsageDic = new IshaUsageMasterInfo().GetDictionary();
            subcontainerDic = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary();
            rankDic = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary();
            protectorDic = new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetDictionary();
            riskreduceDic = new CommonMasterInfo(NikonCommonMasterName.RISK_REDUCE).GetDictionary();
            ledgerStatusDic = new LedgerStatusListMasterInfo().GetDictionary();
            regulationDic = new LedRegulationMasterInfo().GetDictionary();
        }

        public int GetIdentity()
        {
            var sql = string.Empty;
            if (Common.DatabaseType == DatabaseType.Oracle)
            {
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                sql = "select @@IDENTITY ID";
            }
            var dt = DbUtil.Select(_Context,sql);
            return Convert.ToInt32(dt.Select()?.FirstOrDefault()["ID"]);
        }




        protected override void CreateInsertText(T_LEDGER_HISTORY value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(T_LEDGER_HISTORY value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        public override List<T_LEDGER_HISTORY> GetSearchResult(T_LEDGER_HISTORY condition)
        {
            throw new NotImplementedException();
        }

        public override void EditData(T_LEDGER_HISTORY Value)
        {
            throw new NotImplementedException();
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override void CreateInsertText(T_LEDGER_HISTORY value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.FLOW_CD) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.HIERARCHY) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.APPLI_CLASS) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.GROUP_CD) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.REASON_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.OTHER_REASON) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.CHEM_CD) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.MAKER_NAME) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.SDS) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.SDS_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.MEMO) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.FLOW_CD) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.HIERARCHY) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.REG_NO) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.APPLI_CLASS) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.GROUP_CD) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.REASON_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.OTHER_REASON) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.CHEM_CD) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.MAKER_NAME) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.SDS) + ",");
                builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.SDS_MIMETYPE) + ",");
                builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.MEMO) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LEDGER_HISTORY.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY.FLOW_CD), value.FLOW_CD);
            parameters.Add(nameof(T_LEDGER_HISTORY.HIERARCHY), OrgConvert.ToNullableInt32(value.HIERARCHY));
            parameters.Add(nameof(T_LEDGER_HISTORY.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_LEDGER_HISTORY.APPLI_CLASS), OrgConvert.ToNullableInt32(value.APPLI_CLASS));
            parameters.Add(nameof(T_LEDGER_HISTORY.GROUP_CD), value.GROUP_CD);
            parameters.Add(nameof(T_LEDGER_HISTORY.REASON_ID), OrgConvert.ToNullableInt32(value.REASON_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY.OTHER_REASON), value.OTHER_REASON);
            parameters.Add(nameof(T_LEDGER_HISTORY.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(T_LEDGER_HISTORY.MAKER_NAME), value.MAKER_NAME);
            parameters.Add(nameof(T_LEDGER_HISTORY.OTHER_REGULATION), value.OTHER_REGULATION);
            parameters.Add(nameof(T_LEDGER_HISTORY.REG_USER_CD), value.REG_USER_CD);
            if (value.SDS != null)
            {
                parameters.Add(nameof(T_LEDGER_HISTORY.SDS), value.SDS);
                parameters.Add(nameof(T_LEDGER_HISTORY.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_HISTORY.SDS_FILENAME), value.SDS_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_HISTORY.SDS_URL), value.SDS_URL);
            if (value.UNREGISTERED_CHEM != null)
            {
                parameters.Add(nameof(T_LEDGER_HISTORY.UNREGISTERED_CHEM), value.UNREGISTERED_CHEM);
                parameters.Add(nameof(T_LEDGER_HISTORY.UNREGISTERED_CHEM_MIMETYPE), value.UNREGISTERED_CHEM_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_HISTORY.UNREGISTERED_CHEM_FILENAME), value.UNREGISTERED_CHEM_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_HISTORY.MEMO), value.MEMO);

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LEDGER_HISTORY value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}