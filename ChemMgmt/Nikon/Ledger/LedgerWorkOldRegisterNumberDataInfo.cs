﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerWorkOldRegisterNumberDataInfo : DataInfoBase<T_LED_WORK_REG_OLD_NO>, IDisposable
    {
        protected override string TableName { get; } = nameof(T_LED_WORK_REG_OLD_NO);

        //protected override void CreateInsertText(T_LED_WORK_REG_OLD_NO value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
        //    parameters.Add(nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ), OrgConvert.ToNullableInt32(value.REG_OLD_NO_SEQ));
        //    parameters.Add(nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO), value.REG_OLD_NO);

        //    sql = builder.ToString();
        //}

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerOldRegisterNumberSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerOldRegisterNumberSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID = @LEDGER_FLOW_ID");
                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
            }

            sql.AppendLine(")");

            return sql.ToString();
        }

        //public override IQueryable<T_LED_WORK_REG_OLD_NO> GetSearchResult(T_LED_WORK_REG_OLD_NO condition)
        //{
        //    throw new NotImplementedException();
        //}

        public IQueryable<LedgerOldRegisterNumberModel> GetSearchResult(LedgerOldRegisterNumberSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_REG_OLD_NO.REG_DATE));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<LedgerOldRegisterNumberModel> records = DbUtil.Select<LedgerOldRegisterNumberModel>(sql.ToString(), parameters);
            //DataTable dt = ConvertToDataTable(records);
            //var dataArray = new List<LedgerOldRegisterNumberModel>();

            //foreach (DataRow record in dt.Rows)
            //{
            //    var model = new LedgerOldRegisterNumberModel();
            //    model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)]);
            //    model.REG_OLD_NO_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.REG_OLD_NO_SEQ)]);
            //    model.REG_OLD_NO = record[nameof(model.REG_OLD_NO)].ToString();
            //    model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
            //    dataArray.Add(model);
            //}

            var searchQuery = records.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.REG_OLD_NO_SEQ);

            return searchQuery;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override void InitializeDictionary() { }

        public override void EditData(T_LED_WORK_REG_OLD_NO Value) { }

        //protected override void CreateInsertText(T_LED_WORK_REG_OLD_NO value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
        //    parameters.Add(nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ), OrgConvert.ToNullableInt32(value.REG_OLD_NO_SEQ));
        //    parameters.Add(nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO), value.REG_OLD_NO);

        //    sql = builder.ToString();
        //}

        protected override void CreateUpdateText(T_LED_WORK_REG_OLD_NO value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.REG_OLD_NO) + " = :" + nameof(value.REG_OLD_NO));
            builder.AppendLine("where ");
            builder.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(value.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(value.REG_OLD_NO_SEQ), OrgConvert.ToNullableInt32(value.REG_OLD_NO_SEQ));
            parameters.Add(nameof(value.REG_OLD_NO), value.REG_OLD_NO);

            sql = builder.ToString();
        }

        /// <summary>
        /// 保存ロジック
        /// </summary>
        /// <param name="ledger">LedgerModel</param>
        /// <param name="oldRegisterNumbers">IEnumerable<LedgerOldRegisterNumberModel></param>
        public void Save(LedgerModel ledger, IEnumerable<LedgerOldRegisterNumberModel> oldRegisterNumbers)
        {
            var condition = new LedgerOldRegisterNumberSearchModel();
            condition.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
            var deleteDataArray = GetSearchResult(condition);
            Remove(deleteDataArray.Select(x => (T_LED_WORK_REG_OLD_NO)x), true);
            var i = 1;
            foreach (var oldRegisterNumber in oldRegisterNumbers)
            {
                oldRegisterNumber.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                oldRegisterNumber.REG_OLD_NO_SEQ = i;
                i++;
            }
            Add(oldRegisterNumbers.Select(x => (T_LED_WORK_REG_OLD_NO)x));
        }
        

        protected override void CreateInsertText(T_LED_WORK_REG_OLD_NO value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  @" + nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO_SEQ), OrgConvert.ToNullableInt32(value.REG_OLD_NO_SEQ));
            parameters.Add(nameof(T_LED_WORK_REG_OLD_NO.REG_OLD_NO), value.REG_OLD_NO);

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LED_WORK_REG_OLD_NO value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        public override List<T_LED_WORK_REG_OLD_NO> GetSearchResult(T_LED_WORK_REG_OLD_NO condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_LED_WORK_REG_OLD_NO value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}