﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerWorkRegulationDataInfo : DataInfoBase<T_LEDGER_WORK_REGULATION>, IDisposable
    {
        private IDictionary<int?, string> regulationDic { get; set; }

        private IDictionary<int?, string> locationDic { get; set; }

        private IDictionary<int?, string> ishaUsageDic { get; set; }

        private IDictionary<string, string> userDic { get; set; }

        private IDictionary<int?, string> subcontainerDic { get; set; }

        private IDictionary<int?, string> rankDic { get; set; }

        private IDictionary<int?, string> regTransactionVolumeDic { get; set; }

        private IDictionary<int?, string> regWorkFrequencyDic { get; set; }

        private IDictionary<int?, string> regDisasterPossibilityDic { get; set; }

        protected override string TableName { get; } = nameof(T_LEDGER_WORK_REGULATION);

        //protected override void CreateInsertText(T_LEDGER_WORK_REGULATION value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
        //    parameters.Add(nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID), OrgConvert.ToNullableInt32(value.LED_REGULATION_ID));
        //    parameters.Add(nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
        //    parameters.Add(nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD), Convert.ToString(value.REG_USER_CD));

        //    sql = builder.ToString();
        //}
        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerRegulationSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);
            sql.AppendLine("inner join ");
            sql.AppendLine("V_REGULATION_ALL on T_LEDGER_WORK_REGULATION.REG_TYPE_ID = V_REGULATION_ALL.REG_TYPE_ID");
            sql.AppendLine("inner join M_REGULATION on T_LEDGER_WORK_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID");

            return sql.ToString();
        }

        //protected override string WhereWardCreate(dynamic dynamicCondition, out Hashtable parameters)
        //{
        //    var condition = (LedgerRegulationSearchModel)dynamicCondition;
        //    parameters = new Hashtable();
        //    var sql = new StringBuilder();

        //    sql.AppendLine("where");
        //    sql.AppendLine("(");
        //    sql.AppendLine(" 1 = 1");

        //    if (condition.LEDGER_FLOW_ID != null)
        //    {
        //        sql.AppendLine(" and");
        //        sql.AppendLine(" T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID = :LEDGER_FLOW_ID");

        //        parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
        //    }

        //    sql.AppendLine(" )");

        //    return sql.ToString();
        //}

        //public override IQueryable<T_LEDGER_WORK_REGULATION> GetSearchResult(T_LEDGER_WORK_REGULATION condition)
        //{
        //    throw new NotImplementedException();
        //}

        //public IQueryable<LedgerRegulationModel> GetSearchResult(LedgerRegulationSearchModel condition)
        //{

        //    var sql = new StringBuilder();
        //    Hashtable parameters = null;

        //    sql.AppendLine("select");
        //    if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
        //    {
        //        sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
        //    }
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID) + ",");
        //    sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT) + ",");
        //    sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT_LAST) + ",");
        //    sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_ICON_PATH) + ",");
        //    sql.AppendLine("V_REGULATION_ALL." + nameof(M_REGULATION.WORKTIME_MGMT) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.REG_DATE));

        //    sql.AppendLine(FromWardCreate(condition));
        //    sql.AppendLine(WhereWardCreate(condition, out parameters));
        //    if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
        //    {
        //        sql.AppendLine(" and");
        //        sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
        //    }
        //    //StartTransaction();
        //    DataTable records = DbUtil.select(Context, sql.ToString(), parameters);

        //    var dataArray = new List<LedgerRegulationModel>();

        //    var protectorDataInfo = new LedgerWorkProtectorDataInfo();
        //    var riskreduceDataInfo = new LedgerWorkRiskReduceDataInfo();
        //    var accesmentregDataInfo = new LedgerWorkAccesmentRegDataInfo();

        //    foreach (DataRow record in records.Rows)
        //    {
        //        var model = new LedgerRegulationModel();
        //        model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)]);
        //        model.REG_TYPE_ID = Convert.ToInt32(record[nameof(model.REG_TYPE_ID)]);
        //        model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
        //        model.REG_USER_CD = userDic.GetValueToString(model.REG_USER_CD);
        //        model.LED_REGULATION_ID = OrgConvert.ToNullableInt32(record[nameof(model.LED_REGULATION_ID)]);
        //        model.REG_TEXT = regulationDic.GetValueToString(model.REG_TYPE_ID);
        //        model.REG_TEXT_LAST = Convert.ToString(record[nameof(model.REG_TEXT_LAST)]);
        //        model.REG_ICON_PATH = Convert.ToString(record[nameof(model.REG_ICON_PATH)]);
        //        model.WORKTIME_MGMT = OrgConvert.ToNullableInt32(record[nameof(model.WORKTIME_MGMT)]);

        //        dataArray.Add(model);
        //    }

        //    var searchQuery = dataArray.AsQueryable();

        //    searchQuery = searchQuery.OrderBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.LED_REGULATION_ID);

        //    return searchQuery;
        //}

        protected override void InitializeDictionary()
        {
            regulationDic = new LedRegulationMasterInfo().GetDictionary();
            locationDic = new LocationMasterInfo().GetDictionary();
            ishaUsageDic = new IshaUsageMasterInfo().GetDictionary();
            userDic = new UserMasterInfo().GetDictionary();
            subcontainerDic = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary();
            rankDic = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary();
            regTransactionVolumeDic = new CommonMasterInfo(NikonCommonMasterName.REG_TRANSACTION_VOLUME).GetDictionary();
            regWorkFrequencyDic = new CommonMasterInfo(NikonCommonMasterName.REG_WORK_FREQUENCY).GetDictionary();
            regDisasterPossibilityDic = new CommonMasterInfo(NikonCommonMasterName.REG_DISASTER_POSSIBILITY).GetDictionary();
        }

        public override void EditData(T_LEDGER_WORK_REGULATION Value)
        {
            throw new NotImplementedException();
        }

        //protected override void CreateInsertText(T_LEDGER_WORK_REGULATION value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
        //    parameters.Add(nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID), OrgConvert.ToNullableInt32(value.LED_REGULATION_ID));
        //    parameters.Add(nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
        //    parameters.Add(nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD), Convert.ToString(value.REG_USER_CD));

        //    sql = builder.ToString();
        //}

        protected override void CreateUpdateText(T_LEDGER_WORK_REGULATION value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.REG_TYPE_ID) + " = :" + nameof(value.REG_TYPE_ID));
            builder.AppendLine("where ");
            builder.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(value.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
            sql = builder.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    regulationDic = null;
                    locationDic = null;
                    ishaUsageDic = null;
                    userDic = null;
                    rankDic = null;
                    regTransactionVolumeDic = null;
                    regWorkFrequencyDic = null;
                    regDisasterPossibilityDic = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        /// <summary>
        /// 保存ロジック
        /// </summary>
        /// <param name="ledger">LedgerModel</param>
        /// <param name="regulations">IEnumerable<LedgerRegulationModel></param>
        public void Save(LedgerModel ledger, List<LedgerRegulationModel> regulations)
        {
            var condition = new LedgerRegulationSearchModel();
            condition.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
            var deleteDataArray = GetSearchResult(condition);
            Remove(deleteDataArray.Select(x => (T_LEDGER_WORK_REGULATION)x), true);
            var i = 1;
            foreach (var regulation in regulations)
            {
                regulation.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                regulation.LED_REGULATION_ID = i;
                i++;
            }
            Add(regulations.Select(x => (T_LEDGER_WORK_REGULATION)x));
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerRegulationSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID = @LEDGER_FLOW_ID");

                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
            }

            sql.AppendLine(" )");

            return sql.ToString();
        }

        protected override void CreateInsertText(T_LEDGER_WORK_REGULATION value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID), OrgConvert.ToNullableInt32(value.LED_REGULATION_ID));
            parameters.Add(nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
            parameters.Add(nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD), Convert.ToString(value.REG_USER_CD));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LEDGER_WORK_REGULATION value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }
        public IQueryable<LedgerRegulationModel> GetSearchResult(LedgerRegulationSearchModel condition)
        {

            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT_LAST) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_ICON_PATH) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(M_REGULATION.WORKTIME_MGMT) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.REG_DATE));

            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }
            sql.AppendLine(" order by  " + TableName + "." + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
            sql.AppendLine(" " + TableName + "." + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + " ");
            //StartTransaction();
            List<LedgerRegulationModel> records = DbUtil.Select<LedgerRegulationModel>(sql.ToString(), parameters);
            //DataTable dt = ConvertToDataTable(records);
            //var dataArray = new List<LedgerRegulationModel>();

            //var protectorDataInfo = new LedgerWorkProtectorDataInfo();
            //var riskreduceDataInfo = new LedgerWorkRiskReduceDataInfo();
            //var accesmentregDataInfo = new LedgerWorkAccesmentRegDataInfo();

            //foreach (DataRow record in dt.Rows)
            //{
            //    var model = new LedgerRegulationModel();
            //    model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)]);
            //    model.REG_TYPE_ID = Convert.ToInt32(record[nameof(model.REG_TYPE_ID)]);
            //    model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
            //    model.REG_USER_CD = userDic.GetValueToString(model.REG_USER_CD);
            //    model.LED_REGULATION_ID = OrgConvert.ToNullableInt32(record[nameof(model.LED_REGULATION_ID)]);
            //    model.REG_TEXT = regulationDic.GetValueToString(model.REG_TYPE_ID);
            //    model.REG_TEXT_LAST = Convert.ToString(record[nameof(model.REG_TEXT_LAST)]);
            //    model.REG_ICON_PATH = Convert.ToString(record[nameof(model.REG_ICON_PATH)]);
            //    model.WORKTIME_MGMT = OrgConvert.ToNullableInt32(record[nameof(model.WORKTIME_MGMT)]);

            //    dataArray.Add(model);
            //}

            var searchQuery = records.AsQueryable();

            //searchQuery = searchQuery.OrderBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.LED_REGULATION_ID);

            return searchQuery;
        }

        //public IQueryable<LedgerRegulationModel> GetSearchResult(LedgerRegulationSearchModel condition)
        //{

        //    var sql = new StringBuilder();
        //    DynamicParameters parameters = null;

        //    sql.AppendLine("select");
        //    if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
        //    {
        //        sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
        //    }
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.LED_REGULATION_ID) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.REG_TYPE_ID) + ",");
        //    sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT) + ",");
        //    sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT_LAST) + ",");
        //    sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_ICON_PATH) + ",");
        //    sql.AppendLine("V_REGULATION_ALL." + nameof(M_REGULATION.WORKTIME_MGMT) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.REG_USER_CD) + ",");
        //    sql.AppendLine(TableName + "." + nameof(T_LEDGER_WORK_REGULATION.REG_DATE));

        //    sql.AppendLine(FromWardCreate(condition));
        //    sql.AppendLine(WhereWardCreate(condition, out parameters));
        //    if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
        //    {
        //        sql.AppendLine(" and");
        //        sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
        //    }
        //    //StartTransaction();
        //    List<LedgerRegulationSearchModel> records = DbUtil.Select<LedgerRegulationSearchModel>(sql.ToString(), parameters);
        //    DataTable dt = ConvertToDataTable(records);
        //    var dataArray = new List<LedgerRegulationModel>();

        //    var protectorDataInfo = new LedgerWorkProtectorDataInfo();
        //    var riskreduceDataInfo = new LedgerWorkRiskReduceDataInfo();
        //    var accesmentregDataInfo = new LedgerWorkAccesmentRegDataInfo();

        //    foreach (DataRow record in dt.Rows)
        //    {
        //        var model = new LedgerRegulationModel();
        //        model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)]);
        //        model.REG_TYPE_ID = Convert.ToInt32(record[nameof(model.REG_TYPE_ID)]);
        //        model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
        //        model.REG_USER_CD = userDic.GetValueToString(model.REG_USER_CD);
        //        model.LED_REGULATION_ID = OrgConvert.ToNullableInt32(record[nameof(model.LED_REGULATION_ID)]);
        //        model.REG_TEXT = regulationDic.GetValueToString(model.REG_TYPE_ID);
        //        model.REG_TEXT_LAST = Convert.ToString(record[nameof(model.REG_TEXT_LAST)]);
        //        model.REG_ICON_PATH = Convert.ToString(record[nameof(model.REG_ICON_PATH)]);
        //        model.WORKTIME_MGMT = OrgConvert.ToNullableInt32(record[nameof(model.WORKTIME_MGMT)]);

        //        dataArray.Add(model);
        //    }

        //    var searchQuery = dataArray.AsQueryable();

        //    searchQuery = searchQuery.OrderBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.LED_REGULATION_ID);

        //    return searchQuery;
        //}

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        public override List<T_LEDGER_WORK_REGULATION> GetSearchResult(T_LEDGER_WORK_REGULATION condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_LEDGER_WORK_REGULATION value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}