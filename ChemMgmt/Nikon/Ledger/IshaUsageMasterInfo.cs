﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ChemMgmt.Classes.util;
using ZyCoaG.Util;
using ZyCoaG.Classes.util;

namespace ChemMgmt.Nikon.Ledger
{
    public class IshaUsageMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        /// <summary>
        /// データを全て取得します。
        /// </summary>
        /// <returns>データを<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" M_ISHA_USAGE.KEY_VALUE as ISHA_USAGE_ID,");
            sql.AppendLine(" M_ISHA_USAGE.DISPLAY_VALUE as ISHA_USAGE_TEXT,");
            sql.AppendLine(" M_ISHA_USAGE.DISPLAY_ORDER as ISHA_USAGE_ORDER,");
            sql.AppendLine(" M_ISHA_USAGE.DEL_FLAG as DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" M_COMMON M_ISHA_USAGE");
            sql.AppendLine("where");
            sql.AppendLine(" TABLE_NAME = 'M_ISHA_USAGE'");

            var context = DbUtil.Open(true);
            var records = DbUtil.Select(context, sql.ToString(), new Hashtable());
            context.Close();

            var collection = new List<CommonDataModel<int?>>();
            foreach (DataRow record in records.Rows)
            {
                collection.Add(new CommonDataModel<int?>
                {
                    Id = OrgConvert.ToNullableInt32(record["ISHA_USAGE_ID"].ToString()),
                    Name = record["ISHA_USAGE_TEXT"].ToString(),
                    Order = Convert.ToInt32(record["ISHA_USAGE_ORDER"].ToString()),
                    IsDeleted = Convert.ToInt32(record["DEL_FLAG"].ToString()).ToBool()
                });
            };
            return collection;
        }
    }
}