﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using ChemMgmt.Classes;
using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ChemMgmt.Nikon.Ledger;
using ChemMgmt.Nikon.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.History;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon
{
    public class LedgerWorkDataInfo : DataInfoBase<T_LEDGER_WORK>
    {
        public LedgerWorkDataInfo()
        {
            reasonDic = new CommonMasterInfo(NikonCommonMasterName.REASON).GetDictionary();
            userDic = new UserMasterInfo().GetDictionary();
            groupDic = new GroupMasterInfo().GetDictionary();
            figureDic = new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetDictionary();
            unitsizeDic = new UnitSizeMasterInfo().GetDictionary();
            unitsizeSelectList = new UnitSizeMasterInfo().GetSelectList();
            locationDic = new LocationMasterInfo().GetDictionary();
            ishaUsageDic = new IshaUsageMasterInfo().GetDictionary();
            subcontainerDic = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary();
            rankDic = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary();
            protectorDic = new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetDictionary();
            riskreduceDic = new CommonMasterInfo(NikonCommonMasterName.RISK_REDUCE).GetDictionary();
            ledgerStatusDic = new LedgerStatusListMasterInfo().GetDictionary();
            regulationDic = new LedRegulationMasterInfo().GetDictionary();
        }

        public LedgerWorkDataInfo(int i)
        {
            switch (i)
            {
                case 1:
                    userDic = new UserMasterInfo().GetDictionary();
                    groupDic = new GroupMasterInfo().GetDictionary();
                    ledgerStatusDic = new LedgerStatusListMasterInfo().GetDictionary();
                    break;

                //管理台帳検索時
                case 2:
                    reasonDic = new CommonMasterInfo(NikonCommonMasterName.REASON).GetDictionary();
                    userDic = new UserMasterInfo().GetDictionary();
                    groupDic = new GroupMasterInfo().GetDictionary();
                    figureDic = new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetDictionary();
                    locationDic = new LocationMasterInfo().GetDictionary();
                    ishaUsageDic = new IshaUsageMasterInfo().GetDictionary();
                    subcontainerDic = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary();
                    rankDic = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary();
                    protectorDic = new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetDictionary();
                    riskreduceDic = new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetDictionary();
                    ledgerStatusDic = new LedgerStatusListMasterInfo().GetDictionary();
                    regulationDic = new LedRegulationMasterInfo().GetDictionary();
                    break;
            }
        }
        protected override string TableName { get; } = nameof(T_LEDGER_WORK);

        private IDictionary<int?, string> reasonDic { get; set; }

        private IDictionary<string, string> userDic { get; set; }

        private IDictionary<string, string> groupDic { get; set; }

        private IDictionary<int?, string> figureDic { get; set; }

        private IDictionary<int?, string> unitsizeDic { get; set; }

        private IEnumerable<CommonDataModel<int?>> unitsizeSelectList { get; set; }

        private IDictionary<int?, string> locationDic { get; set; }

        private IDictionary<int?, string> ishaUsageDic { get; set; }

        private IDictionary<int?, string> subcontainerDic { get; set; }

        private IDictionary<int?, string> rankDic { get; set; }

        private IDictionary<int?, string> protectorDic { get; set; }

        private IDictionary<int?, string> ledgerStatusDic { get; set; }

        private IDictionary<int?, string> regulationDic { get; set; }

        private IDictionary<int?, string> riskreduceDic { get; set; }

        protected override void CreateInsertText(T_LEDGER_WORK value, out string sql, out Hashtable parameters)
        {

            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.FLOW_CD) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.HIERARCHY) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.APPLI_CLASS) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.GROUP_CD) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.REASON_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.OTHER_REASON) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.CHEM_CD) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.MAKER_NAME) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.SDS) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.SDS_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.MEMO) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.FLOW_CD) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.HIERARCHY) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.REG_NO) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.APPLI_CLASS) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.GROUP_CD) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.REASON_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.OTHER_REASON) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.CHEM_CD) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.MAKER_NAME) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.SDS) + ",");
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.SDS_MIMETYPE) + ",");
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.MEMO) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LEDGER_WORK.FLOW_CD), value.FLOW_CD);
            parameters.Add(nameof(T_LEDGER_WORK.HIERARCHY), OrgConvert.ToNullableInt32(value.HIERARCHY));
            parameters.Add(nameof(T_LEDGER_WORK.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_LEDGER_WORK.APPLI_CLASS), OrgConvert.ToNullableInt32(value.APPLI_CLASS));
            parameters.Add(nameof(T_LEDGER_WORK.GROUP_CD), value.GROUP_CD);
            parameters.Add(nameof(T_LEDGER_WORK.REASON_ID), OrgConvert.ToNullableInt32(value.REASON_ID));
            parameters.Add(nameof(T_LEDGER_WORK.OTHER_REASON), value.OTHER_REASON);
            parameters.Add(nameof(T_LEDGER_WORK.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(T_LEDGER_WORK.MAKER_NAME), value.MAKER_NAME);//TODO:修正待ち
            parameters.Add(nameof(T_LEDGER_WORK.OTHER_REGULATION), value.OTHER_REGULATION);
            if (value.SDS != null)
            {
                parameters.Add(nameof(T_LEDGER_WORK.SDS), value.SDS);
                parameters.Add(nameof(T_LEDGER_WORK.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_WORK.SDS_FILENAME), value.SDS_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_WORK.SDS_URL), value.SDS_URL);
            if (value.UNREGISTERED_CHEM != null)
            {
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM), value.UNREGISTERED_CHEM);
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE), value.UNREGISTERED_CHEM_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME), value.UNREGISTERED_CHEM_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_WORK.MEMO), value.MEMO);
            parameters.Add(nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG), value.TEMP_SAVED_FLAG);
            parameters.Add(nameof(T_LEDGER_WORK.REG_USER_CD), value.REG_USER_CD);

            sql = builder.ToString();
        }

        protected string FromWardCreate2(dynamic dynamicCondition)
        {
            var condition = (LedgerSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);

            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_USER");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_WORK.REG_USER_CD = M_USER.USER_CD");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" (");
            sql.AppendLine("  select LEDGER_FLOW_ID, REG_OLD_NO from T_LED_WORK_REG_OLD_NO O1 where REG_OLD_NO_SEQ = ");
            sql.AppendLine("  (");
            sql.AppendLine("   select MIN(REG_OLD_NO_SEQ) from T_LED_WORK_REG_OLD_NO O2 where O1.LEDGER_FLOW_ID = O2.LEDGER_FLOW_ID");
            sql.AppendLine("  )");
            sql.AppendLine(" ) OLD_NO");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = OLD_NO.LEDGER_FLOW_ID");

            sql.AppendLine(" left join T_LED_WORK_HISTORY" +
                " on T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_HISTORY.LEDGER_FLOW_ID" +
                " and T_LED_WORK_HISTORY.LED_WORK_HISTORY_ID =" +
                " (select max(LED_WORK_HISTORY_ID) from T_LED_WORK_HISTORY" +
                " where LEDGER_FLOW_ID = T_LEDGER_WORK.LEDGER_FLOW_ID and HIERARCHY = T_LEDGER_WORK.HIERARCHY+1)");

            return sql.ToString();
        }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);

            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_CHEM");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_WORK.CHEM_CD = M_CHEM.CHEM_CD");

            sql.AppendLine("  left join");
            sql.AppendLine(" (");
            sql.AppendLine("select * from M_CHEM_PRODUCT t1");
            sql.AppendLine("where not exists(");
            sql.AppendLine("select");
            sql.AppendLine("  1");
            sql.AppendLine(" from M_CHEM_PRODUCT t2");
            sql.AppendLine("where");
            sql.AppendLine("  t1.CHEM_CD = t2.CHEM_CD");
            sql.AppendLine("  and");
            sql.AppendLine("  t1.PRODUCT_SEQ > t2.PRODUCT_SEQ)");
            sql.AppendLine(") M_CHEM_PRODUCT");
            sql.AppendLine(" on");
            sql.AppendLine(" M_CHEM.CHEM_CD = M_CHEM_PRODUCT.CHEM_CD");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select CHEM_CD, COUNT(1) CHEM_PRODUCT_COUNT from M_CHEM_PRODUCT group by CHEM_CD) CHEM_PRODUCT_COUNT");
            sql.AppendLine("on");
            sql.AppendLine(" M_CHEM.CHEM_CD = CHEM_PRODUCT_COUNT.CHEM_CD");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select LEDGER_FLOW_ID, COUNT(1) REG_OLD_NO_COUNT from T_LED_WORK_REG_OLD_NO group by LEDGER_FLOW_ID) REG_OLD_NO_COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = REG_OLD_NO_COUNT.LEDGER_FLOW_ID");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_WORK_LED_REGULATION");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = V_WORK_LED_REGULATION.LEDGER_FLOW_ID");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_USER");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_WORK.REG_USER_CD = M_USER.USER_CD");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" (");
            sql.AppendLine("  select LEDGER_FLOW_ID, REG_OLD_NO from T_LED_WORK_REG_OLD_NO O1 where REG_OLD_NO_SEQ = ");
            sql.AppendLine("  (");
            sql.AppendLine("   select MIN(REG_OLD_NO_SEQ) from T_LED_WORK_REG_OLD_NO O2 where O1.LEDGER_FLOW_ID = O2.LEDGER_FLOW_ID");
            sql.AppendLine("  )");
            sql.AppendLine(" ) OLD_NO");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = OLD_NO.LEDGER_FLOW_ID");


            if (condition.REGULATION_BASE)
            {
                sql.AppendLine(" left join T_LEDGER_WORK_REGULATION on T_LEDGER_WORK.LEDGER_FLOW_ID = T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID");
            }
            else
            {
                sql.AppendLine(" left join");
                sql.AppendLine(" (");
                sql.AppendLine(" select * from T_LEDGER_WORK_REGULATION t1 ");
                sql.AppendLine(" where not exists (");
                sql.AppendLine("  select ");
                sql.AppendLine("   1");
                sql.AppendLine("  from T_LEDGER_WORK_REGULATION t2 ");
                sql.AppendLine("  where ");
                sql.AppendLine("   t1.LEDGER_FLOW_ID = t2.LEDGER_FLOW_ID ");
                sql.AppendLine("   and ");
                sql.AppendLine("   t1.LED_REGULATION_ID > t2.LED_REGULATION_ID ");
                sql.AppendLine("  group by LEDGER_FLOW_ID)");
                sql.AppendLine(" ) T_LEDGER_WORK_REGULATION");
                sql.AppendLine(" on");

                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = T_LEDGER_WORK_REGULATION.LEDGER_FLOW_ID");
                sql.AppendLine(" left outer join");
                sql.AppendLine(" (select LEDGER_FLOW_ID, COUNT(1) REGULATION_COUNT from T_LEDGER_WORK_REGULATION group by LEDGER_FLOW_ID) REGULATION_COUNT");
                sql.AppendLine(" on");
                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = REGULATION_COUNT.LEDGER_FLOW_ID");
            }

            if (condition.LOC_BASE)
            {
                sql.AppendLine(" left join T_LED_WORK_LOC on T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_LOC.LEDGER_FLOW_ID");
            }
            else
            {
                sql.AppendLine(" left join");
                sql.AppendLine(" (");
                sql.AppendLine(" select * from T_LED_WORK_LOC t1 ");
                sql.AppendLine(" where not exists (");
                sql.AppendLine("  select ");
                sql.AppendLine("   1");
                sql.AppendLine("  from T_LED_WORK_LOC t2 ");
                sql.AppendLine("  where ");
                sql.AppendLine("   t1.LEDGER_FLOW_ID = t2.LEDGER_FLOW_ID ");
                sql.AppendLine("   and ");
                sql.AppendLine("   t1.LOC_SEQ > t2.LOC_SEQ ");
                sql.AppendLine("  group by LEDGER_FLOW_ID)");
                sql.AppendLine(" ) T_LED_WORK_LOC");
                sql.AppendLine(" on");

                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_LOC.LEDGER_FLOW_ID");
                sql.AppendLine(" left outer join");
                sql.AppendLine(" (select LEDGER_FLOW_ID, COUNT(1) LOC_COUNT from T_LED_WORK_LOC group by LEDGER_FLOW_ID) LOC_COUNT");
                sql.AppendLine(" on");
                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = LOC_COUNT.LEDGER_FLOW_ID");
            }

            if (condition.USAGE_LOC_BASE)
            {
                sql.AppendLine(" left join T_LED_WORK_USAGE_LOC on T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID");
            }
            else
            {
                sql.AppendLine(" left join");
                sql.AppendLine(" (");
                sql.AppendLine(" select * from T_LED_WORK_USAGE_LOC t1 ");
                sql.AppendLine(" where not exists (");
                sql.AppendLine("  select ");
                sql.AppendLine("   1");
                sql.AppendLine("  from T_LED_WORK_USAGE_LOC t2");
                sql.AppendLine("  where ");
                sql.AppendLine("   t1.LEDGER_FLOW_ID = t2.LEDGER_FLOW_ID ");
                sql.AppendLine("   and");
                sql.AppendLine("   t1.USAGE_LOC_SEQ > t2.USAGE_LOC_SEQ)");
                sql.AppendLine(" ) T_LED_WORK_USAGE_LOC");
                sql.AppendLine(" on");
                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID");

                sql.AppendLine(" left outer join");
                sql.AppendLine(" (select LEDGER_FLOW_ID, COUNT(1) USAGE_LOC_COUNT from T_LED_WORK_USAGE_LOC group by LEDGER_FLOW_ID) USAGE_LOC_COUNT");
                sql.AppendLine(" on");
                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = USAGE_LOC_COUNT.LEDGER_FLOW_ID");
            }

            sql.AppendLine(" left join");
            sql.AppendLine(" (");
            sql.AppendLine(" select * from T_LED_WORK_PROTECTOR t1 ");
            sql.AppendLine(" where not exists (");
            sql.AppendLine("  select ");
            sql.AppendLine("   1");
            sql.AppendLine("  from T_LED_WORK_PROTECTOR t2");
            sql.AppendLine("  where ");
            sql.AppendLine("   t1.LEDGER_FLOW_ID = t2.LEDGER_FLOW_ID ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.USAGE_LOC_SEQ = t2.USAGE_LOC_SEQ ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.PROTECTOR_SEQ > t2.PROTECTOR_SEQ)");
            sql.AppendLine(" ) T_LED_WORK_PROTECTOR");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID = T_LED_WORK_PROTECTOR.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ = T_LED_WORK_PROTECTOR.USAGE_LOC_SEQ");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select LEDGER_FLOW_ID, USAGE_LOC_SEQ, COUNT(1) PROTECTOR_COUNT from T_LED_WORK_PROTECTOR group by LEDGER_FLOW_ID, USAGE_LOC_SEQ) PROTECTOR_COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID = PROTECTOR_COUNT.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ = PROTECTOR_COUNT.USAGE_LOC_SEQ");

            sql.AppendLine(" left join");
            sql.AppendLine(" (");
            sql.AppendLine(" select * from T_LED_WORK_ACCESMENT_REG t1 ");
            sql.AppendLine(" where not exists (");
            sql.AppendLine("  select ");
            sql.AppendLine("   1");
            sql.AppendLine("  from T_LED_WORK_ACCESMENT_REG t2");
            sql.AppendLine("  where ");
            sql.AppendLine("   t1.LEDGER_FLOW_ID = t2.LEDGER_FLOW_ID ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.USAGE_LOC_SEQ = t2.USAGE_LOC_SEQ ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.LED_ACCESMENT_REG_ID > t2.LED_ACCESMENT_REG_ID)");
            sql.AppendLine(" ) T_LED_WORK_ACCESMENT_REG");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID = T_LED_WORK_ACCESMENT_REG.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ = T_LED_WORK_ACCESMENT_REG.USAGE_LOC_SEQ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select LEDGER_FLOW_ID, USAGE_LOC_SEQ, COUNT(1) ACCESMENT_REG_COUNT from T_LED_WORK_ACCESMENT_REG group by LEDGER_FLOW_ID, USAGE_LOC_SEQ) ACCESMENT_REG_COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID = ACCESMENT_REG_COUNT.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ = ACCESMENT_REG_COUNT.USAGE_LOC_SEQ");

            sql.AppendLine(" left join");
            sql.AppendLine(" (");
            sql.AppendLine(" select * from T_LED_WORK_RISK_REDUCE t1 ");
            sql.AppendLine(" where not exists (");
            sql.AppendLine("  select ");
            sql.AppendLine("   1");
            sql.AppendLine("  from T_LED_WORK_RISK_REDUCE t2");
            sql.AppendLine("  where ");
            sql.AppendLine("   t1.LEDGER_FLOW_ID = t2.LEDGER_FLOW_ID ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.USAGE_LOC_SEQ = t2.USAGE_LOC_SEQ ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.LED_RISK_REDUCE_ID > t2.LED_RISK_REDUCE_ID)");
            sql.AppendLine(" ) T_LED_WORK_RISK_REDUCE");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID = T_LED_WORK_RISK_REDUCE.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ = T_LED_WORK_RISK_REDUCE.USAGE_LOC_SEQ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select LEDGER_FLOW_ID, USAGE_LOC_SEQ, COUNT(1) RISK_REDUCE_COUNT from T_LED_WORK_RISK_REDUCE group by LEDGER_FLOW_ID, USAGE_LOC_SEQ) RISK_REDUCE_COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID = RISK_REDUCE_COUNT.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ = RISK_REDUCE_COUNT.USAGE_LOC_SEQ");

            sql.AppendLine(" left join T_LED_WORK_HISTORY" +
                " on T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_HISTORY.LEDGER_FLOW_ID" +
                " and T_LED_WORK_HISTORY.LED_WORK_HISTORY_ID =" +
                " (select max(LED_WORK_HISTORY_ID) from T_LED_WORK_HISTORY" +
                " where LEDGER_FLOW_ID = T_LEDGER_WORK.LEDGER_FLOW_ID and HIERARCHY = T_LEDGER_WORK.HIERARCHY+1)");

            //2018/08/14 TPE Add Start
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select");
            sql.AppendLine(" LEDGER_FLOW_ID,");
            sql.AppendLine(" CLASS_ID,");
            sql.AppendLine(" USAGE_LOC_SEQ,");
            sql.AppendLine(" (select M_CHIEF.USER_CD + ';'");
            sql.AppendLine("  from M_CHIEF,T_LED_WORK_USAGE_LOC");
            sql.AppendLine("  where M_CHIEF.LOC_ID = T_LED_WORK_USAGE_LOC.LOC_ID");
            sql.AppendLine("  and A.LOC_ID = T_LED_WORK_USAGE_LOC.LOC_ID");
            sql.AppendLine("  and M_CHIEF.LOC_ID = B.LOC_ID");
            sql.AppendLine("  and A.LEDGER_FLOW_ID = T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID"); //2018/10/05 Rin Add
            sql.AppendLine("  and A.USAGE_LOC_SEQ = T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ"); //2018/10/05 Rin Add
            sql.AppendLine("  group by USER_CD");
            sql.AppendLine("  order by M_CHIEF.USER_CD");
            sql.AppendLine("  for xml path('')");
            sql.AppendLine(" ) as CHEM_OPERATION_CHIEF,");
            sql.AppendLine(" (select M_USER.USER_NAME + ';'");
            sql.AppendLine("  from M_CHIEF,T_LED_WORK_USAGE_LOC,M_USER");
            sql.AppendLine("  where M_CHIEF.LOC_ID = T_LED_WORK_USAGE_LOC.LOC_ID");
            sql.AppendLine("  and M_CHIEF.USER_CD = M_USER.USER_CD");
            sql.AppendLine("  and A.LOC_ID = T_LED_WORK_USAGE_LOC.LOC_ID");
            sql.AppendLine("  and M_CHIEF.LOC_ID = B.LOC_ID");
            sql.AppendLine("  and A.LEDGER_FLOW_ID = T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID"); //2018/10/05 Rin Add
            sql.AppendLine("  and A.USAGE_LOC_SEQ = T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ"); //2018/10/05 Rin Add
            sql.AppendLine("  group by USER_NAME,M_CHIEF.USER_CD");
            sql.AppendLine("  order by M_CHIEF.USER_CD");
            sql.AppendLine("  for xml path('')");
            sql.AppendLine(" ) as CHEM_OPERATION_CHIEF_NAME");
            sql.AppendLine(" from");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC A");
            sql.AppendLine(" ,M_LOCATION B");
            sql.AppendLine(" where");
            sql.AppendLine(" A.LOC_ID = B.LOC_ID");
            sql.AppendLine(" ) U");
            sql.AppendLine(" on");
            sql.AppendLine(" U.LEDGER_FLOW_ID = T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" U.USAGE_LOC_SEQ = T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ");
            //2018/08/14 TPE Add End

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            // 申請番号
            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID = @LEDGER_FLOW_ID");
                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);

                MakeConditionInLineOfCsv("申請番号", condition.LEDGER_FLOW_ID.ToString());
            }

            if (condition.LEDGER_FLOW_ID_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID in");
                sql.AppendLine(" (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.LEDGER_FLOW_ID_COLLECTION.ToList(), nameof(T_LEDGER_WORK.LEDGER_FLOW_ID), parameters));
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.LEDGER_FLOW_ID_COLLECTION)), string.Join(",", condition.LEDGER_FLOW_ID_COLLECTION));
            }

            if (!string.IsNullOrWhiteSpace(condition.CHEM_NAME))
            {
                condition.CHEM_NAME = condition.CHEM_NAME.Trim();
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_WORK.CHEM_CD in");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_ALIAS.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_ALIAS");
                sql.AppendLine("  where");
                switch ((SearchConditionType)condition.CHEM_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) = @CHEM_NAME");
                        break;
                }
                sql.AppendLine(" )");
                if ((SearchConditionType)condition.CHEM_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.CHEM_NAME), condition.CHEM_NAME.ToUpper());
                }
                else
                {
                    parameters.Add(nameof(condition.CHEM_NAME), DbUtil.ConvertIntoEscapeChar(condition.CHEM_NAME.ToUpper()));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_NAME)), condition.CHEM_NAME_CONDITION, condition.CHEM_NAME);
            }

            // 商品名
            if (!string.IsNullOrEmpty(condition.PRODUCT_NAME))
            {
                condition.PRODUCT_NAME = condition.PRODUCT_NAME.Trim();
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_WORK.CHEM_CD in");
                sql.AppendLine(" ( select M_CHEM_PRODUCT.CHEM_CD from M_CHEM_PRODUCT where");
                switch ((SearchConditionType)condition.PRODUCT_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME like '%' + @PRODUCT_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME like @PRODUCT_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME like '%' + @PRODUCT_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME = @PRODUCT_NAME");
                        break;
                }
                sql.AppendLine(")");
                if ((SearchConditionType)condition.PRODUCT_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.PRODUCT_NAME), condition.PRODUCT_NAME);
                }
                else
                {
                    parameters.Add(nameof(condition.PRODUCT_NAME), DbUtil.ConvertIntoEscapeChar(condition.PRODUCT_NAME));
                }
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.PRODUCT_NAME)), condition.PRODUCT_NAME_CONDITION, condition.PRODUCT_NAME);


                //MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.ACTION_LOC_COLLECTION)),
                //    condition.ACTION_LOC_COLLECTION, locationDic);
            }

            if (!string.IsNullOrEmpty(condition.CAS_NO))
            {
                condition.CAS_NO = condition.CAS_NO.Trim();
                sql.AppendLine(" and");
                switch ((SearchConditionType)condition.CAS_NO_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO like '%' + @CAS_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO like @CAS_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO like '%' + @CAS_NO " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO = @CAS_NO");
                        break;
                }
                if ((SearchConditionType)condition.CAS_NO_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.CAS_NO), condition.CAS_NO);
                }
                else
                {
                    parameters.Add(nameof(condition.CAS_NO), DbUtil.ConvertIntoEscapeChar(condition.CAS_NO));
                }
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CAS_NO)), condition.CAS_NO_CONDITION, condition.CAS_NO);
            }

            if (!string.IsNullOrWhiteSpace(condition.REG_NO))
            {
                condition.REG_NO = condition.REG_NO.Trim();
                sql.AppendLine(" and (");
                switch ((SearchConditionType)condition.REG_NO_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" T_LEDGER_WORK.REG_NO like '%' + @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" or ");
                        sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID in");
                        sql.AppendLine(" (");
                        sql.AppendLine("  SELECT distinct T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID FROM T_LED_WORK_REG_OLD_NO where");
                        sql.AppendLine("  T_LED_WORK_REG_OLD_NO.REG_OLD_NO like '%' + @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" )");
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" T_LEDGER_WORK.REG_NO like @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" or ");
                        sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID in");
                        sql.AppendLine(" (");
                        sql.AppendLine("  SELECT distinct T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID FROM T_LED_WORK_REG_OLD_NO where");
                        sql.AppendLine("  T_LED_WORK_REG_OLD_NO.REG_OLD_NO like @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" )");
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" T_LEDGER_WORK.REG_NO like '%' + @REG_NO " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" or ");
                        sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID in");
                        sql.AppendLine(" (");
                        sql.AppendLine("  SELECT distinct T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID FROM T_LED_WORK_REG_OLD_NO where");
                        sql.AppendLine("  T_LED_WORK_REG_OLD_NO.REG_OLD_NO like '%' + @REG_NO " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" )");
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" T_LEDGER_WORK.REG_NO = @REG_NO");
                        sql.AppendLine(" or ");
                        sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID in");
                        sql.AppendLine(" (");
                        sql.AppendLine("  SELECT distinct T_LED_WORK_REG_OLD_NO.LEDGER_FLOW_ID FROM T_LED_WORK_REG_OLD_NO where");
                        sql.AppendLine("  T_LED_WORK_REG_OLD_NO.REG_OLD_NO = @REG_NO");
                        sql.AppendLine(" )");
                        break;
                }
                sql.AppendLine(" )");
                if ((SearchConditionType)condition.REG_NO_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.REG_NO), condition.REG_NO);
                }
                else
                {
                    parameters.Add(nameof(condition.REG_NO), DbUtil.ConvertIntoEscapeChar(condition.REG_NO));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_NO)), condition.REG_NO_CONDITION, condition.REG_NO);
            }

            // 化学物質コード
            if (!string.IsNullOrEmpty(condition.CHEM_CD))
            {
                condition.CHEM_CD = condition.CHEM_CD.Trim();
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_WORK.CHEM_CD in");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_ALIAS.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_ALIAS");
                sql.AppendLine("  where");
                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_CD) =@CHEM_CD");
                sql.AppendLine(" )");

                parameters.Add(nameof(condition.CHEM_CD), condition.CHEM_CD.ToUpper());

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_CD)), condition.CHEM_CD);
            }

            //管理部門
            if (condition.GROUP_CD_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_WORK.GROUP_CD IN");
                sql.AppendLine(" (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.GROUP_CD_COLLECTION.ToList(), nameof(T_LEDGER_WORK.GROUP_CD), parameters));
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.GROUP_CD_COLLECTION)),
                    condition.GROUP_CD_COLLECTION, groupDic);
            }
            //法規制
            if (condition.REG_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and T_LEDGER_WORK.CHEM_CD in (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_REGULATION.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_REGULATION");
                sql.AppendLine("  where");
                sql.AppendLine("   M_CHEM_REGULATION.REG_TYPE_ID in (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.REG_COLLECTION.ToList(), nameof(condition.REG_COLLECTION), parameters));
                sql.AppendLine("   )");
                sql.AppendLine(" )");
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_COLLECTION)),
                    condition.REG_COLLECTION, regulationDic);
            }
            //保管場所
            if (condition.ACTION_LOC_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID in");
                sql.AppendLine(" ( select T_LED_WORK_LOC.LEDGER_FLOW_ID from T_LED_WORK_LOC where T_LED_WORK_LOC.LOC_ID in ");
                sql.AppendLine("  (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.ACTION_LOC_COLLECTION.ToList(), nameof(condition.ACTION_LOC_COLLECTION), parameters));
                sql.AppendLine("  )");
                sql.AppendLine(" )");
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.ACTION_LOC_COLLECTION)),
                    condition.ACTION_LOC_COLLECTION, locationDic);
            }
            //使用場所
            if (condition.USAGE_LOC_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID in");
                sql.AppendLine(" ( select T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID from T_LED_WORK_USAGE_LOC where T_LED_WORK_USAGE_LOC.LOC_ID in ");
                sql.AppendLine("  (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.USAGE_LOC_COLLECTION.ToList(), nameof(condition.USAGE_LOC_COLLECTION), parameters));
                sql.AppendLine("  )");
                sql.AppendLine(" )");
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.USAGE_LOC_COLLECTION)),
                    condition.USAGE_LOC_COLLECTION, locationDic);
            }
            if (string.IsNullOrWhiteSpace(condition.CHEM_OPERATION_CHIEF) == false)
            {
                condition.CHEM_OPERATION_CHIEF = condition.CHEM_OPERATION_CHIEF.Trim();
                sql.AppendLine(" and exists");
                sql.AppendLine(" (");
                //2018/08/14 TPE Delete Start
                //sql.AppendLine("  select 1 from T_LED_WORK_USAGE_LOC where T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID and T_LED_WORK_USAGE_LOC.CHEM_OPERATION_CHIEF = :CHEM_OPERATION_CHIEF");
                //2018/08/14 TPE Delete End

                //2018/08/14 TPE Add Start
                sql.AppendLine("  select 1 from T_LED_WORK_USAGE_LOC,M_CHIEF,M_LOCATION where T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID and T_LED_WORK_USAGE_LOC.LOC_ID = M_CHIEF.LOC_ID and M_CHIEF.LOC_ID = M_LOCATION.LOC_ID and M_LOCATION.CLASS_ID = 2 and M_CHIEF.USER_CD = @CHEM_OPERATION_CHIEF");
                //2018/08/14 TPE Add End

                sql.AppendLine(" )");

                //2018/08/14 TPE Add Start
                //sql.AppendLine(" and U.CHEM_OPERATION_CHIEF like '%' || :CHEM_OPERATION_CHIEF || '%' " + DbUtil.LikeEscapeInfoAdd());
                //sql.AppendLine(" and U.CLASS_ID = '2'");
                //2018/08/14 TPE Add End

                parameters.Add(nameof(condition.CHEM_OPERATION_CHIEF), condition.CHEM_OPERATION_CHIEF);
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_OPERATION_CHIEF)), condition.CHEM_OPERATION_CHIEF.ToString());

            }

            if (condition.REFERENCE_AUTHORITY == Const.cREFERENCE_AUTHORITY_FLAG_NORMAL)
            {
                sql.AppendLine(" and ");
                sql.AppendLine(" T_LEDGER_WORK.GROUP_CD in ");
                sql.AppendLine(" (");
                sql.AppendLine("    (select M_USER.GROUP_CD from M_USER where M_USER.USER_CD = @LOGIN_USER_CD)");
                sql.AppendLine("    ,(select M_GROUP.P_GROUP_CD from M_GROUP where M_GROUP.GROUP_CD = (select M_USER.GROUP_CD from M_USER where M_USER.USER_CD = @LOGIN_USER_CD))");
                //if (condition.USER_ADMINFLG == Const.cADMIN_FLG_ADMIN)
                //{
                //sql.AppendLine("    ,(select M_USER.PREVIOUS_GROUP_CD from M_USER where M_USER.USER_CD = :LOGIN_USER_CD)");
                //sql.AppendLine("    ,(select M_GROUP.P_GROUP_CD from M_GROUP where M_GROUP.GROUP_CD = (select M_USER.PREVIOUS_GROUP_CD from M_USER where M_USER.USER_CD = :LOGIN_USER_CD))");
                sql.AppendLine("    ,(select M_GROUP.GROUP_CD from M_GROUP where M_GROUP.GROUP_CD = (select M_USER.PREVIOUS_GROUP_CD from M_USER where M_USER.USER_CD = @LOGIN_USER_CD) and M_GROUP.DEL_FLAG = 1)");
                sql.AppendLine("    ,(select M_GROUP.P_GROUP_CD from M_GROUP where M_GROUP.GROUP_CD = (select M_USER.PREVIOUS_GROUP_CD from M_USER where M_USER.USER_CD = @LOGIN_USER_CD) and M_GROUP.DEL_FLAG = 1)");
                //}
                sql.AppendLine(" )");
                parameters.Add(nameof(condition.LOGIN_USER_CD), condition.LOGIN_USER_CD);
            }

            sql.AppendLine(" )");

            switch (condition.LEDGER_STATUS)
            {
                case (int)LedgerStatusConditionType.Using:
                    sql.AppendLine(" and");
                    sql.AppendLine(" T_LEDGER_WORK.DEL_FLAG = @DEL_FLAG");
                    parameters.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Undelete);
                    break;
                case (int)LedgerStatusConditionType.Applying:
                    sql.AppendLine(" and");
                    sql.AppendLine(" T_LEDGER_WORK.DEL_FLAG = @DEL_FLAG");
                    parameters.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Undelete);
                    break;
                case (int)LedgerStatusConditionType.Abolished:
                    sql.AppendLine(" and");
                    sql.AppendLine(" T_LEDGER_WORK.DEL_FLAG = @DEL_FLAG");
                    parameters.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Deleted);
                    var dic = new IncludesContainedListMasterInfo().GetDictionary();
                    break;
                case (int)LedgerStatusConditionType.All:
                    break;
            }
            MakeConditionInLineOfCsv("状態", ledgerStatusDic[condition.LEDGER_STATUS]);

            if (condition.LEDGER_FLOW_ID_EXT != null)
            {
                sql.AppendLine(" and T_LEDGER_WORK.LEDGER_FLOW_ID <> @LEDGER_FLOW_ID_EXT");
                parameters.Add(nameof(condition.LEDGER_FLOW_ID_EXT), condition.LEDGER_FLOW_ID_EXT);
            }

            return sql.ToString();
        }

        //public override IQueryable<T_LEDGER_WORK> GetSearchResult(T_LEDGER_WORK condition)
        //{
        //    throw new NotImplementedException();
        //}

        public IQueryable<LedgerCommonInfo> GetSearchProjectList(LedgerSearchModel condition, bool isFileRead = true)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID,");
            sql.AppendLine(" T_LEDGER_WORK.FLOW_CD,");
            sql.AppendLine(" T_LEDGER_WORK.HIERARCHY,");
            sql.AppendLine(" T_LEDGER_WORK.REG_NO,");
            sql.AppendLine(" T_LEDGER_WORK.APPLI_CLASS,");
            sql.AppendLine(" T_LEDGER_WORK.GROUP_CD,");
            sql.AppendLine(" T_LEDGER_WORK.REASON_ID,");
            sql.AppendLine(" T_LEDGER_WORK.OTHER_REASON,");
            sql.AppendLine(" T_LEDGER_WORK.CHEM_CD,");
            sql.AppendLine(" T_LEDGER_WORK.MAKER_NAME,");
            sql.AppendLine(" M_USER.APPROVER1,");
            sql.AppendLine(" M_USER.APPROVER2,");
            sql.AppendLine(" T_LEDGER_WORK.OTHER_REGULATION,");
            sql.AppendLine(" T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE,");
            sql.AppendLine(" T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME,");
            sql.AppendLine(" T_LEDGER_WORK.MEMO,");
            sql.AppendLine(" T_LEDGER_WORK.TEMP_SAVED_FLAG,");
            sql.AppendLine(" T_LEDGER_WORK.REG_DATE,");
            sql.AppendLine(" T_LEDGER_WORK.UPD_DATE,");
            sql.AppendLine(" T_LEDGER_WORK.DEL_DATE,");
            sql.AppendLine(" T_LEDGER_WORK.DEL_FLAG,");
            sql.AppendLine(" T_LED_WORK_HISTORY.APPROVAL_USER_CD,");
            sql.AppendLine(" T_LED_WORK_HISTORY.SUBSITUTE_USER_CD,");
            sql.AppendLine(" T_LED_WORK_HISTORY.APPROVAL_TARGET,");
            sql.AppendLine(" T_LEDGER_WORK.REG_USER_CD,");
            sql.AppendLine(" T_LED_WORK_HISTORY.RESULT HISTORY_RESULT");
            sql.AppendLine(FromWardCreate2(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<LedgerCommonInfo> records = DbUtil.Select<LedgerCommonInfo>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<LedgerCommonInfo>();

            var regulationInfo = new LedRegulationMasterInfo();
            foreach (DataRow record in dt.Rows)
            {
                var model = new LedgerCommonInfo();
                model.TableType = TableType.Work;
                model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)].ToString());
                model.HIERARCHY = OrgConvert.ToNullableInt32(record[nameof(model.HIERARCHY)].ToString());
                model.REG_NO = record[nameof(model.REG_NO)].ToString();
                model.APPLI_CLASS = OrgConvert.ToNullableInt32(record[nameof(model.APPLI_CLASS)].ToString());
                model.GROUP_CD = record[nameof(model.GROUP_CD)].ToString();
                model.GROUP_NAME = groupDic.GetValueToString(model.GROUP_CD);

                model.TEMP_SAVED_FLAG = OrgConvert.ToNullableInt32(record[nameof(model.TEMP_SAVED_FLAG)].ToString());
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)].ToString());
                model.UPD_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.UPD_DATE)].ToString());
                model.DEL_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.DEL_DATE)].ToString());
                model.DEL_FLAG = OrgConvert.ToNullableInt32(record[nameof(model.DEL_FLAG)].ToString());

                model.APPROVAL_TARGET = record[nameof(model.APPROVAL_TARGET)].ToString();
                model.APPROVAL_USER_CD = record[nameof(model.APPROVAL_USER_CD)].ToString();
                model.APPROVAL_USER_NAME = userDic.GetValueToString(model.APPROVAL_USER_CD);
                model.SUBSITUTE_USER_CD = record[nameof(model.SUBSITUTE_USER_CD)].ToString();
                if (isFileRead)
                {
                    using (var hist = new LedgerWorkHistoryDataInfo() { Context = Context })
                    {
                        var list = hist.GetSearchResult(new T_LED_WORK_HISTORY() { LEDGER_FLOW_ID = model.LEDGER_FLOW_ID });
                        if (list.Count() > 0)
                        {
                            model.FLOW_HIERARCHY = list.Max(x => x.HIERARCHY).ToString();
                            var beforeId = list.Where(x => x.RESULT != null).Max(x => x.LED_WORK_HISTORY_ID);
                            if (beforeId.HasValue)
                            {
                                var before = list.First(x => x.LED_WORK_HISTORY_ID == beforeId);
                                model.BEFORE_WORKFLOW_RESULT = before.RESULT;
                                model.BEFORE_APPROVAL_TARGET = before.APPROVAL_TARGET;
                                model.BEFORE_APPROVAL_USER_CD = before.APPROVAL_USER_CD;
                                model.BEFORE_APPROVAL_USER_NAME = userDic.GetValueToString(before.APPROVAL_USER_CD);
                            }
                            var first = list.LastOrDefault(x => x.DEL_FLAG == 0 && x.HIERARCHY == -1);
                            if (first != null)
                            {
                                model.ApplicationDate = first.REG_DATE;
                            }
                        }
                    }
                }
                model.REG_USER_CD = record[nameof(model.REG_USER_CD)].ToString();
                model.APPLICANT = userDic.GetValueToString(model.REG_USER_CD);
                model.APPROVER1 = record[nameof(model.APPROVER1)].ToString();
                model.APPROVER2 = record[nameof(model.APPROVER2)].ToString();
                model.NID = model.REG_USER_CD;


                if (string.IsNullOrWhiteSpace(model.REG_NO))
                {
                    model.Status = LedgerStatusConditionType.Applying;
                }
                else if (model.APPLI_CLASS == (int)AppliClass.Stopped)
                {
                    model.Status = LedgerStatusConditionType.Applying;
                }
                else if (model.APPLI_CLASS == (int)AppliClass.Edit)
                {
                    model.Status = LedgerStatusConditionType.Applying;
                }
                else
                {
                    model.Status = LedgerStatusConditionType.Using;
                }
                //model.STATUS_TEXT = ledgerStatusDic.GetValueToString(model._Status);

                dataArray.Add(model);
            }
            var searchQuery = dataArray.AsQueryable();

            regulationInfo.Dispose();

            searchQuery.OrderBy(x => x.LEDGER_FLOW_ID);
            return searchQuery;
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
        public IQueryable<LedgerCommonInfo> GetSearchResult(LedgerSearchModel condition, bool isFileRead = true)
        {
            List<LedgerCommonInfo> records = null;

            StringBuilder sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(" T_LEDGER_WORK.LEDGER_FLOW_ID,");
            sql.AppendLine(" T_LEDGER_WORK.FLOW_CD,");
            sql.AppendLine(" T_LEDGER_WORK.HIERARCHY,");
            sql.AppendLine(" T_LEDGER_WORK.REG_NO,");
            sql.AppendLine(" T_LEDGER_WORK.APPLI_CLASS,");
            sql.AppendLine(" T_LEDGER_WORK.GROUP_CD,");
            sql.AppendLine(" T_LEDGER_WORK.REASON_ID,");
            sql.AppendLine(" T_LEDGER_WORK.OTHER_REASON,");
            sql.AppendLine(" T_LEDGER_WORK.CHEM_CD,");
            sql.AppendLine(" T_LEDGER_WORK.MAKER_NAME,");
            sql.AppendLine(" M_CHEM.CHEM_NAME,");
            sql.AppendLine(" M_CHEM.CAS_NO,");
            sql.AppendLine(" M_CHEM.FIGURE,");
            sql.AppendLine(" M_CHEM.CHEM_CAT,");
            sql.AppendLine(" V_WORK_LED_REGULATION.REG_TYPE_ID ARRAY_REG_TYPE_IDS,");
            sql.AppendLine(" M_USER.APPROVER1,");
            sql.AppendLine(" M_USER.APPROVER2,");
            sql.AppendLine(" T_LEDGER_WORK.OTHER_REGULATION,");
            if (isFileRead)
            {
                sql.AppendLine(" T_LEDGER_WORK.SDS,");
            }
            sql.AppendLine(" T_LEDGER_WORK.SDS_MIMETYPE,");
            sql.AppendLine(" T_LEDGER_WORK.SDS_FILENAME,");
            sql.AppendLine(" T_LEDGER_WORK.SDS_URL,");
            if (isFileRead)
            {
                sql.AppendLine(" T_LEDGER_WORK.UNREGISTERED_CHEM,");
            }
            sql.AppendLine(" T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE,");
            sql.AppendLine(" T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME,");
            sql.AppendLine(" T_LEDGER_WORK.MEMO,");
            sql.AppendLine(" T_LEDGER_WORK.TEMP_SAVED_FLAG,");
            sql.AppendLine(" T_LEDGER_WORK.REG_DATE,");
            sql.AppendLine(" T_LEDGER_WORK.UPD_DATE,");
            sql.AppendLine(" T_LEDGER_WORK.DEL_DATE,");
            sql.AppendLine(" T_LEDGER_WORK.DEL_FLAG,");
            sql.AppendLine(" REG_OLD_NO_COUNT, ");
            sql.AppendLine(" REG_OLD_NO,");
            sql.AppendLine(" CHEM_PRODUCT_COUNT, ");
            sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME,");

            sql.AppendLine(" T_LED_WORK_HISTORY.APPROVAL_USER_CD,");
            sql.AppendLine(" T_LED_WORK_HISTORY.SUBSITUTE_USER_CD,");
            sql.AppendLine(" T_LED_WORK_HISTORY.APPROVAL_TARGET,");

            sql.AppendLine(" T_LEDGER_WORK.REG_USER_CD,");

            sql.AppendLine(" T_LEDGER_WORK_REGULATION.REG_TYPE_ID,");
            sql.AppendLine(" REGULATION_COUNT,");

            sql.AppendLine(" T_LED_WORK_LOC.LOC_SEQ,");
            sql.AppendLine(" T_LED_WORK_LOC.LOC_ID,");
            if (!condition.LOC_BASE)
            {
                sql.AppendLine(" LOC_COUNT,");
            }
            sql.AppendLine(" T_LED_WORK_LOC.AMOUNT,");
            sql.AppendLine(" T_LED_WORK_LOC.UNITSIZE_ID,");

            sql.AppendLine(" T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.LOC_ID USAGE_LOC_ID,");
            if (!condition.USAGE_LOC_BASE)
            {
                sql.AppendLine(" USAGE_LOC_COUNT,");
            }
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.AMOUNT USAGE_LOC_AMOUNT,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.UNITSIZE_ID USAGE_LOC_UNITSIZE_ID,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.ISHA_USAGE,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.ISHA_OTHER_USAGE,");

            //2018/08/14 TPE Delete Start
            //sql.AppendLine(" T_LED_WORK_USAGE_LOC.CHEM_OPERATION_CHIEF,");
            //2018/08/14 TPE Delete End

            //2018/08/14 TPE Add Start
            sql.AppendLine(" case when len(U.CHEM_OPERATION_CHIEF) > 0 then");
            sql.AppendLine("  left(U.CHEM_OPERATION_CHIEF,charindex(';',U.CHEM_OPERATION_CHIEF,len(U.CHEM_OPERATION_CHIEF)) -1)");
            sql.AppendLine(" else");
            sql.AppendLine("  null");
            sql.AppendLine(" end CHEM_OPERATION_CHIEF,");
            sql.AppendLine(" case when len(U.CHEM_OPERATION_CHIEF_NAME) > 0 then");
            sql.AppendLine("  left(U.CHEM_OPERATION_CHIEF_NAME,charindex(';',U.CHEM_OPERATION_CHIEF_NAME,len(U.CHEM_OPERATION_CHIEF_NAME)) -1)");
            sql.AppendLine(" else");
            sql.AppendLine("  null");
            sql.AppendLine(" end CHEM_OPERATION_CHIEF_NAME,");
            //2018/08/14 TPE Add End

            sql.AppendLine(" T_LED_WORK_USAGE_LOC.SUB_CONTAINER,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.ENTRY_DEVICE,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.OTHER_PROTECTOR,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_SCORE,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_RANK,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.MEASURES_AFTER_SCORE,");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC.MEASURES_AFTER_RANK,");
            sql.AppendLine(" T_LED_WORK_PROTECTOR.PROTECTOR_ID,");
            sql.AppendLine(" PROTECTOR_COUNT,");

            sql.AppendLine(" T_LED_WORK_ACCESMENT_REG.REG_TYPE_ID,");
            sql.AppendLine(" ACCESMENT_REG_COUNT,");

            sql.AppendLine(" T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_TYPE,");
            sql.AppendLine(" T_LED_WORK_RISK_REDUCE.LED_RISK_REDUCE_FLAG AS REAL,");
            sql.AppendLine(" RISK_REDUCE_COUNT,");

            sql.AppendLine(" T_LED_WORK_HISTORY.RESULT HISTORY_RESULT");
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            StartTransaction();
            records = DbUtil.Select<LedgerCommonInfo>(sql.ToString(), parameters);

            var dataArray = new List<LedgerCommonInfo>();

            var regulationInfo = new LedRegulationMasterInfo();
            for (int i = 0; i < records.Count; i++)
            {
                var model = new LedgerCommonInfo();
                records[i].TableType = TableType.Work;
                //model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record.LEDGER_FLOW_ID.ToString());
                //if (record.FLOW_CD == null)
                //{
                //model.FLOW_CD = string.Empty;
                //}
                //else
                //{
                //  model.FLOW_CD = record.FLOW_CD.ToString();
                //}

                //model.HIERARCHY = OrgConvert.ToNullableInt32(record.HIERARCHY.ToString());

                if (records[i].REG_NO == null)
                {
                    records[i].REG_NO = string.Empty;
                }
                else
                {
                    records[i].REG_NO = records[i].REG_NO.ToString();
                }
                //model.APPLI_CLASS = OrgConvert.ToNullableInt32(record.APPLI_CLASS.ToString());

                //if (record.GROUP_CD == null)
                //{
                //  model.GROUP_CD = string.Empty;
                //}
                //else
                //{
                //  model.GROUP_CD = record.GROUP_CD.ToString();
                //}
                records[i].GROUP_NAME = groupDic.GetValueToString(records[i].GROUP_CD);
                //model.REASON_ID = OrgConvert.ToNullableInt32(record.REASON_ID.ToString());
                records[i].REASON = reasonDic.GetValueToString(records[i].REASON_ID);
                //if (record.OTHER_REASON == null)
                //{
                //  model.OTHER_REASON = string.Empty;
                //}
                //else
                //{
                //  model.OTHER_REASON = record.OTHER_REASON.ToString();
                //}
                //if (record.CHEM_CD == null)
                //{
                //   model.CHEM_CD = string.Empty;
                //}
                //else
                //{
                //  model.CHEM_CD = record.CHEM_CD.ToString();
                //}
                //if (record.MAKER_NAME == null)
                //{
                //  model.MAKER_NAME = string.Empty;
                //}
                //else
                //{
                //  model.MAKER_NAME = record.MAKER_NAME.ToString();
                //}
                if (records[i].CHEM_CD == NikonConst.UnregisteredChemCd)
                {
                    records[i].CHEM_NAME = NikonConst.UnregisteredChemName;
                }
                else
                {
                    if (records[i].CHEM_NAME == null)
                    {
                        records[i].CHEM_NAME = string.Empty;
                    }
                    else
                    {
                        records[i].CHEM_NAME = records[i].CHEM_NAME;
                    }

                }
                //if (record.PRODUCT_NAME == null)
                //{
                //  model.PRODUCT_NAME = string.Empty;
                //}
                //else
                //{
                //model.PRODUCT_NAME = record.PRODUCT_NAME.ToString();
                //}
                //if (record.CAS_NO == null)
                //{
                //  model.CAS_NO = string.Empty;
                //}
                //else
                //{
                //  model.CAS_NO = record.CAS_NO.ToString();
                //}
                //model.FIGURE = OrgConvert.ToNullableInt32(record.FIGURE.ToString());
                records[i].FIGURE_NAME = figureDic.GetValueToString(records[i].FIGURE);
                //if (record.CHEM_CAT == null)
                //{
                //  model.CHEM_CAT = string.Empty;
                //}
                //else
                //{
                //  model.CHEM_CAT = record.CHEM_CAT.ToString();
                //}
                //model.REG_TYPE_ID = OrgConvert.ToNullableInt32(record.REG_TYPE_ID.ToString());
                if (records[i].ARRAY_REG_TYPE_IDS == null)
                {
                    records[i].ARRAY_REG_TYPE_IDS = string.Empty;
                }
                else
                {
                    records[i].ARRAY_REG_TYPE_IDS = records[i].ARRAY_REG_TYPE_IDS.ToString();
                }

                if (!condition.REGULATION_BASE)
                {
                    records[i].REGULATION_COUNT = OrgConvert.ToNullableInt32(records[i].REGULATION_COUNT);
                }
                if (records[i].ARRAY_REG_TYPE_IDS != "")
                {
                    var sqlReg = new StringBuilder();
                    sqlReg.AppendLine("select ");
                    sqlReg.AppendLine("V_REGULATION_ALL.REG_TEXT ");
                    sqlReg.AppendLine("from ");
                    sqlReg.AppendLine("V_REGULATION_ALL where V_REGULATION_ALL.REG_TYPE_ID in (");
                    sqlReg.AppendLine(records[i].ARRAY_REG_TYPE_IDS);
                    sqlReg.AppendLine(") ");
                    sqlReg.AppendLine("and V_REGULATION_ALL.DEL_FLAG = 0 ");
                    sqlReg.AppendLine("order by V_REGULATION_ALL.P_REG_TYPE_ID, V_REGULATION_ALL.SORT_ORDER ");

                    List<LedgerCommonInfo> recordsReg = DbUtil.Select<LedgerCommonInfo>(sqlReg.ToString(), parameters);
                    string strRegText = "";
                    foreach (LedgerCommonInfo row in recordsReg)
                    {
                        if (strRegText != "")
                        {
                            strRegText += ",";
                        }
                        strRegText += row.REG_TEXT.ToString();
                    }
                    records[i].REG_TEXT = strRegText;
                }
                //if (record.OTHER_REGULATION == null)
                //{
                //  model.OTHER_REGULATION = string.Empty;
                //}
                //else
                //{
                //  model.OTHER_REGULATION = record.OTHER_REGULATION.ToString();
                //}
                if (isFileRead == true && records[i].SDS != null)
                {
                    records[i].SDS = (byte[])records[i].SDS;
                    records[i].SDS_MIMETYPE = records[i].SDS_MIMETYPE;
                    records[i].SDS_FILENAME = records[i].SDS_FILENAME;
                }
                else
                {
                    records[i].SDS = new byte[0];
                    records[i].SDS_MIMETYPE = records[i].SDS_MIMETYPE;
                    records[i].SDS_FILENAME = records[i].SDS_FILENAME;
                }
                //if (record.SDS_MIMETYPE == null)
                //{
                //  model.SDS_MIMETYPE = string.Empty;
                //}
                //else
                //{
                //  model.SDS_MIMETYPE = record.SDS_MIMETYPE.ToString();
                //}
                //if (record.SDS_FILENAME == null)
                //{
                //  model.SDS_FILENAME = string.Empty;
                //}
                //else
                //{
                //  model.SDS_FILENAME = record.SDS_FILENAME.ToString();
                //}
                //if (record.SDS_URL == null)
                // {
                //  model.SDS_URL = string.Empty;
                //}
                //else
                //{
                //  model.SDS_URL = record.SDS_URL.ToString();
                //}
                if (isFileRead == true && records[i].UNREGISTERED_CHEM != null)
                {
                    records[i].UNREGISTERED_CHEM = (byte[])records[i].UNREGISTERED_CHEM;
                    records[i].UNREGISTERED_CHEM_MIMETYPE = records[i].UNREGISTERED_CHEM_MIMETYPE;
                    records[i].UNREGISTERED_CHEM_FILENAME = records[i].UNREGISTERED_CHEM_FILENAME;
                    records[i].HasUnregisteredChem = true;
                }
                else
                {
                    records[i].UNREGISTERED_CHEM = new byte[0];
                    records[i].UNREGISTERED_CHEM_MIMETYPE = records[i].UNREGISTERED_CHEM_MIMETYPE;
                    records[i].UNREGISTERED_CHEM_FILENAME = records[i].UNREGISTERED_CHEM_FILENAME;
                    records[i].HasUnregisteredChem = true;
                }
                //if (record.MEMO == null)
                //{
                //  model.MEMO = string.Empty;
                //}
                //else
                //{
                //  model.MEMO = record.MEMO.ToString();
                //}
                //model.TEMP_SAVED_FLAG = OrgConvert.ToNullableInt32(record.TEMP_SAVED_FLAG.ToString());
                //model.REG_DATE = OrgConvert.ToNullableDateTime(record.REG_DATE.ToString());
                //model.UPD_DATE = OrgConvert.ToNullableDateTime(record.UPD_DATE.ToString());
                //model.DEL_DATE = OrgConvert.ToNullableDateTime(record.DEL_DATE.ToString());
                //model.DEL_FLAG = OrgConvert.ToNullableInt32(record.DEL_FLAG.ToString());
                //model.REG_OLD_NO_COUNT = OrgConvert.ToNullableInt32(record.REG_OLD_NO_COUNT.ToString());
                //if (record.REG_OLD_NO == null)
                //{
                //  model.REG_OLD_NO = string.Empty;
                //}
                //else
                //{
                //  model.REG_OLD_NO = record.REG_OLD_NO.ToString();
                //}
                //model.CHEM_PRODUCT_COUNT = OrgConvert.ToNullableInt32(record.CHEM_PRODUCT_COUNT.ToString());
                //if (record.PRODUCT_NAME == null)
                //{
                //  model.PRODUCT_NAME = string.Empty;
                //}
                //else
                //{
                //  model.PRODUCT_NAME = record.PRODUCT_NAME.ToString();
                //}
                //if (record.APPROVAL_TARGET == null)
                //{
                //  model.APPROVAL_TARGET = string.Empty;
                //}
                //else
                //{
                //  model.APPROVAL_TARGET = record.APPROVAL_TARGET.ToString();
                //}
                //if (record.APPROVAL_USER_CD == null)
                //{
                //  model.APPROVAL_USER_CD = string.Empty;
                //}
                //else
                //{
                //  model.APPROVAL_USER_CD = record.APPROVAL_USER_CD.ToString();
                //}
                records[i].APPROVAL_USER_NAME = userDic.GetValueToString(records[i].APPROVAL_USER_CD);
                //if (record.SUBSITUTE_USER_CD == null)
                //{
                //  model.SUBSITUTE_USER_CD = string.Empty;
                //}
                //else
                //{
                //  model.SUBSITUTE_USER_CD = record.SUBSITUTE_USER_CD.ToString();
                //}
                if (isFileRead)
                {
                    using (var hist = new LedgerWorkHistoryDataInfo() { Context = Context })
                    {
                        var list = hist.GetSearchResult(new T_LED_WORK_HISTORY() { LEDGER_FLOW_ID = records[i].LEDGER_FLOW_ID });
                        if (list.Count() > 0)
                        {
                            records[i].FLOW_HIERARCHY = list.Max(x => x.HIERARCHY).ToString();
                            var beforeId = list.Where(x => x.RESULT != null).Max(x => x.LED_WORK_HISTORY_ID);
                            if (beforeId.HasValue)
                            {
                                var before = list.First(x => x.LED_WORK_HISTORY_ID == beforeId);
                                records[i].BEFORE_WORKFLOW_RESULT = before.RESULT;
                                records[i].BEFORE_APPROVAL_TARGET = before.APPROVAL_TARGET;
                                records[i].BEFORE_APPROVAL_USER_CD = before.APPROVAL_USER_CD;
                                records[i].BEFORE_APPROVAL_USER_NAME = userDic.GetValueToString(before.APPROVAL_USER_CD);
                            }
                            var first = list.LastOrDefault(x => x.DEL_FLAG == 0 && x.HIERARCHY == -1);
                            if (first != null)
                            {
                                records[i].ApplicationDate = first.REG_DATE;
                            }
                        }
                    }
                }
                //if (record.REG_USER_CD == null)
                //{
                //  model.REG_USER_CD = string.Empty;
                //}
                //else
                //{
                //  model.REG_USER_CD = record.REG_USER_CD.ToString();
                //}

                records[i].APPLICANT = userDic.GetValueToString(records[i].REG_USER_CD);
                //if (record.APPROVER1 == null)
                //{
                //  model.APPROVER1 = string.Empty;
                //}
                //else
                //{
                //  model.APPROVER1 = record.APPROVER1.ToString();
                //}
                //if (record.APPROVER2 == null)
                //{
                //  model.APPROVER2 = string.Empty;
                //}
                //else
                //{
                //  model.APPROVER2 = record.APPROVER2.ToString();
                //}

                records[i].NID = records[i].REG_USER_CD;


                //model.LOC_ID = OrgConvert.ToNullableInt32(record.LOC_ID.ToString());
                if (!condition.LOC_BASE)
                {
                    records[i].LOC_COUNT = OrgConvert.ToNullableInt32(records[i].LOC_COUNT);
                }
                records[i].LOC_NAME = locationDic.GetValueToString(records[i].LOC_ID);
                //if (record.AMOUNT == null)
                //{
                //  model.AMOUNT = string.Empty;
                //}
                //else
                //{
                //  model.AMOUNT = record.AMOUNT.ToString();
                //}
                //model.UNITSIZE_ID = OrgConvert.ToNullableInt32(record.UNITSIZE_ID);

                //model.USAGE_LOC_ID = OrgConvert.ToNullableInt32(record.USAGE_LOC_ID.ToString());
                if (!condition.USAGE_LOC_BASE)
                {
                    records[i].USAGE_LOC_COUNT = OrgConvert.ToNullableInt32(records[i].USAGE_LOC_COUNT);
                }
                records[i].USAGE_LOC_NAME = locationDic.GetValueToString(records[i].USAGE_LOC_ID);
                //if (record.USAGE_LOC_AMOUNT == null)
                //{
                //  model.USAGE_LOC_AMOUNT = string.Empty;
                //}
                //else
                //{
                //  model.USAGE_LOC_AMOUNT = record.USAGE_LOC_AMOUNT.ToString();
                //}
                //model.USAGE_LOC_UNITSIZE_ID = OrgConvert.ToNullableInt32(record.USAGE_LOC_UNITSIZE_ID);
                //model.REG_DATE = OrgConvert.ToNullableDateTime(record.REG_DATE);
                //model.ISHA_USAGE = OrgConvert.ToNullableInt32(record.ISHA_USAGE);
                records[i].ISHA_USAGE_TEXT = ishaUsageDic.GetValueToString(records[i].ISHA_USAGE);
                //if (record.ISHA_OTHER_USAGE == null)
                //{
                //  model.ISHA_OTHER_USAGE = string.Empty;
                //}
                //else
                //{
                //  model.ISHA_OTHER_USAGE = record.ISHA_OTHER_USAGE.ToString();
                //}

                //if (record.CHEM_OPERATION_CHIEF == null)
                //{
                //  model.CHEM_OPERATION_CHIEF = string.Empty;
                //}
                //else
                //{
                //  model.CHEM_OPERATION_CHIEF = record.CHEM_OPERATION_CHIEF.ToString();
                //}

                //2018/08/14 TPE Delete Start
                //model.CHEM_OPERATION_CHIEF_NAME = userDic.GetValueToString(model.CHEM_OPERATION_CHIEF);
                //2018/08/14 TPE Delete End

                //2018/08/14 TPE Add Start
                //if (record.CHEM_OPERATION_CHIEF_NAME == null)
                //{
                //  model.CHEM_OPERATION_CHIEF_NAME = string.Empty;
                //}
                //else
                //{
                //  model.CHEM_OPERATION_CHIEF_NAME = record.CHEM_OPERATION_CHIEF_NAME.ToString();
                //}
                //2018/08/14 TPE Add End

                //model.SUB_CONTAINER = OrgConvert.ToNullableInt32(record.SUB_CONTAINER);
                records[i].SUB_CONTAINER_TEXT = subcontainerDic.GetValueToString(records[i].SUB_CONTAINER);
                //if (record.ENTRY_DEVICE == null)
                //{
                //  model.ENTRY_DEVICE = string.Empty;
                //}
                //else
                //{
                //  model.ENTRY_DEVICE = record.ENTRY_DEVICE.ToString();
                //}
                //if (record.OTHER_PROTECTOR == null)
                //{
                //  model.OTHER_PROTECTOR = string.Empty;
                //}
                //else
                //{
                //  model.OTHER_PROTECTOR = record.OTHER_PROTECTOR.ToString();
                //}
                //model.MEASURES_BEFORE_SCORE = OrgConvert.ToNullableDecimal(record.MEASURES_BEFORE_SCORE);
                //model.MEASURES_BEFORE_RANK = OrgConvert.ToNullableInt32(record.MEASURES_BEFORE_RANK);
                records[i].MEASURES_BEFORE_RANK_TEXT = rankDic.GetValueToString(records[i].MEASURES_BEFORE_RANK);
                //model.MEASURES_AFTER_SCORE = OrgConvert.ToNullableDecimal(record.MEASURES_AFTER_SCORE);
                //model.MEASURES_AFTER_RANK = OrgConvert.ToNullableInt32(record.MEASURES_AFTER_RANK);
                records[i].MEASURES_AFTER_RANK_TEXT = rankDic.GetValueToString(records[i].MEASURES_AFTER_RANK);
                //model.PROTECTOR_ID = OrgConvert.ToNullableInt32(record.PROTECTOR_ID);
                records[i].PROTECTOR_NAME = protectorDic.GetValueToString(records[i].PROTECTOR_ID);
                //model.PROTECTOR_COUNT = OrgConvert.ToNullableInt32(record.PROTECTOR_COUNT);

                //model.LED_RISK_REDUCE_TYPE = OrgConvert.ToNullableInt32(record.LED_RISK_REDUCE_TYPE);
                if (records[i].LED_RISK_REDUCE_TYPE != null)
                {
                    if (Convert.ToInt32(records[i].REAL) == 1)
                    {
                        records[i].LED_RISK_REDUCE_FLAG = true;
                    }
                    else
                    {
                        records[i].LED_RISK_REDUCE_FLAG = false;
                    }
                }
                //model.RISK_REDUCE_COUNT = OrgConvert.ToNullableInt32(record.RISK_REDUCE_COUNT);

                if (string.IsNullOrWhiteSpace(records[i].REG_NO))
                {
                    records[i].Status = LedgerStatusConditionType.Applying;
                }
                else if (records[i].APPLI_CLASS == (int)AppliClass.Stopped)
                {
                    records[i].Status = LedgerStatusConditionType.Applying;
                }
                else if (records[i].APPLI_CLASS == (int)AppliClass.Edit)
                {
                    records[i].Status = LedgerStatusConditionType.Applying;
                }
                else
                {
                    records[i].Status = LedgerStatusConditionType.Using;
                }
                records[i].STATUS_TEXT = ledgerStatusDic.GetValueToString(records[i]._Status);

                var regulations = regulationInfo.GetSelectedItems(records[i].REG_TYPE_ID_COLLECTION).AsQueryable();
                records[i].REQUIRED_CHEM = regulations.Any(x => x.WORKTIME_MGMT == 1) ? "該当" : "非該当";
                var hazmatText = regulations.FirstOrDefault(x => x.REG_TEXT.IndexOf("消防法") != -1)?.REG_TEXT;
                records[i].HAZMAT_TYPE = hazmatText ?? "非該当";

                //dataArray.Add(model);
            }

            var searchQuery = records.AsQueryable();

            regulationInfo.Dispose();

            searchQuery.OrderBy(x => x.LEDGER_FLOW_ID);
            return searchQuery;
        }

        protected override void InitializeDictionary()
        {
            reasonDic = new CommonMasterInfo(NikonCommonMasterName.REASON).GetDictionary();
            userDic = new UserMasterInfo().GetDictionary();
            groupDic = new GroupMasterInfo().GetDictionary();
            figureDic = new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetDictionary();
            unitsizeDic = new UnitSizeMasterInfo().GetDictionary();
            unitsizeSelectList = new UnitSizeMasterInfo().GetSelectList();
            locationDic = new LocationMasterInfo().GetDictionary();
            ishaUsageDic = new IshaUsageMasterInfo().GetDictionary();
            subcontainerDic = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary();
            rankDic = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary();
            protectorDic = new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetDictionary();
            ledgerStatusDic = new LedgerStatusListMasterInfo().GetDictionary();
            regulationDic = new LedRegulationMasterInfo().GetDictionary();
        }

        public override void EditData(T_LEDGER_WORK Value) { }

        protected override void CreateInsertText(T_LEDGER_WORK value, out string sql, out DynamicParameters parameters)
        {

            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.FLOW_CD) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.HIERARCHY) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.APPLI_CLASS) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.GROUP_CD) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.REASON_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.OTHER_REASON) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.CHEM_CD) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.MAKER_NAME) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.SDS) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.SDS_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.MEMO) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_WORK.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.FLOW_CD) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.HIERARCHY) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.REG_NO) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.APPLI_CLASS) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.GROUP_CD) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.REASON_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.OTHER_REASON) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.CHEM_CD) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.MAKER_NAME) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.SDS) + ",");
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.SDS_MIMETYPE) + ",");
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  @" + nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.MEMO) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_WORK.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LEDGER_WORK.FLOW_CD), value.FLOW_CD);
            parameters.Add(nameof(T_LEDGER_WORK.HIERARCHY), OrgConvert.ToNullableInt32(value.HIERARCHY));
            parameters.Add(nameof(T_LEDGER_WORK.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_LEDGER_WORK.APPLI_CLASS), OrgConvert.ToNullableInt32(value.APPLI_CLASS));
            parameters.Add(nameof(T_LEDGER_WORK.GROUP_CD), value.GROUP_CD);
            parameters.Add(nameof(T_LEDGER_WORK.REASON_ID), OrgConvert.ToNullableInt32(value.REASON_ID));
            parameters.Add(nameof(T_LEDGER_WORK.OTHER_REASON), value.OTHER_REASON);
            parameters.Add(nameof(T_LEDGER_WORK.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(T_LEDGER_WORK.MAKER_NAME), value.MAKER_NAME);//TODO:修正待ち
            parameters.Add(nameof(T_LEDGER_WORK.OTHER_REGULATION), value.OTHER_REGULATION);
            if (value.SDS != null)
            {
                parameters.Add(nameof(T_LEDGER_WORK.SDS), value.SDS);
                parameters.Add(nameof(T_LEDGER_WORK.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_WORK.SDS_FILENAME), value.SDS_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_WORK.SDS_URL), value.SDS_URL);
            if (value.UNREGISTERED_CHEM != null)
            {
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM), value.UNREGISTERED_CHEM);
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE), value.UNREGISTERED_CHEM_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME), value.UNREGISTERED_CHEM_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_WORK.MEMO), value.MEMO);
            parameters.Add(nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG), value.TEMP_SAVED_FLAG);
            parameters.Add(nameof(T_LEDGER_WORK.REG_USER_CD), value.REG_USER_CD);

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LEDGER_WORK value, out string sql, out DynamicParameters parameters)
        {

            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.FLOW_CD) + " = @" + nameof(value.FLOW_CD) + ",");
            builder.AppendLine(" " + nameof(value.HIERARCHY) + " = @" + nameof(value.HIERARCHY) + ",");
            builder.AppendLine(" " + nameof(value.REG_NO) + " = @" + nameof(value.REG_NO) + ",");
            builder.AppendLine(" " + nameof(value.APPLI_CLASS) + " = @" + nameof(value.APPLI_CLASS) + ",");
            builder.AppendLine(" " + nameof(value.GROUP_CD) + " = @" + nameof(value.GROUP_CD) + ",");
            builder.AppendLine(" " + nameof(value.REASON_ID) + " = @" + nameof(value.REASON_ID) + ",");
            builder.AppendLine(" " + nameof(value.OTHER_REASON) + " = @" + nameof(value.OTHER_REASON) + ",");
            builder.AppendLine(" " + nameof(value.CHEM_CD) + " = @" + nameof(value.CHEM_CD) + ",");
            builder.AppendLine(" " + nameof(value.MAKER_NAME) + " = @" + nameof(value.MAKER_NAME) + ",");
            builder.AppendLine(" " + nameof(value.OTHER_REGULATION) + " = @" + nameof(value.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine(" " + nameof(value.SDS) + " = @" + nameof(value.SDS) + ",");
                builder.AppendLine(" " + nameof(value.SDS_MIMETYPE) + " = @" + nameof(value.SDS_MIMETYPE) + ",");
                builder.AppendLine(" " + nameof(value.SDS_FILENAME) + " = @" + nameof(value.SDS_FILENAME) + ",");
            }
            builder.AppendLine(" " + nameof(value.SDS_URL) + " = @" + nameof(value.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine(" " + nameof(value.UNREGISTERED_CHEM) + " = @" + nameof(value.UNREGISTERED_CHEM) + ",");
                builder.AppendLine(" " + nameof(value.UNREGISTERED_CHEM_MIMETYPE) + " = @" + nameof(value.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine(" " + nameof(value.UNREGISTERED_CHEM_FILENAME) + " = @" + nameof(value.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine(" " + nameof(value.MEMO) + " = @" + nameof(value.MEMO) + ",");
            builder.AppendLine(" " + nameof(value.TEMP_SAVED_FLAG) + " = @" + nameof(value.TEMP_SAVED_FLAG));
            builder.AppendLine("where ");
            builder.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(T_LEDGER_WORK.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LEDGER_WORK.FLOW_CD), value.FLOW_CD);
            parameters.Add(nameof(T_LEDGER_WORK.HIERARCHY), OrgConvert.ToNullableInt32(value.HIERARCHY));
            parameters.Add(nameof(T_LEDGER_WORK.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_LEDGER_WORK.APPLI_CLASS), OrgConvert.ToNullableInt32(value.APPLI_CLASS));
            parameters.Add(nameof(T_LEDGER_WORK.GROUP_CD), value.GROUP_CD);
            parameters.Add(nameof(T_LEDGER_WORK.REASON_ID), OrgConvert.ToNullableInt32(value.REASON_ID));
            parameters.Add(nameof(T_LEDGER_WORK.OTHER_REASON), value.OTHER_REASON);
            parameters.Add(nameof(T_LEDGER_WORK.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(T_LEDGER_WORK.MAKER_NAME), value.MAKER_NAME);
            parameters.Add(nameof(T_LEDGER_WORK.OTHER_REGULATION), value.OTHER_REGULATION);
            if (value.SDS != null)
            {
                parameters.Add(nameof(T_LEDGER_WORK.SDS), value.SDS);
                parameters.Add(nameof(T_LEDGER_WORK.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_WORK.SDS_FILENAME), value.SDS_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_WORK.SDS_URL), value.SDS_URL);
            if (value.UNREGISTERED_CHEM != null)
            {
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM), value.UNREGISTERED_CHEM);
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE), value.UNREGISTERED_CHEM_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME), value.UNREGISTERED_CHEM_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_WORK.MEMO), value.MEMO);
            parameters.Add(nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG), value.TEMP_SAVED_FLAG);

            sql = builder.ToString();
        }

        protected override void Delete(T_LEDGER_WORK value)
        {
            var sql = new StringBuilder();
            sql.AppendLine("update");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("set");
            sql.AppendLine(" " + nameof(value.DEL_FLAG) + " = " + ((int)DEL_FLAG.Deleted).ToString());
            sql.AppendLine("where");
            sql.AppendLine(getKeyWhere(value));

            var keyColumns = getKeyProperty(value);
            var parameters = keyColumns.ToHashtable();

            DbUtil.ExecuteUpdate(_Context, sql.ToString(), parameters);
        }

        protected override void UndoDeleted(T_LEDGER_WORK value)
        {
            var sql = new StringBuilder();

            sql.AppendLine("update");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("set");
            sql.AppendLine(" " + nameof(value.DEL_FLAG) + " = " + ((int)DEL_FLAG.Undelete).ToString() + ",");
            sql.AppendLine(" " + nameof(value.DEL_DATE) + " = null");
            sql.AppendLine("where");
            sql.AppendLine(getKeyWhere(value));

            var keyColumns = getKeyProperty(value);
            var parameters = keyColumns.ToHashtable();
            parameters.Add(nameof(value.UPD_USER_CD), value.UPD_USER_CD);

            DbUtil.ExecuteUpdate(_Context, sql.ToString(), parameters);
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    reasonDic = null;
                    userDic = null;
                    groupDic = null;
                    figureDic = null;
                    unitsizeDic = null;
                    unitsizeSelectList = null;
                    locationDic = null;
                    ishaUsageDic = null;
                    rankDic = null;
                    protectorDic = null;
                    riskreduceDic = null;
                    ledgerStatusDic = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        protected string FileTypeSuffix { get { return "_MIMETYPE"; } }

        //public dynamic GetBinaryData(string columnName, string id)
        //{
        //    var sql = "select {0}, {1} from {2} where {3} = {4}";
        //    var record = DbUtil.select(string.Format(sql, columnName, columnName + FileTypeSuffix, TableName, nameof(T_LEDGER_WORK.LEDGER_FLOW_ID), id));
        //    return record.Select().Select(x => new { DATA = x[columnName], TYPE = x[columnName + FileTypeSuffix] }).FirstOrDefault();
        //}

        //protected int GetIdentity(T_LEDGER_WORK value)
        //{
        //    string sql;
        //    string sql1 = "";
        //    Hashtable parameters = null; ;
        //    if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
        //    {
        //    }
        //    if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
        //    {
        //        CreateInsertText(value, out sql, out parameters);
        //        DbUtil.ExecuteUpdate(_Context, sql, parameters);
        //        sql1 = "select @@IDENTITY ID";
        //    }
        //    var dt = DbUtil.Select(_Context, sql1);
        //    return Convert.ToInt32(dt.Select()?.FirstOrDefault()["ID"]);
        //}
        protected int GetIdentity()
        {
            var sql = string.Empty;
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
            {
            }
            if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
            {
                // sql = "select @@IDENTITY ID";
                sql = "select IDENT_CURRENT('T_LEDGER_WORK') AS ID";
            }
            var dt = DbUtil.Select(_Context, sql);
            return Convert.ToInt32(dt.Select()?.FirstOrDefault()["ID"]);
        }

        protected List<LedgerCommonInfo> OnWorkflowLogic(string USER_CD)
        {
            var builder = new StringBuilder();
            var parameters = new DynamicParameters();
            builder.AppendLine("select distinct T_LED_WORK_HISTORY.LEDGER_FLOW_ID from T_LED_WORK_HISTORY");
            builder.AppendLine("    left join T_LEDGER_WORK on T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_HISTORY.LEDGER_FLOW_ID");
            builder.AppendLine("    and T_LEDGER_WORK.HIERARCHY + 1 <> T_LED_WORK_HISTORY.HIERARCHY");
            builder.AppendLine("where");
            builder.AppendLine("(");
            builder.AppendLine("    T_LED_WORK_HISTORY.APPROVAL_USER_CD = @USER_CD");
            builder.AppendLine("    or");
            builder.AppendLine("    T_LED_WORK_HISTORY.SUBSITUTE_USER_CD = @USER_CD");
            builder.AppendLine(")");
            builder.AppendLine("and T_LED_WORK_HISTORY.LEDGER_FLOW_ID not in ");
            builder.AppendLine("(");
            builder.AppendLine("    select T_LED_WORK_HISTORY.LEDGER_FLOW_ID");
            builder.AppendLine("    from T_LED_WORK_HISTORY");
            builder.AppendLine("    inner join T_LEDGER_WORK on");
            builder.AppendLine("    T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_HISTORY.LEDGER_FLOW_ID");
            builder.AppendLine("   and T_LEDGER_WORK.HIERARCHY + 1 = T_LED_WORK_HISTORY.HIERARCHY");
            builder.AppendLine("    and");
            builder.AppendLine("    (");
            builder.AppendLine("        T_LED_WORK_HISTORY.APPROVAL_USER_CD = @USER_CD");
            builder.AppendLine("        or");
            builder.AppendLine("        T_LED_WORK_HISTORY.SUBSITUTE_USER_CD = @USER_CD");
            builder.AppendLine("    )");
            builder.AppendLine("    and T_LED_WORK_HISTORY.DEL_FLAG = @DEL_FLAG");
            builder.AppendLine(")");
            builder.AppendLine("and T_LEDGER_WORK.TEMP_SAVED_FLAG <> @TEMP_SAVED_FLAG");
            builder.AppendLine("and T_LED_WORK_HISTORY.DEL_FLAG = @DEL_FLAG");
            parameters.Add(nameof(USER_CD), USER_CD);
            parameters.Add(nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG), 1);
            parameters.Add(nameof(T_LED_WORK_HISTORY.DEL_FLAG), 0);

            //StartTransaction();


            List<LedgerCommonInfo> records = DbUtil.Select<LedgerCommonInfo>(builder.ToString(), parameters);
            return records;
        }

        public int OnWorkflowCount(string USER_CD)
        {
            List<LedgerCommonInfo> result = OnWorkflowLogic(USER_CD);
            return result.Count;
        }

        public IQueryable<LedgerCommonInfo> GetOnWorkflowList(string USER_CD)
        {
            var result = OnWorkflowLogic(USER_CD);
            var cond = new LedgerSearchModel();
            foreach (LedgerCommonInfo row in result)
            {
                cond.LEDGER_FLOW_ID_SELECTION += (row.LEDGER_FLOW_ID).ToString() + ",";
            }
            return GetSearchProjectList(cond);
        }

        //public IQueryable<LedgerModel> GetOnWorkflowModel(string USER_CD)
        //{
        //    var result = OnWorkflowLogic(USER_CD);
        //    var cond = new LedgerSearchModel();
        //    foreach (DataRow row in result.Rows)
        //    {
        //        cond.LEDGER_FLOW_ID_SELECTION += (row["LEDGER_FLOW_ID"]).ToString() + ",";
        //    }
        //    return GetSearchResult(cond);
        //}

        public int WaitingCount(string USER_CD)
        {
            List<LedgerCommonInfo> records = WaitingLogic(USER_CD);
            return records.Count;
        }

        protected List<LedgerCommonInfo> WaitingLogic(string USER_CD)
        {
            var builder = new StringBuilder();
            var parameters = new DynamicParameters();
            builder.AppendLine("(");
            builder.AppendLine("	select distinct T_LED_WORK_HISTORY.LEDGER_FLOW_ID LEDGER_FLOW_ID from T_LED_WORK_HISTORY");
            builder.AppendLine("		left join T_LEDGER_WORK on T_LEDGER_WORK.LEDGER_FLOW_ID = T_LED_WORK_HISTORY.LEDGER_FLOW_ID");
            builder.AppendLine("	where");
            builder.AppendLine("	(");
            builder.AppendLine("		T_LED_WORK_HISTORY.APPROVAL_USER_CD = @USER_CD");
            builder.AppendLine("		or");
            builder.AppendLine("		T_LED_WORK_HISTORY.SUBSITUTE_USER_CD = @USER_CD");
            builder.AppendLine("	)");
            builder.AppendLine("	and T_LEDGER_WORK.HIERARCHY + 1 = T_LED_WORK_HISTORY.HIERARCHY");
            builder.AppendLine("	and T_LED_WORK_HISTORY.DEL_FLAG = @DEL_FLAG");
            builder.AppendLine("	union");
            builder.AppendLine("	select T_LEDGER_WORK.LEDGER_FLOW_ID LEDGER_FLOW_ID from T_LEDGER_WORK");
            builder.AppendLine("	where ");
            builder.AppendLine("		T_LEDGER_WORK.TEMP_SAVED_FLAG = @TEMP_SAVED_FLAG");
            builder.AppendLine("		and T_LEDGER_WORK.REG_USER_CD = @USER_CD");
            builder.AppendLine(")");
            builder.AppendLine("order by LEDGER_FLOW_ID");
            parameters.Add(nameof(USER_CD), USER_CD);
            parameters.Add(nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG), 1);
            parameters.Add(nameof(T_LED_WORK_HISTORY.DEL_FLAG), 0);

            //StartTransaction();
            /*List<LedgerSearchModel> records = */
            return DbUtil.Select<LedgerCommonInfo>(builder.ToString(), parameters);
        }

        public IQueryable<LedgerCommonInfo> GetWaitingList(string USER_CD)
        {
            List<LedgerCommonInfo> result = WaitingLogic(USER_CD);
            var cond = new LedgerSearchModel();
            foreach (LedgerCommonInfo row in result)
            {
                cond.LEDGER_FLOW_ID_SELECTION += (row.LEDGER_FLOW_ID).ToString() + ",";
            }
            return GetSearchProjectList(cond);
        }

        //public IQueryable<LedgerModel> GetWaitingModel(string USER_CD)
        //{
        //    DataTable result = WaitingLogic(USER_CD);
        //    var cond = new LedgerSearchModel();
        //    foreach (DataRow row in result.Rows)
        //    {
        //        cond.LEDGER_FLOW_ID_SELECTION += (row["LEDGER_FLOW_ID"]).ToString() + ",";
        //    }
        //    return GetSearchResult(cond);
        //}

        /// <summary>
        /// DB上のモデルと情報同期しているか
        /// </summary>
        public bool IsInfoSyncFromDB(LedgerCommonInfo model, bool isSearchRegNo = false)
        {

            if (model == null && model.LEDGER_FLOW_ID == null)
            {
                return true;
            }
            LedgerModel db;
            if (isSearchRegNo)
            {
                db = GetSearchResult(new LedgerSearchModel()
                {
                    REG_NO = model.REG_NO,
                }).FirstOrDefault();
            }
            else
            {
                db = GetSearchResult(new LedgerSearchModel()
                {
                    LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                }).FirstOrDefault();
            }
            if (db == null)
            {
                return true;
            }
            if (model.REG_DATE == db.REG_DATE && model.UPD_DATE == db.UPD_DATE && model.DEL_DATE == db.DEL_DATE)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public void AddBulk(LedgerModel ledger)
        {
            base.Add((T_LEDGER_WORK)ledger);
            ledger.LEDGER_FLOW_ID = GetIdentity();
        }

        public void AddWithChildren(LedgerCommonInfo ledger,
            IEnumerable<LedgerOldRegisterNumberModel> oldRegisterNumbers,
            List<LedgerRegulationModel> regulations,
            List<LedgerLocationModel> storingLocations,
            IEnumerable<LedgerUsageLocationModel> usageLocations)
        {
            SaveWithChildren(ledger, oldRegisterNumbers, regulations, storingLocations, usageLocations);
        }

        public void SaveWithChildren(LedgerCommonInfo ledger,
            IEnumerable<LedgerOldRegisterNumberModel> oldRegisterNumbers,
            List<LedgerRegulationModel> regulations,
            List<LedgerLocationModel> storingLocations,
            IEnumerable<LedgerUsageLocationModel> usageLocations)
        {
            LedgerSearchModel objLedgerColumn = new LedgerSearchModel();
            List<int> lstcount = null;
            if (ledger.LEDGER_FLOW_ID != null)
            {
                objLedgerColumn.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;


                lstcount = lstGetSearchResultCount(new LedgerSearchModel() { LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID });
            }

            if (lstcount != null && lstcount[0] > 0)
            {
                save((T_LEDGER_WORK)ledger);
                if (!IsValid)
                {
                    return;
                }
                SaveChildrenModels(ledger, oldRegisterNumbers, regulations, storingLocations, usageLocations);
            }
            else
            {
                Add((T_LEDGER_WORK)ledger);
                if (!IsValid)
                {
                    return;
                }
                ledger.LEDGER_FLOW_ID = GetIdentity();
                SaveChildrenModels(ledger, oldRegisterNumbers, regulations, storingLocations, usageLocations);
            }
        }



        /// <summary>
        /// 子モデルの保存ロジック
        /// </summary>
        /// <param name="ledger"></param>
        /// <param name="oldRegisterNumbers"></param>
        /// <param name="storingLocations"></param>
        /// <param name="usageLocations"></param>
        protected void SaveChildrenModels(LedgerModel ledger,
            IEnumerable<LedgerOldRegisterNumberModel> oldRegisterNumbers,
            List<LedgerRegulationModel> regulations,
            List<LedgerLocationModel> storingLocations,
            IEnumerable<LedgerUsageLocationModel> usageLocations)
        {
            using (var dataInfo = new LedgerWorkOldRegisterNumberDataInfo()
            {
                Context = Context,
                IsValidateUseAdd = IsValidateUseAdd,
                IsValidateUseSave = IsValidateUseSave
            })
            {
                dataInfo.Save(ledger, oldRegisterNumbers);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }

            using (var dataInfo = new LedgerWorkRegulationDataInfo()
            {
                Context = Context,
                IsValidateUseAdd = IsValidateUseAdd,
                IsValidateUseSave = IsValidateUseSave
            })
            {
                dataInfo.Save(ledger, regulations);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }

            using (var dataInfo = new LedgerWorkLocationDataInfo()
            {
                Context = Context,
                IsValidateUseAdd = IsValidateUseAdd,
                IsValidateUseSave = IsValidateUseSave
            })
            {
                dataInfo.Save(ledger, storingLocations);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }
            using (var dataInfo = new LedgerWorkUsageLocationDataInfo()
            {
                Context = Context,
                IsValidateUseAdd = IsValidateUseAdd,
                IsValidateUseSave = IsValidateUseSave
            })
            {
                dataInfo.Save(ledger, usageLocations);

                if (ledger.APPLI_CLASS == (int)AppliClass.Stopped)
                {
                    foreach (var usageLocation in usageLocations)
                    {
                        if (usageLocation.REG_TRANSACTION_VOLUME == -1)
                        {
                            UpdDummy(usageLocation.LEDGER_FLOW_ID, usageLocation.USAGE_LOC_SEQ, "REG_TRANSACTION_VOLUME");
                        }
                        if (usageLocation.REG_WORK_FREQUENCY == -1)
                        {
                            UpdDummy(usageLocation.LEDGER_FLOW_ID, usageLocation.USAGE_LOC_SEQ, "REG_WORK_FREQUENCY");
                        }
                        if (usageLocation.REG_DISASTER_POSSIBILITY == -1)
                        {
                            UpdDummy(usageLocation.LEDGER_FLOW_ID, usageLocation.USAGE_LOC_SEQ, "REG_DISASTER_POSSIBILITY");
                        }
                    }
                }

                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }
        }

        /// <summary>
        /// 案件削除と最終承認後のworkモデルの削除
        /// </summary>
        /// <param name="value"></param>
        /// <param name="isPhysicalDelete"></param>
        public void RemoveWithChildren(T_LEDGER_WORK value, bool isPhysicalDelete)
        {
            base.remove(value, isPhysicalDelete);
            if (!IsValid)
            {
                return;
            }
            DeleteChildrenModels(value.LEDGER_FLOW_ID);
        }

        /// <summary>
        /// 子モデルを削除する
        /// </summary>
        /// <param name="ledger"></param>
        protected void DeleteChildrenModels(int? LEDGER_FLOW_ID)
        {
            using (var dataInfo = new LedgerWorkOldRegisterNumberDataInfo() { Context = Context })
            {
                var condition = new LedgerOldRegisterNumberSearchModel();
                condition.LEDGER_FLOW_ID = LEDGER_FLOW_ID;
                var deleteDataArray = dataInfo.GetSearchResult(condition);
                dataInfo.remove(deleteDataArray.Select(x => (T_LED_WORK_REG_OLD_NO)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }

            //using (var dataInfo = new LedgerWorkRegulationDataInfo() { Context = Context })
            //{
            //    var condition = new LedgerRegulationSearchModel();
            //    condition.LEDGER_FLOW_ID = LEDGER_FLOW_ID;
            //    var deleteDataArray = dataInfo.GetSearchResult(condition);
            //    dataInfo.Remove(deleteDataArray.Select(x => (T_LEDGER_WORK_REGULATION)x), true);
            //    if (!dataInfo.IsValid)
            //    {
            //        IsValid = dataInfo.IsValid;
            //        ErrorMessages.AddRange(dataInfo.ErrorMessages);
            //        return;
            //    }
            //}

            using (var dataInfo = new LedgerWorkLocationDataInfo() { Context = Context })
            {
                var condition = new LedgerLocationSearchModel();
                condition.LEDGER_FLOW_ID = LEDGER_FLOW_ID;
                var deleteDataArray = dataInfo.GetSearchResult(condition);
                dataInfo.remove(deleteDataArray.Select(x => (T_LED_WORK_LOC)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }
            using (var dataInfo = new LedgerWorkUsageLocationDataInfo() { Context = Context })
            {
                var condition = new LedgerUsageLocationSearchModel();
                condition.LEDGER_FLOW_ID = LEDGER_FLOW_ID;
                var deleteDataArray = dataInfo.GetSearchResult(condition);
                dataInfo.remove(deleteDataArray.Select(x => (T_LED_WORK_USAGE_LOC)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }
            using (var dataInfo = new LedgerWorkProtectorDataInfo() { Context = Context })
            {
                var condition = new LedgerProtectorSearchModel();
                condition.LEDGER_FLOW_ID = LEDGER_FLOW_ID;
                var deleteDataArray = dataInfo.GetSearchResult(condition);
                dataInfo.remove(deleteDataArray.Select(x => (T_LED_WORK_PROTECTOR)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }
            using (var dataInfo = new LedgerWorkAccesmentRegDataInfo() { Context = Context })
            {
                var condition = new LedgerAccesmentRegSearchModel();
                condition.LEDGER_FLOW_ID = LEDGER_FLOW_ID;
                var deleteDataArray = dataInfo.GetSearchResult(condition);
                dataInfo.remove(deleteDataArray.Select(x => (T_LED_WORK_ACCESMENT_REG)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }
            using (var dataInfo = new LedgerWorkRiskReduceDataInfo() { Context = Context })
            {
                var condition = new LedgerRiskReduceSearchModel();
                condition.LEDGER_FLOW_ID = LEDGER_FLOW_ID;
                var deleteDataArray = dataInfo.GetSearchResult(condition);
                dataInfo.remove(deleteDataArray.Select(x => (T_LED_WORK_RISK_REDUCE)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }
            using (var dataInfo = new LedgerWorkRegulationDataInfo() { Context = Context })
            {
                var condition = new LedgerRegulationSearchModel();
                condition.LEDGER_FLOW_ID = LEDGER_FLOW_ID;
                var deleteDataArray = dataInfo.GetSearchResult(condition);
                dataInfo.remove(deleteDataArray.Select(x => (T_LEDGER_WORK_REGULATION)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
            }
        }

        // 職場の取扱量/作業頻度/災害発生の可能性 はクラスでRequired設定をしているため
        // ダミーで-1を登録し、改めてnullを登録
        public void UpdDummy(int? id, int? usagelocseq, string field)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = new DynamicParameters();

            sql.AppendLine("update");
            sql.AppendLine(" T_LED_WORK_USAGE_LOC ");
            sql.AppendLine("set");
            sql.AppendLine(field);
            sql.AppendLine(" = null ");
            sql.AppendLine("where ");
            sql.AppendLine("LEDGER_FLOW_ID = @LEDGER_FLOW_ID ");
            sql.AppendLine("and ");
            sql.AppendLine("USAGE_LOC_SEQ = @USAGE_LOC_SEQ ");

            parameters.Add("LEDGER_FLOW_ID", id);
            parameters.Add("USAGE_LOC_SEQ", usagelocseq);

            DbUtil.ExecuteUpdate(sql.ToString(), parameters);
        }

        public override List<T_LEDGER_WORK> GetSearchResult(T_LEDGER_WORK condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(T_LEDGER_WORK value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.FLOW_CD) + " = :" + nameof(value.FLOW_CD) + ",");
            builder.AppendLine(" " + nameof(value.HIERARCHY) + " = :" + nameof(value.HIERARCHY) + ",");
            builder.AppendLine(" " + nameof(value.REG_NO) + " = :" + nameof(value.REG_NO) + ",");
            builder.AppendLine(" " + nameof(value.APPLI_CLASS) + " = :" + nameof(value.APPLI_CLASS) + ",");
            builder.AppendLine(" " + nameof(value.GROUP_CD) + " = :" + nameof(value.GROUP_CD) + ",");
            builder.AppendLine(" " + nameof(value.REASON_ID) + " = :" + nameof(value.REASON_ID) + ",");
            builder.AppendLine(" " + nameof(value.OTHER_REASON) + " = :" + nameof(value.OTHER_REASON) + ",");
            builder.AppendLine(" " + nameof(value.CHEM_CD) + " = :" + nameof(value.CHEM_CD) + ",");
            builder.AppendLine(" " + nameof(value.MAKER_NAME) + " = :" + nameof(value.MAKER_NAME) + ",");
            builder.AppendLine(" " + nameof(value.OTHER_REGULATION) + " = :" + nameof(value.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine(" " + nameof(value.SDS) + " = :" + nameof(value.SDS) + ",");
                builder.AppendLine(" " + nameof(value.SDS_MIMETYPE) + " = :" + nameof(value.SDS_MIMETYPE) + ",");
                builder.AppendLine(" " + nameof(value.SDS_FILENAME) + " = :" + nameof(value.SDS_FILENAME) + ",");
            }
            builder.AppendLine(" " + nameof(value.SDS_URL) + " = :" + nameof(value.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine(" " + nameof(value.UNREGISTERED_CHEM) + " = :" + nameof(value.UNREGISTERED_CHEM) + ",");
                builder.AppendLine(" " + nameof(value.UNREGISTERED_CHEM_MIMETYPE) + " = :" + nameof(value.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine(" " + nameof(value.UNREGISTERED_CHEM_FILENAME) + " = :" + nameof(value.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine(" " + nameof(value.MEMO) + " = :" + nameof(value.MEMO) + ",");
            builder.AppendLine(" " + nameof(value.TEMP_SAVED_FLAG) + " = :" + nameof(value.TEMP_SAVED_FLAG));
            builder.AppendLine("where ");
            builder.AppendLine(getkeywhere(value));
            parameters.Add(nameof(T_LEDGER_WORK.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LEDGER_WORK.FLOW_CD), value.FLOW_CD);
            parameters.Add(nameof(T_LEDGER_WORK.HIERARCHY), OrgConvert.ToNullableInt32(value.HIERARCHY));
            parameters.Add(nameof(T_LEDGER_WORK.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_LEDGER_WORK.APPLI_CLASS), OrgConvert.ToNullableInt32(value.APPLI_CLASS));
            parameters.Add(nameof(T_LEDGER_WORK.GROUP_CD), value.GROUP_CD);
            parameters.Add(nameof(T_LEDGER_WORK.REASON_ID), OrgConvert.ToNullableInt32(value.REASON_ID));
            parameters.Add(nameof(T_LEDGER_WORK.OTHER_REASON), value.OTHER_REASON);
            parameters.Add(nameof(T_LEDGER_WORK.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(T_LEDGER_WORK.MAKER_NAME), value.MAKER_NAME);
            parameters.Add(nameof(T_LEDGER_WORK.OTHER_REGULATION), value.OTHER_REGULATION);
            if (value.SDS != null)
            {
                parameters.Add(nameof(T_LEDGER_WORK.SDS), value.SDS);
                parameters.Add(nameof(T_LEDGER_WORK.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_WORK.SDS_FILENAME), value.SDS_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_WORK.SDS_URL), value.SDS_URL);
            if (value.UNREGISTERED_CHEM != null)
            {
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM), value.UNREGISTERED_CHEM);
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_MIMETYPE), value.UNREGISTERED_CHEM_MIMETYPE);
                parameters.Add(nameof(T_LEDGER_WORK.UNREGISTERED_CHEM_FILENAME), value.UNREGISTERED_CHEM_FILENAME);
            }
            parameters.Add(nameof(T_LEDGER_WORK.MEMO), value.MEMO);
            parameters.Add(nameof(T_LEDGER_WORK.TEMP_SAVED_FLAG), value.TEMP_SAVED_FLAG);

            sql = builder.ToString();
        }
    }
}