﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon.Models;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class ApplicableLawDataInfo : DataInfoBase<V_APPLICABLE_LAW>
    {
        public ApplicableLawDataInfo()
        {
        }

        protected override void InitializeDictionary()
        {
        }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            sql.AppendLine("from");
            sql.AppendLine(TableName);
            return sql.ToString();
        }
        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();
            sql.AppendLine("where");
            sql.AppendLine(TableName + ".REG_TYPE_ID = V_REGULATION_ALL.REG_TYPE_ID");
            sql.AppendLine("and");
            sql.AppendLine(TableName + ".DEL_FLAG = @DEL_FLAG");
            sql.AppendLine("and");
            sql.AppendLine("(" + TableName + ".ISHA_USAGE = @ISHA_USAGE ");
            sql.AppendLine("or " + TableName + ".ISHA_USAGE is NULL) ");

            parameters.Add(nameof(condition.DEL_FLAG), (int)ZyCoaG.DEL_FLAG.Undelete);
            parameters.Add(nameof(condition.ISHA_USAGE), condition.ISHA_USAGE);

            return sql.ToString();
        }
        public override void EditData(V_APPLICABLE_LAW Value) { }

        protected override string TableName { get; } = nameof(V_APPLICABLE_LAW);

        public override List<V_APPLICABLE_LAW> GetSearchResult(V_APPLICABLE_LAW condition)
        {
            throw new NotImplementedException();
        }

        public IQueryable<ApplicableLawModel> GetSearchResult(LedgerSearchModel condition, bool isFileRead = true)
        {
            //DataTable records = new DataTable();
            //DataTable recordsCas = new DataTable();
            List<ApplicableLawModel> records = new List<ApplicableLawModel>();
            if (condition.REG_TYPE_IDS != null)
            {
                var sql = new StringBuilder();
                DynamicParameters parameters = null;

                sql.AppendLine("select ");
                sql.AppendLine(" distinct M_CHEM_REGULATION." + nameof(V_APPLICABLE_LAW.REG_TYPE_ID) + ",");
                sql.AppendLine(" M_REGULATION." + nameof(V_APPLICABLE_LAW.MARKS) + ",");
                sql.AppendLine(" M_REGULATION." + nameof(V_APPLICABLE_LAW.RA_FLAG) + ",");
                sql.AppendLine(" ISNULL(M_REGULATION.FIRE_SIZE, 0) FIRE_SIZE, ");
                sql.AppendLine(" V_REGULATION_ALL.REG_TEXT_BASE, ");
                sql.AppendLine(" V_REGULATION_ALL.REG_TEXT_LAST, ");
                sql.AppendLine(" V_REGULATION_ALL.REG_TEXT, ");
                sql.AppendLine(" V_REGULATION_ALL.REG_ICON_PATH, ");
                sql.AppendLine(" ISNULL(V_REGULATION_ALL.WORKTIME_MGMT, 0) WORKTIME_MGMT ");
                sql.AppendLine(" from M_CHEM_REGULATION ");
                sql.AppendLine(", V_REGULATION_ALL ");
                sql.AppendLine(", M_REGULATION ");
                sql.AppendLine(" where ");
                sql.AppendLine(" M_CHEM_REGULATION.REG_TYPE_ID = V_REGULATION_ALL.REG_TYPE_ID ");
                sql.AppendLine(" and ");
                sql.AppendLine(" M_CHEM_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.AppendLine(" and ");
                sql.AppendLine(" M_CHEM_REGULATION.REG_TYPE_ID not in ( ");
                sql.AppendLine(" select V_CAS_REGULATION.REG_TYPE_ID from V_CAS_REGULATION) ");
                sql.AppendLine(" and M_CHEM_REGULATION.REG_TYPE_ID in(" + condition.REG_TYPE_IDS + ")");
                sql.AppendLine(" union ");
                sql.AppendLine(" select ");
                sql.AppendLine(" distinct ");
                sql.AppendLine(TableName + "." + nameof(V_APPLICABLE_LAW.REG_TYPE_ID) + ",");
                sql.AppendLine(TableName + "." + nameof(V_APPLICABLE_LAW.MARKS) + ",");
                sql.AppendLine(TableName + "." + nameof(V_APPLICABLE_LAW.RA_FLAG) + ",");
                sql.AppendLine("ISNULL(" + TableName + "." + nameof(V_APPLICABLE_LAW.FIRE_SIZE) + ", 0) FIRE_SIZE,");
                sql.AppendLine("V_REGULATION_ALL." + nameof(V_APPLICABLE_LAW.REG_TEXT_BASE) + ",");
                sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_TEXT_LAST) + ",");
                sql.AppendLine("V_REGULATION_ALL." + nameof(V_APPLICABLE_LAW.REG_TEXT) + ",");
                sql.AppendLine("V_REGULATION_ALL." + nameof(V_REGULATION.REG_ICON_PATH) + ",");
                sql.AppendLine("ISNULL(V_REGULATION_ALL." + nameof(V_REGULATION.WORKTIME_MGMT) + ", 0) WORKTIME_MGMT");

                sql.AppendLine(FromWardCreate(condition));
                sql.AppendLine(", V_REGULATION_ALL ");
                sql.AppendLine(WhereWardCreate(condition, out parameters));
                sql.AppendLine("and");
                sql.AppendLine(TableName + "." + nameof(V_APPLICABLE_LAW.REG_TYPE_ID) + " in(" + condition.REG_TYPE_IDS + ") ");
                sql.AppendLine("order by V_REGULATION_ALL.REG_TEXT");

                //StartTransaction();
                records = DbUtil.Select<ApplicableLawModel>(sql.ToString(), parameters);
            }

            var dataArray = new List<ApplicableLawModel>();

            if (records.Count == 0)
            {
                var model = new ApplicableLawModel();
                model.REG_TEXT = "該当なし/-";
                model.REG_TEXT_BASE = "該当なし";
                model.REG_TEXT_LAST = "-";
                model.MARKS = 1;
                dataArray.Add(model);
            }
            else
            {
                foreach (var record in records)
                {
                    var model = new ApplicableLawModel();
                    model.REG_TEXT = record.REG_TEXT.ToString();
                    model.REG_TEXT_BASE = record.REG_TEXT_BASE.ToString();
                    model.REG_TEXT_LAST = record.REG_TEXT_LAST.ToString();
                    model.MARKS = OrgConvert.ToNullableInt32(record.MARKS);
                    if (model.MARKS == null)
                    {
                        model.MARKS = 0;
                    }
                    model.RA_FLAG = OrgConvert.ToNullableInt32(record.RA_FLAG);
                    model.REG_TYPE_ID = OrgConvert.ToNullableInt32(record.REG_TYPE_ID);
                    model.REG_ICON_PATH = record.REG_ICON_PATH.ToString();
                    model.WORKTIME_MGMT = OrgConvert.ToNullableInt32(record.WORKTIME_MGMT);
                    model.FIRE_SIZE = Convert.ToDecimal(record.FIRE_SIZE);
                    dataArray.Add(model);
                }
            }
            var searchQuery = dataArray.AsQueryable();

            return searchQuery;
        }

        protected override void CreateUpdateText(V_APPLICABLE_LAW value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(V_APPLICABLE_LAW value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(V_APPLICABLE_LAW value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(V_APPLICABLE_LAW value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}