﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerWorkUsageLocationDataInfo : DataInfoBase<T_LED_WORK_USAGE_LOC>, IDisposable
    {
        private IDictionary<int?, string> locationDic { get; set; }

        private IDictionary<int?, string> ishaUsageDic { get; set; }

        private IDictionary<string, string> userDic { get; set; }

        private IDictionary<int?, string> subcontainerDic { get; set; }

        private IDictionary<int?, string> rankDic { get; set; }

        private IDictionary<int?, string> regTransactionVolumeDic { get; set; }

        private IDictionary<int?, string> regWorkFrequencyDic { get; set; }

        private IDictionary<int?, string> regDisasterPossibilityDic { get; set; }

        protected override string TableName { get; } = nameof(T_LED_WORK_USAGE_LOC);

        //protected override void CreateInsertText(T_LED_WORK_USAGE_LOC value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.LOC_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.AMOUNT) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.UNITSIZE_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.ISHA_USAGE) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.ISHA_OTHER_USAGE) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.CHEM_OPERATION_CHIEF) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.SUB_CONTAINER) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.ENTRY_DEVICE) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.OTHER_PROTECTOR) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_SCORE) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_RANK) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_SCORE) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_RANK) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.REG_TRANSACTION_VOLUME) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.REG_WORK_FREQUENCY) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.REG_DISASTER_POSSIBILITY));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.LOC_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.AMOUNT) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.UNITSIZE_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.ISHA_USAGE) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.ISHA_OTHER_USAGE) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.CHEM_OPERATION_CHIEF) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.SUB_CONTAINER) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.ENTRY_DEVICE) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.OTHER_PROTECTOR) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_SCORE) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_RANK) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_SCORE) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_RANK) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.REG_TRANSACTION_VOLUME) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.REG_WORK_FREQUENCY) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_WORK_USAGE_LOC.REG_DISASTER_POSSIBILITY));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.AMOUNT), value.AMOUNT);
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.ISHA_USAGE), OrgConvert.ToNullableInt32(value.ISHA_USAGE));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.ISHA_OTHER_USAGE), value.ISHA_OTHER_USAGE);
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.CHEM_OPERATION_CHIEF), value.CHEM_OPERATION_CHIEF);
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.SUB_CONTAINER), OrgConvert.ToNullableInt32(value.SUB_CONTAINER));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.OTHER_PROTECTOR), value.OTHER_PROTECTOR);
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.ENTRY_DEVICE), value.ENTRY_DEVICE);
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_SCORE), OrgConvert.ToNullableDecimal(value.MEASURES_BEFORE_SCORE));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_RANK), OrgConvert.ToNullableInt32(value.MEASURES_BEFORE_RANK));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_SCORE), OrgConvert.ToNullableDecimal(value.MEASURES_AFTER_SCORE));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_RANK), OrgConvert.ToNullableInt32(value.MEASURES_AFTER_RANK));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.REG_TRANSACTION_VOLUME), OrgConvert.ToNullableInt32(value.REG_TRANSACTION_VOLUME));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.REG_WORK_FREQUENCY), OrgConvert.ToNullableInt32(value.REG_WORK_FREQUENCY));
        //    parameters.Add(nameof(T_LED_WORK_USAGE_LOC.REG_DISASTER_POSSIBILITY), OrgConvert.ToNullableInt32(value.REG_DISASTER_POSSIBILITY));

        //    sql = builder.ToString();
        //}
        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerUsageLocationSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);
            //2018/08/13 TPE Add Start
            sql.AppendLine(" ,(select LEDGER_FLOW_ID,USAGE_LOC_SEQ,");
            sql.AppendLine(" (select USER_CD + ';'");
            sql.AppendLine(" from M_CHIEF,T_LED_WORK_USAGE_LOC");
            sql.AppendLine(" where T_LED_WORK_USAGE_LOC.LOC_ID = M_CHIEF.LOC_ID");
            sql.AppendLine(" and A.LEDGER_FLOW_ID = LEDGER_FLOW_ID");
            sql.AppendLine(" and A.USAGE_LOC_SEQ = USAGE_LOC_SEQ");
            sql.AppendLine(" group by USER_CD");
            sql.AppendLine(" for xml path('')) as USER_CD");
            sql.AppendLine(" from T_LED_WORK_USAGE_LOC A");
            sql.AppendLine(" ) U");
            sql.AppendLine(",(select LEDGER_FLOW_ID,USAGE_LOC_SEQ,");
            sql.AppendLine(" (select USER_NAME + ';'");
            sql.AppendLine(" from M_CHIEF,T_LED_WORK_USAGE_LOC,M_USER");
            sql.AppendLine(" where T_LED_WORK_USAGE_LOC.LOC_ID = M_CHIEF.LOC_ID");
            sql.AppendLine(" and M_USER.USER_CD = M_CHIEF.USER_CD");
            sql.AppendLine(" and A.LEDGER_FLOW_ID = LEDGER_FLOW_ID");
            sql.AppendLine(" and A.USAGE_LOC_SEQ = USAGE_LOC_SEQ");
            sql.AppendLine(" group by USER_NAME");
            sql.AppendLine(" for xml path('')) as USER_NAME");
            sql.AppendLine(" from T_LED_WORK_USAGE_LOC A");
            sql.AppendLine(" ) V");
            //2018/08/13 TPE Add End

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerUsageLocationSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID = @LEDGER_FLOW_ID");

                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
            }

            //2018/08/13 TPE Add Start
            sql.AppendLine(" and");
            sql.AppendLine("T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID = U.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine("T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID = V.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine("T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ = U.USAGE_LOC_SEQ");
            sql.AppendLine(" and");
            sql.AppendLine("T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ = V.USAGE_LOC_SEQ");
            //2018/08/13 TPE Add End

            sql.AppendLine(" )");

            return sql.ToString();
        }

        public IQueryable<LedgerUsageLocationModel> GetSearchResult(LedgerUsageLocationSearchModel condition)
        {

            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            sql.AppendLine("distinct");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.LOC_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.AMOUNT) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.UNITSIZE_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.ISHA_USAGE) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.ISHA_OTHER_USAGE) + ",");

            //2018/08/13 TPE Delete Start
            //sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.CHEM_OPERATION_CHIEF) + ",");
            //2018/08/13 TPE Delete End

            //2018/08/13 TPE Add Start
            sql.AppendLine(" case when len(U.USER_CD) > 0 then");
            sql.AppendLine(" left(U.USER_CD,charindex(';',U.USER_CD,len(U.USER_CD)) -1)");
            sql.AppendLine(" else");
            //sql.AppendLine(" null");  //2018/08/26 TPE Delete
            sql.AppendLine("''");
            sql.AppendLine(" end as CHEM_OPERATION_CHIEF_CD,");
            sql.AppendLine(" case when len(V.USER_NAME) > 0 then");
            sql.AppendLine(" left(V.USER_NAME,charindex(';',V.USER_NAME,len(V.USER_NAME)) -1)");
            sql.AppendLine(" else");
            //sql.AppendLine(" null");  //2018/08/26 TPE Delete
            sql.AppendLine("''");
            sql.AppendLine(" end as CHEM_OPERATION_CHIEF_NAME,");
            //2018/08/13 TPE Add End

            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.SUB_CONTAINER) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.ENTRY_DEVICE) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.OTHER_PROTECTOR) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_SCORE) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_RANK) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_SCORE) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_RANK) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.REG_TRANSACTION_VOLUME) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.REG_WORK_FREQUENCY) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.REG_DISASTER_POSSIBILITY) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_WORK_USAGE_LOC.REG_DATE));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }
            //StartTransaction();
            List<LedgerUsageLocationModel> records = DbUtil.Select<LedgerUsageLocationModel>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<LedgerUsageLocationModel>();

            foreach (DataRow record in dt.Rows)
            {
                var model = new LedgerUsageLocationModel();
                model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)]);
                model.USAGE_LOC_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.USAGE_LOC_SEQ)]);
                model.LOC_ID = OrgConvert.ToNullableInt32(record[nameof(model.LOC_ID)].ToString());
                model.LOC_NAME = locationDic.GetValueToString(model.LOC_ID);
                model.AMOUNT = record[nameof(model.AMOUNT)].ToString();
                model.UNITSIZE_ID = OrgConvert.ToNullableInt32(record[nameof(model.UNITSIZE_ID)]);
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
                model.ISHA_USAGE = OrgConvert.ToNullableInt32(record[nameof(model.ISHA_USAGE)]);
                model.ISHA_USAGE_TEXT = ishaUsageDic.GetValueToString(model.ISHA_USAGE);
                model.ISHA_OTHER_USAGE = record[nameof(model.ISHA_OTHER_USAGE)].ToString();

                //2018/08/13 TPE Delete Start
                //model.CHEM_OPERATION_CHIEF = record[nameof(model.CHEM_OPERATION_CHIEF)].ToString();
                //model.CHEM_OPERATION_CHIEF_NAME = userDic.GetValueToString(model.CHEM_OPERATION_CHIEF);
                //2018/08/13 TPE Delete End

                //2018/08/13 TPE Add Start
                model.CHEM_OPERATION_CHIEF_CD = record[nameof(model.CHEM_OPERATION_CHIEF_CD)].ToString();
                model.CHEM_OPERATION_CHIEF_NAME = record[nameof(model.CHEM_OPERATION_CHIEF_NAME)].ToString();
                //2018/08/13 TPE Add End

                model.SUB_CONTAINER = OrgConvert.ToNullableInt32(record[nameof(model.SUB_CONTAINER)]);
                model.SUB_CONTAINER_TEXT = subcontainerDic.GetValueToString(model.SUB_CONTAINER);
                model.ENTRY_DEVICE = record[nameof(model.ENTRY_DEVICE)].ToString();
                model.OTHER_PROTECTOR = record[nameof(model.OTHER_PROTECTOR)].ToString();
                model.MEASURES_BEFORE_SCORE = OrgConvert.ToNullableDecimal(record[nameof(model.MEASURES_BEFORE_SCORE)]);
                model.MEASURES_BEFORE_RANK = OrgConvert.ToNullableInt32(record[nameof(model.MEASURES_BEFORE_RANK)]);
                model.MEASURES_BEFORE_RANK_TEXT = rankDic.GetValueToString(model.MEASURES_BEFORE_RANK);
                model.MEASURES_AFTER_SCORE = OrgConvert.ToNullableDecimal(record[nameof(model.MEASURES_AFTER_SCORE)]);
                model.MEASURES_AFTER_RANK = OrgConvert.ToNullableInt32(record[nameof(model.MEASURES_AFTER_RANK)]);
                model.MEASURES_AFTER_RANK_TEXT = rankDic.GetValueToString(model.MEASURES_AFTER_RANK);
                model.REG_TRANSACTION_VOLUME = OrgConvert.ToNullableInt32(record[nameof(model.REG_TRANSACTION_VOLUME)]);
                model.REG_TRANSACTION_VOLUME_TEXT = regTransactionVolumeDic.GetValueToString(model.REG_TRANSACTION_VOLUME);
                model.REG_WORK_FREQUENCY = OrgConvert.ToNullableInt32(record[nameof(model.REG_WORK_FREQUENCY)]);
                model.REG_WORK_FREQUENCY_TEXT = regWorkFrequencyDic.GetValueToString(model.REG_WORK_FREQUENCY);
                model.REG_DISASTER_POSSIBILITY = OrgConvert.ToNullableInt32(record[nameof(model.REG_DISASTER_POSSIBILITY)]);
                model.REG_DISASTER_POSSIBILITY_TEXT = regDisasterPossibilityDic.GetValueToString(model.REG_DISASTER_POSSIBILITY);
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);

                using (var protectorDataInfo = new LedgerWorkProtectorDataInfo())
                {
                    model.PROTECTOR_COLLECTION = protectorDataInfo.GetSearchResult(new LedgerProtectorSearchModel()
                    {
                        LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                        USAGE_LOC_SEQ = model.USAGE_LOC_SEQ
                    });
                }

                using (var riskreduceDataInfo = new LedgerWorkRiskReduceDataInfo())
                {
                    model.RISK_REDUCE_COLLECTION = riskreduceDataInfo.GetSearchResult(new LedgerRiskReduceSearchModel()
                    {
                        LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                        USAGE_LOC_SEQ = model.USAGE_LOC_SEQ
                    });
                }

                using (var accesmentregDataInfo = new LedgerWorkAccesmentRegDataInfo())
                {
                    model.ACCESMENT_REG_COLLECTION = accesmentregDataInfo.GetSearchResult(new LedgerAccesmentRegSearchModel()
                    {
                        LEDGER_FLOW_ID = model.LEDGER_FLOW_ID,
                        USAGE_LOC_SEQ = model.USAGE_LOC_SEQ
                    });
                }

                model.FLG_RISK_ASSESSMENT = true;
                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.USAGE_LOC_SEQ);

            return searchQuery;
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override void InitializeDictionary()
        {
            locationDic = new LocationMasterInfo().GetDictionary();
            ishaUsageDic = new IshaUsageMasterInfo().GetDictionary();
            userDic = new UserMasterInfo().GetDictionary();
            subcontainerDic = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary();
            rankDic = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary();
            regTransactionVolumeDic = new CommonMasterInfo(NikonCommonMasterName.REG_TRANSACTION_VOLUME).GetDictionary();
            regWorkFrequencyDic = new CommonMasterInfo(NikonCommonMasterName.REG_WORK_FREQUENCY).GetDictionary();
            regDisasterPossibilityDic = new CommonMasterInfo(NikonCommonMasterName.REG_DISASTER_POSSIBILITY).GetDictionary();
        }

        public override void EditData(T_LED_WORK_USAGE_LOC Value)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_LED_WORK_USAGE_LOC value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.LOC_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.AMOUNT) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.UNITSIZE_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.ISHA_USAGE) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.ISHA_OTHER_USAGE) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.CHEM_OPERATION_CHIEF) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.SUB_CONTAINER) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.ENTRY_DEVICE) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.OTHER_PROTECTOR) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_SCORE) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_RANK) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_SCORE) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_RANK) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.REG_TRANSACTION_VOLUME) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.REG_WORK_FREQUENCY) + ",");
            builder.AppendLine("  " + nameof(T_LED_WORK_USAGE_LOC.REG_DISASTER_POSSIBILITY));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.LOC_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.AMOUNT) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.UNITSIZE_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.ISHA_USAGE) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.ISHA_OTHER_USAGE) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.CHEM_OPERATION_CHIEF) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.SUB_CONTAINER) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.ENTRY_DEVICE) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.OTHER_PROTECTOR) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_SCORE) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_RANK) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_SCORE) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_RANK) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.REG_TRANSACTION_VOLUME) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.REG_WORK_FREQUENCY) + ",");
            builder.AppendLine("  @" + nameof(T_LED_WORK_USAGE_LOC.REG_DISASTER_POSSIBILITY));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.AMOUNT), value.AMOUNT);
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.ISHA_USAGE), OrgConvert.ToNullableInt32(value.ISHA_USAGE));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.ISHA_OTHER_USAGE), value.ISHA_OTHER_USAGE);
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.CHEM_OPERATION_CHIEF), value.CHEM_OPERATION_CHIEF);
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.SUB_CONTAINER), OrgConvert.ToNullableInt32(value.SUB_CONTAINER));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.OTHER_PROTECTOR), value.OTHER_PROTECTOR);
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.ENTRY_DEVICE), value.ENTRY_DEVICE);
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_SCORE), OrgConvert.ToNullableDecimal(value.MEASURES_BEFORE_SCORE));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.MEASURES_BEFORE_RANK), OrgConvert.ToNullableInt32(value.MEASURES_BEFORE_RANK));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_SCORE), OrgConvert.ToNullableDecimal(value.MEASURES_AFTER_SCORE));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.MEASURES_AFTER_RANK), OrgConvert.ToNullableInt32(value.MEASURES_AFTER_RANK));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.REG_TRANSACTION_VOLUME), OrgConvert.ToNullableInt32(value.REG_TRANSACTION_VOLUME));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.REG_WORK_FREQUENCY), OrgConvert.ToNullableInt32(value.REG_WORK_FREQUENCY));
            parameters.Add(nameof(T_LED_WORK_USAGE_LOC.REG_DISASTER_POSSIBILITY), OrgConvert.ToNullableInt32(value.REG_DISASTER_POSSIBILITY));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LED_WORK_USAGE_LOC value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.USAGE_LOC_SEQ) + " = @" + nameof(value.USAGE_LOC_SEQ) + ",");
            builder.AppendLine(" " + nameof(value.LOC_ID) + " = @" + nameof(value.LOC_ID) + ",");
            builder.AppendLine(" " + nameof(value.AMOUNT) + " = @" + nameof(value.AMOUNT) + ",");
            builder.AppendLine(" " + nameof(value.UNITSIZE_ID) + " = @" + nameof(value.UNITSIZE_ID) + ",");
            builder.AppendLine(" " + nameof(value.ISHA_USAGE) + " = @" + nameof(value.ISHA_USAGE) + ",");
            builder.AppendLine(" " + nameof(value.ISHA_OTHER_USAGE) + " = @" + nameof(value.ISHA_OTHER_USAGE) + ",");
            builder.AppendLine(" " + nameof(value.CHEM_OPERATION_CHIEF) + " = @" + nameof(value.CHEM_OPERATION_CHIEF) + ",");
            builder.AppendLine(" " + nameof(value.SUB_CONTAINER) + " = @" + nameof(value.SUB_CONTAINER) + ",");
            builder.AppendLine(" " + nameof(value.ENTRY_DEVICE) + " = @" + nameof(value.ENTRY_DEVICE) + ",");
            builder.AppendLine(" " + nameof(value.OTHER_PROTECTOR) + " = @" + nameof(value.OTHER_PROTECTOR) + ",");
            builder.AppendLine(" " + nameof(value.MEASURES_BEFORE_SCORE) + " = @" + nameof(value.MEASURES_BEFORE_SCORE) + ",");
            builder.AppendLine(" " + nameof(value.MEASURES_BEFORE_RANK) + " = @" + nameof(value.MEASURES_BEFORE_RANK) + ",");
            builder.AppendLine(" " + nameof(value.MEASURES_AFTER_SCORE) + " = @" + nameof(value.MEASURES_AFTER_SCORE) + ",");
            builder.AppendLine(" " + nameof(value.MEASURES_AFTER_RANK) + " = @" + nameof(value.MEASURES_AFTER_RANK) + ",");
            builder.AppendLine(" " + nameof(value.REG_TRANSACTION_VOLUME) + " = @" + nameof(value.REG_TRANSACTION_VOLUME) + ",");
            builder.AppendLine(" " + nameof(value.REG_WORK_FREQUENCY) + " = @" + nameof(value.REG_WORK_FREQUENCY) + ",");
            builder.AppendLine(" " + nameof(value.REG_DISASTER_POSSIBILITY) + " = @" + nameof(value.REG_DISASTER_POSSIBILITY));
            builder.AppendLine("where ");
            builder.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(value.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(value.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
            parameters.Add(nameof(value.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
            parameters.Add(nameof(value.AMOUNT), value.AMOUNT);
            parameters.Add(nameof(value.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));
            parameters.Add(nameof(value.ISHA_USAGE), OrgConvert.ToNullableInt32(value.ISHA_USAGE));
            parameters.Add(nameof(value.ISHA_OTHER_USAGE), value.ISHA_OTHER_USAGE);
            parameters.Add(nameof(value.CHEM_OPERATION_CHIEF), value.CHEM_OPERATION_CHIEF);
            parameters.Add(nameof(value.SUB_CONTAINER), OrgConvert.ToNullableInt32(value.SUB_CONTAINER));
            parameters.Add(nameof(value.ENTRY_DEVICE), value.ENTRY_DEVICE);
            parameters.Add(nameof(value.OTHER_PROTECTOR), value.OTHER_PROTECTOR);
            parameters.Add(nameof(value.MEASURES_BEFORE_SCORE), OrgConvert.ToNullableDecimal(value.MEASURES_BEFORE_SCORE));
            parameters.Add(nameof(value.MEASURES_BEFORE_RANK), OrgConvert.ToNullableInt32(value.MEASURES_BEFORE_RANK));
            parameters.Add(nameof(value.MEASURES_AFTER_SCORE), OrgConvert.ToNullableDecimal(value.MEASURES_AFTER_SCORE));
            parameters.Add(nameof(value.MEASURES_AFTER_RANK), OrgConvert.ToNullableInt32(value.MEASURES_AFTER_RANK) + ",");
            parameters.Add(nameof(value.REG_TRANSACTION_VOLUME), OrgConvert.ToNullableInt32(value.REG_TRANSACTION_VOLUME) + ",");
            parameters.Add(nameof(value.REG_WORK_FREQUENCY), OrgConvert.ToNullableInt32(value.REG_WORK_FREQUENCY) + ",");
            parameters.Add(nameof(value.REG_DISASTER_POSSIBILITY), OrgConvert.ToNullableInt32(value.REG_DISASTER_POSSIBILITY));
            sql = builder.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    locationDic = null;
                    ishaUsageDic = null;
                    userDic = null;
                    rankDic = null;
                    regTransactionVolumeDic = null;
                    regWorkFrequencyDic = null;
                    regDisasterPossibilityDic = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        /// <summary>
        /// 保存ロジック
        /// </summary>
        /// <param name="ledger">LedgerModel</param>
        /// <param name="usageLocations">IEnumerable<LedgerUsageLocationModel></param>
        public void Save(LedgerModel ledger, IEnumerable<LedgerUsageLocationModel> usageLocations)
        {
            var condition = new LedgerUsageLocationSearchModel();
            condition.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
            var deleteDataArray = GetSearchResult(condition);
            Remove(deleteDataArray.Select(x => (T_LED_WORK_USAGE_LOC)x), true);
            var i = 1;
            foreach (var usageLocation in usageLocations)
            {
                usageLocation.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                usageLocation.USAGE_LOC_SEQ = i;
                i++;

                using (var protectorDataInfo = new LedgerWorkProtectorDataInfo() { SQLContext = SQLContext})
                {
                    protectorDataInfo.Save(ledger, usageLocation);

                    if (!protectorDataInfo.IsValid)
                    {
                        IsValid = protectorDataInfo.IsValid;
                        ErrorMessages.AddRange(protectorDataInfo.ErrorMessages);
                        return;
                    }
                }

                using (var riskreduceDataInfo = new LedgerWorkRiskReduceDataInfo() { SQLContext = SQLContext })
                {
                    riskreduceDataInfo.Save(ledger, usageLocation);

                    if (!riskreduceDataInfo.IsValid)
                    {
                        IsValid = riskreduceDataInfo.IsValid;
                        ErrorMessages.AddRange(riskreduceDataInfo.ErrorMessages);
                        return;
                    }
                }

                using (var accesmentregDataInfo = new LedgerWorkAccesmentRegDataInfo() { SQLContext = SQLContext })
                {
                    accesmentregDataInfo.Save(ledger, usageLocation);

                    if (!accesmentregDataInfo.IsValid)
                    {
                        IsValid = accesmentregDataInfo.IsValid;
                        ErrorMessages.AddRange(accesmentregDataInfo.ErrorMessages);
                        return;
                    }
                }
            }
            Add(usageLocations.Select(x => (T_LED_WORK_USAGE_LOC)x));
        }

        public override List<T_LED_WORK_USAGE_LOC> GetSearchResult(T_LED_WORK_USAGE_LOC condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_LED_WORK_USAGE_LOC value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(T_LED_WORK_USAGE_LOC value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}