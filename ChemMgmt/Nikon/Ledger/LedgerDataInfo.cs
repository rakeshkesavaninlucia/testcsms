﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.History;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerDataInfo : DataInfoBase<T_MGMT_LEDGER>
    {
        protected override string TableName { get; } = nameof(T_MGMT_LEDGER);

        private IDictionary<int?, string> reasonDic { get; set; }

        private IDictionary<string, string> userDic { get; set; }

        private IDictionary<string, string> groupDic { get; set; }

        private IDictionary<int?, string> figureDic { get; set; }

        private IDictionary<int?, string> unitsizeDic { get; set; }

        private IEnumerable<CommonDataModel<int?>> unitsizeSelectList { get; set; }

        private IDictionary<int?, string> locationDic { get; set; }

        private IDictionary<int?, string> ishaUsageDic { get; set; }

        private IDictionary<int?, string> subcontainerDic { get; set; }

        private IDictionary<int?, string> rankDic { get; set; }

        private IDictionary<int?, string> protectorDic { get; set; }

        private IDictionary<int?, string> regulationDic { get; set; }

        private IDictionary<int?, string> ledgerStatusDic { get; set; }

        private IDictionary<int?, string> riskreduceDic { get; set; }

        public int HistoryID { get; protected set; }

        public LedgerDataInfo()
        {
            reasonDic = new CommonMasterInfo(NikonCommonMasterName.REASON).GetDictionary();
            userDic = new UserMasterInfo().GetDictionary();
            groupDic = new GroupMasterInfo().GetDictionary();
            figureDic = new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetDictionary();
            unitsizeDic = new UnitSizeMasterInfo().GetDictionary();
            unitsizeSelectList = new UnitSizeMasterInfo().GetSelectList();
            locationDic = new LocationMasterInfo().GetDictionary();
            ishaUsageDic = new IshaUsageMasterInfo().GetDictionary();
            subcontainerDic = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary();
            rankDic = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary();
            protectorDic = new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetDictionary();
            regulationDic = new LedRegulationMasterInfo().GetDictionary();
            ledgerStatusDic = new LedgerStatusListMasterInfo().GetDictionary();
            riskreduceDic = new CommonMasterInfo(NikonCommonMasterName.RISK_REDUCE).GetDictionary();
        }
        public LedgerDataInfo(int i)
        {

            switch (i)
            {


                //管理台帳検索時
                case 2:
                    reasonDic = new CommonMasterInfo(NikonCommonMasterName.REASON).GetDictionary();
                    userDic = new UserMasterInfo().GetDictionary();
                    groupDic = new GroupMasterInfo().GetDictionary();
                    figureDic = new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetDictionary();
                    locationDic = new LocationMasterInfo().GetDictionary();
                    ishaUsageDic = new IshaUsageMasterInfo().GetDictionary();
                    subcontainerDic = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary();
                    rankDic = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary();
                    protectorDic = new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetDictionary();
                    ledgerStatusDic = new LedgerStatusListMasterInfo().GetDictionary();
                    regulationDic = new LedRegulationMasterInfo().GetDictionary();
                    riskreduceDic = new CommonMasterInfo(NikonCommonMasterName.RISK_REDUCE).GetDictionary();
                    break;
            }
        }
        //protected override string TableName => throw new NotImplementedException();

        public override void EditData(T_MGMT_LEDGER Value)
        {
            throw new NotImplementedException();
        }
        public List<int> GetDelFlag(LedgerSearchModel condition, bool isFileRead = true)
        {
            List<int> records = null;
            try
            {
                var sql = new StringBuilder();
                DynamicParameters parameters = null;

                sql.AppendLine("select T_MGMT_LEDGER.DEL_FLAG from T_MGMT_LEDGER where ");
                sql.AppendLine("T_MGMT_LEDGER.REG_NO='" + condition.REG_NO + "'");
                //parameters.Add(nameof(condition.REG_NO), condition.REG_NO);

                records = DbUtil.Select<int>(sql.ToString(), parameters);
            }
            catch (Exception ex)
            {

                throw ex;
            }

            return records;
        }
        public IQueryable<LedgerCommonInfo> GetSearchResult(LedgerSearchModel condition, bool isFileRead = true)
        {

            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(" T_MGMT_LEDGER.REG_NO,");
            sql.AppendLine(" T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID,");
            sql.AppendLine(" T_MGMT_LEDGER.APPLI_CLASS,");
            sql.AppendLine(" T_MGMT_LEDGER.GROUP_CD,");
            sql.AppendLine(" T_MGMT_LEDGER.REASON_ID,");
            sql.AppendLine(" T_MGMT_LEDGER.OTHER_REASON,");
            sql.AppendLine(" T_MGMT_LEDGER.CHEM_CD,");
            sql.AppendLine(" T_MGMT_LEDGER.MAKER_NAME,");
            sql.AppendLine(" M_CHEM.CHEM_NAME,");
            sql.AppendLine(" M_CHEM.CAS_NO,");
            sql.AppendLine(" M_CHEM.FIGURE,");
            sql.AppendLine(" M_CHEM.CHEM_CAT,");
            sql.AppendLine(" V_LED_REGULATION.REG_TYPE_ID ARRAY_REG_TYPE_IDS,");
            sql.AppendLine(" M_USER.APPROVER1,");
            sql.AppendLine(" M_USER.APPROVER2,");
            sql.AppendLine(" T_MGMT_LEDGER.OTHER_REGULATION,");
            if (isFileRead)
            {
                sql.AppendLine(" T_MGMT_LEDGER.SDS,");
            }
            sql.AppendLine(" T_MGMT_LEDGER.SDS_MIMETYPE,");
            sql.AppendLine(" T_MGMT_LEDGER.SDS_FILENAME,");
            sql.AppendLine(" T_MGMT_LEDGER.SDS_URL,");
            if (isFileRead)
            {
                sql.AppendLine(" T_MGMT_LEDGER.UNREGISTERED_CHEM,");
            }
            sql.AppendLine(" T_MGMT_LEDGER.UNREGISTERED_CHEM_MIMETYPE,");
            sql.AppendLine(" T_MGMT_LEDGER.UNREGISTERED_CHEM_FILENAME,");
            sql.AppendLine(" T_MGMT_LEDGER.MEMO,");
            sql.AppendLine(" T_LEDGER_HISTORY.REG_USER_CD,");
            sql.AppendLine(" T_MGMT_LEDGER.REG_DATE,");
            sql.AppendLine(" T_MGMT_LEDGER.UPD_DATE,");
            sql.AppendLine(" T_MGMT_LEDGER.DEL_DATE,");
            sql.AppendLine(" T_MGMT_LEDGER.DEL_FLAG,");
            sql.AppendLine(" REG_OLD_NO_COUNT, ");
            sql.AppendLine(" REG_OLD_NO,");
            sql.AppendLine(" CHEM_PRODUCT_COUNT, ");
            sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME,");

            sql.AppendLine(" T_MGMT_LED_LOC.LOC_SEQ,");
            sql.AppendLine(" T_MGMT_LED_LOC.LOC_ID,");
            if (!condition.LOC_BASE)
            {
                sql.AppendLine(" LOC_COUNT,");
            }
            sql.AppendLine(" T_MGMT_LED_LOC.AMOUNT,");
            sql.AppendLine(" T_MGMT_LED_LOC.UNITSIZE_ID,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.LOC_ID USAGE_LOC_ID,");
            if (!condition.USAGE_LOC_BASE)
            {
                sql.AppendLine(" USAGE_LOC_COUNT,");
            }
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.AMOUNT USAGE_LOC_AMOUNT,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.UNITSIZE_ID USAGE_LOC_UNITSIZE_ID,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.ISHA_USAGE,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.ISHA_OTHER_USAGE,");

            //2018/08/14 TPE Delete Start
            //sql.AppendLine(" T_MGMT_LED_USAGE_LOC.CHEM_OPERATION_CHIEF,");
            //2018/08/14 TPE Delete End

            //2018/08/14 TPE Add Start
            sql.AppendLine(" case when len(U.CHEM_OPERATION_CHIEF) > 0 then");
            sql.AppendLine("  left(U.CHEM_OPERATION_CHIEF,charindex(';',U.CHEM_OPERATION_CHIEF,len(U.CHEM_OPERATION_CHIEF)) -1)");
            sql.AppendLine(" else");
            sql.AppendLine("  null");
            sql.AppendLine(" end CHEM_OPERATION_CHIEF,");
            sql.AppendLine(" case when len(U.CHEM_OPERATION_CHIEF_NAME) > 0 then");
            sql.AppendLine("  left(U.CHEM_OPERATION_CHIEF_NAME,charindex(';',U.CHEM_OPERATION_CHIEF_NAME,len(U.CHEM_OPERATION_CHIEF_NAME)) -1)");
            sql.AppendLine(" else");
            sql.AppendLine("  null");
            sql.AppendLine(" end CHEM_OPERATION_CHIEF_NAME,");
            //2018/08/14 TPE Add End

            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.SUB_CONTAINER,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.ENTRY_DEVICE,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.OTHER_PROTECTOR,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.MEASURES_BEFORE_SCORE,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.MEASURES_BEFORE_RANK,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.MEASURES_AFTER_SCORE,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.MEASURES_AFTER_RANK,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.REG_TRANSACTION_VOLUME,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.REG_WORK_FREQUENCY,");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.REG_DISASTER_POSSIBILITY,");
            sql.AppendLine(" T_MGMT_LED_PROTECTOR.PROTECTOR_ID,");
            sql.AppendLine(" PROTECTOR_COUNT,");
            sql.AppendLine(" T_LED_RISK_REDUCE.LED_RISK_REDUCE_TYPE,");
            sql.AppendLine(" T_LED_RISK_REDUCE.LED_RISK_REDUCE_FLAG AS REAL,");
            sql.AppendLine(" RISK_REDUCE_COUNT,");
            sql.AppendLine(" T_LED_ACCESMENT_REG.REG_TYPE_ID,");
            sql.AppendLine(" REGULATION_COUNT");

            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<LedgerCommonInfo> records = DbUtil.Select<LedgerCommonInfo>(sql.ToString(), parameters);
            //DataTable dt = ConvertToDataTable(records);
            //var dataArray = new List<LedgerCommonInfo>();

            // ForEach内のSelect処理を移動
            var regulationInfo = new LedRegulationMasterInfo();

            for (int i = 0; i < records.Count; i++)
            {
               // var model = new LedgerCommonInfo();
                records[i].TableType = TableType.Ledger;
                records[i].LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(records[i].RECENT_LEDGER_FLOW_ID.ToString());
                //if (record.REG_NO == null)
                //{
                //  model.REG_NO = string.Empty;
                //}
                //else
                //{
                //  model.REG_NO = record.REG_NO;
                //}

                //model.RECENT_LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record.RECENT_LEDGER_FLOW_ID);

                //model.APPLI_CLASS = OrgConvert.ToNullableInt32(record.APPLI_CLASS);
                //if (record.GROUP_CD == null)
                //{
                //  model.GROUP_CD = string.Empty;
                //}
                //else
                //{
                //  model.GROUP_CD = record.GROUP_CD;
                //}
                records[i].GROUP_NAME = groupDic.GetValueToString(records[i].GROUP_CD);
                //model.REASON_ID = OrgConvert.ToNullableInt32(record.REASON_ID);
                records[i].REASON = reasonDic.GetValueToString(records[i].REASON_ID);
                //if (record.OTHER_REASON == null)
                //{
                //  model.OTHER_REASON = string.Empty;
                //}
                //else
                //{
                //  model.OTHER_REASON = record.OTHER_REASON;
                //}
                //if (record.CHEM_CD == null)
                //{
                //   model.CHEM_CD = string.Empty;
                //}
                //else
                //{
                //  model.CHEM_CD = record.CHEM_CD;
                //}
                //if (record.MAKER_NAME == null)
                //{
                //  model.MAKER_NAME = string.Empty;
                //}
                //else
                //{
                //  model.MAKER_NAME = record.MAKER_NAME;
                //}
                if (records[i].CHEM_CD == NikonConst.UnregisteredChemCd)
                {
                    records[i].CHEM_NAME = NikonConst.UnregisteredChemName;
                }
                else
                {
                    records[i].CHEM_NAME = records[i].CHEM_NAME;
                }
                //if (record.CAS_NO == null)
                //{
                //  model.CAS_NO = string.Empty;
                //}
                //else
                //{
                //  model.CAS_NO = record.CAS_NO;
                //}
                //model.FIGURE = OrgConvert.ToNullableInt32(record.FIGURE);
                records[i].FIGURE_NAME = figureDic.GetValueToString(records[i].FIGURE);
                //if (record.CHEM_CAT == null)
                //{
                //  model.CHEM_CAT = string.Empty;
                //}
                //else
                //{
                //  model.CHEM_CAT = record.CHEM_CAT;
                //}
                //if (record.OTHER_REGULATION == null)
                //{
                //  model.OTHER_REGULATION = string.Empty;
                //}
                //else
                //{
                //  model.OTHER_REGULATION = record.OTHER_REGULATION;
                //}
                if (isFileRead == true && records[i].SDS != null)
                {
                    records[i].SDS = (byte[])records[i].SDS;
                    records[i].SDS_MIMETYPE = records[i].SDS_MIMETYPE;
                    records[i].SDS_FILENAME = records[i].SDS_FILENAME;
                }
                else
                {
                    records[i].SDS = new byte[0];
                    records[i].SDS_MIMETYPE = records[i].SDS_MIMETYPE;
                    records[i].SDS_FILENAME = records[i].SDS_FILENAME;
                }
                //if (record.SDS_URL == null)
                //{
                //  model.SDS_URL = string.Empty;
                //}
                //else
                //{
                //  model.SDS_URL = record.SDS_URL;
                //}
                if (isFileRead == true && records[i].UNREGISTERED_CHEM != null)
                {
                    records[i].UNREGISTERED_CHEM = (byte[])records[i].UNREGISTERED_CHEM;
                    records[i].UNREGISTERED_CHEM_MIMETYPE = records[i].UNREGISTERED_CHEM_MIMETYPE;
                    records[i].UNREGISTERED_CHEM_FILENAME = records[i].UNREGISTERED_CHEM_FILENAME;
                    records[i].HasUnregisteredChem = true;
                }
                else
                {
                    records[i].UNREGISTERED_CHEM = new byte[0];
                    records[i].UNREGISTERED_CHEM_MIMETYPE = records[i].UNREGISTERED_CHEM_MIMETYPE;
                    records[i].UNREGISTERED_CHEM_FILENAME = records[i].UNREGISTERED_CHEM_FILENAME;
                    records[i].HasUnregisteredChem = true;
                }
                //if (record.MEMO == null)
                //{
                //  model.MEMO = string.Empty;
                //}
                //else
                //{
                //  model.MEMO = record.MEMO;
                //}
                //model.REG_DATE = OrgConvert.ToNullableDateTime(record.REG_DATE);
                //model.UPD_DATE = OrgConvert.ToNullableDateTime(record.UPD_DATE);
                //model.DEL_DATE = OrgConvert.ToNullableDateTime(record.DEL_DATE);
                //model.DEL_FLAG = OrgConvert.ToNullableInt32(record.DEL_FLAG);
                //model.REG_OLD_NO_COUNT = OrgConvert.ToNullableInt32(record.REG_OLD_NO_COUNT);
                //if (record.REG_OLD_NO == null)
                //{
                //  model.REG_OLD_NO = string.Empty;
                //}
                //else
                //{
                //   model.REG_OLD_NO = record.REG_OLD_NO;
                //}

                //model.CHEM_PRODUCT_COUNT = OrgConvert.ToNullableInt32(record.CHEM_PRODUCT_COUNT);
                //if (record.PRODUCT_NAME == null)
                //{
                //  model.PRODUCT_NAME = string.Empty;
                //}
                //else
                //{
                //  model.PRODUCT_NAME = record.PRODUCT_NAME;
                //}
                //if (record.REG_USER_CD == null)
                //{
                //  model.REG_USER_CD = string.Empty;
                //}
                //else
                //{
                //  model.REG_USER_CD = record.REG_USER_CD;
                //}
                records[i].APPLICANT = userDic.GetValueToString(records[i].REG_USER_CD);
                //if (record.APPROVER1 == null)
                //{
                //  model.APPROVER1 = string.Empty;
                //}
                //else
                //{
                //  model.APPROVER1 = record.APPROVER1;
                //}

                //if (record.APPROVER2 == null)
                //{
                //  model.APPROVER2 = string.Empty;
                //}
                //else
                //{
                //   model.APPROVER2 = record.APPROVER2;
                //}
                records[i].NID = records[i].REG_USER_CD;

                //model.REG_TYPE_ID = OrgConvert.ToNullableInt32(record.REG_TYPE_ID);
                if (records[i].ARRAY_REG_TYPE_IDS == null)
                {
                    records[i].ARRAY_REG_TYPE_IDS = string.Empty;
                }
                else
                {
                    records[i].ARRAY_REG_TYPE_IDS = records[i].ARRAY_REG_TYPE_IDS;
                }
                if (!condition.REGULATION_BASE)
                {
                    records[i].REGULATION_COUNT = OrgConvert.ToNullableInt32(records[i].REGULATION_COUNT);
                }

                if (records[i].ARRAY_REG_TYPE_IDS != "")
                {
                    var sqlReg = new StringBuilder();
                    sqlReg.AppendLine("select ");
                    sqlReg.AppendLine("V_REGULATION_ALL.REG_TEXT ");
                    sqlReg.AppendLine("from ");
                    sqlReg.AppendLine("V_REGULATION_ALL where V_REGULATION_ALL.REG_TYPE_ID in (");
                    sqlReg.AppendLine(records[i].ARRAY_REG_TYPE_IDS);
                    sqlReg.AppendLine(") ");
                    sqlReg.AppendLine("and V_REGULATION_ALL.DEL_FLAG = 0 ");
                    sqlReg.AppendLine("order by V_REGULATION_ALL.P_REG_TYPE_ID, V_REGULATION_ALL.SORT_ORDER ");

                    List<LedgerCommonInfo> recordsReg = DbUtil.Select<LedgerCommonInfo>(sqlReg.ToString(), parameters);
                    string strRegText = "";
                    foreach (LedgerCommonInfo row in recordsReg)
                    {
                        if (strRegText != "")
                        {
                            strRegText += ",";
                        }
                        strRegText += row.REG_TEXT.ToString();
                    }
                    records[i].REG_TEXT = strRegText;
                }

                //model.LOC_ID = OrgConvert.ToNullableInt32(record.LOC_ID);
                if (!condition.LOC_BASE)
                {
                    records[i].LOC_COUNT = OrgConvert.ToNullableInt32(records[i].LOC_COUNT);
                }
                records[i].LOC_NAME = locationDic.GetValueToString(records[i].LOC_ID);
                //if (record.AMOUNT == null)
                //{
                //  model.AMOUNT = string.Empty;
                //}
                //else
                //{
                //  model.AMOUNT = record.AMOUNT;
                //}


                //model.UNITSIZE_ID = OrgConvert.ToNullableInt32(record.UNITSIZE_ID);
                //model.USAGE_LOC_ID = OrgConvert.ToNullableInt32(record.USAGE_LOC_ID);
                if (!condition.USAGE_LOC_BASE)
                {
                    records[i].USAGE_LOC_COUNT = OrgConvert.ToNullableInt32(records[i].USAGE_LOC_COUNT);
                }
                records[i].USAGE_LOC_NAME = locationDic.GetValueToString(records[i].USAGE_LOC_ID);
                //if (record.USAGE_LOC_AMOUNT == null)
                //{
                //  model.USAGE_LOC_AMOUNT = string.Empty;
                //}
                //else
                //{
                //  model.USAGE_LOC_AMOUNT = record.USAGE_LOC_AMOUNT;
                //}
                //model.USAGE_LOC_UNITSIZE_ID = OrgConvert.ToNullableInt32(record.USAGE_LOC_UNITSIZE_ID);
                //model.ISHA_USAGE = OrgConvert.ToNullableInt32(record.ISHA_USAGE);
                records[i].ISHA_USAGE_TEXT = ishaUsageDic.GetValueToString(records[i].ISHA_USAGE);
                //if (record.ISHA_OTHER_USAGE == null)
                //{
                //  model.ISHA_OTHER_USAGE = string.Empty;
                //}
                //else
                //{
                //  model.ISHA_OTHER_USAGE = record.ISHA_OTHER_USAGE;
                //}
                //if (record.CHEM_OPERATION_CHIEF == null)
                //{
                //  model.CHEM_OPERATION_CHIEF = string.Empty;
                //}
                //else
                //{
                //  model.CHEM_OPERATION_CHIEF = record.CHEM_OPERATION_CHIEF;
                //}

                //2018/08/14 TPE Delete Start
                //model.CHEM_OPERATION_CHIEF_NAME = userDic.GetValueToString(model.CHEM_OPERATION_CHIEF);
                //2018/08/14 TPE Delete End

                //2018/08/14 TPE Add Start
                //if (record.CHEM_OPERATION_CHIEF_NAME == null)
                //{
                //  model.CHEM_OPERATION_CHIEF_NAME = string.Empty;
                //}
                //else
                //{
                //  model.CHEM_OPERATION_CHIEF_NAME = record.CHEM_OPERATION_CHIEF_NAME;
                //}
                //2018/08/14 TPE Add End

                //model.SUB_CONTAINER = OrgConvert.ToNullableInt32(record.SUB_CONTAINER);
                records[i].SUB_CONTAINER_TEXT = subcontainerDic.GetValueToString(records[i].SUB_CONTAINER);
                //if (record.ENTRY_DEVICE == null)
                //{
                //  model.ENTRY_DEVICE = string.Empty;
                //}
                //else
                //{
                //  model.ENTRY_DEVICE = record.ENTRY_DEVICE;
                //}
                //if (record.OTHER_PROTECTOR == null)
                //{
                //   model.OTHER_PROTECTOR = string.Empty;
                //}
                //else
                //{
                //  model.OTHER_PROTECTOR = record.OTHER_PROTECTOR;
                //}
                //model.MEASURES_BEFORE_SCORE = OrgConvert.ToNullableDecimal(record.MEASURES_BEFORE_SCORE);
                //model.MEASURES_BEFORE_RANK = OrgConvert.ToNullableInt32(record.MEASURES_BEFORE_RANK);
                records[i].MEASURES_BEFORE_RANK_TEXT = rankDic.GetValueToString(records[i].MEASURES_BEFORE_RANK);
                //model.MEASURES_AFTER_SCORE = OrgConvert.ToNullableDecimal(record.MEASURES_AFTER_SCORE);
                //model.MEASURES_AFTER_RANK = OrgConvert.ToNullableInt32(record.MEASURES_AFTER_RANK);
                records[i].MEASURES_AFTER_RANK_TEXT = rankDic.GetValueToString(records[i].MEASURES_AFTER_RANK);
                //model.REG_TRANSACTION_VOLUME = OrgConvert.ToNullableInt32(record.REG_TRANSACTION_VOLUME);
                records[i].REG_TRANSACTION_VOLUME_TEXT = rankDic.GetValueToString(records[i].REG_TRANSACTION_VOLUME);
                //model.REG_WORK_FREQUENCY = OrgConvert.ToNullableInt32(record.REG_WORK_FREQUENCY);
                records[i].REG_WORK_FREQUENCY_TEXT = rankDic.GetValueToString(records[i].REG_WORK_FREQUENCY);
                //model.REG_DISASTER_POSSIBILITY = OrgConvert.ToNullableInt32(record.REG_DISASTER_POSSIBILITY);
                records[i].REG_DISASTER_POSSIBILITY_TEXT = rankDic.GetValueToString(records[i].REG_DISASTER_POSSIBILITY);
                //model.PROTECTOR_ID = OrgConvert.ToNullableInt32(record.PROTECTOR_ID);
                records[i].PROTECTOR_NAME = protectorDic.GetValueToString(records[i].PROTECTOR_ID);
                //model.PROTECTOR_COUNT = OrgConvert.ToNullableInt32(record.PROTECTOR_COUNT);

                if (records[i].DEL_FLAG == (int)DEL_FLAG.Deleted)
                {
                    records[i].Status = LedgerStatusConditionType.Abolished;
                }
                else
                {
                    records[i].Status = LedgerStatusConditionType.Using;
                }
                records[i].STATUS_TEXT = ledgerStatusDic.GetValueToString(records[i]._Status);

                var regulations = regulationInfo.GetSelectedItems(records[i].REG_TYPE_ID_COLLECTION).AsQueryable();
                records[i].REQUIRED_CHEM = regulations.Any(x => x.WORKTIME_MGMT == 1) ? "該当" : "非該当";
                var hazmatText = regulations.FirstOrDefault(x => x.REG_TEXT.IndexOf("消防法") != -1)?.REG_TEXT;
                records[i].HAZMAT_TYPE = hazmatText ?? "非該当";

                //dataArray.Add(model);
            }


            var searchQuery = records.AsQueryable();

            regulationInfo.Dispose();

            searchQuery.OrderBy(x => x.REG_NO);

            return searchQuery;


        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override void CreateUpdateText(T_MGMT_LEDGER value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }


        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);

            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_CHEM");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LEDGER.CHEM_CD = M_CHEM.CHEM_CD");

            sql.AppendLine("  left join");
            sql.AppendLine(" (");
            sql.AppendLine("select * from M_CHEM_PRODUCT t1");
            sql.AppendLine("where not exists(");
            sql.AppendLine("select");
            sql.AppendLine("  1");
            sql.AppendLine(" from M_CHEM_PRODUCT t2");
            sql.AppendLine("where");
            sql.AppendLine("  t1.CHEM_CD = t2.CHEM_CD");
            sql.AppendLine("  and");
            sql.AppendLine("  t1.PRODUCT_SEQ > t2.PRODUCT_SEQ)");
            sql.AppendLine(") M_CHEM_PRODUCT");
            sql.AppendLine(" on");
            sql.AppendLine(" M_CHEM.CHEM_CD = M_CHEM_PRODUCT.CHEM_CD");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select CHEM_CD, COUNT(1) CHEM_PRODUCT_COUNT from M_CHEM_PRODUCT group by CHEM_CD) CHEM_PRODUCT_COUNT");
            sql.AppendLine("on");
            sql.AppendLine(" M_CHEM.CHEM_CD = CHEM_PRODUCT_COUNT.CHEM_CD");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select REG_NO, COUNT(1) REG_OLD_NO_COUNT from T_MGMT_LED_REG_OLD_NO group by REG_NO) COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LEDGER.REG_NO = COUNT.REG_NO");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_LED_REGULATION");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LEDGER.REG_NO = V_LED_REGULATION.REG_NO");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_USER");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LEDGER.REG_USER_CD = M_USER.USER_CD");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (");
            sql.AppendLine("  select REG_NO, REG_OLD_NO from T_MGMT_LED_REG_OLD_NO O1 where REG_OLD_NO_SEQ = ");
            sql.AppendLine("  (");
            sql.AppendLine("   select MIN(REG_OLD_NO_SEQ) from T_MGMT_LED_REG_OLD_NO O2 where O1.REG_NO = O2.REG_NO");
            sql.AppendLine("  )");
            sql.AppendLine(" ) OLD_NO");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LEDGER.REG_NO = OLD_NO.REG_NO");

            if (condition.LOC_BASE)
            {
                sql.AppendLine(" left join T_MGMT_LED_LOC on T_MGMT_LEDGER.REG_NO = T_MGMT_LED_LOC.REG_NO");
            }
            else
            {
                sql.AppendLine(" left join");
                sql.AppendLine(" (");
                sql.AppendLine(" select * from T_MGMT_LED_LOC t1 ");
                sql.AppendLine(" where not exists (");
                sql.AppendLine("  select ");
                sql.AppendLine("   1");
                sql.AppendLine("  from T_MGMT_LED_LOC t2 ");
                sql.AppendLine("  where ");
                sql.AppendLine("   t1.REG_NO = t2.REG_NO ");
                sql.AppendLine("   and ");
                sql.AppendLine("   t1.LOC_SEQ > t2.LOC_SEQ ");
                sql.AppendLine("  group by REG_NO)");
                sql.AppendLine(" ) T_MGMT_LED_LOC");
                sql.AppendLine(" on");
                sql.AppendLine(" T_MGMT_LEDGER.REG_NO = T_MGMT_LED_LOC.REG_NO");
                sql.AppendLine(" left outer join");
                sql.AppendLine(" (select REG_NO, COUNT(1) LOC_COUNT from T_MGMT_LED_LOC group by REG_NO) LOC_COUNT");
                sql.AppendLine(" on");
                sql.AppendLine(" T_MGMT_LEDGER.REG_NO = LOC_COUNT.REG_NO");
            }
            if (condition.USAGE_LOC_BASE)
            {
                sql.AppendLine(" left join T_MGMT_LED_USAGE_LOC on T_MGMT_LEDGER.REG_NO = T_MGMT_LED_USAGE_LOC.REG_NO");
            }
            else
            {
                sql.AppendLine(" left join");
                sql.AppendLine(" (");
                sql.AppendLine(" select * from T_MGMT_LED_USAGE_LOC t1 ");
                sql.AppendLine(" where not exists (");
                sql.AppendLine("  select ");
                sql.AppendLine("   1");
                sql.AppendLine("  from T_MGMT_LED_USAGE_LOC t2");
                sql.AppendLine("  where ");
                sql.AppendLine("   t1.REG_NO = t2.REG_NO ");
                sql.AppendLine("   and");
                sql.AppendLine("   t1.USAGE_LOC_SEQ > t2.USAGE_LOC_SEQ)");
                sql.AppendLine(" ) T_MGMT_LED_USAGE_LOC");
                sql.AppendLine(" on");
                sql.AppendLine(" T_MGMT_LEDGER.REG_NO = T_MGMT_LED_USAGE_LOC.REG_NO");
                sql.AppendLine(" left outer join");
                sql.AppendLine(" (select REG_NO, COUNT(1) USAGE_LOC_COUNT from T_MGMT_LED_USAGE_LOC group by REG_NO) USAGE_LOC_COUNT");
                sql.AppendLine(" on");
                sql.AppendLine(" T_MGMT_LEDGER.REG_NO = USAGE_LOC_COUNT.REG_NO");
            }

            sql.AppendLine(" left join");
            sql.AppendLine(" (");
            sql.AppendLine(" select * from T_MGMT_LED_PROTECTOR t1 ");
            sql.AppendLine(" where not exists (");
            sql.AppendLine("  select ");
            sql.AppendLine("   1");
            sql.AppendLine("  from T_MGMT_LED_PROTECTOR t2");
            sql.AppendLine("  where ");
            sql.AppendLine("   t1.REG_NO = t2.REG_NO ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.USAGE_LOC_SEQ = t2.USAGE_LOC_SEQ ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.PROTECTOR_SEQ > t2.PROTECTOR_SEQ)");
            sql.AppendLine(" ) T_MGMT_LED_PROTECTOR");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.REG_NO = T_MGMT_LED_PROTECTOR.REG_NO");
            sql.AppendLine(" and");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ = T_MGMT_LED_PROTECTOR.USAGE_LOC_SEQ");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select REG_NO, USAGE_LOC_SEQ, COUNT(1) PROTECTOR_COUNT from T_MGMT_LED_PROTECTOR group by REG_NO, USAGE_LOC_SEQ) PROTECTOR_COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.REG_NO = PROTECTOR_COUNT.REG_NO");
            sql.AppendLine(" and");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ = PROTECTOR_COUNT.USAGE_LOC_SEQ");

            sql.AppendLine(" left join");
            sql.AppendLine(" (");
            sql.AppendLine(" select * from T_LED_ACCESMENT_REG t1 ");
            sql.AppendLine(" where not exists (");
            sql.AppendLine("  select ");
            sql.AppendLine("   1");
            sql.AppendLine("  from T_LED_ACCESMENT_REG t2");
            sql.AppendLine("  where ");
            sql.AppendLine("   t1.REG_NO = t2.REG_NO ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.USAGE_LOC_SEQ = t2.USAGE_LOC_SEQ ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.LED_ACCESMENT_REG_ID > t2.LED_ACCESMENT_REG_ID)");
            sql.AppendLine(" ) T_LED_ACCESMENT_REG");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.REG_NO = T_LED_ACCESMENT_REG.REG_NO");
            sql.AppendLine(" and");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ = T_LED_ACCESMENT_REG.USAGE_LOC_SEQ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select REG_NO, USAGE_LOC_SEQ, COUNT(1) REGULATION_COUNT from T_LED_ACCESMENT_REG group by REG_NO, USAGE_LOC_SEQ) REGULATION_COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.REG_NO = REGULATION_COUNT.REG_NO");
            sql.AppendLine(" and");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ = REGULATION_COUNT.USAGE_LOC_SEQ");

            sql.AppendLine(" left join");
            sql.AppendLine(" (");
            sql.AppendLine(" select * from T_LED_RISK_REDUCE t1 ");
            sql.AppendLine(" where not exists (");
            sql.AppendLine("  select ");
            sql.AppendLine("   1");
            sql.AppendLine("  from T_LED_RISK_REDUCE t2");
            sql.AppendLine("  where ");
            sql.AppendLine("   t1.REG_NO = t2.REG_NO ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.USAGE_LOC_SEQ = t2.USAGE_LOC_SEQ ");
            sql.AppendLine("   and ");
            sql.AppendLine("   t1.LED_RISK_REDUCE_ID > t2.LED_RISK_REDUCE_ID)");
            sql.AppendLine(" ) T_LED_RISK_REDUCE");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.REG_NO = T_LED_RISK_REDUCE.REG_NO");
            sql.AppendLine(" and");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ = T_LED_RISK_REDUCE.USAGE_LOC_SEQ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select REG_NO, USAGE_LOC_SEQ, COUNT(1) RISK_REDUCE_COUNT from T_LED_RISK_REDUCE group by REG_NO, USAGE_LOC_SEQ) RISK_REDUCE_COUNT");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.REG_NO = RISK_REDUCE_COUNT.REG_NO");
            sql.AppendLine(" and");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ = RISK_REDUCE_COUNT.USAGE_LOC_SEQ");

            sql.AppendLine(" left outer join");
            sql.AppendLine(" T_LEDGER_HISTORY");
            sql.AppendLine(" on");
            sql.AppendLine(" T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID = T_LEDGER_HISTORY.LEDGER_FLOW_ID");
            sql.AppendLine(" and");
            sql.AppendLine(" T_MGMT_LEDGER.REG_NO = T_LEDGER_HISTORY.REG_NO");

            //2018/08/14 TPE Add Start
            sql.AppendLine(" left outer join");
            sql.AppendLine(" (select");
            sql.AppendLine("  REG_NO,");
            sql.AppendLine("  CLASS_ID,");
            sql.AppendLine("  USAGE_LOC_SEQ,");
            sql.AppendLine("  (select M_CHIEF.USER_CD + ';'");
            sql.AppendLine("   from M_CHIEF,T_MGMT_LED_USAGE_LOC");
            sql.AppendLine("   where M_CHIEF.LOC_ID = T_MGMT_LED_USAGE_LOC.LOC_ID");
            sql.AppendLine("   and A.LOC_ID = T_MGMT_LED_USAGE_LOC.LOC_ID");
            sql.AppendLine("   and M_CHIEF.LOC_ID = B.LOC_ID");
            sql.AppendLine("   and A.REG_NO = T_MGMT_LED_USAGE_LOC.REG_NO");    //2018/10/05 Rin Add
            sql.AppendLine("   and A.USAGE_LOC_SEQ = T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ");    //2018/10/05 Rin Add
            sql.AppendLine("   order by M_CHIEF.USER_CD");
            sql.AppendLine("   for xml path('')");
            sql.AppendLine("   ) as CHEM_OPERATION_CHIEF,");
            sql.AppendLine("   (select M_USER.USER_NAME + ';'");
            sql.AppendLine("   from M_CHIEF,T_MGMT_LED_USAGE_LOC,M_USER");
            sql.AppendLine("   where M_CHIEF.LOC_ID = T_MGMT_LED_USAGE_LOC.LOC_ID");
            sql.AppendLine("   and M_CHIEF.USER_CD = M_USER.USER_CD");
            sql.AppendLine("   and A.LOC_ID = T_MGMT_LED_USAGE_LOC.LOC_ID");
            sql.AppendLine("   and M_CHIEF.LOC_ID = B.LOC_ID");
            sql.AppendLine("   and A.REG_NO = T_MGMT_LED_USAGE_LOC.REG_NO");    //2018/10/05 Rin Add
            sql.AppendLine("   and A.USAGE_LOC_SEQ = T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ");    //2018/10/05 Rin Add
            sql.AppendLine("   order by M_CHIEF.USER_CD");
            sql.AppendLine("   for xml path('')");
            sql.AppendLine("  ) as CHEM_OPERATION_CHIEF_NAME");
            sql.AppendLine(" from");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC A");
            sql.AppendLine(" ,M_LOCATION B");
            sql.AppendLine(" where");
            sql.AppendLine(" A.LOC_ID = B.LOC_ID");
            sql.AppendLine(" ) U");
            sql.AppendLine(" on");
            sql.AppendLine(" U.REG_NO = T_MGMT_LED_USAGE_LOC.REG_NO");
            sql.AppendLine(" and");
            sql.AppendLine(" U.USAGE_LOC_SEQ = T_MGMT_LED_USAGE_LOC.USAGE_LOC_SEQ");
            //2018/08/14 TPE Add End

            return sql.ToString();
        }

        protected override void InitializeDictionary()
        {
            reasonDic = new CommonMasterInfo(NikonCommonMasterName.REASON).GetDictionary();
            userDic = new UserMasterInfo().GetDictionary();
            groupDic = new GroupMasterInfo().GetDictionary();
            figureDic = new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetDictionary();
            unitsizeDic = new UnitSizeMasterInfo().GetDictionary();
            unitsizeSelectList = new UnitSizeMasterInfo().GetSelectList();
            locationDic = new LocationMasterInfo().GetDictionary();
            ishaUsageDic = new IshaUsageMasterInfo().GetDictionary();
            subcontainerDic = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary();
            rankDic = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary();
            protectorDic = new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetDictionary();
            regulationDic = new RegulationMasterInfo().GetDictionary();
            ledgerStatusDic = new LedgerStatusListMasterInfo().GetDictionary();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.REG_NO_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LEDGER.REG_NO in");
                sql.AppendLine(" (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.REG_NO_COLLECTION.ToList(), nameof(T_MGMT_LEDGER.REG_NO), parameters));
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_NO_COLLECTION)), string.Join(",", condition.REG_NO_COLLECTION));
            }

            if (!string.IsNullOrWhiteSpace(condition.CHEM_NAME))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LEDGER.CHEM_CD in");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_ALIAS.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_ALIAS");
                sql.AppendLine("  where");
                switch ((SearchConditionType)condition.CHEM_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_NAME) = @CHEM_NAME");
                        break;
                }
                sql.AppendLine(" )");
                if ((SearchConditionType)condition.CHEM_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.CHEM_NAME), condition.CHEM_NAME.ToUpper());
                }
                else
                {
                    parameters.Add(nameof(condition.CHEM_NAME), DbUtil.ConvertIntoEscapeChar(condition.CHEM_NAME.ToUpper()));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_NAME)), condition.CHEM_NAME_CONDITION, condition.CHEM_NAME);
            }

            // 商品名
            if (!string.IsNullOrEmpty(condition.PRODUCT_NAME))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LEDGER.CHEM_CD in");
                sql.AppendLine(" ( select M_CHEM_PRODUCT.CHEM_CD from M_CHEM_PRODUCT where");
                switch ((SearchConditionType)condition.PRODUCT_NAME_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME like '%' + @PRODUCT_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME like @PRODUCT_NAME + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME like '%' + @PRODUCT_NAME " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME = @PRODUCT_NAME");
                        break;
                }
                sql.AppendLine(")");
                if ((SearchConditionType)condition.PRODUCT_NAME_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.PRODUCT_NAME), condition.PRODUCT_NAME);
                }
                else
                {
                    parameters.Add(nameof(condition.PRODUCT_NAME), DbUtil.ConvertIntoEscapeChar(condition.PRODUCT_NAME));
                }
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.PRODUCT_NAME)), condition.PRODUCT_NAME_CONDITION, condition.PRODUCT_NAME);
            }

            //CAS No
            if (!string.IsNullOrEmpty(condition.CAS_NO))
            {
                sql.AppendLine(" and");
                switch ((SearchConditionType)condition.CAS_NO_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO like '%' + @CAS_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO like @CAS_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO like '%' + @CAS_NO " + DbUtil.LikeEscapeInfoAdd());
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" M_CHEM.CAS_NO = @CAS_NO");
                        break;
                }
                if ((SearchConditionType)condition.CAS_NO_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.CAS_NO), condition.CAS_NO);
                }
                else
                {
                    parameters.Add(nameof(condition.CAS_NO), DbUtil.ConvertIntoEscapeChar(condition.CAS_NO));
                }
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CAS_NO)), condition.CAS_NO_CONDITION, condition.CAS_NO);
            }

            if (!string.IsNullOrWhiteSpace(condition.REG_NO))
            {
                sql.AppendLine(" and (");
                switch ((SearchConditionType)condition.REG_NO_CONDITION)
                {
                    case SearchConditionType.PartialMatching:
                        sql.AppendLine(" T_MGMT_LEDGER.REG_NO like '%' + @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" or ");
                        sql.AppendLine(" T_MGMT_LEDGER.REG_NO in");
                        sql.AppendLine(" (");
                        sql.AppendLine("  SELECT distinct T_MGMT_LED_REG_OLD_NO.REG_NO FROM T_MGMT_LED_REG_OLD_NO where");
                        sql.AppendLine("  T_MGMT_LED_REG_OLD_NO.REG_OLD_NO like '%' + @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" )");
                        break;
                    case SearchConditionType.ForwardMatching:
                        sql.AppendLine(" T_MGMT_LEDGER.REG_NO like @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" or ");
                        sql.AppendLine(" T_MGMT_LEDGER.REG_NO in");
                        sql.AppendLine(" (");
                        sql.AppendLine("  SELECT distinct T_MGMT_LED_REG_OLD_NO.REG_NO FROM T_MGMT_LED_REG_OLD_NO where");
                        sql.AppendLine("  T_MGMT_LED_REG_OLD_NO.REG_OLD_NO like @REG_NO + '%' " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" )");
                        break;
                    case SearchConditionType.BackwardMatching:
                        sql.AppendLine(" T_MGMT_LEDGER.REG_NO like '%' + @REG_NO " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" or ");
                        sql.AppendLine(" T_MGMT_LEDGER.REG_NO in");
                        sql.AppendLine(" (");
                        sql.AppendLine("  SELECT distinct T_MGMT_LED_REG_OLD_NO.REG_NO FROM T_MGMT_LED_REG_OLD_NO where");
                        sql.AppendLine("  T_MGMT_LED_REG_OLD_NO.REG_OLD_NO like '%' + @REG_NO " + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" )");
                        break;
                    case SearchConditionType.PerfectMatching:
                        sql.AppendLine(" T_MGMT_LEDGER.REG_NO = @REG_NO");
                        sql.AppendLine(" or ");
                        sql.AppendLine(" T_MGMT_LEDGER.REG_NO in");
                        sql.AppendLine(" (");
                        sql.AppendLine("  SELECT distinct T_MGMT_LED_REG_OLD_NO.REG_NO FROM T_MGMT_LED_REG_OLD_NO where");
                        sql.AppendLine("  T_MGMT_LED_REG_OLD_NO.REG_OLD_NO = @REG_NO");
                        sql.AppendLine(" )");
                        break;
                }
                sql.AppendLine(" )");
                if ((SearchConditionType)condition.REG_NO_CONDITION == SearchConditionType.PerfectMatching)
                {
                    parameters.Add(nameof(condition.REG_NO), condition.REG_NO);
                }
                else
                {
                    parameters.Add(nameof(condition.REG_NO), DbUtil.ConvertIntoEscapeChar(condition.REG_NO));
                }

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_NO)), condition.REG_NO_CONDITION, condition.REG_NO);
            }

            // 申請番号
            if (condition.LEDGER_FLOW_ID != null || condition.RECENT_LEDGER_FLOW_ID != null)
            {
                string strFlowId = null;
                if (condition.RECENT_LEDGER_FLOW_ID != null)
                {
                    strFlowId = condition.RECENT_LEDGER_FLOW_ID.ToString();
                }
                else
                {
                    strFlowId = condition.LEDGER_FLOW_ID.ToString();
                }
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID = @RECENT_LEDGER_FLOW_ID");
                parameters.Add("RECENT_LEDGER_FLOW_ID", strFlowId);

                if (condition.LEDGER_FLOW_ID != null)
                {
                    MakeConditionInLineOfCsv("申請番号", strFlowId);
                }
            }

            // 化学物質コード
            if (!string.IsNullOrEmpty(condition.CHEM_CD))
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LEDGER.CHEM_CD in");
                sql.AppendLine(" (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_ALIAS.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_ALIAS");
                sql.AppendLine("  where");
                sql.AppendLine("   upper(M_CHEM_ALIAS.CHEM_CD) =@CHEM_CD");
                sql.AppendLine(" )");

                parameters.Add(nameof(condition.CHEM_CD), condition.CHEM_CD.ToUpper());

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_CD)), condition.CHEM_CD);
            }

            //管理部門
            if (condition.GROUP_CD_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LEDGER.GROUP_CD IN");
                sql.AppendLine(" (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.GROUP_CD_COLLECTION.ToList(), nameof(T_MGMT_LEDGER.GROUP_CD), parameters));
                sql.AppendLine(" )");

                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.GROUP_CD_COLLECTION)), string.Join(",", condition.GROUP_CD_COLLECTION));
            }
            //法規制
            if (condition.REG_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and T_MGMT_LEDGER.CHEM_CD in (");
                sql.AppendLine("  select");
                sql.AppendLine("   distinct(M_CHEM_REGULATION.CHEM_CD)");
                sql.AppendLine("  from");
                sql.AppendLine("   M_CHEM_REGULATION");
                sql.AppendLine("  where");
                sql.AppendLine("   M_CHEM_REGULATION.REG_TYPE_ID in (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.REG_COLLECTION.ToList(), nameof(condition.REG_COLLECTION), parameters));
                sql.AppendLine("   )");
                sql.AppendLine(" )");
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.REG_COLLECTION)),
                    condition.REG_COLLECTION, regulationDic);
            }
            //保管場所
            if (condition.ACTION_LOC_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LEDGER.REG_NO in");
                sql.AppendLine(" ( select T_MGMT_LED_LOC.REG_NO from T_MGMT_LED_LOC where T_MGMT_LED_LOC.LOC_ID in ");
                sql.AppendLine("  (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.ACTION_LOC_COLLECTION.ToList(), nameof(condition.ACTION_LOC_COLLECTION), parameters));
                sql.AppendLine("  )");
                sql.AppendLine(" )");
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.ACTION_LOC_COLLECTION)), string.Join(",", condition.ACTION_LOC_COLLECTION));
            }
            //使用場所
            if (condition.USAGE_LOC_COLLECTION.Count() > 0)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LEDGER.REG_NO in");
                sql.AppendLine(" ( select T_MGMT_LED_USAGE_LOC.REG_NO from T_MGMT_LED_USAGE_LOC where T_MGMT_LED_USAGE_LOC.LOC_ID in ");
                sql.AppendLine("  (");
                sql.AppendLine(DbUtil.CreateInOperator(condition.USAGE_LOC_COLLECTION.ToList(), nameof(condition.USAGE_LOC_COLLECTION), parameters));
                sql.AppendLine("  )");
                sql.AppendLine(" )");
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.USAGE_LOC_COLLECTION)), string.Join(",", condition.USAGE_LOC_COLLECTION));
            }
            if (string.IsNullOrWhiteSpace(condition.CHEM_OPERATION_CHIEF) == false)
            {


                sql.AppendLine(" and exists");
                sql.AppendLine(" (");
                //2018/08/14 TPE Delete Start
                //sql.AppendLine("  select 1 from T_MGMT_LED_USAGE_LOC where T_MGMT_LEDGER.REG_NO = T_MGMT_LED_USAGE_LOC.REG_NO and T_MGMT_LED_USAGE_LOC.CHEM_OPERATION_CHIEF = :CHEM_OPERATION_CHIEF");
                //2018/08/14 TPE Delete End

                //2018/08/14 TPE Add Start
                sql.AppendLine(" select 1 from T_MGMT_LED_USAGE_LOC,M_CHIEF,M_LOCATION where T_MGMT_LEDGER.REG_NO = T_MGMT_LED_USAGE_LOC.REG_NO and T_MGMT_LED_USAGE_LOC.LOC_ID = M_CHIEF.LOC_ID and M_CHIEF.LOC_ID = M_LOCATION.LOC_ID and M_LOCATION.CLASS_ID = 2 and M_CHIEF.USER_CD = @CHEM_OPERATION_CHIEF");
                //2018/08/14 TPE Add End

                sql.AppendLine(" )");

                //2018/08/14 TPE Add Start
                //sql.AppendLine(" and U.CHEM_OPERATION_CHIEF like '%' || :CHEM_OPERATION_CHIEF || '%' " + DbUtil.LikeEscapeInfoAdd());
                //sql.AppendLine(" and U.CLASS_ID = '2'"); 
                //2018/08/14 TPE Add End

                parameters.Add(nameof(condition.CHEM_OPERATION_CHIEF), condition.CHEM_OPERATION_CHIEF);
                MakeConditionInLineOfCsv(condition.GetType().GetProperty(nameof(condition.CHEM_OPERATION_CHIEF)), condition.CHEM_OPERATION_CHIEF.ToString());
            }

            sql.AppendLine(" )");

            switch (condition.LEDGER_STATUS)
            {
                case (int)LedgerStatusConditionType.Using:
                    sql.AppendLine(" and");
                    sql.AppendLine(" T_MGMT_LEDGER.DEL_FLAG = @DEL_FLAG");
                    parameters.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Undelete);
                    break;
                case (int)LedgerStatusConditionType.Applying:
                    sql.AppendLine(" and");
                    sql.AppendLine(" 1 = 0");
                    break;
                case (int)LedgerStatusConditionType.Abolished:
                    sql.AppendLine(" and");
                    sql.AppendLine(" T_MGMT_LEDGER.DEL_FLAG = @DEL_FLAG");
                    parameters.Add(nameof(condition.DEL_FLAG), (int)DEL_FLAG.Deleted);
                    var dic = new IncludesContainedListMasterInfo().GetDictionary();
                    break;
                case (int)LedgerStatusConditionType.All:
                    break;
            }
            MakeConditionInLineOfCsv("状態", ledgerStatusDic[condition.LEDGER_STATUS]);

            if (condition.HasExcludeOverlap)
            {
                sql.AppendLine(" and not exists");
                sql.AppendLine(" (");
                sql.AppendLine("  select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = T_MGMT_LEDGER.REG_NO");
                sql.AppendLine(" )");
            }

            if (condition.REFERENCE_AUTHORITY == Const.cREFERENCE_AUTHORITY_FLAG_NORMAL)
            {
                sql.AppendLine(" and ");
                sql.AppendLine(" T_MGMT_LEDGER.GROUP_CD in ");
                sql.AppendLine(" (");
                sql.AppendLine("    (select M_USER.GROUP_CD from M_USER where M_USER.USER_CD = @LOGIN_USER_CD)");
                sql.AppendLine("    ,(select M_GROUP.P_GROUP_CD from M_GROUP where M_GROUP.GROUP_CD = (select M_USER.GROUP_CD from M_USER where M_USER.USER_CD = @LOGIN_USER_CD))");
                //if (condition.USER_ADMINFLG == Const.cADMIN_FLG_ADMIN)
                //{
                //    sql.AppendLine("    ,(select M_USER.PREVIOUS_GROUP_CD from M_USER where M_USER.USER_CD = :LOGIN_USER_CD)");
                //    sql.AppendLine("    ,(select M_GROUP.P_GROUP_CD from M_GROUP where M_GROUP.GROUP_CD = (select M_USER.PREVIOUS_GROUP_CD from M_USER where M_USER.USER_CD = :LOGIN_USER_CD))");
                sql.AppendLine("    ,(select M_GROUP.GROUP_CD from M_GROUP where M_GROUP.GROUP_CD = (select M_USER.PREVIOUS_GROUP_CD from M_USER where M_USER.USER_CD = @LOGIN_USER_CD) and M_GROUP.DEL_FLAG = 1)");
                sql.AppendLine("    ,(select M_GROUP.P_GROUP_CD from M_GROUP where M_GROUP.GROUP_CD = (select M_USER.PREVIOUS_GROUP_CD from M_USER where M_USER.USER_CD = @LOGIN_USER_CD) and M_GROUP.DEL_FLAG = 1)");
                //}
                sql.AppendLine(" )");
                parameters.Add(nameof(condition.LOGIN_USER_CD), condition.LOGIN_USER_CD);
            }

            if (!string.IsNullOrEmpty(condition.REG_NO_EXT))
            {
                sql.AppendLine(" and T_MGMT_LEDGER.REG_NO <> @REG_NO_EXT");
                parameters.Add(nameof(condition.REG_NO_EXT), condition.REG_NO_EXT);
            }

            return sql.ToString();
        }


        public void AddWithChildren(LedgerModel ledger,
            IEnumerable<LedgerOldRegisterNumberModel> oldRegisterNumbers,
            IEnumerable<LedgerRegulationModel> regulations,
            IEnumerable<LedgerLocationModel> storingLocations,
            IEnumerable<LedgerUsageLocationModel> usageLocations)
        {
            ledger.REG_NO = GetNewRegisterNumber();
            if (string.IsNullOrWhiteSpace(ledger.REG_NO))
            {
                ErrorMessages.Add(string.Format(WarningMessages.CannnotSelect, "登録番号の取得に失敗したため", "最終承認"));
                return;
            }
            base.add((T_MGMT_LEDGER)ledger);
            if (!IsValid)
            {
                return;
            }
            AddLedgerHistory(ledger);
            if (!IsValid)
            {
                return;
            }
            SaveChildrenModels(ledger, oldRegisterNumbers, regulations, storingLocations, usageLocations);
        }

        public void RemoveWithChildren(LedgerModel ledger, IEnumerable<LedgerOldRegisterNumberModel> oldRegisterNumbers, IEnumerable<LedgerRegulationModel> regulations, IEnumerable<LedgerLocationModel> storingLocations, IEnumerable<LedgerUsageLocationModel> usageLocations)
        {
            base.save((T_MGMT_LEDGER)ledger);
            if (!IsValid)
            {
                return;
            }

            base.remove((T_MGMT_LEDGER)ledger, false);
            if (!IsValid)
            {
                return;
            }

            AddLedgerHistory(ledger);
            if (!IsValid)
            {
                return;
            }
            SaveChildrenModels(ledger, oldRegisterNumbers, regulations, storingLocations, usageLocations);
        }


        public void SaveWithChildren(LedgerModel ledger,
          IEnumerable<LedgerOldRegisterNumberModel> oldRegisterNumbers,
          IEnumerable<LedgerRegulationModel> regulations,
          IEnumerable<LedgerLocationModel> storingLocations,
          IEnumerable<LedgerUsageLocationModel> usageLocations)
        {
            base.save((T_MGMT_LEDGER)ledger);
            if (!IsValid)
            {
                return;
            }
            AddLedgerHistory(ledger);
            if (!IsValid)
            {
                return;
            }
            SaveChildrenModels(ledger, oldRegisterNumbers, regulations, storingLocations, usageLocations);
        }
        protected string GetNewRegisterNumber()
        {
            while (true)
            {
                var sql = string.Empty;
                if (Common.DatabaseType == DatabaseType.Oracle)
                {
                    sql = "select REGISTERNUMBER_SEQ.nextval as REGISTERNUMBER_SEQ from dual";
                }
                if (Common.DatabaseType == DatabaseType.SqlServer)
                {
                    sql = "select next value for REGISTERNUMBER_SEQ";
                }
                var dt = DbUtil.Select(sql);
                var row = dt.Rows[0];

                var registerNumber = NikonConst.ManagementLedgerRegisterNumberPrefix + Convert.ToInt32(row[0].ToString()).ToString("D10");

                // 登録番号が使用されていないかチェックし、使用されていればもう一度発行します。
                sql = "select REG_NO from T_MGMT_LEDGER where REG_NO = @REG_NO";
                //var parameters = new Hashtable();
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("REG_NO", registerNumber);

                List<T_MGMT_LEDGER> registerNumberExists;

                registerNumberExists = DbUtil.Select<T_MGMT_LEDGER>(sql, parameters);

                if (registerNumberExists.Count == 0)
                {
                    return registerNumber;
                }
            }
        }

        protected void AddLedgerHistory(LedgerModel ledger)
        {
            using (var historyDataInfo = new LedgerHistoryDataInfo() { Context = Context })
            {
                historyDataInfo.add((T_LEDGER_HISTORY)ledger);
                HistoryID = historyDataInfo.GetIdentity();
                if (!historyDataInfo.IsValid)
                {
                    IsValid = historyDataInfo.IsValid;
                    ErrorMessages.AddRange(historyDataInfo.ErrorMessages);
                    return;
                }
            }
        }

        /// <summary>
        /// 子モデルの保存ロジック
        /// </summary>
        /// <param name="ledger"></param>
        protected void SaveChildrenModels(LedgerModel ledger,
            IEnumerable<LedgerOldRegisterNumberModel> oldRegisterNumbers,
            IEnumerable<LedgerRegulationModel> regulations,
            IEnumerable<LedgerLocationModel> storingLocations,
            IEnumerable<LedgerUsageLocationModel> usageLocations)
        {
            var idx = 1;
            foreach (var data in oldRegisterNumbers)
            {
                data.LEDGER_HISTORY_ID = HistoryID;
                data.REG_OLD_NO_SEQ = idx;
                data.REG_NO = ledger.REG_NO;
                idx++;
            }

            idx = 1;
            foreach (var data in regulations)
            {
                data.LEDGER_HISTORY_ID = HistoryID;
                data.LED_REGULATION_ID = idx;
                data.REG_NO = ledger.REG_NO;
                idx++;
            }

            idx = 1;
            foreach (var data in storingLocations)
            {
                data.LEDGER_HISTORY_ID = HistoryID;
                data.LOC_SEQ = idx;
                data.REG_NO = ledger.REG_NO;
                idx++;
            }

            idx = 1;
            foreach (var data in usageLocations)
            {
                data.LEDGER_HISTORY_ID = HistoryID;
                data.USAGE_LOC_SEQ = idx;
                data.REG_NO = ledger.REG_NO;
                idx++;
                var idxProtector = 1;
                foreach (var protector in data.PROTECTOR_COLLECTION)
                {
                    protector.LEDGER_HISTORY_ID = HistoryID;
                    protector.USAGE_LOC_SEQ = data.USAGE_LOC_SEQ;
                    protector.PROTECTOR_SEQ = idxProtector;
                    protector.REG_NO = ledger.REG_NO;
                    idxProtector++;
                }

                var idxRiskReduce = 1;
                foreach (var riskreduce in data.RISK_REDUCE_COLLECTION)
                {
                    riskreduce.LEDGER_HISTORY_ID = HistoryID;
                    riskreduce.USAGE_LOC_SEQ = data.USAGE_LOC_SEQ;
                    riskreduce.LED_RISK_REDUCE_ID = idxRiskReduce;
                    riskreduce.REG_NO = ledger.REG_NO;
                    idxRiskReduce++;
                }

                var idxAccesmentReg = 1;
                foreach (var accesmentreg in data.ACCESMENT_REG_COLLECTION)
                {
                    accesmentreg.LEDGER_HISTORY_ID = HistoryID;
                    accesmentreg.USAGE_LOC_SEQ = data.USAGE_LOC_SEQ;
                    accesmentreg.LED_ACCESMENT_REG_ID = idxAccesmentReg;
                    accesmentreg.REG_NO = ledger.REG_NO;
                    idxAccesmentReg++;
                }
            }
            using (var dataInfo = new LedgerOldRegisterNumberDataInfo() { Context = Context })
            {
                var db = dataInfo.GetSearchResult(new LedgerOldRegisterNumberSearchModel { REG_NO = ledger.REG_NO });
                dataInfo.remove(db.Select(x => (T_MGMT_LED_REG_OLD_NO)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
                dataInfo.add(oldRegisterNumbers.Select(x => (T_MGMT_LED_REG_OLD_NO)x));
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
                using (var historyDataInfo = new LedgerHistoryOldRegisterNumberDataInfo() { Context = Context })
                {
                    historyDataInfo.add(oldRegisterNumbers.Select(x => (T_LED_HISTORY_REG_OLD_NO)x));
                    if (!historyDataInfo.IsValid)
                    {
                        IsValid = historyDataInfo.IsValid;
                        ErrorMessages.AddRange(historyDataInfo.ErrorMessages);
                        return;
                    }
                }
            }

            using (var dataInfo = new LedgerRegulationDataInfo() { Context = Context })
            {
                var db = dataInfo.GetSearchResult(new LedgerRegulationSearchModel { REG_NO = ledger.REG_NO });
                dataInfo.remove(db.Select(x => (T_LED_REGULATION)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
                dataInfo.add(regulations.Select(x => (T_LED_REGULATION)x));
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
                using (var historyDataInfo = new LedgerHistoryRegulationDataInfo() { Context = Context })
                {
                    historyDataInfo.add(regulations.Select(x => (T_LEDGER_HISTORY_REGULATION)x));
                    if (!historyDataInfo.IsValid)
                    {
                        IsValid = historyDataInfo.IsValid;
                        ErrorMessages.AddRange(historyDataInfo.ErrorMessages);
                        return;
                    }
                }
            }

            using (var dataInfo = new LedgerLocationDataInfo() { Context = Context })
            {
                var db = dataInfo.GetSearchResult(new LedgerLocationSearchModel() { REG_NO = ledger.REG_NO });
                dataInfo.remove(db.Select(x => (T_MGMT_LED_LOC)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
                dataInfo.add(storingLocations.Select(x => (T_MGMT_LED_LOC)x));
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
                using (var historyDataInfo = new LedgerHistoryLocationDataInfo() { Context = Context })
                {
                    historyDataInfo.add(storingLocations.Select(x => (T_LED_HISTORY_LOC)x));
                    if (!historyDataInfo.IsValid)
                    {
                        IsValid = historyDataInfo.IsValid;
                        ErrorMessages.AddRange(historyDataInfo.ErrorMessages);
                        return;
                    }
                }
            }

            using (var dataInfo = new LedgerUsageLocationDataInfo() { Context = Context })
            {
                var db = dataInfo.GetSearchResult(new LedgerUsageLocationSearchModel() { REG_NO = ledger.REG_NO });
                dataInfo.remove(db.Select(x => (T_MGMT_LED_USAGE_LOC)x), true);
                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
                dataInfo.add(usageLocations.Select(x => (T_MGMT_LED_USAGE_LOC)x));

                if (ledger.APPLI_CLASS == (int)AppliClass.Stopped)
                {
                    foreach (var usageLocation in usageLocations)
                    {
                        if (usageLocation.REG_TRANSACTION_VOLUME == -1)
                        {
                            UpdDummy(ledger.REG_NO, usageLocation.USAGE_LOC_SEQ, "REG_TRANSACTION_VOLUME");
                        }
                        if (usageLocation.REG_WORK_FREQUENCY == -1)
                        {
                            UpdDummy(ledger.REG_NO, usageLocation.USAGE_LOC_SEQ, "REG_WORK_FREQUENCY");
                        }
                        if (usageLocation.REG_DISASTER_POSSIBILITY == -1)
                        {
                            UpdDummy(ledger.REG_NO, usageLocation.USAGE_LOC_SEQ, "REG_DISASTER_POSSIBILITY");
                        }
                    }
                }

                if (!dataInfo.IsValid)
                {
                    IsValid = dataInfo.IsValid;
                    ErrorMessages.AddRange(dataInfo.ErrorMessages);
                    return;
                }
                using (var historyDataInfo = new LedgerHistoryUsageLocationDataInfo() { Context = Context })
                {
                    historyDataInfo.add(usageLocations.Select(x => (T_LED_HISTORY_USAGE_LOC)x));

                    if (ledger.APPLI_CLASS == (int)AppliClass.Stopped)
                    {
                        foreach (var usageLocation in usageLocations)
                        {
                            if (usageLocation.REG_TRANSACTION_VOLUME == -1)
                            {
                                UpdDummyHistory(ledger.LEDGER_FLOW_ID, usageLocation.USAGE_LOC_SEQ, "REG_TRANSACTION_VOLUME");
                            }
                            if (usageLocation.REG_WORK_FREQUENCY == -1)
                            {
                                UpdDummyHistory(ledger.LEDGER_FLOW_ID, usageLocation.USAGE_LOC_SEQ, "REG_WORK_FREQUENCY");
                            }
                            if (usageLocation.REG_DISASTER_POSSIBILITY == -1)
                            {
                                UpdDummyHistory(ledger.LEDGER_FLOW_ID, usageLocation.USAGE_LOC_SEQ, "REG_DISASTER_POSSIBILITY");
                            }
                        }
                    }

                    if (!historyDataInfo.IsValid)
                    {
                        IsValid = historyDataInfo.IsValid;
                        ErrorMessages.AddRange(historyDataInfo.ErrorMessages);
                        return;
                    }
                }
                using (var protectorDataInfo = new LedgerProtectorDataInfo() { Context = Context })
                {
                    var dbprotector = protectorDataInfo.GetSearchResult(new LedgerProtectorSearchModel() { REG_NO = ledger.REG_NO });
                    protectorDataInfo.remove(dbprotector.Select(x => (T_MGMT_LED_PROTECTOR)x), true);
                    if (!protectorDataInfo.IsValid)
                    {
                        IsValid = protectorDataInfo.IsValid;
                        ErrorMessages.AddRange(protectorDataInfo.ErrorMessages);
                        return;
                    }
                    foreach (var usage in usageLocations)
                    {
                        protectorDataInfo.add(usage.PROTECTOR_COLLECTION.Select(x => (T_MGMT_LED_PROTECTOR)x));
                        if (!protectorDataInfo.IsValid)
                        {
                            IsValid = protectorDataInfo.IsValid;
                            ErrorMessages.AddRange(protectorDataInfo.ErrorMessages);
                            return;
                        }
                        using (var protectorHistoryDataInfo = new LedgerHistoryProtectorDataInfo() { Context = Context })
                        {
                            protectorHistoryDataInfo.add(usage.PROTECTOR_COLLECTION.Select(x => (T_LED_HISTORY_PROTECTOR)x));
                            if (!protectorHistoryDataInfo.IsValid)
                            {
                                IsValid = protectorHistoryDataInfo.IsValid;
                                ErrorMessages.AddRange(protectorHistoryDataInfo.ErrorMessages);
                                return;
                            }
                        }
                    }
                }

                using (var accesmentregDataInfo = new LedgerAccesmentRegDataInfo() { Context = Context })
                {
                    var dbaccesmentreg = accesmentregDataInfo.GetSearchResult(new LedgerAccesmentRegSearchModel() { REG_NO = ledger.REG_NO });
                    accesmentregDataInfo.remove(dbaccesmentreg.Select(x => (T_LED_ACCESMENT_REG)x), true);
                    if (!accesmentregDataInfo.IsValid)
                    {
                        IsValid = accesmentregDataInfo.IsValid;
                        ErrorMessages.AddRange(accesmentregDataInfo.ErrorMessages);
                        return;
                    }
                    foreach (var usage in usageLocations)
                    {
                        accesmentregDataInfo.add(usage.ACCESMENT_REG_COLLECTION.Select(x => (T_LED_ACCESMENT_REG)x));
                        if (!accesmentregDataInfo.IsValid)
                        {
                            IsValid = accesmentregDataInfo.IsValid;
                            ErrorMessages.AddRange(accesmentregDataInfo.ErrorMessages);
                            return;
                        }
                        using (var accesmentregHistoryDataInfo = new LedgerHistoryAccesmentRegDataInfo() { Context = Context })
                        {
                            accesmentregHistoryDataInfo.add(usage.ACCESMENT_REG_COLLECTION.Select(x => (T_LED_HISTORY_ACCESMENT_REG)x));
                            if (!accesmentregHistoryDataInfo.IsValid)
                            {
                                IsValid = accesmentregHistoryDataInfo.IsValid;
                                ErrorMessages.AddRange(accesmentregHistoryDataInfo.ErrorMessages);
                                return;
                            }
                        }
                    }
                }

                using (var riskreduceDataInfo = new LedgerRiskReduceDataInfo() { Context = Context })
                {
                    var dbriskreduce = riskreduceDataInfo.GetSearchResult(new LedgerRiskReduceSearchModel() { REG_NO = ledger.REG_NO });
                    riskreduceDataInfo.remove(dbriskreduce.Select(x => (T_LED_RISK_REDUCE)x), true);
                    if (!riskreduceDataInfo.IsValid)
                    {
                        IsValid = riskreduceDataInfo.IsValid;
                        ErrorMessages.AddRange(riskreduceDataInfo.ErrorMessages);
                        return;
                    }
                    foreach (var usage in usageLocations)
                    {
                        riskreduceDataInfo.add(usage.RISK_REDUCE_COLLECTION.Select(x => (T_LED_RISK_REDUCE)x));
                        if (!riskreduceDataInfo.IsValid)
                        {
                            IsValid = riskreduceDataInfo.IsValid;
                            ErrorMessages.AddRange(riskreduceDataInfo.ErrorMessages);
                            return;
                        }
                        using (var riskreduceHistoryDataInfo = new LedgerHistoryRiskReduceDataInfo() { Context = Context })
                        {
                            riskreduceHistoryDataInfo.add(usage.RISK_REDUCE_COLLECTION.Select(x => (T_LED_HISTORY_RISK_REDUCE)x));
                            if (!riskreduceHistoryDataInfo.IsValid)
                            {
                                IsValid = riskreduceHistoryDataInfo.IsValid;
                                ErrorMessages.AddRange(riskreduceHistoryDataInfo.ErrorMessages);
                                return;
                            }
                        }
                    }
                }
            }
        }

        // 職場の取扱量/作業頻度/災害発生の可能性 はクラスでRequired設定をしているため
        // ダミーで-1を登録し、改めてnullを登録
        protected void UpdDummy(string no, int? usagelocseq, string field)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = new DynamicParameters();

            sql.AppendLine("update");
            sql.AppendLine(" T_MGMT_LED_USAGE_LOC ");
            sql.AppendLine("set");
            sql.AppendLine(field);
            sql.AppendLine(" = null ");
            sql.AppendLine("where ");
            sql.AppendLine("REG_NO = @REG_NO ");
            sql.AppendLine(" and ");
            sql.AppendLine("USAGE_LOC_SEQ = @USAGE_LOC_SEQ ");

            parameters.Add("REG_NO", no);
            parameters.Add("USAGE_LOC_SEQ", usagelocseq);

            DbUtil.ExecuteUpdate(sql.ToString(), parameters);
        }


        // 職場の取扱量/作業頻度/災害発生の可能性 はクラスでRequired設定をしているため
        // ダミーで-1を登録し、改めてnullを登録
        protected void UpdDummyHistory(int? ledgerflowid, int? usagelocseq, string field)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = new DynamicParameters();

            sql.AppendLine("update");
            sql.AppendLine(" T_LED_HISTORY_USAGE_LOC ");
            sql.AppendLine("set");
            sql.AppendLine(field);
            sql.AppendLine(" = null ");
            sql.AppendLine("where ");
            sql.AppendLine("LEDGER_FLOW_ID = @LEDGER_FLOW_ID ");
            sql.AppendLine(" and ");
            sql.AppendLine("USAGE_LOC_SEQ = @USAGE_LOC_SEQ ");

            parameters.Add("LEDGER_FLOW_ID", ledgerflowid);
            parameters.Add("USAGE_LOC_SEQ", usagelocseq);

            DbUtil.ExecuteUpdate(sql.ToString(), parameters);
        }



        public override List<T_MGMT_LEDGER> GetSearchResult(T_MGMT_LEDGER condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_MGMT_LEDGER value, out string sql, out DynamicParameters parameters)
        {

            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.APPLI_CLASS) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.GROUP_CD) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.REASON_ID) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.OTHER_REASON) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.CHEM_CD) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.MAKER_NAME) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.SDS) + ",");
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.SDS_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.MEMO) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.REG_NO) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.APPLI_CLASS) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.GROUP_CD) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.REASON_ID) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.OTHER_REASON) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.CHEM_CD) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.MAKER_NAME) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.SDS) + ",");
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.SDS_MIMETYPE) + ",");
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.MEMO) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_MGMT_LEDGER.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_MGMT_LEDGER.APPLI_CLASS), value.APPLI_CLASS);
            parameters.Add(nameof(T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID), value.RECENT_LEDGER_FLOW_ID);
            parameters.Add(nameof(T_MGMT_LEDGER.GROUP_CD), value.GROUP_CD);
            parameters.Add(nameof(T_MGMT_LEDGER.REASON_ID), OrgConvert.ToNullableInt32(value.REASON_ID));
            parameters.Add(nameof(T_MGMT_LEDGER.OTHER_REASON), value.OTHER_REASON);
            parameters.Add(nameof(T_MGMT_LEDGER.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(T_MGMT_LEDGER.MAKER_NAME), value.MAKER_NAME);
            parameters.Add(nameof(T_MGMT_LEDGER.OTHER_REGULATION), value.OTHER_REGULATION);
            if (value.SDS != null)
            {
                parameters.Add(nameof(T_MGMT_LEDGER.SDS), value.SDS);
                parameters.Add(nameof(T_MGMT_LEDGER.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(T_MGMT_LEDGER.SDS_FILENAME), value.SDS_FILENAME);
            }
            parameters.Add(nameof(T_MGMT_LEDGER.SDS_URL), value.SDS_URL);
            if (value.UNREGISTERED_CHEM != null)
            {
                parameters.Add(nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM), value.UNREGISTERED_CHEM);
                parameters.Add(nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_MIMETYPE), value.UNREGISTERED_CHEM_MIMETYPE);
                parameters.Add(nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_FILENAME), value.UNREGISTERED_CHEM_FILENAME);
            }
            parameters.Add(nameof(T_MGMT_LEDGER.MEMO), value.MEMO);
            parameters.Add(nameof(T_MGMT_LEDGER.REG_USER_CD), value.REG_USER_CD);

            sql = builder.ToString();

        }

        protected override void CreateInsertText(T_MGMT_LEDGER value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.APPLI_CLASS) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.GROUP_CD) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.REASON_ID) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.OTHER_REASON) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.CHEM_CD) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.MAKER_NAME) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.SDS) + ",");
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.SDS_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  " + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.MEMO) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LEDGER.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.REG_NO) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.APPLI_CLASS) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.GROUP_CD) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.REASON_ID) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.OTHER_REASON) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.CHEM_CD) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.MAKER_NAME) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.SDS) + ",");
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.SDS_MIMETYPE) + ",");
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.SDS_FILENAME) + ",");
            }
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM) + ",");
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.MEMO) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LEDGER.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_MGMT_LEDGER.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_MGMT_LEDGER.APPLI_CLASS), value.APPLI_CLASS);
            parameters.Add(nameof(T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID), value.RECENT_LEDGER_FLOW_ID);
            parameters.Add(nameof(T_MGMT_LEDGER.GROUP_CD), value.GROUP_CD);
            parameters.Add(nameof(T_MGMT_LEDGER.REASON_ID), OrgConvert.ToNullableInt32(value.REASON_ID));
            parameters.Add(nameof(T_MGMT_LEDGER.OTHER_REASON), value.OTHER_REASON);
            parameters.Add(nameof(T_MGMT_LEDGER.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(T_MGMT_LEDGER.MAKER_NAME), value.MAKER_NAME);
            parameters.Add(nameof(T_MGMT_LEDGER.OTHER_REGULATION), value.OTHER_REGULATION);
            if (value.SDS != null)
            {
                parameters.Add(nameof(T_MGMT_LEDGER.SDS), value.SDS);
                parameters.Add(nameof(T_MGMT_LEDGER.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(T_MGMT_LEDGER.SDS_FILENAME), value.SDS_FILENAME);
            }
            parameters.Add(nameof(T_MGMT_LEDGER.SDS_URL), value.SDS_URL);
            if (value.UNREGISTERED_CHEM != null)
            {
                parameters.Add(nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM), value.UNREGISTERED_CHEM);
                parameters.Add(nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_MIMETYPE), value.UNREGISTERED_CHEM_MIMETYPE);
                parameters.Add(nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_FILENAME), value.UNREGISTERED_CHEM_FILENAME);
            }
            parameters.Add(nameof(T_MGMT_LEDGER.MEMO), value.MEMO);
            parameters.Add(nameof(T_MGMT_LEDGER.REG_USER_CD), value.REG_USER_CD);

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_MGMT_LEDGER value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.RECENT_LEDGER_FLOW_ID) + " = :" + nameof(value.RECENT_LEDGER_FLOW_ID) + ",");
            builder.AppendLine(" " + nameof(value.APPLI_CLASS) + " = :" + nameof(value.APPLI_CLASS) + ",");
            builder.AppendLine(" " + nameof(value.GROUP_CD) + " = :" + nameof(value.GROUP_CD) + ",");
            builder.AppendLine(" " + nameof(value.REASON_ID) + " = :" + nameof(value.REASON_ID) + ",");
            builder.AppendLine(" " + nameof(value.OTHER_REASON) + " = :" + nameof(value.OTHER_REASON) + ",");
            builder.AppendLine(" " + nameof(value.CHEM_CD) + " = :" + nameof(value.CHEM_CD) + ",");
            builder.AppendLine(" " + nameof(value.MAKER_NAME) + " = :" + nameof(value.MAKER_NAME) + ",");
            builder.AppendLine(" " + nameof(value.OTHER_REGULATION) + " = :" + nameof(value.OTHER_REGULATION) + ",");
            if (value.SDS != null)
            {
                builder.AppendLine(" " + nameof(value.SDS) + " = :" + nameof(value.SDS) + ",");
                builder.AppendLine(" " + nameof(value.SDS_MIMETYPE) + " = :" + nameof(value.SDS_MIMETYPE) + ",");
                builder.AppendLine(" " + nameof(value.SDS_FILENAME) + " = :" + nameof(value.SDS_FILENAME) + ",");
            }
            builder.AppendLine(" " + nameof(value.SDS_URL) + " = :" + nameof(value.SDS_URL) + ",");
            if (value.UNREGISTERED_CHEM != null)
            {
                builder.AppendLine(" " + nameof(value.UNREGISTERED_CHEM) + " = :" + nameof(value.UNREGISTERED_CHEM) + ",");
                builder.AppendLine(" " + nameof(value.UNREGISTERED_CHEM_MIMETYPE) + " = :" + nameof(value.UNREGISTERED_CHEM_MIMETYPE) + ",");
                builder.AppendLine(" " + nameof(value.UNREGISTERED_CHEM_FILENAME) + " = :" + nameof(value.UNREGISTERED_CHEM_FILENAME) + ",");
            }
            builder.AppendLine(" " + nameof(value.MEMO) + " = :" + nameof(value.MEMO) + ",");
            builder.AppendLine(" " + nameof(SystemColumn.UPD_DATE) + " = getdate()");
            builder.AppendLine("where ");
            builder.AppendLine(getKeyWhere(value));

            parameters.Add(nameof(T_MGMT_LEDGER.RECENT_LEDGER_FLOW_ID), value.RECENT_LEDGER_FLOW_ID);
            parameters.Add(nameof(T_MGMT_LEDGER.APPLI_CLASS), value.APPLI_CLASS);
            parameters.Add(nameof(T_MGMT_LEDGER.GROUP_CD), value.GROUP_CD);
            parameters.Add(nameof(T_MGMT_LEDGER.REASON_ID), OrgConvert.ToNullableInt32(value.REASON_ID));
            parameters.Add(nameof(T_MGMT_LEDGER.OTHER_REASON), value.OTHER_REASON);
            parameters.Add(nameof(T_MGMT_LEDGER.CHEM_CD), value.CHEM_CD);
            parameters.Add(nameof(T_MGMT_LEDGER.MAKER_NAME), value.MAKER_NAME);
            parameters.Add(nameof(T_MGMT_LEDGER.OTHER_REGULATION), value.OTHER_REGULATION);
            if (value.SDS != null)
            {
                parameters.Add(nameof(T_MGMT_LEDGER.SDS), value.SDS);
                parameters.Add(nameof(T_MGMT_LEDGER.SDS_MIMETYPE), value.SDS_MIMETYPE);
                parameters.Add(nameof(T_MGMT_LEDGER.SDS_FILENAME), value.SDS_FILENAME);
            }
            parameters.Add(nameof(T_MGMT_LEDGER.SDS_URL), value.SDS_URL);
            if (value.UNREGISTERED_CHEM != null)
            {
                parameters.Add(nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM), value.UNREGISTERED_CHEM);
                parameters.Add(nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_MIMETYPE), value.UNREGISTERED_CHEM_MIMETYPE);
                parameters.Add(nameof(T_MGMT_LEDGER.UNREGISTERED_CHEM_FILENAME), value.UNREGISTERED_CHEM_FILENAME);
            }
            parameters.Add(nameof(T_MGMT_LEDGER.MEMO), value.MEMO);
            parameters.Add(nameof(T_MGMT_LEDGER.REG_NO), value.REG_NO);

            sql = builder.ToString();
        }
    }
}