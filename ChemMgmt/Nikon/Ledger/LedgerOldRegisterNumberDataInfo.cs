﻿using ChemMgmt.Classes;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerOldRegisterNumberDataInfo : DataInfoBase<T_MGMT_LED_REG_OLD_NO>, IDisposable
    {
        protected override string TableName { get; } = nameof(T_MGMT_LED_REG_OLD_NO);

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerOldRegisterNumberSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerOldRegisterNumberSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.REG_NO != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LED_REG_OLD_NO.REG_NO = @REG_NO");
                parameters.Add(nameof(condition.REG_NO), condition.REG_NO);
            }

            sql.AppendLine(")");

            return sql.ToString();
        }

        //public override IQueryable<T_MGMT_LED_REG_OLD_NO> GetSearchResult(T_MGMT_LED_REG_OLD_NO condition)
        //{
        //    throw new NotImplementedException();
        //}

        //protected override void CreateInsertText(T_MGMT_LED_REG_OLD_NO value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_REG_OLD_NO.REG_NO) + ",");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  @" + nameof(T_MGMT_LED_REG_OLD_NO.REG_NO) + ",");
        //    builder.AppendLine("  @" + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
        //    builder.AppendLine("  @" + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(T_MGMT_LED_REG_OLD_NO.REG_NO), value.REG_NO);
        //    parameters.Add(nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO_SEQ), OrgConvert.ToNullableInt32(value.REG_OLD_NO_SEQ));
        //    parameters.Add(nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO), value.REG_OLD_NO);

        //    sql = builder.ToString();
        //}

        public IQueryable<LedgerOldRegisterNumberModel> GetSearchResult(LedgerOldRegisterNumberSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_REG_OLD_NO.REG_NO) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_REG_OLD_NO.REG_DATE));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<LedgerOldRegisterNumberModel> records = DbUtil.Select<LedgerOldRegisterNumberModel>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<LedgerOldRegisterNumberModel>();

            foreach (DataRow record in dt.Rows)
            {
                var model = new LedgerOldRegisterNumberModel();
                model.REG_NO = record[nameof(model.REG_NO)].ToString();
                model.REG_OLD_NO_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.REG_OLD_NO_SEQ)]);
                model.REG_OLD_NO = record[nameof(model.REG_OLD_NO)].ToString();
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.REG_NO).ThenBy(x => x.REG_OLD_NO_SEQ);

            return searchQuery;
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
        protected override void InitializeDictionary() { }

        //public override void EditData(T_MGMT_LED_REG_OLD_NO Value) { }

        

        //protected override void CreateUpdateText(T_MGMT_LED_REG_OLD_NO value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("update ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine("set ");
        //    builder.AppendLine(" " + nameof(value.REG_OLD_NO) + " = :" + nameof(value.REG_OLD_NO));
        //    builder.AppendLine("where ");
        //    builder.AppendLine(getKeyWhere(value));
        //    parameters.Add(nameof(value.REG_NO), value.REG_NO);
        //    parameters.Add(nameof(value.REG_OLD_NO_SEQ), OrgConvert.ToNullableInt32(value.REG_OLD_NO_SEQ));
        //    parameters.Add(nameof(value.REG_OLD_NO), value.REG_OLD_NO);

        //    sql = builder.ToString();
        //}

        //protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters param)
        //{
        //    throw new NotImplementedException();
        //}

        //protected override void CreateInsertText(T_MGMT_LED_REG_OLD_NO value, out string sql, out DynamicParameters parameters)
        //{
        //    throw new NotImplementedException();
        //}

        //protected override void CreateUpdateText(T_MGMT_LED_REG_OLD_NO value, out string sql, out DynamicParameters parameters)
        //{
        //    throw new NotImplementedException();
        //}

        public override void EditData(T_MGMT_LED_REG_OLD_NO Value)
        {
            throw new NotImplementedException();
        }

        public override List<T_MGMT_LED_REG_OLD_NO> GetSearchResult(T_MGMT_LED_REG_OLD_NO condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_MGMT_LED_REG_OLD_NO value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(T_MGMT_LED_REG_OLD_NO value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_MGMT_LED_REG_OLD_NO value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_MGMT_LED_REG_OLD_NO.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_REG_OLD_NO.REG_NO) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_MGMT_LED_REG_OLD_NO.REG_NO), value.REG_NO);
            parameters.Add(nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO_SEQ), OrgConvert.ToNullableInt32(value.REG_OLD_NO_SEQ));
            parameters.Add(nameof(T_MGMT_LED_REG_OLD_NO.REG_OLD_NO), value.REG_OLD_NO);

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_MGMT_LED_REG_OLD_NO value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.REG_OLD_NO) + " = :" + nameof(value.REG_OLD_NO));
            builder.AppendLine("where ");
            builder.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(value.REG_NO), value.REG_NO);
            parameters.Add(nameof(value.REG_OLD_NO_SEQ), OrgConvert.ToNullableInt32(value.REG_OLD_NO_SEQ));
            parameters.Add(nameof(value.REG_OLD_NO), value.REG_OLD_NO);

            sql = builder.ToString();
        }
    }
}