﻿using COE.Common;
using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Threading.Tasks;
using ZyCoaG.Util;
using ZyCoaG.Classes.util;
using ChemMgmt.Classes;
using Dapper;
using ZyCoaG;

namespace ChemMgmt.Nikon.Ledger
{

    public class LedgerHistoryRegulationDataInfo : DataInfoBase<T_LEDGER_HISTORY_REGULATION>, IDisposable
    {
        protected override string TableName { get; } = nameof(T_LEDGER_HISTORY_REGULATION);


        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerAccesmentRegSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerAccesmentRegSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.LEDGER_HISTORY_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID = :LEDGER_HISTORY_ID");
                parameters.Add(nameof(condition.LEDGER_HISTORY_ID), condition.LEDGER_HISTORY_ID);
            }

            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LEDGER_HISTORY_REGULATION.LEDGER_FLOW_ID = :LEDGER_FLOW_ID");
                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
            }
            sql.AppendLine(")");
            return sql.ToString();
        }

        public override List<T_LEDGER_HISTORY_REGULATION> GetSearchResult(T_LEDGER_HISTORY_REGULATION condition)
        {
            throw new NotImplementedException();
        }

        public List<LedgerRegulationModel> GetSearchResult(LedgerRegulationSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_FLOW_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LEDGER_HISTORY_REGULATION.LED_REGULATION_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LEDGER_HISTORY_REGULATION.REG_TYPE_ID));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            StartTransaction();
            List<LedgerRegulationModel> records = DbUtil.Select<LedgerRegulationModel>(sql.ToString(), parameters);

            var dataArray = new List<LedgerRegulationModel>();

            foreach (var record in records)
            {
                var model = new LedgerRegulationModel();
                model.LEDGER_HISTORY_ID = OrgConvert.ToNullableInt32(record.LEDGER_HISTORY_ID);
                model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record.LEDGER_FLOW_ID);
                model.LED_REGULATION_ID = OrgConvert.ToNullableInt32(record.LED_REGULATION_ID);
                model.REG_TYPE_ID = Convert.ToInt32(record.REG_TYPE_ID);
                model.REG_DATE = OrgConvert.ToNullableDateTime(record.REG_DATE);
                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.LEDGER_HISTORY_ID).ThenBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.LED_REGULATION_ID);

            return searchQuery.ToList();
        }

        protected override void InitializeDictionary()
        {
        }

        public override void EditData(T_LEDGER_HISTORY_REGULATION Value) { }

        protected override void CreateInsertText(T_LEDGER_HISTORY_REGULATION value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.LED_REGULATION_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.REG_TYPE_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  @" + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_HISTORY_REGULATION.LED_REGULATION_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_HISTORY_REGULATION.REG_TYPE_ID) + ",");
            builder.AppendLine("  @" + nameof(T_LEDGER_HISTORY_REGULATION.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID), OrgConvert.ToNullableInt32(value.LEDGER_HISTORY_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.LED_REGULATION_ID), OrgConvert.ToNullableInt32(value.LED_REGULATION_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.REG_USER_CD), Convert.ToString(value.REG_USER_CD));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LEDGER_HISTORY_REGULATION value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }
                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        protected override void CreateInsertText(T_LEDGER_HISTORY_REGULATION value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.LED_REGULATION_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.REG_TYPE_ID) + ",");
            builder.AppendLine("  " + nameof(T_LEDGER_HISTORY_REGULATION.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY_REGULATION.LED_REGULATION_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY_REGULATION.REG_TYPE_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LEDGER_HISTORY_REGULATION.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_HISTORY_ID), OrgConvert.ToNullableInt32(value.LEDGER_HISTORY_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.LED_REGULATION_ID), OrgConvert.ToNullableInt32(value.LED_REGULATION_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
            parameters.Add(nameof(T_LEDGER_HISTORY_REGULATION.REG_USER_CD), Convert.ToString(value.REG_USER_CD));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LEDGER_HISTORY_REGULATION value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}
