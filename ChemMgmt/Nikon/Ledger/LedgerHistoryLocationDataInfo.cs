﻿using ChemMgmt.Classes;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerHistoryLocationDataInfo : DataInfoBase<T_LED_HISTORY_LOC>, IDisposable
    {
        private IDictionary<int?, string> locationDic { get; set; }

        protected override string TableName { get; } = nameof(T_LED_HISTORY_LOC);

        //protected override void CreateInsertText(T_LED_HISTORY_LOC value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LOC_SEQ) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LOC_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.AMOUNT) + ",");
        //    builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.UNITSIZE_ID));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LOC_SEQ) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LOC_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.AMOUNT) + ",");
        //    builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.UNITSIZE_ID));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID), OrgConvert.ToNullableInt32(value.LEDGER_HISTORY_ID));
        //    parameters.Add(nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
        //    parameters.Add(nameof(T_LED_HISTORY_LOC.LOC_SEQ), OrgConvert.ToNullableInt32(value.LOC_SEQ));
        //    parameters.Add(nameof(T_LED_HISTORY_LOC.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
        //    parameters.Add(nameof(T_LED_HISTORY_LOC.AMOUNT), value.AMOUNT);
        //    parameters.Add(nameof(T_LED_HISTORY_LOC.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));

        //    sql = builder.ToString();
        //}
        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerLocationSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);
            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerLocationSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.LEDGER_HISTORY_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LED_HISTORY_LOC.LEDGER_HISTORY_ID = @LEDGER_HISTORY_ID");
                parameters.Add(nameof(condition.LEDGER_HISTORY_ID), condition.LEDGER_HISTORY_ID);
            }

            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_LED_HISTORY_LOC.LEDGER_FLOW_ID = @LEDGER_FLOW_ID");
                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
            }

            sql.AppendLine(")");

            return sql.ToString();
        }

        public IQueryable<LedgerLocationModel> GetSearchResult(LedgerLocationSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_LOC.LOC_SEQ) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_LOC.LOC_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_LOC.AMOUNT) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_LOC.UNITSIZE_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_LOC.REG_DATE));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<T_LED_HISTORY_LOC> records = DbUtil.Select<T_LED_HISTORY_LOC>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<LedgerLocationModel>();

            foreach (DataRow record in dt.Rows)
            {
                var model = new LedgerLocationModel();
                model.LEDGER_HISTORY_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_HISTORY_ID)]);
                model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)]);
                model.LOC_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.LOC_SEQ)]);
                model.LOC_ID = OrgConvert.ToNullableInt32(record[nameof(model.LOC_ID)]);
                model.LOC_NAME = locationDic.GetValueToString(model.LOC_ID);
                model.AMOUNT = record[nameof(model.AMOUNT)].ToString();
                model.UNITSIZE_ID = OrgConvert.ToNullableInt32(record[nameof(model.UNITSIZE_ID)]);
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
                model.FLG_LOC_HAZARD = true;
                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.LEDGER_HISTORY_ID)
                .ThenBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.LOC_SEQ);

            return searchQuery;
        }


        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }
        protected override void InitializeDictionary()
        {
            locationDic = new LocationMasterInfo().GetDictionary();
        }

        public override void EditData(T_LED_HISTORY_LOC Value) { }

        protected override void CreateInsertText(T_LED_HISTORY_LOC value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LOC_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.AMOUNT) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.UNITSIZE_ID));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LOC_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LOC_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.AMOUNT) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.UNITSIZE_ID));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID), OrgConvert.ToNullableInt32(value.LEDGER_HISTORY_ID));
            parameters.Add(nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LED_HISTORY_LOC.LOC_SEQ), OrgConvert.ToNullableInt32(value.LOC_SEQ));
            parameters.Add(nameof(T_LED_HISTORY_LOC.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
            parameters.Add(nameof(T_LED_HISTORY_LOC.AMOUNT), value.AMOUNT);
            parameters.Add(nameof(T_LED_HISTORY_LOC.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));

            sql = builder.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    locationDic = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        protected override void CreateUpdateText(T_LED_HISTORY_LOC value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        public override List<T_LED_HISTORY_LOC> GetSearchResult(T_LED_HISTORY_LOC condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_LED_HISTORY_LOC value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.LOC_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.AMOUNT) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_LOC.UNITSIZE_ID));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LOC_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.LOC_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.AMOUNT) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_LOC.UNITSIZE_ID));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LED_HISTORY_LOC.LEDGER_HISTORY_ID), OrgConvert.ToNullableInt32(value.LEDGER_HISTORY_ID));
            parameters.Add(nameof(T_LED_HISTORY_LOC.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LED_HISTORY_LOC.LOC_SEQ), OrgConvert.ToNullableInt32(value.LOC_SEQ));
            parameters.Add(nameof(T_LED_HISTORY_LOC.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
            parameters.Add(nameof(T_LED_HISTORY_LOC.AMOUNT), value.AMOUNT);
            parameters.Add(nameof(T_LED_HISTORY_LOC.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LED_HISTORY_LOC value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}