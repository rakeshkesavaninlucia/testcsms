﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon.Models;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerHistoryAccesmentRegDataInfo : DataInfoBase<T_LED_HISTORY_ACCESMENT_REG>, IDisposable
    {
        protected override string TableName { get; } = nameof(T_LED_HISTORY_ACCESMENT_REG);
        
        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerAccesmentRegSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine(" from ");
            sql.AppendLine(TableName);
            sql.AppendLine(", V_REGULATION_ALL ");

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerAccesmentRegSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");
            sql.AppendLine(" and ");
            sql.AppendLine(TableName + ".REG_TYPE_ID= V_REGULATION_ALL.REG_TYPE_ID ");

            if (condition.LEDGER_HISTORY_ID != null)
            {
                sql.AppendLine(" and ");
                sql.AppendLine(TableName + ".LEDGER_HISTORY_ID = @LEDGER_HISTORY_ID");
                parameters.Add(nameof(condition.LEDGER_HISTORY_ID), condition.LEDGER_HISTORY_ID);
            }

            if (condition.LEDGER_FLOW_ID != null)
            {
                sql.AppendLine(" and ");
                sql.AppendLine(TableName + ".LEDGER_FLOW_ID = @LEDGER_FLOW_ID");
                parameters.Add(nameof(condition.LEDGER_FLOW_ID), condition.LEDGER_FLOW_ID);
            }

            if (condition.USAGE_LOC_SEQ != null)
            {
                sql.AppendLine(" and ");
                sql.AppendLine(TableName + ".USAGE_LOC_SEQ = @USAGE_LOC_SEQ");
                parameters.Add(nameof(condition.USAGE_LOC_SEQ), condition.USAGE_LOC_SEQ);
            }

            sql.AppendLine(")");

            return sql.ToString();
        }

        public IQueryable<LedgerAccesmentRegModel> GetSearchResult(LedgerAccesmentRegSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_HISTORY_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_FLOW_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_ACCESMENT_REG.USAGE_LOC_SEQ) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_ACCESMENT_REG.LED_ACCESMENT_REG_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_TYPE_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_DATE) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_TEXT) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_TEXT_LAST) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_ICON_PATH) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(T_LED_HISTORY_ACCESMENT_REG.FIRE_SIZE) + ",");
            sql.AppendLine("V_REGULATION_ALL." + nameof(T_LED_HISTORY_ACCESMENT_REG.WORKTIME_MGMT));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<T_LED_HISTORY_ACCESMENT_REG> records = DbUtil.Select<T_LED_HISTORY_ACCESMENT_REG>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<LedgerAccesmentRegModel>();

            foreach (DataRow record in dt.Rows)
            {
                var model = new LedgerAccesmentRegModel();
                model.LEDGER_HISTORY_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_HISTORY_ID)]);
                model.LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(record[nameof(model.LEDGER_FLOW_ID)]);
                model.USAGE_LOC_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.USAGE_LOC_SEQ)]);
                model.LED_ACCESMENT_REG_ID = OrgConvert.ToNullableInt32(record[nameof(model.LED_ACCESMENT_REG_ID)]);
                model.REG_TYPE_ID = OrgConvert.ToNullableInt32(record[nameof(model.REG_TYPE_ID)]);
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
                model.REG_TEXT = Convert.ToString(record[nameof(model.REG_TEXT)]);
                model.REG_TEXT_LAST = Convert.ToString(record[nameof(model.REG_TEXT_LAST)]);
                model.REG_ICON_PATH = Convert.ToString(record[nameof(model.REG_ICON_PATH)]);
                if (record[nameof(model.FIRE_SIZE)].ToString().Length != 0)
                {
                    model.FIRE_SIZE = Convert.ToDecimal(record[nameof(model.FIRE_SIZE)]);
                }
                else
                {
                    model.FIRE_SIZE = 0;
                }
                model.WORKTIME_MGMT = OrgConvert.ToNullableInt32(record[nameof(model.WORKTIME_MGMT)]);
                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.LEDGER_HISTORY_ID).ThenBy(x => x.LEDGER_FLOW_ID).ThenBy(x => x.USAGE_LOC_SEQ);

            return searchQuery;
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override void InitializeDictionary()
        {
        }

        public override void EditData(T_LED_HISTORY_ACCESMENT_REG Value) { }

        protected override void CreateInsertText(T_LED_HISTORY_ACCESMENT_REG value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.LED_ACCESMENT_REG_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_TYPE_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.LED_ACCESMENT_REG_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_TYPE_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_HISTORY_ID), OrgConvert.ToNullableInt32(value.LEDGER_HISTORY_ID));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.LED_ACCESMENT_REG_ID), OrgConvert.ToNullableInt32(value.LED_ACCESMENT_REG_ID));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.REG_USER_CD), value.REG_USER_CD);

            sql = builder.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        protected override void CreateUpdateText(T_LED_HISTORY_ACCESMENT_REG value, out string sql, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        public override List<T_LED_HISTORY_ACCESMENT_REG> GetSearchResult(T_LED_HISTORY_ACCESMENT_REG condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_LED_HISTORY_ACCESMENT_REG value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.LED_ACCESMENT_REG_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_TYPE_ID) + ",");
            builder.AppendLine("  " + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_USER_CD));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_HISTORY_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_FLOW_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.USAGE_LOC_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.LED_ACCESMENT_REG_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_TYPE_ID) + ",");
            builder.AppendLine("  :" + nameof(T_LED_HISTORY_ACCESMENT_REG.REG_USER_CD));
            builder.AppendLine(" )");
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_HISTORY_ID), OrgConvert.ToNullableInt32(value.LEDGER_HISTORY_ID));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.LEDGER_FLOW_ID), OrgConvert.ToNullableInt32(value.LEDGER_FLOW_ID));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.USAGE_LOC_SEQ), OrgConvert.ToNullableInt32(value.USAGE_LOC_SEQ));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.LED_ACCESMENT_REG_ID), OrgConvert.ToNullableInt32(value.LED_ACCESMENT_REG_ID));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.REG_TYPE_ID), OrgConvert.ToNullableInt32(value.REG_TYPE_ID));
            parameters.Add(nameof(T_LED_HISTORY_ACCESMENT_REG.REG_USER_CD), value.REG_USER_CD);

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_LED_HISTORY_ACCESMENT_REG value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}