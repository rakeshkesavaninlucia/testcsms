﻿using ChemMgmt.Classes;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerLocationDataInfo : DataInfoBase<T_MGMT_LED_LOC>, IDisposable
    {
        private IDictionary<int?, string> locationDic { get; set; }

        protected override string TableName { get; } = nameof(T_MGMT_LED_LOC);

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (LedgerLocationSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            var ledgerSearchTargetName = string.Empty;
            sql.AppendLine("from");
            sql.AppendLine(TableName);
            return sql.ToString();
        }

        //protected override void CreateInsertText(T_MGMT_LED_LOC value, out string sql, out Hashtable parameters)
        //{
        //    var builder = new StringBuilder();
        //    parameters = new Hashtable();

        //    builder.AppendLine("insert into ");
        //    builder.AppendLine(" " + TableName);
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.REG_NO) + ",");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.LOC_SEQ) + ",");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.LOC_ID) + ",");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.AMOUNT) + ",");
        //    builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.UNITSIZE_ID));
        //    builder.AppendLine(" )");
        //    builder.AppendLine("values ");
        //    builder.AppendLine(" (");
        //    builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.REG_NO) + ",");
        //    builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.LOC_SEQ) + ",");
        //    builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.LOC_ID) + ",");
        //    builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.AMOUNT) + ",");
        //    builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.UNITSIZE_ID));
        //    builder.AppendLine(" )");
        //    parameters.Add(nameof(value.REG_NO), value.REG_NO.ToString());
        //    parameters.Add(nameof(T_MGMT_LED_LOC.LOC_SEQ), OrgConvert.ToNullableInt32(value.LOC_SEQ));
        //    parameters.Add(nameof(T_MGMT_LED_LOC.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
        //    parameters.Add(nameof(T_MGMT_LED_LOC.AMOUNT), value.AMOUNT);
        //    parameters.Add(nameof(T_MGMT_LED_LOC.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));

        //    sql = builder.ToString();
        //}

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (LedgerLocationSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where");
            sql.AppendLine("(");
            sql.AppendLine(" 1 = 1");

            if (condition.REG_NO != null)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" T_MGMT_LED_LOC.REG_NO = @REG_NO");
                parameters.Add(nameof(condition.REG_NO), condition.REG_NO);
            }

            sql.AppendLine(")");

            return sql.ToString();
        }

        public IQueryable<LedgerLocationModel> GetSearchResult(LedgerLocationSearchModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            if (Common.DatabaseType == DatabaseType.SqlServer && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" top(" + condition.MaxSearchResult + ")");
            }
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_LOC.REG_NO) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_LOC.LOC_SEQ) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_LOC.LOC_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_LOC.AMOUNT) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_LOC.UNITSIZE_ID) + ",");
            sql.AppendLine(TableName + "." + nameof(T_MGMT_LED_LOC.REG_DATE));
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));
            if (Common.DatabaseType == DatabaseType.Oracle && condition.MaxSearchResult.HasValue)
            {
                sql.AppendLine(" and");
                sql.AppendLine(" rownum <= " + condition.MaxSearchResult);
            }

            //StartTransaction();
            List<T_MGMT_LED_LOC> records = DbUtil.Select<T_MGMT_LED_LOC>(sql.ToString(), parameters);
            DataTable dt = ConvertToDataTable(records);
            var dataArray = new List<LedgerLocationModel>();

            foreach (DataRow record in dt.Rows)
            {
                var model = new LedgerLocationModel();
                model.REG_NO = record[nameof(model.REG_NO)].ToString();
                model.LOC_SEQ = OrgConvert.ToNullableInt32(record[nameof(model.LOC_SEQ)]);
                model.LOC_ID = OrgConvert.ToNullableInt32(record[nameof(model.LOC_ID)]);
                model.LOC_NAME = locationDic.GetValueToString(model.LOC_ID);
                model.AMOUNT = record[nameof(model.AMOUNT)].ToString();
                model.UNITSIZE_ID = OrgConvert.ToNullableInt32(record[nameof(model.UNITSIZE_ID)]);
                model.REG_DATE = OrgConvert.ToNullableDateTime(record[nameof(model.REG_DATE)]);
                model.FLG_LOC_HAZARD = true;
                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.REG_NO).ThenBy(x => x.LOC_SEQ);

            return searchQuery;
        }

        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override void InitializeDictionary()
        {
            locationDic = new LocationMasterInfo().GetDictionary();
        }

        public override void EditData(T_MGMT_LED_LOC Value) { }

        protected override void CreateInsertText(T_MGMT_LED_LOC value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.LOC_ID) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.AMOUNT) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.UNITSIZE_ID));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.REG_NO) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.LOC_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.LOC_ID) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.AMOUNT) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.UNITSIZE_ID));
            builder.AppendLine(" )");
            parameters.Add(nameof(value.REG_NO), value.REG_NO.ToString());
            parameters.Add(nameof(T_MGMT_LED_LOC.LOC_SEQ), OrgConvert.ToNullableInt32(value.LOC_SEQ));
            parameters.Add(nameof(T_MGMT_LED_LOC.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
            parameters.Add(nameof(T_MGMT_LED_LOC.AMOUNT), value.AMOUNT);
            parameters.Add(nameof(T_MGMT_LED_LOC.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_MGMT_LED_LOC value, out string sql, out DynamicParameters parameters)
        {
            var builder = new StringBuilder();
            parameters = new DynamicParameters();

            builder.AppendLine("update ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine("set ");
            builder.AppendLine(" " + nameof(value.LOC_SEQ) + " = :" + nameof(value.LOC_SEQ) + ",");
            builder.AppendLine(" " + nameof(value.LOC_ID) + " = :" + nameof(value.LOC_ID) + ",");
            builder.AppendLine(" " + nameof(value.AMOUNT) + " = :" + nameof(value.AMOUNT) + ",");
            builder.AppendLine(" " + nameof(value.UNITSIZE_ID) + " = :" + nameof(value.UNITSIZE_ID));
            builder.AppendLine("where ");
            builder.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(value.REG_NO), value.REG_NO.ToString());
            parameters.Add(nameof(value.LOC_SEQ), OrgConvert.ToNullableInt32(value.LOC_SEQ));
            parameters.Add(nameof(value.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
            parameters.Add(nameof(value.AMOUNT), value.AMOUNT);
            parameters.Add(nameof(value.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));

            sql = builder.ToString();
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    locationDic = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                disposedValue = true;
            }
        }

        public override List<T_MGMT_LED_LOC> GetSearchResult(T_MGMT_LED_LOC condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_MGMT_LED_LOC value, out string sql, out Hashtable parameters)
        {
            var builder = new StringBuilder();
            parameters = new Hashtable();

            builder.AppendLine("insert into ");
            builder.AppendLine(" " + TableName);
            builder.AppendLine(" (");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.REG_NO) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.LOC_SEQ) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.LOC_ID) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.AMOUNT) + ",");
            builder.AppendLine("  " + nameof(T_MGMT_LED_LOC.UNITSIZE_ID));
            builder.AppendLine(" )");
            builder.AppendLine("values ");
            builder.AppendLine(" (");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.REG_NO) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.LOC_SEQ) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.LOC_ID) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.AMOUNT) + ",");
            builder.AppendLine("  :" + nameof(T_MGMT_LED_LOC.UNITSIZE_ID));
            builder.AppendLine(" )");
            parameters.Add(nameof(value.REG_NO), value.REG_NO.ToString());
            parameters.Add(nameof(T_MGMT_LED_LOC.LOC_SEQ), OrgConvert.ToNullableInt32(value.LOC_SEQ));
            parameters.Add(nameof(T_MGMT_LED_LOC.LOC_ID), OrgConvert.ToNullableInt32(value.LOC_ID));
            parameters.Add(nameof(T_MGMT_LED_LOC.AMOUNT), value.AMOUNT);
            parameters.Add(nameof(T_MGMT_LED_LOC.UNITSIZE_ID), OrgConvert.ToNullableInt32(value.UNITSIZE_ID));

            sql = builder.ToString();
        }

        protected override void CreateUpdateText(T_MGMT_LED_LOC value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}