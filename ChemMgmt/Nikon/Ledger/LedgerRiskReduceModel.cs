﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon.Ledger
{
    public class LedgerRiskReduceModel : LedgerRiskReduceColumn
    {
        public int _Mode { get; set; }

        public Mode Mode
        {
            get
            {
                Mode resultMode;
                if (Mode.TryParse(_Mode.ToString(), out resultMode)) return resultMode;
                return default(Mode);
            }
            set
            {
                _Mode = (int)value;
            }
        }

        public LedgerRiskReduceModel() { }

        /// <summary>
        /// リスク低減策
        /// </summary>
        public string RISK_REDUCE_POLICY { get; set; }

        public bool IsChecked { get; set; }
    }

    public class LedgerRiskReduceSearchModel : LedgerRiskReduceModel
    {
        public int? MaxSearchResult { get; set; }

        public LedgerRiskReduceSearchModel()
        {
            SearchRiskReduces = new List<LedgerRiskReduceModel>();
        }

        public IEnumerable<LedgerRiskReduceModel> SearchRiskReduces;

        /// <summary>
        /// 項目が読取のみか
        /// </summary>
        public bool IsReadOnly
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                        return false;
                    default:
                        return true;
                }
            }
        }

        public bool IsButtonVisibleOrEnabled
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Edit:
                    case Mode.Copy:
                        return true;
                    default:
                        return false;
                }
            }
        }
    }
}