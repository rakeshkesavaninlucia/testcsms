﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon.History;
using ChemMgmt.Nikon.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;
using ZyCoaG.Nikon;

namespace ChemMgmt.Nikon.Ledger
{
    /// <summary>
    /// 申請区分を定義します。
    /// </summary>
    public enum AppliClass
    {
        /// <summary>
        /// 新規導入
        /// </summary>
        New = 1,
        /// <summary>
        /// 変更
        /// </summary>
        Edit = 2,
        /// <summary>
        /// 廃止
        /// </summary>
        Stopped = 3
    }

    /// <summary>
    /// 管理台帳テーブルの種類を定義します。
    /// </summary>
    public enum TableType
    {
        /// <summary>
        /// ワーク
        /// </summary>
        Work,
        /// <summary>
        /// 履歴
        /// </summary>
        History,
        /// <summary>
        /// 管理台帳
        /// </summary>
        Ledger
    }

    /// <summary>
    /// 検索対象とする案件の種別を定義します。
    /// </summary>
    public enum FlowSearchTarget
    {
        /// <summary>
        /// 処理待ち案件
        /// </summary>
        Await,
        /// <summary>
        /// フロー中案件
        /// </summary>
        Processing
    }

    /// <summary>
    /// 管理台帳の状態を定義します。
    /// </summary>
    public enum LedgerStatusConditionType
    {
        /// <summary>
        /// 使用中
        /// </summary>
        Using,
        /// <summary>
        /// 申請中
        /// </summary>
        Applying,
        /// <summary>
        /// 廃止済み
        /// </summary>
        Abolished,
        /// <summary>
        /// 全て
        /// </summary>
        All
    }

    /// <summary>
    /// 管理台帳の状態を取得するクラスです。
    /// </summary>
    public class LedgerStatusListMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        /// <summary>
        /// 選択項目に空白を追加するか
        /// </summary>
        protected override bool IsAddSpace { get; set; } = false;

        /// <summary>
        /// 全状態を取得します。
        /// </summary>
        /// <returns>全使用状態を<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var searchCondition = new List<CommonDataModel<int?>>();
            searchCondition.Add(new CommonDataModel<int?>((int)LedgerStatusConditionType.Using, "使用中", 0));
            searchCondition.Add(new CommonDataModel<int?>((int)LedgerStatusConditionType.Applying, "申請中", 1));
            searchCondition.Add(new CommonDataModel<int?>((int)LedgerStatusConditionType.Abolished, "廃止済み", 2));
            searchCondition.Add(new CommonDataModel<int?>((int)LedgerStatusConditionType.All, "全て", 3));
            return searchCondition;
        }
    }

    public class LedgerModel : LedgerColumn
    {

        public int _Mode { get; set; }

        public Mode Mode
        {
            get
            {
                Mode resultMode;
                if (Mode.TryParse(_Mode.ToString(), out resultMode)) return resultMode;
                return default(Mode);
            }
            set
            {
                _Mode = (int)value;
            }
        }

        public int _TableType { get; set; }

        public TableType TableType
        {
            get
            {
                TableType resultTableType;
                if (TableType.TryParse(_TableType.ToString(), out resultTableType)) return resultTableType;
                return default(TableType);
            }
            set
            {
                _TableType = (int)value;
            }
        }

        public int _Status { get; set; }

        public LedgerStatusConditionType Status
        {
            get
            {
                LedgerStatusConditionType resultLedgerStatus;
                if (TableType.TryParse(_TableType.ToString(), out resultLedgerStatus)) return resultLedgerStatus;
                return default(LedgerStatusConditionType);
            }
            set
            {
                _Status = (int)value;
            }
        }

        /// <summary>
        /// その他の変更・廃止の事由のIDを定義します。
        /// </summary>
        public static int OTHER_REASON_ID { get; } = 6;

        public bool CHECK { get; set; }

        public string APPLI_NO { get { return string.Format("{0:D10}", LEDGER_FLOW_ID.HasValue ? LEDGER_FLOW_ID : RECENT_LEDGER_FLOW_ID); } }

        public string GROUP_NAME { get; set; }

        public string APPLICANT { get; set; }

        public string NID { get; set; }

        public string APPLI_CLASS_NAME
        {
            get
            {
                switch (APPLI_CLASS)
                {
                    case (int)AppliClass.New:
                        return "新規導入";
                    case (int)AppliClass.Edit:
                        return "変更";
                    case (int)AppliClass.Stopped:
                        return "廃止";
                    default:
                        return "";
                }
            }
        }

        public string REASON { get; set; }

        [Display(Name = "化学物質名")]
        public string CHEM_NAME { get; set; }

        [Display(Name = "商品名")]
        public string PRODUCT_NAME { get; set; }

        public int? CHEM_PRODUCT_COUNT { get; set; }

        public string CHEM_PRODUCT_COUNT_STR { get { return CHEM_PRODUCT_COUNT.HasValue ? string.Format("{0}件", CHEM_PRODUCT_COUNT) : null; } }

        public string CAS_NO { get; set; }

        public string REG_TYPE_IDS { get; set; }

        public int? FIGURE { get; set; }

        public string FIGURE_NAME { get; set; }

        public string CHEM_CAT { get; set; }

        public bool HasUnregisteredChem { get; set; }

        public string REG_TEXT { get; set; }

        public int? REG_TYPE_ID { get; set; }

        public IEnumerable<int> REG_TYPE_ID_COLLECTION
        {
            get
            {
                if (REG_TYPE_ID == null) yield break;
                foreach (var s in ARRAY_REG_TYPE_IDS?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
                yield return Convert.ToInt32("1");
            }
        }

        public string ARRAY_REG_TYPE_IDS { get; set; }

        public string REQUIRED_CHEM { get; set; }

        public string HAZMAT_TYPE { get; set; }

        public string REG_OLD_NO { get; set; }

        public int? REG_OLD_NO_COUNT { get; set; }

        public string REG_OLD_NO_COUNT_STR { get { return REG_OLD_NO_COUNT.HasValue ? string.Format("{0}件", REG_OLD_NO_COUNT) : null; } }

        public string APPROVER1 { get; set; }

        public string APPROVER2 { get; set; }

        public string APPROVAL_TARGET { get; set; }

        public string BEFORE_APPROVAL_TARGET { get; set; }

        public string APPROVAL_USER_CD { get; set; }

        public string BEFORE_APPROVAL_USER_CD { get; set; }

        public string BEFORE_APPROVAL_USER_NAME { get; set; }

        public string REAL { get; set; }
        public string APPROVAL_USER_NAME { get; set; }

        public string SUBSITUTE_USER_CD { get; set; }

        public string FLOW_HIERARCHY { get; set; }

        #region "保管場所明細項目"

        public int? LOC_ID { get; set; }

        public string LOC_NAME { get; set; }

        public int? LOC_COUNT { get; set; }

        public string LOC_COUNT_STR { get { return LOC_COUNT.HasValue ? string.Format("{0}箇所", LOC_COUNT) : null; } }

        public string AMOUNT { get; set; }

        public int? UNITSIZE_ID { get; set; }

        public string UNITSIZE
        {
            get
            {
                return new UnitSizeMasterInfo().GetSelectList().FirstOrDefault(x => x.Id == UNITSIZE_ID)?.Name;
            }
        }

        public string AMOUNTWITHUNITSIZE
        {
            get
            {
                return AMOUNT + UNITSIZE;
            }
        }

        #endregion

        #region "使用場所明細項目"

        public int? USAGE_LOC_ID { get; set; }

        public string USAGE_LOC_NAME { get; set; }

        public int? USAGE_LOC_COUNT { get; set; }

        public string USAGE_LOC_COUNT_STR { get { return USAGE_LOC_COUNT.HasValue ? string.Format("{0}箇所", USAGE_LOC_COUNT) : null; } }

        public string USAGE_LOC_AMOUNT { get; set; }

        public int? USAGE_LOC_UNITSIZE_ID { get; set; }

        public string USAGE_LOC_UNITSIZE
        {
            get
            {
                return new UnitSizeMasterInfo().GetSelectList().FirstOrDefault(x => x.Id == USAGE_LOC_UNITSIZE_ID)?.Name;
            }
        }

        public string USAGE_LOC_AMOUNTWITHUNITSIZE
        {
            get
            {
                return USAGE_LOC_AMOUNT + USAGE_LOC_UNITSIZE;
            }
        }

        public int? ISHA_USAGE { get; set; }

        public string ISHA_USAGE_TEXT { get; set; }

        public string ISHA_OTHER_USAGE { get; set; }

        [Display(Name = "化学物質取扱責任者")]
        public string CHEM_OPERATION_CHIEF { get; set; }

        public string CHEM_OPERATION_CHIEF_NAME { get; set; }

        public int? SUB_CONTAINER { get; set; }

        public string SUB_CONTAINER_TEXT { get; set; }

        public string ENTRY_DEVICE { get; set; }

        public int? PROTECTOR_ID { get; set; }

        public string PROTECTOR_NAME { get; set; }

        public int? PROTECTOR_COUNT { get; set; }

        public string OTHER_PROTECTOR { get; set; }

        public int? REGULATION_COUNT { get; set; }

        public int? LED_RISK_REDUCE_TYPE { get; set; }

        public bool LED_RISK_REDUCE_FLAG { get; set; }

        public int? RISK_REDUCE_COUNT { get; set; }

        public decimal? MEASURES_BEFORE_SCORE { get; set; }

        public int? MEASURES_BEFORE_RANK { get; set; }

        public decimal? MEASURES_AFTER_SCORE { get; set; }

        public int? MEASURES_AFTER_RANK { get; set; }

        public string MEASURES_BEFORE_RANK_TEXT { get; set; }

        public string MEASURES_AFTER_RANK_TEXT { get; set; }

        public int? REG_TRANSACTION_VOLUME { get; set; }

        public string REG_TRANSACTION_VOLUME_TEXT { get; set; }

        public int? REG_WORK_FREQUENCY { get; set; }

        public string REG_WORK_FREQUENCY_TEXT { get; set; }

        public int? REG_DISASTER_POSSIBILITY { get; set; }

        public string REG_DISASTER_POSSIBILITY_TEXT { get; set; }
        #endregion

        public string STATUS_TEXT { get; set; }

        public int? BEFORE_WORKFLOW_RESULT { get; set; }

        public string BEFORE_WORKFLOW_RESULT_TEXT
        {
            get
            {
                switch (BEFORE_WORKFLOW_RESULT)
                {
                    case (int)WorkflowResult.Application:
                        return "承認待ち";
                    case (int)WorkflowResult.Approval:
                        return "承認";
                    case (int)WorkflowResult.Remand:
                        return "差戻";
                    case (int)WorkflowResult.Regained:
                        return "取戻";
                    default:
                        return "未申請";
                }
            }
        }

        public DateTime? ApplicationDate { get; set; }

        /// <summary>
        /// 削除されているか
        /// </summary>
        //public bool IsShowDeleted
        //{
        //    get
        //    {
        //        if (DEL_FLAG == (int)ZyCoaG.DEL_FLAG.Deleted) return true;
        //        return false;
        //    }
        //}

        /// <summary>
        /// 変更・廃止の事由が読取のみか
        /// </summary>
        public bool IsChangeReasonEnabled
        {
            get
            {
                switch (Mode)
                {
                    case Mode.Edit:
                    case Mode.Abolition:
                        return true;
                    default:
                        return false;
                }
            }
        }

        /// <summary>
        /// 化学物質コードが読取のみか
        /// </summary>
        public bool IsChemCodeReadOnly
        {
            get
            {
                switch (Mode)
                {
                    case Mode.New:
                    case Mode.Copy:
                        return false;
                    default:
                        return true;
                }
            }
        }

        public static explicit operator T_MGMT_LEDGER(LedgerModel m)
        {
            return new T_MGMT_LEDGER()
            {
                REG_NO = m.REG_NO,
                RECENT_LEDGER_FLOW_ID = m.RECENT_LEDGER_FLOW_ID,
                APPLI_CLASS = m.APPLI_CLASS,
                REASON_ID = m.REASON_ID,
                OTHER_REASON = m.OTHER_REASON,
                CHEM_CD = m.CHEM_CD,
                MAKER_NAME = m.MAKER_NAME,
                SDS = m.SDS,
                SDS_MIMETYPE = m.SDS_MIMETYPE,
                SDS_FILENAME = m.SDS_FILENAME,
                SDS_URL = m.SDS_URL,
                UNREGISTERED_CHEM = m.UNREGISTERED_CHEM,
                UNREGISTERED_CHEM_MIMETYPE = m.UNREGISTERED_CHEM_MIMETYPE,
                UNREGISTERED_CHEM_FILENAME = m.UNREGISTERED_CHEM_FILENAME,
                MEMO = m.MEMO,
                REG_USER_CD = m.REG_USER_CD,
                REG_DATE = m.REG_DATE,
                UPD_DATE = m.UPD_DATE,
                DEL_DATE = m.DEL_DATE,
                DEL_FLAG = m.DEL_FLAG,
                OTHER_REGULATION = m.OTHER_REGULATION,
                GROUP_CD = m.GROUP_CD,
                DEL_USER_CD = m.DEL_USER_CD,
                UPD_USER_CD = m.UPD_USER_CD,
            };
        }

        public static explicit operator T_LEDGER_WORK(LedgerModel m)
        {
            return new T_LEDGER_WORK()
            {
                LEDGER_FLOW_ID = m.LEDGER_FLOW_ID,
                FLOW_CD = m.FLOW_CD,
                HIERARCHY = m.HIERARCHY,
                REG_NO = m.REG_NO,
                APPLI_CLASS = m.APPLI_CLASS,
                REASON_ID = m.REASON_ID,
                OTHER_REASON = m.OTHER_REASON,
                CHEM_CD = m.CHEM_CD,
                MAKER_NAME = m.MAKER_NAME,
                SDS = m.SDS,
                SDS_MIMETYPE = m.SDS_MIMETYPE,
                SDS_FILENAME = m.SDS_FILENAME,
                SDS_URL = m.SDS_URL,
                UNREGISTERED_CHEM = m.UNREGISTERED_CHEM,
                UNREGISTERED_CHEM_MIMETYPE = m.UNREGISTERED_CHEM_MIMETYPE,
                UNREGISTERED_CHEM_FILENAME = m.UNREGISTERED_CHEM_FILENAME,
                MEMO = m.MEMO,
                TEMP_SAVED_FLAG = m.TEMP_SAVED_FLAG,
                REG_USER_CD = m.REG_USER_CD,
                REG_DATE = m.REG_DATE,
                UPD_DATE = m.UPD_DATE,
                DEL_DATE = m.DEL_DATE,
                DEL_FLAG = m.DEL_FLAG,
                OTHER_REGULATION = m.OTHER_REGULATION,
                GROUP_CD = m.GROUP_CD,
                DEL_USER_CD = m.DEL_USER_CD,
                UPD_USER_CD = m.UPD_USER_CD,
            };
        }
    }

    public class LedgerSearchModel : LedgerModel
    {
        public int? MaxSearchResult { get; set; }

        public LedgerSearchModel()
        {
            SearchLedgerList = new List<LedgerModel>();
        }

        public string ApplicationNo { get; set; }
        public string LEDGER_FLOW_ID_SELECTION { get; set; }

        public LedgerSearchModel SearchinputList { get; set; }

        public IEnumerable<int> LEDGER_FLOW_ID_COLLECTION
        {
            get
            {
                if (LEDGER_FLOW_ID_SELECTION == null) yield break;
                foreach (var s in LEDGER_FLOW_ID_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string REG_NO_SELECTION { get; set; }

        public IEnumerable<string> REG_NO_COLLECTION
        {
            get
            {
                if (REG_NO_SELECTION == null) yield break;
                foreach (var s in REG_NO_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return s;
                    }
                }
            }
        }

        public int CHEM_SEARCH_TARGET { get; set; }

        public int CHEM_NAME_CONDITION { get; set; }

        public int PRODUCT_NAME_CONDITION { get; set; }

        public int CAS_NO_CONDITION { get; set; }

        public string CAS_CONDITION { get; set; }

        public int REG_NO_SEARCH_TARGET { get; set; }

        public int REG_NO_CONDITION { get; set; }

        public int LEDGER_STATUS { get; set; } = (int)Classes.UsageStatusCondtionType.Using;

        public int FLOW_SEARCH_TARGET { get; set; }

        public string FLOW_USER_CD { get; set; }

        public bool HasExcludeOverlap { get; set; }

        public IEnumerable<LedgerModel> SearchLedgerList;

        public bool IsAdmin { get; set; } = false;

        public string GROUP_CD_SELECTION { get; set; }

        private IDictionary<string, string> groupDic { get; set; } = new GroupMasterInfo().GetDictionary();
        public string GROUP_CD_SELECTION_TEXT
        {
            get
            {
                if (GROUP_CD_SELECTION != null && groupDic.ContainsKey(GROUP_CD_SELECTION))
                {
                    return groupDic[GROUP_CD_SELECTION];
                }
                return string.Empty;
            }
        }

        [Display(Name = "管理部門")]
        public IEnumerable<string> GROUP_CD_COLLECTION
        {
            get
            {
                if (GROUP_CD_SELECTION == null) yield break;
                foreach (var s in GROUP_CD_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return s;
                    }
                }
            }
        }

        public string ManagementGroupSelectionStyle
        {
            get
            {
                return GROUP_CD_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string ManagementGroupSelectionText
        {
            get
            {
                return GROUP_CD_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string ManagementGroupClickEvent { get; set; }

        public string REG_SELECTION { get; set; }

        [Display(Name = "法規制")]
        public IEnumerable<int> REG_COLLECTION
        {
            get
            {
                if (REG_SELECTION == null) yield break;
                foreach (var s in REG_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string RegulationSelectionStyle
        {
            get
            {
                return REG_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string RegulationSelectionText
        {
            get
            {
                return REG_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string RegulationClickEvent { get; set; }

        public bool REGULATION_BASE { get; set; }

        public string ACTION_LOC_SELECTION { get; set; }

        public bool LOC_BASE { get; set; }

        [Display(Name = "保管場所")]
        public IEnumerable<int> ACTION_LOC_COLLECTION
        {
            get
            {
                if (ACTION_LOC_SELECTION == null) yield break;
                foreach (var s in ACTION_LOC_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string ActionLocationNameSelectionStyle
        {
            get
            {
                return ACTION_LOC_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string ActionLocationNameSelectionText
        {
            get
            {
                return ACTION_LOC_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string StorageLocationNameClickEvent { get; set; }

        public string USAGE_LOC_SELECTION { get; set; }

        public bool USAGE_LOC_BASE { get; set; }

        [Display(Name = "使用場所")]
        public IEnumerable<int> USAGE_LOC_COLLECTION
        {
            get
            {
                if (USAGE_LOC_SELECTION == null) yield break;
                foreach (var s in USAGE_LOC_SELECTION?.Split(SystemConst.SelectionsDelimiter.ToCharArray()))
                {
                    if (!string.IsNullOrWhiteSpace(s))
                    {
                        yield return Convert.ToInt32(s);
                    }
                }
            }
        }

        public string UsageLocationNameSelectionStyle
        {
            get
            {
                return USAGE_LOC_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetStyle : SystemConst.SelectionViewSetStyle;
            }
        }

        public string UsageLocationNameSelectionText
        {
            get
            {
                return USAGE_LOC_COLLECTION.Count() == 0 ? SystemConst.SelectionViewUnsetText : SystemConst.SelectionViewSetText;
            }
        }

        public string UsageLocationNameClickEvent { get; set; }

        /// <summary>検索ユーザーの管理者権限（0:一般 1:管理者）</summary>
        public string USER_ADMINFLG { get; set; }
        /// <summary>検索ユーザーの部署ID,上位部署含む</summary>
        public IEnumerable<string> USER_GROUP_CD { get; set; }

        public new string STATUS_TEXT
        {
            get { return ""; }
        }

        [Display(Name = "参照権限")]
        public string REFERENCE_AUTHORITY { get; set; }

        [Display(Name = "ログインユーザーコード")]
        public string LOGIN_USER_CD { get; set; }

        public int? LEDGER_FLOW_ID_EXT { get; set; }

        public string REG_NO_EXT { get; set; }
    }
}