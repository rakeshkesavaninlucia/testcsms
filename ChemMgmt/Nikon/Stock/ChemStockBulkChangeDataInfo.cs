﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ZyCoaG.Nikon
{
    /// <summary>
    /// 保管場所一括変更
    /// </summary>
    public class ChemStockBulkChangeModel : ChemStockModel
    {
        public ChemStockBulkChangeModel() { }

        /// <summary>
        /// 
        /// </summary>
        public string CONTENT_WEIGHT_VOLUME { get; set; }
    }

    public class ChemStockBulkChangekSearchModel : ChemStockBulkChangeModel
    {
        public ChemStockBulkChangekSearchModel() { }

        public string LocationSelectionText
        {
            get
            {
                return LOC_NAME;
            }
        }

        public string LocationClickEvent { get; set; }

        /// <summary>
        /// 参照権限
        /// </summary>
        public string REFERENCE_AUTHORITY { get; set; }

        /// <summary>
        /// ログインユーザー
        /// </summary>
        public string LOGIN_USER_CD { get; set; }
    }
}

namespace ZyCoaG.Nikon.Stock
{
    public class ChemStockBulkChangeDataInfo : DataInfoBase<ChemStockBulkChangeModel>
    {
        public IEnumerable<ChemStockBulkChangeModel> dataArray { get; private set; }

        public string LocationClickEvent { get; set; }

        protected override string TableName { get; } = nameof(T_STOCK);

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (ChemStockBulkChangekSearchModel)dynamicCondition;
            var sql = new StringBuilder();
            sql.AppendLine("from");
            sql.AppendLine(" T_STOCK s ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" T_ORDER o ");
            sql.AppendLine("  on s.ORDER_NO = o.ORDER_NO ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_CHEM c ");
            sql.AppendLine("  on s.CAT_CD = c.CHEM_CD ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_MAKER m ");
            sql.AppendLine("  on s.MAKER_ID = m.MAKER_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_USER u ");
            sql.AppendLine("  on o.ORDER_USER_CD = u.USER_CD ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_UNITSIZE us ");
            sql.AppendLine("  on s.UNITSIZE_ID = us.UNITSIZE_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_GROUP g ");
            sql.AppendLine("  on u.GROUP_ID = g.GROUP_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_VENDOR v ");
            sql.AppendLine("  on o.VENDOR_ID = v.VENDOR_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_KANJYO k ");
            sql.AppendLine("  on o.KANJYO_ID = k.KANJYO_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_GHSCAT gc ");
            sql.AppendLine("  on s.CAT_CD = gc.CHEM_CD ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_REGULATION r ");
            sql.AppendLine("  on s.CAT_CD = r.CHEM_CD ");
            //2019/4/26 TPE.Rin Add Start
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_FIRE cf ");
            sql.AppendLine("  on s.CAT_CD = cf.CHEM_CD ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_OFFICE co ");
            sql.AppendLine("  on s.CAT_CD = co.CHEM_CD ");
            //2019/4/26 TPE.Rin Add End
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_STATUS st ");
            sql.AppendLine("  on s.STATUS_ID = st.STATUS_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_LOCATION sl ");
            sql.AppendLine("  on s.LOC_ID = sl.LOC_ID ");

            if (condition.REFERENCE_AUTHORITY == Const.cREFERENCE_AUTHORITY_FLAG_NORMAL)
            {
                sql.AppendLine(" left outer join");
                sql.AppendLine("  T_MGMT_LEDGER ml ");
                sql.AppendLine("  on s.REG_NO = ml.REG_NO ");
            }

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (ChemStockBulkChangekSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where ");
            sql.AppendLine(" s.DEL_FLAG <> 1 ");
            sql.AppendLine(" and s.STATUS_ID <> 205 ");
            sql.AppendLine(" and s.BARCODE in (" + condition.BARCODE + ")");

            return sql.ToString();
        }

        public override List<ChemStockBulkChangeModel> GetSearchResult(ChemStockBulkChangeModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            sql.AppendLine(" s.STOCK_ID,");
            sql.AppendLine(" c.CHEM_NAME PRODUCT_NAME_JP,");
            sql.AppendLine(" m.MAKER_ID,");
            sql.AppendLine(" m.MAKER_NAME,");
            sql.AppendLine(" v.VENDOR_ID,");
            sql.AppendLine(" v.VENDOR_NAME,");
            sql.AppendLine(" s.CAT_CD,");
            if (Common.DatabaseType == DatabaseType.Oracle)
            {
                sql.AppendLine(" to_char(s.UNITSIZE) || us.UNIT_NAME DISPLAY_SIZE,");
                sql.AppendLine(" s.UNITSIZE,");
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                sql.AppendLine(" format(s.UNITSIZE,'0.#########') + us.UNIT_NAME DISPLAY_SIZE,");
                sql.AppendLine(" format(s.UNITSIZE,'0.#########') UNITSIZE,");
            }
            sql.AppendLine(" s.UNITSIZE_ID,");
            sql.AppendLine(" us.UNIT_NAME UNITSIZE_NAME,");
            sql.AppendLine(" s.GRADE,");
            if (Common.DatabaseType == DatabaseType.Oracle)
            {
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (to_char(s.CUR_GROSS_WEIGHT_G - nvl(s.BOTTLE_WEIGHT_G, 0)) || 'g') end) CONTENT_WEIGHT,");
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (to_char(s.CUR_GROSS_WEIGHT_G - nvl(s.BOTTLE_WEIGHT_G, 0))) end) CONTENT_WEIGHT_VOLUME,");
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else 'g' end) CONTENT_WEIGHT_UNITSIZE,");
                sql.AppendLine(" to_char(s.DELI_DATE, 'yyyy/MM/dd') DELI_DATE,");
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (convert(nvarchar,s.CUR_GROSS_WEIGHT_G - IsNull(s.BOTTLE_WEIGHT_G, 0)) + 'g') end) CONTENT_WEIGHT,");
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (convert(nvarchar,s.CUR_GROSS_WEIGHT_G - IsNull(s.BOTTLE_WEIGHT_G, 0))) end) CONTENT_WEIGHT_VOLUME,");
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else 'g' end) CONTENT_WEIGHT_UNITSIZE,");
                sql.AppendLine(" convert(nvarchar,s.DELI_DATE,111) DELI_DATE,");
            }
            sql.AppendLine(" s.ORDER_NO,");
            sql.AppendLine(" s.BARCODE,");
            sql.AppendLine(" s.REG_NO,");
            sql.AppendLine(" s.FIRST_LOC_ID,");
            sql.AppendLine(" gc.GHSCAT_NAME,");
            //2019/04/26 TPE.Rin.Add Start
            sql.AppendLine(" cf.FIRECAT_NAME,");
            sql.AppendLine(" co.OFFICECAT_NAME,");
            //2019/04/26 TPE.Rin.Add End
            sql.AppendLine(" r.REG_TEXT,");
            sql.AppendLine(" o.PRICE,");
            sql.AppendLine(" u.USER_NAME ORDER_USER,");
            sql.AppendLine(" o.ORDER_USER_CD OWNER,");
            sql.AppendLine(" g.GROUP_NAME,");
            sql.AppendLine(" v.VENDOR_NAME,");
            sql.AppendLine(" k.KANJYO_NAME,");
            sql.AppendLine(" st.STATUS_TEXT,");
            sql.AppendLine(" sl.LOC_NAME ");

            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));

            //StartTransaction();
            List<ChemStockBulkChangeModel> dataArray = DbUtil.Select<ChemStockBulkChangeModel>(sql.ToString(), parameters);

            //var dataArray = new List<ChemStockBulkChangeModel>();
            //foreach (DataRow record in records.Rows)
            //{
            //    var data = new ChemStockBulkChangeModel();
            //    data.STOCK_ID = Convert.ToInt32(record[nameof(ChemStockBulkChangeModel.STOCK_ID)].ToString());
            //    data.PRODUCT_NAME_JP = record[nameof(ChemStockBulkChangeModel.PRODUCT_NAME_JP)].ToString();
            //    data.MAKER_NAME = record[nameof(ChemStockBulkChangeModel.MAKER_NAME)].ToString();
            //    data.UNITSIZE = record[nameof(ChemStockBulkChangeModel.UNITSIZE)].ToString();
            //    data.UNITSIZE_NAME = record[nameof(ChemStockBulkChangeModel.UNITSIZE_NAME)].ToString();
            //    data.BARCODE = record[nameof(ChemStockBulkChangeModel.BARCODE)].ToString();
            //    data.LOC_NAME = record[nameof(ChemStockBulkChangeModel.LOC_NAME)].ToString();
            //    data.FIRST_LOC_ID = OrgConvert.ToNullableInt32(record[nameof(ChemStockBulkChangeModel.FIRST_LOC_ID)].ToString());
            //    data.CAT_CD = record[nameof(ChemStockBulkChangeModel.CAT_CD)].ToString();
            //    data.OWNER = record[nameof(ChemStockBulkChangeModel.OWNER)].ToString();
            //    data.DELI_DATE = OrgConvert.ToNullableDateTime(record[nameof(ChemStockBulkChangeModel.DELI_DATE)].ToString());
            //    data.DETAIL_WEIGHT = OrgConvert.ToNullableDecimal(record[nameof(ChemStockBulkChangeModel.CONTENT_WEIGHT_VOLUME)].ToString());
            //    data.STATUS_TEXT = record[nameof(ChemStockBulkChangeModel.STATUS_TEXT)].ToString();
            //    data.GHSCAT_NAME = record[nameof(ChemStockBulkChangeModel.GHSCAT_NAME)].ToString();
            //    data.REG_TEXT = record[nameof(ChemStockBulkChangeModel.REG_TEXT)].ToString();
            //    data.REG_NO = record[nameof(ChemStockBulkChangeModel.REG_NO)].ToString();
            //    //2019/04/26 TPE.Rin Add Start
            //    data.FIRECAT_NAME = record[nameof(ChemStockBulkChangeModel.FIRECAT_NAME)].ToString();
            //    data.OFFICECAT_NAME = record[nameof(ChemStockBulkChangeModel.OFFICECAT_NAME)].ToString();
            //    //2019/04/26 TPE.Rin Add End

            //    dataArray.Add(data);
            //}

            var searchQuery = dataArray.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.BARCODE);

            return searchQuery.ToList();
        }

        protected override void InitializeDictionary() { }

        protected override void CreateInsertText(ChemStockBulkChangeModel value, out string sql, out DynamicParameters parameters)
        {
            sql = ChemMgmt.Controllers.SQL.ACTION_HISTORY;

            parameters = new DynamicParameters();

            parameters.Add("ACTION_TYPE_ID", Const.cACTION_HISTORY_TYPE.MOVE.ToString("D"));
            parameters.Add(nameof(value.BARCODE), value.BARCODE);
            parameters.Add(nameof(T_ACTION_HISTORY.USER_CD), value.BORROWER);
            parameters.Add(nameof(T_ACTION_HISTORY.MEMO), null);
            parameters.Add(nameof(T_ACTION_HISTORY.LOC_ID), value.LOC_ID);
            parameters.Add(nameof(T_ACTION_HISTORY.LOC_NAME), value.LOC_NAME);
            parameters.Add(nameof(T_ACTION_HISTORY.USAGE_LOC_ID), null);
            parameters.Add(nameof(T_ACTION_HISTORY.USAGE_LOC_NAME), null);
            parameters.Add(nameof(T_ACTION_HISTORY.CUR_GROSS_WEIGHT_G), null);
            parameters.Add(nameof(T_ACTION_HISTORY.STOCK_ID), value.STOCK_ID);
            parameters.Add(nameof(T_ACTION_HISTORY.INTENDED), null);
            parameters.Add(nameof(T_ACTION_HISTORY.WORKING_TIMES), null);
            parameters.Add(nameof(T_ACTION_HISTORY.INTENDED_NM), null);
            parameters.Add(nameof(T_ACTION_HISTORY.INTENDED_USE_ID), null);
            // 2018/12/11 Add Start TPE Kometani
            Hashtable _params = new Hashtable();
            string sql2 = "SELECT mu.USER_NAME, mu.GROUP_CD, mg.GROUP_NAME ";
            sql2 += "from M_USER mu inner join V_GROUP mg ";
            sql2 += "on mg.GROUP_CD = mu.GROUP_CD ";
            sql2 += "where mu.USER_CD = @USER_CD";
            _params.Add("USER_CD", value.BORROWER);
            DataTable dt = DbUtil.Select(sql2, _params);
            parameters.Add(nameof(T_ACTION_HISTORY.ACTION_USER_NM), Convert.ToString(dt.Rows[0]["USER_NAME"]));
            parameters.Add(nameof(T_ACTION_HISTORY.ACTION_GROUP_CD), Convert.ToString(dt.Rows[0]["GROUP_CD"]));
            parameters.Add(nameof(T_ACTION_HISTORY.ACTION_GROUP_NAME), Convert.ToString(dt.Rows[0]["GROUP_NAME"]));
            // 2018/12/11 Add End TPE Kometani
        }

        protected override void CreateUpdateText(ChemStockBulkChangeModel value, out string sql, out DynamicParameters parameters)
        {
            var updateSql = new StringBuilder();
            parameters = new DynamicParameters();
            updateSql.AppendLine("update ");
            updateSql.AppendLine(" " + TableName + " ");
            updateSql.AppendLine("set ");
            updateSql.AppendLine(" " + nameof(value.STATUS_ID) + " = @" + nameof(value.STATUS_ID) + ", ");
            updateSql.AppendLine(" " + nameof(value.LOC_ID) + " = @" + nameof(value.LOC_ID) + ", ");
            updateSql.AppendLine(" " + nameof(value.BORROWER) + " = @" + nameof(value.BORROWER) + ", ");
            updateSql.AppendLine(" " + nameof(value.BRW_DATE) + " = getdate() ");
            updateSql.AppendLine("where ");
            updateSql.AppendLine(" " + nameof(value.BARCODE) + " = @" + nameof(value.BARCODE) + ";");

            parameters.Add(nameof(value.STATUS_ID), OrgConvert.ToNullableInt32(Const.cSTOCK_STATUS_ID.STOCK));
            parameters.Add(nameof(value.LOC_ID), value.LOC_ID);
            parameters.Add(nameof(value.BORROWER), value.BORROWER);
            parameters.Add(nameof(value.BARCODE), value.BARCODE);

            sql = updateSql.ToString();
        }

        private string SQL_ACTION_HISTORY()
        {
            return ChemMgmt.Controllers.SQL.ACTION_HISTORY;
        }

        public override void EditData(ChemStockBulkChangeModel Value)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ChemStockBulkChangeModel> getData(string barcode)
        {
            return dataArray.Where(x => x.BARCODE == barcode);
        }

        protected override void saveEntity(ChemStockBulkChangeModel value, int? rowNumber)
        {
            DynamicParameters parameters;
            string sql;
            CreateUpdateText(value, out sql, out parameters);
            var retUpdate = DbUtil.ExecuteUpdate(sql, parameters);

            CreateInsertText(value, out sql, out parameters);
            var retInsert = DbUtil.ExecuteUpdate(sql, parameters);
            if (retUpdate == 0 || retInsert == 0)
            {
                IsValid = false;
            }
        }

        protected override void CreateInsertText(ChemStockBulkChangeModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(ChemStockBulkChangeModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}