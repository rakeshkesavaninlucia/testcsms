﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Chem;
using COE.Common;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon
{
    public class ChemStockModel : T_STOCK
    {
        public ChemStockModel() { }

        public ChemStockModel(ChemModel value)
        {
            CAT_CD = value.CHEM_CD;
            PRODUCT_NAME_JP = value.CHEM_NAME;
            MAKER_ID = value.MAKER_ID;
            MAKER_NAME = value.MAKER_NAME;
            GHSCAT_NAME = value.GHSCAT_NAME;
            REG_TEXT = value.REG_TEXT;
            UNITSIZE = value.UNITSIZE;
            UNITSIZE_ID = value.UNITSIZE_ID;
            UNITSIZE_NAME = value.UNITSIZE_NAME;
            WEIGHT_MGMT = value.WEIGHT_MGMT;
            REG_NO = value.REG_NO;
            //2019/04/26 TPE.Rin Add Start
            FIRECAT_NAME = value.FIRECAT_NAME;
            OFFICECAT_NAME = value.OFFICECAT_NAME;
            //2019/04/26 TPE.Rin Add End
        }

        public void SetLocation(V_LOCATION location)
        {
            if (location.LOC_ID == default(int))
            {
                LOC_ID = null;
            }
            else
            {
                LOC_ID = location.LOC_ID;
            }
            LOC_BARCODE = location.LOC_BARCODE;
            LOC_NAME = location.LOC_NAME;
        }

        public void SetFirstLocation(V_LOCATION location)
        {
            if (location.LOC_ID == default(int))
            {
                FIRST_LOC_ID = null;
            }
            else
            {
                FIRST_LOC_ID = location.LOC_ID;
            }
            FIRST_LOC_BARCODE = location.LOC_BARCODE;
            FIRST_LOC_NAME = location.LOC_NAME;
        }

        private const int SEQUENCE_FIRST = 0;
        public IEnumerable<CommonDataModel<int?>> LOCATION_LIST { get; set; }

        public void setLocationList(IEnumerable<CommonDataModel<int?>> locationList)
        {
            this.LOCATION_LIST = locationList;
        }

        public void setFirstLocationList(IEnumerable<CommonDataModel<int?>> locationList)
        {
            this.LOCATION_LIST = locationList;
            LOC_ID = locationList.Single(x => x.Order == SEQUENCE_FIRST).Id;
        }

        public ChemStockModel Clone()
        {
            return (ChemStockModel)MemberwiseClone();
        }

        /// <summary>
        /// 行識別ID
        /// </summary>
        public string RowId { get; set; } = Guid.NewGuid().ToString();

        /// <summary>
        /// チェック
        /// </summary>
        public bool CHECK { get; set; }

        /// <summary>
        /// 登録番号
        /// </summary>
        [Display(Name = "登録番号")]
        [StringLength(11)]
        [Halfwidth]
        [Required]
        [ExcludingProhibition]
        public string REG_NO { get; set; }

        /// <summary>
        /// メーカー名
        /// </summary>
        public string MAKER_NAME { get; set; }

        /// <summary>
        /// <para>重量管理</para>
        /// <para>必須の場合は重量測定されていない場合はエラーとする。</para>
        /// <para>推奨の場合も重量測定は可能とする。</para>
        /// <para>重量測定を行った場合、在庫テーブルの残量管理は管理対象となる。</para>
        /// </summary>
        public int? WEIGHT_MGMT { get; set; }

        /// <summary>
        /// <para>重量測定されているか</para>
        /// <para>重量管理が必須の場合かつ初期重量が測定されていない場合は null を返す。</para>
        /// <para><see cref="DataInfoBase{T}.ValidateData(T)"/> で有効な状態であるかを検証する。</para>
        /// </summary>
        [Required(ErrorMessage = "重量管理が必須のため、重量測定を行ってください。")]
        public string NeedsWeightMeasurement
        {
            get
            {
                if (WEIGHT_MGMT == (int)WeightManagement.Required && !CUR_GROSS_WEIGHT_G.HasValue) return null;
                return "OK";
            }
        }

        /// <summary>
        /// GHS分類名
        /// </summary>
        public string GHSCAT_NAME { get; set; } = string.Empty;

        /// <summary>
        /// 法規制名
        /// </summary>
        public string REG_TEXT { get; set; } = string.Empty;

        /// <summary>
        /// 単位名
        /// </summary>
        public string UNITSIZE_NAME { get; set; }

        /// <summary>
        /// 容量+単位名
        /// </summary>
        public string UNITSIZE_NAME_JOIN
        {
            get
            {
                return UNITSIZE + UNITSIZE_NAME;
            }
        }

        /// <summary>
        /// 保管場所名
        /// </summary>
        public string LOC_NAME { get; set; }

        private string _LOC_BARCODE { get; set; }
        /// <summary>
        /// 保管場所コード
        /// </summary>
        public string LOC_BARCODE
        {
            get
            {
                return _LOC_BARCODE;
            }
            set
            {
                _LOC_BARCODE = value?.ToUpper();
            }
        }

        /// <summary>
        /// 初期保管場所名
        /// </summary>
        public string FIRST_LOC_NAME { get; set; }

        private string _FIRST_LOC_BARCODE { get; set; }
        /// <summary>
        /// 初期保管場所コード
        /// </summary>
        public string FIRST_LOC_BARCODE
        {
            get
            {
                return _FIRST_LOC_BARCODE;
            }
            set
            {
                _FIRST_LOC_BARCODE = value?.ToUpper();
            }
        }


        /// <summary>
        /// 使用者コード
        /// </summary>
        public string USAGE_USER_CD { get; set; }

        /// <summary>
        /// 使用者名
        /// </summary>
        public string USAGE_USER_NM { get; set; }

        /// <summary>
        /// 使用者グループ名
        /// </summary>
        public string USAGE_GROUP_NAME { get; set; }

        /// <summary>
        /// 使用者グループコード
        /// </summary>
        public string USAGE_GROUP_CD { get; set; }

        /// <summary>
        /// 使用履歴メモ
        /// </summary>
        public string USAGE_MEMO { get; set; }

        /// <summary>
        /// 内容量
        /// </summary>
        public decimal? DETAIL_WEIGHT { get; set; }

        /// <summary>
        /// 初期重量表示名
        /// </summary>
        public string INI_GROSS_WEIGHT_G_NAME { get; set; }
        public string DETAIL_WEIGHT_NAME { get; set; }

        //public string INI_GROSS_WEIGHT_G_NAME
        //{
        //    get
        //    {
        //        if (INI_GROSS_WEIGHT_G.HasValue) return INI_GROSS_WEIGHT_G.ToString() + "g";
        //        return null;
        //    };
        //}

            /// <summary>
            /// 内容量表示名
            /// </summary>
            //public string DETAIL_WEIGHT_NAME
            //{
            //    get
            //    {
            //        if (DETAIL_WEIGHT.HasValue) return DETAIL_WEIGHT.ToString() + "g";
            //        return null;
            //    }

            //}

            /// <summary>
            /// 受入済みか
            /// </summary>
        public bool IsAccepted { get; set; } = false;

        /// <summary>
        /// 瓶重量を計算し格納します。
        /// </summary>
        public void CalculateBottleWeight()
        {
            if (INI_GROSS_WEIGHT_G.HasValue && DETAIL_WEIGHT.HasValue)
            {
                BOTTLE_WEIGHT_G = INI_GROSS_WEIGHT_G - DETAIL_WEIGHT;
            }
            else
            {
                BOTTLE_WEIGHT_G = null;
            }
        }

        /// <summary>
        /// ステータス名を格納します。
        /// </summary>
        public string STATUS_TEXT { get; set; }

        public string CHANGE_RESULT { get; set; }

        //2019/04/26 TPE.Rin Add Start
        /// <summary>
        /// 消防法
        /// </summary>
        public string FIRECAT_NAME { get; set; }

        /// <summary>
        /// 社内ルール
        /// </summary>
        public string OFFICECAT_NAME { get; set; }

        //2019/04/26 TPE.Rin Add End

        //Included for username and groupcode and groupname values on 19/08/2019
        public string USER_NAME { get; set; }
        public string GROUP_CD { get; set; }
        public string GROUP_NAME { get; set; }        
    }

    public class ChemStockSearchModel : ChemStockModel
    {
        public string LOCATION_SELECTION { get; set; }

        public string LocationSelectionText
        {
            get
            {
                return LOCATION_SELECTION;
            }
        }

        public string LocationClickEvent { get; set; }

        /// <summary>
        /// ID
        /// </summary>
        public string JOIN_STOCK_ID { get; set; }
        public int INDEX { get; set; }

        public string RESULT { get; set; }
    }
}

namespace ZyCoaG.Nikon.Stock
{
    public class ChemStockDataInfo : DataInfoBase<ChemStockModel>
    {
        protected override string TableName { get; } = nameof(T_STOCK);

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            throw new NotImplementedException();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            throw new NotImplementedException();
        }

        public override List<ChemStockModel> GetSearchResult(ChemStockModel condition)
        {
            throw new NotImplementedException();
        }

        protected override void InitializeDictionary() { }

        public override void EditData(ChemStockModel Value)
        {
            throw new NotImplementedException();
        }

        public override void Add(IEnumerable<ChemStockModel> values)
        {
            IsValid = true;
            ErrorMessages.Clear();
            //StartTransaction();
            int rowNumber = 1;
            foreach (var value in values)
            {
                // 画面で瓶番号採番済み且つまだ受入していないデータを対象とします。
                if (!string.IsNullOrWhiteSpace(value.BARCODE) && !value.IsAccepted)
                {
                    addEntity(value, rowNumber);
                }
                rowNumber++;
            }
            //if (IsValid)
            //{
            //    Commit();
            //}
            //else
            //{
            //    Rollback();
            //}
            //Close();
        }

        protected override string addEntity(ChemStockModel value, int? rowNumber)
        {
            ValidateDataEntity(value, rowNumber);
            if (!IsValid) return null;
            ValidateKeyData(value, UpdateType.Add, rowNumber);
            if (!IsValid) return null;
            DynamicParameters parameters = new DynamicParameters();
            string sql;
            List<ChemStockModel> records = new List<ChemStockModel>();

            CreateOrderInsertText(value, out sql, out parameters);
            DbUtil.ExecuteUpdate(sql, parameters);

            CreateOrderSequenceSelectText(value, out sql, out parameters);
            List<string> recordss = DbUtil.selectChemSingle<string>(sql, parameters);
            value.ORDER_NO = Convert.ToInt32(recordss[0].ToString());

            CreateInsertText(value, out sql, out parameters);
            DbUtil.ExecuteUpdate(sql, parameters);

            CreateStockSequenceSelectText(value, out sql, out parameters);
            records = DbUtil.Select<ChemStockModel>(sql, parameters);
            value.STOCK_ID = Convert.ToInt32(records[0].STOCK_ID.ToString());
            value.USAGE_USER_CD = value.OWNER;

            // 2018/12/11 Add End TPE Kometani
            CreateGetGroupSelectText(value, out sql, out parameters);
            records = DbUtil.Select<ChemStockModel>(sql, parameters);
            value.USAGE_USER_NM = Convert.ToString(records[0].USER_NAME.ToString());
            value.USAGE_GROUP_NAME = Convert.ToString(records[0].GROUP_NAME.ToString());
            value.USAGE_GROUP_CD = Convert.ToString(records[0].GROUP_CD.ToString());
            // 2018/12/11 Add End TPE Kometani
            CreateUsageHistoryInsertText(value, Const.cACTION_HISTORY_TYPE.FIRST_ADD, out sql, out parameters);
            DbUtil.ExecuteUpdate(sql, parameters);
            return null;
        }

        protected void CreateOrderInsertText(ChemStockModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            insertSql.AppendLine("insert into ");
            insertSql.AppendLine(" " + nameof(T_ORDER));
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  " + nameof(T_ORDER.STATUS_ID) + ",");
            insertSql.AppendLine("  " + nameof(T_ORDER.ORDER_USER_CD) + ",");
            insertSql.AppendLine("  " + nameof(T_ORDER.CAT_CD) + ",");
            insertSql.AppendLine("  " + nameof(T_ORDER.UNITSIZE) + ",");
            insertSql.AppendLine("  " + nameof(T_ORDER.UNITSIZE_ID) + ",");
            insertSql.AppendLine("  " + nameof(T_ORDER.PRODUCT_NAME_JP) + ",");
            insertSql.AppendLine("  " + nameof(T_ORDER.PRODUCT_NAME_SEARCH) + ",");
            insertSql.AppendLine("  " + nameof(T_ORDER.MAKER_ID) + ",");
            insertSql.AppendLine("  " + nameof(T_ORDER.MEMO) + ",");
            insertSql.AppendLine("  " + nameof(T_ORDER.ORDER_DATE));
            insertSql.AppendLine(" )");
            insertSql.AppendLine("values ");
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  @" + nameof(T_ORDER.STATUS_ID) + ",");
            insertSql.AppendLine("  @" + nameof(T_ORDER.ORDER_USER_CD) + ",");
            insertSql.AppendLine("  @" + nameof(T_ORDER.CAT_CD) + ",");
            insertSql.AppendLine("  @" + nameof(T_ORDER.UNITSIZE) + ",");
            insertSql.AppendLine("  @" + nameof(T_ORDER.UNITSIZE_ID) + ",");
            insertSql.AppendLine("  @" + nameof(T_ORDER.PRODUCT_NAME_JP) + ",");
            insertSql.AppendLine("  @" + nameof(T_ORDER.PRODUCT_NAME_JP) + ",");
            insertSql.AppendLine("  @" + nameof(T_ORDER.MAKER_ID) + ",");
            insertSql.AppendLine("  @" + nameof(T_ORDER.MEMO) + ",");
            insertSql.AppendLine("  @" + nameof(T_ORDER.ORDER_DATE));
            insertSql.AppendLine(" )");
            parameters.Add(nameof(T_ORDER.STATUS_ID), (int)Const.cORDER_STATUS_ID.ORDER_FIN);
            parameters.Add(nameof(T_ORDER.ORDER_USER_CD), value.OWNER);
            parameters.Add(nameof(T_ORDER.CAT_CD), value.CAT_CD);
            parameters.Add(nameof(T_ORDER.UNITSIZE), value.UNITSIZE);
            parameters.Add(nameof(T_ORDER.UNITSIZE_ID), value.UNITSIZE_ID);
            parameters.Add(nameof(T_ORDER.PRODUCT_NAME_JP), value.PRODUCT_NAME_JP);
            parameters.Add(nameof(T_ORDER.MAKER_ID), value.MAKER_ID);
            parameters.Add(nameof(T_ORDER.MEMO), value.MEMO);
            parameters.Add(nameof(T_ORDER.ORDER_DATE), value.DELI_DATE);

            sql = insertSql.ToString();
        }

        protected void CreateOrderSequenceSelectText(ChemStockModel value, out string sql, out DynamicParameters parameters)
        {
            var selectSql = new StringBuilder();
            parameters = new DynamicParameters();

            if (Common.DatabaseType == DatabaseType.Oracle)
            {
                selectSql.AppendLine("select ");
                selectSql.AppendLine(" ORDER_NO_SEQ.currval");
                selectSql.AppendLine("from ");
                selectSql.AppendLine(" dual");
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                selectSql.AppendLine("select ");
                selectSql.AppendLine(" IDENT_CURRENT('T_ORDER') AS Id");
            }
            sql = selectSql.ToString();
        }

        protected void CreateStockSequenceSelectText(ChemStockModel value, out string sql, out DynamicParameters parameters)
        {
            var selectSql = new StringBuilder();
            parameters = new DynamicParameters();

            if (Common.DatabaseType == DatabaseType.Oracle)
            {
                selectSql.AppendLine("select ");
                selectSql.AppendLine(" STOCK_ID_SEQ.currval");
                selectSql.AppendLine("from ");
                selectSql.AppendLine(" dual");
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                selectSql.AppendLine("select ");
                selectSql.AppendLine(" IDENT_CURRENT('T_STOCK') AS Id");
            }

            sql = selectSql.ToString();
        }

        protected override void CreateInsertText(ChemStockModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            value.context = Context;

            insertSql.AppendLine("insert into ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  " + nameof(ChemStockModel.STATUS_ID) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.BARCODE) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.OWNER) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.UNITSIZE) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.UNITSIZE_ID) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.PRODUCT_NAME_JP) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.PRODUCT_NAME_SEARCH) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.MAKER_ID) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.MANAGE_WEIGHT) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.FIRST_LOC_ID) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.LOC_ID) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.INI_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.BOTTLE_WEIGHT_G) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.CUR_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.CAT_CD) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.MEMO) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.ORDER_NO) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.DELI_DATE) + ",");
            insertSql.AppendLine("  " + nameof(ChemStockModel.REG_NO));
            insertSql.AppendLine(" )");
            insertSql.AppendLine("values ");
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.STATUS_ID) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.BARCODE) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.OWNER) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.UNITSIZE) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.UNITSIZE_ID) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.PRODUCT_NAME_JP) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.PRODUCT_NAME_JP) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.MAKER_ID) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.MANAGE_WEIGHT) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.FIRST_LOC_ID) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.LOC_ID) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.INI_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.BOTTLE_WEIGHT_G) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.CUR_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.CAT_CD) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.MEMO) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.ORDER_NO) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.DELI_DATE) + ",");
            insertSql.AppendLine("  @" + nameof(ChemStockModel.REG_NO));
            insertSql.AppendLine(" )");
            parameters.Add(nameof(ChemStockModel.STATUS_ID), (int)Const.cSTOCK_STATUS_ID.STOCK);
            parameters.Add(nameof(ChemStockModel.OWNER), value.OWNER);
            parameters.Add(nameof(ChemStockModel.BARCODE), value.REG_BARCODE);
            parameters.Add(nameof(ChemStockModel.UNITSIZE), value.UNITSIZE);
            parameters.Add(nameof(ChemStockModel.UNITSIZE_ID), value.UNITSIZE_ID);
            parameters.Add(nameof(ChemStockModel.PRODUCT_NAME_JP), value.PRODUCT_NAME_JP);
            parameters.Add(nameof(ChemStockModel.MAKER_ID), value.MAKER_ID);
            // 重量測定済みであれば管理対象とする。
            if (value.INI_GROSS_WEIGHT_G.HasValue)
            {
                parameters.Add(nameof(ChemStockModel.MANAGE_WEIGHT), value.WEIGHT_MGMT);
            }
            else
            {
                parameters.Add(nameof(ChemStockModel.MANAGE_WEIGHT), (int)WeightManagement.None);
            }
            parameters.Add(nameof(ChemStockModel.FIRST_LOC_ID), value.FIRST_LOC_ID);
            parameters.Add(nameof(ChemStockModel.LOC_ID), value.LOC_ID);
            parameters.Add(nameof(ChemStockModel.INI_GROSS_WEIGHT_G), value.INI_GROSS_WEIGHT_G);
            parameters.Add(nameof(ChemStockModel.BOTTLE_WEIGHT_G), value.BOTTLE_WEIGHT_G);
            parameters.Add(nameof(ChemStockModel.CUR_GROSS_WEIGHT_G), value.CUR_GROSS_WEIGHT_G);
            parameters.Add(nameof(ChemStockModel.CAT_CD), value.CAT_CD);
            parameters.Add(nameof(ChemStockModel.MEMO), value.MEMO);
            parameters.Add(nameof(ChemStockModel.ORDER_NO), value.ORDER_NO);
            parameters.Add(nameof(ChemStockModel.DELI_DATE), value.DELI_DATE);
            parameters.Add(nameof(ChemStockModel.REG_NO), value.REG_NO);

            sql = insertSql.ToString();
        }

        protected void CreateUsageHistoryInsertText(ChemStockModel value, Const.cACTION_HISTORY_TYPE actionType, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            insertSql.AppendLine("insert into ");
            insertSql.AppendLine(" " + nameof(T_ACTION_HISTORY));
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.ACTION_TYPE_ID) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.BARCODE) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.USER_CD) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.LOC_ID) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.LOC_NAME) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.CUR_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.INTENDED_USE_ID) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.INTENDED_NM) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.INTENDED) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.MEMO) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.STOCK_ID) + ",");
            // 2018/12/11 Add Start TPE Kometani
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.ACTION_USER_NM) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.ACTION_GROUP_NAME) + ",");
            insertSql.AppendLine("  " + nameof(T_ACTION_HISTORY.ACTION_GROUP_CD));
            // 2018/12/11 Add End TPE Kometani
            insertSql.AppendLine(" )");
            insertSql.AppendLine("values ");
            insertSql.AppendLine(" (");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.ACTION_TYPE_ID) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.BARCODE) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.USER_CD) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.LOC_ID) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.LOC_NAME) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.CUR_GROSS_WEIGHT_G) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.INTENDED_USE_ID) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.INTENDED_NM) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.INTENDED) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.MEMO) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.STOCK_ID) + ",");
            // 2018/12/11 Add Start TPE Kometani
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.ACTION_USER_NM) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.ACTION_GROUP_NAME) + ",");
            insertSql.AppendLine("  @" + nameof(T_ACTION_HISTORY.ACTION_GROUP_CD));
            // 2018/12/11 Add End TPE Kometani
            insertSql.AppendLine(" )");
            parameters.Add(nameof(T_ACTION_HISTORY.ACTION_TYPE_ID), (int)actionType);
            parameters.Add(nameof(T_ACTION_HISTORY.BARCODE), value.REG_BARCODE);
            parameters.Add(nameof(T_ACTION_HISTORY.USER_CD), value.USAGE_USER_CD);
            parameters.Add(nameof(T_ACTION_HISTORY.LOC_ID), value.LOC_ID);
            parameters.Add(nameof(T_ACTION_HISTORY.LOC_NAME), value.FIRST_LOC_NAME);
            parameters.Add(nameof(T_ACTION_HISTORY.CUR_GROSS_WEIGHT_G), value.CUR_GROSS_WEIGHT_G);
            parameters.Add(nameof(T_ACTION_HISTORY.INTENDED_USE_ID), value.INTENDED_USE_ID);
            parameters.Add(nameof(T_ACTION_HISTORY.INTENDED_NM), value.INTENDED_NM);
            parameters.Add(nameof(T_ACTION_HISTORY.INTENDED), value.INTENDED);
            parameters.Add(nameof(T_ACTION_HISTORY.MEMO), value.USAGE_MEMO);
            parameters.Add(nameof(T_ACTION_HISTORY.STOCK_ID), value.STOCK_ID);
            // 2018/12/11 Add Start TPE Kometani
            parameters.Add(nameof(T_ACTION_HISTORY.ACTION_USER_NM), value.USAGE_USER_NM);
            parameters.Add(nameof(T_ACTION_HISTORY.ACTION_GROUP_NAME), value.USAGE_GROUP_NAME);
            parameters.Add(nameof(T_ACTION_HISTORY.ACTION_GROUP_CD), value.USAGE_GROUP_CD);
            // 2018/12/11 Add End TPE Kometani

            sql = insertSql.ToString();
        }

        protected override void CreateUpdateText(ChemStockModel value, out string sql, out DynamicParameters parameters)
        {
            var insertSql = new StringBuilder();
            parameters = new DynamicParameters();

            insertSql.AppendLine("update ");
            insertSql.AppendLine(" " + TableName);
            insertSql.AppendLine("set ");
            insertSql.AppendLine(" " + nameof(value.REG_NO) + " = @" + nameof(value.REG_NO) + ",");
            insertSql.AppendLine(" " + nameof(value.MAKER_ID) + " = @" + nameof(value.MAKER_ID) + ",");
            insertSql.AppendLine(" " + nameof(value.UNITSIZE) + " = @" + nameof(value.UNITSIZE) + ",");
            insertSql.AppendLine(" " + nameof(value.UNITSIZE_ID) + " = @" + nameof(value.UNITSIZE_ID) + ",");
            insertSql.AppendLine(" " + nameof(value.MEMO) + " = @" + nameof(value.MEMO) + ",");
            insertSql.AppendLine(" " + nameof(value.BORROWER) + " = @" + nameof(value.BORROWER) + ",");
            if (Common.DatabaseType == DatabaseType.Oracle)
            {
                insertSql.AppendLine(" " + nameof(value.BRW_DATE) + " = SYSDATE");
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                insertSql.AppendLine(" " + nameof(value.BRW_DATE) + " = getdate()");
            }
            insertSql.AppendLine("where ");
            insertSql.AppendLine(getKeyWhere(value));
            parameters.Add(nameof(value.REG_NO), value.REG_NO);
            parameters.Add(nameof(value.MAKER_ID), value.MAKER_ID);
            parameters.Add(nameof(value.UNITSIZE), value.UNITSIZE);
            parameters.Add(nameof(value.UNITSIZE_ID), value.UNITSIZE_ID);
            parameters.Add(nameof(value.MEMO), value.MEMO);
            parameters.Add(nameof(value.BORROWER), value.BORROWER);
            parameters.Add(nameof(value.STOCK_ID), value.STOCK_ID);

            sql = insertSql.ToString();
        }

        protected void CreateGetGroupSelectText(ChemStockModel value, out string sql, out DynamicParameters parameters)
        {
            var selectSql = new StringBuilder();
            parameters = new DynamicParameters();

            selectSql.AppendLine("SELECT ");
            selectSql.AppendLine("mu.USER_NAME, mu.GROUP_CD, mg.GROUP_NAME ");
            selectSql.AppendLine("from M_USER mu inner ");
            selectSql.AppendLine("join V_GROUP mg ");
            selectSql.AppendLine("on mg.GROUP_CD = mu.GROUP_CD ");
            selectSql.AppendLine("where mu.USER_CD = @USAGE_USER_CD ");

            parameters.Add(nameof(value.USAGE_USER_CD), value.USAGE_USER_CD);

            sql = selectSql.ToString();
        }

        protected override void CreateInsertText(ChemStockModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(ChemStockModel value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }

    public class ChemStockDataInfoBulkChange : ChemStockDataInfo
    {
        /// <summary>
        /// 参照権限
        /// </summary>
        public string REFERENCE_AUTHORITY { get; set; }

        /// <summary>
        /// ログインユーザー
        /// </summary>
        public string LOGIN_USER_CD { get; set; }

        public string LocationClickEvent { get; set; }

        public void ChangeLocation()
        {

        }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            var condition = (ChemStockModel)dynamicCondition;
            var sql = new StringBuilder();
            sql.AppendLine("from");
            sql.AppendLine(" T_STOCK s ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" T_ORDER o ");
            sql.AppendLine("  on s.ORDER_NO = o.ORDER_NO ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_CHEM c ");
            sql.AppendLine("  on s.CAT_CD = c.CHEM_CD ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_MAKER m ");
            sql.AppendLine("  on s.MAKER_ID = m.MAKER_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_USER u ");
            sql.AppendLine("  on o.ORDER_USER_CD = u.USER_CD ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_UNITSIZE us ");
            sql.AppendLine("  on s.UNITSIZE_ID = us.UNITSIZE_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_GROUP g ");
            sql.AppendLine("  on u.GROUP_ID = g.GROUP_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_VENDOR v ");
            sql.AppendLine("  on o.VENDOR_ID = v.VENDOR_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_KANJYO k ");
            sql.AppendLine("  on o.KANJYO_ID = k.KANJYO_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_GHSCAT gc ");
            sql.AppendLine("  on s.CAT_CD = gc.CHEM_CD ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_REGULATION r ");
            sql.AppendLine("  on s.CAT_CD = r.CHEM_CD ");
            //2019/04/26 TPE.Rin Add Start
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_FIRE cf ");
            sql.AppendLine("  on s.CAT_CD = cf.CHEM_CD ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_CHEM_OFFICE co ");
            sql.AppendLine("  on s.CAT_CD = co.CHEM_CD ");
            //2019/04/26 TPE.Rin Add End
            sql.AppendLine(" left outer join");
            sql.AppendLine(" M_STATUS st ");
            sql.AppendLine("  on s.STATUS_ID = st.STATUS_ID ");
            sql.AppendLine(" left outer join");
            sql.AppendLine(" V_LOCATION sl ");
            sql.AppendLine("  on s.LOC_ID = sl.LOC_ID ");

            if (REFERENCE_AUTHORITY == Const.cREFERENCE_AUTHORITY_FLAG_NORMAL)
            {
                sql.AppendLine(" left outer join");
                sql.AppendLine("  T_MGMT_LEDGER ml ");
                sql.AppendLine("  on s.REG_NO = ml.REG_NO ");
            }

            return sql.ToString();
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters parameters)
        {
            var condition = (ChemStockSearchModel)dynamicCondition;
            parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("where ");
            sql.AppendLine(" s.DEL_FLAG <> 1 ");
            //sql.AppendLine("  and s.BARCODE in( 'A00000000001','A00000000002' )");
            sql.AppendLine("  and s.BARCODE in ('@BARCODE' );");

            parameters.Add("BARCODE", condition.JOIN_STOCK_ID.Replace(",", "','"));

            return sql.ToString();
        }

        public override List<ChemStockModel> GetSearchResult(ChemStockModel condition)
        {
            var sql = new StringBuilder();
            DynamicParameters parameters = null;

            sql.AppendLine("select");
            sql.AppendLine(" s.STOCK_ID,");
            sql.AppendLine(" c.CHEM_NAME PRODUCT_NAME_JP,");
            sql.AppendLine(" m.MAKER_ID,");
            sql.AppendLine(" m.MAKER_NAME,");
            sql.AppendLine(" v.VENDOR_ID,");
            sql.AppendLine(" v.VENDOR_NAME,");
            sql.AppendLine(" s.CAT_CD,");
            if (Common.DatabaseType == DatabaseType.Oracle)
            {
                sql.AppendLine(" to_char(s.UNITSIZE) || us.UNIT_NAME DISPLAY_SIZE,");
                sql.AppendLine(" s.UNITSIZE,");
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                sql.AppendLine(" format(s.UNITSIZE,'0.#########') + us.UNIT_NAME DISPLAY_SIZE,");
                sql.AppendLine(" format(s.UNITSIZE,'0.#########') UNITSIZE,");
            }
            sql.AppendLine(" s.UNITSIZE_ID,");
            sql.AppendLine(" us.UNIT_NAME UNITSIZE_NAME,");
            sql.AppendLine(" s.GRADE,");
            if (Common.DatabaseType == DatabaseType.Oracle)
            {
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (to_char(s.CUR_GROSS_WEIGHT_G - nvl(s.BOTTLE_WEIGHT_G, 0)) || 'g') end) CONTENT_WEIGHT,");
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (to_char(s.CUR_GROSS_WEIGHT_G - nvl(s.BOTTLE_WEIGHT_G, 0))) end) CONTENT_WEIGHT_VOLUME,");
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else 'g' end) CONTENT_WEIGHT_UNITSIZE,");
                sql.AppendLine(" to_char(s.DELI_DATE, 'yyyy/MM/dd') DELI_DATE,");
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (convert(nvarchar,s.CUR_GROSS_WEIGHT_G - IsNull(s.BOTTLE_WEIGHT_G, 0)) + 'g') end) CONTENT_WEIGHT,");
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (convert(nvarchar,s.CUR_GROSS_WEIGHT_G - IsNull(s.BOTTLE_WEIGHT_G, 0))) end) CONTENT_WEIGHT_VOLUME,");
                sql.AppendLine(" (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else 'g' end) CONTENT_WEIGHT_UNITSIZE,");
                sql.AppendLine(" convert(nvarchar,s.DELI_DATE,111) DELI_DATE,");
            }
            sql.AppendLine(" s.ORDER_NO,");
            sql.AppendLine(" s.BARCODE,");
            sql.AppendLine(" gc.GHSCAT_NAME,");
            sql.AppendLine(" r.REG_TEXT,");
            //2019/04/26 TPE.Rin Add Start
            sql.AppendLine(" cf.FIRECAT_NAME,");
            sql.AppendLine(" co.OFFICECAT_NAME,");
            //2019/04/26 TPE.Rin Add End
            sql.AppendLine(" o.PRICE,");
            sql.AppendLine(" u.USER_NAME ORDER_USER,");
            sql.AppendLine(" o.ORDER_USER_CD OWNER,");
            sql.AppendLine(" g.GROUP_NAME,");
            sql.AppendLine(" v.VENDOR_NAME,");
            sql.AppendLine(" k.KANJYO_NAME,");
            sql.AppendLine(" st.STATUS_TEXT,");
            sql.AppendLine(" sl.LOC_NAME ");

            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out parameters));

            //StartTransaction();
            List<ChemStockModel> records = DbUtil.Select<ChemStockModel>(sql.ToString(), parameters);

            //var dataArray = new List<ChemStockModel>();
            //foreach (DataRow record in records.Rows)
            //{
            //    var data = new ChemStockModel();
            //    data.STOCK_ID = Convert.ToInt32(record[nameof(ChemStockModel.STOCK_ID)].ToString());
            //    data.PRODUCT_NAME_JP = record[nameof(ChemStockModel.PRODUCT_NAME_JP)].ToString();
            //    data.MAKER_NAME = record[nameof(ChemStockModel.MAKER_NAME)].ToString();
            //    data.UNITSIZE = record[nameof(ChemStockModel.UNITSIZE)].ToString();
            //    data.UNITSIZE_NAME = record[nameof(ChemStockModel.UNITSIZE_NAME)].ToString();
            //    data.BARCODE = record[nameof(ChemStockModel.BARCODE)].ToString();
            //    data.LOC_NAME = record[nameof(ChemStockModel.LOC_NAME)].ToString();
            //    data.CAT_CD = record[nameof(ChemStockModel.CAT_CD)].ToString();
            //    data.OWNER = record[nameof(ChemStockModel.OWNER)].ToString();
            //    data.DELI_DATE = OrgConvert.ToNullableDateTime(record[nameof(ChemStockModel.DELI_DATE)].ToString());
            //    data.DETAIL_WEIGHT = OrgConvert.ToNullableDecimal(record["CONTENT_WEIGHT_VOLUME"].ToString());
            //    data.STATUS_TEXT = record[nameof(ChemStockModel.STATUS_TEXT)].ToString();
            //    data.GHSCAT_NAME = record[nameof(ChemStockModel.GHSCAT_NAME)].ToString();
            //    data.REG_TEXT = record[nameof(ChemStockModel.REG_TEXT)].ToString();
            //    //2019/04/26 TPE.Rin Start
            //    data.FIRECAT_NAME = record[nameof(ChemStockModel.FIRECAT_NAME)].ToString();
            //    data.OFFICECAT_NAME = record[nameof(ChemStockModel.OFFICECAT_NAME)].ToString();
            //    //2019/04/26 TPE.Rin End
            //    dataArray.Add(data);
            //}

            var searchQuery = records.AsQueryable();

            searchQuery = searchQuery.OrderBy(x => x.BARCODE);

            return searchQuery.ToList();
        }
    }
}