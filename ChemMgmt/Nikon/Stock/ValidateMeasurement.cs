﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.Nikon.Stock
{
    public class Measurement
    {
        public List<string> ValidateMessages { get; private set; }

        public bool IsValid { get; private set; } = true;

        public decimal? TotalWeight { get; set; }

        public decimal? DetailWeight { get; set; }

        private string totalWeightName { get; } = "総重量";

        private string detailWeightName { get; } = "内容量";

        private int integerNumberLength { get; } = 8;

        private int decimalNumberLength { get; } = 3;

        public Measurement(string totalWeight, string detailWeight)
        {
            ValidateMessages = new List<string>();

            decimal? totalWeightDecimal;
            weightValidate(totalWeight, totalWeightName, out totalWeightDecimal);
            TotalWeight = totalWeightDecimal;

            decimal? detailWeightDecimal;
            weightValidate(detailWeight, detailWeightName, out detailWeightDecimal);
            DetailWeight = detailWeightDecimal;

            if (TotalWeight.HasValue && DetailWeight.HasValue) validateDetailAndTotalWeight(TotalWeight.Value, DetailWeight.Value);
        }

        private void weightValidate(string weight, string name, out decimal? weightDecimal)
        {
            weightDecimal = null;
            if (string.IsNullOrEmpty(weight))
            {
                IsValid = false;
                ValidateMessages.Add(string.Format(WarningMessages.Required, name));
            }
            else
            {
                var w = default(decimal);
                if (!decimal.TryParse(weight, out w))
                {
                    IsValid = false;
                    ValidateMessages.Add(string.Format(WarningMessages.NotNumber, name));
                }

                var numberArray = weight.Replace("-", string.Empty).Replace(",", string.Empty).Split(".".ToCharArray());
                if (numberArray[0].Length > integerNumberLength)
                {
                    IsValid = false;
                    ValidateMessages.Add(string.Format(WarningMessages.PositiveNumberLength, name, integerNumberLength.ToString()));
                    return;
                }

                weightValidate(ref w, name);
                weightDecimal = w;
            }
        }

        private void weightValidate(ref decimal weight, string name)
        {
            if (weight <= 0)
            {
                IsValid = false;
                ValidateMessages.Add(string.Format(WarningMessages.NotRegularNumber, name));
                return;
            }
            if (weight.ToString().IndexOf(".") != -1 && weight.ToString().Substring(weight.ToString().IndexOf(".") + 1).Length > 3)
            {
                weight = Math.Round(weight, 3, MidpointRounding.AwayFromZero);
                string InfoMessages = "{0}の小数点第{1}位を四捨五入します。";
                ValidateMessages.Add(string.Format(InfoMessages, name, 4));
            }
        }

        private void validateDetailAndTotalWeight(decimal totalWeight, decimal detailWeight)
        {
            if (totalWeight < detailWeight)
            {
                IsValid = false;
                ValidateMessages.Add(WarningMessages.DetailWeightExceeding);
            }
        }
    }
}