﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Nikon
{
    public abstract partial class NikonEditPageBase : NikonZyCoaGMasterPageBase
    {

        /// <summary>
        /// 画面のモードを取得または設定します。
        /// </summary>
        protected Mode mode { get; set; }

        /// <summary>
        /// メーカーマスターからデータを取得します。
        /// </summary>
        /// <returns>メーカーマスターデータを取得します。</returns>
        public override IEnumerable<CommonDataModel<int?>> GetMakerList()
        {
            switch (mode)
            {
                case Mode.New:
                case Mode.Copy:
                case Mode.Edit:
                case Mode.TemporaryNew:  // 20180723 FJ)Shinagawa Add
                case Mode.TemporaryEdit: // 20180723 FJ)Shinagawa Add
                    return new MakerMasterInfo().GetSelectList();
                case Mode.View:
                case Mode.Deleted:
                case Mode.ReferenceAfterRegistration:
                case Mode.Abolition:
                case Mode.Approval:
                    return new MakerMasterInfo().GetViewList();
                default:
                    throw new ArgumentNullException();
            }
        }

        /// <summary>
        /// 容量単位マスターからデータを取得します。
        /// </summary>
        /// <returns>容量単位マスターデータを取得します。</returns>
        //public override IEnumerable<CommonDataModel<int?>> GetUnitSizeList()
        //{
        //    switch (mode)
        //    {
        //        case Mode.New:
        //        case Mode.Copy:
        //        case Mode.Edit:
        //        case Mode.TemporaryNew:  // 20180723 FJ)Shinagawa Add
        //        case Mode.TemporaryEdit: // 20180723 FJ)Shinagawa Add
        //                                 //case Mode.Approval: // 20180910 FJ)Sugimoto Add
        //            return new UnitSizeMasterInfo().GetSelectList();
        //        case Mode.View:
        //        case Mode.Deleted:
        //        case Mode.ReferenceAfterRegistration:
        //        case Mode.Abolition:
        //            //case Mode.Approval: // 20180910 FJ)Sugimoto Del
        //            return new UnitSizeMasterInfo().GetViewList();
        //        case Mode.Approval: // 20180910 FJ)Sugimoto Add
        //            return new UnitSizeMasterInfo().GetApprovalList(); // 20180910 FJ)Sugimoto Add
        //        default:
        //            throw new ArgumentNullException();
        //    }
        //}
    }
}