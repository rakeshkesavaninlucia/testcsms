﻿using ChemMgmt.Classes;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Util;

namespace ChemMgmt.Nikon.Flow
{
    public class UserWorkflowDataInfo : DataInfoBase<T_LED_WORK_HISTORY>
    {

        protected override string TableName { get { return null; } }

        public override void EditData(T_LED_WORK_HISTORY Value) { }

        public static string HIERARCHY_NULL_new = "新規登録申請";

        public static string HIERARCHY_NULL_Remand = "変更申請";

        public static string HIERARCHY_NULL_Abolish = "廃止申請";

        public static string HIERARCHY_0 = "上長承認";


        public IQueryable<T_LED_WORK_HISTORY> GetBlankWorkflow(int? LEDGER_FLOW_ID, string USER_CD, bool modify, string HIERARCHY_NULL_NAME = null)
        {
            DynamicParameters parameters = new DynamicParameters();
            var sql = new StringBuilder();

            sql.AppendLine("select ");
            if (LEDGER_FLOW_ID == null)
            {
                sql.AppendLine("NULL " + "LEDGER_FLOW_ID");
            }
            else
            {
                sql.AppendLine(" " + "'" + LEDGER_FLOW_ID + "'" + " " + "LEDGER_FLOW_ID");
            }
            sql.AppendLine(", " + "'" + USER_CD + "'" + " " + "REGISTER_USER_CD , ");

            sql.AppendLine("* from (");
            sql.AppendLine("select");
            sql.AppendLine("    M_USER.USER_CD");
            sql.AppendLine("    ,M_WORKFLOW.FLOW_HIERARCHY");
            sql.AppendLine("    ,M_WORKFLOW.FLOW_CD");
            sql.AppendLine("    ,-1 HIERARCHY");
            if (string.IsNullOrEmpty(HIERARCHY_NULL_NAME))
            {
                if (modify)
                {
                    sql.AppendLine(string.Format("    ,'{0}' APPROVAL_TARGET", HIERARCHY_NULL_Remand));
                }
                else
                {
                    sql.AppendLine(string.Format("    ,'{0}' APPROVAL_TARGET", HIERARCHY_NULL_new));
                }
            }
            else
            {
                sql.AppendLine(string.Format("    ,'{0}' APPROVAL_TARGET", HIERARCHY_NULL_NAME));
            }
            sql.AppendLine("    ,M_USER.USER_CD APPROVAL_USER_CD");
            sql.AppendLine("    , NULL SUBSITUTE_USER_CD");
            sql.AppendLine("from M_USER inner join M_WORKFLOW on M_USER.FLOW_CD = M_WORKFLOW.FLOW_CD");
            sql.AppendLine("union all");
            sql.AppendLine("select");
            sql.AppendLine("    M_USER.USER_CD");
            sql.AppendLine("    ,M_WORKFLOW.FLOW_HIERARCHY");
            sql.AppendLine("    ,M_WORKFLOW.FLOW_CD");
            sql.AppendLine("    ,0");
            sql.AppendLine(string.Format("    ,'{0}'", HIERARCHY_0));
            sql.AppendLine("    ,M_USER.APPROVER1 APPROVAL_USER_CD");
            sql.AppendLine("    , M_USER.APPROVER2 SUBSITUTE_USER_CD");
            sql.AppendLine("from M_USER inner join M_WORKFLOW on M_USER.FLOW_CD = M_WORKFLOW.FLOW_CD");
            for (var i = 1; i <= 6; i++)
            {
                sql.AppendLine("union all");
                sql.AppendLine("select");
                sql.AppendLine("    M_USER.USER_CD");
                sql.AppendLine("    ,M_WORKFLOW.FLOW_HIERARCHY");
                sql.AppendLine("    ,M_WORKFLOW.FLOW_CD");
                sql.AppendLine(string.Format("    ,{0}", i));
                sql.AppendLine(string.Format("    ,M_WORKFLOW.APPROVAL_TARGET{0}", i));
                sql.AppendLine(string.Format("    ,M_WORKFLOW.APPROVAL_USER_CD{0}", i));
                sql.AppendLine(string.Format("    ,M_WORKFLOW.SUBSITUTE_USER_CD{0}", i));
                sql.AppendLine("from M_USER inner join M_WORKFLOW on M_USER.FLOW_CD = M_WORKFLOW.FLOW_CD");
            }
            sql.AppendLine(") T");
            sql.AppendLine("where T.USER_CD=@USER_CD and (T.APPROVAL_USER_CD is not null or T.HIERARCHY < 1)");
            parameters.Add("USER_CD", USER_CD);
            //StartTransaction();
            List<T_LED_WORK_HISTORY> records = DbUtil.Select<T_LED_WORK_HISTORY>(sql.ToString(), parameters);
            //DataTable dt = ConvertToDataTable(records);
            //var dataArray = new List<T_LED_WORK_HISTORY>();

            //foreach (DataRow record in dt.Rows)
            //{
            //    var model = new T_LED_WORK_HISTORY();
            //    model.SetDataRow(record);
            //    model.LEDGER_FLOW_ID = LEDGER_FLOW_ID;
            //    model.REGISTER_USER_CD = USER_CD;
            //    if (model.FLOW_HIERARCHY >= model.HIERARCHY)
            //    {
            //        dataArray.Add(model);
            //    }
            //}
            records = records.Where(x => x.FLOW_HIERARCHY >= x.HIERARCHY).ToList();

            return records.AsQueryable();
        }
        public DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;

        }

        protected override string FromWardCreate(dynamic dynamicCondition)
        {
            return string.Empty;
        }

        protected override string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters param)
        {
            param = new DynamicParameters();
            return string.Empty;
        }

        protected override void InitializeDictionary()
        {

        }

        protected override void CreateInsertText(T_LED_WORK_HISTORY value, out string sql, out DynamicParameters parameters)
        {
            sql = string.Empty;
            parameters = new DynamicParameters();
        }

        protected override void CreateUpdateText(T_LED_WORK_HISTORY value, out string sql, out DynamicParameters parameters)
        {
            sql = string.Empty;
            parameters = new DynamicParameters();
        }

        public override List<T_LED_WORK_HISTORY> GetSearchResult(T_LED_WORK_HISTORY condition)
        {
            throw new NotImplementedException();
        }

        protected override void CreateInsertText(T_LED_WORK_HISTORY value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }

        protected override void CreateUpdateText(T_LED_WORK_HISTORY value, out string sql, out Hashtable parameters)
        {
            throw new NotImplementedException();
        }
    }
}