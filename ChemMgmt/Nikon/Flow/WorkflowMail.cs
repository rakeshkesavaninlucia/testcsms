﻿using ChemDesign.Classes.Util;
using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Nikon.History;
using ChemMgmt.Nikon.Ledger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.History;

namespace ChemMgmt.Nikon.Flow
{
    /// <summary>
    /// ワークフロー  メール送信
    /// </summary>
    public class WorkflowMail
    {
        /// <summary>
        /// メール送信
        /// </summary>
        /// <param name="model"></param>
        public void SendMail(IEnumerable<LedgerModel> models)
        {
            foreach (var model in models)
            {
                SendMail(model);
            }
        }
        
        /// <summary>
        /// メール送信
        /// </summary>
        /// <param name="model"></param>
        public void SendMail(LedgerModel model)
        {
            using (var hist = new LedgerWorkHistoryDataInfo())
            {
                var flow = hist.GetSearchResult(new T_LED_WORK_HISTORY { LEDGER_FLOW_ID = model.LEDGER_FLOW_ID });
                var first = flow.LastOrDefault(x => x.HIERARCHY == -1);
                var current = flow.LastOrDefault(x => x.RESULT != null);
                var next = flow.FirstOrDefault(x => x.RESULT == null);
                if (first == null)
                {
                    first = new T_LED_WORK_HISTORY();
                }
                if (current.RESULT == (int)WorkflowResult.Application)
                {
                    SendMailNew(model, current, next);
                }
                else if (current.RESULT == (int)WorkflowResult.Approval && next != null)
                {
                    SendMailNext(model, first, next);
                }
                else if (current.RESULT == (int)WorkflowResult.Approval && next == null)
                {
                    SendMailFinal(model, first);
                }
                else if (current.RESULT == (int)WorkflowResult.Remand)
                {
                    SendMailRemand(model, flow.LastOrDefault(x => x.HIERARCHY == -1 && x.DEL_FLAG == 1));
                }
            }
        }

        ///
        private void SendMailChange(IEnumerable<LedgerModel> models)
        {

        }

        /// <summary>
        /// 差し戻し時	申請者へメール送信
        /// </summary>
        /// <param name="model"></param>
        /// <param name="first"></param>
        private void SendMailRemand(LedgerModel model, T_LED_WORK_HISTORY first)
        {
            if (first == null)
            {
                first = new T_LED_WORK_HISTORY();
            }
            using (var mailMessage = new MailMessage())
            {
                using (var user = new UserMasterInfo())
                {
                    var approval = user.GetUser(first.ACTUAL_USER_CD);
                    if (!string.IsNullOrEmpty(approval.EMAIL))
                    {
                        mailMessage.To.Add(new MailAddress(approval.EMAIL, approval.USER_NAME));
                    }
                }
                mailMessage.Subject = Common.GetMailBodyString(
                    Path.Combine(WebConfigUtil.MAIL_TXT_PATH, "MAIL_LEDGER_REMAND_SUBJECT.txt"));
                var body = Common.GetMailBodyString(
                    Path.Combine(WebConfigUtil.MAIL_TXT_PATH, "MAIL_LEDGER_REMAND_BODY.txt"));
                mailMessage.Body = body.Replace("#1", model.APPLI_NO).Replace("#2", model.REG_NO)
                    .Replace("#3", first.ACTUAL_USER_NAME).Replace("#4", model.CHEM_NAME);
                Send(mailMessage);
            }
        }

        /// <summary>
        /// 最終承認時	申請者へメール送信
        /// </summary>
        /// <param name="model"></param>
        /// <param name="first"></param>
        private void SendMailFinal(LedgerModel model, T_LED_WORK_HISTORY first)
        {
            using (var mailMessage = new MailMessage())
            {
                using (var user = new UserMasterInfo())
                {
                    var approval = user.GetUser(first.ACTUAL_USER_CD);
                    if (!string.IsNullOrEmpty(approval.EMAIL))
                    {
                        mailMessage.To.Add(new MailAddress(approval.EMAIL, approval.USER_NAME));
                    }
                }
                mailMessage.Subject = Common.GetMailBodyString(
                    Path.Combine(WebConfigUtil.MAIL_TXT_PATH, "MAIL_LEDGER_FINALAPPROVAL_SUBJECT.txt"));
                var body = Common.GetMailBodyString(
                    Path.Combine(WebConfigUtil.MAIL_TXT_PATH, "MAIL_LEDGER_FINALAPPROVAL_BODY.txt"));
                mailMessage.Body = body.Replace("#1", model.APPLI_NO).Replace("#2", model.REG_NO)
                    .Replace("#3", first.ACTUAL_USER_NAME).Replace("#4", model.CHEM_NAME);
                Send(mailMessage);
            }
        }

        /// <summary>
        /// 承認ボタン押下時	申請フローの次の承認者および代行者へメール送信
        /// </summary>
        /// <param name="model"></param>
        /// <param name="first"></param>
        /// <param name="next"></param>
        private void SendMailNext(LedgerModel model, T_LED_WORK_HISTORY first, T_LED_WORK_HISTORY next)
        {
            using (var mailMessage = new MailMessage())
            {
                using (var user = new UserMasterInfo())
                {
                    var approval = user.GetUser(next.APPROVAL_USER_CD);
                    if (!string.IsNullOrEmpty(approval.EMAIL))
                    {
                        mailMessage.To.Add(new MailAddress(approval.EMAIL, approval.USER_NAME));
                    }
                    if (!string.IsNullOrEmpty(next.SUBSITUTE_USER_CD))
                    {
                        var subsitute = user.GetUser(next.SUBSITUTE_USER_CD);
                        if (!string.IsNullOrEmpty(subsitute.EMAIL))
                        {
                            mailMessage.To.Add(new MailAddress(subsitute.EMAIL, subsitute.USER_NAME));
                        }
                    }
                }
                mailMessage.Subject = Common.GetMailBodyString(
                    Path.Combine(WebConfigUtil.MAIL_TXT_PATH, "MAIL_LEDGER_NEW_SUBJECT.txt"));
                var body = Common.GetMailBodyString(
                    Path.Combine(WebConfigUtil.MAIL_TXT_PATH, "MAIL_LEDGER_NEW_BODY.txt"));
                mailMessage.Body = body.Replace("#1", model.APPLI_NO).Replace("#2", model.REG_NO)
                    .Replace("#3", first.ACTUAL_USER_NAME).Replace("#4", model.CHEM_NAME);
                Send(mailMessage);
            }
        }

        /// <summary>
        /// 申請ボタン押下時	申請者の上長および代理者へメール送信
        /// </summary>
        /// <param name="model"></param>
        /// <param name="current"></param>
        /// <param name="next"></param>
        private void SendMailNew(LedgerModel model, T_LED_WORK_HISTORY current, T_LED_WORK_HISTORY next)
        {
            using (var mailMessage = new MailMessage())
            {
                using (var user = new UserMasterInfo())
                {
                    var approval = user.GetUser(next.APPROVAL_USER_CD);
                    if (!string.IsNullOrEmpty(approval.EMAIL))
                    {
                        mailMessage.To.Add(new MailAddress(approval.EMAIL, approval.USER_NAME));
                    }
                    if (!string.IsNullOrEmpty(next.SUBSITUTE_USER_CD))
                    {
                        var subsitute = user.GetUser(next.SUBSITUTE_USER_CD);
                        if (!string.IsNullOrEmpty(subsitute.EMAIL))
                        {
                            mailMessage.To.Add(new MailAddress(subsitute.EMAIL, subsitute.USER_NAME));
                        }
                    }
                }
                mailMessage.Subject = Common.GetMailBodyString(
                    Path.Combine(WebConfigUtil.MAIL_TXT_PATH, "MAIL_LEDGER_NEW_SUBJECT.txt"));
                var body = Common.GetMailBodyString(
                    Path.Combine(WebConfigUtil.MAIL_TXT_PATH, "MAIL_LEDGER_NEW_BODY.txt"));
                mailMessage.Body = body.Replace("#1", model.APPLI_NO).Replace("#2", model.REG_NO)
                    .Replace("#3", current.ACTUAL_USER_NAME).Replace("#4", model.CHEM_NAME);
                Send(mailMessage);
            }
        }

        /// <summary>
        /// メール送信+モック付
        /// </summary>
        /// <param name="mailMessage"></param>
        private void Send(MailMessage mailMessage)
        {
#if DEBUG_MODE
            System.Diagnostics.Debug.WriteLine("---メール送信モック---");
            foreach (var to in mailMessage.To)
            {
                System.Diagnostics.Debug.WriteLine("To=" + to);
            }
            System.Diagnostics.Debug.WriteLine("Subject=" + mailMessage.Subject);
            System.Diagnostics.Debug.WriteLine(mailMessage.Body);
            System.Diagnostics.Debug.WriteLine("---------------------");
#else
            using (var smtpClient = new SmtpClient())
            {
                try
                {
                    smtpClient.Send(mailMessage);
                }
                catch
                {
                }
            }
#endif
        }
    }
}