﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Nikon
{
    /// <summary>
    /// 作業環境測定
    /// </summary>
    public enum MeasurementList
    {
        /// <summary>
        /// 非該当
        /// </summary>
        Higaitou,
        /// <summary>
        /// 該当
        /// </summary>
        Gaitou,
        /// <summary>
        /// 全て
        /// </summary>
        All
    }

    /// <summary>
    /// 含有物を含めるかの選択肢を取得するクラスです。
    /// </summary>
    public class MeasurementListMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        /// <summary>
        /// 選択項目に空白を追加するか
        /// </summary>
        protected override bool IsAddSpace { get; set; } = false;

        /// <summary>
        /// 全選択肢を取得します。
        /// </summary>
        /// <returns>全選択肢を<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var searchCondition = new List<CommonDataModel<int?>>();
            searchCondition.Add(new CommonDataModel<int?>((int)MeasurementList.Higaitou, "非該当", 0));
            searchCondition.Add(new CommonDataModel<int?>((int)MeasurementList.Gaitou, "該当", 1));
            searchCondition.Add(new CommonDataModel<int?>((int)MeasurementList.All, "全て", 2));
            return searchCondition;
        }
    }
}