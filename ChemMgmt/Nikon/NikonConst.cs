﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.Nikon
{
    /// <summary>
    /// システム定数を定義します。
    /// </summary>
    public static class NikonConst
    {
        /// <summary>
        /// 化学物質コードの接頭辞を取得します。
        /// </summary>
        public static string ChemCodePrefix { get; } = "C";

        /// <summary>
        /// 化学物質（一時保存）コードの接頭辞を取得します。
        /// </summary>
        public static string RefChemCodePrefix { get; } = "WK"; // 20180723 FJ)Shinagawa Add

        /// <summary>
        /// フローコードの接頭辞を取得します。
        /// </summary>
        public static string FlowCodePrefix { get; } = "F";

        /// <summary>
        /// 法規制マスターのGHS区分カテゴリの法規制タイプIDを取得します。
        /// </summary>
        public static int GhsCategoryTypeId { get; } = 1;

        /// <summary>
        /// 法規制マスターの法規制カテゴリの法規制タイプIDを取得します。
        /// </summary>
        public static int RegulationTypeId { get; } = 2;

        /// <summary>
        /// 場所マスターの保管場所カテゴリの場所タイプIDを取得します。
        /// </summary>
        public static int StoringLocationTypeId { get; } = 1;

        /// <summary>
        /// 場所マスターの使用場所カテゴリの場所タイプIDを取得します。
        /// </summary>
        public static int UsageLocationTypeId { get; } = 2;

        /// <summary>
        /// 同時に受入れる最大数を取得します。
        /// </summary>
        public static int MaxAcceptNumber { get; } = 100;

        /// <summary>
        /// 未登録の化学物質コードを定義します。
        /// </summary>
        public static string UnregisteredChemCd = "C000000000";

        /// <summary>
        /// 未登録の化学物質名を定義します。
        /// </summary>
        public static string UnregisteredChemName = "未登録";

        public static string ManagementLedgerRegisterNumberPrefix { get; } = "K";

        /// <summary>
        /// 選択項目で「その他」を定義します。
        /// </summary>
        public static string SelectedOther { get; } = "その他";

        /// <summary>
        /// リスト出力(CSV)時の拡張子を定義します。
        /// </summary>
        public static string CsvFileNameSuffix { get; } = ".csv";

        /// <summary>
        /// リスト出力時のタイムスタンプのフォーマットを定義します。
        /// </summary>
        public static string CsvFileNameSuffixPlusTimeStamp { get; } = "_{0:yyyyMMddHHmmss}.csv";

        /// <summary>
        /// Phase2適応前の在庫を管理台帳管理対象から除外するためにPhase2導入日を定義します。
        /// </summary>
        public static string Phase2StartDate { get; } = "2018/02/19";
    }

    /// <summary>
    /// 化学物質検索対象
    /// </summary>
    public enum ChemSearchTarget
    {
        /// <summary>
        /// 化学物質
        /// </summary>
        Chem = 0,
        /// <summary>
        /// 商品
        /// </summary>
        Product = 1
    }

    /// <summary>
    /// 鍵管理
    /// </summary>
    public enum KeyManagement
    {
        /// <summary>
        /// 推奨
        /// </summary>
        Recommendation = 1,

        /// <summary>
        /// 必須
        /// </summary>
        Required
    }

    /// <summary>
    /// 
    /// </summary>
    public enum StockSearchTarget
    {
        /// <summary>
        /// 在庫ID
        /// </summary>
        stockId = 0,

        /// <summary>
        /// 瓶番号
        /// </summary>
        barcode
    }
}