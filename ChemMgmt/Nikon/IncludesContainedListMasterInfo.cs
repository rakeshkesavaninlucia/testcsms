﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Nikon
{
    /// <summary>
    /// 化学物質含有物検索条件
    /// </summary>
    public enum ChemSearchContained
    {
        /// <summary>
        /// 含めない
        /// </summary>
        Without,
        /// <summary>
        /// 含める
        /// </summary>
        With
    }

    /// <summary>
    /// 含有物を含めるかの選択肢を取得するクラスです。
    /// </summary>
    public class IncludesContainedListMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        /// <summary>
        /// 選択項目に空白を追加するか
        /// </summary>
        protected override bool IsAddSpace { get; set; } = false;

        /// <summary>
        /// 全選択肢を取得します。
        /// </summary>
        /// <returns>全選択肢を<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var searchCondition = new List<CommonDataModel<int?>>();
            searchCondition.Add(new CommonDataModel<int?>((int)ChemSearchContained.Without, "含めない", 0));
            searchCondition.Add(new CommonDataModel<int?>((int)ChemSearchContained.With, "含める", 1));
            return searchCondition;
        }
    }
}