﻿using System.Web;
using System.Web.Optimization;

namespace ChemMgmt
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery141").Include(
                        "~/Scripts/jquery-1.4.1.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery141min").Include(
                   "~/Scripts/jquery-1.4.1.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery1102").Include(
            "~/Scripts/jquery-1.10.2.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery1124").Include(
            "~/Scripts/jquery-1.12.4.js"));
            bundles.Add(new ScriptBundle("~/bundles/jquery223").Include(
            "~/Scripts/jquery-2.2.3.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery331").Include(
            "~/Scripts/jquery-3.3.1.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));
            bundles.Add(new ScriptBundle("~/bundles/printlabelscript").Include(
        "~/Scripts/LabelPrint.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryUI").Include(
"~/Scripts/jquery-ui.js"));
            bundles.Add(new ScriptBundle("~/bundles/Totalcsmsscript").Include(
                         "~/Scripts/LabelPrint.js",
                         "~/Scripts/objectExtensions.js",
                         "~/Scripts/jquery.blockUI.js",
                         "~/Scripts/wait.js",
                         "~/Scripts/zycg.js",
                         "~/Scripts/warningMessages.js"));
            bundles.Add(new ScriptBundle("~/bundles/PopUp").Include(
             "~/Scripts/jquery-2.2.3.min.js",
                         "~/Scripts/jquery.dynatree.js",
                         "~/Scripts/jquery.js",
                         "~/Scripts/jquery-ui.custom.js",
                         "~/Scripts/jquery.cookie.js",
                         "~/Scripts/zycg.js"));
            bundles.Add(new ScriptBundle("~/bundles/dynatreescript").Include(
            "~/Scripts/jquery.dynatree.js"));
            bundles.Add(new ScriptBundle("~/bundles/Errorcsmsscript").Include(
                   "~/Scripts/zycg.js"
                   ));
            bundles.Add(new ScriptBundle("~/bundles/Logincsmsscript").Include(
            "~/Scripts/jquery.blockUI.js",
            "~/Scripts/wait.js"
            /*"~/Scripts/zycg.js"*/));
            bundles.Add(new ScriptBundle("~/bundles/RegistrationScripts").Include(
           "~/Scripts/jquery-2.2.3.min.js",
           "~/Scripts/jquery-ui.js"));
            bundles.Add(new ScriptBundle("~/bundles/ContentScripts").Include(
          "~/Scripts/jquery-1.4.1.min.js",
            //"~/Scripts/jquery-ui.js",
            "~/Scripts/jquery.blockUI.js",
            "~/Scripts/wait.js"));
            bundles.Add(new ScriptBundle("~/bundles/DataTableScripts").Include(
            "~/Scripts/jquery-1.8.3.min.js",
            "~/Scripts/jquery.dataTables.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                    "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/customDynatree").Include(
                      "~/Content/Css/ui.customDynatree.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/Css/bootstrap.css",
                      "~/Content/Css/site.css"));

            bundles.Add(new StyleBundle("~/Content/csmscss").Include(
                       "~/Content/Css/Style.css",
                    "~/Content/Css/StyleSheet.css",
                   "~/Content/Css/jquery-ui.min.css",
                   "~/Content/Css/jquery-ui.css",
                    "~/Content/Css/autoSuggest.css",
                    "~/Content/Css/ui.*"));
            bundles.Add(new StyleBundle("~/Content/ZC01021").Include(
           "~/Content/Css/Style.css",
        "~/Content/Css/StyleSheet.css"));
            bundles.Add(new StyleBundle("~/Content/Totalcsmscss").Include(
                      "~/Content/Css/style.css",
                      "~/Content/Css/Site.css",
                      "~/Content/Css/StyleSheet.css",
                      "~/Content/Css/jquery-ui.min.css",
                      "~/Content/Css/autoSuggest.css",
                      "~/Content/Css/webgrid.css",
                      "~/Content/Css/ui.*"));
            bundles.Add(new StyleBundle("~/Content/webgrid").Include(
         "~/Content/Css/webgrid.css"));


            bundles.Add(new ScriptBundle("~/bundles/jqueryLedger30").Include(
                    "~/Scripts/jquery-1.4.1.js",
                     "~/Scripts/jquery-1.12.4.js",
                      "~/Scripts/jquery-ui.js"));


            bundles.Add(new StyleBundle("~/Content/WebgridLedger").Include(
        "~/Content/Css/WebgridLedger.css"));



            bundles.Add(new ScriptBundle("~/bundles/Datatable").Include(
                                 "~/Scripts/Datatable.js"));
        }
    }
}
