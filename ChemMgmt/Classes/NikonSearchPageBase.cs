﻿using ChemMgmt;
using ChemMgmt.Classes.util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Classes
{
    public abstract partial class NikonSearchPageBase : NikonZyCoaGMasterPageBase
    {
        string GridResultCount = string.Empty;
        protected virtual string CsvFileName { get; } = "化学物質マスター.csv";

        protected virtual bool ExistsListOutputFeature { get; } = true;

        protected bool IsSort { get; set; } = true;

        public bool IsListOutputProcess { get; set; } = false;

        protected string SearchConditionCsv { get; set; } = string.Empty;

        public int MaxSearchResultCount { get; set; }

        public bool IsMaxSerchResultCountOver { get; set; } = false;

        protected bool IsNoCheckSearch { get; set; } = true;

        protected abstract int GetMaxSearchResultCount();
        /// <summary>
        /// 検索ボタンが押されたか
        /// </summary>
        protected bool IsSearchButton { get; set; } = false;
        public string SearchCountFormat { get; set; } = "件数:0件"; //this property belongs to Definition class
        public virtual void ViewSearchResultCount(int resultCount)
        {
            MaxSearchResultCount = GetMaxSearchResultCount();
            if (resultCount <= MaxSearchResultCount)
            {
                GridResultCount = resultCount.ToString(SearchCountFormat);
            }
            else if (IsNoCheckSearch)
            {
                GridResultCount = MaxSearchResultCount.ToString(SearchCountFormat);
            }
            else
            {
                var message = ExistsListOutputFeature ? InfoMessages.MaxSearchCountOver : InfoMessages.MaxSearchCountOverNoCsv;
                if (IsSearchButton)
                {
                    LogUtil logUtil = new LogUtil();
                    IsMaxSerchResultCountOver = true;
                }
            }
            HttpContext.Current.Session["FixedCount"] = MaxSearchResultCount;
        }
    }
}