﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Classes
{
    /// <summary>
    /// システム画面パスを定義します。
    /// </summary>
    public static class SystemFormPath
    {
        /// <summary>
        /// ログイン
        /// </summary>
        public static string Login { get; } = "~/ZC010/ZC01001";

        /// <summary>
        /// メニュー
        /// </summary>
        public static string Menu { get; } = "~/ZC010/ZC01011";
        
        /// <summary>
        /// 選択
        /// </summary>
        public static string Selection { get; } = "../ZC010/ZC01081";

        /// <summary>
        /// ダウンロード
        /// </summary>
        public static string Download { get; } = "../ZC010/ZC01091";

        /// <summary>
        /// ユーザー設定
        /// </summary>
        public static string UserConfig { get; } = "~/ZC010/ZC01022";

        /// <summary>
        /// 在庫検索
        /// </summary>
        public static string StockSearch { get; } = "~/ZC020/ZC02002";

        /// <summary>
        /// 在庫メンテナンス(修正)
        /// </summary>
        public static string StockEdit { get; } = "~/ZC020/ZC02021";

        /// <summary>
        /// 在庫管理
        /// </summary>
        public static string StockControl { get; } = "~/ZC040/ZC04001";

        /// <summary>
        /// 使用履歴(単一)
        /// </summary>
        public static string UsageHistory { get; } = "~/ZC010/ZC01041";
    }
}