﻿using ChemMgmt.Classes.Extensions;
using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;

namespace ChemMgmt.Classes
{
    public abstract class DataInfoBase<T> : IDisposable
    {
        protected DbContext _Context;
        public DbContext Context
        {
            protected get
            {
                return _Context;
            }
            set
            {
                IsInteriorTransaction = false;
                _Context = value;
            }
        }

        public SqlConnection _SQLContext;
        public SqlConnection SQLContext
        {
            protected get
            {
                return _SQLContext;
            }
            set
            {
                IsInteriorTransaction = false;
                _SQLContext = value;
            }
        }

        SqlConnection sqlcontext = DbUtil.Open();

        private StringBuilder csv { get; set; }
        public string Csv
        {
            get
            {
                return csv.ToString();
            }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                {
                    csv = new StringBuilder();
                }
                if (!string.IsNullOrWhiteSpace(value))
                {
                    csv.AppendLine(value);
                }
            }
        }

        protected virtual bool IsInteriorTransaction { get; set; } = true;

        protected abstract string TableName { get; }

        protected abstract string FromWardCreate(dynamic dynamicCondition);

        protected abstract string WhereWardCreate(dynamic dynamicCondition, out DynamicParameters param);
        private IEnumerable<CommonDataModel<int?>> makerSelectListMak { get; set; }
        public virtual List<int> lstGetSearchResultCount(dynamic condition)
        {

            var sql = new StringBuilder();
            DynamicParameters param = new DynamicParameters();

            sql.AppendLine("select");
            sql.AppendLine(" count(*)");
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out param));
            StartTransaction();
            List<int> count = DbUtil.Select<int>(sql.ToString(), param).ToList();
            return count;
        }

        public virtual List<int> GetSearchResultCount(dynamic condition)
        {

            var sql = new StringBuilder();
            var sqlRowCount = new StringBuilder();
            DynamicParameters param = null;
            DynamicParameters param1 = null;


            sqlRowCount.AppendLine("select");
            sqlRowCount.AppendLine(" count(*)");
            sqlRowCount.AppendLine(FromWardCreate(condition));
            sqlRowCount.AppendLine(WhereWardCreate(condition, out param1));
            List<int> count = DbUtil.Select<int>(sqlRowCount.ToString(), param1);


            sql.AppendLine("select top(501)");
            sql.AppendLine(" M_CHEM.CHEM_CD,M_CHEM.CHEM_NAME,M_CHEM.CHEM_CAT,M_CHEM.DENSITY,M_CHEM.FLASH_POINT,M_CHEM.EXPIRATION_DATE,M_CHEM.KEY_MGMT,M_CHEM.WEIGHT_MGMT,M_CHEM.FIGURE,");
            sql.AppendLine(" M_CHEM.CAS_NO,M_CHEM.MEMO,M_CHEM.OPTIONAL1, M_CHEM.OPTIONAL2,M_CHEM.OPTIONAL3,M_CHEM.OPTIONAL4, M_CHEM.OPTIONAL5,M_CHEM.REG_USER_CD,M_CHEM.UPD_USER_CD,M_CHEM.DEL_USER_CD,");
            sql.AppendLine(" M_CHEM.REG_DATE,M_CHEM.UPD_DATE,M_CHEM.DEL_FLAG,M_CHEM.DEL_DATE,M_CHEM.SDS,M_CHEM.SDS_MIMETYPE,M_CHEM.SDS_FILENAME,M_CHEM.SDS_URL AS SDS_URL2,M_CHEM.SDS_UPD_DATE SDS_UPD_DATE2,M_CHEM_PRODUCT.UNITSIZE_ID,M_CHEM_PRODUCT.MAKER_ID, ");
            sql.AppendLine(" V_CHEM_GHSCAT.GHS_ICON_PATH,V_CHEM_REGULATION.REG_ACRONYM,V_CHEM_FIRE.FIRE_ACRONYM,V_CHEM_OFFICE.OFFICE_ACRONYM,");
            sql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_SEQ,M_CHEM_PRODUCT.PRODUCT_NAME,M_CHEM_PRODUCT.UNITSIZE,M_CHEM_PRODUCT.PRODUCT_BARCODE,");   //2019/04/02 TPE.Rin Add
            sql.AppendLine(" M_CHEM_PRODUCT.SDS_URL,M_CHEM_PRODUCT.SDS_UPD_DATE,M_CHEM.REF_CHEM_CD,M_CHEM.FIRE_FLAG,");

            sql.AppendLine("(CASE WHEN V_CHEM_REGULATION.EXAMINATION=NULL THEN '非該当' WHEN V_CHEM_REGULATION.EXAMINATION='1' THEN '該当' ELSE '非該当' END) AS EXAMINATION,");
            sql.AppendLine("(CASE WHEN V_CHEM_REGULATION.MEASUREMENT = NULL THEN '非該当' WHEN V_CHEM_REGULATION.MEASUREMENT = '1' THEN '該当' ELSE '非該当' END) AS MEASUREMENT,");

            sql.AppendLine("(CASE WHEN isnull((select M_MAKER.DEL_FLAG FROM M_MAKER WHERE MAKER_ID = M_CHEM_PRODUCT.MAKER_ID),0) = 0 THEN 1 ELSE (select M_MAKER.DEL_FLAG FROM M_MAKER WHERE MAKER_ID = M_CHEM_PRODUCT.MAKER_ID) END) MAKER_NAME_DEL_FLAG , ");
            sql.AppendLine("(CASE  WHEN  isnull((select M_UNITSIZE.DEL_FLAG FROM M_UNITSIZE WHERE UNITSIZE_ID = M_CHEM_PRODUCT.UNITSIZE_ID),0) = 0 THEN 1 ELSE (select M_UNITSIZE.DEL_FLAG FROM M_UNITSIZE WHERE UNITSIZE_ID = M_CHEM_PRODUCT.UNITSIZE_ID) END) AS UNITSIZE_NAME_DEL_FLAG, ");

            sql.AppendLine(" (select M_UNITSIZE.UNIT_NAME FROM M_UNITSIZE WHERE UNITSIZE_ID = M_CHEM_PRODUCT.UNITSIZE_ID) AS UNITSIZE_NAME,");
            sql.AppendLine("(SELECT DISPLAY_VALUE FROM M_COMMON WHERE TABLE_NAME = 'M_KEY_MGMT' AND KEY_VALUE = M_CHEM.KEY_MGMT) AS KEY_MGMT_NAME,");
            sql.AppendLine("(SELECT DISPLAY_VALUE FROM M_COMMON WHERE TABLE_NAME = 'M_FIGURE' AND KEY_VALUE = M_CHEM.FIGURE) AS FIGURE_NAME,");
            sql.AppendLine("(SELECT  MAKER_NAME FROM M_MAKER WHERE MAKER_ID = M_CHEM_PRODUCT.MAKER_ID) AS MAKER_NAME,");
            sql.AppendLine("(SELECT DISPLAY_VALUE FROM M_COMMON WHERE TABLE_NAME = 'M_WEIGHT_MGMT' AND KEY_VALUE = M_CHEM.WEIGHT_MGMT) AS WEIGHT_MGMT_NAME,");
            sql.AppendLine("(SELECT TOP 1 USER_NAME FROM M_USER WHERE USER_CD = M_CHEM.REG_USER_CD) AS REG_USER_CD,");
            sql.AppendLine("(SELECT TOP 1 USER_NAME FROM M_USER WHERE USER_CD = M_CHEM.UPD_USER_CD) AS UPD_USER_CD,");
            sql.AppendLine("(SELECT TOP 1 USER_NAME FROM M_USER WHERE USER_CD = M_CHEM.DEL_USER_CD) AS DEL_USER_CD,");
            //sql.AppendLine("(select convert(varchar, M_CHEM.SDS_UPD_DATE, 105)) AS SDS_UPD_DATE2, ");
            sql.AppendLine("(CASE WHEN M_CHEM.DEL_FLAG=0 THEN '使用中' WHEN M_CHEM.DEL_FLAG=1 THEN '削除済み' WHEN M_CHEM.DEL_FLAG=2 THEN '保存中' WHEN M_CHEM.DEL_FLAG=3 THEN '全て' END) AS USAGE_STATUS_NAME ");
            sql.AppendLine(FromWardCreate(condition));
            sql.AppendLine(WhereWardCreate(condition, out param));
            sql.AppendLine(" order by  CHEM_CD asc");
            List<CsmsCommonInfo> SearchResultData = DbUtil.Select<CsmsCommonInfo>(sql.ToString(), param);
            HttpContext.Current.Session.Add("SearchResultRows", SearchResultData);

            makerSelectListMak = new MakerMasterInfo().GetSelectList();
            SearchResultData[0].MAKER_NAME_DEL_FLAG = makerSelectListMak.Any(x => x.Id == SearchResultData[0].MAKER_ID);
            return count;
        }

        public virtual List<int> GetSearchResultValuesCount(dynamic condition)
        {

            var sql = new StringBuilder();
            var sqlRowCount = new StringBuilder();
            DynamicParameters param1 = null;

            sqlRowCount.AppendLine("select");
            sqlRowCount.AppendLine(" count(*)");
            sqlRowCount.AppendLine(FromWardCreate(condition));
            sqlRowCount.AppendLine(WhereWardCreate(condition, out param1));
            List<int> count = DbUtil.Select<int>(sqlRowCount.ToString(), param1);
            return count;
        }

        public bool IsValid { get; protected set; }

        public List<string> ErrorMessages { get; private set; }

        public DataInfoBase()
        {
            InitializeDictionary();
            ErrorMessages = new List<string>();
            csv = new StringBuilder();
        }

        protected abstract void InitializeDictionary();

        /// <summary>
        /// 化学物質個別エラーチェック 2018/5/10 Add
        /// </summary>
        /// <param name="value"></param>
        public void ValidateDataSingle(T value, string checkName, int mode, int? rowNumber = null)//2018/08/14 TPE.Sugimoto Upd
        {
            var rowNumberString = string.Empty;
            if (rowNumber.HasValue) rowNumberString = rowNumber.ToString() + "行目:";

            var properties = value.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.CanRead) continue;

                var notVerifyValue = (NotVerifyValueAttribute)Attribute.GetCustomAttribute(property, typeof(NotVerifyValueAttribute));
                if (notVerifyValue != null)
                {
                    continue;
                }

                var displayName = property.Name;

                var stringValue = property.GetValue(value)?.ToString();

                if (stringValue == null)
                {
                    stringValue = string.Empty;
                }
                else
                {
                    stringValue = property.GetValue(value)?.ToString();
                }

                var display = (DisplayAttribute)Attribute.GetCustomAttribute(property, typeof(DisplayAttribute));
                if (display != null) displayName = display.Name;

                if (displayName.ToUpper() != checkName.ToUpper()) continue;

                var required = (RequiredAttribute)Attribute.GetCustomAttribute(property, typeof(RequiredAttribute));
                if (required != null)
                {
                    if (string.IsNullOrWhiteSpace(stringValue))
                    {
                        IsValid = false;
                        if (!string.IsNullOrEmpty(required.ErrorMessage))
                        {
                            ErrorMessages.Add(rowNumberString + string.Format(required.ErrorMessage, displayName));
                        }
                        else
                        {
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.Required, displayName));
                        }
                    }
                }

                var length = (StringLengthAttribute)Attribute.GetCustomAttribute(property, typeof(StringLengthAttribute));
                if (length != null)
                {
                    if (stringValue == null || stringValue.Length < length.MinimumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MinimumLength, displayName, length.MinimumLength.ToString()));
                    }
                    if (stringValue != null && stringValue.Length > length.MaximumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MaximumLength, displayName, length.MaximumLength.ToString()));
                    }
                }

                var halfWidth = (HalfwidthAttribute)Attribute.GetCustomAttribute(property, typeof(HalfwidthAttribute));
                if (stringValue != null && halfWidth != null)
                {
                    var matchCollection = System.Text.RegularExpressions.Regex.Matches(stringValue, halfWidth.Pattern);
                    if (!string.IsNullOrWhiteSpace(stringValue) && (matchCollection.Count != 1 || !(stringValue.Length == matchCollection[0].Length)))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotHalfwidth, displayName));
                    }
                }

                var isValidNumber = true;
                var number = (NumberAttribute)Attribute.GetCustomAttribute(property, typeof(NumberAttribute));
                if (stringValue != null && number != null)
                {
                    var dec1 = default(decimal);
                    var dec2 = default(decimal);
                    var str1 = string.Empty;
                    var str2 = string.Empty;
                    if (stringValue.Length > 28)
                    {
                        str1 = stringValue.Substring(0, 28);
                        str2 = stringValue.Substring(28);
                    }
                    else
                    {
                        str1 = stringValue;
                        str2 = "0";
                    }
                    if (!string.IsNullOrWhiteSpace(stringValue) && (!decimal.TryParse(str1, out dec1) || !decimal.TryParse(str2, out dec2)))
                    {
                        isValidNumber = false;
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotNumber, displayName));
                    }
                    else if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var regularNumber = (RegularNumberAttribute)Attribute.GetCustomAttribute(property, typeof(RegularNumberAttribute));
                        if (regularNumber != null)
                        {
                            if (property.Name == "MEASURES_BEFORE_SCORE" || property.Name == "MEASURES_AFTER_SCORE")
                            {
                                // 措置前点数と措置後点数は数値「０」を許可
                            }
                            else if (dec1 <= 0)
                            {
                                isValidNumber = false;
                                IsValid = false;
                                ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotRegularNumber, displayName));
                            }
                        }
                    }
                }

                var numberLength = (NumberLengthAttribute)Attribute.GetCustomAttribute(property, typeof(NumberLengthAttribute));
                if (numberLength != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var numberArray = stringValue.Replace("-", string.Empty).Replace(",", string.Empty).Split(".".ToCharArray());
                        if (numberLength.integerNumberLength > 0 && numberArray[0].Length > numberLength.integerNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumberLength, displayName, numberLength.integerNumberLength.ToString()));
                        }
                        if (numberLength.decimalNumberLength == 0 && numberArray.Length == 2)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumber, displayName));
                        }
                        else if (numberArray.Length == 2 && numberArray[1].Length > numberLength.decimalNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.DecimalNumberLength, displayName, numberLength.decimalNumberLength.ToString()));
                        }
                    }
                }

                var range = (RangeAttribute)Attribute.GetCustomAttribute(property, typeof(RangeAttribute));
                if (range != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var dec = Convert.ToDecimal(stringValue);
                        if (dec < Convert.ToDecimal(range.Minimum) || Convert.ToDecimal(range.Maximum) < dec)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ThresholdValueRangeOver, displayName, range.Minimum.ToString(), range.Maximum.ToString()));
                        }
                    }
                }

                var date = (DateAttribute)Attribute.GetCustomAttribute(property, typeof(DateAttribute));
                if (date != null)
                {
                    DateTime datetime = default(DateTime);
                    if (!string.IsNullOrWhiteSpace(stringValue) && !DateTime.TryParse(stringValue, out datetime))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotDate, displayName));
                    }
                }

                var permitAllCharacter = (ExcludingProhibitionAttribute)Attribute.GetCustomAttribute(property, typeof(ExcludingProhibitionAttribute));
                if (stringValue != null && permitAllCharacter != null)
                {
                    var p = stringValue.ExtractProhibited();
                    if (p.Count() > 0)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ContainProhibitionCharacter, displayName, string.Join(" ", p).Replace(@"\", @"\\")));
                    }
                }

                var smallCompare = (SmallCompareAttribute)Attribute.GetCustomAttribute(property, typeof(SmallCompareAttribute));
                if (smallCompare != null)
                {
                    var compareProperty = properties.Single(x => x.Name == smallCompare.OtherProperty);
                    var compareDisplayName = compareProperty.Name;

                    var compareValue = compareProperty.GetValue(value)?.ToString();

                    var compareDisplay = (DisplayAttribute)Attribute.GetCustomAttribute(compareProperty, typeof(DisplayAttribute));
                    if (compareDisplay != null) compareDisplayName = compareDisplay.Name;

                    var dec1 = default(decimal);
                    var dec2 = default(decimal);

                    if (decimal.TryParse(stringValue, out dec1) && decimal.TryParse(compareValue, out dec2))
                    {
                        if (decimal.Compare(dec1, dec2) > 0)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ValueIsLarge, displayName, compareDisplayName));
                        }
                    }
                }

                var unique = (UniqueAttribute)Attribute.GetCustomAttribute(property, typeof(UniqueAttribute));
                if (unique != null)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        //StartTransaction();

                        var sql = new StringBuilder();
                        sql.AppendLine("select");
                        sql.AppendLine(" " + property.Name);
                        sql.AppendLine("from");
                        sql.AppendLine(" " + TableName);
                        sql.AppendLine("where");
                        sql.AppendLine(" not(" + getKeyWhere(value) + ")");
                        sql.AppendLine(" and");
                        sql.AppendLine(" " + property.Name + " = @" + property.Name);

                        DynamicParameters dapperparameter = new DynamicParameters();
                        var keyColumns = getKeyProperty(value);
                        var parameters = keyColumns;
                        if (!parameters.ContainsKey(property.Name)) parameters.Add(property.Name, stringValue);

                        foreach (var dappervalues in parameters)
                        {
                            dapperparameter.Add("@" + dappervalues.Key, dappervalues.Value);
                        }

                        List<object> records = DbUtil.Select<object>(sql.ToString(), dapperparameter);
                        if (records.Count > 0)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotUnique, displayName, stringValue));
                        }
                    }
                }
            }
        }

        protected void StartTransaction()
        {
            if (_Context == null)
            {
                _Context = DbUtil.Open(true);
            }
        }

        // 20180723 FJ)Shinagawa Add Start ->
        /// <summary>
        /// 化学物質個別エラーチェック
        /// 桁数チェック、禁則文字チェックのみ
        /// </summary>
        /// <param name="value"></param>
        public void ValidateDataSingleSimple(T value, string checkName, int? rowNumber = null)
        {
            var rowNumberString = string.Empty;
            if (rowNumber.HasValue) rowNumberString = rowNumber.ToString() + "行目:";

            var properties = value.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.CanRead) continue;

                var notVerifyValue = (NotVerifyValueAttribute)Attribute.GetCustomAttribute(property, typeof(NotVerifyValueAttribute));
                if (notVerifyValue != null)
                {
                    continue;
                }

                var displayName = property.Name;


                var stringValue = property.GetValue(value)?.ToString();

                var display = (DisplayAttribute)Attribute.GetCustomAttribute(property, typeof(DisplayAttribute));
                if (display != null) displayName = display.Name;

                if (displayName.ToUpper() != checkName.ToUpper()) continue;

                var length = (StringLengthAttribute)Attribute.GetCustomAttribute(property, typeof(StringLengthAttribute));
                if (length != null)
                {
                    if (stringValue == null || stringValue.Length < length.MinimumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MinimumLength, displayName, length.MinimumLength.ToString()));
                    }
                    if (stringValue != null && stringValue.Length > length.MaximumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MaximumLength, displayName, length.MaximumLength.ToString()));
                    }
                }

                var halfWidth = (HalfwidthAttribute)Attribute.GetCustomAttribute(property, typeof(HalfwidthAttribute));
                if (stringValue != null && halfWidth != null)
                {
                    var matchCollection = System.Text.RegularExpressions.Regex.Matches(stringValue, halfWidth.Pattern);
                    if (!string.IsNullOrWhiteSpace(stringValue) && (matchCollection.Count != 1 || !(stringValue.Length == matchCollection[0].Length)))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotHalfwidth, displayName));
                    }
                }

                var isValidNumber = true;
                var number = (NumberAttribute)Attribute.GetCustomAttribute(property, typeof(NumberAttribute));
                if (stringValue != null && number != null)
                {
                    var dec1 = default(decimal);
                    var dec2 = default(decimal);
                    var str1 = string.Empty;
                    var str2 = string.Empty;
                    if (stringValue.Length > 28)
                    {
                        str1 = stringValue.Substring(0, 28);
                        str2 = stringValue.Substring(28);
                    }
                    else
                    {
                        str1 = stringValue;
                        str2 = "0";
                    }
                    if (!string.IsNullOrWhiteSpace(stringValue) && (!decimal.TryParse(str1, out dec1) || !decimal.TryParse(str2, out dec2)))
                    {
                        isValidNumber = false;
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotNumber, displayName));
                    }
                    else if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var regularNumber = (RegularNumberAttribute)Attribute.GetCustomAttribute(property, typeof(RegularNumberAttribute));
                        if (regularNumber != null)
                        {
                            if (property.Name == "MEASURES_BEFORE_SCORE" || property.Name == "MEASURES_AFTER_SCORE")
                            {
                                // 措置前点数と措置後点数は数値「０」を許可
                            }
                            else if (dec1 <= 0)
                            {
                                isValidNumber = false;
                                IsValid = false;
                                ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotRegularNumber, displayName));
                            }
                        }
                    }
                }

                var numberLength = (NumberLengthAttribute)Attribute.GetCustomAttribute(property, typeof(NumberLengthAttribute));
                if (numberLength != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var numberArray = stringValue.Replace("-", string.Empty).Replace(",", string.Empty).Split(".".ToCharArray());
                        if (numberLength.integerNumberLength > 0 && numberArray[0].Length > numberLength.integerNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumberLength, displayName, numberLength.integerNumberLength.ToString()));
                        }
                        if (numberLength.decimalNumberLength == 0 && numberArray.Length == 2)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumber, displayName));
                        }
                        else if (numberArray.Length == 2 && numberArray[1].Length > numberLength.decimalNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.DecimalNumberLength, displayName, numberLength.decimalNumberLength.ToString()));
                        }
                    }
                }

                var range = (RangeAttribute)Attribute.GetCustomAttribute(property, typeof(RangeAttribute));
                if (range != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var dec = Convert.ToDecimal(stringValue);
                        if (dec < Convert.ToDecimal(range.Minimum) || Convert.ToDecimal(range.Maximum) < dec)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ThresholdValueRangeOver, displayName, range.Minimum.ToString(), range.Maximum.ToString()));
                        }
                    }
                }

                var date = (DateAttribute)Attribute.GetCustomAttribute(property, typeof(DateAttribute));
                if (date != null)
                {
                    DateTime datetime = default(DateTime);
                    if (!string.IsNullOrWhiteSpace(stringValue) && !DateTime.TryParse(stringValue, out datetime))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotDate, displayName));
                    }
                }

                var permitAllCharacter = (ExcludingProhibitionAttribute)Attribute.GetCustomAttribute(property, typeof(ExcludingProhibitionAttribute));
                if (stringValue != null && permitAllCharacter != null)
                {
                    var p = stringValue.ExtractProhibited();
                    if (p.Count() > 0)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ContainProhibitionCharacter, displayName, string.Join(" ", p).Replace(@"\", @"\\")));
                    }
                }

                var smallCompare = (SmallCompareAttribute)Attribute.GetCustomAttribute(property, typeof(SmallCompareAttribute));
                if (smallCompare != null)
                {
                    var compareProperty = properties.Single(x => x.Name == smallCompare.OtherProperty);
                    var compareDisplayName = compareProperty.Name;

                    var compareValue = compareProperty.GetValue(value)?.ToString();

                    var compareDisplay = (DisplayAttribute)Attribute.GetCustomAttribute(compareProperty, typeof(DisplayAttribute));
                    if (compareDisplay != null) compareDisplayName = compareDisplay.Name;

                    var dec1 = default(decimal);
                    var dec2 = default(decimal);

                    if (decimal.TryParse(stringValue, out dec1) && decimal.TryParse(compareValue, out dec2))
                    {
                        if (decimal.Compare(dec1, dec2) > 0)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ValueIsLarge, displayName, compareDisplayName));
                        }
                    }
                }
            }
        }
        // 20180723 FJ)Shinagawa Add End <-    

        public virtual string ValidateData(T value)
        {
            string message = ValidateDataEntity(value);
            return message;
        }

        public virtual void ValidateData(IEnumerable<T> values)
        {
            int i = 1;
            foreach (var value in values)
            {
                ValidateDataEntity(value, i);
                i++;
            }
        }

        protected virtual string ValidateDataEntity(T value, int? rowNumber = null)
        {
            string alertMessages = null;
            var rowNumberString = string.Empty;
            if (rowNumber.HasValue) rowNumberString = rowNumber.ToString() + "行目:";

            var properties = value.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.CanRead) continue;

                var notVerifyValue = (NotVerifyValueAttribute)Attribute.GetCustomAttribute(property, typeof(NotVerifyValueAttribute));
                if (notVerifyValue != null)
                {
                    continue;
                }

                var displayName = property.Name;

                var stringValue = property.GetValue(value)?.ToString();

                if (stringValue == null)
                {
                    stringValue = string.Empty;
                }
                else
                {
                    stringValue = property.GetValue(value)?.ToString();
                }
                var display = (DisplayAttribute)Attribute.GetCustomAttribute(property, typeof(DisplayAttribute));
                if (display != null) displayName = display.Name;

                var required = (RequiredAttribute)Attribute.GetCustomAttribute(property, typeof(RequiredAttribute));
                if (required != null)
                {
                    if (string.IsNullOrWhiteSpace(stringValue) || stringValue == "0")
                    {
                        IsValid = false;
                        if (!string.IsNullOrEmpty(required.ErrorMessage))
                        {
                            ErrorMessages.Add(rowNumberString + string.Format(required.ErrorMessage, displayName));
                        }
                        else
                        {
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.Required, displayName));
                        }
                    }
                }

                var length = (StringLengthAttribute)Attribute.GetCustomAttribute(property, typeof(StringLengthAttribute));
                if (length != null)
                {
                    if (stringValue == null || stringValue.Length < length.MinimumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MinimumLength, displayName, length.MinimumLength.ToString()));
                    }
                    if (stringValue != null && stringValue.Length > length.MaximumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MaximumLength, displayName, length.MaximumLength.ToString()));
                    }
                }

                var halfWidth = (HalfwidthAttribute)Attribute.GetCustomAttribute(property, typeof(HalfwidthAttribute));
                if (stringValue != null && halfWidth != null)
                {
                    var matchCollection = System.Text.RegularExpressions.Regex.Matches(stringValue, halfWidth.Pattern);
                    if (!string.IsNullOrWhiteSpace(stringValue) && (matchCollection.Count != 1 || !(stringValue.Length == matchCollection[0].Length)))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotHalfwidth, displayName));
                    }
                }

                var isValidNumber = true;
                var number = (NumberAttribute)Attribute.GetCustomAttribute(property, typeof(NumberAttribute));
                if (stringValue != null && number != null)
                {
                    var dec1 = default(decimal);
                    var dec2 = default(decimal);
                    var str1 = string.Empty;
                    var str2 = string.Empty;
                    if (stringValue.Length > 28)
                    {
                        str1 = stringValue.Substring(0, 28);
                        str2 = stringValue.Substring(28);
                    }
                    else
                    {
                        str1 = stringValue;
                        str2 = "0";
                    }
                    if (!string.IsNullOrWhiteSpace(stringValue) && (!decimal.TryParse(str1, out dec1) || !decimal.TryParse(str2, out dec2)))
                    {
                        isValidNumber = false;
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotNumber, displayName));
                    }
                    else if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var regularNumber = (RegularNumberAttribute)Attribute.GetCustomAttribute(property, typeof(RegularNumberAttribute));
                        if (regularNumber != null)
                        {
                            if (property.Name == "MEASURES_BEFORE_SCORE" || property.Name == "MEASURES_AFTER_SCORE")
                            {
                                // 措置前点数と措置後点数は数値「０」を許可
                            }
                            else if (dec1 <= 0)
                            {
                                isValidNumber = false;
                                IsValid = false;
                                ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotRegularNumber, displayName));
                            }
                        }
                    }
                }

                var numberLength = (NumberLengthAttribute)Attribute.GetCustomAttribute(property, typeof(NumberLengthAttribute));
                if (numberLength != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var numberArray = stringValue.Replace("-", string.Empty).Replace(",", string.Empty).Split(".".ToCharArray());
                        if (numberLength.integerNumberLength > 0 && numberArray[0].Length > numberLength.integerNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumberLength, displayName, numberLength.integerNumberLength.ToString()));
                        }
                        if (numberLength.decimalNumberLength == 0 && numberArray.Length == 2)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumber, displayName));
                        }
                        else if (numberArray.Length == 2 && numberArray[1].Length > numberLength.decimalNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.DecimalNumberLength, displayName, numberLength.decimalNumberLength.ToString()));
                        }
                    }
                }

                var range = (RangeAttribute)Attribute.GetCustomAttribute(property, typeof(RangeAttribute));
                if (range != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var dec = Convert.ToDecimal(stringValue);
                        if (dec < Convert.ToDecimal(range.Minimum) || Convert.ToDecimal(range.Maximum) < dec)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ThresholdValueRangeOver, displayName, range.Minimum.ToString(), range.Maximum.ToString()));
                        }
                    }
                }

                var date = (DateAttribute)Attribute.GetCustomAttribute(property, typeof(DateAttribute));
                if (date != null)
                {
                    DateTime datetime = default(DateTime);
                    if (!string.IsNullOrWhiteSpace(stringValue) && !DateTime.TryParse(stringValue, out datetime))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotDate, displayName));
                    }
                }

                var permitAllCharacter = (ExcludingProhibitionAttribute)Attribute.GetCustomAttribute(property, typeof(ExcludingProhibitionAttribute));
                if (stringValue != null && permitAllCharacter != null)
                {
                    var p = stringValue.ExtractProhibited();
                    if (p.Count() > 0)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ContainProhibitionCharacter, displayName, string.Join(" ", p).Replace(@"\", @"\\")));
                    }
                }

                var smallCompare = (SmallCompareAttribute)Attribute.GetCustomAttribute(property, typeof(SmallCompareAttribute));
                if (smallCompare != null)
                {
                    var compareProperty = properties.Single(x => x.Name == smallCompare.OtherProperty);
                    var compareDisplayName = compareProperty.Name;

                    var compareValue = compareProperty.GetValue(value)?.ToString();

                    var compareDisplay = (DisplayAttribute)Attribute.GetCustomAttribute(compareProperty, typeof(DisplayAttribute));
                    if (compareDisplay != null) compareDisplayName = compareDisplay.Name;

                    var dec1 = default(decimal);
                    var dec2 = default(decimal);

                    if (decimal.TryParse(stringValue, out dec1) && decimal.TryParse(compareValue, out dec2))
                    {
                        if (decimal.Compare(dec1, dec2) > 0)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ValueIsLarge, displayName, compareDisplayName));
                        }
                    }
                }

                var unique = (UniqueAttribute)Attribute.GetCustomAttribute(property, typeof(UniqueAttribute));
                if (unique != null)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {

                        var sql = new StringBuilder();
                        sql.AppendLine("select");
                        sql.AppendLine(" " + property.Name);
                        sql.AppendLine("from");
                        sql.AppendLine(" " + TableName);
                        sql.AppendLine("where");
                        sql.AppendLine(" not(" + getKeyWhere(value) + ")");
                        sql.AppendLine(" and");
                        sql.AppendLine(" " + property.Name + " = @" + property.Name);
                        DynamicParameters dapperparameters = new DynamicParameters();
                        var keyColumns = getKeyProperty(value);
                        var parameters = keyColumns;
                        if (!parameters.ContainsKey(property.Name)) parameters.Add(property.Name, stringValue);

                        foreach (var dappervalues in parameters)
                        {
                            dapperparameters.Add(dappervalues.Key, dappervalues.Value);
                        }
                        List<object> records = DbUtil.Select<object>(sql.ToString(), dapperparameters);
                        if (records.Count > 0)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotUnique, displayName, stringValue));
                        }
                    }
                }
            }

            if (ErrorMessages != null && ErrorMessages.Count > 0)
            {
                foreach (var items in ErrorMessages)
                {
                    alertMessages += items.ToString() + "\n";
                }

            }
            if (alertMessages != null)
            {
                alertMessages = alertMessages.TrimEnd('\n');
            }
            return alertMessages;
        }

        protected virtual string validateDataEntity(T value, int? rowNumber = null)
        {
            string alertMessages = null;
            var rowNumberString = string.Empty;
            if (rowNumber.HasValue) rowNumberString = rowNumber.ToString() + "行目:";

            var properties = value.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.CanRead) continue;

                var notVerifyValue = (NotVerifyValueAttribute)Attribute.GetCustomAttribute(property, typeof(NotVerifyValueAttribute));
                if (notVerifyValue != null)
                {
                    continue;
                }

                var displayName = property.Name;

                var stringValue = property.GetValue(value)?.ToString();

                if (stringValue == null)
                {
                    stringValue = string.Empty;
                }
                else
                {
                    stringValue = property.GetValue(value)?.ToString();
                }
                var display = (DisplayAttribute)Attribute.GetCustomAttribute(property, typeof(DisplayAttribute));
                if (display != null) displayName = display.Name;

                var required = (RequiredAttribute)Attribute.GetCustomAttribute(property, typeof(RequiredAttribute));
                if (required != null)
                {
                    if (string.IsNullOrWhiteSpace(stringValue))
                    {
                        IsValid = false;
                        if (!string.IsNullOrEmpty(required.ErrorMessage))
                        {
                            ErrorMessages.Add(rowNumberString + string.Format(required.ErrorMessage, displayName));
                        }
                        else
                        {
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.Required, displayName));
                        }
                    }
                }

                var length = (StringLengthAttribute)Attribute.GetCustomAttribute(property, typeof(StringLengthAttribute));
                if (length != null)
                {
                    if (stringValue == null || stringValue.Length < length.MinimumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MinimumLength, displayName, length.MinimumLength.ToString()));
                    }
                    if (stringValue != null && stringValue.Length > length.MaximumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MaximumLength, displayName, length.MaximumLength.ToString()));
                    }
                }

                var halfWidth = (HalfwidthAttribute)Attribute.GetCustomAttribute(property, typeof(HalfwidthAttribute));
                if (stringValue != null && halfWidth != null)
                {
                    var matchCollection = System.Text.RegularExpressions.Regex.Matches(stringValue, halfWidth.Pattern);
                    if (!string.IsNullOrWhiteSpace(stringValue) && (matchCollection.Count != 1 || !(stringValue.Length == matchCollection[0].Length)))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotHalfwidth, displayName));
                    }
                }

                var isValidNumber = true;
                var number = (NumberAttribute)Attribute.GetCustomAttribute(property, typeof(NumberAttribute));
                if (stringValue != null && number != null)
                {
                    var dec1 = default(decimal);
                    var dec2 = default(decimal);
                    var str1 = string.Empty;
                    var str2 = string.Empty;
                    if (stringValue.Length > 28)
                    {
                        str1 = stringValue.Substring(0, 28);
                        str2 = stringValue.Substring(28);
                    }
                    else
                    {
                        str1 = stringValue;
                        str2 = "0";
                    }
                    if (!string.IsNullOrWhiteSpace(stringValue) && (!decimal.TryParse(str1, out dec1) || !decimal.TryParse(str2, out dec2)))
                    {
                        isValidNumber = false;
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotNumber, displayName));
                    }
                    else if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var regularNumber = (RegularNumberAttribute)Attribute.GetCustomAttribute(property, typeof(RegularNumberAttribute));
                        if (regularNumber != null)
                        {
                            if (property.Name == "MEASURES_BEFORE_SCORE" || property.Name == "MEASURES_AFTER_SCORE")
                            {
                                // 措置前点数と措置後点数は数値「０」を許可
                            }
                            else if (dec1 <= 0)
                            {
                                isValidNumber = false;
                                IsValid = false;
                                ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotRegularNumber, displayName));
                            }
                        }
                    }
                }

                var numberLength = (NumberLengthAttribute)Attribute.GetCustomAttribute(property, typeof(NumberLengthAttribute));
                if (numberLength != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var numberArray = stringValue.Replace("-", string.Empty).Replace(",", string.Empty).Split(".".ToCharArray());
                        if (numberLength.integerNumberLength > 0 && numberArray[0].Length > numberLength.integerNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumberLength, displayName, numberLength.integerNumberLength.ToString()));
                        }
                        if (numberLength.decimalNumberLength == 0 && numberArray.Length == 2)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumber, displayName));
                        }
                        else if (numberArray.Length == 2 && numberArray[1].Length > numberLength.decimalNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.DecimalNumberLength, displayName, numberLength.decimalNumberLength.ToString()));
                        }
                    }
                }

                var range = (RangeAttribute)Attribute.GetCustomAttribute(property, typeof(RangeAttribute));
                if (range != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var dec = Convert.ToDecimal(stringValue);
                        if (dec < Convert.ToDecimal(range.Minimum) || Convert.ToDecimal(range.Maximum) < dec)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ThresholdValueRangeOver, displayName, range.Minimum.ToString(), range.Maximum.ToString()));
                        }
                    }
                }

                var date = (DateAttribute)Attribute.GetCustomAttribute(property, typeof(DateAttribute));
                if (date != null)
                {
                    DateTime datetime = default(DateTime);
                    if (!string.IsNullOrWhiteSpace(stringValue) && !DateTime.TryParse(stringValue, out datetime))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotDate, displayName));
                    }
                }

                var permitAllCharacter = (ExcludingProhibitionAttribute)Attribute.GetCustomAttribute(property, typeof(ExcludingProhibitionAttribute));
                if (stringValue != null && permitAllCharacter != null)
                {
                    var p = stringValue.ExtractProhibited();
                    if (p.Count() > 0)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ContainProhibitionCharacter, displayName, string.Join(" ", p).Replace(@"\", @"\\")));
                    }
                }

                var smallCompare = (SmallCompareAttribute)Attribute.GetCustomAttribute(property, typeof(SmallCompareAttribute));
                if (smallCompare != null)
                {
                    var compareProperty = properties.Single(x => x.Name == smallCompare.OtherProperty);
                    var compareDisplayName = compareProperty.Name;

                    var compareValue = compareProperty.GetValue(value)?.ToString();

                    var compareDisplay = (DisplayAttribute)Attribute.GetCustomAttribute(compareProperty, typeof(DisplayAttribute));
                    if (compareDisplay != null) compareDisplayName = compareDisplay.Name;

                    var dec1 = default(decimal);
                    var dec2 = default(decimal);

                    if (decimal.TryParse(stringValue, out dec1) && decimal.TryParse(compareValue, out dec2))
                    {
                        if (decimal.Compare(dec1, dec2) > 0)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ValueIsLarge, displayName, compareDisplayName));
                        }
                    }
                }

                var unique = (UniqueAttribute)Attribute.GetCustomAttribute(property, typeof(UniqueAttribute));
                if (unique != null)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {

                        var sql = new StringBuilder();
                        sql.AppendLine("select");
                        sql.AppendLine(" " + property.Name);
                        sql.AppendLine("from");
                        sql.AppendLine(" " + TableName);
                        sql.AppendLine("where");
                        sql.AppendLine(" not(" + getKeyWhere(value) + ")");
                        sql.AppendLine(" and");
                        sql.AppendLine(" " + property.Name + " = :" + property.Name);
                        DynamicParameters dapperparameters = new DynamicParameters();
                        var keyColumns = getKeyProperty(value);
                        var parameters = keyColumns;
                        if (!parameters.ContainsKey(property.Name)) parameters.Add(property.Name, stringValue);

                        foreach (var dappervalues in parameters)
                        {
                            dapperparameters.Add(dappervalues.Key, dappervalues.Value);
                        }
                        List<object> records = DbUtil.Select<object>(sql.ToString(), dapperparameters);
                        if (records.Count > 0)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotUnique, displayName, stringValue));
                        }
                    }
                }
            }

            if (ErrorMessages != null && ErrorMessages.Count > 0)
            {
                foreach (var items in ErrorMessages)
                {
                    alertMessages += items.ToString() + "\n";
                }

            }
            if (alertMessages != null)
            {
                alertMessages = alertMessages.TrimEnd('\n');
            }
            return alertMessages;
        }
        public virtual void ValidateDataSimple(IEnumerable<T> values)
        {
            int i = 1;
            foreach (var value in values)
            {
                ValidateDataEntitySimple(value, i);
                i++;
            }
        }
        protected virtual void ValidateDataEntitySimple(T value, int? rowNumber = null)
        {
            var rowNumberString = string.Empty;
            if (rowNumber.HasValue) rowNumberString = rowNumber.ToString() + "行目:";

            var properties = value.GetType().GetProperties();
            foreach (var property in properties)
            {
                if (!property.CanRead) continue;

                var notVerifyValue = (NotVerifyValueAttribute)Attribute.GetCustomAttribute(property, typeof(NotVerifyValueAttribute));
                if (notVerifyValue != null)
                {
                    continue;
                }

                var displayName = property.Name;

                var stringValue = property.GetValue(value)?.ToString();

                if (stringValue == null)
                {
                    stringValue = string.Empty;
                }
                else
                {
                    stringValue = property.GetValue(value)?.ToString();
                }
                var display = (DisplayAttribute)Attribute.GetCustomAttribute(property, typeof(DisplayAttribute));
                if (display != null) displayName = display.Name;

                var length = (StringLengthAttribute)Attribute.GetCustomAttribute(property, typeof(StringLengthAttribute));
                if (length != null)
                {
                    if (stringValue == null || stringValue.Length < length.MinimumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MinimumLength, displayName, length.MinimumLength.ToString()));
                    }
                    if (stringValue != null && stringValue.Length > length.MaximumLength)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.MaximumLength, displayName, length.MaximumLength.ToString()));
                    }
                }

                var halfWidth = (HalfwidthAttribute)Attribute.GetCustomAttribute(property, typeof(HalfwidthAttribute));
                if (stringValue != null && halfWidth != null)
                {
                    var matchCollection = System.Text.RegularExpressions.Regex.Matches(stringValue, halfWidth.Pattern);
                    if (!string.IsNullOrWhiteSpace(stringValue) && (matchCollection.Count != 1 || !(stringValue.Length == matchCollection[0].Length)))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotHalfwidth, displayName));
                    }
                }

                var isValidNumber = true;
                var number = (NumberAttribute)Attribute.GetCustomAttribute(property, typeof(NumberAttribute));
                if (stringValue != null && number != null)
                {
                    var dec1 = default(decimal);
                    var dec2 = default(decimal);
                    var str1 = string.Empty;
                    var str2 = string.Empty;
                    if (stringValue.Length > 28)
                    {
                        str1 = stringValue.Substring(0, 28);
                        str2 = stringValue.Substring(28);
                    }
                    else
                    {
                        str1 = stringValue;
                        str2 = "0";
                    }
                    if (!string.IsNullOrWhiteSpace(stringValue) && (!decimal.TryParse(str1, out dec1) || !decimal.TryParse(str2, out dec2)))
                    {
                        isValidNumber = false;
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotNumber, displayName));
                    }
                    else if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var regularNumber = (RegularNumberAttribute)Attribute.GetCustomAttribute(property, typeof(RegularNumberAttribute));
                        if (regularNumber != null)
                        {
                            if (property.Name == "MEASURES_BEFORE_SCORE" || property.Name == "MEASURES_AFTER_SCORE")
                            {
                                // 措置前点数と措置後点数は数値「０」を許可
                            }
                            else if (dec1 <= 0)
                            {
                                isValidNumber = false;
                                IsValid = false;
                                ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotRegularNumber, displayName));
                            }
                        }
                    }
                }

                var numberLength = (NumberLengthAttribute)Attribute.GetCustomAttribute(property, typeof(NumberLengthAttribute));
                if (numberLength != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var numberArray = stringValue.Replace("-", string.Empty).Replace(",", string.Empty).Split(".".ToCharArray());
                        if (numberLength.integerNumberLength > 0 && numberArray[0].Length > numberLength.integerNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumberLength, displayName, numberLength.integerNumberLength.ToString()));
                        }
                        if (numberLength.decimalNumberLength == 0 && numberArray.Length == 2)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.PositiveNumber, displayName));
                        }
                        else if (numberArray.Length == 2 && numberArray[1].Length > numberLength.decimalNumberLength)
                        {
                            isValidNumber = false;
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.DecimalNumberLength, displayName, numberLength.decimalNumberLength.ToString()));
                        }
                    }
                }

                var range = (RangeAttribute)Attribute.GetCustomAttribute(property, typeof(RangeAttribute));
                if (range != null && isValidNumber)
                {
                    if (!string.IsNullOrWhiteSpace(stringValue))
                    {
                        var dec = Convert.ToDecimal(stringValue);
                        if (dec < Convert.ToDecimal(range.Minimum) || Convert.ToDecimal(range.Maximum) < dec)
                        {
                            IsValid = false;
                            ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ThresholdValueRangeOver, displayName, range.Minimum.ToString(), range.Maximum.ToString()));
                        }
                    }
                }

                var date = (DateAttribute)Attribute.GetCustomAttribute(property, typeof(DateAttribute));
                if (date != null)
                {
                    DateTime datetime = default(DateTime);
                    if (!string.IsNullOrWhiteSpace(stringValue) && !DateTime.TryParse(stringValue, out datetime))
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.NotDate, displayName));
                    }
                }

                var permitAllCharacter = (ExcludingProhibitionAttribute)Attribute.GetCustomAttribute(property, typeof(ExcludingProhibitionAttribute));
                if (stringValue != null && permitAllCharacter != null)
                {
                    var p = stringValue.ExtractProhibited();
                    if (p.Count() > 0)
                    {
                        IsValid = false;
                        ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.ContainProhibitionCharacter, displayName, string.Join(" ", p).Replace(@"\", @"\\")));
                    }
                }
            }
        }
        public enum UpdateType
        {
            Add,
            Save,
            Delete,
            Undo
        }

        protected virtual string ValidateKeyData(T value, UpdateType updateType, int? rowNumber = null)
        {
            string Errormessage = string.Empty;
            var rowNumberString = string.Empty;
            if (rowNumber.HasValue) rowNumberString = rowNumber.ToString() + "行目:";

            var count = Select(value);
            if (count == 0)
            {
                if (updateType == UpdateType.Save || updateType == UpdateType.Delete || updateType == UpdateType.Undo)
                {
                    IsValid = false;
                    ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.KeyDoseNotExist));
                }
            }
            else if (count == 1)
            {
                if (updateType == UpdateType.Add)
                {
                    IsValid = false;
                    ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.KeyDuplicate));
                }
            }
            else
            {
                throw new AggregateException();
            }
            return Errormessage;
        }


        protected virtual string validateKeyData(T value, UpdateType updateType, int? rowNumber = null)
        {
            string Errormessage = string.Empty;
            var rowNumberString = string.Empty;
            if (rowNumber.HasValue) rowNumberString = rowNumber.ToString() + "行目:";

            var count = select(value);
            if (count == 0)
            {
                if (updateType == UpdateType.Save || updateType == UpdateType.Delete || updateType == UpdateType.Undo)
                {
                    IsValid = false;
                    ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.KeyDoseNotExist));
                }
            }
            else if (count == 1)
            {
                if (updateType == UpdateType.Add)
                {
                    IsValid = false;
                    ErrorMessages.Add(rowNumberString + string.Format(WarningMessages.KeyDuplicate));
                }
            }
            else
            {
                throw new AggregateException();
            }
            return Errormessage;
        }
        public virtual string Add(T value)
        {
            IsValid = true;
            //ErrorMessages.Clear();
            string Result = addEntity(value, null);
            return Result;
        }

        public virtual void add(T value)
        {
            IsValid = true;
            ErrorMessages.Clear();
            StartTransaction();
            addentity(value, null);
            if (IsValid)
            {
                Commit();
            }
            else
            {
                Rollback();
            }
            Close();
        }
        public virtual void Add(T value, bool isSimpleEntity)
        {
            IsValid = true;
            ErrorMessages.Clear();
            addEntity(value, isSimpleEntity, null);
        }

        public virtual void Add(IEnumerable<T> values)
        {
            IsValid = true;
            int rowNumber = 1;
            foreach (var value in values)
            {
                addEntity(value, rowNumber);
                rowNumber++;
            }
        }

        public virtual void add(IEnumerable<T> values)
        {
            IsValid = true;
            int rowNumber = 1;
            foreach (var value in values)
            {
                addentity(value, rowNumber);
                rowNumber++;
            }
        }

        // 20180723 FJ)Shinagawa Add Start ->
        public virtual void Add(IEnumerable<T> values, bool isSimpleEntity)
        {
            IsValid = true;
            ErrorMessages.Clear();
            int rowNumber = 1;
            foreach (var value in values)
            {
                addEntity(value, isSimpleEntity, rowNumber);
                rowNumber++;
            }
        }
        // 20180723 FJ)Shinagawa Add End <-

        /// <summary>
        /// Addで検証機能を利用するか
        /// </summary>
        public bool IsValidateUseAdd { get; set; } = true;

        protected virtual string addEntity(T value, int? rowNumber)
        {
            string Result = string.Empty;
            SqlConnection connection = null;
            DynamicParameters parameters;
            string sql;
            CreateInsertText(value, out sql, out parameters);
            try
            {
                int Count = DbUtil.ExecuteUpdate(sql, parameters);
                if (Count > 0)
                {
                    //connection.BeginTransaction().Commit();
                    //connection.Close();
                }
            }
            catch (Exception ex)
            {
                connection.BeginTransaction().Rollback();
                connection.Close();
                throw ex;
            }
            return Result;
        }

        protected virtual void addentity(T value, int? rowNumber)
        {
            if (IsValidateUseAdd)
            {
                validateDataEntity(value, rowNumber);
                if (!IsValid) return;
                validateKeyData(value, UpdateType.Add, rowNumber);
                if (!IsValid) return;
            }
            Hashtable parameters;
            string sql;
            CreateInsertText(value, out sql, out parameters);
            DbUtil.execUpdate(_Context, sql, parameters);
        }

        protected virtual void addEntity(T value, bool isSimpleEntity, int? rowNumber)
        {
            if (IsValidateUseAdd)
            {
                if (isSimpleEntity)
                {
                    ValidateDataEntitySimple(value, rowNumber);
                }
                else
                {
                    ValidateDataEntity(value, rowNumber);
                }

                if (!IsValid) return;
                ValidateKeyData(value, UpdateType.Add, rowNumber);
                if (!IsValid) return;
            }
            DynamicParameters parameters;
            string sql;
            CreateInsertText(value, out sql, out parameters);
            DbUtil.ExecuteUpdate(sql, parameters);
        }

        public virtual void Save(T value)
        {
            IsValid = true;
            saveEntity(value, null);
        }
        public virtual void Save(T value, bool isSimpleEntity)
        {
            IsValid = true;
            ErrorMessages.Clear();
            saveEntity(value, isSimpleEntity, null);
        }


        public virtual void save(T value)
        {
            IsValid = true;
            ErrorMessages.Clear();
            StartTransaction();
            saveentity(value, null);
            if (IsValid)
            {
                Commit();
            }
            else
            {
                Rollback();
            }
            Close();
        }

        protected virtual void saveentity(T value, int? rowNumber)
        {
            if (IsValidateUseSave)
            {
                validateDataEntity(value, rowNumber);
                if (!IsValid) return;
                validateKeyData(value, UpdateType.Save, rowNumber);
                if (!IsValid) return;
            }
            Hashtable parameters;
            string sql;
            CreateUpdateText(value, out sql, out parameters);
            DbUtil.execUpdate(_Context, sql, parameters);
        }

        public virtual void Save(IEnumerable<T> values)
        {
            IsValid = true;
            ErrorMessages.Clear();
            int rowNumber = 1;
            foreach (var value in values)
            {
                saveEntity(value, rowNumber);
                rowNumber++;
            }
        }
        /// <summary>
        /// Saveで検証機能を利用するか
        /// </summary>
        public bool IsValidateUseSave { get; set; } = true;
        protected virtual void saveEntity(T value, int? rowNumber)
        {

            if (IsValidateUseSave)
            {
                //ValidateDataEntity(value, rowNumber);
                //if (!IsValid) return;
                //ValidateKeyData(value, UpdateType.Save, rowNumber);
                //if (!IsValid) return;
            }
            DynamicParameters parameters;
            string sql;
            CreateUpdateText(value, out sql, out parameters);
            DbUtil.ExecuteUpdate(sql, parameters);
        }

        protected virtual void saveEntity(T value, bool isSimpleEntity, int? rowNumber)
        {
            if (IsValidateUseSave)
            {
                if (isSimpleEntity)
                {
                    ValidateDataEntitySimple(value, rowNumber);
                }
                else
                {
                    ValidateDataEntity(value, rowNumber);
                }
                if (!IsValid) return;
                ValidateKeyData(value, UpdateType.Save, rowNumber);
                if (!IsValid) return;
            }
            DynamicParameters parameters;
            string sql;
            CreateUpdateText(value, out sql, out parameters);

            DbUtil.ExecuteUpdate(sql, parameters);
        }

        public virtual void Remove(T value)
        {
            Remove(value, false);
        }

        public virtual void Remove(T value, bool isPhysicalDelete)
        {
            IsValid = true;
            removeEntity(value, null, isPhysicalDelete);
        }

        public virtual void Remove(IEnumerable<T> values)
        {
            Remove(values, false);
        }

        public virtual void remove(IEnumerable<T> values)
        {
            remove(values, false);
        }

        public virtual void remove(IEnumerable<T> values, bool isPhysicalDelete)
        {
            IsValid = true;
            ErrorMessages.Clear();
            StartTransaction();
            int rowNumber = 1;
            foreach (var value in values)
            {
                removeentity(value, rowNumber, isPhysicalDelete);
                rowNumber++;
            }
            if (IsValid)
            {
                Commit();
            }
            else
            {
                Rollback();
            }
            Close();
        }

        public virtual void Remove(IEnumerable<T> values, bool isPhysicalDelete)
        {
            IsValid = true;
            ErrorMessages.Clear();
            int rowNumber = 1;
            foreach (var value in values)
            {
                removeEntity(value, rowNumber, isPhysicalDelete);
                rowNumber++;
            }
        }

        public virtual void remove(T value, bool isPhysicalDelete)
        {
            IsValid = true;
            ErrorMessages.Clear();
            StartTransaction();
            removeentity(value, null, isPhysicalDelete);
            if (IsValid)
            {
                Commit();
            }
            else
            {
                Rollback();
            }
            Close();
        }

        protected virtual void removeEntity(T value, int? rowNumber, bool isPhysicalDelete)
        {
            ValidateKeyData(value, UpdateType.Delete, rowNumber);
            if (!IsValid) return;
            if (isPhysicalDelete)
            {
                DeletePhysical(value);
            }
            else
            {
                Delete(value);
            }
        }

        protected virtual void removeentity(T value, int? rowNumber, bool isPhysicalDelete)
        {
            ValidateKeyData(value, UpdateType.Delete, rowNumber);
            if (!IsValid) return;
            if (isPhysicalDelete)
            {
                deletephysical(value);
            }
            else
            {
                delete(value);
            }
        }

        public virtual void Undo(T value)
        {
            IsValid = true;
            undoEntity(value, null);
        }

        public virtual void Undo(IEnumerable<T> values)
        {
            IsValid = true;
            ErrorMessages.Clear();
            int rowNumber = 1;
            foreach (var value in values)
            {
                undoEntity(value, rowNumber);
                rowNumber++;
            }
        }

        protected virtual void undoEntity(T value, int? rowNumber)
        {
            ValidateKeyData(value, UpdateType.Undo, rowNumber);
            if (!IsValid) return;
            UndoDeleted(value);
        }

        protected virtual IDictionary<string, object> getKeyProperty(T value)
        {
            var properties = value.GetType().GetProperties();

            var keyColumns = new Dictionary<string, object>();

            foreach (var property in properties)
            {
                if (!property.CanRead) continue;

                var key = (KeyAttribute)Attribute.GetCustomAttribute(property, typeof(KeyAttribute));
                if (key != null)
                {
                    keyColumns.Add(property.Name, property.GetValue(value));
                }
            }
            if (keyColumns.Count == 0) throw new AggregateException();

            return keyColumns;
        }

        protected virtual string getKeyWhere(T value)
        {
            var keyColumns = getKeyProperty(value);
            var sql = new StringBuilder();
            foreach (var keyColumn in keyColumns)
            {
                sql.AppendLine(" " + keyColumn.Key + " = @" + keyColumn.Key);
                if (!keyColumns.Last().Equals(keyColumn)) sql.Append(" and");
            }
            return sql.ToString();
        }

        protected virtual string getkeywhere(T value)
        {
            var keyColumns = getKeyProperty(value);

            var sql = new StringBuilder();

            foreach (var keyColumn in keyColumns)
            {
                sql.AppendLine(" " + keyColumn.Key + " = :" + keyColumn.Key);
                if (!keyColumns.Last().Equals(keyColumn)) sql.Append(" and");
            }
            return sql.ToString();
        }

        protected virtual int Select(T value)
        {
            var sql = new StringBuilder();

            sql.AppendLine("select");
            sql.AppendLine(" *");
            sql.AppendLine("from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("where");
            sql.AppendLine(getKeyWhere(value));

            var keyColumns = getKeyProperty(value);
            var parameters = keyColumns;
            DynamicParameters parameter = new DynamicParameters();
            foreach (var proerties in parameters)
            {
                parameter.Add("@" + proerties.Key, proerties.Value);
            }
            List<string> records = DbUtil.Select<string>(sql.ToString(), parameter);
            return records.Count;
        }

        protected virtual int select(T value)
        {
            var sql = new StringBuilder();

            sql.AppendLine("select");
            sql.AppendLine(" *");
            sql.AppendLine("from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("where");
            sql.AppendLine(getkeywhere(value));

            var keyColumns = getKeyProperty(value);
            var parameters = keyColumns.ToHashtable();

            DataTable records = DbUtil.Select(_Context, sql.ToString(), parameters);

            return records.Rows.Count;
        }

        protected virtual void DeletePhysical(T value)
        {
            var sql = new StringBuilder();

            sql.AppendLine("delete");
            sql.AppendLine("from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("where");
            sql.AppendLine(getKeyWhere(value));

            var keyColumns = getKeyProperty(value);
            DynamicParameters dapperparameter = new DynamicParameters();
            var parameters = keyColumns;
            foreach (var parameter in parameters)
            {
                dapperparameter.Add("@" + parameter.Key, parameter.Value);
            }
            DbUtil.ExecuteUpdate(sql.ToString(), dapperparameter);
        }

        protected virtual void deletephysical(T value)
        {
            var sql = new StringBuilder();

            sql.AppendLine("delete");
            sql.AppendLine("from");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("where");
            sql.AppendLine(getkeywhere(value));

            var keyColumns = getKeyProperty(value);
            var parameters = keyColumns.ToHashtable();

            DbUtil.execUpdate(_Context, sql.ToString(), parameters);
        }

        protected virtual void Delete(T value)
        {
            var sql = new StringBuilder();

            sql.AppendLine("update");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("set");
            sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + ((int)DEL_FLAG.Deleted).ToString() + ",");
            sql.AppendLine(" " + nameof(SystemColumn.DEL_DATE) + " = getdate()");
            sql.AppendLine("where");
            sql.AppendLine(getKeyWhere(value));

            var keyColumns = getKeyProperty(value);
            var parameters = keyColumns;
            DynamicParameters dapperparameter = new DynamicParameters();

            foreach (var parameter in parameters)
            {
                dapperparameter.Add("@" + parameter.Key, parameter.Value);
            }
            DbUtil.ExecuteUpdate(sql.ToString(), dapperparameter);
        }

        protected virtual void delete(T value)
        {
            var sql = new StringBuilder();

            sql.AppendLine("update");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("set");
            sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + ((int)DEL_FLAG.Deleted).ToString() + ",");
            sql.AppendLine(" " + nameof(SystemColumn.DEL_DATE) + " = getdate()");
            sql.AppendLine("where");
            sql.AppendLine(getKeyWhere(value));

            var keyColumns = getKeyProperty(value);
            var parameters = keyColumns.ToHashtable();

            DbUtil.execUpdate(_Context, sql.ToString(), parameters);
        }



        protected virtual void UndoDeleted(T value)
        {
            var sql = new StringBuilder();

            sql.AppendLine("update");
            sql.AppendLine(" " + TableName);
            sql.AppendLine("set");
            sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + ((int)DEL_FLAG.Undelete).ToString() + ",");
            sql.AppendLine(" " + nameof(SystemColumn.DEL_DATE) + " = '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "'");
            sql.AppendLine("where");
            sql.AppendLine(getKeyWhere(value));

            var keyColumns = getKeyProperty(value);
            var parameters = keyColumns;

            DynamicParameters dapperparameter = new DynamicParameters();
            foreach (var parameter in parameters)
            {
                dapperparameter.Add("@" + parameter.Key, parameter.Value);
            }

            DbUtil.ExecuteUpdate(sql.ToString(), dapperparameter);
        }

        protected abstract void CreateInsertText(T value, out string sql, out DynamicParameters parameters);

        protected abstract void CreateUpdateText(T value, out string sql, out DynamicParameters parameters);

        protected abstract void CreateInsertText(T value, out string sql, out Hashtable parameters);

        protected abstract void CreateUpdateText(T value, out string sql, out Hashtable parameters);

        public abstract List<T> GetSearchResult(T condition);

        public abstract void EditData(T Value);

        public void MakeConditionInLineOfCsv(string name, string value)
        {
            Csv = "\"" + name + "\",\"" + value + "\"";
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, string value)
        {
            Csv = "\"" + GetPropertyAttributeName(property) + "\",\"" + value + "\"";
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, int conditionType, string value)
        {
            var searchConditionDic = new SearchConditionListMasterInfo().GetDictionary();
            Csv = "\"" + GetPropertyAttributeName(property) + "\",\"" + searchConditionDic[conditionType] + "\",\"" + value + "\"";
        }

        public void MakeConditionInLineOfCsv(string name, int conditionType, string value)
        {
            var searchConditionDic = new SearchConditionListMasterInfo().GetDictionary();
            Csv = "\"" + name + "\",\"" + searchConditionDic[conditionType] + "\",\"" + value + "\"";
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, string conditionName, string value)
        {
            var searchConditionDic = new SearchConditionListMasterInfo().GetDictionary();
            Csv = "\"" + GetPropertyAttributeName(property) + "\",\"" + conditionName + "\",\"" + value + "\"";
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, int value)
        {
            Csv = "\"" + GetPropertyAttributeName(property) + "\",\"" + value.ToString() + "\"";
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, int value, Dictionary<int?, string> dic)
        {
            Csv = "\"" + GetPropertyAttributeName(property) + "\",\"" + dic[value];
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, int conditionType, int value, Dictionary<int?, string> dic)
        {
            var searchConditionDic = new SearchConditionListMasterInfo().GetDictionary();
            Csv = "\"" + GetPropertyAttributeName(property) + "\",\"" + searchConditionDic[conditionType] + "\",\"" + dic[value] + "\"";
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, IEnumerable<int> values, IDictionary<int?, string> dic)
        {
            var searchConditionDic = new SearchConditionListMasterInfo().GetDictionary();
            var mergeValue = new List<string>();
            foreach (var value in values)
            {
                if (!dic.ContainsKey(value))
                {
                    continue;
                }
                mergeValue.Add("\"" + dic[value] + "\"");
            }
            Csv = "\"" + GetPropertyAttributeName(property) + "\"," + string.Join(",", mergeValue.Distinct().OrderBy(x => x));
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, IEnumerable<string> values, IDictionary<int?, string> dic)
        {
            var searchConditionDic = new SearchConditionListMasterInfo().GetDictionary();
            var mergeValue = new List<string>();
            foreach (var value in values)
            {
                if (!dic.ContainsKey(Convert.ToInt32(value)))
                {
                    continue;
                }
                mergeValue.Add("\"" + dic[Convert.ToInt32(value)] + "\"");
            }
            Csv = "\"" + GetPropertyAttributeName(property) + "\"," + string.Join(",", mergeValue.Distinct().OrderBy(x => x));
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, IEnumerable<string> values, IDictionary<string, string> dic)
        {
            var searchConditionDic = new SearchConditionListMasterInfo().GetDictionary();
            var mergeValue = new List<string>();
            foreach (var value in values)
            {
                if (!dic.ContainsKey(value))
                {
                    continue;
                }
                mergeValue.Add("\"" + dic[value] + "\"");
            }
            Csv = "\"" + GetPropertyAttributeName(property) + "\"," + string.Join(",", mergeValue.Distinct().OrderBy(x => x));
        }

        public void MakeConditionInLineOfCsv(PropertyInfo property, dynamic items)
        {
            var mergeValue = new List<string>();
            for (var i = 0; i < items.Count; i++)
            {
                mergeValue.Add("\"" + items[i] + "\"");

            }
            Csv = "\"" + GetPropertyAttributeName(property) + "\"," + string.Join(",", mergeValue.Distinct().OrderBy(x => x));
        }

        protected void Commit()
        {
            if (IsInteriorTransaction)
            {
                _Context.Commit();
            }
        }

        protected void Rollback()
        {
            if (IsInteriorTransaction)
            {
                _Context.Rollback();
            }
        }

        protected void Close()
        {
            if (IsInteriorTransaction && _Context != null)
            {
                _Context.Close();
                _Context = null;
            }
        }

        protected string GetPropertyAttributeName(PropertyInfo property)
        {
            var displayName = property.Name;
            var display = (DisplayAttribute)Attribute.GetCustomAttribute(property, typeof(DisplayAttribute));
            if (display != null) displayName = display.Name;
            return displayName;
        }

        #region IDisposable Support
        protected bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    ErrorMessages.Clear();
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                    csv = null;
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~DataInfoBase() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public virtual void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}