﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.IO;

namespace ZyCoaG.Classes
{
    public class UploadFiles : ConcurrentDictionary<string, UploadFileInfo>, IDisposable
    {
        public new void Clear()
        {
            foreach(var file in Values)
            {
                EnsureDelete(file);
            }
            base.Clear();
        }

        private void EnsureDelete(UploadFileInfo uploadFileInfo)
        {
            if (uploadFileInfo.File.Exists) uploadFileInfo.File.Delete();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Clear();
                }

                disposedValue = true;
            }
        }

        public void Dispose() => Dispose(true);
        #endregion
    }
}