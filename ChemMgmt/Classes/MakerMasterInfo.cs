﻿using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZyCoaG;
using ZyCoaG.Util;
using Dapper;
using ZyCoaG.Classes.util;

namespace ChemMgmt.Classes
{
    public class MakerMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        /// <summary>
        /// データを全て取得します。
        /// </summary>
        /// <returns>データを<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" M_MAKER.MAKER_ID,");
            sql.AppendLine(" M_MAKER.MAKER_NAME,");
            sql.AppendLine(" M_MAKER.DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" M_MAKER");

            var parameters = new DynamicParameters();

            DbContext context = null;
            context = DbUtil.Open(true);
            List<M_MAKER> records = DbUtil.Select<M_MAKER>(sql.ToString(), parameters);
            context.Close();

            var makers = new List<CommonDataModel<int?>>();
            foreach (M_MAKER record in records)
            {
                var maker = new CommonDataModel<int?>();
                maker.Id = OrgConvert.ToNullableInt32(record.MAKER_ID.ToString());
                if (record.MAKER_NAME != null)
                {
                    maker.Name = record.MAKER_NAME.ToString();
                }
                //maker.Name = record.MAKER_NAME.ToString();
                maker.IsDeleted = Convert.ToInt32(record.DEL_FLAG.ToString()).ToBool();
                //maker.Order=Convert.ToInt32(record[""])
                //Changed property order from int Null to Int Not Null
                maker.Order = (maker.Id == null) ? maker.Id.Value : 0;
                makers.Add(maker);
            }
            return makers;
        }
    }
}