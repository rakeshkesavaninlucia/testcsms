﻿using ChemMgmt;
using ChemMgmt.Classes;
using ChemMgmt.Models;
using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ZyCoaG.BaseCommon
{
    /// <summary>
    /// システム制御用クラスです。
    /// </summary>
    public static class Control
    {
        /// <summary>
        /// システムパラメーターを取得します。
        /// </summary>
        /// <param name="Name">値を取得する名前を指定します。</param>
        /// <returns>取得した値を返します。</returns>
        public static string GetParameter(string Name)
        {
            var sql = new StringBuilder();
            var tableControls = new List<S_PARAMETER>();

            sql.AppendLine("select ");
            sql.AppendLine(" " + nameof(S_PARAMETER.PARAMETER_VALUE) + " ");
            sql.AppendLine("from ");
            sql.AppendLine(" " + nameof(S_PARAMETER) + " ");
            sql.AppendLine("where ");
            sql.AppendLine(" " + nameof(S_PARAMETER.PARAMETER_NAME) + " = '" + Name + "'");

            List<string> data = DbUtil.Select<string>(sql.ToString(), null);

            string count = string.Join(Environment.NewLine, data.ToArray());
            // if (data.Rows.Count == 0) throw new Exception("設定が参照できません。");

            return count;
        }

        /// <summary>
        /// テーブル管理システムデータを<see cref="List{S_TAB_CONTROL}"/>で取得します。
        /// </summary>
        /// <returns>S_TAB_CONTROLの全データを返します。</returns>
        public static List<S_TAB_CONTROL> GetTableControl()
        {
            var stb = new StringBuilder();                    /*sql*/
            var tableControls = new List<S_TAB_CONTROL>();

            stb.AppendLine("select ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.TABLE_NAME) + ", ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.TABLE_DISPLAY_NAME) + ", ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.MAINTENANCE_URL) + ", ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.DISPLAY_ORDER) + ", ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.IS_INSERT) + ", ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.IS_UPDATE) + ", ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.IS_DELETE) + ", ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.IS_VIEW) + " ");
            stb.AppendLine("from ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL) + " ");
            stb.AppendLine("order by ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.DISPLAY_ORDER) + ", ");
            stb.AppendLine(" " + nameof(S_TAB_CONTROL.TABLE_NAME) + " ");

            //var datas = DbUtil.select(sql.ToString());
            List<S_TAB_CONTROL> datas = DbUtil.Select<S_TAB_CONTROL>(stb.ToString(), null);
            foreach (S_TAB_CONTROL data in datas)
            {
                var tableControl = new S_TAB_CONTROL();
                tableControl.TABLE_NAME = data.TABLE_NAME;
                tableControl.TABLE_DISPLAY_NAME = data.TABLE_DISPLAY_NAME;
                tableControl.MAINTENANCE_URL = data.MAINTENANCE_URL;
                tableControl.DISPLAY_ORDER = Convert.ToInt32(data.DISPLAY_ORDER);
                tableControl.IS_INSERT = Convert.ToInt32(data.IS_INSERT).ToBool();
                tableControl.IS_UPDATE = Convert.ToInt32(data.IS_UPDATE).ToBool();
                tableControl.IS_DELETE = Convert.ToInt32(data.IS_DELETE).ToBool();
                tableControl.IS_VIEW = Convert.ToInt32(data.IS_VIEW).ToBool();
                tableControls.Add(tableControl);
            }
            return tableControls;
        }

        /// <summary>
        /// テーブル項目管理システムデータを<see cref="List{S_TAB_COL_CONTROL}"/>で取得します。
        /// </summary>
        /// <returns>S_TAB_COL_CONTROLの全データを返します。</returns>
        public static List<S_TAB_COL_CONTROL> GetTableColumnControl(string TableName)
        {
            var sql = new StringBuilder();
            var parameter = new Hashtable();
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.TABLE_NAME) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.COLUMN_NAME) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.COLUMN_DISPLAY_NAME) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.DISPLAY_ORDER) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.DATA_TYPE) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.DATA_LENGTH) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.IS_SEARCH) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.IS_KEY) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.IS_IDENTITY) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.IS_UNIQUE) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.IS_REQUIRED) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.IS_HALFWIDTH) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.IS_PASSWORD) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.CONVERT_KEY) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.CONVERT_TABLE) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.CONVERT_VALUE) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_1) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_2) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_3) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_4) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_5) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_6) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_7) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_8) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_9) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.SUB_10) + " ");
            sql.AppendLine("from ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL) + " ");
            sql.AppendLine("where ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.TABLE_NAME) + " = :" + nameof(S_TAB_COL_CONTROL.TABLE_NAME) + " ");
            sql.AppendLine("order by ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.DISPLAY_ORDER) + ", ");
            sql.AppendLine(" " + nameof(S_TAB_COL_CONTROL.COLUMN_NAME) + " ");

            parameter.Add("TABLE_NAME", TableName);

            var datas = DbUtil.Select(sql.ToString(), parameter);

            foreach (DataRow data in datas.Rows)
            {
                var tableColumnControl = new S_TAB_COL_CONTROL();
                tableColumnControl.TABLE_NAME = Convert.ToString(data[nameof(S_TAB_COL_CONTROL.TABLE_NAME)]);
                tableColumnControl.COLUMN_NAME = Convert.ToString(data[nameof(S_TAB_COL_CONTROL.COLUMN_NAME)]);
                tableColumnControl.COLUMN_DISPLAY_NAME = Convert.ToString(data[nameof(S_TAB_COL_CONTROL.COLUMN_DISPLAY_NAME)]);
                tableColumnControl.DISPLAY_ORDER = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.DISPLAY_ORDER)]);
                tableColumnControl.DATA_TYPE = (S_TAB_COL_CONTROL.DataType)Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.DATA_TYPE)]);
                tableColumnControl.DATA_LENGTH = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.DATA_LENGTH)]);
                tableColumnControl.IS_SEARCH = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.IS_SEARCH)]).ToBool();
                tableColumnControl.IS_KEY = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.IS_KEY)]).ToBool();
                tableColumnControl.IS_IDENTITY = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.IS_IDENTITY)]).ToBool();
                tableColumnControl.IS_UNIQUE = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.IS_UNIQUE)]).ToBool();
                tableColumnControl.IS_REQUIRED = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.IS_REQUIRED)]).ToBool();
                tableColumnControl.IS_HALFWIDTH = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.IS_HALFWIDTH)]).ToBool();
                tableColumnControl.IS_PASSWORD = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.IS_PASSWORD)]).ToBool();
                tableColumnControl.CONVERT_TABLE = Convert.ToString(data[nameof(S_TAB_COL_CONTROL.CONVERT_TABLE)]);
                tableColumnControl.CONVERT_KEY = Convert.ToString(data[nameof(S_TAB_COL_CONTROL.CONVERT_KEY)]);
                tableColumnControl.CONVERT_VALUE = Convert.ToString(data[nameof(S_TAB_COL_CONTROL.CONVERT_VALUE)]);
                tableColumnControl.SUB_1 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_1)]).ToBool();
                tableColumnControl.SUB_2 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_2)]).ToBool();
                tableColumnControl.SUB_3 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_3)]).ToBool();
                tableColumnControl.SUB_4 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_4)]).ToBool();
                tableColumnControl.SUB_5 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_5)]).ToBool();
                tableColumnControl.SUB_6 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_6)]).ToBool();
                tableColumnControl.SUB_7 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_7)]).ToBool();
                tableColumnControl.SUB_8 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_8)]).ToBool();
                tableColumnControl.SUB_9 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_9)]).ToBool();
                tableColumnControl.SUB_10 = Convert.ToInt32(data[nameof(S_TAB_COL_CONTROL.SUB_10)]).ToBool();
                tableColumnControl.SelectData = new Dictionary<string, string>();
                tableColumnControl.ConvertData = new Dictionary<string, string>();

                // データ型が選択の場合は変換データを格納します。
                if (tableColumnControl.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select ||
                    tableColumnControl.DATA_TYPE == S_TAB_COL_CONTROL.DataType.NIDInput)
                {
                    sql = new StringBuilder();
                    parameter = new Hashtable();

                    sql.AppendLine("select ");
                    if (tableColumnControl.CONVERT_TABLE == nameof(S_TAB_COL_CONVERT))
                    {
                        // テーブル項目変換システムデータの場合は削除フラグは全て未削除とします。
                        sql.AppendLine(" " + Convert.ToString((int)DEL_FLAG.Undelete) + " as " + nameof(SystemColumn.DEL_FLAG) + ", ");
                    }
                    else
                    {
                        // 他のマスターの場合は削除フラグを取得する。
                        sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + ", ");
                    }
                    sql.AppendLine(" " + tableColumnControl.CONVERT_KEY + ", ");
                    sql.AppendLine(" " + tableColumnControl.CONVERT_VALUE + " ");
                    sql.AppendLine("from ");
                    sql.AppendLine(" " + tableColumnControl.CONVERT_TABLE.Split(":".ToCharArray())[0] + " ");
                    if (tableColumnControl.CONVERT_TABLE == nameof(S_TAB_COL_CONVERT))
                    {
                        // テーブル項目変換システムデータの場合はテーブル名と項目名を条件にします。
                        sql.AppendLine("where ");
                        sql.AppendLine(" " + nameof(S_TAB_COL_CONVERT.TABLE_NAME) + " = :" + nameof(S_TAB_COL_CONVERT.TABLE_NAME) + " ");
                        sql.AppendLine(" and ");
                        sql.AppendLine(" " + nameof(S_TAB_COL_CONVERT.COLUMN_NAME) + " = :" + nameof(S_TAB_COL_CONVERT.COLUMN_NAME) + " ");
                        parameter.Add(nameof(S_TAB_COL_CONVERT.TABLE_NAME), TableName);
                        parameter.Add(nameof(S_TAB_COL_CONVERT.COLUMN_NAME), tableColumnControl.COLUMN_NAME);
                    }
                    if (tableColumnControl.CONVERT_TABLE.StartsWith(nameof(M_COMMON)))
                    {
                        var slaveTableName = tableColumnControl.CONVERT_TABLE.Split(":".ToCharArray())[1];
                        sql.AppendLine("where ");
                        sql.AppendLine(" " + nameof(M_COMMON.TABLE_NAME) + " = :" + nameof(M_COMMON.TABLE_NAME) + " ");
                        sql.AppendLine(" and");
                        sql.AppendLine(" " + nameof(M_COMMON.KEY_VALUE) + " <> 0 ");
                        parameter.Add(nameof(S_TAB_COL_CONVERT.TABLE_NAME), slaveTableName);
                    }
                    sql.AppendLine("order by ");
                    sql.AppendLine(" " + tableColumnControl.CONVERT_KEY + " ");

                    var convertData = DbUtil.Select(sql.ToString(), parameter);

                    for (int j = 0; j < convertData.Rows.Count; j++)
                    {
                        // 未削除、削除済は変換データへ格納します。
                        tableColumnControl.ConvertData.Add(Convert.ToString(convertData.Rows[j][tableColumnControl.CONVERT_KEY]),
                                                           Convert.ToString(convertData.Rows[j][tableColumnControl.CONVERT_VALUE]));
                        // 未削除は選択データへ格納します。
                        if ((DEL_FLAG)Convert.ToInt32(convertData.Rows[j][nameof(SystemColumn.DEL_FLAG)]) == DEL_FLAG.Undelete)
                        {
                            tableColumnControl.SelectData.Add(Convert.ToString(convertData.Rows[j][tableColumnControl.CONVERT_KEY]),
                                                              Convert.ToString(convertData.Rows[j][tableColumnControl.CONVERT_VALUE]));
                        }
                    }
                }

                tableColumnControls.Add(tableColumnControl);
            }

            tableColumnControls.Where(x => x.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number).ToList().ForEach(x =>
            {
                sql = new StringBuilder();
                parameter = new Hashtable();

                sql.AppendLine("select ");
                sql.AppendLine(" sys.columns.precision,");
                sql.AppendLine(" sys.columns.scale ");
                sql.AppendLine("from ");
                sql.AppendLine(" sys.objects");
                sql.AppendLine(" inner join");
                sql.AppendLine(" sys.columns");
                sql.AppendLine(" on");
                sql.AppendLine(" sys.objects.object_id = sys.columns.object_id");
                sql.AppendLine("where ");
                sql.AppendLine(" sys.objects.name = :" + nameof(S_TAB_COL_CONTROL.TABLE_NAME) + " ");
                sql.AppendLine(" and");
                sql.AppendLine(" sys.columns.name = :" + nameof(S_TAB_COL_CONTROL.COLUMN_NAME) + " ");

                parameter.Add("TABLE_NAME", x.TABLE_NAME);
                parameter.Add("COLUMN_NAME", x.COLUMN_NAME);

                datas = DbUtil.Select(sql.ToString(), parameter);

                if (Convert.ToInt32(datas.Rows[0]["precision"].ToString()) != 0)
                {
                    var scale = Convert.ToInt32(datas.Rows[0]["scale"].ToString());
                    x.DATA_LENGTH = x.DATA_LENGTH - scale;
                    x.DEC_DATA_LENGTH = scale;
                }

            });
            return tableColumnControls;
        }

        /// <summary>
        /// 選択ボタンの追加対象か。
        /// </summary>
        /// <param name="tableName">判定するテーブル名を指定します。</param>
        /// <param name="columnName">判定する項目名を指定します。</param>
        /// <returns>追加対象の場合は<see cref="true"/>、そうでない場合は<see cref="false"/>を返します。</returns>
        public static bool IsSelectButtonTarget(string tableName, string columnName)
        {
            string keyColumnName;
            string valueColumnName;
            string iconPathColumnName;
            string sortOrderColumnName;
            string classColumnName;
            string selFlagName;//2018/08/19 TPE.Sugimoto Upd

            return IsSelectButtonTarget(tableName, columnName, out keyColumnName, out valueColumnName, out iconPathColumnName, out sortOrderColumnName, out classColumnName, out selFlagName);//2018/08/19 TPE.Sugimoto Upd
        }

        /// <summary>
        /// 選択ボタンの追加対象か、また上位カテゴリーのキーと値の項目情報を返します。
        /// </summary>
        /// <param name="tableName">判定するテーブル名を指定します。</param>
        /// <param name="columnName">判定する項目名を指定します。</param>
        /// <param name="keyColumnName">上位カテゴリーのキー項目を返します。</param>
        /// <param name="valueColumnName">上位カテゴリーの値項目を返します。</param>
        /// <param name="iconPathColumnName">アイコンの値項目を返します。</param>
        /// <param name="sortOrderColumnName">ソート順項目を返します。</param>
        /// <param name="classColumnName">区分項目を返します。</param>
        /// <returns>追加対象の場合は<see cref="true"/>、そうでない場合は<see cref="false"/>を返します。</returns>
        public static bool IsSelectButtonTarget(string tableName, string columnName, out string keyColumnName, out string valueColumnName, out string iconPathColumnName,
                                                out string sortOrderColumnName, out string classColumnName, out string selFlagName)//2018/08/19 TPE.Sugimoto Upd
        {
            bool isTarget = false;
            keyColumnName = null;
            valueColumnName = null;
            iconPathColumnName = null;
            sortOrderColumnName = null;
            classColumnName = null;
            selFlagName = null;

            // ユーザーマスター(上位無し)
            if (tableName == "V_GROUP_USER" && columnName == "PID")
            {
                isTarget = true;
                keyColumnName = "ID";
                valueColumnName = "NAME";
            }
            // 部署マスターの上位部署CD
            if (tableName == nameof(M_GROUP) && columnName == nameof(M_GROUP.P_GROUP_CD))
            {
                isTarget = true;
                keyColumnName = nameof(M_GROUP.GROUP_CD);
                valueColumnName = nameof(M_GROUP.GROUP_NAME);
                sortOrderColumnName = nameof(M_GROUP.GROUP_CD);
            }
            // 保管場所マスターの上位保管場所ID
            if ((tableName == nameof(M_LOCATION) || tableName == nameof(M_STORING_LOCATION) || tableName == nameof(M_USAGE_LOCATION)) &&
                 columnName == nameof(M_LOCATION.P_LOC_ID))
            {
                isTarget = true;
                keyColumnName = nameof(M_LOCATION.LOC_ID);
                valueColumnName = nameof(M_LOCATION.LOC_NAME);
                sortOrderColumnName = nameof(M_LOCATION.SORT_LEVEL);
                classColumnName = nameof(M_LOCATION.CLASS_ID);
                selFlagName = nameof(M_LOCATION.SEL_FLAG); //2018/08/19 TPE.Sugimoto Upd
            }
            // 法規制マスター(法規制のみ)の上位法規制種類ID
            if (tableName == nameof(M_REG) && columnName == nameof(M_REG.P_REG_TYPE_ID))
            {
                isTarget = true;
                keyColumnName = nameof(M_REG.REG_TYPE_ID);
                valueColumnName = nameof(M_REG.REG_TEXT);
                iconPathColumnName = nameof(M_REG.REG_ICON_PATH);
                sortOrderColumnName = nameof(M_REG.SORT_ORDER);
            }
            // 法規制マスター(GHS分類・法規制)の上位法規制種類ID
            if (tableName == nameof(M_REGULATION) && columnName == nameof(M_REGULATION.P_REG_TYPE_ID))
            {
                isTarget = true;
                keyColumnName = nameof(M_REGULATION.REG_TYPE_ID);
                valueColumnName = nameof(M_REGULATION.REG_TEXT);
                iconPathColumnName = nameof(M_REGULATION.REG_ICON_PATH);
                sortOrderColumnName = nameof(M_REGULATION.SORT_ORDER);
                classColumnName = nameof(M_REGULATION.CLASS_ID);
            }
            // GHS分類マスターの上位GHS分類ID
            if (tableName == "M_GHSCAT" && columnName == "P_GHSCAT_ID")
            {
                isTarget = true;
                keyColumnName = "GHSCAT_ID";
                valueColumnName = "GHSCAT_NAME";
                iconPathColumnName = "GHSCAT_ICON_PATH";
                sortOrderColumnName = "SORT_ORDER";
            }
            //2019/02/19 TPE.Rin Add Start
            // M_COMMON
            if (tableName == "M_COMMON")
            {
                isTarget = true;
                keyColumnName = nameof(M_COMMON.KEY_VALUE);
                valueColumnName = nameof(M_COMMON.DISPLAY_VALUE);
                sortOrderColumnName = nameof(M_COMMON.DISPLAY_ORDER);
                classColumnName = nameof(M_COMMON.TABLE_NAME);
            }
            //2019/02/19 TPE.Rin Add End
            //2019/04/04 TPE.Rin Add Start
            // 消防法マスターの上位GHS分類ID
            if (tableName == "M_FIRE" && columnName == "P_FIRE_ID")
            {
                isTarget = true;
                keyColumnName = "FIRE_ID";
                valueColumnName = "FIRE_NAME";
                iconPathColumnName = "FIRE_ICON_PATH";
                sortOrderColumnName = "SORT_ORDER";
            }
            // 社内ルールマスターの上位GHS分類ID
            if (tableName == "M_OFFICE" && columnName == "P_OFFICE_ID")
            {
                isTarget = true;
                keyColumnName = "OFFICE_ID";
                valueColumnName = "OFFICE_NAME";
                iconPathColumnName = "OFFICE_ICON_PATH";
                sortOrderColumnName = "SORT_ORDER";
            }
            //2019/04/04 TPE.Rin Add End
            return isTarget;
        }

        /// <summary>
        /// ファイルのダウンロードダイアログを表示します。
        /// </summary>
        /// <param name="targetPage">呼び出し元の<see cref="Page"/>。</param>
        /// <param name="filePath">ダウンロード元のファイルパス。</param>
        /// <param name="fileName">ダウンロード時のファイル名。</param>
        public static void ViewFileDownloadDialog(string filePath, string fileName)
        {
            HttpContext.Current.Session[SessionConst.DownloadFilePath] = filePath;
            HttpContext.Current.Session[SessionConst.DownloadFileName] = fileName;

            string script;
            var requestUri = new Uri(HttpContext.Current.Request.Url, HttpContext.Current.Request.ApplicationPath);
            script = @"window.open(""" + SystemFormPath.Download + @""",""_blank"",""left=3000,top=3000"");";    //2018/10/05 FJ Upd Wait処理対応

            //  targetPage.ClientScript.RegisterStartupScript(targetPage.GetType(), "Download", script, true);
        }



        /// <summary>
        /// 削除ダイアログのスクリプトを作成します。
        /// </summary>
        public static string CreateDeleteMessageScript()
        {
            return CreateConfirmMessageScript("削除");
        }

        /// <summary>
        /// 確認ダイアログのスクリプトを作成します。
        /// </summary>
        /// <param name="itemName">確認する内容。</param>
        public static string CreateConfirmMessageScript(string itemName)
        {
            return CreateMessageScript(string.Format(InfoMessages.ConfirmMessage, itemName));
        }

        /// <summary>
        /// メッセージを表示するスクリプトを作成します。
        /// </summary>
        /// <param name="message">表示するメッセージ内容。</param>
        /// <returns></returns>
        public static string CreateMessageScript(string message)
        {
            //2018/9/2 FJ Add Wait処理追加
            return "var cret = confirm('" + message + "');" +
                   "if (cret == false) { return false; }else{block();};";
        }

        /// <summary>
        /// 未確定確認ダイアログのスクリプトを作成します。
        /// </summary>
        /// <param name="updateActionName">更新操作内容。</param>
        /// <param name="undoActionName">取消操作内容。</param>
        public static string CreateUnfixedConfirmMessageScript(string updateActionName, string undoActionName)
        {
            //2018/9/2 FJ Add Wait処理追加
            //return "return confirm('" + string.Format(InfoMessages.UnfixedDataConfirm, updateActionName, undoActionName) + "');";
            return "var ret = confirm('" + string.Format(InfoMessages.UnfixedDataConfirm, updateActionName, undoActionName) + "');" +
                "if (ret == false) { return false; }else{block();};";

        }

        /// <summary>
        /// 選択確認(単一)のスクリプトを作成します。
        /// </summary>
        /// <param name="itemName">警告する内容。</param>
        /// <param name="control">確認するコントロール。</param>
        public static string CreateSingleSelectionCheckScript(string itemName, System.Web.UI.Control control)
        {
            return "var ret = SingleSelectionCheck('" + string.Format(WarningMessages.NotSelected, itemName) + "', " +
                                                  "'" + string.Format(WarningMessages.SelectedMultiple, itemName) + "', " +
                                                  "'" + control.ClientID + "');" +
                   "if (ret == false) { return false; };";
        }

        /// <summary>
        /// 選択確認(単一)のスクリプトを作成します。
        /// </summary>
        /// <param name="itemName">警告する内容。</param>
        /// <param name="control">確認するコントロール。</param>
        public static string CreateSingleSelectionRadioScript(string itemName, System.Web.UI.Control control)
        {
            return "var ret = SingleSelectionRadio('" + string.Format(WarningMessages.NotSelected, itemName) + "', " +
                                                  "'" + string.Format(WarningMessages.SelectedMultiple, itemName) + "', " +
                                                  "'" + control.ClientID + "');" +
                   "if (ret == false) { return false; };";
        }

        /// <summary>
        /// 選択確認(複数)のスクリプトを作成します。
        /// </summary>
        /// <param name="itemName">警告する内容。</param>
        /// <param name="control">確認するコントロール。</param>
        public static string CreateMultipleSelectionCheckScript(string itemName, System.Web.UI.Control control)
        {
            //2018/9/2 FJ Add Wait処理追加
            return "var ret = MultipleSelectionCheck('" + string.Format(WarningMessages.NotSelected, itemName) + "', '" + control.ClientID + "');" +
                   "if (ret == false) { return false; }else{block();};";
        }

        /// <summary>
        /// ファイルのダウンロードを行います。
        /// </summary>
        /// <param name="targetPage">呼び出し元の<see cref="Page"/>。</param>
        /// <param name="originalFilePath">ダウンロードするファイルパス。</param>
        /// <param name="fileName">ダウンロードする際のデフォルトファイル名。</param>
        public static void FileDownload(string originalFilePath, string fileName)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(originalFilePath);
                var response = HttpContext.Current.Response;
                if (fileInfo.Exists)
                {
                    response.Clear();
                    //response.AddHeader("Content-Disposition", "attachment; filename=" + (fileName));
                    switch (fileName.Substring(fileName.LastIndexOf(".") + 1).ToLower())
                    {
                        case "xlsx":
                            response.ContentType = "application/excel";
                            break;
                        case "csv":
                            response.ContentType = "text/comma-separated-values";
                            break;
                        //2018/10/05 FJ Upd Wait処理対応
                        case "zip":
                            response.ContentType = "application/zip";
                            break;
                        case "pdf":
                            response.ContentType = "Application/pdf";
                            break;
                            //2018/10/05 FJ Upd Wait処理対応
                    }
                    //response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    //response.ContentType = "application/octet-stream";
                    //response.Flush();
                    //response.TransmitFile(fileInfo.FullName);
                    //response.End();

                    //response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    response.ContentType = "text/csv";
                    response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    response.AddHeader("Content-disposition", "attachment; filename=\"" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8) + "\"");
                    response.Flush();
                    response.TransmitFile(fileInfo.FullName);
                    response.End();
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }
    }
}