﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.BaseCommon
{
    /// <summary>
    /// システム設定の値を定義します。
    /// </summary>
    public static class SystemParameterValues
    {
        /// <summary>
        /// 受入タイミング
        /// </summary>
        public static class AcceptanceTiming
        {
            /// <summary>
            /// 購買連携で受入
            /// </summary>
            public const string PurchasingCoordination = "0";

            /// <summary>
            /// 発注承認で受入
            /// </summary>
            public const string ApproveOrder = "1";

            /// <summary>
            /// 直接注文で受入
            /// </summary>
            public const string OrderDirect = "2";
        }

        /// <summary>
        /// バーコード方式
        /// </summary>
        public static class BarcodeMethod
        {
            /// <summary>
            /// 自動採番または手動採番
            /// </summary>
            public const string AutoOrManual = "0";

            /// <summary>
            /// 自動採番のみ
            /// </summary>
            public const string AutoOnly = "1";

            /// <summary>
            /// 手動採番のみ
            /// </summary>
            public const string ManualOnly = "2";
        }

        /// <summary>
        /// 認証方式
        /// </summary>
        public static class AuthMethod
        {
            /// <summary>
            /// ローカルまたはLDAP
            /// </summary>
            public const string LocalOrLdap = "0";

            /// <summary>
            /// ローカルのみ
            /// </summary>
            public const string LocalOnly = "1";

            /// <summary>
            /// LDAPのみ
            /// </summary>
            public const string LdapOnly = "2";
        }

        public static class TransferMethod
        {
            /// <summary>
            /// ダウンロード
            /// </summary>
            public const string Download = "0";

            /// <summary>
            /// メール
            /// </summary>
            public const string Mail = "1";
        }
    }
}