﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG.Util;
using System.Text;
using System.Data;
using ChemMgmt.Classes.util;
using Dapper;
using System.Data.SqlClient;
using ChemMgmt.Classes;
using ChemMgmt.Models;
using ZyCoaG.Classes.util;

namespace ZyCoaG.BaseCommon
{
    /// <summary>
    /// カテゴリー
    /// </summary>
    public class Category
    {
        /// <summary>
        /// キー
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// 値
        /// </summary>
        public string Value { get; set; }

        /// <summary>
        /// 上位キー
        /// </summary>
        public string UpperLevel { get; set; }

        private string _IconPath { get; set; }
        /// <summary>
        /// アイコンパス
        /// </summary>
        public string IconPath
        {
            get
            {
                return !string.IsNullOrWhiteSpace(_IconPath) ? "'" + _IconPath + "'" : "false";
            }
            set
            {
                _IconPath = value;
            }

        }

        /// <summary>
        /// ★
        /// </summary>
        public string SelFlg { get; set; }
    }

    /// <summary>
    /// カテゴリー選択の機能を提供するクラスです。
    /// </summary>
    public class CategorySelect : IDisposable
    {
        /// <summary>
        /// カテゴリー情報
        /// </summary>
        public List<Category> CategoryItems { get; set; }

        /// <summary>
        /// テーブル名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// キー項目名
        /// </summary>
        public string KeyColumnName { get; set; }

        /// <summary>
        /// 値項目名
        /// </summary>
        public string ValueColumnName { get; set; }
        public string STR_DEL_FLAG { get; set; }

        /// <summary>
        /// 上位キー項目名
        /// </summary>
        public string UpperLevelColumnName { get; set; }

        /// <summary>
        /// アイコンパス項目名
        /// </summary>
        public string IconPathColumnName { get; set; }

        /// <summary>
        /// ★
        /// </summary>
        public string SelFlagName { get; set; }


        /// Author: MSCGI
        /// Created date: 16-7-2019
        /// Description : Modified ADO.net calls with Dapper
        /// <summary>
        /// <see cref="CategorySelect"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        /// <param name="TableName">インスタンスを作成する対象のテーブル名を指定します。</param>
        /// <param name="KeyColumnName">キーが格納されている項目名を指定します。</param>
        /// <param name="ValueColumnName">値が格納されている項目名を指定します。</param>
        /// <param name="UpperLevelColumnName">上位キーが格納されている項目名を指定します。</param>
        /// <param name="iconPathColumnName">アイコンが格納されている項目名を指定します。</param>
        /// <param name="SortOrderPathColumnName">ソート順が格納されている項目名を指定します。</param>
        /// <param name="RootNodeKey">親となる項目のキーを指定します。</param>
        //public CategorySelect(string TableName, string KeyColumnName, string ValueColumnName, string UpperLevelColumnName, string IconPathColumnName, string SortOrderPathColumnName, string SelFlgName,string isDelete, string RootNodeKey = null)//2018/08/19 TPE.Sugimoto Upd
        public CategorySelect(string TableName, string KeyColumnName, string ValueColumnName, string UpperLevelColumnName, string IconPathColumnName, string SortOrderPathColumnName, string SelFlagName, string isDelete, string classId, string RootNodeKey = null)//2019/02/09 TPE.Rin Mod
        {
            var sql = new StringBuilder();
            CategoryItems = new List<Category>();

            sql.AppendLine("select ");
            sql.AppendLine(" " + KeyColumnName + ", ");
            sql.AppendLine(" " + ValueColumnName + ", ");
            if (!string.IsNullOrWhiteSpace(UpperLevelColumnName))
            {
                if (TableName == "M_COMMON")
                {
                    sql.AppendLine(" '' DUMMY1, ");
                }
                else
                {
                    sql.AppendLine(" " + UpperLevelColumnName + ", ");
                }
            }
            //else
            //{
            //    sql.AppendLine(" '' DUMMY1, ");
            //}
            if (!string.IsNullOrWhiteSpace(IconPathColumnName))
            {
                sql.AppendLine(" " + IconPathColumnName + ", ");
            }
            //else
            //{
            //    sql.AppendLine(" '' DUMMY2, ");
            //}
            if (!string.IsNullOrWhiteSpace(SelFlagName))
            {
                sql.AppendLine(" " + SelFlagName + ", ");
            }
            //else
            //{
            //    sql.AppendLine(" '' DUMMY3, ");
            //}
            if (!string.IsNullOrWhiteSpace(isDelete))
            {
                sql.AppendLine(nameof(DEL_FLAG) + " ");
            }
            else
            {
                //sql.AppendLine(" '' DUMMY4 ");
                // remove last (,) if it is final column to add in sql.
                sql.Remove(sql.Length - 4, 1);
            }
            sql.AppendLine("from ");
            sql.AppendLine(" " + TableName + " ");
            if (string.IsNullOrWhiteSpace(isDelete))
            {
                sql.AppendLine("where ");
                sql.AppendLine(" " + nameof(DEL_FLAG) + " = " + Convert.ToString((int)DEL_FLAG.Undelete) + " ");
            }

            if (!string.IsNullOrWhiteSpace(classId))
            {
                sql.AppendLine("and ");
                switch (classId)
                {
                    case "Regulation":
                        sql.AppendLine(" " + nameof(CLASS_ID) + " = " + Convert.ToString((int)CLASS_ID.Regulation) + " ");
                        break;

                    case "Gas":
                        sql.AppendLine(" " + nameof(CLASS_ID) + " = " + Convert.ToString((int)CLASS_ID.Gas) + " ");
                        break;

                    case "Fire":
                        sql.AppendLine(" " + nameof(CLASS_ID) + " = " + Convert.ToString((int)CLASS_ID.Fire) + " ");
                        break;

                    case "Office":
                        sql.AppendLine(" " + nameof(CLASS_ID) + " = " + Convert.ToString((int)CLASS_ID.Office) + " ");
                        break;

                }
                if (TableName == "M_COMMON")
                {
                    sql.AppendLine(" TABLE_NAME='" + classId + "'");
                }
            }
            sql.AppendLine("order by ");
            if (!string.IsNullOrWhiteSpace(SortOrderPathColumnName))
            {
                sql.AppendLine(" " + SortOrderPathColumnName + ", ");
            }
            sql.AppendLine(" " + ValueColumnName + " ");

            /// Author: MSCGI
            /// Created date: 16-7-2019
            /// Description : Make if else condition based on the TableNames with Dapper calls
            /// <summary>
            if (TableName.Equals("M_GROUP"))
            {
                SqlConnection connection = DbUtil.Open();
                List<M_GROUP> datas = DbUtil.Select<M_GROUP>(connection, sql.ToString());
                connection.Close();

                foreach (M_GROUP data in datas)
                {
                    var categoryItem = new Category();
                    categoryItem.Key = data.GROUP_CD; 
                    if (string.IsNullOrWhiteSpace(isDelete))
                    {
                        categoryItem.Value = data.GROUP_NAME; 
                    }
                    else
                    {
                        categoryItem.Value = data.GROUP_NAME + (data.DEL_FLAG == 0 ? "" : "【削除済】");
                    }

                    if (!string.IsNullOrWhiteSpace(data.P_GROUP_CD))
                    {
                        categoryItem.UpperLevel = data.P_GROUP_CD;
                    }
                    else
                    {
                        categoryItem.UpperLevel = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(IconPathColumnName))
                    {
                        categoryItem.IconPath = IconPathColumnName;
                    }
                    else
                    {
                        categoryItem.IconPath = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(SelFlagName))
                    {
                        categoryItem.SelFlg = SelFlagName;
                    }
                    else
                    {
                        categoryItem.SelFlg = string.Empty;
                    }

                    CategoryItems.Add(categoryItem);
                }
            }
            else if (TableName.Equals("M_LOCATION"))
            {
                SqlConnection connection = DbUtil.Open();
                List<M_LOCATION> datas = DbUtil.Select<M_LOCATION>(connection, sql.ToString());
                connection.Close();

                foreach (M_LOCATION data in datas)
                {
                    var categoryItem = new Category();
                    categoryItem.Key = data.LOC_ID.ToString();
                    if (string.IsNullOrWhiteSpace(isDelete))
                    {
                        categoryItem.Value = data.LOC_NAME;
                    }
                    else
                    {
                        categoryItem.Value = data.LOC_NAME + (data.DEL_FLAG == 0 ? "" : "【削除済】");
                    }

                    if (!string.IsNullOrWhiteSpace(data.P_LOC_ID.ToString()))
                    {
                        categoryItem.UpperLevel = data.P_LOC_ID.ToString();
                    }
                    else
                    {
                        categoryItem.UpperLevel = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(IconPathColumnName))
                    {
                        categoryItem.IconPath = IconPathColumnName;
                    }
                    else
                    {
                        categoryItem.IconPath = string.Empty;
                    }

                    if (!string.IsNullOrWhiteSpace(SelFlagName))
                    {
                        categoryItem.SelFlg = SelFlagName;
                    }
                    else
                    {
                        categoryItem.SelFlg = string.Empty;
                    }

                    CategoryItems.Add(categoryItem);
                }
            }
            else if (TableName.Equals("M_STORING_LOCATION"))
            {
                SqlConnection connection = DbUtil.Open();
                List<M_STORING_LOCATION> datas = DbUtil.Select<M_STORING_LOCATION>(connection, sql.ToString());
                connection.Close();

                foreach (M_STORING_LOCATION data in datas)
                {
                    var categoryItem = new Category();
                    categoryItem.Key = data.LOC_ID.ToString();
                    if (string.IsNullOrWhiteSpace(isDelete))
                    {
                        categoryItem.Value = data.LOC_NAME;
                    }
                    else
                    {
                        categoryItem.Value = data.LOC_NAME + (data.DEL_FLAG == 0 ? "" : "【削除済】");
                    }

                    if (!string.IsNullOrWhiteSpace(data.P_LOC_ID.ToString()))
                    {
                        categoryItem.UpperLevel = data.P_LOC_ID.ToString();
                    }

                    else
                    {
                        categoryItem.UpperLevel = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(IconPathColumnName))
                    {
                        categoryItem.IconPath = IconPathColumnName;
                    }
                    else
                    {
                        categoryItem.IconPath = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(SelFlagName))//2018/08/19 TPE.Sugimoto Add Sta
                    {
                        categoryItem.SelFlg = SelFlagName;
                    }
                    else
                    {
                        categoryItem.SelFlg = string.Empty;
                    }//2018/08/19 TPE.Sugimoto Add End

                    CategoryItems.Add(categoryItem);
                }
            }
            else if (TableName.Equals("M_LOCATION"))
            {
                SqlConnection connection = DbUtil.Open(); // without Transaction
                List<M_LOCATION> datas = DbUtil.Select<M_LOCATION>(connection, sql.ToString());
                connection.Close();

                foreach (M_LOCATION data in datas)
                {
                    var categoryItem = new Category();
                    categoryItem.Key = data.LOC_ID.ToString(); 
                                                           
                    if (string.IsNullOrWhiteSpace(isDelete))
                    {
                        categoryItem.Value = data.LOC_NAME; 
                    }
                    else
                    {
                        categoryItem.Value = data.LOC_NAME + (data.DEL_FLAG == 0 ? "" : "【削除済】"); 
                    }
                    if (!string.IsNullOrWhiteSpace(data.P_LOC_ID.ToString())) 
                    {
                        //2019/04/13 TPE.Rin Mod
                        if (TableName == "M_COMMON")
                        {
                            categoryItem.UpperLevel = string.Empty;
                        }
                        else
                        {
                            categoryItem.UpperLevel = data.P_LOC_ID.ToString();
                        }
                    }
                    else
                    {
                        categoryItem.UpperLevel = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(IconPathColumnName))
                    {
                        categoryItem.IconPath = IconPathColumnName;
                    }
                    else
                    {
                        categoryItem.IconPath = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(SelFlagName))//2018/08/19 TPE.Sugimoto Add Sta
                    {
                        categoryItem.SelFlg = SelFlagName;
                    }
                    else
                    {
                        categoryItem.SelFlg = string.Empty;
                    }//2018/08/19 TPE.Sugimoto Add End

                    CategoryItems.Add(categoryItem);
                }
            }
            else if (TableName.Equals("M_USAGE_LOCATION"))
            {
                SqlConnection connection = DbUtil.Open(); // without Transaction
                List<M_USAGE_LOCATION> datas = DbUtil.Select<M_USAGE_LOCATION>(connection, sql.ToString());
                connection.Close();

                foreach (M_USAGE_LOCATION data in datas)
                {
                    var categoryItem = new Category();
                    categoryItem.Key = data.LOC_ID.ToString(); 
                    if (string.IsNullOrWhiteSpace(isDelete))
                    {
                        categoryItem.Value = data.LOC_NAME;
                    }
                    else
                    {
                        categoryItem.Value = data.LOC_NAME + (data.DEL_FLAG == 0 ? "" : "【削除済】");  
                    }
                    if (!string.IsNullOrWhiteSpace(data.P_LOC_ID.ToString())) 
                    {
                        //2019/04/13 TPE.Rin Mod
                        if (TableName == "M_COMMON")
                        {
                            categoryItem.UpperLevel = string.Empty;
                        }
                        else
                        {
                            categoryItem.UpperLevel = data.P_LOC_ID.ToString();
                        }
                    }
                    else
                    {
                        categoryItem.UpperLevel = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(IconPathColumnName))
                    {
                        categoryItem.IconPath = IconPathColumnName;
                    }
                    else
                    {
                        categoryItem.IconPath = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(SelFlagName))//2018/08/19 TPE.Sugimoto Add Sta
                    {
                        categoryItem.SelFlg = SelFlagName;
                    }
                    else
                    {
                        categoryItem.SelFlg = string.Empty;
                    }//2018/08/19 TPE.Sugimoto Add End

                    CategoryItems.Add(categoryItem);
                }
            }
            else if (TableName.Equals("M_STORING_LOCATION"))
            {
                SqlConnection connection = DbUtil.Open();  // without Transaction
                List<M_STORING_LOCATION> datas = DbUtil.Select<M_STORING_LOCATION>(connection, sql.ToString());
                connection.Close();

                foreach (M_STORING_LOCATION data in datas)
                {
                    var categoryItem = new Category();
                    categoryItem.Key = data.LOC_ID.ToString(); 
                    if (string.IsNullOrWhiteSpace(isDelete))
                    {
                        categoryItem.Value = data.LOC_NAME; 
                    }
                    else
                    {
                        categoryItem.Value = data.LOC_NAME + (data.DEL_FLAG == 0 ? "" : "【削除済】");
                    }
                    if (!string.IsNullOrWhiteSpace(data.P_LOC_ID.ToString()))
                    {
                        //2019/04/13 TPE.Rin Mod
                        if (TableName == "M_COMMON")
                        {
                            categoryItem.UpperLevel = string.Empty;
                        }
                        else
                        {
                            categoryItem.UpperLevel = data.P_LOC_ID.ToString();
                        }
                    }
                    else
                    {
                        categoryItem.UpperLevel = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(IconPathColumnName))
                    {
                        categoryItem.IconPath = IconPathColumnName;
                    }
                    else
                    {
                        categoryItem.IconPath = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(SelFlagName))//2018/08/19 TPE.Sugimoto Add Sta
                    {
                        categoryItem.SelFlg = Convert.ToInt32(data.SEL_FLAG).ToString();
                    }
                    else
                    {
                        categoryItem.SelFlg = string.Empty;
                    }

                    CategoryItems.Add(categoryItem);
                }
            }
            else if (TableName.Equals("M_REGULATION"))
            {
                SqlConnection connection = DbUtil.Open();
                List<M_REGULATION> datas = DbUtil.Select<M_REGULATION>(connection, sql.ToString());
                connection.Close();
                foreach (M_REGULATION data in datas)
                {
                    var categoryItem = new Category();
                    categoryItem.Key = data.REG_TYPE_ID.ToString();
                    if (string.IsNullOrWhiteSpace(isDelete))
                    {
                        categoryItem.Value = data.REG_TEXT;
                    }
                    else
                    {
                        categoryItem.Value = data.REG_TEXT + (data.DEL_FLAG == 0 ? "" : "【削除済】");
                    }
                    if (!string.IsNullOrWhiteSpace(data.P_REG_TYPE_ID.ToString()))
                    {
                        categoryItem.UpperLevel = data.P_REG_TYPE_ID.ToString();
                    }
                    else
                    {
                        categoryItem.UpperLevel = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(data.REG_ICON_PATH))
                    {
                        categoryItem.IconPath = data.REG_ICON_PATH;
                    }
                    else
                    {
                        categoryItem.IconPath = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(SelFlagName))
                    {
                        categoryItem.SelFlg = Convert.ToInt32(data.SEL_FLAG).ToString();
                    }
                    else
                    {
                        categoryItem.SelFlg = string.Empty;
                    }//2018/08/19 TPE.Sugimoto Add End
                    CategoryItems.Add(categoryItem);
                }
            }
            else if (TableName.Equals("M_COMMON"))
            {
                SqlConnection connection = DbUtil.Open();
                List<M_COMMON> data = DbUtil.Select<M_COMMON>(connection, sql.ToString());
                connection.Close();
                foreach (M_COMMON datas in data)
                {
                    var categoryItem = new Category();
                    categoryItem.Key = datas.KEY_VALUE.ToString();
                    if (string.IsNullOrWhiteSpace(isDelete))
                    {
                        categoryItem.Value = datas.DISPLAY_VALUE;
                    }
                    else
                    {
                        categoryItem.Value = datas.DISPLAY_VALUE + (STR_DEL_FLAG == "0" ? "" : "【削除済】");
                    }
                    if (!string.IsNullOrWhiteSpace(UpperLevelColumnName))
                    {
                        //2019/04/13 TPE.Rin Mod
                        if (TableName == "M_COMMON")
                        {
                            categoryItem.UpperLevel = string.Empty;
                        }
                        else
                        {
                            categoryItem.UpperLevel = datas.COMMON_ID.ToString();
                        }

                    }
                    else
                    {
                        categoryItem.UpperLevel = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(IconPathColumnName))
                    {
                        categoryItem.IconPath = IconPathColumnName;
                    }
                    else
                    {
                        categoryItem.IconPath = string.Empty;
                    }
                    if (!string.IsNullOrWhiteSpace(SelFlagName))//2018/08/19 TPE.Sugimoto Add Sta
                    {
                        categoryItem.SelFlg = Convert.ToInt32(datas.SEL_FLAG).ToString();
                    }
                    else
                    {
                        categoryItem.SelFlg = string.Empty;
                    }//2018/08/19 TPE.Sugimoto Add End

                    CategoryItems.Add(categoryItem);
                }

            }

            if (string.IsNullOrWhiteSpace(RootNodeKey) == false)
            {
                RefineCategoryList(RootNodeKey);
            }

            this.TableName = TableName;
            this.KeyColumnName = KeyColumnName;
            this.ValueColumnName = ValueColumnName;
            if (TableName == "M_COMMON")
            {
                this.UpperLevelColumnName = string.Empty;
            }
            else
            {
                this.UpperLevelColumnName = UpperLevelColumnName;

            }
            this.IconPathColumnName = IconPathColumnName;
            this.SelFlagName = SelFlagName;
        }

        void RefineCategoryList(string rootNodeKey)
        {
            var refinedItems = new List<Category>();
            var currentItem = CategoryItems.Where(x => x.Key == rootNodeKey).FirstOrDefault();
            if (currentItem == null) return;

            currentItem.UpperLevel = "";
            refinedItems.Add(currentItem);
            RefineCategoryListInner(refinedItems, currentItem);

            CategoryItems = refinedItems;
        }

        void RefineCategoryListInner(List<Category> refinedItems, Category currentItem)
        {
            foreach (var item in CategoryItems)
            {
                if (currentItem.Key == item.UpperLevel)
                {
                    refinedItems.Add(item);
                    RefineCategoryListInner(refinedItems, item);
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                CategoryItems.Clear();
                CategoryItems = null;

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~CategorySelect() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}