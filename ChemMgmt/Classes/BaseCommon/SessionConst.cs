﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.BaseCommon
{
    /// <summary>
    /// セッション変数に格納する名前を定義します。
    /// </summary>
    public static class SessionConst
    {

        /// <summary>
        /// テーブル情報を格納するセッション変数名を定義します。
        /// </summary>
        public const string TableInformation = "TableInformation";

        /// <summary>
        /// テーブル項目情報を格納するセッション変数名を定義します。
        /// </summary>
        public const string TableColumnInformations = "TableColumnInformation";

        /// <summary>
        /// マスターのメンテナンス区分を格納するセッション変数名を定義します。
        /// </summary>
        public const string MaintenanceType = "MaintenanceType";

        /// <summary>
        /// 自身のユーザー設定状態であるかを格納するセッション変数名を定義します。
        /// </summary>
        public const string IsSelfSettings = "IsSelfSettings";

        /// <summary>
        /// マスターのキー項目、キー値を格納するセッション変数名を定義します。
        /// </summary>
        public const string MasterKeys = "";

        /// <summary>
        /// マスターの編集した値を格納するセッション変数名を定義します。
        /// </summary>
        public const string MasterData = "MasterData";
        public const string MasterModelZC05901 = "MasterModelZC05901";
        public const string MasterModelZC05902 = "MasterModelZC05902";
        /// <summary>
        /// カテゴリー選択の対象となるテーブル名のセッション変数名を定義します。
        /// </summary>
        public const string CategoryTableName = "TableName";

        /// <summary>
        /// カテゴリー選択の対象となる項目名のセッション変数名を定義します。
        /// </summary>
        public const string CategoryColumnName = "ColumnName";

        /// <summary>
        /// カテゴリー選択を格納するセッション変数名を定義します。
        /// </summary>
        public const string CategorySelect = "CategorySelect";

        /// <summary>
        /// 検索種類を格納するセッション変数名を定義します。
        /// </summary>
        public const string SearchType = "SearchType";

        /// <summary>
        /// 行識別IDを格納するセッション変数名を定義します。
        /// </summary>
        public const string RowId = "RowId";

        /// <summary>
        /// カテゴリー選択の帰り値を格納するセッション変数名を定義します。
        /// </summary>
        public const string CategorySelectReturnValue = "CategorySelectReturnValue";

        /// <summary>
        /// 汎用マスターメンテナンス画面へ戻る場合の変数名を定義します。
        /// </summary>
        public const string IsCommonMasterFormBack = "IsCommonMasterFormBack";

        /// <summary>
        /// 汎用マスターメンテナンス画面で検索されたかの変数名を定義します。
        /// </summary>
        public const string IsCommonMasterSearch = "IsCommonMasterSearch";

        /// <summary>
        /// ダウンロードファイルパスの変数名を定義します。
        /// </summary>
        public const string DownloadFilePath = "DownloadFilePath";

        /// <summary>
        /// ダウンロードファイル名の変数名を定義します。
        /// </summary>
        public const string DownloadFileName = "DownloadFileName";

        /// <summary>
        /// 在庫管理IDの変数名を定義します。
        /// </summary>
        public const string StockId = "StockId";

        /// <summary>
        /// 在庫管理バーコードの変数名を定義します。
        /// </summary>
        public const string Barcode = "Barcode";

        /// <summary>
        /// 検索後かの変数名を定義します。
        /// </summary>
        public const string IsScanned = "IsScanned";

        /// <summary>
        /// 管理者かの変数名を定義します。
        /// </summary>
        public const string IsAdmin = "IsAdmin";

        /// <summary>
        /// ログイン時かの変数名を定義します。
        /// </summary>
        public const string IsLogin = "IsLogin";

        /// <summary>
        /// メンテナンス画面から戻りであるかの変数名を定義します。
        /// </summary>
        public const string IsMaintenaceFormBack = "IsMaintenaceFormBack";

        //2018/08/02 TPE Add
        /// <summary>
        /// 化学物質取扱責任者を格納します。
        /// </summary>
        public const string ChemChief = "ChemChief";
        public const string sessionChemChief = "sessionChemChief";
        //2018/08/02 TPE Add
        /// <summary>
        /// セッション変数に格納する商品一覧の名前を定義します。
        /// </summary>
        public const string SessionChief = "Chief";
        //sessionChief
        //2019/02/19 TPE Add
        /// <summary>
        /// セッション変数に格納するCASを定義します。
        /// </summary>
        public const string SessionCas = "Cas";
        //sessionCas

        //2019/02/20 TPE.Rin Add
        public const string ClassId = "ClasId";

        // To retain webGrid 
        public const string WebGrid = "WebGrid";
        public const string WebGridCount = "WebGridCount";

        public const string cSESSION_NAME_SEARCH_RESULT = "cSESSION_NAME_SEARCH_RESULT";
        public const string cSESSION_NAME_InputSearch = "cSESSION_NAME_InputSearch";

        public const string CurrentRowId = "CurrentRowId";

        public const string UnRegisteredChemCD = "UnRegisteredChemCD";
    }
}