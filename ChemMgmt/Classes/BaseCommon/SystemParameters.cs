﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static ZyCoaG.BaseCommon.Control;
using ZyCoaG.Barcode;

namespace ZyCoaG.BaseCommon
{
    /// <summary>
    /// システムパラメーターを取得するクラスです。
    /// </summary>
    public class SystemParameters
    {
        /// <summary>
        /// 受入するタイミングを取得します。
        /// </summary>
        public string AcceptanceTiming
        {
            get
            {
                return GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_",""));
            }
        }

        /// <summary>
        /// 化学物質のバーコードの種類を取得します。
        /// </summary>
        public BarcodeType BarcodeTypeChem
        {
            get
            {
                return GetBarcodeType(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }

        /// <summary>
        /// ユーザーのバーコードの種類を取得します。
        /// </summary>
        public BarcodeType BarcodeTypeUser
        {
            get
            {
                return GetBarcodeType(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }

        /// <summary>
        /// 保管場所のバーコードの種類を取得します。
        /// </summary>
        public BarcodeType BarcodeTypeLocation
        {
            get
            {
                return GetBarcodeType(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }

        /// <summary>
        /// 保管場所のバーコードを取得する実体です。
        /// </summary>
        /// <param name="ParameterName"></param>
        /// <returns></returns>
        private BarcodeType GetBarcodeType(string ParameterName)
        {
            var value = GetParameter(ParameterName);
            return (BarcodeType)int.Parse(value);
        }

        /// <summary>
        /// バーコード方式を取得します。
        /// </summary>
        public string BarcodeMethod
        {
            get
            {
                return GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }

        /// <summary>
        /// 認証方式を取得します。
        /// </summary>
        public string AuthMethod
        {
            get
            {
                return GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }

        /// <summary>
        /// LDAPサーバーパスを取得します。
        /// </summary>
        public string LdapServerPath
        {
            get
            {
                return GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }

        /// <summary>
        /// LDAPユーザー名を取得します。
        /// </summary>
        public string LdapUserName
        {
            get
            {
                return GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }
        
        /// <summary>
        /// LDAP認証方式を取得します。
        /// </summary>
        public string LdapAuthMethod
        {
            get
            {
                return GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }

        /// <summary>
        /// 化学物質一覧の送信方法を取得します。
        /// </summary>
        public string TransferMethodChem
        {
            get
            {
                return GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }
        
        /// <summary>
        /// 在庫一覧の送信方法を取得します。
        /// </summary>
        public string TransferMethodStock
        {
            get
            {
                return GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }
        
        /// <summary>
        /// 発注履歴一覧の送信方法を取得します。
        /// </summary>
        public string TransferMethodOrder
        {
            get
            {
                return GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", ""));
            }
        }
        
        /// <summary>
        /// 化学物質マスター検索結果の最大表示件数を取得します。
        /// </summary>
        public int MaxChemResultCount
        {
            get
            {
                return Convert.ToInt32(GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", "")));
            }
        }

        /// <summary>
        /// 管理台帳一括変更申請の上限数を取得します。
        /// </summary>
        public int MaxBulkChangeCount
        {
            get
            {
                return Convert.ToInt32(GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", "")));
            }
        }

        /// <summary>
        /// 在庫検索結果の最大表示件数を取得します。
        /// </summary>
        public int MaxStockResultCount
        {
            get
            {
                return Convert.ToInt32(GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", "")));
            }
        }

        /// <summary>
        /// 使用履歴検索結果の最大表示件数を取得します。
        /// </summary>
        public int MaxHistResultCount
        {
            get
            {
                return Convert.ToInt32(GetParameter(System.Reflection.MethodBase.GetCurrentMethod().Name.Replace("get_", "")));
            }
        }
    }
}