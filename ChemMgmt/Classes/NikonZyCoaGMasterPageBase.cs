﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Ledger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Classes
{
    public abstract partial class NikonZyCoaGMasterPageBase
    {
        /// <summary>
        /// 含有物を含めるかの選択肢を取得します。
        /// </summary>
        /// <returns>使用状態データを取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetIncludesContainedList() => new IncludesContainedListMasterInfo().GetSelectList();

        /// <summary>
        /// 鍵管理の選択項目を取得します。
        /// </summary>
        /// <returns>鍵管理の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetKeyManagementList() => new CommonMasterInfo(NikonCommonMasterName.KEY_MGMT).GetSelectList();

        /// <summary>
        /// 重量管理の選択項目を取得します。
        /// </summary>
        /// <returns>重量管理の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetWeightManagementList() => new CommonMasterInfo(NikonCommonMasterName.WEIGHT_MGMT).GetSelectList();

        /// <summary>
        /// 形状の選択項目を取得します。
        /// </summary>
        /// <returns>形状の選択項目を取得します。</returns>
        /// 
        ///This value should be in dropdown
        public virtual IEnumerable<CommonDataModel<int?>> GetFigureList() => new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetSelectList();

        /// <summary>
        /// 変更・廃止の事由の選択項目を取得します。
        /// </summary>
        /// <returns>変更・廃止の事由の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetReasonList() => new CommonMasterInfo(NikonCommonMasterName.REASON).GetSelectList();

        /// <summary>
        /// 労働安全衛生法上の用途の選択項目を取得します。
        /// </summary>
        /// <returns>労働安全衛生法上の用途の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetIshaUsageList() => new IshaUsageMasterInfo().GetSelectList();

        /// <summary>
        /// 小分けの有無の選択項目を取得します。
        /// </summary>
        /// <returns>小分けの選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetSubContainerList() => new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetSelectList();

        /// <summary>
        /// 保護具の選択項目を取得します。
        /// </summary>
        /// <returns>保護具の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetProtectorList() => new CommonMasterInfo(NikonCommonMasterName.PROTECTOR).GetSelectList();

        /// <summary>
        /// ランクの選択項目を取得します。
        /// </summary>
        /// <returns>ランクの選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetRankList() => new CommonMasterInfo(NikonCommonMasterName.RANK).GetSelectList();

        /// <summary>
        /// ランクの選択項目を取得します。
        /// </summary>
        /// <returns>ランクの選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetLedgerStatusList() => new LedgerStatusListMasterInfo().GetSelectList();

        /// <summary>
        /// 分類の選択項目を取得します。
        /// </summary>
        /// <returns>分類の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetCHEM_CATList() => new CommonMasterInfo(NikonCommonMasterName.CHEM_CAT).GetSelectList();

        //2019/02/14 TPE Add Start
        /// <summary>
        /// 職場の取扱量の選択項目を取得します。
        /// </summary>
        /// <returns>職場の取扱量の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetRegTransactionVolumeList() => new CommonMasterInfo(NikonCommonMasterName.REG_TRANSACTION_VOLUME).GetSelectList();

        /// <summary>
        /// 作業頻度の選択項目を取得します。
        /// </summary>
        /// <returns>作業頻度の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetRegWorkFrequencyList() => new CommonMasterInfo(NikonCommonMasterName.REG_WORK_FREQUENCY).GetSelectList();

        /// <summary>
        /// 災害発生の可能性の選択項目を取得します。
        /// </summary>
        /// <returns>災害発生の可能性の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetRegDisasterPossibilityList() => new CommonMasterInfo(NikonCommonMasterName.REG_DISASTER_POSSIBILITY).GetSelectList();
        //2019/02/14 TPE Add End


        /// <summary>
        /// ユーザーマスターからデータを取得します。
        /// </summary>
        /// <returns>ユーザーマスターデータを取得します。</returns>
        public virtual IEnumerable<CommonDataModel<string>> GetUserList() => new UserMasterInfo().GetSelectList();

        /// <summary>
        /// メーカーマスターからデータを取得します。
        /// </summary>
        /// <returns>メーカーマスターデータを取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetMakerList() => new MakerMasterInfo().GetSelectList();



        //2019/04/01 TPE.Rin Add Start
        public virtual IEnumerable<CommonDataModel<int?>> GetMeasurementList() => new MeasurementListMasterInfo().GetSelectList();

        public virtual IEnumerable<CommonDataModel<int?>> GetExaminationList() => new ExaminationListMasterInfo().GetSelectList();
        //2019/04/02 TPE.Rin add End

        /// <summary>
        /// リスク低減策の選択項目を取得します。
        /// </summary>
        /// <returns>リスク低減策の選択項目を取得します。</returns>
        public virtual IEnumerable<CommonDataModel<int?>> GetRiskReduceList() => new CommonMasterInfo(NikonCommonMasterName.RISK_REDUCE).GetSelectList();


        /// <summary>
        /// 部署マスターからデータを取得します。
        /// </summary>
        /// <returns>部署マスターデータを取得します。</returns>
        public virtual IEnumerable<CommonDataModel<string>> GetGroupList() => new GroupMasterInfo().GetSelectList();

    }
}