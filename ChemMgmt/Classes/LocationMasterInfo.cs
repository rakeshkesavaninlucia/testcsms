﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;


namespace ZyCoaG
{
   
        public enum LocationClassId
        {
            /// <summary>
            /// 全て
            /// </summary>
            All,

            /// <summary>
            /// 保管場所
            /// </summary>
            Storing,

            /// <summary>
            /// 使用場所
            /// </summary>
            Usage,

            /// <summary>
            /// 保管場所(管理台帳)
            /// </summary>
            StoringLedger,
        }

        public class LocationMasterInfo : IDisposable
        {
            public IEnumerable<V_LOCATION> dataArray { get; private set; }

            private LocationClassId locationClassId { get; set; } = LocationClassId.All;

            private string locationRegNo { get; set; }

            public LocationMasterInfo()
            {
                getLocationMaster();
            }

            public LocationMasterInfo(LocationClassId locationClassId)
            {
                this.locationClassId = locationClassId;
                getLocationMaster();
            }

            public LocationMasterInfo(LocationClassId locationClassId, string RegNo)
            {
                this.locationClassId = locationClassId;
                this.locationRegNo = RegNo;
                getLocationMaster();
            }

        private void getLocationMaster()
        {
            var sql = new StringBuilder();
            var parameters = new DynamicParameters();
            sql.AppendLine("select");
            sql.AppendLine(" V_LOCATION.LOC_ID,");
            sql.AppendLine(" V_LOCATION.CLASS_ID,");
            sql.AppendLine(" V_LOCATION.LOC_BARCODE,");
            sql.AppendLine(" V_LOCATION.LOC_NAME,");
            sql.AppendLine(" V_LOCATION.LOC_NAME_BASE,");
            sql.AppendLine(" V_LOCATION.LOC_NAME_LAST,");
            sql.AppendLine(" V_LOCATION.P_LOC_ID,");
            sql.AppendLine(" V_LOCATION.SORT_LEVEL,");
            sql.AppendLine(" V_LOCATION.DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" V_LOCATION");
            switch (locationClassId)
            {
                case (LocationClassId.Storing):
                    sql.AppendLine("where");
                    sql.AppendLine(" V_LOCATION.CLASS_ID = @" + nameof(V_LOCATION.CLASS_ID));
                    parameters.Add(nameof(V_LOCATION.CLASS_ID), Nikon.NikonConst.StoringLocationTypeId);
                    break;
                case (LocationClassId.Usage):
                    sql.AppendLine("where");
                    sql.AppendLine(" V_LOCATION.CLASS_ID = @" + nameof(V_LOCATION.CLASS_ID));
                    parameters.Add(nameof(V_LOCATION.CLASS_ID), Nikon.NikonConst.UsageLocationTypeId);
                    break;
                case (LocationClassId.StoringLedger):
                    sql.AppendLine("where");
                    sql.AppendLine(" V_LOCATION.CLASS_ID = @" + nameof(V_LOCATION.CLASS_ID));
                    sql.AppendLine("and ");
                    sql.AppendLine(" V_LOCATION.LOC_ID in (");
                    sql.AppendLine("select T_MGMT_LED_LOC.LOC_ID from T_MGMT_LED_LOC where T_MGMT_LED_LOC.REG_NO = @REG_NO);");
                    parameters.Add(nameof(V_LOCATION.CLASS_ID), Nikon.NikonConst.StoringLocationTypeId);
                    parameters.Add("@REG_NO", this.locationRegNo);
                    break;
            }

            //DbContext context = null;
            //context = DbUtil.Open(true);
            List<V_LOCATION> records = DbUtil.Select<V_LOCATION>(sql.ToString(), parameters);
            //context.Close();

            var dataArray = new List<V_LOCATION>();
            foreach (var record in records)
            {
                var data = new V_LOCATION();
                data.LOC_ID = Convert.ToInt32((record.LOC_ID).ToString());
                data.CLASS_ID = OrgConvert.ToNullableInt32((record.CLASS_ID).ToString());
                if (record.LOC_BARCODE != null)
                {
                    data.LOC_BARCODE = record.LOC_BARCODE.ToString();
                }
                data.LOC_NAME = record.LOC_NAME.ToString();
                data.LOC_NAME_BASE = record.LOC_NAME_BASE.ToString();
                data.LOC_NAME_LAST = record.LOC_NAME_LAST.ToString();
                data.P_LOC_ID = OrgConvert.ToNullableInt32((record.P_LOC_ID).ToString());
                data.SORT_LEVEL = OrgConvert.ToNullableInt32((record.SORT_LEVEL).ToString());
                data.DEL_FLAG = OrgConvert.ToNullableInt32((record.DEL_FLAG).ToString());
                dataArray.Add(data);
            }
            this.dataArray = dataArray;
        }

            public V_LOCATION GetData(int Id)
            {
                return dataArray.Where(x => x.LOC_ID == Id).SingleOrDefault();
            }

            public V_LOCATION GetData(int Id, DEL_FLAG DeleteFlag)
            {
                return dataArray.Where(x => x.LOC_ID == Id && x.DEL_FLAG == (int)DeleteFlag).SingleOrDefault();
            }

            public IEnumerable<V_LOCATION> GetData(string barcode)
            {
                return dataArray.Where(x => x.LOC_BARCODE == barcode);
            }

            public IEnumerable<V_LOCATION> GetData(string barcode, DEL_FLAG DeleteFlag)
            {
                return dataArray.Where(x => x.LOC_BARCODE == barcode && x.DEL_FLAG == (int)DeleteFlag);
            }

            public IEnumerable<V_LOCATION> GetData(string barcode, DEL_FLAG DeleteFlag, string RegNo)
            {
                return dataArray.Where(x => x.LOC_BARCODE == barcode && x.DEL_FLAG == (int)DeleteFlag);
            }

            public IEnumerable<V_LOCATION> GetSelectedItem(int id)
            {
                return dataArray.Where(x => x.LOC_ID == id);
            }

            public IEnumerable<V_LOCATION> GetSelectedItems(IEnumerable<int> ids)
            {
                return dataArray.Where(x => ids.Contains(x.LOC_ID)).OrderBy(x => x.SORT_LEVEL).ThenBy(x => x.LOC_ID);
            }

            public IEnumerable<V_LOCATION> GetChildrenData(int id, DEL_FLAG deleteFlag)
            {
                return dataArray.Where(x => x.P_LOC_ID == id && x.DEL_FLAG == (int)deleteFlag);
            }

            public IEnumerable<V_LOCATION> GetChildrenData(string barcode, DEL_FLAG deleteFlag)
            {
                var id = GetData(barcode, deleteFlag).First().LOC_ID;
                return GetChildrenData(id, deleteFlag);
            }

            #region IDisposable Support
            private bool disposedValue = false; // 重複する呼び出しを検出するには

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                        dataArray = null;
                    }

                    // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                    // TODO: 大きなフィールドを null に設定します。

                    disposedValue = true;
                }
            }

            /// <summary>
            /// <para>変換辞書を取得します。</para>
            /// <para>重複したデータが存在した場合、後のデータが優先されます。</para>
            /// <para>基本機能→各社用の用語などのために後のデータを優先しています。</para>
            /// </summary>
            /// <returns>変換辞書を<see cref="IDictionary{int?,string}"/>で返します。</returns>
            public virtual IDictionary<int?, string> GetDictionary()
            {
                var dic = new Dictionary<int?, string>();
                foreach (var data in dataArray)
                {
                    dic.Add(data.LOC_ID, data.LOC_NAME);
                }
                return dic;
            }

            // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
            // ~RegulationInfo() {
            //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            //   Dispose(false);
            // }

            // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
            public void Dispose()
            {
                // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
                Dispose(true);
                // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
                // GC.SuppressFinalize(this);
            }
            #endregion
        }

        public class LocationMasterInfoInLedger : CommonDataModelMasterInfoBase<int?>
        {
            private LocationClassId locationClassId { get; set; } = LocationClassId.All;
            private String regNo { get; set; }

            protected override bool IsAddSpace { get; set; } = false;
            public LocationMasterInfoInLedger(LocationClassId locationClassId, string regNo)
            {
                this.regNo = regNo;
                this.locationClassId = locationClassId;
                getLocationMaster();
            }

            protected override IEnumerable<CommonDataModel<int?>> GetAllData()
            {
                return new List<CommonDataModel<int?>>();
            }

        private void getLocationMaster()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" V_LOCATION.LOC_ID,");
            sql.AppendLine(" V_LOCATION.LOC_NAME,");
            sql.AppendLine(" V_LOCATION.DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" V_LOCATION");
            sql.AppendLine("where");
            sql.AppendLine(" V_LOCATION.CLASS_ID = @" + nameof(V_LOCATION.CLASS_ID));
            sql.AppendLine("and ");
            sql.AppendLine(" V_LOCATION.LOC_ID in (");
            sql.AppendLine("select T_MGMT_LED_LOC.LOC_ID from T_MGMT_LED_LOC where T_MGMT_LED_LOC.REG_NO = @REG_NO);");
            var parameters = new DynamicParameters();
            parameters.Add(nameof(V_LOCATION.CLASS_ID), Nikon.NikonConst.StoringLocationTypeId);
            parameters.Add("@REG_NO", this.regNo);

            //DbContext context = null;
            //context = DbUtil.Open(true);           
            List<V_LOCATION> records = DbUtil.Select<V_LOCATION>(sql.ToString(), parameters);
            //context.Close();

            var collection = new List<CommonDataModel<int?>>();
            int order = 0;
            foreach (var record in records)
            {
                collection.Add(new CommonDataModel<int?>
                {
                    Id = OrgConvert.ToNullableInt32(record.LOC_ID.ToString()),
                    Name = record.LOC_NAME.ToString(),
                    Order = order,
                    IsDeleted = Convert.ToInt32(record.DEL_FLAG.ToString()).ToBool()
                });
                order++;
            };
            DataArray = collection;
        }
    }
}
