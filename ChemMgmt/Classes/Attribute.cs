﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChemMgmt.Classes
{
    [AttributeUsage(AttributeTargets.Property)]
    public class UniqueAttribute : Attribute
    {
        public UniqueAttribute() { }
    }
    //2018/08/08 TPE.Sugimoto Add Sta
    [AttributeUsage(AttributeTargets.Property)]
    public class ChemUniqueAttribute : Attribute
    {
        public ChemUniqueAttribute() { }
    }
    //2018/08/08 TPE.Sugimoto Add End
    [AttributeUsage(AttributeTargets.Property)]
    public class HalfwidthAttribute : RegularExpressionAttribute
    {
        public HalfwidthAttribute() : base(@"[a-zA-Z0-9!-/:-@\[-`{-~]+$") { }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class NumberAttribute : Attribute
    {
        public NumberAttribute() { }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class RegularNumberAttribute : NumberAttribute
    {
        public RegularNumberAttribute() { }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class NumberLengthAttribute : Attribute
    {
        public int integerNumberLength { get; private set; }
        public int decimalNumberLength { get; private set; }

        public NumberLengthAttribute(int numberLength)
        {
            this.integerNumberLength = numberLength;
            this.decimalNumberLength = 0;
        }

        public NumberLengthAttribute(int integerNumberLength, int decimalNumberLength)
        {
            this.integerNumberLength = integerNumberLength;
            this.decimalNumberLength = decimalNumberLength;
        }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class DateAttribute : Attribute
    {
        public DateAttribute() { }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class NotVerifyValueAttribute : Attribute
    {
        public NotVerifyValueAttribute() { }
    }

    [AttributeUsage(AttributeTargets.Property)]
    public class ExcludingProhibitionAttribute : Attribute { }

    [AttributeUsage(AttributeTargets.Property)]
    public class SmallCompareAttribute : CompareAttribute
    {
        public SmallCompareAttribute(string otherProperty) : base(otherProperty) { }
    }

    [AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
    public class NotColumnAttribute : Attribute { }

    public static class PropertyUtil
    {
        public static string GetName(dynamic obj, string name)
        {
            var display = (DisplayAttribute)Attribute.GetCustomAttribute(
                obj.GetType().GetProperty(name), typeof(DisplayAttribute));
            return display == null ? string.Empty : display.Name;
        }
        public static string GetName(Type type, string name)
        {
            var display = (DisplayAttribute)Attribute.GetCustomAttribute(
                type.GetProperty(name), typeof(DisplayAttribute));
            return display == null ? string.Empty : display.Name;
        }
    }
}