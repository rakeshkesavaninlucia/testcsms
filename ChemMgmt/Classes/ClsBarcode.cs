﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ZyCoaG.Util
{
    public class ClsBarcode
    {
        #region 定数

        private const int BARCODE_LENGTH = 15; //バーコードの桁数(現時点では15桁バーコードで発行)

        #endregion

        #region メソッド
        /// <summary>
        /// バーコードを指定枚数、自動採番する。
        /// </summary>
        /// <param name="intCount">発行枚数</param>
        /// <returns>発行されたバーコード番号(複数可)</returns>
        public string[] CreateBarcode(int intCount)
        {
            string[] listBarcode = new string[intCount];

            for (int i = 0; i < intCount; i++)
            {
                string strSQL = "SELECT BARCODE_SEQ.NEXTVAL AS SEQ FROM DUAL ";

                DataTable dt = DbUtil.Select(strSQL);

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        listBarcode[i] = Conv36(Convert.ToInt64(dr["SEQ"]));
                    }
                }
            }

            return listBarcode;
        }

        /// <summary>
        /// 36進数に変換
        /// </summary>
        /// <param name="lngNumber">変換する数値</param>
        /// <returns>変換後の値</returns>
        public static string Conv36(long lngNumber)
        {
            string[] str = new string[BARCODE_LENGTH];
            long lngTemp0;
            long lngTemp1;
            string strBarcode = null;

            lngTemp0 = lngNumber;
            for (int i = 0; i < BARCODE_LENGTH; i++)
            {
                lngTemp1 = lngTemp0 % 36;
                if (lngTemp1 < 10)
                {
                    str[i] = Convert.ToString(lngTemp1 + 48);
                }
                else
                {
                    str[i] = Convert.ToString(lngTemp1 + 55);
                }

                if (i < BARCODE_LENGTH)
                {
                    lngTemp0 = Convert.ToInt64(Math.Floor((decimal)lngTemp0 / 36));
                }

                strBarcode = Convert.ToChar(Convert.ToInt32(str[i])) + strBarcode;
            }

            return strBarcode;
        }

        /// <summary>
        /// ニコン用のバーコードフォーマットを作成します。
        /// </summary>
        /// <param name="barcodeNumber">採番されたバーコード番号。</param>
        /// <returns>[X][NNNNNNNNNNN]のフォーマットを返します。</returns>
        public static string NikonBarcodeConvert(long barcodeNumber)
        {
            // 全体を13桁に変換します。[NNNNNNNNNNNNN]
            var barcodeString = barcodeNumber.ToString("D13");

            var barcodeChar = string.Empty;
            // 下11桁を抜きだします。[NNNNNNNNNNN]
            var barcodeNumberString = barcodeString.Substring(2);

            // 上2桁の変換表を作成します。「I」「L」「O」は除外します。
            var convertDic = new Dictionary<string, string>();
            for (var i = 0; i < 26; i++)
            {
                var alphabet = Convert.ToChar(char.Parse("A") + i);

                var passAlphabet = new List<char>() { char.Parse("I"), char.Parse("L"), char.Parse("L") };

                if (!passAlphabet.Contains(alphabet))
                {
                    convertDic.Add(convertDic.Count.ToString("D2"), alphabet.ToString());
                }
            }

            // 上2桁から[X]に変換します。[X]
            var barcodeCharNumber = barcodeString.Substring(0, 2);
            if (convertDic.ContainsKey(barcodeCharNumber))
            {
                // [X][NNNNNNNNNNN]を返します。
                return convertDic[barcodeCharNumber] + barcodeNumberString;
            }
            else
            {
                // 変換表にあてはまらない場合。
                throw new ArgumentOutOfRangeException();
            }
        }

        #endregion
    }
}