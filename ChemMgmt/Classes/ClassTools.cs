﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Reflection;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace ZyCoaG
{
    public class ClassTools<T>
    {
        private T classObject { get; set; }

        public ClassTools(T ClassObject)
        {
            classObject = ClassObject;
        }

        public T Trim()
        {
            var stringProperties = GetProperties(typeof(string));
            Parallel.ForEach(stringProperties, x =>
            {
                if (x.CanWrite) x.SetValue(classObject, x.GetValue(classObject)?.ToString().Trim());
            });
            return classObject;
        }

        public PropertyInfo[] GetProperties()
        {
            return GetPropertiesEntity();
        }

        public PropertyInfo[] GetProperties(Type Type)
        {
            return GetPropertiesEntity(Type);
        }

        private PropertyInfo[] GetPropertiesEntity(Type Type = null)
        {
            var properties = classObject.GetType().GetProperties();
            if (Type != null) properties = properties.Where(x => x.GetType().Equals(Type)).ToArray();
            return properties;
        }

        //public T GetControlValues(Control Control)
        //{
        //    var properties = GetProperties();
        //    foreach (var property in properties)
        //    {
        //        {
        //            var control = Control.FindControl(property.Name) as Label;
        //            if (control != null)
        //            {
        //                if (property.CanWrite) property.SetValue(classObject, getValueForProperty(property, control.Text));
        //                continue;
        //            }
        //        }
        //        {
        //            var control = Control.FindControl(property.Name) as TextBox;
        //            if (control != null)
        //            {
        //                GetControlValue(control.Text?.Trim(), control.Text?.Trim(), property);
        //                continue;
        //            }
        //        }
        //        {
        //            var control = Control.FindControl(property.Name) as DropDownList;
        //            if (control != null)
        //            {
        //                if (control.Items.Count > 0 && property.Name == "FIRST_LOC_NAME")
        //                {
        //                    GetControlValue(control.SelectedValue, control.SelectedItem.Text, property);
        //                    continue;
        //                }
        //                else
        //                {
        //                    GetControlValue(control.SelectedValue, control.Text, property);
        //                    continue;
        //                }
        //            }
        //        }
        //        {
        //            var control = Control.FindControl(property.Name) as ComboBox;
        //            if (control != null)
        //            {
        //                if (property.CanWrite) property.SetValue(classObject, control.SelectedItem.Text);
        //                continue;
        //            }
        //        }
        //        {
        //            var control = Control.FindControl(property.Name) as RadioButton;
        //            if (control != null)
        //            {
        //                if (property.CanWrite) property.SetValue(classObject, control.Checked);
        //                continue;
        //            }
        //        }
        //        {
        //            var control = Control.FindControl(property.Name) as RadioButtonList;
        //            if (control != null)
        //            {
        //                GetControlValue(control.SelectedValue, control.Text, property);
        //                continue;
        //            }
        //        }
        //        {
        //            var control = Control.FindControl(property.Name) as CheckBox;
        //            if (control != null)
        //            {
        //                if (property.CanWrite) property.SetValue(classObject, control.Checked);
        //                continue;
        //            }
        //        }
        //        {
        //            var control = Control.FindControl(property.Name) as HiddenField;
        //            if (control != null)
        //            {
        //                if (property.CanWrite) property.SetValue(classObject, getValueForProperty(property, control.Value));
        //                continue;
        //            }
        //        }
        //    }
        //    return classObject;
        //}

        public void GetControlValue(dynamic value, string text, PropertyInfo property)
        {
            if (property.CanWrite)
            {
                if (property.PropertyType == typeof(int))
                {
                    int convertInt;
                    if (int.TryParse(value, out convertInt))
                    {
                        property.SetValue(classObject, convertInt);
                        return;
                    }
                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(int?))
                {
                    int convertInt;
                    if (int.TryParse(value, out convertInt))
                    {
                        property.SetValue(classObject, convertInt);
                        return;
                    }
                    if (string.IsNullOrEmpty(value?.ToString()))
                    {
                        property.SetValue(classObject, null);
                        return;
                    }

                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(decimal))
                {
                    decimal convertDecimal;
                    if (decimal.TryParse(value, out convertDecimal))
                    {
                        property.SetValue(classObject, convertDecimal);
                        return;
                    }
                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(decimal?))
                {
                    decimal convertDecimal;
                    if (decimal.TryParse(value, out convertDecimal))
                    {
                        property.SetValue(classObject, convertDecimal);
                        return;
                    }
                    if (string.IsNullOrEmpty(value?.ToString()))
                    {
                        property.SetValue(classObject, null);
                        return;
                    }

                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(string))
                {
                    property.SetValue(classObject, text);
                    return;
                }
                //if(property.PropertyType == typeof(IEnumerable<CommonDataModel<int?>>))
                //{
                //    if(property.Name == "ddlFIRST_LOC_NAME")
                //    {
                //        var list = (ListItemCollection)value;
                //        property.SetValue(classObject, list.AsQueryable());

                //    }
                //    return;
                //}
                throw new ArgumentException();
            }
            return;
        }

        private object getValueForProperty(PropertyInfo property, object value)
        {
            if (property.CanWrite)
            {
                if (property.PropertyType == typeof(int))
                {
                    int convertInt;
                    if (int.TryParse(value.ToString(), out convertInt))
                    {
                        return convertInt;
                    }
                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(int?))
                {
                    int convertInt;
                    if (int.TryParse(value.ToString(), out convertInt))
                    {
                        return convertInt;
                    }
                    if (string.IsNullOrEmpty(value.ToString()))
                    {
                        return null;
                    }

                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(decimal))
                {
                    decimal convertDecimal;
                    if (decimal.TryParse(value.ToString(), out convertDecimal))
                    {
                        return convertDecimal;
                    }
                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(decimal?))
                {
                    decimal convertDecimal;
                    if (decimal.TryParse(value.ToString(), out convertDecimal))
                    {
                        return convertDecimal;
                    }
                    if (string.IsNullOrEmpty(value.ToString()))
                    {
                        return null;
                    }

                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(DateTime))
                {
                    DateTime convertDateTime;
                    if (DateTime.TryParse(value.ToString(), out convertDateTime))
                    {
                        return convertDateTime;
                    }
                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(DateTime?))
                {
                    DateTime convertDateTime;
                    if (DateTime.TryParse(value.ToString(), out convertDateTime))
                    {
                        return convertDateTime;
                    }
                    if (string.IsNullOrEmpty(value.ToString()))
                    {
                        return null;
                    }

                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(bool))
                {
                    if (value.ToString() == true.ToString())
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (property.PropertyType == typeof(bool?))
                {
                    if (value.ToString() == true.ToString())
                    {
                        return true;
                    }
                    else if (value.ToString() == false.ToString())
                    {
                        return false;
                    }
                    else if (string.IsNullOrEmpty(value.ToString()))
                    {
                        return null;
                    }

                    throw new ArgumentException();
                }
                if (property.PropertyType == typeof(string))
                {
                    return value.ToString()?.Trim();
                }
                if (property.PropertyType == typeof(byte[]))
                {
                    return value;
                }
                throw new ArgumentException();
            }
            return null;
        }


        //Created for custom tree values getting MSCGI
        public string GetCustomControlValues(string Id)
        {
            string ReturnValue = null;
            var properties = GetProperties();
            object obj = null;
            foreach (var property in properties)
            {

                if (Id != null)
                {
                    if (property.CanWrite)
                    {
                        property.SetValue(classObject, obj);
                    }

                    continue;
                }
            }
            return ReturnValue;
        }
    }
}