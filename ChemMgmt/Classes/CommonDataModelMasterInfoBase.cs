﻿using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ChemMgmt.Classes
{
    /// <summary>
    /// ZyCoaGの<see cref="CommonDataModel{T}"/>を使用したマスター情報を取得する基底クラスです。
    /// </summary>
    public abstract class CommonDataModelMasterInfoBase<T> : IDisposable
    {
        /// <summary>
        /// 選択項目に空白を追加するか
        /// </summary>
        protected virtual bool IsAddSpace { get; set; } = true;

        /// <summary>
        /// 全マスター情報を格納します。
        /// </summary>
        protected IEnumerable<CommonDataModel<T>> DataArray { get; set; }

        /// <summary>
        /// <see cref="CommonDataModelMasterInfoBase{T}"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        public CommonDataModelMasterInfoBase()
        {
            DataArray = GetAllData();
        }

        /// <summary>
        /// 選択項目を取得します。
        /// </summary>
        /// <returns>選択項目を<see cref="IEnumerable{CommonDataModel{T}}"/>で返します。</returns>
        public virtual IEnumerable<CommonDataModel<T>> GetSelectList()
        {
            var dataArray = DataArray.Where(x => x.IsDeleted == false && x.Order >= 0).OrderBy(x => x.Order).ThenBy(x => x.Name).ToList();
            if (IsAddSpace) dataArray.Insert(0, new CommonDataModel<T>(default(T), " "));
            return dataArray;
        }

        /// <summary>
        /// 参照項目を取得します。
        /// </summary>
        /// <returns>参照項目を<see cref="IEnumerable{CommonDataModel{T}}"/>で返します。</returns>
        public virtual IEnumerable<CommonDataModel<T>> GetViewList()
        {
            var dataArray = DataArray.OrderBy(x => x.Order).ThenBy(x => x.Name).ToList();
            if (IsAddSpace) dataArray.Insert(0, new CommonDataModel<T>(default(T), " "));
            return dataArray;
        }

        // 20180910 FJ)Sugimoto Add Sta
        /// <summary>
        /// 参照項目を取得します。(承認用)
        /// </summary>
        /// <returns>参照項目を<see cref="IEnumerable{CommonDataModel{T}}"/>で返します。</returns>
        public virtual IEnumerable<CommonDataModel<T>> GetApprovalList()
        {
            var dataArray = DataArray.Where(x => x.IsDeleted == false).OrderBy(x => x.Order).ThenBy(x => x.Name).ToList();
            if (IsAddSpace) dataArray.Insert(0, new CommonDataModel<T>(default(T), " "));
            return dataArray;
        }
        // 20180910 FJ)Sugimoto Add End

        /// <summary>
        /// <para>変換辞書を取得します。</para>
        /// <para>重複したデータが存在した場合、後のデータが優先されます。</para>
        /// <para>基本機能→各社用の用語などのために後のデータを優先しています。</para>
        /// </summary>
        /// <returns>変換辞書を<see cref="IDictionary{T,string}"/>で返します。</returns>
        public virtual IDictionary<T, string> GetDictionary()
        {
            var dic = new Dictionary<T, string>();
            foreach (var data in DataArray)
            {
                if (dic.ContainsKey(data.Id))
                {
                    dic.Remove(data.Id);
                }
                dic.Add(data.Id, data.Name);
            }
            return dic;
        }

        /// <summary>
        /// 全データを取得する基底クラスです。
        /// </summary>
        /// <returns>全データを<see cref="IEnumerable{CommonDataModel{T}}"/>で返します。</returns>
        protected abstract IEnumerable<CommonDataModel<T>> GetAllData();

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    DataArray = null;
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~MasterInfoBase() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}