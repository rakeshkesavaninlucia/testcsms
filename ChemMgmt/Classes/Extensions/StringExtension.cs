﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.VisualBasic;
using System.Text.RegularExpressions;
namespace ChemMgmt.Classes.Extensions
{
    public static class StringExtension
    {
        /// <summary>
        /// 入力必須記号
        /// </summary>
        private static string requiredMark
        {
            get
            {
                return "*";
            }
        }

        /// <summary>
        /// <para>文字を整えます。</para>
        /// <para>全角数字英字記号を半角、半角カナを全角、禁止記号を全角に変換します。</para>
        /// </summary>
        /// <param name="s">整える対象の文字。</param>
        /// <returns>整えた文字。</returns>
        public static string UnityWidth(this string s)
        {
            var unityString = s;

            // 一度全て半角に置換します。
            unityString = Strings.StrConv(unityString, VbStrConv.Narrow, 1041);

            var halfWidthRegex = new Regex(@"[ｦｧ-ﾝﾞﾟ]+");
            unityString = halfWidthRegex.Replace(unityString, fullWidthReplacer);

            return unityString;
        }

        /// <summary>
        /// 正規表現でマッチした文字を全角に変換します。
        /// </summary>
        /// <param name="m">正規表現の結果。</param>
        /// <returns>全角に変換した文字。</returns>
        private static string fullWidthReplacer(Match m)
        {
            return Strings.StrConv(m.Value, VbStrConv.Wide, 1041);
        }

        /// <summary>
        /// 禁則文字を抽出します。
        /// </summary>
        /// <param name="s">抽出する文字。</param>
        /// <returns>禁則文字。</returns>
        public static IEnumerable<string> ExtractProhibited(this string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                yield break;
            }

            IEnumerable<string> characters = new string[] { "|", "^", @"\", "?", "&" };

            foreach (var c in characters)
            {
                if (s.Contains(c)) yield return c;
            }
        }

        /// <summary>
        /// アルファベットのみであるか。
        /// </summary>
        /// <param name="s">検証する文字。</param>
        /// <returns>アルファベットのみの場合はtrue、そうでない場合はfalseを返します。</returns>
        public static bool IsOnlyAlphabet(this string s)
        {
            var r = new Regex(@"[a-zA-Z ]+");
            var match = r.Match(s);
            return s.Length == match.Length;
        }

        /// <summary>
        /// アルファベット・数字・-「・」ハイフン・「 」スペースであるか。
        /// </summary>
        /// <param name="s">検証する文字。</param>
        /// <returns>アルファベットのみの場合はtrue、そうでない場合はfalseを返します。</returns>
        public static bool IsAlphabetOrNumber(this string s)
        {
            var r = new Regex(@"[a-zA-Z0-9 -]+");
            var match = r.Match(s);
            return s.Length == match.Length;
        }

        /// <summary>
        /// メールアドレス形式であるか。
        /// </summary>
        /// <param name="s">検証する文字。</param>
        /// <returns>メールアドレス形式の場合はture、そうでない場合はfalseを返します。</returns>
        public static bool IsMailAddressFormat(this string s)
        {
            try
            {
                var mail = new System.Net.Mail.MailAddress(s);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        /// <summary>
        /// 入力必須マークを追加します。
        /// </summary>
        /// <param name="s">追加する文字列。</param>
        /// <returns>入力必須マークが追加された文字列。</returns>
        public static string AddRequiredMark(this string s)
        {
            return s.Insert(0, requiredMark);
        }

        /// <summary>
        /// 入力必須マークを削除します。
        /// </summary>
        /// <param name="s">削除する文字列。</param>
        /// <returns>入力必須マークが削除された文字列。</returns>
        public static string RemoveRequiredMark(this string s)
        {
            if (string.IsNullOrEmpty(s)) return s;
            if (s.Substring(0, 1) == requiredMark) s = s.Remove(0, 1);
            return s;
        }
    }
}