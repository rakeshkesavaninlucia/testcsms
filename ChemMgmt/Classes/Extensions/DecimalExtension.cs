﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.Extensions
{
    public static class DecimalExtension
    {
        /// <summary>
        /// 小数点以下を0サプレスします。
        /// </summary>
        /// <param name="d">0サプレスする値。</param>
        /// <returns>0サプレスされた値。</returns>
        public static decimal DecimalZeroSuppress(this decimal d)
        {
            var s = string.Format("{0:0.#####################}", d);
            return Convert.ToDecimal(s);
        }
    }
}