﻿using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Classes.Extensions
{
    public static class GridViewExtension
    {
        /// <summary>
        /// <para><see cref="GridView"/>に出力されている情報をCSVファイルに出力します。 </para>
        /// <para><see cref="TemplateField"/>は無視されます。</para> 
        /// </summary>
        /// <param name="g">CSVファイルに出力する<see cref="GridView"/>。</param>
        /// <param name="fileFullPath">CSVファイルパス。</param>
        //public static void OutputCSV(List<CsmsCommonInfo> g, string fileFullPath)
        //{
        //    OutputCSV(g, fileFullPath, false, false);
        //}

        public static void OutputCSV(List<LedgerCommonInfo> g, string fileFullPath, int i)
        {
            OutputCSV(g, fileFullPath, i, false, false);
        }

        public static void OutputCSV(List<LedgerCommonInfo> g1, string fileFullPath, int i, bool isHeaderDataColumnName, bool isDoubleQuotation)
        {
            LedgerCommonInfo g = new LedgerCommonInfo();
            var lines = new List<string>();
            var line = new List<string>();
            g.ChemCommonList = g1;

            if (i == 1)
            {
                using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
                {
                    for (int I = 0; I < g.ChemCommonList.Count; I++)
                    {
                        if (I == 0)
                        {
                            csv.WriteLine("状態" + "," + "申請番号" + "," + "登録番号" + "," + "旧安全衛生番号" + "," + "件数" + "," + "化学物質コード" + "," + "化学物質名" + "," + "商品名" + "," + "件数" + "," + "CAS#" + "," + "メーカー名" + "," + "管理部門" + "," + "申請者" + "," + "NID" + "," + "区分" + "," + "変更・廃止の事由" + "," + "変更・廃止の事由（その他）" + "," + "危険物の類別" + "," + "保管場所" + "," + "箇所" + "," + "最大保管量" + "," + "単位" + "," + "使用場所" + "," + "箇所" + "," + "最大使用量" + "," + "単位" + "," + "形状" + "," + "分類" + "," + "該当法令" + "," + "その他該当法令" + "," + "作業記録必須物質" + "," + "保護具" + "," + "保護具（その他）" + "," + "アセスメント評価値" + "," + "対策前のリスクレベル" + "," + "リスク低減策後アセスメント評価値" + "," + "対策後のリスクレベル" + "," + "SDS(PDF)" + "," + "SDS(URL)" + "," + "未登録化学物質情報" + "," + "備考 ");
                            csv.WriteLine("\"" + g.ChemCommonList[I].STATUS_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_OLD_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_OLD_NO_COUNT + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_CD + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].PRODUCT_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_PRODUCT_COUNT + "\"" + "," + "\"" + g.ChemCommonList[I].CAS_NO + "\"" + "," + "\"" + g.ChemCommonList[I].MAKER_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].GROUP_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].APPLICANT + "\"" + "," + "\"" + g.ChemCommonList[I].NID + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_CLASS_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].REASON + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REASON + "\"" + "," + "\"" + g.ChemCommonList[I].HAZMAT_TYPE + "\"" + "," + "\"" + g.ChemCommonList[I].LOC_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].LOC_COUNT + "\"" + "," + "\"" + g.ChemCommonList[I].AMOUNT + "\"" + "," + "\"" + g.ChemCommonList[I].UNITSIZE + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_COUNT + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_AMOUNT + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_UNITSIZE + "\"" + "," + "\"" + g.ChemCommonList[I].FIGURE_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_CAT + "\"" + "," + "\"" + g.ChemCommonList[I].REG_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REGULATION + "\"" + "," + "\"" + g.ChemCommonList[I].REQUIRED_CHEM + "\"" + "," + "\"" + g.ChemCommonList[I].PROTECTOR_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_PROTECTOR + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_BEFORE_SCORE + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_BEFORE_RANK_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_AFTER_SCORE + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_AFTER_RANK_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].SDS_FILENAME + "\"" + "," + "\"" + g.ChemCommonList[I].SDS_URL + "\"" + "," + "\"" + g.ChemCommonList[I].UNREGISTERED_CHEM_FILENAME + "\"" + "," + "\"" + g.ChemCommonList[I].MEMO + "\"");

                        }
                        else
                        {
                            csv.WriteLine("\"" + g.ChemCommonList[I].STATUS_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_OLD_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_OLD_NO_COUNT + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_CD + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].PRODUCT_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_PRODUCT_COUNT + "\"" + "," + "\"" + g.ChemCommonList[I].CAS_NO + "\"" + "," + "\"" + g.ChemCommonList[I].MAKER_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].GROUP_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].APPLICANT + "\"" + "," + "\"" + g.ChemCommonList[I].NID + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_CLASS_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].REASON + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REASON + "\"" + "," + "\"" + g.ChemCommonList[I].HAZMAT_TYPE + "\"" + "," + "\"" + g.ChemCommonList[I].LOC_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].LOC_COUNT + "\"" + "," + "\"" + g.ChemCommonList[I].AMOUNT + "\"" + "," + "\"" + g.ChemCommonList[I].UNITSIZE + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_COUNT + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_AMOUNT + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_UNITSIZE + "\"" + "," + "\"" + g.ChemCommonList[I].FIGURE_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_CAT + "\"" + "," + "\"" + g.ChemCommonList[I].REG_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REGULATION + "\"" + "," + "\"" + g.ChemCommonList[I].REQUIRED_CHEM + "\"" + "," + "\"" + g.ChemCommonList[I].PROTECTOR_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_PROTECTOR + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_BEFORE_SCORE + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_BEFORE_RANK_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_AFTER_SCORE + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_AFTER_RANK_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].SDS_FILENAME + "\"" + "," + "\"" + g.ChemCommonList[I].SDS_URL + "\"" + "," + "\"" + g.ChemCommonList[I].UNREGISTERED_CHEM_FILENAME + "\"" + "," + "\"" + g.ChemCommonList[I].MEMO + "\"");
                        }
                    }
                }
            }
            else if (i == 2)
            {
                using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
                {
                    for (int I = 0; I < g.ChemCommonList.Count; I++)
                    {
                        if (I == 0)
                        {
                            csv.WriteLine("状態" + "," + "申請番号" + "," + "登録番号" + "," + "化学物質名" + "," + "CAS#" + "," + "メーカー名" + "," + "管理部門" + "," + "申請者" + "," + "NID" + "," + "区分" + "," + "変更・廃止の事由" + "," + "変更・廃止の事由（その他）" + "," + "危険物の類別" + "," + "保管場所" + "," + "最大保管量" + "," + "単位" + "," + "形状" + "," + "分類" + "," + "該当法令" + "," + "その他該当法令" + "," + "作業記録必須物質");
                            csv.WriteLine("\"" + g.ChemCommonList[I].STATUS_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_NO + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CAS_NO + "\"" + "," + "\"" + g.ChemCommonList[I].MAKER_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].GROUP_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].APPLICANT + "\"" + "," + "\"" + g.ChemCommonList[I].NID + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_CLASS_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].REASON + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REASON + "\"" + "," + "\"" + g.ChemCommonList[I].HAZMAT_TYPE + "\"" + "," + "\"" + g.ChemCommonList[I].LOC_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].AMOUNT + "\"" + "," + "\"" + g.ChemCommonList[I].UNITSIZE + "\"" + "," + "\"" + g.ChemCommonList[I].FIGURE_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_CAT + "\"" + "," + "\"" + g.ChemCommonList[I].REG_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REGULATION + "\"" + "," + "\"" + g.ChemCommonList[I].REQUIRED_CHEM + "\"");

                        }
                        else
                        {
                            csv.WriteLine("\"" + g.ChemCommonList[I].STATUS_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_NO + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CAS_NO + "\"" + "," + "\"" + g.ChemCommonList[I].MAKER_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].GROUP_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].APPLICANT + "\"" + "," + "\"" + g.ChemCommonList[I].NID + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_CLASS_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].REASON + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REASON + "\"" + "," + "\"" + g.ChemCommonList[I].HAZMAT_TYPE + "\"" + "," + "\"" + g.ChemCommonList[I].LOC_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].AMOUNT + "\"" + "," + "\"" + g.ChemCommonList[I].UNITSIZE + "\"" + "," + "\"" + g.ChemCommonList[I].FIGURE_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_CAT + "\"" + "," + "\"" + g.ChemCommonList[I].REG_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REGULATION + "\"" + "," + "\"" + g.ChemCommonList[I].REQUIRED_CHEM + "\"");
                        }
                    }
                }
            }
            else if (i == 3)
            {
                using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
                {
                    for (int I = 0; I < g.ChemCommonList.Count; I++)
                    {
                        if (I == 0)
                        {
                            csv.WriteLine("状態" + "," + "申請番号" + "," + "登録番号" + "," + "化学物質名" + "," + "CAS#" + "," + "メーカー名" + "," + "管理部門" + "," + "申請者" + "," + "NID" + "," + "区分" + "," + "変更・廃止の事由" + "," + "変更・廃止の事由（その他）" + "," + "危険物の類別" + "," + "使用場所" + "," + "最大使用量" + "," + "単位" + "," + "労働安全衛生法の用途" + "," + "労働安全衛生法の用途(その他)" + "," + "化学物質取扱責任者(NID)" + "," + "化学物質取扱責任者(氏名)" + "," + "小分け容器の有無" + "," + "装置への投入" + "," + "保護具" + "," + "保護具（その他）" + "," + "措置前点数" + "," + "措置前ランク" + "," + "措置後点数" + "," + "措置後ランク" + "," + "形状" + "," + "分類" + "," + "該当法令" + "," + "その他該当法令" + "," + "作業記録必須物質");
                            csv.WriteLine("\"" + g.ChemCommonList[I].STATUS_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_NO + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CAS_NO + "\"" + "," + "\"" + g.ChemCommonList[I].MAKER_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].GROUP_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].APPLICANT + "\"" + "," + "\"" + g.ChemCommonList[I].NID + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_CLASS_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].REASON + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REASON + "\"" + "," + "\"" + g.ChemCommonList[I].HAZMAT_TYPE + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_AMOUNT + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_UNITSIZE + "\"" + "," + "\"" + g.ChemCommonList[I].ISHA_USAGE_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].ISHA_OTHER_USAGE + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_OPERATION_CHIEF + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_OPERATION_CHIEF_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].SUB_CONTAINER_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].ENTRY_DEVICE + "\"" + "," + "\"" + g.ChemCommonList[I].PROTECTOR_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_PROTECTOR + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_BEFORE_SCORE + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_BEFORE_RANK_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_AFTER_SCORE + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_AFTER_RANK_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].FIGURE_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_CAT + "\"" + "," + "\"" + g.ChemCommonList[I].REG_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REGULATION + "\"" + "," + "\"" + g.ChemCommonList[I].REQUIRED_CHEM + "\"");

                        }
                        else
                        {
                            csv.WriteLine("\"" + g.ChemCommonList[I].STATUS_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_NO + "\"" + "," + "\"" + g.ChemCommonList[I].REG_NO + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CAS_NO + "\"" + "," + "\"" + g.ChemCommonList[I].MAKER_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].GROUP_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].APPLICANT + "\"" + "," + "\"" + g.ChemCommonList[I].NID + "\"" + "," + "\"" + g.ChemCommonList[I].APPLI_CLASS_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].REASON + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REASON + "\"" + "," + "\"" + g.ChemCommonList[I].HAZMAT_TYPE + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_AMOUNT + "\"" + "," + "\"" + g.ChemCommonList[I].USAGE_LOC_UNITSIZE + "\"" + "," + "\"" + g.ChemCommonList[I].ISHA_USAGE_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].ISHA_OTHER_USAGE + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_OPERATION_CHIEF + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_OPERATION_CHIEF_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].SUB_CONTAINER_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].ENTRY_DEVICE + "\"" + "," + "\"" + g.ChemCommonList[I].PROTECTOR_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_PROTECTOR + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_BEFORE_SCORE + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_BEFORE_RANK_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_AFTER_SCORE + "\"" + "," + "\"" + g.ChemCommonList[I].MEASURES_AFTER_RANK_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].FIGURE_NAME + "\"" + "," + "\"" + g.ChemCommonList[I].CHEM_CAT + "\"" + "," + "\"" + g.ChemCommonList[I].REG_TEXT + "\"" + "," + "\"" + g.ChemCommonList[I].OTHER_REGULATION + "\"" + "," + "\"" + g.ChemCommonList[I].REQUIRED_CHEM + "\"");
                        }
                    }
                }
            }
        }

        /// <summary>
        /// <para><see cref="GridView"/>に出力されている情報をCSVファイルに出力します。 </para>
        /// <para><see cref="TemplateField"/>は無視されます。</para> 
        /// </summary>
        /// <param name="g">CSVファイルに出力する<see cref="GridView"/>。</param>
        /// <param name="fileFullPath">CSVファイルパス。</param>
        /// <param name="isHeaderDataColumnName">ヘッダーにはデータ項目名を表示するか。</param>
        /// <param name="isDoubleQuotation">ヘッダー名にダブルクォーテーションを付与するか。</param>
        public static void OutputCSV(List<CsmsCommonInfo> g, string fileFullPath)
        {           
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    string sdsupddate = ""; string upddates = ""; string RegDate = ""; string DelDates = "";
                    if (g[I].SDS_UPD_DATE2 != null)
                    {
                        DateTime regDate = Convert.ToDateTime(g[I].SDS_UPD_DATE);
                        sdsupddate = regDate.ToString("dd-MM-yyyy HH:mm:ss");
                    }

                    if (g[I].UPD_DATE != null)
                    {
                        DateTime updDate = Convert.ToDateTime(g[I].UPD_DATE);
                        upddates = updDate.ToString("dd-MM-yyyy HH:mm:ss");
                    }
                    if (g[I].REG_DATE != null)
                    {
                        DateTime regDate = Convert.ToDateTime(g[I].REG_DATE);
                        RegDate = regDate.ToString("dd-MM-yyyy HH:mm:ss");
                    }

                    if (g[I].DEL_DATE != null)
                    {
                        DateTime DelDate = Convert.ToDateTime(g[I].DEL_DATE);
                        DelDates = DelDate.ToString("dd-MM-yyyy HH:mm:ss");
                    }

                    if (I == 0)
                    {
                        csv.WriteLine("化学物質コード" + "," + "化学物質名" + "," + "分類" + "," + "CAS#" + "," + "鍵管理" + "," + "重量管理" + "," + "形状" + "," + "比重" + "," + "引火点" + "," + "SDS" + "," + "SDS_URL" + "," + "SDS最終更新日" + "," + "備考" + "," + "状態" + "," + "任意項目1" + "," + "任意項目2" + "," + "任意項目3" + "," + "任意項目4" + "," + "任意項目5" + "," + "メーカー" + "," + "商品名" + "," + "容量" + "," + "容量単位" + "," + "商品バーコード" + "," + "SDSリンクURL" + "," + "SDS更新日" + "," + "開封後有効期限" + "," + "法規制" + "," + "区分" + "," + "消防法" + "," + "区分" + "," + "消防法非該当フラグ" + "," + "社内ルール" + "," + "区分" + "," + "GHS分類" + "," + "区分" + "," + "変更中化学物質コード" + "," + "登録者" + "," + "登録日時" + "," + "更新者" + "," + "更新日時 " + "," + "削除者" + "," + "削除日時");
                        csv.WriteLine("\"" + g[I].CHEM_CD + "\"" + "," + "\"" + g[I].CHEM_NAME + "\"" + "," + "\"" + g[I].CHEM_CAT + "\"" + "," + "\"" + g[I].CAS_NO + "\"" + "," + "\"" + g[I].KEY_MGMT_NAME + "\"" + "," + "\"" + g[I].WEIGHT_MGMT_NAME + "\"" + "," + "\"" + g[I].FIGURE_NAME + "\"" + "," + "\"" + g[I].DENSITY + "\"" + "," + "\"" + g[I].FLASH_POINT + "\"" + "," + "\"" + g[I].SDS_FILENAME + "\"" + "," + "\"" + g[I].SDS_URL2 + "\"" + "," + "\"" + sdsupddate + "\"" + "," + "\"" + g[I].MEMO + "\"" + "," + "\"" + g[I].USAGE_STATUS_NAME + "\"" + "," + "\"" + g[I].OPTIONAL1 + "\"" + "," + "\"" + g[I].OPTIONAL2 + "\"" + "," + "\"" + g[I].OPTIONAL3 + "\"" + "," + "\"" + g[I].OPTIONAL4 + "\"" + "," + "\"" + g[I].OPTIONAL5 + "\"" + "," + "\"" + g[I].MAKER_NAME + "\"" + "," + "\"" + g[I].PRODUCT_NAME + "\"" + "," + "\"" + g[I].UNITSIZE_NAME_JOIN + "\"" + "," + "\"" + g[I].UNITSIZE + "\"" + "," + "\"" + g[I].PRODUCT_BARCODE + "\"" + "," + "\"" + g[I].SDS_URL + "\"" + "," + "\"" + g[I].SDS_UPD_DATE + "\"" + "," + "\"" + g[I].EXPIRATION_DATE + "\"" + "," + "\"" + g[I].REGULATION_Text + "\"" + "," + "\"" + g[I].REGULATION_Division_Text + "\"" + "," + "\"" + g[I].Fire_Text + "\"" + "," + "\"" + g[I].Fire_Division + "\"" + "," + "\"" + g[I].Fire_FLAG_Text + "\"" + "," + "\"" + g[I].Office_Text + "\"" + "," + "\"" + g[I].Office_Division_Text + "\"" + "," + "\"" + g[I].GHS_TEXT + "\"" + "," + "\"" + g[I].GHS_Division_Text + "\"" + "," + "\"" + g[I].REF_CHEM_CD + "\"" + "," + "\"" + g[I].REG_USER_CD + "\"" + "," + "\"" + RegDate + "\"" + "," + "\"" + g[I].UPD_USER_CD + "\"" + "," + "\"" + upddates + "\"" + "," + "\"" + g[I].DEL_USER_CD + "\"" + "," + "\"" + DelDates + "\"");

                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].CHEM_CD + "\"" + "," + "\"" + g[I].CHEM_NAME + "\"" + "," + "\"" + g[I].CHEM_CAT + "\"" + "," + "\"" + g[I].CAS_NO + "\"" + "," + "\"" + g[I].KEY_MGMT_NAME + "\"" + "," + "\"" + g[I].WEIGHT_MGMT_NAME + "\"" + "," + "\"" + g[I].FIGURE_NAME + "\"" + "," + "\"" + g[I].DENSITY + "\"" + "," + "\"" + g[I].FLASH_POINT + "\"" + "," + "\"" + g[I].SDS_FILENAME + "\"" + "," + "\"" + g[I].SDS_URL2 + "\"" + "," + "\"" + sdsupddate + "\"" + "," + "\"" + g[I].MEMO + "\"" + "," + "\"" + g[I].USAGE_STATUS_NAME + "\"" + "," + "\"" + g[I].OPTIONAL1 + "\"" + "," + "\"" + g[I].OPTIONAL2 + "\"" + "," + "\"" + g[I].OPTIONAL3 + "\"" + "," + "\"" + g[I].OPTIONAL4 + "\"" + "," + "\"" + g[I].OPTIONAL5 + "\"" + "," + "\"" + g[I].MAKER_NAME + "\"" + "," + "\"" + g[I].PRODUCT_NAME + "\"" + "," + "\"" + g[I].UNITSIZE_NAME_JOIN + "\"" + "," + "\"" + g[I].UNITSIZE + "\"" + "," + "\"" + g[I].PRODUCT_BARCODE + "\"" + "," + "\"" + g[I].SDS_URL + "\"" + "," + "\"" + g[I].SDS_UPD_DATE + "\"" + "," + "\"" + g[I].EXPIRATION_DATE + "\"" + "," + "\"" + g[I].REGULATION_Text + "\"" + "," + "\"" + g[I].REGULATION_Division_Text + "\"" + "," + "\"" + g[I].Fire_Text + "\"" + "," + "\"" + g[I].Fire_Division + "\"" + "," + "\"" + g[I].Fire_FLAG_Text + "\"" + "," + "\"" + g[I].Office_Text + "\"" + "," + "\"" + g[I].Office_Division_Text + "\"" + "," + "\"" + g[I].GHS_TEXT + "\"" + "," + "\"" + g[I].GHS_Division_Text + "\"" + "," + "\"" + g[I].REF_CHEM_CD + "\"" + "," + "\"" + g[I].REG_USER_CD + "\"" + "," + "\"" + RegDate + "\"" + "," + "\"" + g[I].UPD_USER_CD + "\"" + "," + "\"" + upddates + "\"" + "," + "\"" + g[I].DEL_USER_CD + "\"" + "," + "\"" + DelDates + "\"");
                    }
                }
            }
        }

        public static void RegulationOutputCSV(List<M_REGULATION> reg, string fileFullPath)
        {
            var lines = new List<string>();
            var line = new List<string>();
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < reg.Count; I++)
                {
                    if (I == 0)
                    {

                        csv.WriteLine("\"*状態\",\"法規制ID\",\"*分類\",\"*法規制名\",\"法規制名略称\",\"アイコン種別\",\"指定数量\",\"管理単位\",\"*表示順\",\"鍵管理\",\"重量管理\",\"特殊健康診断\",\"作業環境測定\",\"リスクアセスメント対象\",\"点数\",\"作業記録管理\",\"上位法規制名\",\"ばく露作業報告該当物質\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\",\"削除者\",\"削除日時\"");
                        csv.WriteLine("\"" + reg[I].DEL_FLAG2 + "\",\"" + reg[I].REG_TYPE_ID + "\",\"" + reg[I].CLASSIFICATION + "\",\"" + reg[I].REG_TEXT + "\",\"" + reg[I].REG_ACRONYM_TEXT + "\",\"" + reg[I].REG_ICON_NAME + "\",\"" + reg[I].FIRE_SIZE + "\",\"" + reg[I].UNITSIZE_NAME + "\",\"" + reg[I].SORT_ORDER + "\",\"" + reg[I].KEY_MGMT_NAME + "\",\"" + reg[I].WEIGHT_MGMT_NAME + "\",\"" + reg[I].EXAMINATION_FLAG + "\",\"" + reg[I].MEASUREMENT_FLAG + "\",\"" + reg[I].RA_FLAG + "\",\"" + reg[I].MARKS + "\",\"" + reg[I].WORKTIME_MGMT_NAME + "\",\"" + reg[I].P_REG_TYPE_VALUE + "\",\"" + reg[I].EXPOSURE_REPORT_VALUE + "\",\"" + reg[I].REG_USER_NAME + "\",\"" + reg[I].REG_DATE + "\",\"" + reg[I].UPD_USER_NAME + "\",\"" + reg[I].UPD_DATE + "\",\"" + reg[I].DEL_USER_NAME + "\",\"" + reg[I].DEL_DATE + "\"");
                    }
                    else
                    {
                        csv.WriteLine("\"" + reg[I].DEL_FLAG2 + "\",\"" + reg[I].REG_TYPE_ID + "\",\"" + reg[I].CLASSIFICATION + "\",\"" + reg[I].REG_TEXT + "\",\"" + reg[I].REG_ACRONYM_TEXT + "\",\"" + reg[I].REG_ICON_NAME + "\",\"" + reg[I].FIRE_SIZE + "\",\"" + reg[I].UNITSIZE_NAME + "\",\"" + reg[I].SORT_ORDER + "\",\"" + reg[I].KEY_MGMT_NAME + "\",\"" + reg[I].WEIGHT_MGMT_NAME + "\",\"" + reg[I].EXAMINATION_FLAG + "\",\"" + reg[I].MEASUREMENT_FLAG + "\",\"" + reg[I].RA_FLAG + "\",\"" + reg[I].MARKS + "\",\"" + reg[I].WORKTIME_MGMT_NAME + "\",\"" + reg[I].P_REG_TYPE_VALUE + "\",\"" + reg[I].EXPOSURE_REPORT_VALUE + "\",\"" + reg[I].REG_USER_NAME + "\",\"" + reg[I].REG_DATE + "\",\"" + reg[I].UPD_USER_NAME + "\",\"" + reg[I].UPD_DATE + "\",\"" + reg[I].DEL_USER_NAME + "\",\"" + reg[I].DEL_DATE + "\"");
                    }
                }
            }
        }


        /// <summary>
        /// <para><see cref="GridView"/>に出力されている情報をCSVファイルに出力します。 </para>
        /// <para><see cref="TemplateField"/>は無視されます。</para> 
        /// </summary>
        /// <param name="g">CSVファイルに出力する<see cref="GridView"/>。</param>
        /// <param name="fileFullPath">CSVファイルパス。</param>
        public static void OutputCSV(List<WebCommonInfo> g, string fileFullPath)
        {
            OutputCSV(g, fileFullPath, false, false);
        }

        /// <summary>
        /// <para><see cref="GridView"/>に出力されている情報をCSVファイルに出力します。 </para>
        /// <para><see cref="TemplateField"/>は無視されます。</para> 
        /// </summary>
        /// <param name="g">CSVファイルに出力する<see cref="GridView"/>。</param>
        /// <param name="fileFullPath">CSVファイルパス。</param>
        /// <param name="isHeaderDataColumnName">ヘッダーにはデータ項目名を表示するか。</param>
        /// <param name="isDoubleQuotation">ヘッダー名にダブルクォーテーションを付与するか。</param>
        public static void OutputCSV(List<WebCommonInfo> g, string fileFullPath, bool isHeaderDataColumnName, bool isDoubleQuotation)
        {
            var lines = new List<string>();
            var line = new List<string>();
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    if (I == 0)
                    {
                        csv.WriteLine("\"状態\",\"*NID\",\"*社員名\",\"*社員名（ローマ字）\",\"パスワード\",\"*部署名\",\"上長\",\"代理者\",\"内線番号\",\"*メールアドレス\",\"前回部署\",\"*フロー名\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\",\"管理者権限\",\"削除者\",\"削除日時\",\"バッチ更新対象外\"");
                        csv.WriteLine("\"" + g[I].DEL_FLAGNAME + "\",\"" + g[I].USER_CD + "\",\"" + g[I].USER_NAME + "\",\"" + g[I].USER_NAME_KANA + "\",\"" + g[I].PASSWORD + "\",\"" + g[I].GROUP_NAME + "\",\"" + g[I].APPROVER1 + "\",\"" + g[I].APPROVER2 + "\",\"" + g[I].TEL + "\",\"" + g[I].EMAIL + "\",\"" + g[I].PREVIOUS_GROUP_NM + "\",\"" + g[I].USAGE + "\",\"" + g[I].REG_USER + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].ADMIN_FLAG + "\",\"" + g[I].DEL_USER + "\",\"" + g[I].DEL_DATE + "\",\"" + g[I].BATCH_FLAG + "\"");
                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].DEL_FLAGNAME + "\",\"" + g[I].USER_CD + "\",\"" + g[I].USER_NAME + "\",\"" + g[I].USER_NAME_KANA + "\",\"" + g[I].PASSWORD + "\",\"" + g[I].GROUP_NAME + "\",\"" + g[I].APPROVER1 + "\",\"" + g[I].APPROVER2 + "\",\"" + g[I].TEL + "\",\"" + g[I].EMAIL + "\",\"" + g[I].PREVIOUS_GROUP_NM + "\",\"" + g[I].USAGE + "\",\"" + g[I].REG_USER + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].ADMIN_FLAG + "\",\"" + g[I].DEL_USER + "\",\"" + g[I].DEL_DATE + "\",\"" + g[I].BATCH_FLAG + "\"");
                    }
                }
            }
        }
        public static void MakerOutputCSV(List<M_MAKER> g, string fileFullPath)
        {
            List<string> lines = new List<string>();
            List<string> line = new List<string>();
            using (StreamWriter csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    if (I == 0)
                    {
                        csv.WriteLine("\"*状態\",\"メーカーID\",\"*メーカー名\",\"URL\",\"電話番号\",\"メールアドレス1\",\"メールアドレス2\",\"メールアドレス3\",\"住所\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\",\"削除者\",\"削除日時\"");
                        csv.WriteLine("\"" + g[I].DEL_FLAG2 + "\",\"" + g[I].MAKER_ID + "\",\"" + g[I].MAKER_NAME + "\",\"" + g[I].MAKER_URL + "\",\"" + g[I].MAKER_TEL + "\",\"" + g[I].MAKER_EMAIL1 + "\",\"" + g[I].MAKER_EMAIL2 + "\",\"" + g[I].MAKER_EMAIL3 + "\",\"" + g[I].MAKER_ADDRESS + "\",\"" + g[I].REG_USER_CD + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_CD + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_CD + "\",\"" + g[I].DEL_DATE + "\"");

                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].DEL_FLAG2 + "\",\"" + g[I].MAKER_ID + "\",\"" + g[I].MAKER_NAME + "\",\"" + g[I].MAKER_URL + "\",\"" + g[I].MAKER_TEL + "\",\"" + g[I].MAKER_EMAIL1 + "\",\"" + g[I].MAKER_EMAIL2 + "\",\"" + g[I].MAKER_EMAIL3 + "\",\"" + g[I].MAKER_ADDRESS + "\",\"" + g[I].REG_USER_CD + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_CD + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_CD + "\",\"" + g[I].DEL_DATE + "\"");
                    }
                }
            }
        }

        public static void GroupOutputCSV(List<M_GROUP> g, string fileFullPath)
        {
            var lines = new List<string>();
            var line = new List<string>();
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    if (I == 0)
                    {
                        csv.WriteLine("\"*状態\",\"部署ID\",\"*部署コード\",\"*部署名\",\"上位部署名\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\",\"削除者\",\"削除日時\",\"バッチ更新対象外\"");
                        csv.WriteLine("\"" + g[I].DEL_FLAGNAME + "\",\"" + g[I].GROUP_ID + "\",\"" + g[I].GROUP_CD + "\",\"" + g[I].GROUP_NAME + "\",\"" + g[I].P_GROUP_NAME + "\",\"" + g[I].REG_USER_NM + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_NM + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_NM + "\",\"" + g[I].DEL_DATE + "\",\"" + g[I].BATCH_FLAGNAME + "\"");
                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].DEL_FLAGNAME + "\",\"" + g[I].GROUP_ID + "\",\"" + g[I].GROUP_CD + "\",\"" + g[I].GROUP_NAME + "\",\"" + g[I].P_GROUP_NAME + "\",\"" + g[I].REG_USER_NM + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_NM + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_NM + "\",\"" + g[I].DEL_DATE + "\",\"" + g[I].BATCH_FLAGNAME + "\"");
                    }
                }
            }
        }

        public static void WorkflowOutputCSV(List<M_WORKFLOW> g, string fileFullPath)
        {
            var lines = new List<string>();
            var line = new List<string>();
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    if (I == 0)
                    {
                        csv.WriteLine("\"*状態\",\"フローID\",\"フローコード\",\"*フロー種別\",\"*フロー名\",\"フロー階層\",\"*承認階層1\",\"*承認者1\",\"代行者1\",\"承認階層2\",\"承認者2\",\"代行者2\",\"承認階層3\",\"承認者3\",\"代行者3\",\"承認階層4\",\"承認者4\",\"代行者4\",\"承認階層5\",\"承認者5\",\"代行者5\",\"承認階層6\",\"承認者6\",\"代行者6\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\",\"削除者\",\"削除日時\"");
                        csv.WriteLine("\"" + g[I].DEL_FLAGNAME + "\",\"" + g[I].FLOW_ID + "\",\"" + g[I].FLOW_CD + "\",\"" + g[I].FLOW_NAME + "\",\"" + g[I].USAGE + "\",\"" + g[I].FLOW_HIERARCHY + "\",\"" + g[I].APPROVAL_TARGET1 + "\",\"" + g[I].APPROVAL_USER_CD1 + "\",\"" + g[I].SUBSITUTE_USER_CD1 + "\",\"" + g[I].APPROVAL_TARGET2 + "\",\"" + g[I].APPROVAL_USER_CD2 + "\",\"" + g[I].SUBSITUTE_USER_CD2 + "\",\"" + g[I].APPROVAL_TARGET3 + "\",\"" + g[I].APPROVAL_USER_CD3 + "\",\"" + g[I].SUBSITUTE_USER_CD3 + "\",\"" + g[I].APPROVAL_TARGET4 + "\",\"" + g[I].APPROVAL_USER_CD4 + "\",\"" + g[I].SUBSITUTE_USER_CD4 + "\",\"" + g[I].APPROVAL_TARGET5 + "\",\"" +  g[I].APPROVAL_USER_CD5 + "\",\"" + g[I].SUBSITUTE_USER_CD5 + "\",\"" + g[I].APPROVAL_TARGET6 + "\",\"" + g[I].APPROVAL_USER_CD6 + "\",\"" + g[I].SUBSITUTE_USER_CD6 + "\",\"" +  g[I].REG_USER_NM + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_NM + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_NM + "\",\"" + g[I].DEL_DATE + "\"");
                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].DEL_FLAGNAME + "\",\"" + g[I].FLOW_ID + "\",\"" + g[I].FLOW_CD + "\",\"" + g[I].FLOW_NAME + "\",\"" + g[I].USAGE + "\",\"" + g[I].FLOW_HIERARCHY + "\",\"" + g[I].APPROVAL_TARGET1 + "\",\"" + g[I].APPROVAL_USER_CD1 + "\",\"" + g[I].SUBSITUTE_USER_CD1 + "\",\"" + g[I].APPROVAL_TARGET2 + "\",\"" + g[I].APPROVAL_USER_CD2 + "\",\"" + g[I].SUBSITUTE_USER_CD2 + "\",\"" + g[I].APPROVAL_TARGET3 + "\",\"" + g[I].APPROVAL_USER_CD3 + "\",\"" + g[I].SUBSITUTE_USER_CD3 + "\",\"" + g[I].APPROVAL_TARGET4 + "\",\"" + g[I].APPROVAL_USER_CD4 + "\",\"" + g[I].SUBSITUTE_USER_CD4 + "\",\"" + g[I].APPROVAL_TARGET5 + "\",\"" + g[I].APPROVAL_USER_CD5 + "\",\"" + g[I].SUBSITUTE_USER_CD5 + "\",\"" + g[I].APPROVAL_TARGET6 + "\",\"" + g[I].APPROVAL_USER_CD6 + "\",\"" + g[I].SUBSITUTE_USER_CD6 + "\",\"" + g[I].REG_USER_NM + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_NM + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_NM + "\",\"" + g[I].DEL_DATE + "\"");
                    }
                }
            }
        }

        public static void LocationOutputCSV(List<M_LOCATION> g, string fileFullPath)
        {
            var lines = new List<string>();
            var line = new List<string>();
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    if (I == 0)
                    {
                        csv.WriteLine("\"*状態\",\"保管場所ID\",\"*分類\",\"保管場所コード\",\"*保管場所名\",\"*表示順\",\"上位保管場所ID\",\"上位保管場所名\",\"選択可否\",\"保管可能危険物\",\"倍数1倍超チェック有無\",\"化学物質取扱責任者(NID)\",\"化学物質取扱責任者(氏名)\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\",\"削除者\",\"削除日時\"");
                        csv.WriteLine("\"" + g[I].DEL_FLAGNAME + "\",\"" + g[I].LOC_ID + "\",\"" + g[I].CLASS_NAME + "\",\"" + g[I].LOC_BARCODE + "\",\"" + g[I].LOC_NAME + "\",\"" + g[I].SORT_LEVEL + "\",\"" + g[I].P_LOC_ID + "\",\"" + g[I].P_LOC_NAME + "\",\"" + g[I].SEL_FLAGNAME + "\",\"" + g[I].REG_TEXT + "\",\"" + g[I].OVER_CHECK_FLAGNAME + "\",\"" + g[I].USER_CD + "\",\"" + g[I].USER_NAME + "\",\"" + g[I].REG_USER_NM + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_NM + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_NM + "\",\"" + g[I].DEL_DATE + "\"");
                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].DEL_FLAGNAME + "\",\"" + g[I].LOC_ID + "\",\"" + g[I].CLASS_NAME + "\",\"" + g[I].LOC_BARCODE + "\",\"" + g[I].LOC_NAME + "\",\"" + g[I].SORT_LEVEL + "\",\"" + g[I].P_LOC_ID + "\",\"" + g[I].P_LOC_NAME + "\",\"" + g[I].SEL_FLAGNAME + "\",\"" + g[I].REG_TEXT + "\",\"" + g[I].OVER_CHECK_FLAGNAME + "\",\"" + g[I].USER_CD + "\",\"" + g[I].USER_NAME + "\",\"" + g[I].REG_USER_NM + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_NM + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_NM + "\",\"" + g[I].DEL_DATE + "\"");
                    }
                }
            }
        }

        public static void UnitOutputCSV(List<M_UNITSIZE> g, string fileFullPath, bool isHeaderDataColumnName, bool isDoubleQuotation)
        {
            List<string> lines = new List<string>();
            List<string> line = new List<string>();
            using (StreamWriter csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    if (I == 0)
                    {
                        csv.WriteLine("\"*状態\",\"単位ID\",\"単位名称\",\"変換係数\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\",\"削除者\",\"削除日時\"");
                        csv.WriteLine("\"" + g[I].DEL_FLAG2 + "\",\"" + g[I].UNITSIZE_ID + "\",\"" + g[I].UNIT_NAME + "\",\"" + g[I].FACTOR + "\",\"" + g[I].REG_USER_CD + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_CD + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_CD + "\",\"" + g[I].DEL_DATE + "\"");

                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].DEL_FLAG2 + "\",\"" + g[I].UNITSIZE_ID + "\",\"" + g[I].UNIT_NAME + "\",\"" + g[I].FACTOR + "\",\"" + g[I].REG_USER_CD + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_CD + "\",\"" + g[I].UPD_DATE + "\",\"" + g[I].DEL_USER_CD + "\",\"" + g[I].DEL_DATE + "\"");
                    }
                }
            }
        }
        public static void ActionOutputCSV(List<M_ACTION> g, string fileFullPath, bool isHeaderDataColumnName, bool isDoubleQuotation)
        {
            List<string> lines = new List<string>();
            List<string> line = new List<string>();
            using (StreamWriter csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    if (I == 0)
                    {
                        csv.WriteLine("\"操作タイプID\",\"*操作タイプテキスト\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\"");
                        csv.WriteLine("\"" + g[I].ACTION_TYPE_ID + "\",\"" + g[I].ACTION_TEXT + "\",\"" + g[I].REG_USER_CD + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_CD + "\",\"" + g[I].UPD_DATE + "\"");

                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].ACTION_TYPE_ID + "\",\"" + g[I].ACTION_TEXT + "\",\"" + g[I].REG_USER_CD + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_CD + "\",\"" + g[I].UPD_DATE + "\"");
                    }
                }
            }
        }

        public static void StatusOutputCSV(List<M_STATUS> g, string fileFullPath, bool isHeaderDataColumnName, bool isDoubleQuotation)
        {
            List<string> lines = new List<string>();
            List<string> line = new List<string>();
            using (StreamWriter csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    if (I == 0)
                    {
                        csv.WriteLine("\"ステータスID\",\"*区分\",\"*ステータステキスト\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\"");
                        csv.WriteLine("\"" + g[I].STATUS_ID + "\",\"" + g[I].CLASS_ID + "\",\"" + g[I].STATUS_TEXT + "\",\"" + g[I].REG_USER_CD + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_CD + "\",\"" + g[I].UPD_DATE + "\"");

                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].STATUS_ID + "\",\"" + g[I].CLASS_ID + "\",\"" + g[I].STATUS_TEXT + "\",\"" + g[I].REG_USER_CD + "\",\"" + g[I].REG_DATE + "\",\"" + g[I].UPD_USER_CD + "\",\"" + g[I].UPD_DATE + "\"");
                    }
                }
            }
        }

        public static void CommonOutputCSV(List<M_COMMON> common, string fileFullPath)
        {
            var lines = new List<string>();
            var line = new List<string>();
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < common.Count; I++)
                {
                    if (I == 0)
                    {

                        csv.WriteLine("\"汎用ID\",\"*テーブル名\",\"*キー値\",\"*表示値\",\"*表示順\",\"登録者\",\"登録日時\",\"更新者\",\"更新日時\"");
                        csv.WriteLine("\"" + common[I].COMMON_ID + "\",\"" + common[I].TABLE_NAME + "\",\"" + common[I].KEY_VALUE + "\",\"" + common[I].DISPLAY_VALUE + "\",\"" + common[I].DISPLAY_ORDER + "\",\"" + common[I].REG_USER_NAME + "\",\"" + common[I].REG_DATE + "\",\"" + common[I].UPD_USER_NAME + "\",\"" + common[I].UPD_DATE + "\"");
                    }
                    else
                    {
                        csv.WriteLine("\"" + common[I].COMMON_ID + "\",\"" + common[I].TABLE_NAME + "\",\"" + common[I].KEY_VALUE + "\",\"" + common[I].DISPLAY_VALUE + "\",\"" + common[I].DISPLAY_ORDER + "\",\"" + common[I].REG_USER_NAME + "\",\"" + common[I].REG_DATE + "\",\"" + common[I].UPD_USER_NAME + "\",\"" + common[I].UPD_DATE + "\"");
                    }
                }
            }
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 20-9-2019
        /// Description : This  Method is for generating List output
        /// <summary>
        public static void FireOutputCSV(List<ViewFireDetails> g, string fileFullPath, M_FIRE mFire)
        {
            try
            {
                using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
                {
                    csv.WriteLine("危険物の種類" + "," + "指定数量" + "," + "指定数量単位" + "," + "届出数量" + "," + "届出数量単位" + "," + "倍数");

                    List<tblFireClass> tblFires = new List<tblFireClass>();

                    if (mFire.TYPE == "New")
                    {
                        if (HttpContext.Current.Session["TBLFIRECLASS"] != null)
                        {

                            tblFires = (List<tblFireClass>)HttpContext.Current.Session["TBLFIRECLASS"];
                        }
                    }
                    else
                    {
                        if (HttpContext.Current.Session["TblFireListUpd"] != null)
                        {

                            tblFires = (List<tblFireClass>)HttpContext.Current.Session["TblFireListUpd"];
                        }
                    }

                    int j = 0;

                    for (int I = 0; I < g.Count; I++)
                    {
                        //if (I == j)
                        //{
                        for (j = 0; j < tblFires.Count; j++)
                        {
                            if (tblFires[j].hdnRegTypeId == g[I].fireId)
                            {
                                g[I].txtFireSize = tblFires[j].txtFireSize;
                                g[I].lblMultiple = tblFires[j].mulFirSize;

                            }
                        }
                    }

                    for (int I = 0; I < g.Count; I++)
                    {
                        csv.WriteLine("\"" + g[I].fireName + "\"" + "," + "\"" + g[I].fireSize + "\"" + "," + "\"" + g[I].unitSize + "\"" + "," + "\"" + g[I].txtFireSize + "\"" + "," + "\"" + g[I].unitSize + "\"" + "," + "\"" + g[I].lblMultiple + "\"");

                    }
                }

            }
            catch (Exception ex)
            {
                //LogUtil logUtil = new LogUtil();
                ex.StackTrace.ToString();
                //logUtil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
        }
    }
}