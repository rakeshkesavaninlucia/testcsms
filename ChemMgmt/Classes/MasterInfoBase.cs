﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemMgmt.Classes
{
    /// <summary>
    /// ZyCoaGのマスター情報を取得する基底クラスです。
    /// </summary>
    public abstract class MasterInfoBase<T> : IDisposable
    {
        /// <summary>
        /// 全マスター情報を格納します。
        /// </summary>
        protected IEnumerable<T> DataArray { get; set; }

        /// <summary>
        /// <see cref="CommonDataModelMasterInfoBase{T}"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        public MasterInfoBase()
        {
            DataArray = GetAllData();
        }

        /// <summary>
        /// 選択項目を取得します。
        /// </summary>
        /// <returns>選択項目を<see cref="IEnumerable{T}"/>で返します。</returns>
        public virtual IEnumerable<T> GetSelectList()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 変換辞書を取得します。
        /// </summary>
        /// <returns>変換辞書を<see cref="IDictionary{dynamic, T}"/>で返します。</returns>
        public virtual IDictionary<dynamic, T> GetDictionary()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 全データを取得する基底クラスです。
        /// </summary>
        /// <returns>全データを<see cref="IEnumerable{T}"/>で返します。</returns>
        protected abstract IEnumerable<T> GetAllData();

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    DataArray = null;
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~MasterInfoBase() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
