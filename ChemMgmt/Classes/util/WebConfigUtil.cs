﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ChemDesign.Classes.Util
{
    /// <summary>
    /// WebConfigの操作Utilクラスです。
    /// </summary>
    public class WebConfigUtil
    {
        #region 定数
        // WebConfig内のタグの種類を表すIndex
        private const int cTAGINDEX_CONNECTION_STRING = 0;
        private const int cTAGINDEX_APP_SETTINGS = 1;
        public const string cOUT_ERRLOGMSG_FLG_OFF = "0";
        public const string cOUT_ERRLOGMSG_FLG_ON = "1";
        #endregion

        #region Property
        /// <summary>
        /// SqlServerConnectionString
        /// </summary>
        public static string SQLSERVER_CONNECTION_STRING
        {
            get { return GetWebConfig("SqlServerConnectionString", cTAGINDEX_CONNECTION_STRING).ToString(); }
        }
        /// <summary>
        /// OracleConnectionString
        /// </summary>
        public static string ORACLE_CONNECTION_STRING
        {
            get { return GetWebConfig("OracleConnectionString", cTAGINDEX_CONNECTION_STRING).ToString(); }
        }
        /// <summary>
        /// データベースの種類
        /// </summary>
        public static string DATABASE_TYPE
        {
            get { return GetWebConfig("DatabaseType", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// グリッドのテンプレート格納フォルダ
        /// </summary>
        public static string GRID_TEMPLATE_FOLDER
        {
            get { return GetWebConfig("GridTemplateFolder", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// 試薬管理重量管理フラグ 0:重量管理なし 1:重量管理あり
        /// </summary>
        /// <remarks>
        ///  左から初回、貸出、返却、廃棄、使切
        /// </remarks>
        public static string MANAGE_WEIGHT
        {
            get { return GetWebConfig("MANAGE_WEIGHT", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// LDAP：接続先
        /// </summary>
        public static string LDAP_CONNECTION_STRING
        {
            get { return GetWebConfig("LDAPConnectionString", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// LDAP：接続文字列
        /// </summary>
        public static string USER_BASEDN_STRING
        {
            get { return GetWebConfig("UserBaseDNString", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// LDAP：認証方式
        /// </summary>
        public static string AUTHENTICATION_TYPE
        {
            get { return GetWebConfig("authenticationType", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// 検索確認件数：購入試薬検索
        /// </summary>
        public static long ALERT_RESULT_COUNT_CATALOG
        {
            get { return Convert.ToInt64(GetWebConfig("ALERT_RESULT_COUNT_CATALOG", cTAGINDEX_APP_SETTINGS)); }
        }
        /// <summary>
        /// 検索確認件数：在庫検索
        /// </summary>
        public static long ALERT_RESULT_COUNT_STOCK
        {
            get { return Convert.ToInt64(GetWebConfig("ALERT_RESULT_COUNT_STOCK", cTAGINDEX_APP_SETTINGS)); }
        }
        /// <summary>
        /// 検索確認件数：発注履歴検索
        /// </summary>
        public static long ALERT_RESULT_COUNT_HISTORY
        {
            get { return Convert.ToInt64(GetWebConfig("ALERT_RESULT_COUNT_HISTORY", cTAGINDEX_APP_SETTINGS)); }
        }
        /// <summary>
        /// 検索最大件数：購入試薬検索
        /// </summary>
        public static long LIMIT_RESULT_COUNT_CATALOG
        {
            get { return Convert.ToInt64(GetWebConfig("LIMIT_RESULT_COUNT_CATALOG", cTAGINDEX_APP_SETTINGS)); }
        }
        /// <summary>
        /// 検索最大件数：在庫検索
        /// </summary>
        public static long LIMIT_RESULT_COUNT_STOCK
        {
            get { return Convert.ToInt64(GetWebConfig("LIMIT_RESULT_COUNT_STOCK", cTAGINDEX_APP_SETTINGS)); }
        }
        /// <summary>
        /// 検索最大件数：発注履歴検索
        /// </summary>
        public static long LIMIT_RESULT_COUNT_HISTORY
        {
            get { return Convert.ToInt64(GetWebConfig("LIMIT_RESULT_COUNT_HISTORY", cTAGINDEX_APP_SETTINGS)); }
        }
        /// <summary>
        /// 検索確認ダイアログ表示あり・なし
        /// </summary>
        public static string ALERT_RESULT_DIALOG
        {
            get { return GetWebConfig("ALERT_RESULT_DIALOG", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// 上限到達検索確認ダイアログ表示あり・なし
        /// </summary>
        public static string LIMIT_RESULT_DIALOG
        {
            get { return GetWebConfig("LIMIT_RESULT_DIALOG", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// メール：タイトル（共通）
        /// </summary>
        public static string MAIL_SUB
        {
            get { return GetWebConfig("MAIL_SUB", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// メール：タイトル（ショッピングカート）
        /// </summary>
        public static string MAIL_SUB_ZC02101
        {
            get { return GetWebConfig("MAIL_SUB_ZC02101", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// メール：本文存在パス
        /// </summary>
        public static string MAIL_TXT_PATH
        {
            get { return GetWebConfig("MAIL_TXT_PATH", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// メール：本文存在パス（ショッピングカート)
        /// </summary>
        public static string MAIL_BODY_ZC02101
        {
            get { return GetWebConfig("MAIL_BODY_ZC02101", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// UTF8(html)
        /// </summary>
        public static string CODEPAGE_UTF8
        {
            get { return GetWebConfig("CODEPAGE_UTF8", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// SJIS
        /// </summary>
        public static string CODEPAGE_SJIS
        {
            get { return GetWebConfig("CODEPAGE_SJIS", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// 初期値：指図コード
        /// </summary>
        public static string PROJECT_NAME_INIT_TEXT
        {
            get { return GetWebConfig("PROJECT_NAME_INIT_TEXT", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// 勘定科目名1：受領画面
        /// </summary>
        public static string KANJYO_NAME1
        {
            get { return GetWebConfig("KANJYO_NAME1", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// 勘定科目名2：受領画面
        /// </summary>
        public static string KANJYO_NAME2
        {
            get { return GetWebConfig("KANJYO_NAME2", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// メーカー名：その他
        /// </summary>
        public static string MAKER_NAME_OTHERS
        {
            get { return GetWebConfig("MAKER_NAME_OTHERS", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// 発注代理店：その他
        /// </summary>
        public static string VENDOR_ID_ETC
        {
            get { return GetWebConfig("VENDOR_ID_ETC", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// 通貨の半角、全角変換（：で区切った左側が半角、右側が全角とする）
        /// </summary>
        public static string CONV_CURR
        {
            get { return GetWebConfig("CONV_CURR", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// CRAISチェッカー：設定レベル
        /// </summary>
        public static string CRAIS_CHECKLEVEL
        {
            get { return GetWebConfig("CRAIS_CHECKLEVEL", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// CRAISチェッカー：APP名
        /// </summary>
        public static string CRAIS_APPNAME
        {
            get { return GetWebConfig("CRAIS_APPNAME", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// CRAISチェッカー：キー名
        /// </summary>
        public static string CRAIS_KEYFIELD
        {
            get { return GetWebConfig("CRAIS_KEYFIELD", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// CRAISチェッカー：URL
        /// </summary>
        public static string CRAIS_URL
        {
            get { return GetWebConfig("CRAIS_URL", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// 子画面プログラムID
        /// </summary>
        public static string CHILD_PROGRAM_ID
        {
            get { return GetWebConfig("CHILD_PROGRAM_ID", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        /// <summary>
        /// エラーログメッセージ出力フラグ。このフラグによって出力する、しないを決定するメッセージのみ対応。
        /// </summary>
        public static string OUT_ERRLOGMSG_FLG
        {
            get { return GetWebConfig("OUT_ERRLOGMSG_FLG", cTAGINDEX_APP_SETTINGS).ToString(); }
        }

        /// <summary>
        /// 消防法の計算用の閾値。
        /// </summary>
        public static string FIRE_CHECK_THRESHOLD
        {
            get { return GetWebConfig("FIRE_CHECK_THRESHOLD", cTAGINDEX_APP_SETTINGS).ToString(); }
        }
        #endregion

        #region Method
        /// <summary>
        /// WebConfigから指定した項目の値を返却します。
        /// </summary>
        /// <param name="itemName">項目名</param>
        /// <param name="tagIndex"> WebConfig内のタグの種類</param>
        /// <returns>取得した値</returns>
        private static object GetWebConfig(string itemName, int tagIndex)
        {
            object ret = null;

            switch (tagIndex)
            {
                case cTAGINDEX_CONNECTION_STRING:
                    ret = ConfigurationManager.ConnectionStrings[itemName];
                    break;
                case cTAGINDEX_APP_SETTINGS:
                    ret = ConfigurationManager.AppSettings[itemName];
                    break;
            }

            if (ret == null)
            {
                ret = "";
            }

            return ret;
        }
        #endregion
    }
}