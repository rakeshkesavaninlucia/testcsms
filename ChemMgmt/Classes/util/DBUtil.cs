﻿using Dapper;
using System;
using System.Data;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Data.SqlClient;
using System.Web.Mvc;
using ChemMgmt.Controllers;
using ZyCoaG.ZC010;

namespace ZyCoaG.Classes.util
{

    /// <summary>
    /// データベースのコネクション情報を保持する
    /// </summary>
    public class DbContext
    {
        /// <summary>
        /// OracleConnection
        /// </summary>
        private IDbConnection connection = null;

        /// <summary>
        /// OracleTransaction
        /// </summary>
        private IDbTransaction transaction = null;

        /// <summary>
        /// コネクション
        /// OracleConnection
        /// </summary>
        public IDbConnection Connection
        {
            set { this.connection = value; }
            get { return this.connection; }
        }

        /// <summary>
        /// トランザクション
        /// OracleTransaction
        /// </summary>
        public IDbTransaction Transaction
        {
            set { this.transaction = value; }
            get { return this.transaction; }
        }

        /// <summary>
        /// コミットする
        /// </summary>
        public void Commit()
        {
            if (this.transaction != null)
            {
                this.transaction.Commit();
                this.transaction = null;
            }
        }

        /// <summary>
        /// ロールバックする
        /// </summary>
        public void Rollback()
        {
            if (this.transaction != null)
            {
                try
                {
                    this.transaction.Rollback();
                    this.transaction.Dispose();
                }
                catch { }
                this.transaction = null;
            }
        }

        /// <summary>
        /// Connectionをクローズする
        /// </summary>
        public void Close()
        {
            // 未Commitがある場合、Rollbackする
            Rollback();

            if (this.connection != null)
            {
                try
                {
                    this.connection.Close();
                    this.connection.Dispose();
                }
                catch { }
                this.connection = null;
            }
        }
    }

    /// <summary>
    /// データベース関連メソッドを提供します。
    /// 基本はOracleです。
    /// </summary>
    public class DbUtil
    {
        public static string strConnectionString;
        private static DbProviderFactory _factory;

        // To open the DB connection to corresponding DB
        private static DbProviderFactory factory
        {
            get
            {
                if (_factory == null)
                {
                    if (Common.DatabaseType == DatabaseType.Oracle)
                    {
                        _factory = DbProviderFactories.GetFactory("Oracle.DataAccess.Client");
                    }
                    if (Common.DatabaseType == DatabaseType.SqlServer)
                    {
                        _factory = DbProviderFactories.GetFactory("System.Data.SqlClient");
                    }
                }
                return _factory;
            }
            set
            {
                _factory = value;
            }
        }


        private static string EscapeString { get; } = "#";

        // For ADO.net Need to be removed finally
        public static DbContext Open(bool bWithTransaction)
        {
            DbContext ctx = new DbContext();

            DbConnection conn = factory.CreateConnection();

            conn.ConnectionString = DbUtil.strConnectionString;

            ctx.Connection = conn;

            ctx.Connection.Open();

            if (bWithTransaction)
            {
                ctx.Transaction = ctx.Connection.BeginTransaction();
            }
            return ctx;
        }

        /// <summary>
        /// 更新系クエリを実行する
        /// (1) Open/トランザクション開始～終了/Closeは、呼び出し側で行ってください。
        /// </summary>
        /// <param name="context"></param>
        /// <param name="strSql"></param>
        /// <param name="paramMap"></param>
        /// <returns></returns>
        public static int execUpdate(DbContext context,
                                     string strSql,
                                     Hashtable paramMap)
        {
            int updCnt = 0;

            using (DbCommand cmd = factory.CreateCommand())
            {
                cmd.Connection = (DbConnection)context.Connection;
                cmd.Transaction = (DbTransaction)context.Transaction;
                cmd.CommandTimeout = 6000;　//2019/05/12 TPE.Rin Add Start

                Type typeFieldInfo = cmd.GetType();
                PropertyInfo pi = typeFieldInfo.GetProperty("BindByName");
                if (pi != null)
                {
                    pi.SetValue(cmd, true, null);
                }

                // パラメータを設定する
                AddParams(cmd, paramMap);

                // SQL文を設定する
                cmd.CommandText = ConvertPerDatabase(strSql, paramMap);

                try
                {
                    // SQLを実行する
                    updCnt = cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return updCnt;
            }
        }

        /// <summary>

        /// Author: MSCGI 
        /// Created date: 15-7-2019
        /// Description : To Open the connection of Database For Dapper.
        /// データベースをオープンする
        /// </summary>
        /// <param name="bWithTransaction"></param>
        /// <returns></returns>

        public static SqlConnection Open()
        {
            SqlConnection connection = new SqlConnection(DbUtil.strConnectionString);
            connection.Open();
            return connection;
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 24-7-2019
        /// Description : Select statement with Dapper parameters and return list.
        /// <summary>
        public static List<T> Select<T>(string strSql, DynamicParameters paramMap)
        {
            SqlConnection connection = Open();
            try
            {
                return Select<T>(connection, strSql, paramMap);
            }
            finally
            {
                connection.Close();
            }
        }

        /// <summary>

        /// Author: MSCGI 
        /// Created date: 24-7-2019
        /// Description : Select Single Record SQL statement with Dapper parameters.
        /// <summary>
        public static T SelectSingle<T>(string strSql, DynamicParameters paramMap)
        {
            SqlConnection connection = Open();
            try
            {
                T tblResult;
                tblResult = connection.QueryFirst<T>(strSql, paramMap, commandType: CommandType.Text);
                return tblResult;
            }
            finally
            {
                connection.Close();
            }
        }



        /// <summary>

        /// Author: MSCGI 
        /// Created date: 24-7-2019
        /// Description : Select statement with Parameters in Dapper and return List.
        /// <summary>        
        public static List<T> Select<T>(SqlConnection connection,
                               string strSql)
        {
            List<T> tblResult = new List<T>();
            try
            {
                tblResult = connection.Query<T>(strSql, commandType: CommandType.Text).ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return tblResult;
        }

        /// <summary>

        /// Author: MSCGI 
        /// Created date: 22-7-2019
        /// Description : Select statement with Dapper parameters and return list.
        /// <summary>
        public static List<T> Select<T>(SqlConnection connection, string strSql, DynamicParameters paramMap)
        {
            List<T> tblResult = new List<T>();
            try
            {
                tblResult = connection.Query<T>(strSql, paramMap, commandType: CommandType.Text, commandTimeout: 60).ToList();
            }

            catch (Exception ex)
            {
                ZC010Controller err = new ZC010Controller();
                err.ZC01090(ex.StackTrace);
            }
            return tblResult;
        }



        /// <summary>

        /// Author: MSCGI 
        /// Created date: 16-7-2019
        /// Description : Update statement with Dapper parameters and return result.
        /// <summary>
        public static int ExecuteUpdateInventory(DbContext ctx, string strSql,
                                    DynamicParameters paramMap)
        {
            using (DbCommand cmd = factory.CreateCommand())
            {
                cmd.Connection = (DbConnection)ctx.Connection;
                cmd.Transaction = (DbTransaction)ctx.Transaction;
                int updatedCounter = 0;
                try
                {
                    // SQL文を設定する
                    cmd.CommandText = strSql;
                    AddParams(cmd, paramMap);

                    // SQLを実行する
                    updatedCounter = cmd.ExecuteNonQuery();


                    //// SQLを実行する
                    //updatedCounter = connection.Execute(strSql, paramMap, commandType: CommandType.Text);
                }
                catch (SqlException ex)
                {
                    ChemMgmt.Classes.util.LogUtil logutil = new ChemMgmt.Classes.util.LogUtil();
                    ex.StackTrace.ToString();
                    logutil.OutErrLog(ex.StackTrace.ToString(), strSql);
                }

                return updatedCounter;
            }
        }

        /// <summary>

        /// Author: MSCGI 
        /// Created date: 16-7-2019
        /// Description : Update statement with Dapper parameters and return result.
        /// <summary>
        public static int ExecuteUpdate(SqlConnection connection,
                                    string strSql,
                                    DynamicParameters paramMap)
        {
            int updatedCounter = 0;
            try
            {
                // SQLを実行する
                updatedCounter = connection.Execute(strSql, paramMap, commandType: CommandType.Text);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return updatedCounter;

        }

        /// <summary>

        /// Author: MSCGI 
        /// Created date: 16-7-2019
        /// Description : Update statement with Dapper parameters and return result.
        /// <summary>
        public static int ExecuteUpdate(string strSql,
                                    DynamicParameters paramMap)
        {
            SqlConnection connection = DbUtil.Open();
            int updatedCounter = 0;

            try
            {
                // SQLを実行する
                updatedCounter = connection.Execute(strSql, paramMap, commandType: CommandType.Text);
                connection.BeginTransaction().Commit();

            }

            catch (SqlException ex)
            {
                ChemMgmt.Classes.util.LogUtil logutil = new ChemMgmt.Classes.util.LogUtil();
                ex.StackTrace.ToString();
                logutil.OutErrLog(ex.StackTrace.ToString(), strSql);
                connection.BeginTransaction().Rollback();

            }
            finally
            {
                connection.Close();
            }


            //catch (NullReferenceException ex)
            //{
            //    ChemMgmt.Classes.util.LogUtil logutil = new ChemMgmt.Classes.util.LogUtil();
            //    ex.StackTrace.ToString();
            //    logutil.OutErrLog(ex.StackTrace.ToString(), strSql);
            //}
            //catch (Exception ex)
            //{
            //    ChemMgmt.Classes.util.LogUtil logutil = new ChemMgmt.Classes.util.LogUtil();
            //    ex.StackTrace.ToString();
            //    logutil.OutErrLog(ex.StackTrace.ToString(), strSql);
            //}

            return updatedCounter;
        }

        /// <summary>

        /// Author: MSCGI 
        /// Created date: 18-7-2019
        /// Description : Execute scalar Update statement with Dapper parameters and return result.
        /// <summary>
        public static int ExecuteUpdateReturn(SqlConnection connection,
                             string strSql,
                             DynamicParameters paramMap)
        {
            int localId = 0;
            try
            {
                // SQLを実行する
                localId = (int)connection.ExecuteScalar(strSql, paramMap, commandType: CommandType.Text);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return localId;
        }

        public static int ExecuteUpdateReturn(string strSql, DynamicParameters paramMap)
        {
            SqlConnection connection = Open();
            try
            {

                return ExecuteUpdateReturn(connection, strSql, paramMap);
            }
            finally
            {
                connection.Close();
            }
        }
        /// <summary>
        /// 指定された検索を行う
        /// </summary>
        /// <paramMap name="strSql"></paramMap>
        /// <returns></returns>
        // ADO.net code need to be removed finally
        public static DataTable Select(string strSql)
        {
            return Select(strSql, null);
        }


        //public static List<T> selectDapper<T>(string strSql)
        //{
        //    return select<T>(strSql, null);
        //}

        /// <summary>
        /// 指定された検索を行う
        /// </summary>
        /// <paramMap name="strSql"></paramMap>
        /// <paramMap name="paramMap"></paramMap>
        /// <returns></returns>
        // ADO.net code need to be removed finally
        public static DataTable Select(string strSql, Hashtable paramMap)
        {
            var context = Open(false);
            try
            {

                return Select(context, strSql, paramMap);
            }
            finally
            {
                context.Close();
            }
        }

        /// <summary>
        /// 指定された検索を行う
        /// </summary>
        /// <paramMap name="context"></paramMap>
        /// <paramMap name="strSql"></paramMap>
        /// <returns></returns>
        // ADO.net code need to be removed finally
        public static DataTable Select(DbContext context, string strSql)
        {
            return Select(context, strSql, null);
        }

        /// <summary>
        /// 指定された検索を行う
        /// </summary>
        /// <paramMap name="context"></paramMap>
        /// <paramMap name="strSql"></paramMap>
        /// <paramMap name="paramMap"></paramMap>
        /// <returns></returns>
        // ADO.net function need to be removed finally
        public static DataTable Select(DbContext context,
                                   string strSql,
                                   Hashtable paramMap)
        {
            DataTable tblResult = new DataTable();

            using (DbCommand cmd = factory.CreateCommand())
            {
                try
                {
                    cmd.Connection = (DbConnection)context.Connection;
                    cmd.Transaction = (DbTransaction)context.Transaction;

                    Type typeFieldInfo = cmd.GetType();
                    PropertyInfo pi = typeFieldInfo.GetProperty("BindByName");
                    if (pi != null)
                    {
                        pi.SetValue(cmd, true, null);
                    }

                    // パラメータを追加する
                    AddParams(cmd, paramMap);

                    cmd.CommandText = ConvertPerDatabase(strSql, paramMap);
                    cmd.CommandTimeout = 6000;　//ここを追加
                    // Adapterを生成する
                    using (DbDataAdapter adapter = factory.CreateDataAdapter())
                    {
                        adapter.SelectCommand = cmd;
                        // データを検索する
                        adapter.Fill(tblResult);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return tblResult;
        }

        /// <summary>
        /// SQLコマンドクラスにパラメータを設定する
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="paramMap"></param>
        // ADO.net
        private static void AddParams(DbCommand cmd, Hashtable paramMap)
        {
            if (paramMap != null)
            {
                foreach (object key in paramMap.Keys)
                {
                    string strKey = key.ToString();

                    DbParameter param = factory.CreateParameter();
                    param.ParameterName = strKey;
                    param.Value = !string.IsNullOrEmpty(paramMap[key]?.ToString()) ? paramMap[key] : DBNull.Value;

                    cmd.Parameters.Add(param);
                }
            }
        }
        //Dapper
        private static void AddParams(DbCommand cmd, DynamicParameters paramMap)
        {
            if (paramMap != null)
            {
                foreach (var key in paramMap.ParameterNames)
                {
                    var pValue = paramMap.Get<dynamic>(key);
                    //var pValue1 = paramMap.Get<dynamic>(value);
                    string strKey = key.ToString();

                    DbParameter param = factory.CreateParameter();
                    param.ParameterName = strKey;
                    param.Value = !string.IsNullOrEmpty(key.ToString());

                    cmd.Parameters.Add(param);
                }
            }
        }

        /// <summary>
        /// 更新系クエリを実行する
        /// (1) Open/トランザクション開始～終了/Closeは、呼び出し側で行ってください。
        /// </summary>
        /// <param name="context"></param>
        /// <param name="strSql"></param>
        /// <param name="paramMap"></param>
        /// <returns></returns>
        /// ADO.net
        public static int ExecuteUpdate(DbContext context,
                                     string strSql,
                                     Hashtable paramMap)
        {
            int updatedCounter = 0;

            using (DbCommand cmd = factory.CreateCommand())
            {
                cmd.Connection = (DbConnection)context.Connection;
                cmd.Transaction = (DbTransaction)context.Transaction;

                Type typeFieldInfo = cmd.GetType();
                PropertyInfo pi = typeFieldInfo.GetProperty("BindByName");
                if (pi != null)
                {
                    pi.SetValue(cmd, true, null);
                }

                // パラメータを設定する
                AddParams(cmd, paramMap);

                // SQL文を設定する
                cmd.CommandText = ConvertPerDatabase(strSql, paramMap);

                try
                {
                    // SQLを実行する
                    updatedCounter = cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return updatedCounter;
            }
        }




        //2018/08/04 TPE Add
        /// <summary>
        /// 更新系クエリを実行する
        /// (1) Open/トランザクション開始～終了/Closeは、呼び出し側で行ってください。
        /// </summary>
        /// <param name="context"></param>
        /// <param name="strSql"></param>
        /// <param name="paramMap"></param>
        /// <returns></returns>
      //ADO.net
        public static int ExecuteUpdateReturn(DbContext context,
                                     string strSql,
                                     Hashtable paramMap)
        {

            using (DbCommand cmd = factory.CreateCommand())
            {
                cmd.Connection = (DbConnection)context.Connection;
                cmd.Transaction = (DbTransaction)context.Transaction;
                int localId = 0;
                Type typeFieldInfo = cmd.GetType();
                PropertyInfo pi = typeFieldInfo.GetProperty("BindByName");
                if (pi != null)
                {
                    pi.SetValue(cmd, true, null);
                }

                // パラメータを設定する
                AddParams(cmd, paramMap);

                // SQL文を設定する
                cmd.CommandText = ConvertPerDatabase(strSql, paramMap);

                try
                {
                    // SQLを実行する
                    localId = (int)cmd.ExecuteScalar();
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                return localId;
            }
        }



        /// <summary>
        /// 更新系クエリを実行する（DbParameter を使用します）
        /// (1) Open/トランザクション開始～終了/Closeは、呼び出し側で行ってください。
        /// </summary>
        /// <param name="context"></param>
        /// <param name="strSql"></param>
        /// <param name="paramMap"></param>
        /// <returns></returns>
        // ADO.net
        public static int ExecuteUpdate(DbContext context,
                                     string strSql,
                                     List<DbParameter> DbParams)
        {
            int updatedCounter = 0;

            using (DbCommand cmd = factory.CreateCommand())
            {
                cmd.Connection = (DbConnection)context.Connection;
                cmd.Transaction = (DbTransaction)context.Transaction;

                Type typeFieldInfo = cmd.GetType();
                PropertyInfo pi = typeFieldInfo.GetProperty("BindByName");
                if (pi != null)
                {
                    pi.SetValue(cmd, true, null);
                }

                // パラメータを設定する
                foreach (DbParameter DbParam in DbParams)
                {
                    cmd.Parameters.Add(DbParam);
                }

                // SQL文を設定する
                cmd.CommandText = ConvertPerDatabase(strSql, new Hashtable());

                // SQLを実行する
                updatedCounter = cmd.ExecuteNonQuery();

                return updatedCounter;
            }
        }


        /// <summary>

        /// Author: MSCGI 
        /// Created date: 22-7-2019
        /// Description : Execute Update statement with Dapper List<DbParameter> parameters and return result.
        /// <summary>
        //need to complete the functionality of adding DbParameter into DynamicParameters
        public static int ExecuteUpdate(SqlConnection connection,
                             string strSql,
                             List<DbParameter> DbParams)
        {
            int updatedCounter = 0;

            DynamicParameters paramMap = new DynamicParameters();
            //Type typeFieldInfo = cmd.GetType();
            //PropertyInfo pi = typeFieldInfo.GetProperty("BindByName");
            //if (pi != null)
            //{
            //    pi.SetValue(cmd, true, null);
            //}

            // パラメータを設定する

            // need to be completed. used by page ZC02202 on ucBtnOK_Click;  M_QUANTITY_USER M_PRODUCT_USER DB Tables are used; insertCatalog and updateCatalog functions are using
            foreach (DbParameter DbParam in DbParams)
            {
                // try pushing DbParams into DynamicParameters
                //  cmd.Parameters.Add(DbParam);
                paramMap.Add(DbParam.ParameterName, DbParam.Value);

            }

            // SQL文を設定する
            //cmd.CommandText = convertPerDatabase(strSql, new Hashtable());

            // SQLを実行する
            updatedCounter = connection.Execute(strSql, paramMap, commandType: CommandType.Text);

            return updatedCounter;

        }

        /// <summary>
        /// Stored Procedure / Function を実行します
        /// </summary>
        /// <param name="context"></param>
        /// <param name="strSql"></param>
        /// <param name="paramMap"></param>
        /// <returns></returns>

        // not used in original code itself. need to examine so no corresponding dapper function is created yet.
        public static void ExecProcedure(DbContext context,
                                        string strProcedure,
                                        List<DbParameter> DbParams)
        {
            using (DbCommand cmd = factory.CreateCommand())
            {
                cmd.Connection = (DbConnection)context.Connection;
                cmd.Transaction = (DbTransaction)context.Transaction;
                cmd.CommandText = strProcedure;
                cmd.CommandType = CommandType.StoredProcedure;

                Type typeFieldInfo = cmd.GetType();
                PropertyInfo pi = typeFieldInfo.GetProperty("BindByName");
                if (pi != null)
                {
                    pi.SetValue(cmd, true, null);
                }

                // パラメータを設定する
                foreach (DbParameter DbParam in DbParams)
                {
                    cmd.Parameters.Add(DbParam);
                }

                // SQLを実行する
                cmd.ExecuteNonQuery();
            }
        }

        //ADO.net
        private static string ConvertPerDatabase(string sql, Hashtable paramMap)
        {
            var sqlAfterConversion = sql;
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                if (paramMap != null)
                {
                    foreach (object key in paramMap.Keys)
                    {
                        // SQL文のOracle用のプレースホルダー(:)を他DB用に置換する。
                        if (Common.DatabaseType == DatabaseType.SqlServer)
                        {
                            sqlAfterConversion = sqlAfterConversion.Replace(":" + key.ToString(), "@" + key.ToString());
                        }
                    }
                }

                sqlAfterConversion = sqlAfterConversion.Replace("USER_NAME", "[USER_NAME]");
                sqlAfterConversion = sqlAfterConversion.Replace("@[USER_NAME]", "@USER_NAME");
                sqlAfterConversion = sqlAfterConversion.Replace("[USER_NAME]_KANA", "USER_NAME_KANA");
                sqlAfterConversion = sqlAfterConversion.Replace("||", "+");
                sqlAfterConversion = sqlAfterConversion.Replace("PASSWORD", "[PASSWORD]");
                sqlAfterConversion = sqlAfterConversion.Replace("IS_[PASSWORD]", "[IS_PASSWORD]");
                sqlAfterConversion = sqlAfterConversion.Replace("@[PASSWORD]", "@PASSWORD");
            }
            return sqlAfterConversion;
        }

        /// <summary>
        /// データベースプロバイダー固有のパラメータークラスを返します
        /// 呼び出し側で詳細な設定をした場合に使用してください
        /// </summary>
        /// <returns></returns>
        public static DbParameter CreateDbParameter()
        {
            return factory.CreateParameter();
        }

        /// <summary>
        /// 文字列値の変換を行う
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static object ConvertStrValue(string val)
        {
            if (val == null || val.Length == 0)
            {
                return DBNull.Value;
            }
            return val;
        }

        /// <summary>
        /// DateTime値の変換を行う
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static object ConvertDateTimeValue(DateTime? val)
        {
            if (val == null)
            {
                return DBNull.Value;
            }
            return (DateTime)val;
        }

        /// <summary>
        /// DBNull値の場合にnull値に変換する
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static object dbn2n(object obj)
        {
            if (obj != null && DBNull.Value.Equals(obj))
            {
                return null;
            }
            return obj;
        }

        /// <summary>
        /// データベースのFLAG(varchar2)をboolに変換する
        /// </summary>
        /// <param name="value">DBの値</param>
        /// <returns></returns>
        public static bool ConvertFlag(dynamic value)
        {
            if (value == null || DBNull.Value.Equals(value))
                //if (value == null || value == DBNull.Value)
                value = (byte)0;

            byte b = Convert.ToByte(value);

            return b == 1 ? true : false;
        }

        /// <summary>
        /// LIKE演算子のエスケープ情報を追加します。
        /// </summary>
        /// <returns>LIKE演算子のエスケープSQL</returns>
        public static string LikeEscapeInfoAdd()
        {
            if (Common.DatabaseType == DatabaseType.Oracle || Common.DatabaseType == DatabaseType.SqlServer)
            {
                return " escape '" + EscapeString + "' ";
            }
            return string.Empty;
        }

        /// <summary>
        /// LIKE演算子のパラメーターをエスケープ文字入りに変換します。
        /// </summary>
        /// <param name="parameter">パラメーター。</param>
        /// <returns>エスケープ文字入りのパラメーター。</returns>
        public static string ConvertIntoEscapeChar(string parameter)
        {
            if (Common.DatabaseType == DatabaseType.Oracle || Common.DatabaseType == DatabaseType.SqlServer)
            {
                return parameter.Replace("%", EscapeString + "%").Replace("_", EscapeString + "_");
            }
            throw new AggregateException();
        }

        /// <summary>
        /// IN条件の作成とパラメーター登録を行います。
        /// </summary>
        /// <param name="items"><see cref="IList{T}"/>形式のパラメーターに登録する値。</param>
        /// <param name="baseName">プレースホルダーのベースとなる名前。</param>
        /// <param name="parameters">パラメーターを登録する<see cref="Hashtable"/>。</param>
        /// <returns></returns>
        public static string CreateInOperator(dynamic items, string baseName, DynamicParameters parameters)
        {
            var inCondition = new List<string>();
            for (var i = 0; i < items.Count; i++)
            {
                inCondition.Add("@" + baseName + i.ToString());
                parameters.Add(baseName + i.ToString(), items[i]);
            }
            return string.Join(",", inCondition);
        }

        /// <summary>

        /// Author: MSCGI 
        /// Created date: 24-7-2019
        /// Description : Select Single Record SQL statement with Dapper parameters.
        /// <summary>
        public static List<T> selectChemSingle<T>(string strSql, DynamicParameters paramMap)
        {
            SqlConnection connection = Open();
            List<T> tblResult = new List<T>();
            try
            {
                tblResult = connection.Query<T>(strSql, paramMap, commandType: CommandType.Text).ToList();

            }
            finally
            {
                connection.Close();
            }
            return tblResult;
        }

        internal static object select(DbContext context, string v, Hashtable hashtable)
        {
            throw new NotImplementedException();
        }
    }
}