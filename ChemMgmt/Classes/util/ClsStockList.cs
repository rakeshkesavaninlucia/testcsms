﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.util
{
    public class ClsStockList
    {
        private List<string> items;

        public List<string> STOCK_ID
        {
            set
            {
                items = value;
            }
            get
            {
                return items;
            }
        }

        //コンストラクタ
        public ClsStockList()
        {
            STOCK_ID = new List<string>();
        }

    }
}