﻿using ChemDesign.Classes.Util;
using ChemMgmt;
using ChemMgmt.Classes.Extensions;
using ChemMgmt.Classes.util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZyCoaG.Classes.util;
using ZyCoaG.util;

namespace ZyCoaG.Classes.util
{
    //2019/02/18 TPE.Sugimoto Add Sta
    class clsConfig
    {
        public static string MAKER_CHECK_TXT;
    }
    //2019/02/18 TPE.Sugimoto Add End

    /// <summary>
    /// 入力チェックメソッド群クラス
    /// </summary>
    public class DataCheckBase
    {
        #region メンバー

        public Page pPage = null;

        #endregion

        #region コンストラクタ

        public DataCheckBase()
        {
            //this.pPage = pPage;
        }

        #endregion

        #region 試薬バーコードチェック (+ 1)
        /// <summary>
        /// <para>試薬バーコードチェック</para>
        /// <para>以下の順に入力チェックをおこなう</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.全角チェック</para>
        /// <para>3.半角カナチェック</para>
        /// <para>4.桁数チェック</para>
        /// <para>5.データ存在チェック（blExistCheck = trueの場合）</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="blExistCheck">データ存在チェック true:チェックする false:チェックしない</param>
        /// <param name="strControlId">ControlID</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual bool CheckBarcode(string strText, string strMsg, bool blRequiredCheck, bool blExistCheck, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 20;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                if (Common.IsValidStr(strText) == false)
                {
                    Common.MsgBox(pPage, Common.GetMessage("C046", strMsg), strControlId);
                    return true;
                }
            }

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                Common.MsgBox(pPage, Common.GetMessage("C011", strMsg), strControlId);
                return true;
            }

            // 半角カナチェック
            if (Common.isHankakuKana(strText))
            {
                Common.MsgBox(pPage, Common.GetMessage("C049", strMsg), strControlId);
                return true;
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { strMsg, "半角" + iMaxLength.ToString() }), strControlId);
                return true;
            }

            // データ存在チェックをする場合
            if (blExistCheck)
            {
                ClsStock _clsStock = new ClsStock();
                _clsStock.GetStockData(strText);
                if (_clsStock.STOCK_COUNT == 0)
                {
                    Common.MsgBox(pPage, Common.GetMessage("C047", strMsg), strControlId);
                    return true;
                }
            }

            return false;
        }
        /// <summary>
        /// 別の注文番号で同一の試薬バーコードが存在するかチェックします
        /// </summary>
        /// <param name="barcode">試薬バーコード</param>
        /// <param name="orderNo">注文番号</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual bool CheckBarcode(ClsStock _ClsStock, params string[] strControlId)
        {
            // 存在チェック
            if (_ClsStock.IsExistBarcode())
            {
                Common.MsgBox(pPage, Common.GetMessage("C023", "瓶番号"), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 容量チェック
        /// <summary>
        /// 容量チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual string CheckUnitSize(ref string strText, bool mandatory, params string[] strControlId)
        {
            string ExecuteStatus = null;
            if (mandatory)
            {
                //入力チェック
                if (Common.IsValidStr(strText) == false)
                {
                    ExecuteStatus= Common.GetMessage("C001", "容量");
                    return ExecuteStatus;
                }
            }

            //数値チェック
            if (Common.IsNumber(strText) == false)
            {
                ExecuteStatus =Common.GetMessage("C010", "容量");
                return ExecuteStatus;
            }

            //小数点第４位チェック
            double dblTxt;
            if (Common.CheckDecimalPointValue(strText, 3, out dblTxt) == false)
            {
                ExecuteStatus=Common.GetMessage("I016", "容量");
                strText = Convert.ToString(dblTxt);
                return ExecuteStatus;
            }

            //正数チェック
            if (Common.IsPositiveNumber(strText) == false)
            {
                ExecuteStatus= Common.GetMessage("C032", "容量");
                return ExecuteStatus;
            }

            //範囲チェック
            if (Convert.ToDouble(strText) >= 1000000000)
            {
                ExecuteStatus= Common.GetMessage("C056", new string[] { "容量", "0.001～999999999.999" });
                return ExecuteStatus;
            }
            return ExecuteStatus;
        }
        #endregion

        #region 保管場所チェック
        /// <summary>
        /// 場所チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual string CheckRoom(string strText, LocationClassId locationClassId, params string[] strControlId)
        {
            string CheckRoom = "";
            var locationName = string.Empty;
            switch (locationClassId)
            {
                case LocationClassId.Storing:
                    locationName = "保管場所";
                    break;
                case LocationClassId.Usage:
                    locationName = "使用場所";
                    break;
                case LocationClassId.All:
                    throw new ArgumentException();
            }

            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                CheckRoom = string.Format(WarningMessages.NotInput, locationName);
                return CheckRoom;
            }

            //存在チェック
            using (var locations = new LocationMasterInfo(locationClassId))
            {

                var location = locations.dataArray.Where(x => x.LOC_ID == Convert.ToInt32(strText));
                //if (location.Count() != 1 || locations.dataArray.Where(x => x.P_LOC_ID == location.Single().LOC_ID && x.DEL_FLAG == 0).Count() != 0)  //2019/06/11 TPE.Rin 選択可否対応
                if (location.Count() != 1 || locations.dataArray.Where(x => x.LOC_ID == location.Single().LOC_ID && x.SEL_FLAG == false).Count() != 1)
                {
                    CheckRoom = string.Format(WarningMessages.NotFound, locationName);
                    return CheckRoom;
                }

                return CheckRoom;
            }
        }

        private string SetLedgerUsageLocations(string regNo)
        {
            string strArrayLocId;

            var sql = $@"SELECT LOC_ID
                         FROM T_MGMT_LED_USAGE_LOC
                         WHERE     REG_NO = :REG_NO
                               AND DEL_FLAG = 0";
            var parameters = new Hashtable
            {
                { "REG_NO", regNo }
            };
            var data = DbUtil.Select(sql, parameters);

            if (data.Rows.Count == 0)
            {
                strArrayLocId = string.Empty;
            }
            else
            {
                System.Collections.Generic.List<string> locationIds = new System.Collections.Generic.List<string>();
                foreach (DataRow row in data.Rows)
                {
                    locationIds.Add(row["LOC_ID"].ToString());
                }
                strArrayLocId = locationIds.Aggregate((x, y) => $"{x}{SystemConst.CodeDelimiter}{y}");
            }

            return strArrayLocId;
        }

        /// <summary>
        /// 使用場所のチェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="locationClassId"></param>
        /// <param name="strControlId"></param>
        /// <returns></returns>
        public virtual string CheckUsageRoom(string strRegNo, string strText, LocationClassId locationClassId, params string[] strControlId)
        {
            string UsageRoom = "";
            var locationName = string.Empty;
            switch (locationClassId)
            {
                case LocationClassId.Storing:
                    locationName = "保管場所";
                    break;
                case LocationClassId.Usage:
                    locationName = "使用場所";
                    break;
                case LocationClassId.All:
                    throw new ArgumentException();
            }

            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                UsageRoom = string.Format(WarningMessages.NotInput, locationName);
                return UsageRoom;
            }

            string strLocation = SetLedgerUsageLocations(strRegNo);

            if (strLocation.Trim() != "")
            {
                string[] strArrayLocId = SetLedgerUsageLocations(strRegNo).Split('|');

                if (strArrayLocId.Where(x => x == strText).Count() != 1)
                {
                    UsageRoom = string.Format(WarningMessages.NotFound, locationName);
                    return UsageRoom;
                }
            }
            else
            {
                using (var locations = new LocationMasterInfo(locationClassId))
                {

                    var location = locations.dataArray.Where(x => x.LOC_ID == Convert.ToInt32(strText));
                    //if (location.Count() != 1 || locations.dataArray.Where(x => x.P_LOC_ID == location.Single().LOC_ID && x.DEL_FLAG == 0).Count() != 0)  //2019/06/11 TPE.Rin Mod 選択可否対応
                    if (location.Count() != 1 || locations.dataArray.Where(x => x.LOC_ID == location.Single().LOC_ID && x.SEL_FLAG == false).Count() != 1)
                    {
                        UsageRoom = string.Format(WarningMessages.NotFound, locationName);
                        return UsageRoom;
                    }
                }
            }

            return UsageRoom;

        }


        /// <summary>
        /// 保管場所チェック
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="_ClsLocation">ロケーションクラス</param>
        /// <param name="strControlId">[0]コントロールID　[1]"1"（入力値クリアフラグ）</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        /// <remarks>出力メッセージ「strTextの保管場所は存在しません。」</remarks>
        public virtual bool CheckLocation(string strText, ClsLocation _ClsLocation, params string[] strControlId)
        {
            //存在チェック
            _ClsLocation.SetLocationInfo(strText);
            if (_ClsLocation.LOC_COUNT == 0)
            {
                Common.MsgBox(pPage, Common.GetMessage("C015", strText + "の保管場所"), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 総重量チェック
        /// <summary>
        /// 総重量チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual string CheckAllWeight(ref string strText, params string[] strControlId)
        {
            string ErrorStatus = null;
            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                ErrorStatus = string.Format(WarningMessages.Required, "総重量");
                return ErrorStatus;
            }

            //数値チェック
            if (Common.IsNumber(strText) == false)
            {
                ErrorStatus = Common.GetMessage("C010", "総重量");
                return ErrorStatus;
            }

            //小数点第４位チェック
            double dblTxt;
            if (Common.CheckDecimalPointValue(strText, 3, out dblTxt) == false)
            {
                ErrorStatus = Common.GetMessage("I016", "総重量");
                strText = Convert.ToString(dblTxt);
                return ErrorStatus;
            }

            //正数チェック
            if (Common.IsPositiveNumber(strText) == false)
            {
                ErrorStatus = Common.GetMessage("C032", "総重量");
                return ErrorStatus;
            }

            var w = strText.Replace("-", string.Empty).Split(".".ToCharArray());
            if (w[0].Length > 8)
            {
                ErrorStatus = string.Format(WarningMessages.PositiveNumberLength, "総重量", 8.ToString());
                return ErrorStatus;
            }

            return ErrorStatus;
        }
        #endregion

        #region 風袋チェック
        /// <summary>
        /// 風袋チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual string CheckBottoleWeight(string strAllWeight, string strUnitSize, params string[] strControlId)
        {
            string ExecuteStatus = null;
            //重量差分チェック
            if ((Convert.ToDecimal(strAllWeight) - Convert.ToDecimal(strUnitSize)) <= 0)
            {
                ExecuteStatus= Common.GetMessage("C048");
                return ExecuteStatus;
            }

            return ExecuteStatus;
        }
        #endregion

        #region 総重量と風袋のチェック
        /// <summary>
        /// 風袋重量が総重量を超えていないかチェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual string CheckAllBottoleWeight(string strAllWeight, string strBottoleWeight, params string[] strControlId)
        {
            string ExecuteStatus = null;
            //重量差分チェック
            if (Convert.ToDecimal(strBottoleWeight) > Convert.ToDecimal(strAllWeight))
            {
                ExecuteStatus = Common.GetMessage("C054");
            }

            return ExecuteStatus;
        }
        #endregion

        #region 発注数チェック
        /// <summary>
        /// 発注数チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckOrderNum(string strText, params string[] strControlId)
        {
            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C001", "受入数"), strControlId);
                return true;
            }

            //数値チェック
            if (Common.IsNumber(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C010", "受入数"), strControlId);
                return true;
            }

            //正数チェック
            if (Common.IsPositiveNumber(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C032", "受入数"), strControlId);
                return true;
            }

            //範囲チェック
            if (Convert.ToDouble(strText) >= 10000)
            {
                Common.MsgBox(pPage, Common.GetMessage("C056", new string[] { "受入数", "1～9999" }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 備考チェック
        /// <summary>
        /// 備考チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckMemo(string strText, int validflg, params string[] strControlId)
        {
            if (validflg == 1)
            {
                //入力チェック
                if (Common.IsValidStr(strText) == false)
                {
                    Common.MsgBox(pPage, Common.GetMessage("C033", "備考"), strControlId);
                    return true;
                }
            }

            // 文字数チェック
            // 3バイト対応 MEMO[VARCHAR2(1000 BYTE)]の為、500 => 333に変更
            if (Common.CheckLengthStrCount(strText, 333) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "備考", "333" }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 備考チェック
        /// <summary>
        /// 備考チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual string CheckMemo(string strText, params string[] strControlId)
        {
            string Memo = "";
            // 文字数チェック
            var memoMaxLength = 1000;
            if (!string.IsNullOrEmpty(strText) && strText.Length > memoMaxLength)
            {
                Memo = string.Format(WarningMessages.MaximumLength, "備考", memoMaxLength.ToString());
                return Memo;
            }

            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                Memo = string.Format(WarningMessages.ContainProhibitionCharacter, "備考", string.Join(" ", p).Replace(@"\", @"\\"));
                return Memo;
            }

            return Memo;
        }

        /// <summary>
        /// 使用目的チェック
        /// </summary>
        /// <param name="otherIntended"></param>
        /// <returns></returns>
        public virtual string CheckIntended(string intended, string otherIntended, params string[] strControlId)
        {
            string Inteneted = "";
            // 文字数チェック
            var maxLength = 1000;
            if (string.IsNullOrWhiteSpace(intended))
            {
                Inteneted = string.Format(WarningMessages.NotInput, "使用目的", maxLength.ToString());
                return Inteneted;
            }

            if (intended == "その他" && string.IsNullOrWhiteSpace(otherIntended))
            {
                Inteneted = string.Format(WarningMessages.NotInput, "使用目的(その他)", maxLength.ToString());
                return Inteneted;
            }

            if (!string.IsNullOrEmpty(otherIntended) && otherIntended.Length > maxLength)
            {
                Inteneted = string.Format(WarningMessages.MaximumLength, "使用目的(その他)", maxLength.ToString());
                return Inteneted;
            }

            var p = otherIntended.ExtractProhibited();
            if (p.Count() > 0)
            {
                Inteneted = string.Format(WarningMessages.ContainProhibitionCharacter, "使用目的(その他)", string.Join(" ", p).Replace(@"\", @"\\"));
                return Inteneted;
            }

            return Inteneted;
        }

        /// <summary>
        /// 作業時間チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual string CheckWorkingTimes(string strText, bool isInputCheck, params string[] strControlId)
        {
            string ErrorMessage = "";
            // 文字数チェック
            var maxLength = 10;
            if (isInputCheck && string.IsNullOrWhiteSpace(strText))
            {
                ErrorMessage = string.Format(WarningMessages.NotInput, "作業時間", maxLength.ToString());
                return ErrorMessage;
            }

            decimal workingTime = default(decimal);
            if (!string.IsNullOrWhiteSpace(strText) && !decimal.TryParse(strText, out workingTime))
            {
                ErrorMessage = string.Format(WarningMessages.NotNumber, "作業時間", maxLength.ToString());
                return ErrorMessage;
            }

            if (!string.IsNullOrEmpty(strText) && strText.Length > maxLength)
            {
                ErrorMessage = string.Format(WarningMessages.MaximumLength, "作業時間", maxLength.ToString());
                return ErrorMessage;
            }

            return ErrorMessage;
        }

        #endregion

        #region 発注番号チェック
        /// <summary>
        /// <para>発注番号チェック</para>
        /// <para>以下の順に入力チェックをおこなう</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.数値チェック</para>
        /// <para>3.正数チェック</para>
        /// <para>4.文字数チェック</para>
        /// <para>5.データ存在チェック（blExistCheck = trueの場合）</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="blExistCheck">データ存在チェック true:チェックする false:チェックしない</param>
        /// <param name="strControlId">ControlID</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual bool CheckOrderNo(string strText, string strMsg, bool blRequiredCheck, bool blExistCheck, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 16;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                // 入力チェック
                if (Common.IsValidStr(strText) == false)
                {
                    Common.MsgBox(pPage, Common.GetMessage("C001", strMsg), strControlId);
                    return true;
                }
            }

            // 数値チェック
            if (Common.IsNumber(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C010", strMsg), strControlId);
                return true;
            }

            // 正数チェック
            if (Common.IsPositiveNumber(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C032", strMsg), strControlId);
                return true;
            }

            // 文字数チェック
            if (Common.GetByteCount(strText) > iMaxLength)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { strMsg, "半角" + iMaxLength.ToString() }), strControlId);
                return true;
            }

            // データ存在チェックをする場合
            if (blExistCheck)
            {
                // 存在チェック
                ClsOrder _ClsOrder = new ClsOrder();
                _ClsOrder.GetOrderData(strText);
                if (_ClsOrder.ORDER_COUNT == 0)
                {
                    Common.MsgBox(pPage, Common.GetMessage("C047", strMsg), strControlId);
                    return true;
                }
            }
            return false;
        }
        #endregion

        #region コピー数チェック
        /// <summary>
        /// <para>コピー数チェック</para>
        /// <para>以下の順に入力チェックをおこなう</para>
        /// <para>1.入力チェック</para>
        /// <para>2.数値チェック</para>
        /// <para>3.正数チェック</para>
        /// <para>4.数値範囲チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="strControlId">ControlID</param>
        /// <returns></returns>
        public virtual bool CheckCopyNum(string strText, string strMsg, params string[] strControlId)
        {
            // 最小桁数
            const int iMinLength = 0;
            // 最大桁数
            const int iMaxLength = 100;

            // 入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C001", strMsg), strControlId);
                return true;
            }

            // 数値チェック
            if (Common.IsNumber(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C010", strMsg), strControlId);
                return true;
            }

            // 正数チェック
            if (Common.IsPositiveNumber(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C032", strMsg), strControlId);
                return true;
            }

            // 数値範囲チェック
            if (Convert.ToInt32(strText) <= iMinLength || Convert.ToInt32(strText) > iMaxLength)
            {
                Common.MsgBox(pPage, Common.GetMessage("C038", strMsg), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 勘定科目名称チェック（コンボボックス用）
        /// <summary>
        /// <para>勘定科目名称チェック（コンボボックス用）</para>
        /// <para>選択したコンボボックスの値をチェックする</para>
        /// </summary>
        /// <param name="strText">選択した値</param>
        /// <param name="strControlId">ControlID</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        /// <remarks>出力メッセージ「勘定科目名称を選択してください。」</remarks>
        public virtual bool CheckKanjyoByCombo(string strText, params string[] strControlId)
        {
            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C017", "勘定科目名称"), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 指図コードチェック（コンボボックス用）
        /// <summary>
        /// <para>指図コードチェック（コンボボックス用）</para>
        /// <para>選択したコンボボックスの値をチェックする</para>
        /// </summary>
        /// <param name="strText">選択した値</param>
        /// <param name="strControlId">ControlID</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        /// <remarks>出力メッセージ「指図コードを選択してください。」</remarks>
        public virtual bool CheckProjectByCombo(string strText, params string[] strControlId)
        {
            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C017", "指図コード"), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 警告チェック（受領画面用）
        /// <summary>
        /// 警告チェック（受領画面用）
        /// </summary>
        /// <param name="KanjyoName">勘定科目名称</param>
        /// <param name="OrderNo">注文番号</param>
        /// <param name="LocBarcode">保管場所コード</param>
        /// <param name="_ListQR">試薬バーコード</param>
        /// <param name="strMsg">エラーメッセージ</param>
        /// <returns>true:警告エラーがある場合　false:警告エラーがない場合</returns>
        /// <remarks>警告メッセージが複数ある場合、メッセージを結合して出力する。</remarks>
        public virtual bool CautionCheckReceive(string KanjyoName, string OrderNo, string LocBarcode, ListBox _ListQR, out string strMsg)
        {
            bool blCheckMsg = false;
            strMsg = string.Empty;

            //勘定科目名称＝「研究器具消耗品費」かつ　単価＞￥１０万 の場合 メッセージを出す。
            if (KanjyoName == WebConfigUtil.KANJYO_NAME1)
            {
                ClsAccountData _ClsAccountData = new ClsAccountData();
                string strMaxPrice = _ClsAccountData.GetMaxPrice(OrderNo);

                if (strMaxPrice != "" && Convert.ToInt32(strMaxPrice) > 100000)
                {
                    strMsg += Common.GetMessage("I009");
                    blCheckMsg = true;
                }
            }
            //勘定科目名称＝「研究材料外部委託費」の場合 メッセージを出す。
            else if (KanjyoName == WebConfigUtil.KANJYO_NAME2)
            {
                strMsg += Common.GetMessage("I010");
                blCheckMsg = true;
            }

            //保管場所未入力 かつ バーコードがSHM以外は警告メッセージを表示する
            if (LocBarcode.Length < 1)
            {
                for (int i = 0; i < _ListQR.Items.Count; i++)
                {
                    if (_ListQR.Items[i].Text.Contains(Const.cBARCODE_COMMON_CONSUMABLES) == false)
                    {
                        strMsg += Common.GetMessage("C031");
                        blCheckMsg = true;
                        break;
                    }
                }
            }
            return blCheckMsg;
        }
        #endregion

        #region ユーザーコードチェック (+ 1)
        /// <summary>
        /// <para>ユーザーコードチェック</para>
        /// <para>以下の順に入力チェックをおこなう</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.データ存在チェック（blExistCheck = trueの場合）</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="blExistCheck">データ存在チェック true:チェックする false:チェックしない</param>
        /// <param name="blExistNG">データ存在チェック true:指定コードの重複はエラー false:指定コードの不在はエラー</param>
        /// <param name="strControlId">[0]コントロールID　[1]"1"（入力値クリアフラグ）</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckUserCD(string strText, bool blRequiredCheck, bool blExistCheck, bool blExistNG, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 30;
            // 出力項目名
            const string strMsg = "社員コード";

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                // 入力チェック
                if (Common.IsValidStr(strText) == false)
                {
                    return Common.GetMessage("C001", strMsg);
                     
                }
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
                
            }

            // 全角・半角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", strMsg);
               
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
               
            }

            // データ存在チェックをする場合
            if (blExistCheck)
            {
                // 存在チェック
                MasterService _MasterService = new MasterService();
                DataTable dtUser = _MasterService.SelectAllUser(strText);
                if (blExistNG)
                {
                    // 対象コードが存在する = NG
                    if (dtUser.Rows.Count > 0)
                    {
                        return Common.GetMessage("C016", new string[] { strText + "のユーザー", dtUser.Rows.Count.ToString() });
                      
                    }
                }
                else
                {
                    // 対象コードが存在しない = NG
                    if (dtUser.Rows.Count < 1)
                    {
                        return Common.GetMessage("C015", strText + "のユーザー");
                      
                    }
                }

            }
            return "";
        }


        //public virtual bool CheckUserCD(string strText, bool blRequiredCheck, bool blExistCheck, bool blExistNG, params string[] strControlId)
        //{
        //    // 最大桁数
        //    const int iMaxLength = 30;
        //    // 出力項目名
        //    const string strMsg = "社員コード";

        //    // 必須入力チェックをする場合
        //    if (blRequiredCheck)
        //    {
        //        // 入力チェック
        //        if (Common.IsValidStr(strText) == false)
        //        {
        //            Common.MsgBox(pPage, Common.GetMessage("C001", strMsg), strControlId);
        //            return true;
        //        }
        //    }

        //    // 文字数チェック
        //    if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
        //    {
        //        Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() }), strControlId);
        //        return true;
        //    }

        //    // 全角・半角チェック
        //    if (Common.isZenkaku(strText))
        //    {
        //        Common.MsgBox(pPage, Common.GetMessage("C011", strMsg), strControlId);
        //        return true;
        //    }

        //    // 禁則文字チェック
        //    var p = strText.ExtractProhibited();
        //    if (p.Count() > 0)
        //    {
        //        Common.MsgBox(pPage, string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\")), strControlId);
        //        return true;
        //    }

        //    // データ存在チェックをする場合
        //    if (blExistCheck)
        //    {
        //        // 存在チェック
        //        MasterService _MasterService = new MasterService();
        //        DataTable dtUser = _MasterService.SelectAllUser(strText);
        //        if (blExistNG)
        //        {
        //            // 対象コードが存在する = NG
        //            if (dtUser.Rows.Count > 0)
        //            {
        //                Common.MsgBox(pPage, Common.GetMessage("C016", new string[] { strText + "のユーザー", dtUser.Rows.Count.ToString() }), strControlId);
        //                return true;
        //            }
        //        }
        //        else
        //        {
        //            // 対象コードが存在しない = NG
        //            if (dtUser.Rows.Count < 1)
        //            {
        //                Common.MsgBox(pPage, Common.GetMessage("C015", strText + "のユーザー"), strControlId);
        //                return true;
        //            }
        //        }

        //    }
        //    return false;
        //}
        /// <summary>
        /// ユーザーコードチェック
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strControlId">[0]コントロールID　[1]"1"（入力値クリアフラグ）</param>
        /// <returns>取得結果格納DataTable</returns>
        public virtual DataTable CheckUserCD(string strText, params string[] strControlId)
        {
            // 存在チェック
            MasterService _MasterService = new MasterService();
            DataTable dtUser = _MasterService.selectUser(strText);
            if (dtUser.Rows.Count < 1)
            {
                Common.MsgBox(pPage, Common.GetMessage("C015", strText + "のユーザー"), strControlId);
            }
            return dtUser;
        }
        #endregion

        #region ユーザーコードチェック (+ 1)
        /// <summary>
        /// <para>ユーザーコードチェック</para>
        /// <para>以下の順に入力チェックをおこなう</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.データ存在チェック（blExistCheck = trueの場合）</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="blExistCheck">データ存在チェック true:チェックする false:チェックしない</param>
        /// <param name="blExistNG">データ存在チェック true:指定コードの重複はエラー false:指定コードの不在はエラー</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckUserCD(string strText, string strMsg, bool blRequiredCheck, bool blExistCheck, bool blExistNG)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                // 入力チェック
                if (Common.IsValidStr(strText) == false)
                {
                    return Common.GetMessage("C001", strMsg);
                }
            }


            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
            }

            // 全角・半角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", strMsg);
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
            }

            // データ存在チェックをする場合
            if (blExistCheck)
            {
                if (Common.IsValidStr(strText) == true)
                {
                    // 存在チェック
                    MasterService _MasterService = new MasterService();
                    DataTable dtUser = _MasterService.SelectAllUser(strText);
                    if (blExistNG)
                    {
                        // 対象コードが存在する = NG
                        if (dtUser.Rows.Count > 0)
                        {
                            return Common.GetMessage("C016", new string[] { strText + "のユーザー", dtUser.Rows.Count.ToString() });
                        }
                    }
                    else
                    {
                        // 対象コードが存在しない = NG
                        if (dtUser.Rows.Count < 1)
                        {
                            return Common.GetMessage("C015", strText + "のユーザー");
                        }
                    }
                }

            }

            return "";
        }
        #endregion

        #region 状態チェック
        /// <summary>
        /// <para>状態チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <returns>>エラーメッセージ</returns>
        public virtual string CheckDelFlag(string strText)
        {
            if (strText != "使用中" && strText != "削除済")
            {
                return Common.GetMessage("C059");
            }

            return "";
        }
        #endregion

        #region 状態チェック(M_GROUP用)
        /// <summary>
        /// <para>状態チェック(M_GROUP用)</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <returns>>エラーメッセージ</returns>
        public virtual string CheckDelFlagGroup(string strText, string strKey)
        {
            if (strText != "使用中" && strText != "削除済")
            {
                return Common.GetMessage("C059");
            }

            if (strText == "削除済")
            {
                DbContext context = null;
                context = DbUtil.Open(false);

                var sql = new System.Text.StringBuilder();
                sql.AppendLine("SELECT");
                sql.AppendLine(" GROUP_CD ");
                sql.AppendLine("FROM");
                sql.AppendLine(" M_GROUP");
                sql.AppendLine("WHERE");
                sql.AppendLine(" P_GROUP_CD = '" + strKey + "'");
                sql.AppendLine(" AND");
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

                var records = DbUtil.Select(context, sql.ToString());

                context.Close();

                // 親として1件でも設定されていればエラーとする。
                if (records.Rows.Count > 0)
                {
                    return string.Format(WarningMessages.ParentAsConfigured, "上位部署名", "削除");
                }
            }

            return "";
        }
        #endregion
        //2019/02/18 TPE.Sugimoto Add Sta
        #region 状態チェック(M_MAKER用)
        /// <summary>
        /// <para>状態チェック(M_MAKER用)</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <returns>>エラーメッセージ</returns>
        public virtual string CheckDelFlagMaker(string strText, string strKey)
        {
            if (strText != "使用中" && strText != "削除済")
            {
                return Common.GetMessage("C059");
            }

            if (strText == "削除済")
            {
                DbContext context = null;
                context = DbUtil.Open(false);

                var sql = new System.Text.StringBuilder();
                sql.AppendLine("SELECT");
                sql.AppendLine(" MAKER_ID ");
                sql.AppendLine("FROM");
                sql.AppendLine(" M_MAKER");
                sql.AppendLine("WHERE");
                sql.AppendLine(" MAKER_ID = '" + strKey + "'");
                sql.AppendLine(" AND");
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

                var records = DbUtil.Select(context, sql.ToString());

                context.Close();
            }

            return "";
        }
        #endregion

        #region 状態チェック(M_UNITSIZE用)
        /// <summary>
        /// <para>状態チェック(M_UNITSIZE用)</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <returns>>エラーメッセージ</returns>
        public virtual string CheckDelFlagUnitsize(string strText, string strKey)
        {
            if (strText != "使用中" && strText != "削除済")
            {
                return Common.GetMessage("C059");
            }

            if (strText == "削除済")
            {
                DbContext context = null;
                context = DbUtil.Open(false);

                var sql = new System.Text.StringBuilder();
                sql.AppendLine("SELECT");
                sql.AppendLine(" UNITSIZE_ID ");
                sql.AppendLine("FROM");
                sql.AppendLine(" M_UNITSIZE");
                sql.AppendLine("WHERE");
                sql.AppendLine(" UNITSIZE_ID = '" + strKey + "'");
                sql.AppendLine(" AND");
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

                var records = DbUtil.Select(context, sql.ToString());

                context.Close();
            }

            return "";
        }
        #endregion

        #region 状態チェック(M_WORKFLOW用)
        /// <summary>
        /// <para>状態チェック(M_WORKFLOW用)</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <returns>>エラーメッセージ</returns>
        public virtual string CheckDelFlagWorkflow(string strText, string strKey)
        {
            if (strText != "使用中" && strText != "削除済")
            {
                return Common.GetMessage("C059");
            }

            if (strText == "削除済")
            {
                DbContext context = null;
                context = DbUtil.Open(false);

                var sql = new System.Text.StringBuilder();
                sql.AppendLine("SELECT");
                sql.AppendLine(" FLOW_ID ");
                sql.AppendLine("FROM");
                sql.AppendLine(" M_WORKFLOW");
                sql.AppendLine("WHERE");
                sql.AppendLine(" FLOW_ID = '" + strKey + "'");
                sql.AppendLine(" AND");
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

                var records = DbUtil.Select(context, sql.ToString());

                context.Close();
            }

            return "";
        }
        #endregion
        //2019/02/18 TPE.Sugimoto Add End
        #region 管理者権限チェック
        /// <summary>
        /// <para>状態チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <returns>>エラーメッセージ</returns>
        public virtual string CheckADMIN(string strText)
        {
            if (strText != "一般ユーザー" && strText != "管理者")
            {
                return Common.GetMessage("C060");
            }

            return "";
        }
        #endregion

        #region バッチ更新対象外チェック
        /// <summary>
        /// <para>状態チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <returns>>エラーメッセージ</returns>
        public virtual string CheckBatchFlag(string strText)
        {
            if (strText != "対象" && strText != "対象外")
            {
                return Common.GetMessage("C061");
            }

            return "";
        }
        #endregion

        #region 社員名チェック
        /// <summary>
        /// <para>社員名チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckUserName(string strText, string strMsg, bool blRequiredCheck, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                if (!Common.IsValidStr(strText))
                {
                    return Common.GetMessage("C001", strMsg);
                  
                }
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
            
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
              
            }

            return "";
        }
        #endregion

        #region 社員名チェック
        /// <summary>
        /// <para>社員名チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <returns>>エラーメッセージ</returns>
        public virtual string CheckUserName(string strText, string strMsg, bool blRequiredCheck, bool blExistCheck)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                if (!Common.IsValidStr(strText))
                {
                    return Common.GetMessage("C001", strMsg);
                }
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
            }

            // データ存在チェックをする場合
            if (blExistCheck)
            {
                if (Common.IsValidStr(strText) == true)
                {
                    // 存在チェック
                    MasterService _MasterService = new MasterService();
                    DataTable dtUser = _MasterService.SelectAllUserName(strText);

                    if (dtUser.Rows.Count == 0)
                    {
                        return Common.GetMessage("C015", strMsg);
                    }

                    if (dtUser.Rows.Count > 1)
                    {
                        return Common.GetMessage("C016", new string[] { strText + "のユーザー", dtUser.Rows.Count.ToString() });
                    }
                }

            }

            return "";
        }
        #endregion

        #region 社員名（ローマ字）チェック
        /// <summary>
        /// <para>社員名（ローマ字）チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.全角カナチェック</para>
        /// <para>3.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckUserNameKana(string strText, string strMsg, bool blRequiredCheck, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 60;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                if (!Common.IsValidStr(strText))
                {
                    return Common.GetMessage("C001", strMsg);
                  
                }
            }

            if (!string.IsNullOrWhiteSpace(strText))
            {
                if (!strText.IsAlphabetOrNumber())
                {
                    return string.Format(WarningMessages.NotAlphabetOrNumber, strMsg);
                   
                }
            }

            /*
            // 入力されていれば全角カナチェック
            if (Common.IsValidStr(strText))
            {
                if (!Common.isZenkakuKana(strText, true))
                {
                    Common.MsgBox(pPage, Common.GetMessage("C051", strMsg), strControlId);
                    return true;
                }
            }
            */

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
              
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
              
            }

            return "";
        }
        #endregion

        #region 社員名（ローマ字）チェック
        /// <summary>
        /// <para>社員名（ローマ字）チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.全角カナチェック</para>
        /// <para>3.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <returns>エラーメッセージ</returns>
        public virtual string CheckUserNameKana(string strText, string strMsg, bool blRequiredCheck)
        {
            // 最大桁数
            const int iMaxLength = 60;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                if (!Common.IsValidStr(strText))
                {
                    return Common.GetMessage("C001", strMsg);
                }
            }

            if (!string.IsNullOrWhiteSpace(strText))
            {
                if (!strText.IsAlphabetOrNumber())
                {
                    return string.Format(WarningMessages.NotAlphabetOrNumber, strMsg);
                }
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() }));
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
            }

            return "";
        }
        #endregion

        #region パスワードチェック
        /// <summary>
        /// <para>パスワードチェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.パスワード、パスワード確認が同一かどうか</para>
        /// <para>2.必須入力チェック</para>
        /// <para>3.半角チェック</para>
        /// <para>4.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値（パスワード）</param>
        /// <param name="strTextCheck">入力値（パスワード確認）</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckPassword(string strText, string strTextCheck, bool blRequiredCheck, string strMsg, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 50;

            if (Common.IsValidStr(strText) || Common.IsValidStr(strTextCheck))
            {
                if (strText == strTextCheck)
                {
                    // 必須入力チェック
                    if (!Common.IsValidStr(strText))
                    {
                      return Common.GetMessage("C001", strMsg);
                     
                    }

                    // 全角・半角チェック
                    if (Common.isZenkaku(strText))
                    {
                        return Common.GetMessage("C011", strMsg);
                    
                    }

                    // 桁数チェック
                    if (!Common.CheckLengthStrCount(strText, iMaxLength))
                    {
                       return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
                    
                    }
                }
                else
                {
                    return Common.GetMessage("C050");
                 
                }
            }
            else
            {
                // 必須入力チェックをする場合(新規登録)
                if (blRequiredCheck)
                {
                    // 入力チェック
                    if (Common.IsValidStr(strText) == false)
                    {
                        return Common.GetMessage("C001", strMsg);
                    }
                }
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
        
            }

            return "";
        }
        #endregion

        #region メーカーIDチェック
        /// <summary>
        /// <para>メーカーIDチェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.数値チェック</para>
        /// <para>3.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="errMessage">エラーメッセージ</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual bool CheckMakerId(string strText, string strMsg, bool blRequiredCheck, out string errMessage)
        {
            // 最大桁数
            const int iMaxLength = 8;

            errMessage = "";

            //必須入力チェック
            if (blRequiredCheck)
            {
                if (!Common.IsValidStr(strText))
                {
                    errMessage = Common.GetMessage("C001", strMsg);
                    return true;
                }
            }

            //数値チェック
            if (!Common.IsInteger(strText))
            {
                errMessage = Common.GetMessage("C010", strMsg);
                return true;
            }

            //桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                errMessage = Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
                return true;
            }

            return false;
        }
        #endregion

        #region パターン名チェック
        /// <summary>
        /// <para>パターン名チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual bool CheckPatternName(string strText, string strMsg, bool blRequiredCheck, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 33;

            // 必須入力チェック
            if (blRequiredCheck)
            {
                if (!Common.IsValidStr(strText))
                {
                    Common.MsgBox(pPage, Common.GetMessage("C001", strMsg), strControlId);
                    return true;
                }
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 部署コードチェック
        /// <summary>
        /// 部署コードチェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckGroupCD(string strText, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 15;

            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C001", "部署コード"), strControlId);
                return true;
            }

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                Common.MsgBox(pPage, Common.GetMessage("C011", "部署コード"), strControlId);
                return true;
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "部署コード", "半角" + iMaxLength.ToString() }), strControlId);
                return true;
            }

            // 既存チェック
            if (CheckIsExist(strText, "GROUP_CD", null, null, "M_GROUP"))
            {
                Common.MsgBox(pPage, Common.GetMessage("C004", "部署コード"), strControlId);
                return true;
            }
            return false;
        }
        #endregion

        #region 部署コードチェック
        /// <summary>
        /// 部署コードチェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual string CheckGroupCD(string strText, string strKey)
        {
            // 最大桁数
            const int iMaxLength = 50;

            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                return Common.GetMessage("C001", "部署コード");
            }

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "部署コード");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "部署コード", "半角" + iMaxLength.ToString() });
            }

            // 既存チェック
            if (!Common.IsValidStr(strKey))
            {
                if (CheckIsExistAll(strText, "GROUP_CD", null, null, "M_GROUP"))
                {
                    return Common.GetMessage("C004", "部署コード");
                }
            }
            else
            {
                bool bln = CheckIsExistAll(strKey, "GROUP_ID", null, null, "M_GROUP");

                if (bln == false)
                {
                    if (CheckIsExistAll(strText, "GROUP_CD", null, null, "M_GROUP"))
                    {
                        return Common.GetMessage("C004", "部署コード");
                    }
                }
                else
                {
                    if (CheckIsExistAll(strText, "GROUP_CD", strKey, "GROUP_ID", "M_GROUP"))
                    {
                        return Common.GetMessage("C004", "部署コード");
                    }
                }
            }

            return "";
        }
        #endregion

        #region 部署名チェック
        /// <summary>
        /// 部署名チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual bool CheckGroupName(string strText, string strKey, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 15;

            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                Common.MsgBox(pPage, Common.GetMessage("C001", "部署名"), strControlId);
                return true;
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "部署名", iMaxLength.ToString() }), strControlId);
                return true;
            }

            // 既存チェック
            if (CheckIsExist(strText, "GROUP_NAME", strKey, "GROUP_ID", "M_GROUP"))
            {
                Common.MsgBox(pPage, Common.GetMessage("C004", "部署名"), strControlId);
                return true;
            }
            return false;
        }
        #endregion

        #region 部署名チェック
        /// <summary>
        /// 部署名チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckGroupName(string strText, bool blnDuplication)
        {
            // 最大桁数
            const int iMaxLength = 255;

            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                return Common.GetMessage("C001", "部署名");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "部署名", iMaxLength.ToString() });
            }

            // 既存チェック
            if (blnDuplication == true)
            {
                if (!CheckIsExist(strText, "GROUP_NAME", null, null, "M_GROUP"))
                {
                    return Common.GetMessage("C015", "部署名");
                }
            }
            return "";
        }
        #endregion

        #region 上位部署名チェック
        /// <summary>
        /// 上位部署名チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckPGroupName(string strText)
        {
            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                return "";
            }

            // 既存チェック
            if (!CheckIsExist(strText, "GROUP_NAME", null, null, "V_GROUP"))
            {
                return Common.GetMessage("C015", "上位部署名");
            }


            return "";
        }
        #endregion
        #region 存在チェック
        /// <summary>
        /// 存在チェック
        /// 指定項目（objValue）と同じであれば省く。
        /// キー項目（objKeyValue）がキーカラム（strKeyColumn）を持って決定付ける。
        /// </summary>
        /// <param name="objValue">指定項目</param>
        /// <param name="strColumn">カラム名</param>
        /// <param name="objKeyValue">キー項目</param>
        /// <param name="strColumn">キーカラム名</param>
        /// <param name="strTable">テーブル名</param>
        /// <returns>true：存在します。 / false：存在しません。</returns>
        public bool CheckIsExist(object objValue, string strColumn, object objKeyValue, string strKeyColumn, string strTable)
        {
            bool ret = false;
            string strSQL;
            Hashtable _params = new Hashtable();

            strSQL = "select ";
            strSQL += "  COUNT(*) CNT";
            strSQL += " from " + strTable;
            strSQL += " where DEL_FLAG <> 1";
            strSQL += "  AND " + strColumn + " = :VALUE";

            if (objKeyValue != null && objKeyValue.ToString() != "")
            {
                strSQL += "  AND " + strKeyColumn + "<> :KEYVALUE";
                _params.Add("KEYVALUE", objKeyValue.ToString());
            }

            _params.Add("VALUE", objValue.ToString());

            DataTable dt = DbUtil.Select(strSQL, _params);
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["CNT"]) > 0)
                {
                    ret = true;
                }
            }
            return ret;
        }
        #endregion

        #region 存在チェック(削除済み対象)
        /// <summary>
        /// 存在チェック(削除済み対象)
        /// 指定項目（objValue）と同じであれば省く。
        /// キー項目（objKeyValue）がキーカラム（strKeyColumn）を持って決定付ける。
        /// </summary>
        /// <param name="objValue">指定項目</param>
        /// <param name="strColumn">カラム名</param>
        /// <param name="objKeyValue">キー項目</param>
        /// <param name="strColumn">キーカラム名</param>
        /// <param name="strTable">テーブル名</param>
        /// <returns>true：存在します。 / false：存在しません。</returns>
        public bool CheckIsExistAll(object objValue, string strColumn, object objKeyValue, string strKeyColumn, string strTable)
        {
            bool ret = false;
            string strSQL;
            Hashtable _params = new Hashtable();

            strSQL = "select ";
            strSQL += "  COUNT(*) CNT";
            strSQL += " from " + strTable;
            strSQL += " where ";
            strSQL += " " + strColumn + " = :VALUE";
            if (objKeyValue != null && objKeyValue.ToString() != "")
            {
                strSQL += "  AND " + strKeyColumn + "<> :KEYVALUE";
                _params.Add("KEYVALUE", objKeyValue.ToString());
            }

            _params.Add("VALUE", objValue.ToString());

            DataTable dt = DbUtil.Select(strSQL, _params);
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["CNT"]) > 0)
                {
                    ret = true;
                }
            }
            return ret;
        }
        #endregion

        //2019/02/18 TPE.Sugimoto Add Sta
        #region 存在チェック_メーカー名(削除済み対象)
        /// <summary>
        /// 存在チェック(削除済み対象)
        /// 指定項目（objValue）と同じであれば省く。
        /// キー項目（objKeyValue）がキーカラム（strKeyColumn）を持って決定付ける。
        /// </summary>
        /// <param name="objValue">指定項目</param>
        /// <param name="strColumn">カラム名</param>
        /// <param name="objKeyValue">キー項目</param>
        /// <param name="strColumn">キーカラム名</param>
        /// <param name="strTable">テーブル名</param>
        /// <returns>true：存在します。 / false：存在しません。</returns>
        public bool CheckIsExistAll_MAKERNAME(object objValue, string strColumn, object objKeyValue, string strKeyColumn, string strTable, string strKey)
        {
            bool ret = false;
            string strSQL;
            Hashtable _params = new Hashtable();

            strSQL = "select ";
            strSQL += "  COUNT(*) CNT";
            strSQL += " from " + strTable;
            strSQL += " where ";
            if (strKey.ToString() != "")
            {
                strSQL += " not( MAKER_ID = '" + strKey.ToString() + "')";
                strSQL += " and";
            }
            strSQL += " REPLACE(" + strColumn + ",' ','')" + " = :VALUE";
            strSQL += " COLLATE Japanese_CI_AS_KS ";
            if (objKeyValue != null && objKeyValue.ToString() != "")
            {
                strSQL += "  AND " + strKeyColumn + "<> :KEYVALUE";
                _params.Add("KEYVALUE", objKeyValue.ToString());
            }

            _params.Add("VALUE", objValue.ToString().Replace(" ", "").Replace("　", ""));

            DataTable dt = DbUtil.Select(strSQL, _params);
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["CNT"]) > 0)
                {
                    ret = true;
                }
            }
            return ret;
        }
        #endregion

        #region 存在チェック_単位名称(削除済み対象)
        //2019/02/18 TPE.Sugimoto Add Sta
        /// <summary>
        /// 存在チェック(削除済み対象)
        /// 指定項目（objValue）と同じであれば省く。
        /// キー項目（objKeyValue）がキーカラム（strKeyColumn）を持って決定付ける。
        /// </summary>
        /// <param name="objValue">指定項目</param>
        /// <param name="strColumn">カラム名</param>
        /// <param name="objKeyValue">キー項目</param>
        /// <param name="strColumn">キーカラム名</param>
        /// <param name="strTable">テーブル名</param>
        /// <returns>true：存在します。 / false：存在しません。</returns>
        public bool CheckIsExistAll_UNITNAME(object objValue, string strColumn, object objKeyValue, string strKeyColumn, string strTable, string strKey)
        {
            bool ret = false;
            string strSQL;
            Hashtable _params = new Hashtable();

            strSQL = "select ";
            strSQL += "  COUNT(*) CNT";
            strSQL += " from " + strTable;
            strSQL += " where ";
            if (strKey.ToString() != "")
            {
                strSQL += " not( UNITSIZE_ID = '" + strKey.ToString() + "')";
                strSQL += " and";
            }
            strSQL += " REPLACE(" + strColumn + ",' ','')" + " = :VALUE";
            if (objKeyValue != null && objKeyValue.ToString() != "")
            {
                strSQL += "  AND " + strKeyColumn + "<> :KEYVALUE";
                _params.Add("KEYVALUE", objKeyValue.ToString());
            }

            _params.Add("VALUE", objValue.ToString().Replace(" ", "").Replace("　", ""));

            DataTable dt = DbUtil.Select(strSQL, _params);
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["CNT"]) > 0)
                {
                    ret = true;
                }
            }
            return ret;
        }
        #endregion

        #region 存在チェック_フロー名(削除済み対象)
        //2019/02/18 TPE.Sugimoto Add Sta
        /// <summary>
        /// 存在チェック(削除済み対象)
        /// 指定項目（objValue）と同じであれば省く。
        /// キー項目（objKeyValue）がキーカラム（strKeyColumn）を持って決定付ける。
        /// </summary>
        /// <param name="objValue">指定項目</param>
        /// <param name="strColumn">カラム名</param>
        /// <param name="objKeyValue">キー項目</param>
        /// <param name="strColumn">キーカラム名</param>
        /// <param name="strTable">テーブル名</param>
        /// <returns>true：存在します。 / false：存在しません。</returns>
        public bool CheckIsExistAll_USAGE(object objValue, string strColumn, object objKeyValue, string strKeyColumn, string strTable, string strKey)
        {
            bool ret = false;
            string strSQL;
            Hashtable _params = new Hashtable();

            strSQL = "select ";
            strSQL += "  COUNT(*) CNT";
            strSQL += " from " + strTable;
            strSQL += " where ";
            if (strKey.ToString() != "")
            {
                strSQL += " not( FLOW_ID = '" + strKey.ToString() + "')";
                strSQL += " and";
            }
            strSQL += " REPLACE(" + strColumn + ",' ','')" + " = :VALUE";
            if (objKeyValue != null && objKeyValue.ToString() != "")
            {
                strSQL += "  AND " + strKeyColumn + "<> :KEYVALUE";
                _params.Add("KEYVALUE", objKeyValue.ToString());
            }

            _params.Add("VALUE", objValue.ToString().Replace(" ", "").Replace("　", ""));

            DataTable dt = DbUtil.Select(strSQL, _params);
            if (dt.Rows.Count > 0)
            {
                if (Convert.ToInt32(dt.Rows[0]["CNT"]) > 0)
                {
                    ret = true;
                }
            }
            return ret;
        }
        #endregion
        //2019/02/18 TPE.Sugimoto Add End

        #region 商品名必須入力チェック
        /// <summary>
        /// 商品名必須入力チェック
        /// </summary>
        /// <returns></returns>
        public virtual bool CheckValidProductName(string strText, string strText2, params string[] strControlId)
        {
            //入力チェック
            if (Common.IsValidStr(strText) == false && Common.IsValidStr(strText2) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C001", "商品名"), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 商品名（和名）チェック
        /// <summary>
        /// 商品名（和名）チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckProductNameJP(string strText, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 500;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "化学物質名", iMaxLength.ToString() }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 商品名（英名）チェック
        /// <summary>
        /// 商品名（英名）チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckProductNameEN(string strText, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 500;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                Common.MsgBox(pPage, Common.GetMessage("C011", "化学物質名"), strControlId);
                return true;
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "化学物質名", "半角" + iMaxLength.ToString() }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region メーカー名チェック
        /// <summary>
        /// メーカー名チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckMakerCombo(string strText, params string[] strControlId)
        {
            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C001", "メーカー名"), strControlId);
                return true;
            }

            return false;
        }
        #endregion
        //2019/02/18 TPE.Sugimoto Add Sta
        #region メーカー名チェック
        /// <summary>
        /// メーカー名チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckMakerName(string strText, bool blnDuplication, string strkey)
        {
            // 最大桁数
            const int iMaxLength = 255;

            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                return Common.GetMessage("C001", "メーカー名");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "メーカー名", iMaxLength.ToString() });
            }

            // 既存チェック
            if (CheckIsExistAll_MAKERNAME(strText, "MAKER_NAME", null, null, "M_MAKER", strkey))
            {
                return Common.GetMessage("C004", "メーカー名");
            }

            // メーカー名チェック定義ファイルが存在しないとき、エラーを出力する
            SetConfig();
            string strMakerCheckFilePath = clsConfig.MAKER_CHECK_TXT;
            if (!System.IO.File.Exists(strMakerCheckFilePath))
            {
                return Common.GetMessage("C064");
            }
            // メーカー名チェック定義ファイルの読み込み
            using (var sr = new System.IO.StreamReader(clsConfig.MAKER_CHECK_TXT, System.Text.Encoding.GetEncoding("shift_jis")))
            {
                List<string> list = new List<string>();
                string line = "";

                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine();
                    if (line.Length == 0)  // 空行が発生した時点で抜ける
                    {
                        break;
                    }
                    string[] spLine = line.Split(',');

                    if (string.IsNullOrEmpty(strText))
                    {
                        break;
                    }
                    IEnumerable<string> characters = spLine;

                    foreach (var c in characters)
                    {
                        if (strText.Contains(c))
                        {
                            return Common.GetMessage("C065", new string[] { "メーカー名", c.ToString() });
                        }
                    }
                }
            }

            return "";
        }

        // メーカー名チェック定義ファイルの値取得
        private static void SetConfig()
        {
            clsConfig.MAKER_CHECK_TXT = Convert.ToString(ConfigurationManager.AppSettings["MAKER_CHECK_TXT"]);
        }
        #endregion

        #region URLチェック
        /// <summary>
        /// URLチェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckMakerUrl(string strText)
        {
            // 最大桁数
            const int iMaxLength = 255;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "URL");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "URL", iMaxLength.ToString() });
            }

            return "";
        }

        #endregion

        #region 電話番号チェック
        /// <summary>
        /// 電話番号チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckMakerTel(string strText)
        {
            // 最大桁数
            const int iMaxLength = 255;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "電話番号");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "電話番号", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region メールアドレス1チェック
        /// <summary>
        /// メールアドレス1チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckMakerEmail1(string strText)
        {
            // 最大桁数
            const int iMaxLength = 255;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "メールアドレス１");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "メールアドレス１", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region メールアドレス2チェック
        /// <summary>
        /// メールアドレス2チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckMakerEmail2(string strText)
        {
            // 最大桁数
            const int iMaxLength = 255;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "メールアドレス２");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "メールアドレス２", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region メールアドレス3チェック
        /// <summary>
        /// メールアドレス3チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckMakerEmail3(string strText)
        {
            // 最大桁数
            const int iMaxLength = 255;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "メールアドレス３");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "メールアドレス３", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 住所チェック
        /// <summary>
        /// 住所チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckMakerAddress(string strText)
        {
            // 最大桁数
            const int iMaxLength = 255;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "住所", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 単位名称チェック
        /// <summary>
        /// 単位名称チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckUnitName(string strText, bool blnDuplication, string strkey)
        {
            // 最大桁数
            const int iMaxLength = 50;

            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                return Common.GetMessage("C001", "単位名称");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "単位名称", iMaxLength.ToString() });
            }

            // 既存チェック
            if (CheckIsExistAll_UNITNAME(strText, "UNIT_NAME", null, null, "M_UNITSIZE", strkey))
            {
                return Common.GetMessage("C004", "単位名称");
            }

            return "";
        }
        #endregion

        #region 変換係数チェック
        /// <summary>
        /// 変換係数チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckFactor(string strText)
        {
            decimal input = Convert.ToDecimal(strText);//数値化
            decimal integer_part = Math.Floor(input);//整数部取得
            decimal decimal_part = input % 1;//小数部取得
            const int integer_part_length = 4;//整数部の最大桁数設定
            const int decimal_part_length = 11;//小数部の最大桁数設定(0.も含むため、+2している)

            //数値チェック
            if (Common.IsNumber(strText) == false)
            {
                return Common.GetMessage("C010", "変換係数");
            }
            //値が入っていた場合
            if (Common.IsValidStr(strText))
            {
                // 整数部桁数チェック
                if (Common.CheckLengthStrCount(Convert.ToString(integer_part), integer_part_length) == false)
                {
                    return Common.GetMessage("C062", new string[] { "変換係数", integer_part_length.ToString() });
                }
            }
            // 小数部桁数チェック
            if (Common.CheckLengthStrCount(Convert.ToString(decimal_part), decimal_part_length) == false)
            {
                return Common.GetMessage("C063", new string[] { "変換係数", "9" });
            }

            return "";
        }
        #endregion

        #region フローコードチェック
        /// <summary>
        /// フローコードチェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckFlowCD(string strText)
        {
            // 最大桁数
            const int iMaxLength = 10;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "フローコード");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "フローコード", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region フロー種別チェック
        /// <summary>
        /// フロー種別チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckFlowTypeID(string strText)
        {
            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                return Common.GetMessage("C001", "フロー種別");
            }

            return "";
        }
        #endregion

        #region フロー名チェック
        /// <summary>
        /// フロー名チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckUsage(string strText, bool blnDuplication, string strkey)
        {
            // 最大桁数
            const int iMaxLength = 100;

            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                return Common.GetMessage("C001", "フロー名");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "フロー名", iMaxLength.ToString() });
            }

            // 既存チェック
            if (CheckIsExistAll_USAGE(strText, "USAGE", null, null, "M_WORKFLOW", strkey))
            {
                return Common.GetMessage("C004", "フロー名");
            }
            return "";
        }
        #endregion

        #region フロー階層チェック
        /// <summary>
        /// フロー階層チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckFlowHierarchy(string strText)
        {
            // 最大桁数
            const int iMaxLength = 1;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "フロー階層");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "フロー階層", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認階層1チェック
        /// <summary>
        /// 承認階層1チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalTarget1(string strText)
        {
            // 最大桁数
            const int iMaxLength = 50;

            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                return Common.GetMessage("C001", "承認階層1");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認階層1", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認者1チェック
        /// <summary>
        /// 承認者1チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalUserCD1(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                return Common.GetMessage("C001", "承認者1");
            }

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "承認者1");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認者1", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 代行者1チェック
        /// <summary>
        /// 代行者1チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckSubsituteUserCD1(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "代行者1");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "代行者1", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認階層2チェック
        /// <summary>
        /// 承認階層2チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalTarget2(string strText)
        {
            // 最大桁数
            const int iMaxLength = 50;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認階層2", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認者2チェック
        /// <summary>
        /// 承認者2チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalUserCD2(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "承認者2");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認者2", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 代行者2チェック
        /// <summary>
        /// 代行者2チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckSubsituteUserCD2(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "代行者2");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "代行者2", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認階層3チェック
        /// <summary>
        /// 承認階層3チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalTarget3(string strText)
        {
            // 最大桁数
            const int iMaxLength = 50;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認階層3", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認者3チェック
        /// <summary>
        /// 承認者3チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalUserCD3(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "承認者3");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認者3", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 代行者3チェック
        /// <summary>
        /// 代行者3チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckSubsituteUserCD3(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "代行者3", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認階層4チェック
        /// <summary>
        /// 承認階層4チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalTarget4(string strText)
        {
            // 最大桁数
            const int iMaxLength = 50;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認階層4", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認者4チェック
        /// <summary>
        /// 承認者4チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalUserCD4(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "承認者4");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認者4", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 代行者4チェック
        /// <summary>
        /// 代行者4チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckSubsituteUserCD4(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "代行者4");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "代行者4", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認階層5チェック
        /// <summary>
        /// 承認階層1チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalTarget5(string strText)
        {
            // 最大桁数
            const int iMaxLength = 50;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認階層5", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認者5チェック
        /// <summary>
        /// 承認者5チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalUserCD5(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "承認者5");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認者5", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 代行者5チェック
        /// <summary>
        /// 代行者5チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckSubsituteUserCD5(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "代行者5");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "代行者5", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認階層6チェック
        /// <summary>
        /// 承認階層6チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalTarget6(string strText)
        {
            // 最大桁数
            const int iMaxLength = 50;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認階層6", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 承認者6チェック
        /// <summary>
        /// 承認者6チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckApprovalUserCD6(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "承認者6");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "承認者6", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion

        #region 代行者6チェック
        /// <summary>
        /// 代行者6チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckSubsituteUserCD6(string strText)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", "代行者6");
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                return Common.GetMessage("C003", new string[] { "代行者6", iMaxLength.ToString() });
            }

            return "";
        }
        #endregion
        //2019/02/18 TPE.Sugimoto Add End
        #region カタログコードチェック
        /// <summary>
        /// カタログコードチェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckCatCd(string strText, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 12;

            // 全角チェック
            if (Common.isZenkaku(strText))
            {
                Common.MsgBox(pPage, Common.GetMessage("C011", "化学物質コード"), strControlId);
                return true;
            }

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "化学物質コード", "半角" + iMaxLength.ToString() }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 規格チェック
        /// <summary>
        /// 規格チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckGrade(string strText, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 300;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "規格", iMaxLength.ToString() }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 純度チェック
        /// <summary>
        /// 純度チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckPurity(string strText, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 30;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "純度", iMaxLength.ToString() }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region カタログ価格チェック
        /// <summary>
        /// カタログ価格チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckPrice(string strText, params string[] strControlId)
        {
            //数値チェック
            if (Common.IsNumber(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C010", "カタログ価格"), strControlId);
                return true;
            }

            //範囲チェック
            if (Convert.ToDouble(strText) >= 10000000000)
            {
                Common.MsgBox(pPage, Common.GetMessage("C040", new string[] { "カタログ価格", "10000000000" }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 代理店名チェック
        /// <summary>
        /// 代理店名チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckVendorCombo(string strText, params string[] strControlId)
        {
            //入力チェック
            if (Common.IsValidStr(strText) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C017", "代理店名"), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 保存条件チェック
        /// <summary>
        /// 保存条件チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckPreserveCondition(string strText, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 150;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "保存条件", iMaxLength.ToString() }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 輸送条件チェック
        /// <summary>
        /// 輸送条件チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <returns></returns>
        public virtual bool CheckTransCondition(string strText, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 150;

            // 文字数チェック
            if (Common.CheckLengthStrCount(strText, iMaxLength) == false)
            {
                Common.MsgBox(pPage, Common.GetMessage("C003", new string[] { "輸送条件", iMaxLength.ToString() }), strControlId);
                return true;
            }

            return false;
        }
        #endregion

        #region 内線番号（TEL）チェック
        /// <summary>
        /// <para>内線番号（TEL）チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック</para>
        /// <para>2.半角チェック</para>
        /// <para>3.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckTel(string strText, string strMsg, bool blRequiredCheck, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 50;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                // 入力チェック
                if (Common.IsValidStr(strText) == false)
                {
                    return Common.GetMessage("C001", strMsg);
                
                }
            }

            // 全角・半角チェック
            if (Common.isZenkaku(strText))
            {
               return Common.GetMessage("C011", strMsg);
             
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
              
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
               
            }

            return "";
        }
        #endregion

        #region 内線番号（TEL）チェック
        /// <summary>
        /// <para>内線番号（TEL）チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック</para>
        /// <para>2.半角チェック</para>
        /// <para>3.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strMsg">出力項目名</param>
        /// <returns>エラーメッセージ</returns>
        public virtual string CheckTel(string strText, string strMsg, bool blRequiredCheck)
        {
            // 最大桁数
            const int iMaxLength = 50;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                // 入力チェック
                if (Common.IsValidStr(strText) == false)
                {
                    return Common.GetMessage("C001", strMsg);
                }
            }

            // 全角・半角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", strMsg);
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
            }

            return "";
        }
        #endregion

        #region メールアドレスチェック
        /// <summary>
        /// <para>メールアドレスチェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック</para>
        /// <para>2.メールアドレス形式チェック</para>
        /// <para>3.半角チェック</para>
        /// <para>4.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckEMail(string strText, string strMsg, bool blRequiredCheck, params string[] strControlId)
        {
            // 最大桁数
            const int iMaxLength = 255;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                // 入力チェック
                if (!Common.IsValidStr(strText))
                {
                    return Common.GetMessage("C001", strMsg);
              
                }
            }

            // 全角・半角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", strMsg);
               
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
              
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
            }

            // メールアドレス形式
            if (!string.IsNullOrEmpty(strText) && !strText.IsMailAddressFormat())
            {
                return string.Format(WarningMessages.NotMailAddressFormat, strMsg);
            }

            return "";
        }
        #endregion

        #region メールアドレスチェック
        /// <summary>
        /// <para>メールアドレスチェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック</para>
        /// <para>2.メールアドレス形式チェック</para>
        /// <para>3.半角チェック</para>
        /// <para>4.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strMsg">出力項目名</param>
        /// <returns>エラーメッセージ</returns>
        public virtual string CheckEMail(string strText, string strMsg, bool blRequiredCheck)
        {
            // 最大桁数
            const int iMaxLength = 255;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                // 入力チェック
                if (!Common.IsValidStr(strText))
                {
                    return Common.GetMessage("C001", strMsg);
                }
            }

            // 全角・半角チェック
            if (Common.isZenkaku(strText))
            {
                return Common.GetMessage("C011", strMsg);
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                return Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                return string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
            }

            // メールアドレス形式
            if (!string.IsNullOrEmpty(strText) && !strText.IsMailAddressFormat())
            {
                return string.Format(WarningMessages.NotMailAddressFormat, strMsg);
            }

            return "";
        }
        #endregion

        #region 使用場所チェック
        /// <summary>
        /// <para>使用場所チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック</para>
        /// <para>2.半角チェック</para>
        /// <para>3.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckLocation(string strText, string strMsg, params string[] strControlId)
        {
            string CheckLocation = "";
            // 最大桁数
            const int iMaxLength = 255;

            // 必須入力チェックをする場合
            if (Common.IsValidStr(strText) == false)
            {
                CheckLocation = Common.GetMessage("C001", strMsg);
                return CheckLocation;
            }

            // 全角・半角チェック
            if (Common.isZenkaku(strText))
            {
                CheckLocation = Common.GetMessage("C011", strMsg);
                return CheckLocation;
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                CheckLocation = Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
                return CheckLocation;
            }

            return CheckLocation;
        }
        #endregion

        #region フロー名チェック
        /// <summary>
        /// フロー名チェック
        /// </summary>
        /// <param name="strText"></param>
        /// <param name="strKey"></param>
        /// <returns></returns>
        public virtual string CheckFlowName(string strText)
        {
            //入力チェック
            if (!Common.IsValidStr(strText))
            {
                return Common.GetMessage("C001", "フロー名");
            }

            // 既存チェック
            if (!CheckIsExist(strText, "USAGE", null, null, "M_WORKFLOW"))
            {
                return Common.GetMessage("C015", "フロー名");
            }
            return "";
        }
        #endregion

        #region 届出名称チェック
        /// <summary>
        /// <para>届出名称チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strKey">更新対象のKey</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckReportName(string strText, string strYMD, string strMsg, bool blRequiredCheck, string strKey, params string[] strControlId)
        {
            string errorMessages = null;
            // 最大桁数
            const int iMaxLength = 255;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                if (!Common.IsValidStr(strText))
                {
                    errorMessages = Common.GetMessage("C001", strMsg);
                    return errorMessages;
                }
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                errorMessages = Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
                return errorMessages;
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                errorMessages = string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
                return errorMessages;
            }

            // データ存在チェック
            MasterService _MasterService = new MasterService();
            DataTable dtReport = _MasterService.SelectFireReport(strKey, strText, strYMD);

            // 対象コードが存在する = NG
            if (dtReport.Rows.Count > 0)
            {
                errorMessages = Common.GetMessage("C016", new string[] { strText + "の届出名称", dtReport.Rows.Count.ToString() });
                return errorMessages;
            }

            return errorMessages;
        }
        #endregion

        #region 届出年月日チェック
        /// <summary>
        /// <para>届出年月日チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckReportYMD(string strText, string strMsg, bool blRequiredCheck, params string[] strControlId)
        {
            string errorMessages = null;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                if (!Common.IsValidStr(strText))
                {
                    errorMessages = Common.GetMessage("C001", strMsg);
                    return errorMessages;
                }
            }


            // 日付チェック
            if (!Common.IsDate(strText))
            {
                errorMessages = Common.GetMessage("C056", new string[] { "届出年月日", "日付" });
                return errorMessages;
            }

            return errorMessages;
        }
        #endregion


        #region 届出先チェック
        /// <summary>
        /// <para>届出先チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="strText">入力値</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="blRequiredCheck">必須入力チェック true:チェックする false:チェックしない</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckReportSubmit(string strText, string strMsg, bool blRequiredCheck, params string[] strControlId)
        {
            string errorMessages = null;
            // 最大桁数
            const int iMaxLength = 255;

            // 必須入力チェックをする場合
            if (blRequiredCheck)
            {
                if (!Common.IsValidStr(strText))
                {
                    errorMessages = Common.GetMessage("C001", strMsg);
                    return errorMessages;
                }
            }

            // 桁数チェック
            if (!Common.CheckLengthStrCount(strText, iMaxLength))
            {
                errorMessages = Common.GetMessage("C003", new string[] { strMsg, iMaxLength.ToString() });
                return errorMessages;
            }

            // 禁則文字チェック
            var p = strText.ExtractProhibited();
            if (p.Count() > 0)
            {
                errorMessages = string.Format(WarningMessages.ContainProhibitionCharacter, strMsg, string.Join(" ", p).Replace(@"\", @"\\"));
                return errorMessages;
            }

            return errorMessages;
        }
        #endregion

        #region 届出保管場所/使用場所チェック
        /// <summary>
        /// <para>届出保管場所/使用場所チェック</para>
        /// <para>以下の順にチェックを行う</para>
        /// <para>1.必須入力チェック（blRequiredCheck = trueの場合）</para>
        /// <para>2.桁数チェック</para>
        /// </summary>
        /// <param name="listCnt1">保管場所数</param>
        /// <param name="listCnt2">使用場所数</param>
        /// <param name="strMsg">出力項目名</param>
        /// <param name="strControlId">Common.MsgBox参照</param>
        /// <returns>>true:エラーの場合　false:エラーでない場合</returns>
        public virtual string CheckReportLocation(int listCnt1, int listCnt2, string strMsg, params string[] strControlId)
        {
            string errorMessages = null;
            if (listCnt1 + listCnt2 == 0)
            {
                errorMessages = Common.GetMessage("C001", strMsg);
                return errorMessages;
            }

            return errorMessages;
        }
        #endregion


    }
}