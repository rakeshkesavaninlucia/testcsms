﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using System.Collections;
using ZyCoaG.Classes.util;
//  msc 4 functions commented
namespace ZyCoaG.util
{
    public class UserInfo
    {
        /// <summary>
        /// ユーザ情報保持クラス
        /// </summary>
        /// <paramMap></paramMap>        
        /// <returns></returns>
        public class ClsUser
        {
            // クライアント端末情報
            protected string strIp;

            // M_USER テーブル
            protected string strUserCD;
            protected string strUserName;
            protected string strUserNameKana;
            protected string strApprover1;
            protected string strApprover2;
            protected string strApprover1Name;
            protected string strApprover2Name;
            protected string strTEL;
            protected string strEMail;
            protected string strRoomID;
            protected string strAdminFlag;
            protected string strDeleteAdmin;
            protected string strMasterAdmin;
            protected string strMEMO;
            protected string strBilingualMode;
            protected string strGroupID;
            protected string strGroupName;
            protected string strAreaID;
            protected string strFlowCD;
            protected string strGroupCD;
            protected bool blnDelFlag;
            protected bool blnBatchFlag;
            protected string strPreviousGroupCD;
            protected string strReferenceAuthority;


            // M_USER_REG_ROLE テーブル
            protected string[] aryOrderRegTypeID = new string[] { };
            protected string[] aryUseRegTypeID = new string[] { };

            /// <summary>クライアント端末のIPアドレス</summary>
            public string IP_ADDRESS { set { this.strIp = value; } get { return this.strIp; } }
            /// <summary>ユーザーCD</summary>
            public string USER_CD { set { this.strUserCD = value; } get { return this.strUserCD; } }
            /// <summary>ユーザー名</summary>
            public string USER_NAME { set { this.strUserName = value; } get { return this.strUserName; } }
            /// <summary>
            /// ユーザー名+権限名
            /// </summary>
            //public virtual string USER_NAME_GRANT
            //{
            //    get
            //    {
            //        var grantName = ADMINFLG_NAME;
            //        if (string.IsNullOrWhiteSpace(grantName)) return USER_NAME;
            //        return USER_NAME + "(" + grantName + ")";
            //    }
            //}
            /// <summary>ユーザー名（ローマ字）</summary>
            public string USER_NAME_KANA { set { this.strUserNameKana = value; } get { return this.strUserNameKana; } }
            /// <summary>第一承認者</summary>
            public string APPROVER1 { set { this.strApprover1 = value; } get { return this.strApprover1; } }
            /// <summary>第二承認者</summary>
            public string APPROVER2 { set { this.strApprover2 = value; } get { return this.strApprover2; } }
            /// <summary>第一承認者</summary>
            public string APPROVER1_NAME { set { this.strApprover1Name = value; } get { return this.strApprover1Name; } }
            /// <summary>第二承認者</summary>
            public string APPROVER2_NAME { set { this.strApprover2Name = value; } get { return this.strApprover2Name; } }
            /// <summary>内線番号</summary>
            public string TEL { set { this.strTEL = value; } get { return this.strTEL; } }
            /// <summary>メールアドレス</summary>
            public string EMAIL { set { this.strEMail = value; } get { return this.strEMail; } }
            /// <summary>納品場所ID</summary>
            public string ROOM_ID { set { this.strRoomID = value; } get { return this.strRoomID; } }
            /// <summary>管理者権限（0:一般 1:管理者）</summary>
            public string ADMINFLG { set { this.strAdminFlag = value; } get { return this.strAdminFlag; } }
            /// <summary>
            /// 権限名
            /// </summary>
            //public virtual string ADMINFLG_NAME {
            //    get
            //    {
            //        var dic = new CommonMasterInfo(SystemCommonMasterName.GRANT_NAME).GetDictionary();
            //        if (string.IsNullOrWhiteSpace(ADMINFLG)) return string.Empty;
            //        return dic[Convert.ToInt32(ADMINFLG)];
            //    }
            //}
            /// <summary>メモ？</summary>
            public string MEMO { set { this.strMEMO = value; } get { return this.strMEMO; } }
            /// <summary>言語設定（0:日本語 1:英語）</summary>
            public string BILINGUAL_MODE { set { this.strBilingualMode = value; } get { return this.strBilingualMode; } }
            /// <summary>部署ID</summary>
            public string GROUP_ID { set { this.strGroupID = value; } get { return this.strGroupID; } }
            /// <summary>部署コード</summary>
            public string GROUP_CD { set { this.strGroupCD = value; } get { return this.strGroupCD; } }
            /// <summary>部署名</summary>
            public string GROUP_NAME { set { this.strGroupName = value; } get { return this.strGroupName; } }
            /// <summary>地域ID</summary>
            public string AREA_ID { set { this.strAreaID = value; } get { return this.strAreaID; } }
            /// <summary>所属部署１</summary>
            public string PREVIOUS_GROUP_CD { set { this.strTEL = value; } get { return this.strPreviousGroupCD; } }
            /// <summary>フローコード</summary>
            public string FLOW_CD { set { this.strFlowCD = value; } get { return this.strFlowCD; } }
            /// <summary>削除フラグ</summary>
            public bool DEL_FLAG { set { this.blnDelFlag = value; } get { return this.blnDelFlag; } }
            /// <summary>バッチ更新対象フラグ</summary>
            public bool BATCH_FLAG { set { this.blnBatchFlag = value; } get { return this.blnBatchFlag; } }
            /// <summary>発注権限</summary>
            public string[] ORDER_REG_TYPE_ID { set { this.aryOrderRegTypeID = value; } get { return this.aryOrderRegTypeID; } }
            /// <summary>使用権限</summary>
            public string[] USE_REG_TYPE_ID { set { this.aryUseRegTypeID = value; } get { return this.aryUseRegTypeID; } }
            /// <summary>参照権限（0:一般ユーザー 1:管理者）</summary>
            public string REFERENCE_AUTHORITY { set { this.strReferenceAuthority = value; } get { return this.strReferenceAuthority; } }

            /// <summary>
            /// 法規制に対する発注・使用などの権限を設定します。
            /// </summary>
            public void SetAuthority()
            {
                // ----- 発注権限 -----
                this.aryOrderRegTypeID = SelectRegTypeID(Constant.CgrantTypes.Order);

                // ----- 使用権限 -----
                this.aryUseRegTypeID = SelectRegTypeID(Constant.CgrantTypes.Use);
            }

            private string[] SelectRegTypeID(Constant.CgrantTypes type)
            {
                StringBuilder sbSql = new StringBuilder();
                Hashtable _param = new Hashtable();

                sbSql.Append("select ");
                sbSql.Append("  distinct pattern.REG_TYPE_ID ");
                sbSql.Append("from ");
                sbSql.Append(" M_USER_REG_ROLE role, ");
                sbSql.Append(" M_REG_PATTERN pattern ");
                sbSql.Append("where ");
                sbSql.Append("  role.DEL_FLAG <> 1 AND ");
                sbSql.Append("  pattern.DEL_FLAG <> 1 AND ");
                sbSql.Append("  pattern.PATTERN_CD = role.PATTERN_CD AND ");
                sbSql.Append("  role.USER_CD = :USER_CD AND ");
                sbSql.Append("  (role.GRANT_ID = :ADMIN OR role.GRANT_ID = :TYPE) ");

                _param.Add("USER_CD", this.strUserCD);
                _param.Add("ADMIN", Constant.CgrantTypes.Admin.ToString("D"));
                _param.Add("TYPE", type.ToString("D"));

                DataTable dt = DbUtil.Select(sbSql.ToString(), _param);

                string[] aryRegTypeID = new string[dt.Rows.Count];

                int i = 0;
                foreach (DataRow row in dt.Rows)
                {
                    aryRegTypeID[i++] = row["REG_TYPE_ID"].ToString();
                }

                return aryRegTypeID;
            }
        }
    }
}