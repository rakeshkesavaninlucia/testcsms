﻿using Dapper;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZyCoaG;
using ZyCoaG.Classes.util;

namespace ChemMgmt.Classes.util
{
    public class MasterService
    {
        /// <summary>
        /// 消防法届出マスタを検索する（削除済み含む）
        /// </summary>
        /// <param name="reportId">届出ID</param>
        /// <param name="reportName">届出名称</param>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable SelectFireReport(string reportId, string reportName, string reportYMD)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  REPORT_ID ");
            stbSql.Append("from ");
            stbSql.Append("  M_FIRE_REPORT ");
            stbSql.Append("where ");
            if (reportId == string.Empty)
            {
                stbSql.Append("  REPORT_NAME = :REPORT_NAME ");
                stbSql.Append("  AND REPORT_YMD = :REPORT_YMD ");
            }
            else
            {
                stbSql.Append("  REPORT_ID != :REPORT_ID ");
                stbSql.Append("  AND REPORT_NAME = :REPORT_NAME ");
                stbSql.Append("  AND REPORT_YMD = :REPORT_YMD ");
            }
            stbSql.Append("order by ");
            stbSql.Append("  REPORT_ID ");

            Hashtable param = new Hashtable();
            if (reportId == string.Empty)
            {
                param.Add("REPORT_NAME", reportName);
                param.Add("REPORT_YMD", reportYMD);
            }
            else
            {
                param.Add("REPORT_ID", reportId);
                param.Add("REPORT_NAME", reportName);
                param.Add("REPORT_YMD", reportYMD);
            }
            return DbUtil.Select(stbSql.ToString(), param);
        }
        /// <summary>
        /// メーカーマスタを検索する
        /// </summary>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable SelectMaker()
        //public List<M_MAKER> selectMaker()
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  MAKER_ID ");
            stbSql.Append(" ,MAKER_NAME ");
            stbSql.Append("from ");
            stbSql.Append("  M_MAKER ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("order by ");
            stbSql.Append(" MAKER_NAME ");
            //return DbUtil.select<M_MAKER>(stbSql.ToString(), null);
             return DbUtil.Select(stbSql.ToString());
        }

        /// <summary>
        /// 代理店マスタを検索する
        /// </summary>
        /// <returns>取得結果格納DataTable</returns>
         public DataTable SelectVendor()
       // public List<M_VENDOR> selectVendor()
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  VENDOR_ID ");
            stbSql.Append(" ,VENDOR_NAME ");
            stbSql.Append("from ");
            stbSql.Append("  M_VENDOR ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("order by ");
            stbSql.Append(" VENDOR_NAME ");
          //  return DbUtil.select<M_VENDOR>(stbSql.ToString(),null);
             return DbUtil.Select(stbSql.ToString());
        }

        /// <summary>
        /// 勘定科目マスタを検索する
        /// </summary>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable SelectKanjyo()
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  KANJYO_ID ");
            stbSql.Append(" ,KANJYO_CD ");
            stbSql.Append(" ,KANJYO_NAME ");
            stbSql.Append("from ");
            stbSql.Append("  M_KANJYO ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("order by ");
            stbSql.Append(" KANJYO_ID ");

            return DbUtil.Select(stbSql.ToString());
        }

        public List<M_MAKER> selectMakerList()
        //public List<M_MAKER> selectMaker()
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  MAKER_ID ");
            stbSql.Append(" ,MAKER_NAME ");
            stbSql.Append("from ");
            stbSql.Append("  M_MAKER ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("order by ");
            stbSql.Append(" MAKER_NAME ");

            DynamicParameters _params = new DynamicParameters();


            return DbUtil.Select<M_MAKER>(stbSql.ToString(), _params);

            //return DbUtil.select<M_MAKER>(stbSql.ToString(), null);
            //return DbUtil.select(stbSql.ToString());
        }


        /// <summary>
        /// 指図コードマスタを検索する
        /// </summary>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable SelectProject()
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  PROJECT_CD ");
            stbSql.Append(" ,PROJECT_NAME ");
            stbSql.Append("from ");
            stbSql.Append("  M_PROJECT ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("order by ");
            stbSql.Append(" PROJECT_NAME ");

            return DbUtil.Select(stbSql.ToString());
        }

        /// <summary>
        /// 部署マスタを検索する
        /// </summary>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable SelectGroup()
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  GROUP_ID ");
            stbSql.Append(" ,GROUP_CD ");
            stbSql.Append(" ,GROUP_NAME ");
            stbSql.Append(" ,P_GROUP_CD ");
            stbSql.Append("from ");
            stbSql.Append("  V_GROUP ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("order by ");
            stbSql.Append(" GROUP_CD ");

            return DbUtil.Select(stbSql.ToString());
        }

        /// <summary>
        /// 容量単位管理マスタを検索する
        /// </summary>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable SelectUnitSize()
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  UNITSIZE_ID ");
            stbSql.Append(" ,UNIT_NAME ");
            stbSql.Append(" ,FACTOR ");
            stbSql.Append("from ");
            stbSql.Append("  M_UNITSIZE ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("order by ");
            stbSql.Append(" UNIT_NAME ");

            return DbUtil.Select(stbSql.ToString());
        }

        /// <summary>
        /// 代理店カタログマスタを検索する（コンボボックス用）
        /// </summary>
        /// <param name="strQpSeqNo">商品詳細管理番号</param>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable SelectVendorProductByComboBox(string strQpSeqNo)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  v.VENDOR_ID VENDOR_ID ");
            stbSql.Append(" ,(v.VENDOR_NAME || '：' || q.CURR || q.PRICE) VENDOR_PRICE ");
            stbSql.Append("from ");
            stbSql.Append("  M_VENDOR v ");
            stbSql.Append("      inner join M_PRODUCT p on v.VENDOR_ID = p.VENDOR_ID ");
            stbSql.Append("                and p.DEL_FLAG <> 1 ");
            stbSql.Append("      inner join M_QUANTITY q on p.PRD_SEQNO = q.PRD_SEQNO ");
            stbSql.Append("                and q.DEL_FLAG <> 1 ");
            stbSql.Append("      inner join T_ORDER o on q.COMPOUND_LIST_ID = o.COMPOUND_LIST_ID ");
            stbSql.Append("                and o.DEL_FLAG <> 1 ");
            stbSql.Append("where ");
            stbSql.Append("  v.DEL_FLAG <> 1 ");
            stbSql.Append("and ");
            stbSql.Append("  o.QP_SEQNO = :QP_SEQNO ");
            stbSql.Append("order by ");
            stbSql.Append(" v.VENDOR_NAME ");

            Hashtable param = new Hashtable();
            param.Add("QP_SEQNO", strQpSeqNo);

            return DbUtil.Select(stbSql.ToString(), param);
        }

        /// <summary>
        /// ステータスマスタを検索する
        /// </summary>
        /// <param name="intClassId">1:発注ステータス 2:在庫ステータス</param>
        /// <returns>取得結果格納DataTable</returns>
        public List<M_STATUS> selectStatus(int intClassId)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  STATUS_ID ");
            stbSql.Append(" ,STATUS_TEXT ");
            stbSql.Append("from ");
            stbSql.Append("  M_STATUS ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("and ");
            stbSql.Append("  CLASS_ID = @CLASS_ID ");
            stbSql.Append("order by ");
            stbSql.Append(" STATUS_ID ");

            DynamicParameters _params = new DynamicParameters();
            _params.Add("@CLASS_ID", intClassId);

            return DbUtil.Select<M_STATUS>(stbSql.ToString(), _params);
        }

        // InventorySearch - Status Drop Down
        public List<M_STATUS> selectStatusList(int intClassId)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  STATUS_ID ");
            stbSql.Append(" ,STATUS_TEXT ");
            stbSql.Append("from ");
            stbSql.Append("  M_STATUS ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("and ");
            stbSql.Append("  CLASS_ID = @CLASS_ID ");
            stbSql.Append("order by ");
            stbSql.Append(" STATUS_ID ");

            DynamicParameters _params = new DynamicParameters();
            _params.Add("@CLASS_ID", intClassId);

            return DbUtil.Select<M_STATUS>(stbSql.ToString(), _params);
        }

        /// <summary>
        /// ユーザーメーカーマスタを検索する
        /// </summary>
        /// <param name="userCd">ユーザーコード</param>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable SelectUserMaker(string userCd)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  DISTINCT PATTERN_NAME ");
            stbSql.Append("from ");
            stbSql.Append("  M_USER_MAKER ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("and ");
            stbSql.Append("  USER_CD = :USER_CD ");
            //stbSql.Append("order by ");
            //stbSql.Append(" USER_MAKER_ID ");

            Hashtable _params = new Hashtable();
            _params.Add("USER_CD", userCd);

            return DbUtil.Select(stbSql.ToString(), _params);
        }

        // InventorySearch - Maker Drop Down
        public List<M_USER_MAKER> selectUserMakerList(string userCd)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  DISTINCT PATTERN_NAME ");
            stbSql.Append("from ");
            stbSql.Append("  M_USER_MAKER ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("and ");
            stbSql.Append("  USER_CD = @USER_CD ");
            //stbSql.Append("order by ");
            //stbSql.Append(" USER_MAKER_ID ");

            DynamicParameters _params = new DynamicParameters();
            _params.Add("@USER_CD", userCd);

            return DbUtil.Select<M_USER_MAKER>(stbSql.ToString(), _params);
        }


        public string SelectUserName(string userCd)
        {
            // SELECT SQL生成
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append(" select U0.USER_NAME from M_USER U0 where U0.DEL_FLAG = 0 and U0.USER_CD = @USER_CD order by U0.USER_CD");
            DynamicParameters param = new DynamicParameters();
            param.Add("@USER_CD", userCd);

            List<string> name = DbUtil.Select<string>(stbSql.ToString(), param);
            if (name.Count != 0)
            {
                return name[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// ユーザーマスタを検索する
        /// </summary>
        /// <param name="userCd">ユーザーコード</param>
        /// <returns>取得結果格納DataTable（USER_NAME昇順）</returns>
        public DataTable selectUser(string userCd)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  USER_CD ");
            stbSql.Append(" ,USER_NAME ");
            stbSql.Append(" ,USER_NAME_KANA ");
            stbSql.Append(" ,APPROVER1 ");
            stbSql.Append(" ,APPROVER2 ");
            stbSql.Append(" ,TEL ");
            stbSql.Append(" ,EMAIL ");
            stbSql.Append(" ,ROOM_ID ");
            stbSql.Append(" ,GROUP_CD ");
            stbSql.Append(" ,AREA_ID ");
            stbSql.Append(" ,ADMIN_FLAG ");
            stbSql.Append(" ,FLOW_CD ");
            stbSql.Append(" ,DEL_FLAG ");
            stbSql.Append(" ,BATCH_FLAG ");
            stbSql.Append("from ");
            stbSql.Append("  M_USER ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("and ");
            stbSql.Append("  USER_CD = :USER_CD ");
            stbSql.Append("order by ");
            stbSql.Append("  USER_NAME ");

            Hashtable _params = new Hashtable();
            _params.Add("USER_CD", userCd);

            return DbUtil.Select(stbSql.ToString(), _params);
        }

        /// <summary>
        /// ユーザーマスタを検索する（削除済み含む）
        /// </summary>
        /// <param name="userCd">ユーザーコード</param>
        /// <returns>取得結果格納DataTable（USER_NAME昇順）</returns>
        public DataTable SelectAllUser(string userCd)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  USER_CD ");
            stbSql.Append(" ,USER_NAME ");
            stbSql.Append(" ,USER_NAME_KANA ");
            stbSql.Append(" ,APPROVER1 ");
            stbSql.Append(" ,APPROVER2 ");
            stbSql.Append(" ,TEL ");
            stbSql.Append(" ,EMAIL ");
            stbSql.Append(" ,ROOM_ID ");
            stbSql.Append(" ,GROUP_CD ");
            stbSql.Append(" ,AREA_ID ");
            stbSql.Append(" ,ADMIN_FLAG ");
            stbSql.Append(" ,FLOW_CD ");
            stbSql.Append(" ,DEL_FLAG ");
            stbSql.Append(" ,BATCH_FLAG ");
            stbSql.Append("from ");
            stbSql.Append("  M_USER ");
            stbSql.Append("where ");
            stbSql.Append("  USER_CD = :USER_CD ");
            stbSql.Append("order by ");
            stbSql.Append("  USER_NAME ");

            Hashtable _params = new Hashtable();
            _params.Add("USER_CD", userCd);

            return DbUtil.Select(stbSql.ToString(), _params);
        }

        /// <summary>
        /// ユーザーマスタを検索する（削除済み含む）
        /// </summary>
        /// <param name="userCd">ユーザー名</param>
        /// <returns>取得結果格納DataTable（USER_NAME昇順）</returns>
        public DataTable SelectAllUserName(string userName)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  USER_CD ");
            stbSql.Append(" ,USER_NAME ");
            stbSql.Append(" ,USER_NAME_KANA ");
            stbSql.Append(" ,APPROVER1 ");
            stbSql.Append(" ,APPROVER2 ");
            stbSql.Append(" ,TEL ");
            stbSql.Append(" ,EMAIL ");
            stbSql.Append(" ,ROOM_ID ");
            stbSql.Append(" ,GROUP_CD ");
            stbSql.Append(" ,AREA_ID ");
            stbSql.Append(" ,ADMIN_FLAG ");
            stbSql.Append(" ,FLOW_CD ");
            stbSql.Append(" ,DEL_FLAG ");
            stbSql.Append(" ,BATCH_FLAG ");
            stbSql.Append("from ");
            stbSql.Append("  M_USER ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG = 0 ");
            stbSql.Append("and ");
            stbSql.Append("  USER_NAME = :USER_NAME ");

            Hashtable _params = new Hashtable();
            _params.Add("USER_NAME", userName);

            return DbUtil.Select(stbSql.ToString(), _params);
        }

        /// <summary>
        
        /// Author: MSCGI 
        /// Created date: 19-7-2019
        /// Description : Dapper changes.
        /// <summary>
        /// <summary>
        /// ユーザーマスタを検索する(削除済みも対象)
        /// </summary>
        /// <param name="userCd">ユーザーコード</param>
        /// <returns>取得結果格納DataTable（USER_NAME昇順）</returns>
        public List<WebCommonInfo> SelectAllUserInfo(string userCd)
        {
            // SELECT SQL生成
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append(" select");
            stbSql.Append("   U0.USER_CD");
            stbSql.Append("  ,U0.USER_NAME");
            stbSql.Append("  ,U0.USER_NAME_KANA");
            stbSql.Append("  ,U0.GROUP_CD AS GROUP_CD");
            stbSql.Append("  ,G.GROUP_NAME AS GROUP_NAME");
            stbSql.Append("  ,U0.APPROVER1");
            stbSql.Append("  ,U0.APPROVER2");
            stbSql.Append("  ,U1.USER_NAME AS APPROVER1_NAME");
            stbSql.Append("  ,U2.USER_NAME AS APPROVER2_NAME");
            stbSql.Append("  ,U0.TEL");
            stbSql.Append("  ,U0.EMAIL");
            stbSql.Append("  ,U0.ADMIN_FLAG");
            stbSql.Append("  ,U0.FLOW_CD AS FLOW_CD");
            stbSql.Append("  ,U0.DEL_FLAG");
            stbSql.Append("  ,IsNull(U0.BATCH_FLAG,0) AS BATCH_FLAG");
            //sbSql.Append("  ,W.USAGE AS FLOW_NAME");
            stbSql.Append(" from");
            stbSql.Append("   M_USER U0");
            stbSql.Append("   left join V_GROUP G  on U0.GROUP_CD  = G.GROUP_CD and G.DEL_FLAG = 0");
            stbSql.Append("   left join M_USER  U1 on U0.APPROVER1 = U1.USER_CD and U1.DEL_FLAG = 0");
            stbSql.Append("   left join M_USER  U2 on U0.APPROVER2 = U2.USER_CD and U2.DEL_FLAG = 0");
            //sbSql.Append("   left join M_WORKFLOW  W on U0.FLOW_CD = W.FLOW_CD and W.DEL_FLAG = 0");
            stbSql.Append(" where");
            stbSql.Append("  U0.USER_CD = @USER_CD ");
            stbSql.Append(" order by");
            stbSql.Append("  U0.USER_CD");

            DynamicParameters param = new DynamicParameters();
            param.Add("@USER_CD", userCd);

            return DbUtil.Select<WebCommonInfo>(stbSql.ToString(), param);
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 29-7-2019
        /// Description : Select statement with Dapper parameters and return list.
        /// <summary>
        public WebCommonInfo SelectAllUserInformation(string userCd)
        {
            // SELECT SQL生成
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append(" select");
            stbSql.Append("   U0.USER_CD");
            stbSql.Append("  ,U0.USER_NAME");
            stbSql.Append("  ,U0.USER_NAME_KANA");
            stbSql.Append("  ,U0.GROUP_CD AS GROUP_CD");
            stbSql.Append("  ,G.GROUP_NAME AS GROUP_NAME");
            stbSql.Append("  ,U0.APPROVER1");
            stbSql.Append("  ,U0.APPROVER2");
            stbSql.Append("  ,U1.USER_NAME AS APPROVER1_NAME");
            stbSql.Append("  ,U2.USER_NAME AS APPROVER2_NAME");
            stbSql.Append("  ,U0.TEL");
            stbSql.Append("  ,U0.EMAIL");
            stbSql.Append("  ,U0.ADMIN_FLAG AS ADMIN_FLAG");
            stbSql.Append("  ,U0.FLOW_CD AS FLOW_CD");
            stbSql.Append("  ,U0.DEL_FLAG");
            stbSql.Append("  ,IsNull(U0.BATCH_FLAG,0) AS BATCH_FLAG");
            //sbSql.Append("  ,W.USAGE AS FLOW_NAME");
            stbSql.Append(" from");
            stbSql.Append("   M_USER U0");
            stbSql.Append("   left join V_GROUP G  on U0.GROUP_CD  = G.GROUP_CD and G.DEL_FLAG = 0");
            stbSql.Append("   left join M_USER  U1 on U0.APPROVER1 = U1.USER_CD and U1.DEL_FLAG = 0");
            stbSql.Append("   left join M_USER  U2 on U0.APPROVER2 = U2.USER_CD and U2.DEL_FLAG = 0");
            //sbSql.Append("   left join M_WORKFLOW  W on U0.FLOW_CD = W.FLOW_CD and W.DEL_FLAG = 0");
            stbSql.Append(" where");
            stbSql.Append("  U0.USER_CD = @USER_CD ");
            stbSql.Append(" order by");
            stbSql.Append("  U0.USER_CD");

            DynamicParameters param = new DynamicParameters();
            param.Add("@USER_CD", userCd);

            return DbUtil.SelectSingle<WebCommonInfo>(stbSql.ToString(), param);
        }

        /// <summary>
        /// ユーザーマスター情報リストを取得する
        /// </summary>
        /// <returns>取得結果格納DataTable</returns>
        //public DataTable selectUser()
        //{
        //    StringBuilder stbSql = new StringBuilder();
        //    stbSql.Append("select ");
        //    stbSql.Append("  USER_CD ");
        //    stbSql.Append(" ,USER_NAME ");
        //    stbSql.Append(" ,USER_NAME_KANA ");
        //    stbSql.Append("from ");
        //    stbSql.Append("  M_USER ");
        //    stbSql.Append("where ");
        //    stbSql.Append("  DEL_FLAG <> 1 ");
        //    stbSql.Append("order by ");
        //    stbSql.Append("  USER_CD ");

        //    Hashtable _params = new Hashtable();

        //    return DbUtil.select(stbSql.ToString(), _params);
        //}

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 24-7-2019
        /// Description : Select statement with Dapper parameters and return list.
        /// <summary>
        public List<M_USER> SelectUser()
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  USER_CD ");
            stbSql.Append(" ,USER_NAME ");
            stbSql.Append(" ,USER_NAME_KANA ");
            stbSql.Append("from ");
            stbSql.Append("  M_USER ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("order by ");
            stbSql.Append("  USER_CD ");

            DynamicParameters param = new DynamicParameters();

            return DbUtil.Select<M_USER>(stbSql.ToString(), param);
        }


        /// <summary>
        /// ユーザーマスタを検索する(コードに対する名称含む)
        /// </summary>
        /// <param name="userCd">ユーザーコード</param>
        /// <returns>取得結果格納DataTable（USER_NAME昇順）</returns>
        public DataTable SelectUserInfo(string userCd)
        {
            // SELECT SQL生成
            StringBuilder stbSql = new StringBuilder(); /*sbSql*/
            stbSql.Append(" select");
            stbSql.Append("   U0.USER_CD");
            stbSql.Append("  ,U0.USER_NAME");
            stbSql.Append("  ,U0.USER_NAME_KANA");
            stbSql.Append("  ,U0.GROUP_CD AS GROUP_CD");
            stbSql.Append("  ,G.GROUP_NAME AS GROUP_NAME");
            stbSql.Append("  ,U0.USER_NAME AS APPROVER1");
            stbSql.Append("  ,U0.USER_NAME AS APPROVER2");
            stbSql.Append("  ,U1.USER_NAME AS APPROVER1_NAME");
            stbSql.Append("  ,U2.USER_NAME AS APPROVER2_NAME");
            stbSql.Append("  ,U0.TEL");
            stbSql.Append("  ,U0.EMAIL");
            stbSql.Append("  ,U0.ADMIN_FLAG");
            stbSql.Append("  ,U0.FLOW_CD AS FLOW_CD");
            stbSql.Append("  ,W.USAGE AS FLOW_NAME");
            stbSql.Append(" from");
            stbSql.Append("   M_USER U0");
            stbSql.Append("   left join M_GROUP G  on U0.GROUP_CD  = G.GROUP_CD and G.DEL_FLAG = 0");
            stbSql.Append("   left join M_USER  U1 on U0.APPROVER1 = U1.USER_CD and U1.DEL_FLAG = 0");
            stbSql.Append("   left join M_USER  U2 on U0.APPROVER2 = U2.USER_CD and U2.DEL_FLAG = 0");
            stbSql.Append("   left join M_WORKFLOW  W on U0.FLOW_CD = W.FLOW_CD and W.DEL_FLAG = 0");
            stbSql.Append(" where");
            stbSql.Append("  U0.DEL_FLAG <> 1 ");
            stbSql.Append(" and ");
            stbSql.Append("  U0.USER_CD = :USER_CD ");
            stbSql.Append(" order by");
            stbSql.Append("  U0.USER_CD");

            Hashtable param = new Hashtable();
            param.Add("USER_CD", userCd);

            return DbUtil.Select(stbSql.ToString(), param);

        }

        /// <summary>
        /// ワークフローマスタを検索する
        /// </summary>
        /// <returns>取得結果格納DataTable</returns>
        /// 
        //public DataTable selectWorkFlow()
        //{
        //    StringBuilder stbSql = new StringBuilder();
        //    stbSql.Append("select ");
        //    stbSql.Append("  FLOW_CD ");
        //    stbSql.Append(" ,USAGE AS FLOW_NAME ");
        //    stbSql.Append("from ");
        //    stbSql.Append("  M_WORKFLOW ");
        //    stbSql.Append("where ");
        //    stbSql.Append("  DEL_FLAG <> 1 ");
        //    stbSql.Append("order by ");
        //    stbSql.Append(" FLOW_CD ");

        //    return DbUtil.select(stbSql.ToString());
        //}

        /// <summary>
        
        /// Author: MSCGI 
        /// Created date: 24-7-2019
        /// Description : To Return the Work Flow data by setting it in the M_WORKFLOW Model class.
        /// <summary>
        public List<M_WORKFLOW> SelectWorkFlow()
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  FLOW_CD ");
            stbSql.Append(" ,USAGE AS FLOW_NAME ");
            stbSql.Append("from ");
            stbSql.Append("  M_WORKFLOW ");
            stbSql.Append("where ");
            stbSql.Append("  DEL_FLAG <> 1 ");
            stbSql.Append("order by ");
            stbSql.Append(" FLOW_CD ");

            return DbUtil.Select<M_WORKFLOW>(stbSql.ToString(),null);
        }
        /// <summary>
        /// V_GROUPの検索
        /// </summary>
        /// <returns></returns>
        public DataTable SelectVGroup(string strGroupCd)
        {
            string sql = "SELECT * FROM V_GROUP WHERE GROUP_CD = '" + strGroupCd + "'";

            return DbUtil.Select(sql);
        }

        //2019/03/09 TPE.Rin
        public DataTable SelectMcommon(string strTableName, string strVal)
        {
            string repStrVal = strVal.Replace(',', ';');
            string[] arr = repStrVal.Split(';');
            //string[] arr = strVal.Split(',');
            string value = "";
            foreach (string val in arr)
            {
                value += "'" + val + "',";
            }
            value = value.Trim(',');

            StringBuilder sql = new StringBuilder();
            sql.Append("select DISPLAY_VALUE from M_COMMON");
            sql.Append(" where TABLE_NAME = '" + strTableName + "'");
            sql.Append(" and KEY_VALUE in (" + value + ")");
            sql.Append(" order by DISPLAY_ORDER");

            return DbUtil.Select(sql.ToString());
        }

        /// <summary>
        /// 消防法届出マスタを検索する（削除済み含む）
        /// </summary>
        /// <param name="reportId">届出ID</param>
        /// <param name="reportName">届出名称</param>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable SelectFireReport(string reportId, string reportName)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  REPORT_ID ");
            stbSql.Append("from ");
            stbSql.Append("  M_FIRE_REPORT ");
            stbSql.Append("where ");
            if (reportId == string.Empty)
            {
                stbSql.Append("  REPORT_NAME = :REPORT_NAME ");
            }
            else
            {
                stbSql.Append("  REPORT_ID != :REPORT_ID ");
                stbSql.Append("  AND REPORT_NAME = :REPORT_NAME ");
            }
            stbSql.Append("order by ");
            stbSql.Append("  REPORT_ID ");

            Hashtable param = new Hashtable();
            if (reportId == string.Empty)
            {
                param.Add("REPORT_NAME", reportName);
            }
            else
            {
                param.Add("REPORT_ID", reportId);
                param.Add("REPORT_NAME", reportName);
            }
            return DbUtil.Select(stbSql.ToString(), param);
        }
        /// <summary>
        /// 消防法届出マスタを検索する（削除済み含む）
        /// </summary>
        /// <param name="ReportId">届出ID</param>
        /// <param name="ReportName">届出名称</param>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable selectFireReport(string ReportId, string ReportName, string ReportYMD)
        {
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append("select ");
            stbSql.Append("  REPORT_ID ");
            stbSql.Append("from ");
            stbSql.Append("  M_FIRE_REPORT ");
            stbSql.Append("where ");
            if (ReportId == string.Empty)
            {
                stbSql.Append("  REPORT_NAME = :REPORT_NAME ");
            }
            else
            {
                stbSql.Append("  REPORT_ID != :REPORT_ID ");
                stbSql.Append("  AND REPORT_NAME = :REPORT_NAME ");
            }
            stbSql.Append("order by ");
            stbSql.Append("  REPORT_ID ");

            Hashtable _params = new Hashtable();
            if (ReportId == string.Empty)
            {
                _params.Add("REPORT_NAME", ReportName);
            }
            else
            {
                _params.Add("REPORT_ID", ReportId);
                _params.Add("REPORT_NAME", ReportName);
            }
            return DbUtil.Select(stbSql.ToString(), _params);
        }

    }
}