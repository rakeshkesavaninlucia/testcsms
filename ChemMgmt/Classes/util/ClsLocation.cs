﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Text;
using ZyCoaG.Classes.util;

namespace ZyCoaG.util
{
    /// <summary>
    /// 保管場所データメソッド群クラス
    /// </summary>
    public class ClsLocation
    {
        #region メンバー
        /// <summary>
        /// 保管場所データ保持構造体
        /// </summary>
        public struct LocationData
        {
            public long LOC_ID;
            public string BARCODE;
            public string LOC_NAME;
            public long LEVEL_NO;
            public long WEIGHT_FLAG;
        }
        /// <summary>
        /// 保管場所データ保持構造体
        /// </summary>
        private LocationData[] _location_data = new LocationData[] { };
        /// <summary>
        /// 保管場所データ保持件数
        /// </summary>
        private int intLocCount = 0;
        /// <summary>
        /// 保管場所データ保持最大階層
        /// </summary>
        private int intMaxLevelNo = 0;
        #endregion

        #region プロパティ

        /// <summary>
        /// 最大階層アクセスプロパティ
        /// </summary>
        public int MAX_LEVEL_NO { get { return intMaxLevelNo; } }
        /// <summary>
        /// 設定したデータ件数
        /// </summary>
        public int LOC_COUNT { get { return intLocCount; } }
        /// <summary>
        /// 紐付された保管場所データ全件を取得します。
        /// </summary>
        public LocationData[] LOCATION_DATA_ALL { get { return _location_data; } }
        /// <summary>
        /// 保管場所コード指定された保管場所IDを返します。存在しない場合は-1を返します。
        /// </summary>
        /// <remarks>前提条件として、事前にSetLocationInfo(string barcode)メソッドが呼ばれていること。</remarks>
        public long LOC_ID
        {
            get { return (long)LocationDataItem("LOC_ID"); }
        }
        /// <summary>
        /// 保管場所コード指定された保管場所コードを返します。存在しない場合はnull値を返します。
        /// </summary>
        /// <remarks>前提条件として、事前にSetLocationInfo(string barcode)メソッドが呼ばれていること。</remarks>
        public string BARCODE
        {
            get { return (string)LocationDataItem("LOC_BARCODE"); }
        }
        /// <summary>
        /// 保管場所コード指定された保管場所名を返します。存在しない場合はnull値を返します。
        /// </summary>
        /// <remarks>前提条件として、事前にSetLocationInfo(string barcode)メソッドが呼ばれていること。</remarks>
        public string LOC_NAME
        {
            get { return (string)LocationDataItem("LOC_NAME"); }
        }
        /// <summary>
        /// 保管場所コード指定された階層レベルを返します。存在しない場合は-1を返します。
        /// </summary>
        /// <remarks>前提条件として、事前にSetLocationInfo(string barcode)メソッドが呼ばれていること。</remarks>
        public long LEVEL_NO
        {
            get { return (long)LocationDataItem("LEVEL_NO"); }
        }
        /// <summary>
        /// 保管場所コード指定された重量フラグを返します。存在しない場合は-1を返します。
        /// </summary>
        /// <remarks>前提条件として、事前にSetLocationInfo(string barcode)メソッドが呼ばれていること。</remarks>
        public long WEIGHT_FLAG
        {
            get { return (long)LocationDataItem("WEIGHT_FLAG"); }
        }
        #endregion

        #region 保管場所情報を保管場所ID、階層順に保持し、内部変数へ設定します。
        /// <summary>
        /// 保管場所情報を保管場所ID、階層順に保持し、内部変数へ設定します。
        /// </summary>
        public void SetLocationInfo()
        {
            SetLocationInfo(null);
        }

        /// <summary>
        /// 保管場所情報を保管場所ID、階層順に保持し、内部変数へ設定します。
        /// 保管場所コード指定すると、その保管場所コードに紐づく階層データのみ保持します。
        /// ただし指定した保管場所は最下層に値しなければデータは0件で設定します。
        /// </summary>
        /// <param name="barcode">保管場所コード</param>
        public void SetLocationInfo(string barcode)
        {
            int maxLevel = 0;
            // 保管場所データを全件取得後、階層毎のDataTableにカスタマイズ
            DataTable dt = DbUtil.Select(GetSqlAllLocation());
            dt = GetCustomLocationDataTable(dt, ref maxLevel);
            if (dt.Rows.Count > 0)
            {
                if (barcode != null)
                {
                    // 指定したバーコードに紐づく階層のみを取得
                    // データが不正な階層があれば、0件で取得します。
                    dt = GetLocationDataTable_BarCode(barcode, dt, maxLevel);
                }

                int cnt = dt.Rows.Count;
                if (cnt > 0)
                {
                    this._location_data = new LocationData[cnt];
                    for (int i = 0; i < cnt; i++)
                    {
                        this._location_data[i].LOC_ID = Convert.ToInt32(dt.Rows[i]["LOC_ID"]);
                        this._location_data[i].BARCODE = Convert.ToString(dt.Rows[i]["LOC_BARCODE"]);
                        this._location_data[i].LOC_NAME = Convert.ToString(dt.Rows[i]["LOC_NAME"]);
                        this._location_data[i].LEVEL_NO = Convert.ToInt32(dt.Rows[i]["LEVEL_NO"]);
                        this._location_data[i].WEIGHT_FLAG = Convert.ToInt32(dt.Rows[i]["WEIGHT_FLAG"]);
                    }
                    this.intLocCount = cnt;
                    this.intMaxLevelNo = maxLevel;
                }
            }
            else
            {
                // 保管場所保持用パラメータの初期化
                this._location_data = new LocationData[] { };
                this.intLocCount = 0;
                this.intMaxLevelNo = 0;
            }
        }

        /// <summary>
        /// 保管場所情報を保管場所ID、階層順に保持し、内部変数へ設定します。（あいまい検索）
        /// 保管場所コード指定すると、その保管場所コードに紐づく階層データのみ保持します。
        /// ただし指定した保管場所は最下層に値しなければデータは0件で設定します。
        /// </summary>
        /// <param name="wildCard">キーワード</param>
        public void SetLocationInfoByWildCard(string wildCard)
        {
            int maxLevel = 0;
            // 保管場所データを全件取得後、階層毎のDataTableにカスタマイズ
            DataTable dt = DbUtil.Select(GetSqlAllLocation());
            dt = GetCustomLocationDataTable(dt, ref maxLevel);
            if (dt.Rows.Count > 0)
            {
                if (wildCard != null)
                {
                    // 指定したバーコードに紐づく階層のみを取得
                    // データが不正な階層があれば、0件で取得します。
                    dt = GetLocationDataTable_WildCard(wildCard, dt, maxLevel);
                }

                int cnt = dt.Rows.Count;
                if (cnt > 0)
                {
                    this._location_data = new LocationData[cnt];
                    for (int i = 0; i < cnt; i++)
                    {
                        this._location_data[i].LOC_ID = Convert.ToInt32(dt.Rows[i]["LOC_ID"]);
                        this._location_data[i].BARCODE = Convert.ToString(dt.Rows[i]["LOC_BARCODE"]);
                        this._location_data[i].LOC_NAME = Convert.ToString(dt.Rows[i]["LOC_NAME"]);
                        this._location_data[i].LEVEL_NO = Convert.ToInt32(dt.Rows[i]["LEVEL_NO"]);
                        this._location_data[i].WEIGHT_FLAG = Convert.ToInt32(dt.Rows[i]["WEIGHT_FLAG"]);
                    }
                    this.intLocCount = cnt;
                    this.intMaxLevelNo = maxLevel;
                }
            }
            else
            {
                // 保管場所保持用パラメータの初期化
                this._location_data = new LocationData[] { };
                this.intLocCount = 0;
                this.intMaxLevelNo = 0;
            }
        }

        /// <summary>
        /// M_LOCATION格納DataTableから指定した保管場所コードに紐づく階層のみを取得したDataTableを作成します。
        /// </summary>
        /// <param name="barcode">指定した保管場所</param>
        /// <param name="dt">M_LOCATION格納DataTable</param>
        /// <param name="maxLevel">最下層のレベル</param>
        /// <returns>作成したDataTable</returns>
        /// <remarks>
        /// 以下の通りのデータでなければ、空のデータテーブルを返します。
        /// ・指定した保管場所コードは最下層レベルであること。
        /// ・作成したDataTableは最下層のレベルと同じ件数であること。（１階層につき１件と考える為。）
        /// </remarks>
        private DataTable GetLocationDataTable_BarCode(string barcode, DataTable dt, int maxLevel)
        {
            DataTable ret = dt.Clone();
            DataRow[] drs = dt.Select("LEVEL_NO = " + maxLevel.ToString() + " and LOC_BARCODE = '" + barcode.ToUpper() + "'");
            // 取得データは１件であること。
            if (drs.Length == 1)
            {
                ret.ImportRow(drs[0]);
                GetLocationDataTable_BarCode(ref ret, dt, drs[0]["P_LOC_ID"].ToString(), maxLevel - 1);
                if (ret.Rows.Count != maxLevel)
                {
                    ret = dt.Clone();
                }
            }
            return ret;
        }

        /// <summary>
        /// M_LOCATION格納DataTableから指定した保管場所コードに紐づく階層のみを取得したDataTableを作成します。（あいまい検索）
        /// </summary>
        /// <param name="wildCard">キーワード</param>
        /// <param name="dt">M_LOCATION格納DataTable</param>
        /// <param name="maxLevel">最下層のレベル</param>
        /// <returns>作成したDataTable</returns>
        /// <remarks>
        /// 以下の通りのデータでなければ、空のデータテーブルを返します。
        /// ・指定した保管場所コードは最下層レベルであること。
        /// ・作成したDataTableは最下層のレベルと同じ件数であること。（１階層につき１件と考える為。）
        /// </remarks>
        private DataTable GetLocationDataTable_WildCard(string wildCard, DataTable dt, int maxLevel)
        {
            DataTable ret = dt.Clone();
            DataRow[] drs = dt.Select("LEVEL_NO = " + maxLevel.ToString() + " and LOC_BARCODE like '%" + DbUtil.ConvertIntoEscapeChar(wildCard) + "%'" + DbUtil.LikeEscapeInfoAdd());
            // 取得データは１件であること。
            if (drs.Length == 1)
            {
                ret.ImportRow(drs[0]);
                GetLocationDataTable_BarCode(ref ret, dt, drs[0]["P_LOC_ID"].ToString(), maxLevel - 1);
                if (ret.Rows.Count != maxLevel)
                {
                    ret = dt.Clone();
                }
            }
            return ret;
        }

        /// <summary>
        /// M_LOCATION格納DataTableから指定した保管場所ID、階層レベルをを検索し再帰ループでDataTableを作成します。
        /// 再帰ループ処理なので、階層が存在するだけ何度も呼び出されます。
        /// </summary>
        /// <param name="dtRet">作成したDataTable</param>
        /// <param name="getDt">M_LOCATION格納DataTable</param>
        /// <param name="loc_id">指定保管場所ID</param>
        /// <param name="iLevel">指定階層レベル</param>
        private void GetLocationDataTable_BarCode(ref DataTable dtRet, DataTable getDt, string loc_id, int iLevel)
        {
            if (loc_id.Length > 0 && iLevel > 0)
            {
                DataRow[] drs = getDt.Select("LEVEL_NO = " + iLevel.ToString() + " and LOC_ID = " + loc_id);
                // 取得データは１件であること。
                if (drs.Length == 1)
                {
                    dtRet.ImportRow(drs[0]);
                    GetLocationDataTable_BarCode(ref dtRet, getDt, drs[0]["P_LOC_ID"].ToString(), iLevel - 1);
                }
            }
        }

        /// <summary>
        /// M_LOCATION格納DataTableを保管場所IDで紐づけた階層DatatTableへカスタマイズします。
        /// </summary>
        /// <param name="getDt">M_LOCATION格納DataTable</param>
        /// <returns>カスタマイズ後のDataTable</returns>
        private DataTable GetCustomLocationDataTable(DataTable getDt, ref int maxLevel)
        {
            DataTable retDt = getDt.Clone();
            if (getDt.Rows.Count > 0)
            {
                DataRow[] drs = getDt.Select("LEVEL_NO = MAX(LEVEL_NO)");
                if (drs.Length > 0 && Common.IsNumber(drs[0]["LEVEL_NO"].ToString()))
                {
                    maxLevel = Convert.ToInt32(drs[0]["LEVEL_NO"]);
                    SortLocationDataTable(ref retDt, getDt, 1, null, maxLevel);   // 親保管場所に紐づく、階層DataTableを作成します。。（再帰）
                }
            }
            return retDt;
        }

        /// <summary>
        /// M_LOCATION格納DataTableから保管場所IDで紐づけた階層DatatTableを作成します。（階層順）
        /// 階層レベル1の保管場所を取得後、同保管場所IDを持つ階層2以降のデータを再帰ループにより取得します。
        /// 再帰ループ処理なので、階層が存在するだけ何度も呼び出されます。
        /// </summary>
        /// <param name="retDt">作成したDataTable</param>
        /// <param name="getDt">M_LOCATION格納DataTable。レベル、保管場所IDの昇順に格納されていることが前提。</param>
        /// <param name="iLevel">作成する階層のレベル</param>
        /// <param name="loc_id">作成する親の保管場所ID。レベル1の場合は親がいない為、不要。</param>
        /// <param name="iMaxLevel">最大レベル</param>
        private void SortLocationDataTable(ref DataTable retDt, DataTable getDt, int iLevel, string loc_id, int iMaxLevel)
        {
            string where = null;

            // 階層と保管場所ID（レベル2以降のみ）に紐づくデータを検索
            if (iLevel == 1)
            {
                where = "LEVEL_NO = " + iLevel.ToString();
            }
            else if (Common.IsNumber(loc_id))
            {
                where = "LEVEL_NO = " + iLevel.ToString() + " and P_LOC_ID = " + loc_id;
            }

            if (where != null)
            {
                foreach (DataRow dr in getDt.Select(where))
                {
                    retDt.ImportRow(dr);
                    // 次の階層に自身に紐づく保管場所が存在するかを確認します。（再帰）
                    SortLocationDataTable(ref retDt, getDt, iLevel + 1, dr["LOC_ID"].ToString(), iMaxLevel);
                }
            }
        }

        /// <summary>
        /// 保管場所マスタをレベル、保管場所IDの昇順で全件取得するSQLを作成します。
        /// </summary>
        /// <returns>SQL文</returns>
        private string GetSqlAllLocation()
        {
            StringBuilder ret = new StringBuilder();

            ret.Append(" select ");
            ret.Append("  LOC_ID ");
            ret.Append(", LOC_BARCODE ");
            ret.Append(", LOC_NAME ");
            ret.Append(", P_LOC_ID ");
            ret.Append(", LEVEL_NO ");
            ret.Append(", WEIGHT_FLAG ");
            ret.Append("from M_LOCATION ");
            ret.Append("where ");
            ret.Append("  LEVEL_NO is not null ");
            ret.Append("and DEL_FLAG = 0 ");
            ret.Append("order by ");
            ret.Append("  LEVEL_NO ");
            ret.Append(", LOC_ID ");

            return ret.ToString();

        }
        #endregion

        #region 指定された保管場所コードデータの指定項目の値返します。
        /// <summary>
        /// 指定された保管場所コードデータの指定項目の値返します。
        /// 存在しない場合、String型項目はnullを返し、数値型項目は-1を返します。
        /// </summary>
        /// <param name="col">指定項目</param>
        /// <returns>指定項目の値</returns>
        /// <remarks>
        /// 指定された保管場所は保管場所マスタの最大レベルと同じレベルであること。
        /// そうでない場合は、文字列の場合nullを数値の場合は-1を返します。
        /// </remarks>
        private object LocationDataItem(string col)
        {
            object ret = null;
            if (_location_data != null && MAX_LEVEL_NO > 0 && _location_data.Length == MAX_LEVEL_NO)
            {
                // 配列0が一番深い階層
                switch (col)
                {
                    case "LOC_BARCODE":
                        ret = _location_data[0].BARCODE;
                        break;
                    case "LOC_NAME":
                        ret = _location_data[0].LOC_NAME;
                        break;
                    case "LOC_ID":
                        ret = _location_data[0].LOC_ID;
                        break;
                    case "LEVEL_NO":
                        ret = _location_data[0].LEVEL_NO;
                        break;
                    case "WEIGHT_FLAG":
                        ret = _location_data[0].WEIGHT_FLAG;
                        break;
                }
            }
            else
            {
                switch (col)
                {
                    case "LOC_BARCODE":
                    case "LOC_NAME":
                        ret = null;
                        break;
                    case "LOC_ID":
                    case "LEVEL_NO":
                    case "WEIGHT_FLAG":
                        ret = -1;
                        break;
                }
            }
            return ret;
        }
        #endregion
    }
}