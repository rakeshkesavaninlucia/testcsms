﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemDesign.Classes.util
{
    public class Const
    {
        public const string cCR_MARK = "\r\n";
        /// <summary>
        /// 検索方式：部分一致
        /// </summary>
        public const int cSEARCH_TYPE_CONTAINS = 0;

        /// <summary>
        /// 検索方式：前方一致
        /// </summary>
        public const int cSEARCH_TYPE_STARTSWITH = 1;

        /// <summary>
        /// 検索方式：完全一致
        /// </summary>
        public const int cSEARCH_TYPE_EQUALTO = 2;

        /// <summary>
        /// 検索方式：後方一致
        /// </summary>
        public const int cSEARCH_TYPE_ENDSWITH = 3;

        /// <summary>
        /// 処理モード：登録
        /// </summary>
        public const int cEXEC_MODE_INS = 0;

        /// <summary>
        /// 処理モード：更新
        /// </summary>
        public const int cEXEC_MODE_UPD = 1;

        /// <summary>
        /// 処理モード：削除
        /// </summary>
        public const int cEXEC_MODE_DEL = 2;

        /// <summary>
        /// 処理モード：検索
        /// </summary>
        public const int cEXEC_MODE_SEARCH = 3;

        /// <summary>
        /// 処理モード：コピー
        /// </summary>
        public const int cEXEC_MODE_COPY = 4;

        /// <summary>
        /// 処理モード：作成　ver.2では仮受入画面で使用。ver.3未使用。
        /// </summary>
        public const int cEXEC_MODE_CREATE = 5;

        /// <summary>
        /// 構造式検索方式：Substructure
        /// </summary>
        public const int cSTRUCTURE_SUBSTRUCTURE = 0;

        /// <summary>
        /// 構造式検索方式：Full Structure
        /// </summary>
        public const int cSTRUCTURE_FULL = 1;

        /// <summary>
        /// 構造式検索方式：Exact Structure
        /// </summary>
        public const int cSTRUCTURE_EXACT = 2;

        /// <summary>
        /// 完納フラグ（完了）
        /// </summary>
        public const int cDELI_FLG_FIN = 1;

        /// <summary>
        /// 鍵・重量管理：必須
        /// </summary>
        public const int cMANAGEMENT_TYPE_REQUIRED = 0;

        /// <summary>
        /// 鍵・重量管理：推奨
        /// </summary>
        public const int cMANAGEMENT_TYPE_RECOMMENDATION = 1;

        /// <summary>
        /// 形状：固体
        /// </summary>
        public const int cFIGURE_TYPE_SOLID = 0;

        /// <summary>
        /// 形状：粉体
        /// </summary>
        public const int cFIGURE_TYPE_POWDER = 1;

        /// <summary>
        /// 形状：液体
        /// </summary>
        public const int cFIGURE_TYPE_LIQUID = 2;

        /// <summary>
        /// 形状：気体
        /// </summary>
        public const int cFIGURE_TYPE_GAS = 3;

        /// <summary>
        /// 成分：酸
        /// </summary>
        public const int cCOMPONENT_TYPE_ACID = 0;

        /// <summary>
        /// 成分：アルカリ
        /// </summary>
        public const int cCOMPONENT_TYPE_ALKALI = 1;

        /// <summary>
        /// 成分：有機溶剤
        /// </summary>
        public const int cCOMPONENT_TYPE_ORGANIC_SOLVENE = 2;

        /// <summary>
        /// セッション名：エラー画面へ表示するエラー内容
        /// </summary>
        public const string cSESSION_NAME_ERRMSG = "ZC01090_MSG";

        /// <summary>
        /// セッション名：ユーザー情報
        /// </summary>
        public const string cSESSION_NAME_USER_INFO = "USER_INFO";

        /// <summary>
        /// エラー画面プログラムID
        /// </summary>
        public const string cERROR_PROGRAM_ID = "ZC01090";

        /// <summary>
        /// 相対パス：エラー画面
        /// </summary>
        public const string cPATH_ERROR_ASPX = "../ZC010/ZC01090.aspx";

        /// <summary>
        /// 相対パス：ログイン画面
        /// </summary>
        public const string cPATH_LOGIN_ASPX = "../ZC010/ZC01001.aspx";

        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：Add Cartボタン
        /// </summary>
        public const string cBTN_HEAD_ADD_CART = "btnHeader_AddCart";

        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：Go Cartボタン
        /// </summary>
        public const string cBTN_HEAD_GO_CART = "btnHeader_GoCart";

        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：戻るボタン
        /// </summary>
        public const string cBTN_HEAD_BACK = "btnHeader_Back";

        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：ログアウトボタン
        /// </summary>
        public const string cBTN_HEAD_LOG_OUT = "btnHeader_LogOut";

        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：参照権限ボタン
        /// </summary>
        public const string cBTN_REFERENCE_AUTHORITY = "btnHeader_LegerSearchTarget";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン－１
        /// </summary>
        public const string cBTN_FOOT_m1 = "btnFooter_m1";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン－２
        /// </summary>
        public const string cBTN_FOOT_m2 = "btnFooter_m2";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン－１
        /// </summary>
        public const string cBTN_FOOT_m3 = "btnFooter_m3";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン１
        /// </summary>
        public const string cBTN_FOOT_1 = "btnFooter_1";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン２
        /// </summary>
        public const string cBTN_FOOT_2 = "btnFooter_2";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン３
        /// </summary>
        public const string cBTN_FOOT_3 = "btnFooter_3";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン４
        /// </summary>
        public const string cBTN_FOOT_4 = "btnFooter_4";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン５
        /// </summary>
        public const string cBTN_FOOT_5 = "btnFooter_5";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン６
        /// </summary>
        public const string cBTN_FOOT_6 = "btnFooter_6";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン７
        /// </summary>
        public const string cBTN_FOOT_7 = "btnFooter_7";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン８
        /// </summary>
        public const string cBTN_FOOT_8 = "btnFooter_8";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン９
        /// </summary>
        public const string cBTN_FOOT_9 = "btnFooter_9";

        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン１０
        /// </summary>
        public const string cBTN_FOOT_10 = "btnFooter_10";

        /// <summary>
        /// ユーザーコントロール　保管場所選択：保管場所選択ボタン
        /// </summary>
        public const string cBTN_LOCATION_SELECT = "btnLocationSel";

        /// <summary>
        /// ユーザーコントロール　保管場所選択：クリアボタン
        /// </summary>
        public const string cBTN_LOCATION_CLEAR = "btnLocationClr";

        /// <summary>
        /// ユーザーコントロール　保管場所選択：入力項目
        /// </summary>
        public const string cTXT_LOCATION_BARCODE = "txtLocation";

        /// <summary>
        /// T_ORDERテーブルステータス
        /// </summary>
        public enum cORDER_STATUS_ID
        {
            /// <summary>101:ショッピングカート</summary>
            SHOPPING = 101,
            /// <summary>102:発注申請中</summary>
            ORDER_APPLIED,
            /// <summary>103:承認済</summary>
            ORDER_APPROVE,
            /// <summary>104:本発注</summary>
            ORDER_SET,
            /// <summary>105:発注確認済</summary>
            ORDER_CONF,
            /// <summary>106:納品待</summary>
            DELI_WAIT,
            /// <summary>107:納品済</summary>
            DELI_FIN,
            /// <summary>108:完了</summary>
            ORDER_FIN,
            /// <summary>109:キャンセル</summary>
            CANCEL,
            /// <summary>110:否認</summary>
            DENIAL,
            /// <summary>111:発注取消し</summary>
            ORDER_CANCEL,
            /// <summary>112:返品</summary>
            RETURN
        }

        /// <summary>
        /// T_ORDERテーブル注文タイプ
        /// </summary>
        public enum cORDER_ORDER_TYPE
        {
            /// <summary>1:カタログ</summary>
            CATALOG = 1,
            /// <summary>2:在庫</summary>
            STOCK,
            /// <summary>3:履歴</summary>
            HISTORY,
            /// <summary>4:再発注</summary>
            REORDER
        }

        /// <summary>
        /// T_STOCKテーブルのステータス
        /// </summary>
        public enum cSTOCK_STATUS_ID
        {
            /// <summary>200:納品待</summary>
            DELI_WAIT = 200,
            /// <summary>201:納品済</summary>
            DELI_FIN,
            /// <summary>202:受領済</summary>
            REC_FIN,
            /// <summary>203:在庫</summary>
            STOCK,
            /// <summary>204:貸出</summary>
            BORROW,
            /// <summary>205:廃棄</summary>
            DISPOSE,
            /// <summary>206:使切</summary>
            EMPTY
        }

        /// <summary>
        /// T_ACTION_HISTORYテーブルのACTION_TYPE
        /// </summary>
        public enum cACTION_HISTORY_TYPE
        {
            /// <summary>1:初回登録</summary>
            FIRST_ADD = 1,
            /// <summary>2:貸出</summary>
            BORROW,
            /// <summary>3:返却</summary>
            REPAY,
            /// <summary>4:廃棄</summary>
            DISPOSE,
            /// <summary>5:移管</summary>
            MOVE,
            /// <summary>6:使切</summary>
            EMPTY,
            /// <summary>7:棚卸</summary>
            INVENTORY
        }

        /// <summary>
        /// ユーザーコントロール（社員選択、法規制選択、保管場所選択）処理モード
        /// </summary>
        public enum cUSER_SELECT
        {
            SELECT = 0,     // 選択
            CLOSE           // 閉じる（またはブラウザの×ボタン）
        }

        /// <summary>
        /// 法規制・プロダクト名取得関数(oracle stored function)の引数
        /// </summary>
        public const string cREGULATORY_M_PRODUCT = "0";

        /// <summary>
        /// 法規制・プロダクト名取得関数(oracle stored function)の引数
        /// </summary>
        public const string cREGULATORY_T_STOCK = "1";

        /// <summary>
        /// 法規制・プロダクト名取得関数(oracle stored function)の引数
        /// </summary>
        public const string cREGULATORY_T_ORDER = "2";

        /// <summary>
        /// Modal表示する画面の幅（px）
        /// </summary>
        public const string cMODAL_DISP_WIDTH = "1276";

        /// <summary>
        /// Modal表示する画面の高さ（px）
        /// </summary>
        public const string cMODAL_DISP_HEIGHT = "726";

        /// <summary>
        /// 管理者フラグ：一般
        /// </summary>
        public const string cADMIN_FLG_NORMAL = "0";

        /// <summary>
        /// 管理者フラグ：管理者
        /// </summary>
        public const string cADMIN_FLG_ADMIN = "1";

        /// <summary>
        /// 参照権限フラグ：一般ユーザー
        /// </summary>
        public const string cREFERENCE_AUTHORITY_FLAG_NORMAL = "0";

        /// <summary>
        /// 参照権限フラグ：管理者
        /// </summary>
        public const string cREFERENCE_AUTHORITY_FLAG_ADMIN = "1";

        /// <summary>
        /// 管理者フラグ：受入
        /// </summary>
        public const string cADMIN_FLG_ACCEPT = "2";

        /// <summary>
        /// 法規制パターンに対する権限
        /// </summary>
        public enum cGRANT_TYPE
        {
            /// <summary>0:全権限あり</summary>
            ADMIN = 0,
            /// <summary>1:発注権限あり</summary>
            ORDER,
            /// <summary>2:使用権限あり</summary>
            USE
        }

        /// <summary>
        /// 構造式BASE64の文字数の上限
        /// </summary>
        public const int cLIMIT_BASE64_LENGTH = 80000;

        /// <summary>
        /// ステータスマスタ取得関数(selectStatus)の引数
        /// 1:発注ステータス
        /// </summary>
        public const int cSTATUS_CLASS_ID_T_ORDER = 1;

        /// <summary>
        /// ステータスマスタ取得関数(selectStatus)の引数
        /// 2:在庫ステータス
        /// </summary>
        public const int cSTATUS_CLASS_ID_T_STOCK = 2;

        /// <summary>
        /// 保管場所コード：使い切り
        /// </summary>
        public const string cLOC_BARCODE_EMPTY = "TKK";

        /// <summary>
        /// 保管場所コード：廃棄
        /// </summary>
        public const string cLOC_BARCODE_DISPOSE = "HIK";

        /// <summary>
        /// 試薬バーコード：一般消耗品
        /// </summary>
        public const string cBARCODE_GENERAL_CONSUMABLES = "SHO";

        /// <summary>
        /// 試薬バーコード：共通消耗品
        /// </summary>
        public const string cBARCODE_COMMON_CONSUMABLES = "SHM";

        /// <summary>
        /// 削除フラグ：削除
        /// </summary>
        public const string cDB_DELETE_FLG = "1";

        /// <summary>
        /// 削除フラグ：削除取消
        /// </summary>
        public const string cDB_DELETE_FLG_INVALID = "0";

        /// <summary>
        /// グリッドのハイパーリンクコントロールの文字色
        /// </summary>
        public const int cHYPER_LINK_COLOR = 0x0033cc;

        /// <summary>
        /// ユーザーメーカー：新規追加
        /// </summary>
        public const string cUSER_MAKER_ADD = "ADD";

        /// <summary>
        /// ユーザーメーカー：変更
        /// </summary>
        public const string cUSER_MAKER_MOD = "MOD";

        /// <summary>
        /// 起動確認フラグ
        /// </summary>
        public const string cSTARTUP_FLG = "ON";

        /// <summary>
        /// カタログテーブルタイプ（M_PRODUCT,M_QUANTITY）
        /// </summary>
        public const int cCAT_TBL_TYPE = 0;

        /// <summary>
        /// 検索処理：継続
        /// </summary>
        public const string cSEARCH_OK = "1";

        /// <summary>
        /// 警告ダイアログ表示
        /// </summary>
        public const string cAlert_Dialog_Disp = "1";
    }
}