﻿using IdentityManagement.Entities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using ZyCoaG.Nikon;
using ZyCoaG.util;
//using ZyCoaG.Nikon;

namespace ZyCoaG.Classes.util
{
    //    /// <summary>
    //    /// Zycoagの共通メソッド群です。
    //    /// </summary>
    //    /// <remarks>
    //    /// </remarks>
    public static class Common
    {
        public static WebCommonInfo selectKeys = new WebCommonInfo();
        private static Encoding sjisEnc = Encoding.GetEncoding("Shift_JIS");
        public static string webSiteRoot;

        //        /// <summary>データベースの種類（application_start時にweb.configから読み込みます）</summary>
        public static string DatabaseType;

        /// <summary>検索結果の上限件数（application_start時にweb.configから読み込みます）</summary>
        public static long LimitResultCountCatalog;
        /// <summary>検索結果の上限件数（application_start時にweb.configから読み込みます）</summary>
        public static long LimitResultCountStock;
        /// <summary>検索結果の上限件数（application_start時にweb.configから読み込みます）</summary>
        public static long LimitResultCountHistory;

        /// <summary>検索結果の警告件数（application_start時にweb.configから読み込みます）</summary>
        public static long AlertResultCountCatalog;
        /// <summary>検索結果の警告件数（application_start時にweb.configから読み込みます）</summary>
        public static long AlertResultCountStock;
        /// <summary>検索結果の警告件数（application_start時にweb.configから読み込みます）</summary>
        public static long AlertResultCountHistory;

        /// <summary>検索結果の警告ダイアログ（application_start時にweb.configから読み込みます）</summary>
        public static string AlertResultDialog;
        /// <summary>検索結果の上限到達警告ダイアログ（application_start時にweb.configから読み込みます）</summary>
        public static string LimitResultDialog;

        /// <summary>CRAIS keyField（application_start時にweb.configから読み込みます）</summary>
        public static string keyField;
        /// <summary>CRAIS checkLevel（application_start時にweb.configから読み込みます）</summary>
        public static string checkLevel;
        /// <summary>CRAIS appName（application_start時にweb.configから読み込みます）</summary>
        public static string appName;
        /// <summary>CRAIS keyField（application_start時にweb.configから読み込みます）</summary>
        public static string CRAIS_URL;

        /// <summary>
        /// パスワードをハッシュ値に変換します。
        /// </summary>
        /// <paramMap name="strPassword">パスワード</paramMap>
        /// <returns>ハッシュ値</returns>
        public static string GetSHA1Hash(string strPassword)
        {
            // string から byte配列へ変換
            byte[] bArrayPassword = Encoding.Unicode.GetBytes(strPassword);

            // ハッシュ値取得
            byte[] bHash = System.Security.Cryptography.SHA1.Create().ComputeHash(bArrayPassword);

            return Convert.ToBase64String(bHash);
        }

        #region メッセージ取得 (+ 3)
        /// <summary>
        /// メッセージを取得します。（変換値がない場合）
        /// </summary>
        /// <param name="strMsgID">該当するメッセージID</param>
        /// <returns>取得したメッセージ内容</returns>
        public static string GetMessage(string strMsgID)
        {
            XmlDocument xmlDoc = new XmlDocument();

            xmlDoc.Load(Common.webSiteRoot + @"\Xml\Message.xml");

            XmlNode nodeID = xmlDoc.SelectSingleNode("//Message[ID='" + strMsgID + "']");
            XmlNode nodeText = nodeID.SelectSingleNode("TEXT");

            return nodeText.InnerText.ToString();
        }

        /// <summary>
        /// メッセージを取得します。（変換値が複数の場合）
        /// </summary>
        /// <param name="strMsgID">該当するメッセージID</param>
        /// <param name="convStr">変換値格納配列</param>
        /// <returns>取得したメッセージ内容</returns>
        public static string GetMessage(string strMsgID, string[] convStr)
        {
            string msg = GetMessage(strMsgID);
            for (int i = 1; i <= convStr.Length; i++)
            {
                string oldValue = "[@conv" + i.ToString() + "]";
                msg = msg.Replace(oldValue, convStr[i - 1]);
            }

            return msg;
        }

        /// <summary>
        /// メッセージを取得します。（変換値が一つの場合）
        /// </summary>
        /// <param name="strMsgID">該当するメッセージID</param>
        /// <param name="convStr">変換値</param>
        /// <returns>取得したメッセージ内容</returns>
        public static string GetMessage(string strMsgID, string convStr)
        {
            return GetMessage(strMsgID, new string[] { convStr });
        }
        #endregion

        #region 有効文字列チェック
        /// <summary>
        /// 有効文字列かチェックします。
        /// </summary>
        /// <param name="val">判定したいデータ</param>
        /// <returns>true:判定OK / false:判定NG</returns>
        public static bool IsValidStr(string val)
        {
            if (val != null && val.Trim().Length > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

        //        #region 桁数チェック(バイト数)
        //        /// <summary>
        //        /// 桁数チェックをします。(バイト数)
        //        /// </summary>
        //        /// <param name="strText">判定したいデータ</param>
        //        /// <param name="maxLength">桁数(バイト数)</param>
        //        /// <returns>true:判定OK / false:判定NG</returns>
        //        public static bool CheckLength(string strText, int maxLength)
        //        {

        //            if (GetByteCount(strText) > maxLength)
        //            {
        //                return false;
        //            }
        //            return true;
        //        }
        //        #endregion

        #region 桁数チェック(文字数)
        /// <summary>
        /// 桁数チェックをします。(文字数)
        /// </summary>
        /// <param name="strText">判定したいデータ</param>
        /// <param name="maxLength">桁数(文字数)</param>
        /// <returns>true:判定OK / false:判定NG</returns>
        public static bool CheckLengthStrCount(string strText, int maxLength)
        {
            if (GetStrCount(strText) > maxLength)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region 数値チェック
        /// <summary>
        /// 数値チェックをします。
        /// </summary>
        /// <paramMap name="strText">判定したいデータ</paramMap>
        /// <returns>true:数値 / false:数値以外</returns>
        public static bool IsNumber(string strText)
        {
            double dblData;

            return double.TryParse(strText, System.Globalization.NumberStyles.Any, null, out dblData);
        }
        #endregion

        #region 日付チェック
        /// <summary>
        /// 日付チェックをします。
        /// </summary>
        /// <paramMap name="strText">判定したいデータ</paramMap>
        /// <returns>true:日付 / false:日付以外</returns>
        public static bool IsDate(string strText)
        {
            DateTime dblData;

            return DateTime.TryParse(strText, out dblData);
        }
        #endregion

        #region 整数チェック
        /// <summary>
        /// 整数チェックをします。
        /// </summary>
        /// <param name="strText">判定したいデータ</param>
        /// <returns>true:整数 / false:整数以外</returns>
        public static bool IsInteger(string strText)
        {
            int intData;

            return int.TryParse(strText, System.Globalization.NumberStyles.Any, null, out intData);
        }
        #endregion

        //        #region E-MAIL整合性チェック
        //        /// <summary>
        //        /// E-MAIL整合性チェックをします。
        //        /// </summary>
        //        /// <paramMap name="strText">判定したいデータ</paramMap>
        //        /// <returns>true:正常値 / false異常値</returns>
        //        public static bool IsCheckEMail(string strText)
        //        {
        //            string[] split;

        //            if (GetByteCount(strText) == 0)
        //            {
        //                return false;
        //            }
        //            if (isZenkaku(strText))
        //            {
        //                return false;
        //            }
        //            split = Split(strText, "@");
        //            if (split.Length != 2)
        //            {
        //                return false;
        //            }
        //            if (split[0].Length == 0 || split[1].Length == 0)
        //            {
        //                return false;
        //            }
        //            return true;
        //        }
        //        #endregion

        #region 全角を含んでいるかをチェック
        /// <summary>
        /// 全角を含んでいるかをチェックします。
        /// </summary>
        /// <param name="strText">判定したいデータ</param>
        /// <returns>true:含む / false:含まない</returns>
        public static bool isZenkaku(string strText)
        {
         
            if (strText.Length == 0)
            {
                return false;
            }
            else
            {
                if (GetByteCount(strText) != strText.Length)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        //        #region 半角を含んでいるかをチェック
        //        /// <summary>
        //        /// 半角を含んでいるかをチェックします。
        //        /// </summary>
        //        /// <param name="strText">判定したいデータ</param>
        //        /// <returns>true:含む / false:含まない</returns>
        //        public static bool isHankaku(string strText)
        //        {
        //            if (strText.Length == 0)
        //            {
        //                return false;
        //            }
        //            else
        //            {
        //                if (GetByteCount(strText) != strText.Length)
        //                {
        //                    return false;
        //                }
        //                else
        //                {
        //                    return true;
        //                }
        //            }
        //        }
        //        #endregion

        #region Phase2移行前のデータかチェック
        /// <summary>
        /// Phase2移行前の受入データかチェックします。
        /// true:Phase2以前 / false:Phase2以降
        /// </summary>
        /// <param name="keyword">判定したいデータ</param>
        /// <param name="type">検索条件</param>
        /// <returns>true:Phase2以前 / false:Phase2以降</returns>
        public static bool isAcceptedBeforePhase2(string keyword, StockSearchTarget type)
        {
            Hashtable _params = new Hashtable();
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("SELECT count(STOCK_ID) cnt");
            sql.AppendLine("FROM T_STOCK");
            sql.AppendLine("where");
            sql.AppendLine("REG_DATE <= '" + NikonConst.Phase2StartDate + "'");
            switch (type)
            {
                case StockSearchTarget.stockId:
                    sql.AppendLine("and STOCK_ID = :STOCK_ID");
                    _params.Add("STOCK_ID", keyword);
                    break;
                case StockSearchTarget.barcode:
                    sql.AppendLine("and BARCODE = :BARCODE");
                    _params.Add("BARCODE", keyword);
                    break;
            }

            DataTable ret = DbUtil.Select(sql.ToString(), _params);
            if (Convert.ToInt16(ret.Rows[0]["cnt"]) > 0) return true;
            return false;
        }
        #endregion

        #region 指定文字のバイト数取得
        /// <summary>
        /// 指定文字のバイト数を取得します。
        /// </summary>
        /// <param name="strText">判定したいデータ</param>
        /// <returns>バイト数</returns>
        public static int GetByteCount(string strText)
        {
            return sjisEnc.GetByteCount(strText);
        }
        #endregion

        #region 指定文字の文字数取得
        /// <summary>
        /// 指定文字数のバイト数を取得します。
        /// </summary>
        /// <param name="strText">判定したいデータ</param>
        /// <returns>文字数</returns>
        public static int GetStrCount(string strText)
        {
            if (strText == null)
            {
                return 0;
            }
            else
            {
                return strText.Length;
            }
        }
        #endregion

        #region メッセージボックス出力
        /// <summary>
        /// メッセージボックス出力をします。
        /// </summary>
        /// <param name="page">ページオブジェクト</param>
        /// <param name="strMsg">メッセージ</param>
        /// <param name="strControlId">[0] ControlId [1] 0:コントロールを空にしない 1:コントロールを空にする</param>
        public static void MsgBox(Page page, string strMsg, params string[] strControlId)
        {
            StringBuilder sbScript = new StringBuilder();

            if (strMsg != null)
            {
                sbScript.Append("<script>");
                sbScript.Append("alert('");
                sbScript.Append(strMsg);
                sbScript.Append("');");

                for (int i = 0; i < strControlId.Length; i++)
                {
                    switch (i)
                    {
                        case 0:
                            sbScript.Append("document.getElementById('" + strControlId[0] + "').focus();");
                            break;
                        case 1:
                            if (strControlId[1] == "1")
                            {
                                //スクリプトで空にする場合、同じ値を連続して入れるとテキストチェンジイベントが正しく動作しなくなるため変更
                                //テキストコントロール以外がこの場所を通る場合はTypeを取得して変更してください。
                                //sbScript.Append("document.getElementById('" + strControlId[0] + "').value = '';");
                                if (page.FindControl(strControlId[0]) != null)
                                {
                                    ((TextBox)page.FindControl(strControlId[0])).Text = "";

                                }
                                else
                                {
                                    //ユーザーコントロールの対応
                                    //ユーザーコントロールはFindControlのFindControlをする必要があるため、対応
                                    string[] strConNm = strControlId[0].Split('_');
                                    Control ctl = new Control();
                                    ctl = page;
                                    foreach (string str in strConNm)
                                    {
                                        ctl = ctl.FindControl(str);
                                    }

                                    if (ctl != null)
                                    {
                                        ((TextBox)ctl).Text = "";
                                    }
                                }
                            }
                            break;
                    }
                }
                sbScript.Append("</script>");
            }
            else
            {
                sbScript.Append("");
            }

            string dt = DateTime.Now.ToString("s.ffff");
            ScriptManager.RegisterStartupScript(page, typeof(Page), "ScriptTransfer" + dt, sbScript.ToString(), false);
        }
        #endregion


        //        #region 文字列を指定文字列で分割
        //        /// <summary>
        //        /// 文字列を指定文字列で分割します。
        //        /// </summary>
        //        /// <param name="input">文字列</param>
        //        /// <param name="pattern">指定文字列</param>
        //        /// <returns>分割後の文字列配列</returns>
        //        public static string[] Split(string input, string pattern)
        //        {
        //            return System.Text.RegularExpressions.Regex.Split(input, pattern);
        //        }
        //        #endregion

        //        #region 画面を閉じる
        //        /// <summary>
        //        /// 画面を閉じます。
        //        /// </summary>
        //        /// <param name="page">ページオブジェクト</param>
        //        public static void WindowClose(Page page)
        //        {
        //            StringBuilder sbScript = new StringBuilder();
        //            sbScript.Append("<script>window.close();</script>");
        //            page.ClientScript.RegisterStartupScript(typeof(Page), "WindowClose", sbScript.ToString());
        //        }
        //        #endregion

        //        #region 「なし」「あり」を格納したDataTableを生成
        //        /// <summary>
        //        /// 「なし」「あり」を格納したDataTableを生成します。
        //        /// </summary>
        //        /// <param name="ID">列名 ID</param>
        //        /// <param name="NAME">列名 名前</param>
        //        /// <param name="InsFarstSpaceRow">先頭行に空白を入れるかを確認するフラグ</param>
        //        /// <returns>生成したDataTable</returns>
        //        public static DataTable GetDataTableSelect(string strID, string strNAME, bool InsFarstSpaceRow)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            dt.Columns.Add(strID);
        //            dt.Columns.Add(strNAME);

        //            dr = dt.NewRow();
        //            dr[strID] = 0;
        //            dr[strNAME] = "なし";
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[strID] = 1;
        //            dr[strNAME] = "あり";
        //            dt.Rows.Add(dr);

        //            return dt;
        //        }
        //        #endregion

        //        #region 構造式検索タイプを選択するDropDownList用のDataTableを生成
        //        /// <summary>
        //        /// 構造式検索タイプを選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text">Text列</param>
        //        /// <param name="Value">Value列</param>
        //        /// <returns>生成したDataTable</returns>
        //        public static DataTable GetStructureSearchTypes(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);

        //            dr = dt.NewRow();
        //            dr[Text] = "Substructure";
        //            dr[Value] = Const.cSTRUCTURE_SUBSTRUCTURE;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "Full Structure";
        //            dr[Value] = Const.cSTRUCTURE_FULL;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "Exact Structure";
        //            dr[Value] = Const.cSTRUCTURE_EXACT;
        //            dt.Rows.Add(dr);

        //            return dt;
        //        }
        //        #endregion

        //        #region 文字列の検索タイプを選択するDropDownList用のDataTableを生成
        //        /// <summary>
        //        /// 文字列の検索タイプを選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text">Text列</param>
        //        /// <param name="Value">Value列</param>
        //        /// <returns>生成したDataTable</returns>
        //        public static DataTable GetStringSearchTypes(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);

        //            dr = dt.NewRow();
        //            dr[Text] = "部分一致";
        //            dr[Value] = Const.cSEARCH_TYPE_CONTAINS;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "前方一致";
        //            dr[Value] = Const.cSEARCH_TYPE_STARTSWITH;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "後方一致";
        //            dr[Value] = Const.cSEARCH_TYPE_ENDSWITH;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "完全一致";
        //            dr[Value] = Const.cSEARCH_TYPE_EQUALTO;
        //            dt.Rows.Add(dr);

        //            return dt;
        //        }
        //        #endregion

        //        #region 鍵・重量管理方法を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 鍵・重量管理方法を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text">Text列</param>
        //        /// <param name="Value">Value列</param>
        //        /// <returns>生成したDataTable</returns>
        //        public static DataTable GetManagementTypes(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);

        //            dr = dt.NewRow();
        //            dr[Text] = "必須";
        //            dr[Value] = Const.cMANAGEMENT_TYPE_REQUIRED;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "推奨";
        //            dr[Value] = Const.cMANAGEMENT_TYPE_RECOMMENDATION;
        //            dt.Rows.Add(dr);

        //            return dt;
        //        }
        //        #endregion

        //        #region 形状を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 形状を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text">Text列</param>
        //        /// <param name="Value">Value列</param>
        //        /// <returns>生成したDataTable</returns>
        //        public static DataTable GetFigureTypes(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);

        //            dr = dt.NewRow();
        //            dr[Text] = "固体";
        //            dr[Value] = Const.cFIGURE_TYPE_SOLID;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "粉体";
        //            dr[Value] = Const.cFIGURE_TYPE_POWDER;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "液体";
        //            dr[Value] = Const.cFIGURE_TYPE_LIQUID;
        //            dt.Rows.Add(dr);
        //            dr = dt.NewRow();
        //            dr[Text] = "気体";
        //            dr[Value] = Const.cFIGURE_TYPE_GAS;
        //            dt.Rows.Add(dr);
        //            return dt;
        //        }
        //        #endregion

        //        #region 成分を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 形状を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text">Text列</param>
        //        /// <param name="Value">Value列</param>
        //        /// <returns>生成したDataTable</returns>
        //        public static DataTable GetComponentTypes(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);

        //            dr = dt.NewRow();
        //            dr[Text] = "o-キシレン";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "m-キシレン";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "p-キシレン";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "エチルベンゼン";
        //            dr[Value] = 4;
        //            dt.Rows.Add(dr);

        //            return dt;
        //        }
        //        #endregion

        //        #region 成分を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 形状を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text">Text列</param>
        //        /// <param name="Value">Value列</param>
        //        /// <returns>生成したDataTable</returns>
        //        public static DataTable GetMakerTypes(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);

        //            dr = dt.NewRow();
        //            dr[Text] = "和光純薬工業";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "キシダ化学";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "Biosense Laboratories";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "DAS";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "薬研究";
        //            dr[Value] = 4;
        //            dt.Rows.Add(dr);

        //            return dt;
        //        }
        //        #endregion

        //        #region 調達元を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 調達元を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetProcurement(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "BEAMS購入";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "直接購入";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "サンプル入手";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "内製";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region　荷姿を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 荷姿を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetPacking(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "瓶";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "缶";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "袋";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "ボンベ";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "その他";
        //            dr[Value] = 4;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region　使用頻度を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 使用頻度を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetUsage_Frequency(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "毎日";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "週に1回以上";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "月に1回以上";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "半年に1回以上";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "半年に1回以上";
        //            dr[Value] = 4;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region　使用場所の業務を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 使用場所の業務を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetOperation(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "製造・生産";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "実験・開発";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "事務";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "清掃";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "販売";
        //            dr[Value] = 4;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "その他";
        //            dr[Value] = 5;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 作業環境測定を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 作業環境測定を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetEnv_Meas(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "非該当";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "類似の物質で測定実績有り";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "新規に要請する";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 小分け容器の使用有無を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 小分け容器の使用有無を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetSub_Container(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "使用する";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "使用しない";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 該当物質の使用容器を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 該当物質の使用容器を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetUsage_Container(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "そのまま使用";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "装置、タンク等へ注入";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "別容器へ";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 漏洩対策を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 漏洩対策を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetDisclosure(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "対策済み";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "対策予定（チェック＋日付記入）";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "対策実施予定無し";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "未対応";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 緊急対応を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 緊急対応を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetUrgency_Fix(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "対策済み";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "対策予定（チェック＋日付記入）";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "対策実施予定無し";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "未対応";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 廃棄方針を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 廃棄方針を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetDisposal_Rule(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "職場処理（中和・除外等）";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "廃棄委託";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 廃棄方法を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 廃棄方法を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetDisposal_Method(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "中和処理";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "焼却";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "除外処理";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "廃液保管";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "その他";
        //            dr[Value] = 4;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "記載無し";
        //            dr[Value] = 5;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 廃棄委託時の一時保管場所を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 廃棄委託時の一時保管場所を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetTemporary_Location(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "一時保管場所あり";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "一時保管場所無し";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "保管庫利用";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 作業管理を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 作業管理を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetWorking_Mgmt(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "面体、ゴーグル";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "防塵マスク";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "防毒マスク";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "防護手袋";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "防護服";
        //            dr[Value] = 4;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "無し";
        //            dr[Value] = 5;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "その他";
        //            dr[Value] = 6;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 作業環境管理を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 作業環境管理を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetEnv_Mgmt(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "密閉装置";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "局所排気装置";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "外付け式";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "クリーンベンチ";
        //            dr[Value] = 3;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "全体換気";
        //            dr[Value] = 4;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "無し";
        //            dr[Value] = 5;
        //            dt.Rows.Add(dr);

        //            return dt;

        //        }
        //        #endregion

        //        #region 安全教育を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 安全教育を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetSafety_Educational(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "有";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "無";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            return dt;
        //        }
        //        #endregion

        //        #region 使用量を選択するDropDownList用のDataTableを作成
        //        /// <summary>
        //        /// 使用量を選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text"></param>
        //        /// <param name="Value"></param>
        //        /// <returns></returns>
        //        public static DataTable GetUsage(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;

        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);
        //            dr = dt.NewRow();
        //            dr[Text] = "月に10L(1kg)を超える";
        //            dr[Value] = 0;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "月に10L～0.1L(1kg～0.1kg)";
        //            dr[Value] = 1;
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "月に0.1L(0.1kg)未満";
        //            dr[Value] = 2;
        //            dt.Rows.Add(dr);

        //            return dt;
        //        }
        //        #endregion

        //        #region ボタン押下後のページロードであれば、押されたボタン名を返却
        //        /// <summary>
        //        /// ボタン押下後のページロードであれば、押されたボタン名を返します。
        //        /// ボタン押下後でない場合は、空を返します。
        //        /// </summary>
        //        /// <remarks>
        //        /// 必ずページに存在するボタン名は統一の文字列から始まること。
        //        /// ・「btn**」で始まる等
        //        /// また、統一の文字列は、他のコントロールに含まれていないこと。
        //        /// ・ボタンだけでなく、コンボボックスも「btn**」で始まるなど。
        //        /// </remarks>
        //        /// <param name="page">ページオブジェクト</param>
        //        /// <param name="btnStartName">ボタン名を現す始まりの文字列(「btn」など)</param>
        //        /// <returns>ボタン押下時はボタン名。なければ空。</returns>
        //        public static string IsEnterButtonName_PageLoad(Page page, string btnStartName)
        //        {
        //            string ret = "";
        //            int len = GetByteCount(btnStartName);

        //            foreach (string key in page.Request.Form.AllKeys)
        //            {
        //                if (key != null)
        //                {
        //                    string val = key.Replace("ucHeader$", "").Replace("ucFooter$", "").Replace("ucUserSel$", "");
        //                    if (GetByteCount(val) >= len &&
        //                        val.Substring(0, len) == btnStartName)
        //                    {
        //                        ret = val;
        //                        break;
        //                    }
        //                }
        //            }
        //            return ret;
        //        }
        //        #endregion

        //        #region Pageオベジェクトから指定したユーザーコントロールに指定したボタン名が含まれているかを確認
        //        /// <summary>
        //        /// Pageオベジェクトから指定したユーザーコントロールに指定したボタン名が含まれているかを確認します。
        //        /// </summary>
        //        /// <param name="page">ページオブジェクト</param>
        //        /// <param name="UserContorolName">指定したユーザーコントロール名</param>
        //        /// <param name="btnName">指定ボタン名</param>
        //        /// <returns>true:存在する / false:存在しない</returns>
        //        public static bool IsExistUserControlBtnName_InPage(Page page, string UserContorolName, string btnName)
        //        {
        //            bool ret = false;
        //            string controlname = UserContorolName + "$" + btnName;

        //            foreach (string key in page.Request.Form.AllKeys)
        //            {
        //                if (key == controlname)
        //                {
        //                    ret = true;
        //                    break;
        //                }
        //            }
        //            return ret;
        //        }
        //        #endregion

        //        #region Pageオベジェクトから指定したGridユーザーコントロール名が含まれている行Indexを取得
        //        /// <summary>
        //        /// Pageオベジェクトから指定したGridユーザーコントロール名が含まれている行Indexを取得します。
        //        /// </summary>
        //        /// <param name="page">ページオブジェクト</param>
        //        /// <param name="UserContorolName">指定したユーザーコントロール名</param>
        //        /// <returns>取得した行Index番号（1始まり）。存在しなければ1を返します。0を</returns>
        //        public static int GetGridControlRowIndex_InPage(Page page, string UserContorolName)
        //        {
        //            int ret = 0;

        //            foreach (string key in page.Request.Form.AllKeys)
        //            {
        //                string[] splt = key.Split('$');
        //                // 0：Grid名
        //                // 1：ctl05（などのctl + 行index）
        //                // 2：コントロール名
        //                // (例)grid$ctl06$btn1Sel
        //                if (splt.Length == 3 && splt[2] == UserContorolName)
        //                {
        //                    ret = Convert.ToInt32(splt[1].Replace("ctl", ""));
        //                    break;
        //                }
        //            }
        //            return ret;
        //        }
        //        #endregion

        //        #region チェックしたい文字列がnullまたは空文字であれば、「NULL」文字列を返却（Sql用）
        //        /// <summary>
        //        /// チェックしたい文字列がnullまたは空文字であれば、「NULL」文字列を返却します。（Sql用）
        //        /// </summary>
        //        /// <param name="value">チェックしたい文字列</param>
        //        /// <param name="comma">空文字でない場合、文字列にカンマをつけて返却するか判断</param>
        //        /// <returns>チェック後の文字列</returns>
        //        public static string IsEmptyForSql(string value, bool comma)
        //        {
        //            if (value == null || value == "")
        //            {
        //                return "null";
        //            }
        //            else
        //            {
        //                if (!comma)
        //                {
        //                    return value;
        //                }
        //                else
        //                {
        //                    return "'" + value + "'";
        //                }
        //            }
        //        }
        //        #endregion

        //        #region 指定文字列がHTML文字列であれば、プログラム用に変換 (+1)
        /// <summary>
        /// 指定文字列がHTML文字列であれば、プログラム用に変換します。
        /// </summary>
        /// <param name="value">指定文字列</param>
        /// <param name="nbspNull">
        /// 指定文字列が&nbspの場合、半角スペースか空文字列かを指定します。指定なしは半角スペース。
        /// true:空文字列 / false:半角文字列
        /// </param>
        /// <returns>変換文字列。HML文字列でなければそのまま返却します。</returns>
        public static string ConvertHtmlVal(string value, bool nbspEmpty)
        {
            if (value == null)
            {
                return null;
            }

            value = value.Replace("&quot;", "\"");
            value = value.Replace("&QUOT;", "\"");
            value = value.Replace("&amp;", "&");
            value = value.Replace("&AMP;", "&");
            value = value.Replace("&lt;", "<");
            value = value.Replace("&LT;", "<");
            value = value.Replace("&gt;", ">");
            value = value.Replace("&GT;", ">");
            if (nbspEmpty)
            {
                value = value.Replace("&nbsp;", "");
                value = value.Replace("&NBSP;", "");
            }
            else
            {
                value = value.Replace("&nbsp;", " ");
                value = value.Replace("&NBSP;", " ");
            }
            value = value.Replace("&copy;", "©");
            value = value.Replace("&COPY;", "©");

            return value;

        }

        //        /// <summary>
        //        /// 指定文字列がHTML文字列であれば、プログラム用に変換
        //        /// </summary>
        //        /// <param name="value">指定文字列</param>
        //        /// <returns>変換文字列。HML文字列でなければそのまま返却します。</returns>
        //        public static string ConvertHtmlVal(string value)
        //        {
        //            return ConvertHtmlVal(value, false);
        //        }
        //        #endregion

        //        #region 値がtrueなら1、falseなら0を返却
        //        /// <summary>
        //        /// 値がtrueなら1、falseなら0を返却します。
        //        /// </summary>
        //        /// <param name="check">チェック</param>
        //        /// <returns>結果</returns>
        //        public static int ConvertCheckedToNum(bool check)
        //        {
        //            if (check)
        //            {
        //                return 1;
        //            }
        //            else
        //            {
        //                return 0;
        //            }
        //        }
        //        #endregion

        //        #region 値が「あり」なら1、「なし」なら0を返却
        //        /// <summary>
        //        /// 値が「あり」なら1、「なし」なら0を返却します。以外は-1。
        //        /// </summary>
        //        /// <param name="val">チェック</param>
        //        /// <returns>結果</returns>
        //        public static int ConvertAriNashiToNum(string val)
        //        {
        //            switch (val)
        //            {
        //                case "あり":
        //                    return 1;
        //                case "なし":
        //                    return 0;
        //                default:
        //                    return -1;
        //            }
        //        }
        //        #endregion

        //        #region 権限に値する数値を返却
        //        /// <summary>
        //        /// 権限に値する数値を返却します。存在しなければ-1。
        //        /// </summary>
        //        /// <param name="val">チェック</param>
        //        /// <returns>結果</returns>
        //        public static int ConvertAdminToNum(string val)
        //        {
        //            switch (val)
        //            {
        //                case "一般":
        //                    return 0;
        //                case "管理者":
        //                    return 1;
        //                case "受入":
        //                    return 2;
        //                default:
        //                    return -1;
        //            }
        //        }
        //        #endregion

        //        #region 数値が1ならtrue、1以外ならfalseを返却
        //        /// <summary>
        //        /// 数値が1ならtrue、1以外ならfalseを返却します。
        //        /// </summary>
        //        /// <param name="num">数値</param>
        //        /// <returns>true:1 / false:1以外</returns>
        //        public static bool ConvertNumToChecked(object num)
        //        {
        //            if (!IsNumber(num.ToString())) return false;
        //            if (Convert.ToInt32(num) == 1)
        //            {
        //                return true;
        //            }
        //            else
        //            {
        //                return false;
        //            }
        //        }
        //        #endregion

        //        #region 数値が1ならあり、1以外ならなしを返却
        //        /// <summary>
        //        /// 数値が1ならあり、1以外ならなしを返却します。
        //        /// </summary>
        //        /// <param name="num">数値</param>
        //        /// <returns>結果</returns>
        //        public static string ConvertNumToAriNashi(object num)
        //        {
        //            if (!IsNumber(num.ToString())) return "";
        //            if (Convert.ToInt32(num) == 1)
        //            {
        //                return "あり";
        //            }
        //            else
        //            {
        //                return "なし";
        //            }
        //        }
        //        #endregion

        //        #region 数値により、該当する権限（一般、管理者）を返却
        //        /// <summary>
        //        /// 数値により、該当する権限（一般、管理者）を返却します。
        //        /// </summary>
        //        /// <param name="num">数値</param>
        //        /// <returns>結果</returns>
        //        public static string ConvertNumToAdmin(object num)
        //        {
        //            if (!IsNumber(num.ToString())) return "";
        //            switch (Convert.ToInt32(num))
        //            {
        //                case 0:
        //                    return "一般";
        //                case 1:
        //                    return "管理者";
        //                case 2:
        //                    return "受入";
        //                default:
        //                    return "";
        //            }
        //        }
        //        #endregion

        //        #region 値が0か1かで、チェックボックスを設定
        //        /// <summary>
        //        /// 値が0か1かで、チェックボックスを設定します。
        //        /// </summary>
        //        /// <param name="chk1">値が1の場合にTrueとするコントロール</param>
        //        /// <param name="chk2">値が1の場合にfalseとするコントロール</param>
        //        /// <param name="value">値</param>
        //        public static void SetRadioBtnFromZeroOne(CheckBox chk1, CheckBox chk2, object value)
        //        {
        //            byte byteVal = 0;
        //            if (value != null && value != DBNull.Value && Common.IsNumber(value.ToString()))
        //            {
        //                byteVal = Convert.ToByte(value);
        //            }
        //            bool check = ConvertNumToChecked(byteVal);
        //            chk1.Checked = check;
        //            chk2.Checked = !check;
        //        }
        //        #endregion

        //        #region 値が0か1か2かで、チェックボックスを設定
        //        /// <summary>
        //        /// 値が0か1か2かで、チェックボックスを設定します。
        //        /// </summary>
        //        /// <param name="chk1">値が0の場合にTrueとするコントロール</param>
        //        /// <param name="chk2">値が1の場合にTrueとするコントロール</param>
        //        /// <param name="chk3">値が2の場合にTrueとするコントロール</param>
        //        /// <param name="value">値</param>
        //        public static void SetRadioBtnFromZeroTwo(CheckBox chk1, CheckBox chk2, CheckBox chk3, object value)
        //        {
        //            byte byteVal = 0;
        //            if (value != null && value != DBNull.Value && Common.IsNumber(value.ToString()))
        //            {
        //                byteVal = Convert.ToByte(value);
        //            }

        //            switch (byteVal)
        //            {
        //                case 0:
        //                    chk1.Checked = true;
        //                    chk2.Checked = false;
        //                    chk3.Checked = false;
        //                    break;
        //                case 1:
        //                    chk1.Checked = false;
        //                    chk2.Checked = true;
        //                    chk3.Checked = false;
        //                    break;
        //                case 2:
        //                    chk1.Checked = false;
        //                    chk2.Checked = false;
        //                    chk3.Checked = true;
        //                    break;
        //            }
        //        }
        //        #endregion

        //        #region JavaScriptメソッド名や引数を作成 (+1)
        //        /// <summary>
        //        /// JavaScriptメソッド名や引数を作成します。
        //        /// </summary>
        //        /// <param name="scriptName">スクリプト名</param>
        //        /// <param name="param">引数格納配列</param>
        //        /// <returns>作成文字列（例）ShowModal('ZC0100', '100', '200');</returns>
        //        public static string CreateJavaScript(string scriptName, object[] param)
        //        {
        //            if (param == null || param.Length == 0)
        //            {
        //                return scriptName + "();";
        //            }
        //            else
        //            {
        //                StringBuilder sb = new StringBuilder();
        //                for (int i = 0; i < param.Length; i++)
        //                {
        //                    if (sb.Length > 0)
        //                    {
        //                        sb.Append(",");
        //                    }
        //                    if (param[i] == null)
        //                    {
        //                        sb.Append("null");
        //                    }
        //                    else
        //                    {
        //                        switch (Type.GetTypeCode(param[i].GetType()))
        //                        {
        //                            case TypeCode.String:
        //                                sb.Append("'" + param[i].ToString() + "'");
        //                                break;
        //                            default:
        //                                sb.Append(param[i].ToString());
        //                                break;
        //                        }
        //                    }
        //                }
        //                return scriptName + "(" + sb.ToString() + ");";
        //            }
        //        }

        //        /// <summary>
        //        /// JavaScriptメソッド名や引数を作成します。
        //        /// </summary>
        //        /// <param name="scriptName">スクリプト名</param>
        //        /// <param name="param">引数（1つの場合）</param>
        //        /// <returns>作成文字列（例）ShowModal('ZC0100', '100', '200');</returns>
        //        public static string CreateJavaScript(string scriptName, object param)
        //        {
        //            return CreateJavaScript(scriptName, new object[] { param });
        //        }

        //        #endregion

        //        #region 指定した精度の数値に切り上げ
        //        /// <summary>
        //        ///     指定した精度の数値に切り上げします</summary>
        //        /// <param name="dValue">
        //        ///     丸め対象の倍精度浮動小数点数</param>
        //        /// <param name="iDigits">
        //        ///     戻り値の有効桁数の精度</param>
        //        /// <returns>
        //        ///     iDigits に等しい精度の数値に切り上げられた数値</returns>
        //        public static double ToRoundUp(double dValue, int iDigits)
        //        {
        //            double dCoef = System.Math.Pow(10, iDigits);

        //            return dValue > 0 ? System.Math.Ceiling(dValue * dCoef) / dCoef :
        //                                System.Math.Floor(dValue * dCoef) / dCoef;
        //        }
        //        #endregion

        //        #region 指定した精度の数値に切り捨て
        //        /// <summary>
        //        ///     指定した精度の数値に切り捨てします</summary>
        //        /// <param name="dValue">
        //        ///     丸め対象の倍精度浮動小数点数</param>
        //        /// <param name="iDigits">
        //        ///     戻り値の有効桁数の精度</param>
        //        /// <returns>
        //        ///     iDigits に等しい精度の数値に切り捨てられた数値</returns>
        //        public static double ToRoundDown(double dValue, int iDigits)
        //        {
        //            double dCoef = System.Math.Pow(10, iDigits);

        //            return dValue > 0 ? System.Math.Floor(dValue * dCoef) / dCoef :
        //                                System.Math.Ceiling(dValue * dCoef) / dCoef;
        //        }
        //        #endregion

        //        #region 指定した精度の数値に四捨五入
        //        /// <summary>
        //        ///     指定した精度の数値に四捨五入します</summary>
        //        /// <param name="dValue">
        //        ///     丸め対象の倍精度浮動小数点数</param>
        //        /// <param name="iDigits">
        //        ///     戻り値の有効桁数の精度</param>
        //        /// <returns>
        //        ///     iDigits に等しい精度の数値に四捨五入された数値</returns>
        //        public static double ToHalfAdjust(double dValue, int iDigits)
        //        {
        //            double dCoef = System.Math.Pow(10, iDigits);

        //            return dValue > 0 ? System.Math.Floor((dValue * dCoef) + 0.5) / dCoef :
        //                                System.Math.Ceiling((dValue * dCoef) - 0.5) / dCoef;
        //        }
        //        #endregion

        #region 小数桁入力値チェック
        /// <summary>
        /// 小数桁入力値チェックをします。
        /// </summary>
        /// <param name="strCheckValue">
        ///     入力値</param>
        /// <param name="intDigits">
        ///     精度</param>
        /// <param name="dbCheckValue">
        ///     変換後の値</param>
        /// <returns>true:判定OK / false:判定NG</returns>
        public static bool CheckDecimalPointValue(string strCheckValue, int intDigits, out double dbCheckValue)
        {
            double dblTemp;
            bool blnRcd = false;

            dbCheckValue = Convert.ToDouble(strCheckValue);

            // 指定した精度を四捨五入する
            dblTemp = Math.Round(dbCheckValue, intDigits, MidpointRounding.AwayFromZero);

            // 四捨五入後の数値が変換前と同値かチェックする
            if (dblTemp == dbCheckValue)
            {
                blnRcd = true;
            }
            else
            {
                dbCheckValue = dblTemp;
            }
            return blnRcd;
        }
        #endregion

        #region 指定した数値が正数かチェック
        /// <summary>
        /// 指定した数値が正数かチェックします。
        /// </summary>
        /// <paramMap name="strText">
        ///     判定したいデータ</paramMap>
        /// <returns>true:正数 / false:正数以外</returns>
        public static bool IsPositiveNumber(string strText)
        {
            double dbData;

            // 数値の場合
            if (double.TryParse(strText, System.Globalization.NumberStyles.Any, null, out dbData))
            {
                // 正数の場合
                if (System.Math.Sign(dbData) == 1)
                {
                    return true;
                }
            }
            return false;
        }
        #endregion

        //        #region DataTableからcsvファイルを作成しサーバーに保存
        //        /// <summary>
        //        /// DataTableからcsvファイルを作成しサーバーに保存します。
        //        /// </summary>
        //        /// <param name="dt">データ</param>
        //        /// <param name="expFileName">ファイルパス</param>
        //        public static void ExportCsvFile(DataTable dt, string expFileName)
        //        {
        //            string record = "";

        //            using (StreamWriter sr = new StreamWriter(expFileName, false, Encoding.GetEncoding("Shift_JIS")))
        //            {
        //                // ヘッダー部
        //                foreach (DataColumn col in dt.Columns)
        //                {
        //                    record += "\"" + col.ColumnName + "\",";
        //                }
        //                record = record.Remove(record.Length - 1, 1);   // 末尾の,を削除する
        //                sr.WriteLine(record);

        //                // データ部
        //                foreach (DataRow row in dt.Rows)
        //                {
        //                    record = "";
        //                    for (int i = 0; i < dt.Columns.Count; i++)
        //                    {
        //                        //ダブルクォートで囲む
        //                        record += "\"" + Convert.ToString(row[i]).Replace("\"", "\"\"") + "\",";
        //                    }
        //                    record = record.Remove(record.Length - 1, 1);   // 末尾の,を削除する
        //                    sr.WriteLine(record);
        //                }
        //            }
        //        }
        //        #endregion

        //        #region DataTableからcsvファイルを作成しダイレクトにダウンロード
        //        /// <summary>
        //        /// DataTableからcsvファイルを作成しダイレクトにダウンロードします。
        //        /// </summary>
        //        /// <param name="_Response">プロパティ</param>
        //        /// <param name="dt">データ</param>
        //        /// <param name="expFileName">ファイルパス</param>
        //        public static void DownloadCsvFile(HttpResponse _Response, DataTable dt, string fileName)
        //        {
        //            DownloadCsvFile(_Response, dt, false, fileName);
        //        }

        //        /// <summary>
        //        /// DataTableからcsvファイルを作成しダイレクトにダウンロードします。
        //        /// </summary>
        //        /// <param name="_Response">プロパティ</param>
        //        /// <param name="dt">データ</param>
        //        /// <param name="isCaptionOutput">表示名を出力するか。</param>
        //        /// <param name="expFileName">ファイルパス</param>
        //        /// <param name="searchCondition">検索条件</param>
        //        public static void DownloadCsvFile(HttpResponse _Response, DataTable dt, bool isCaptionOutput, string fileName, string searchCondition = null)
        //        {
        //            string record = "";
        //            string replaceFileName = fileName.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);

        //            _Response.ContentEncoding = Encoding.GetEncoding("Shift_JIS");
        //            _Response.Charset = "shift_jis";
        //            _Response.ContentType = "application/octet-stream";
        //            _Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(string.Format(replaceFileName, DateTime.Now.ToString("yyyyMMddHHmmss"))));
        //            _Response.AddHeader("X-Download-Options", "noopen");

        //            if (!string.IsNullOrEmpty(searchCondition)) record = searchCondition;

        //            // ヘッダー部
        //            foreach (DataColumn col in dt.Columns)
        //            {
        //                if (isCaptionOutput)
        //                {
        //                    record += "\"" + col.Caption + "\",";
        //                }
        //                else     
        //           {
        //                    record += "\"" + col.ColumnName + "\",";
        //                }
        //            }
        //            record = record.Remove(record.Length - 1, 1) + "\r\n";   // 末尾の,を削除する
        //            _Response.BinaryWrite(Encoding.GetEncoding("Shift-JIS").GetBytes(record));

        //            // データ部
        //            foreach (DataRow row in dt.Rows)
        //            {
        //                record = "";
        //                for (int i = 0; i < dt.Columns.Count; i++)
        //                {
        //                    //ダブルクォートで囲む
        //                    record += "\"" + Convert.ToString(row[i]).Replace("\"", "\"\"") + "\",";
        //                }
        //                record = record.Remove(record.Length - 1, 1) + "\r\n";   // 末尾の,を削除する
        //                _Response.BinaryWrite(Encoding.GetEncoding("Shift-JIS").GetBytes(record));
        //            }
        //            _Response.Flush();
        //            HttpContext.Current.ApplicationInstance.CompleteRequest();
        //            _Response.Close();
        //        }

        //        /// <summary>
        //        /// DataTableからcsvファイルを作成しメールで送信します。
        //        /// </summary>
        //        /// <param name="To">送信先のメールアドレス</param>
        //        /// <param name="dt">データ</param>
        //        /// <param name="FileName">ファイル名</param>
        //        public static void SendMailCsvFile(string To, DataTable dt, string FileName)
        //        {
        //            var filePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
        //            string record = string.Empty;

        //            using (var sw = new StreamWriter(filePath, true, Encoding.GetEncoding("Shift-JIS")))
        //            {
        //                foreach(DataColumn column in dt.Columns)
        //                {
        //                    record += "\"" + column.ColumnName + "\",";
        //                }
        //                record = record.Remove(record.Length - 1, 1) + "\r\n";
        //                sw.Write(record);
        //                foreach(DataRow row in dt.Rows)
        //                {
        //                    record = string.Empty;
        //                    for(var i = 0; i < dt.Columns.Count; i++)
        //                    {
        //                        record += "\"" + Convert.ToString(row[i]).Replace("\"", "\"\"") + "\",";
        //                    }
        //                    record = record.Remove(record.Length - 1, 1) + "\r\n";
        //                    sw.Write(record);
        //                }
        //                sw.Close();
        //            }

        //            using(var mailMessage = new MailMessage())
        //            {
        //                mailMessage.To.Add(To);
        //                mailMessage.Subject = FileName.Replace(".csv", "");
        //                mailMessage.Body = "ZyCoaGから送信されました。";
        //                var attachFile = new Attachment(filePath, FileName);
        //                mailMessage.Attachments.Add(attachFile);
        //                using (var smtpClient = new SmtpClient())
        //                {
        //                    smtpClient.Send(mailMessage);
        //                }
        //            }
        //        }
        //        #endregion

        //        #region サーバーにファイルをアップロード
        //        /// <summary>
        //        /// サーバーにファイルをアップロードします。
        //        /// </summary>
        //        /// <param name="postedFile">Inputコントロールから取得したファイル</param>
        //        /// <param name="uploadDir">アップロード先のディレクトリ</param>
        //        /// <returns>
        //        /// アップロードしたファイルのフルパス（サーバーサイド）
        //        /// </returns>
        //        public static string UploadFile(HttpPostedFile postedFile, string uploadDir)
        //        {
        //            string serverFileName = "";

        //            // uploadするフォルダが無ければ作成する
        //            if (!Directory.Exists(uploadDir))
        //            {
        //                Directory.CreateDirectory(uploadDir);
        //            }

        //            // クライアントサイドのファイル名をチェック
        //            if (postedFile.FileName != "")
        //            {
        //                // サーバーサイドにファイルを保存する
        //                postedFile.SaveAs(uploadDir + @"\" + Path.GetFileName(postedFile.FileName));

        //                if (File.Exists(uploadDir + @"\" + Path.GetFileName(postedFile.FileName)))
        //                {
        //                    serverFileName = uploadDir + @"\" + Path.GetFileName(postedFile.FileName);
        //                }
        //            }

        //            return serverFileName;
        //        }
        //        #endregion

        //        #region コレクションを比較
        //        /// <summary>
        //        /// IEnumerableを実装しているクラスを比較します。
        //        /// </summary>
        //        /// <param name="enumerable1">比較対象１</param>
        //        /// <param name="enumerable2">比較対象２</param>
        //        /// <returns>true:判定OK / false:判定NG</returns>
        //        public static bool CollectionComp(IEnumerable enumerable1, IEnumerable enumerable2)
        //        {
        //            IEnumerator e1 = enumerable1.GetEnumerator();
        //            IEnumerator e2 = enumerable2.GetEnumerator();
        //            while (e1.MoveNext())
        //            {
        //                if (!e2.MoveNext())
        //                {
        //                    return false;
        //                }
        //                object o1 = e1.Current;
        //                object o2 = e2.Current;
        //                if (!o1.Equals(o2))
        //                {
        //                    return false;
        //                }
        //            }
        //            return !e2.MoveNext();
        //        }
        //        #endregion

        //        #region DataTableを指定した項目でSort
        //        /// <summary>
        //        /// DataTableを指定した項目でSortします。
        //        /// </summary>
        //        /// <param name="dt">Sort前DataTable</param>
        //        /// <param name="SortColumn">Sortする項目名</param>
        //        /// <returns>Sort後DataTable</returns>
        //        public static DataTable DataTableSort(DataTable dt, string SortColumn)
        //        {
        //            DataTable ret = dt.Clone();

        //            DataView dv = new DataView(dt);
        //            dv.Sort = SortColumn;
        //            foreach (DataRowView drv in dv)
        //            {
        //                ret.ImportRow(drv.Row);
        //            }
        //            return ret;
        //        }
        //        #endregion

        //        #region ボタンの2度押し防止 (+1)
        //        /// <summary>
        //        /// ボタンの2度押しを防止します。
        //        /// </summary>
        //        /// <param name="_btn">指定ボタン</param>
        //        /// <param name="_btnName">指定ボタン名</param>
        //        /// <param name="_clientscript">ClientScriptManagerオブジェクト</param>
        //        public static void DoubleClickPrevention(Button _btn, string _btnName, ClientScriptManager _clientscript)
        //        {
        //            if (_btn != null)
        //            {
        //                string strScript = "document.getElementById('" + _btnName + "').disabled =true;";
        //                _btn.Attributes.Add("onclick", strScript + _clientscript.GetPostBackEventReference(_btn, ""));
        //            }
        //        }

        //        /// <summary>
        //        /// ボタンの2度押しを防止します。（確認ダイアログ用）
        //        /// </summary>
        //        /// <param name="_btn">指定ボタン</param>
        //        /// <param name="_btnName">指定ボタン名</param>
        //        /// <param name="_clientscript">ClientScriptManagerオブジェクト</param>
        //        /// <param name="_msgText">出力メッセージ</param>
        //        public static void DoubleClickPrevention(Button _btn, string _btnName, ClientScriptManager _clientscript, string _msgText)
        //        {
        //            if (_btn != null)
        //            {
        //                string strScript = "if (confirm('" + _msgText + "')){";
        //                strScript += "document.getElementById('" + _btnName + "').disabled =true;}";
        //                strScript += "else{window.event.returnValue = false;return false; return true;};";
        //                _btn.Attributes.Add("onclick", strScript + _clientscript.GetPostBackEventReference(_btn, ""));
        //            }
        //        }
        //        #endregion

        //        #region ツリービューからチェックボックス押下処理用のJavaScriptを生成
        //        /// <summary>
        //        /// ツリービューからチェックボックス押下処理用のJavaScriptを生成し、文字列で返します。
        //        /// </summary>
        //        /// <param name="tree_name">ツリービュー名</param>
        //        /// <param name="select_keys_name">選択した項目のValue値を保持するコントロール名（ASP:Hiddenであること。）</param>
        //        /// <returns>JavaScript文字列</returns>


        public static string GetScriptCheckBoxDyatreeonSelect(string treeName, string select_keys_name)             /*GetScript_CheckBoxDyatree_onSelect*/
        {
            StringBuilder sbRet = new StringBuilder();
            sbRet.Append(" $(function(){ " + Constant.CcrMark);
            //sbRet.Append("alert($(window.opener.document).find('#selected_Keys').val());" + Constant.CcrMark);
            //  sbRet.Append("BeforeLoadingTree());" + Constant.CcrMark);
            sbRet.Append("     $('#" + treeName + "').dynatree({ " + Constant.CcrMark);
            sbRet.Append("         checkbox: true, " + Constant.CcrMark);
            sbRet.Append("         selectMode: 3, " + Constant.CcrMark);
            sbRet.Append("         children: treeData, " + Constant.CcrMark);
            sbRet.Append("         onSelect: function(select, node) { " + Constant.CcrMark);
            sbRet.Append("             var selKeys = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.key; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#" + select_keys_name + "').val(selKeys.join(',')); " + Constant.CcrMark);
            sbRet.Append("             var selKeysname = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.title; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#select_keys_name').val(selKeysname.join(',')); " + Constant.CcrMark);
            sbRet.Append("         }, " + Constant.CcrMark);

            sbRet.Append("         onCreate: function(node) { " + Constant.CcrMark);
            sbRet.Append("             var selKeys = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.key; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#" + select_keys_name + "').val(selKeys.join(',')); " + Constant.CcrMark);
            sbRet.Append("             var selKeysname = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.title; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#select_keys_name').val(selKeysname.join(',')); " + Constant.CcrMark);
            sbRet.Append("         }, " + Constant.CcrMark);

            sbRet.Append("         cookieId: 'dynatree-Cb3', " + Constant.CcrMark);
            sbRet.Append("         idPrefix: 'dynatree-Cb3-', " + Constant.CcrMark);
            sbRet.Append("         imagePath: '../" + SystemConst.RegulationImageFolderName + "/' " + Constant.CcrMark);
            sbRet.Append("     }); " + Constant.CcrMark);
            sbRet.Append(" }); " + Constant.CcrMark);

            return sbRet.ToString();
        }
        //        #endregion


        //        #region ツリービューからチェックボックス(末端のみ)押下処理用のJavaScriptを生成
        //        /// <summary>
        //        /// ツリービューからチェックボックス(末端のみ)押下処理用のJavaScriptを生成し、文字列で返します。
        //        /// </summary>
        //        /// <param name="tree_name">ツリービュー名</param>
        //        /// <param name="select_keys_name">選択した項目のValue値を保持するコントロール名（ASP:Hiddenであること。）</param>
        //        /// <returns>JavaScript文字列</returns>
        public static string GetScriptCheckBoxDyatreeonTerminalSelect(string treeName, string select_keys_name)  /*GetScript_CheckBoxDyatree_onTerminalSelect*/
        {
            StringBuilder sbRet = new StringBuilder();
            sbRet.Append(" $(function(){ " + Constant.CcrMark);
            //  sbRet.Append("alert($(window.opener.document).find('#LocationIds').val());" + Constant.CcrMark);
            sbRet.Append("     $('#" + treeName + "').dynatree({ " + Constant.CcrMark);
            sbRet.Append("         checkbox: true, " + Constant.CcrMark);
            sbRet.Append("         selectMode: 2, " + Constant.CcrMark);
            sbRet.Append("         children: treeData, " + Constant.CcrMark);
            sbRet.Append("         onSelect: function(select, node) { " + Constant.CcrMark);
            sbRet.Append("             var selKeys = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.key; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#" + select_keys_name + "').val(selKeys.join(',')); " + Constant.CcrMark);
            sbRet.Append("             var selKeysname = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.title; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#select_keys_name').val(selKeysname.join(',')); " + Constant.CcrMark);
            sbRet.Append("         }, " + Constant.CcrMark);

            sbRet.Append("         onCreate: function(node) { " + Constant.CcrMark);
            sbRet.Append("             var selKeys = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.key; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#" + select_keys_name + "').val(selKeys.join(',')); " + Constant.CcrMark);
            sbRet.Append("             var selKeysname = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.title; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#select_keys_name').val(selKeysname.join(',')); " + Constant.CcrMark);
            sbRet.Append("         }, " + Constant.CcrMark);

            sbRet.Append("         cookieId: 'dynatree-Cb2', " + Constant.CcrMark);
            sbRet.Append("         idPrefix: 'dynatree-Cb2-', " + Constant.CcrMark);
            sbRet.Append("         imagePath: '../" + SystemConst.RegulationImageFolderName + "/' " + Constant.CcrMark);
            sbRet.Append("     }); " + Constant.CcrMark);
            sbRet.Append(" }); " + Constant.CcrMark);

            return sbRet.ToString();
        }
        //        #endregion


        //        #region ツリービューからラジオボタン押下処理用のJavaScriptを生成
        //        /// <summary>
        //        /// ツリービューからラジオボタン押下処理用のJavaScriptを生成し、文字列で返します。
        //        /// </summary>
        //        /// <param name="tree_name">ツリービュー名</param>
        //        /// <param name="select_keys_name">選択した項目のValue値を保持するコントロール名（ASP:Hiddenであること。）</param>
        //        /// <returns>JavaScript文字列</returns>
        public static string GetScriptRadioDyatreeonSelect(string treeName, string select_keys_name)
        {
            StringBuilder sbRet = new StringBuilder();
            sbRet.Append(" $(function(){ " + Constant.CcrMark); //lblGroupName
                                                                // sbRet.Append("alert($('#hdnGroupCd').val());" + Constant.CcrMark);
                                                                // sbRet.Append("var x = $('#hdnGroupCd').val();" + Constant.CcrMark);
                                                                // sbRet.Append("BeforeLoadingTree(x);" + Constant.CcrMark);
                                                                //sbRet.Append("var x = $('#select_keys_name').val();" + Constant.CcrMark);
                                                                //sbRet.Append("BeforeLoadingTree();" + Constant.CcrMark);
                                                                //sbRet.Append("BeforeLoadingTree(JSON.stringify(" + tree + ") );" + Constant.CcrMark);
            sbRet.Append("     $('#" + treeName + "').dynatree({ " + Constant.CcrMark);
            sbRet.Append("         checkbox: true, " + Constant.CcrMark);
            sbRet.Append("         classNames: { checkbox: 'dynatree-radio' }, " + Constant.CcrMark);
            sbRet.Append("         selectMode: 1, " + Constant.CcrMark);
            sbRet.Append("         children: treeData, " + Constant.CcrMark);
            sbRet.Append("         onSelect: function(select, node) { " + Constant.CcrMark);
            sbRet.Append("             var selKeys = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.key; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#" + select_keys_name + "').val(selKeys.join(',')); " + Constant.CcrMark);
            sbRet.Append("             var selKeysname = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.title; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             $('#select_keys_name').val(selKeysname.join(',')); " + Constant.CcrMark);
            sbRet.Append("         }, " + Constant.CcrMark);

            sbRet.Append("         onCreate: function(node) { " + Constant.CcrMark);
            sbRet.Append("             var selKeys = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.key; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);
            sbRet.Append("             var selKeysname = $.map(node.tree.getSelectedNodes(), function(node){ " + Constant.CcrMark);
            sbRet.Append("                     return node.data.title; " + Constant.CcrMark);
            sbRet.Append("             }); " + Constant.CcrMark);

            sbRet.Append("             $('#" + select_keys_name + "').val(selKeys.join(',')); " + Constant.CcrMark);
            sbRet.Append("             $('#select_keys_name').val(selKeysname.join(',')); " + Constant.CcrMark);

            sbRet.Append("         }, " + Constant.CcrMark);

            sbRet.Append("         initId: 'treeData', " + Constant.CcrMark);
            sbRet.Append("         cookieId: 'dynatree-Cb1', " + Constant.CcrMark);
            sbRet.Append("         idPrefix: 'dynatree-Cb1-', " + Constant.CcrMark);
            sbRet.Append("         imagePath: '../" + SystemConst.RegulationImageFolderName + "/' " + Constant.CcrMark);
            sbRet.Append("     }); " + Constant.CcrMark);
            sbRet.Append(" }); " + Constant.CcrMark);

            return sbRet.ToString();
        }



        #region サーバーにファイルをアップロード
        /// <summary>
        /// サーバーにファイルをアップロードします。
        /// </summary>
        /// <param name="postedFile">Inputコントロールから取得したファイル</param>
        /// <param name="uploadDir">アップロード先のディレクトリ</param>
        /// <returns>
        /// アップロードしたファイルのフルパス（サーバーサイド）
        /// </returns>
        public static string UploadFile(HttpPostedFileBase postedFile, string uploadDir)
        {
            string serverFileName = "";

            // uploadするフォルダが無ければ作成する
            if (!Directory.Exists(uploadDir))
            {
                Directory.CreateDirectory(uploadDir);
            }

            // クライアントサイドのファイル名をチェック
            if (postedFile.FileName != "")
            {
                // サーバーサイドにファイルを保存する
                postedFile.SaveAs(uploadDir + @"\" + Path.GetFileName(postedFile.FileName));

                if (File.Exists(uploadDir + @"\" + Path.GetFileName(postedFile.FileName)))
                {
                    serverFileName = uploadDir + @"\" + Path.GetFileName(postedFile.FileName);
                }
            }

            return serverFileName;
        }
        #endregion

        //        #endregion

        //        #region 保管場所選択画面からの遷移情報を取得し、返す。
        //        /// <summary>
        //        /// 保管場所選択画面からの遷移情報を取得し、返します。
        //        /// </summary>
        //        /// <param name="_page">ページオブジェクト</param>
        //        /// <returns>遷移情報格納配列。不正値があった場合は、nullを返します。</returns>
        //        public static object[] GetZC01031Value(Page _page)
        //        {
        //            object[] ret = null;
        //            if (_page.Session[ZC01031.cSESSION_NAME_ReturnValue] != null)
        //            {
        //                object[] result = (object[])_page.Session[ZC01031.cSESSION_NAME_ReturnValue];
        //                if (result != null && result.Length == ZC01031.cTRANS_ITEM_CNT)
        //                {
        //                    ret = result;
        //                }
        //            }
        //            return ret;
        //        }
        //        #endregion

        //        #region 法規制選択画面からの遷移情報を取得し、返す。
        //        /// <summary>
        //        /// 法規制選択画面からの遷移情報を取得し、返します。
        //        /// </summary>
        //        /// <param name="_page">ページオブジェクト</param>
        //        /// <returns>遷移情報格納配列。不正値があった場合は、nullを返します。</returns>
        //        public static object[] GetZC01032Value(Page _page)
        //        {
        //            object[] ret = null;
        //            if (_page.Session[ZC01032.cSESSION_NAME_ReturnValue] != null)
        //            {
        //                object[] result = (object[])_page.Session[ZC01032.cSESSION_NAME_ReturnValue];
        //                if (result != null && result.Length == ZC01032.cTRANS_ITEM_CNT)
        //                {
        //                    ret = result;
        //                }
        //            }
        //            return ret;
        //        }
        //        #endregion

        //        #region 社員選択画面からの遷移情報を取得し、返す。
        //        /// <summary>
        //        /// 社員選択画面からの遷移情報を取得し、返します。
        //        /// </summary>
        //        /// <param name="_page">ページオブジェクト</param>
        //        /// <returns>遷移情報格納配列。不正値があった場合は、nullを返します。</returns>
        //        public static object[] GetZC01033Value(Page _page)
        //        {
        //            object[] ret = null;
        //            if (_page.Session[ZC01033.cSESSION_NAME_ReturnValue] != null)
        //            {
        //                object[] result = (object[])_page.Session[ZC01033.cSESSION_NAME_ReturnValue];
        //                if (result != null && result.Length == ZC01033.cTRANS_ITEM_CNT)
        //                {
        //                    ret = result;
        //                }
        //            }
        //            return ret;
        //        }
        //        #endregion

        //        #region (受入画面用)在庫ステータス検索タイプを選択するDropDownList用のDataTableを生成
        //        /// <summary>
        //        /// (受入画面用)在庫ステータス検索タイプを選択するDropDownList用のDataTableを生成します。
        //        /// </summary>
        //        /// <param name="Text">Text列</param>
        //        /// <param name="Value">Value列</param>
        //        /// <returns>生成したDataTable</returns>
        //        public static DataTable GetDataTableZaikoStatus(string Text, string Value)
        //        {
        //            DataTable dt = new DataTable();
        //            DataRow dr;
        //            dt.Columns.Add(Text);
        //            dt.Columns.Add(Value);

        //            dr = dt.NewRow();
        //            dr[Text] = "納品待";
        //            dr[Value] = Const.cSTOCK_STATUS_ID.DELI_WAIT.ToString("D");
        //            dt.Rows.Add(dr);

        //            dr = dt.NewRow();
        //            dr[Text] = "納品済";
        //            dr[Value] = Const.cSTOCK_STATUS_ID.DELI_FIN.ToString("D");
        //            dt.Rows.Add(dr);

        //            return dt;
        //        }
        //        #endregion

        //        #region 選択可能ドロップダウンリストにアイテムを設定したJavaSciptを生成し、文字列で返す。
        //        /// <summary>
        //        /// 選択可能ドロップダウンリストにアイテムを設定したJavaSciptを生成し、文字列で返します。
        //        /// </summary>
        //        /// <param name="dtVal">アイテム格納DataTable</param>
        //        /// <param name="colVal">Value値が格納されているカラム名</param>
        //        /// <param name="colName">Name値が格納されているカラム名</param>
        //        /// <param name="controlName">ドロップダウンリスト名</param>
        //        /// <returns>JavaScript文字列</returns>
        //        public static string GetScript_DropDownControl(DataTable dtVal, string colVal, string colName, string ddlName)
        //        {
        //            StringBuilder ret = new StringBuilder();

        //            if (dtVal != null && dtVal.Rows.Count > 0 )
        //            {
        //                string val;
        //                string name;

        //                ret.Append( "$(document).ready(function () {" + Const.cCR_MARK );
        //                ret.Append( " var data = { items: [ " + Const.cCR_MARK );

        //                for (int i = 0; i < dtVal.Rows.Count; i++)
        //                {
        //                    val = dtVal.Rows[i][colVal].ToString();
        //                    name = dtVal.Rows[i][colName].ToString();

        //                    ret.Append( " { value: '" + val + "', name: '" + name + "' }");
        //                    if ( i != dtVal.Rows.Count - 1)
        //                    {
        //                        ret.Append( "," + Const.cCR_MARK );
        //                    }
        //                    else
        //                    {
        //                        ret.Append( Const.cCR_MARK );
        //                    }
        //                }
        //                ret.Append( "  ]" + Const.cCR_MARK );
        //                ret.Append( " };" + Const.cCR_MARK );
        //                ret.Append(" $('#" + ddlName + "').autoSuggest(data.items, { selectedItem: 'name', searchObj: 'name' });" + Const.cCR_MARK);
        //                ret.Append( "});" + Const.cCR_MARK);
        //            }
        //            return ret.ToString();
        //        }
        //        #endregion

        #region 半角カナ文字を含んでいるかをチェック
        /// <summary>
        /// 半角カナ文字を含んでいるかをチェックします。
        /// </summary>
        /// <param name="strText">判定したいデータ</param>
        /// <returns>true:含む / false:含まない</returns>
        public static bool isHankakuKana(string strText)
        {
            if (strText.Length == 0)
            {
                return false;
            }
            else
            {
                //正規表現パターンを指定してRegexオブジェクトを作成
                System.Text.RegularExpressions.Regex r =
                    new System.Text.RegularExpressions.Regex(
                        @"[\uFF61-\uFF9F]");

                //半角カナ文字が含まれているか調べる
                if (r.IsMatch(strText))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        #endregion

        #region 全角カナ文字チェック
        /// <summary>
        /// 全角カナ文字かチェックします。
        /// </summary>
        /// <param name="strText">判定したいデータ</param>
        /// <param name="blSpace">true:全角スペースOK false:全角スペースNG</param>
        /// <returns>true:全角カナ文字 / false:全角カナ文字以外</returns>
        public static bool isZenkakuKana(string strText, bool blSpace)
        {
            bool bl;
            if (blSpace)
            {
                bl = System.Text.RegularExpressions.Regex.IsMatch(strText, @"^[\p{IsKatakana}　 ]+$");
            }
            else
            {
                bl = System.Text.RegularExpressions.Regex.IsMatch(strText, @"^\p{IsKatakana}+$");
            }

            if (bl)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        //        #region 文字列をシングルコーテーションで囲んで返す。(SQL用)
        //        /// <summary>
        //        /// 文字列をシングルコーテーションで囲んで返します。(SQL用)
        //        /// </summary>
        //        /// <param name="strText">文字列</param>
        //        /// <returns>シングルコーテーションで囲んだ文字列</returns>
        //        public static string GetSQLString(string strText)
        //        {
        //            if (strText == null)
        //            {
        //                return strText;
        //            }

        //            string strWork = string.Empty;
        //            // 文字列をシングルコーテーションで囲んで返します
        //            strWork = "'" + strText + "'";

        //            return strWork;
        //        }
        //        #endregion

        //        #region グリッドの内容をCSVへ出力
        //        /// <summary>
        //        /// グリッドの内容をCSVへ出力します。
        //        /// </summary>
        //        /// <param name="_page">ページオブジェクト</param>
        //        /// <param name="_grid">グリッドオブジェクト</param>
        //        /// <param name="_outCsvName">出力するCSVファイル名</param>
        //        /// <param name="IsOutNoVisibleData">true:非表示の列も出力します。 / false:非表示列は出力しません。</param>
        //        /// <remarks>データ１行目に非表示のセルが存在すると、その列はすべて非表示列と判断しているので注意！</remarks>
        //        public static DataTable OutCsvFromGrid(Page _page, GridView _grid, string _outCsvName, bool IsOutNoVisibleData)
        //        {
        //            DataTable dt = new DataTable(); //2018/10/05 FJ Upd Wait処理対応

        //            if (_grid.Rows.Count == 0)
        //            {
        //                // 出力するデータは存在しません。
        //                Common.MsgBox(_page, Common.GetMessage("C015", "出力するデータ"));
        //                return dt;  //2018/10/05 FJ Upd Wait処理対応
        //            }

        //            // ヘッダーの取得・設定
        //            for (int i = 0; i < _grid.Columns.Count; i++)
        //            {
        //                // 非表示列でも表示するモードか、表示列の場合のみヘッダーを取得・設定
        //                if (IsOutNoVisibleData || _grid.Rows[0].Cells[i].Visible || _grid.Columns[i].AccessibleHeaderText == true.ToString())
        //                {
        //                    if(_grid.Columns[i].AccessibleHeaderText != false.ToString())
        //                    {
        //                        dt.Columns.Add(_grid.Columns[i].HeaderText);
        //                    }
        //                }
        //            }

        //            // データの取得・設定
        //            for (int i = 0; i < _grid.Rows.Count; i++)
        //            {
        //                DataRow dr = dt.NewRow();
        //                foreach (DataColumn col in dt.Columns)
        //                {
        //                    dr[col.ColumnName] = HttpUtility.HtmlDecode(GridViewUtil.getRowValue(_grid, i, col.ColumnName));
        //                }
        //                dt.Rows.Add(dr);
        //            }

        //            //2018/10/05 FJ Upd Wait処理対応
        //            //// 出力
        //            //Common.DownloadCsvFile(_page.Response, dt, _outCsvName);

        //            return dt;
        //            //2018/10/05 FJ Upd Wait処理対応

        //        }
        //        #endregion

        //        #region テキストボックスを右寄せスタイルに設定
        //        /// <summary>
        //        /// テキストボックスを右寄せスタイルに設定します。
        //        /// </summary>
        //        /// <param name="txt">テキストボックス</param>
        //        public static void txtBoxStyle_AlignRight(ref TextBox txt)
        //        {
        //            if (txt.Style.Value != "")
        //            {
        //                txt.Style.Value = txt.Style.Value + "; text-align: right";
        //            }
        //            else
        //            {
        //                txt.Style.Value = "text-align: right";
        //            }
        //        }
        //        #endregion

        //        #region 文字列に含まれている通貨のみを半角・全角に変換し返却
        //        /// <summary>
        //        /// 文字列に含まれている通貨のみを半角・全角に変換し返却します。
        //        /// </summary>
        //        /// <param name="val">変換元文字列</param>
        //        /// <param name="convZenkaku">true:半角→全角に変換 / false:全角→半角に変換</param>
        //        /// <returns>変換後文字列</returns>
        //        /// <remarks>
        //        /// 通貨は、Web.ConfgのCONV_CURRに定義。
        //        /// </remarks>
        //        public static string CONV_CURR(string val, bool convZenkaku)
        //        {
        //            string ret = val;
        //            string[] ArrayCurr = Common.Split(WebConfigUtil.CONV_CURR, ",");
        //            foreach (string curr in ArrayCurr)
        //            {
        //                string[] split = Common.Split(curr, ":");
        //                if (split.Length == 2)
        //                {
        //                    if (convZenkaku)
        //                    {
        //                        ret = ret.Replace(split[0], split[1]);
        //                    }
        //                    else
        //                    {
        //                        ret = ret.Replace(split[1], split[0]);
        //                    }
        //                }
        //            }
        //            return ret;
        //        }
        //        #endregion

        //        #region Session変数にログインユーザー情報が存在するかを返却（+2）
        //        /// <summary>
        //        /// Session変数にログインユーザー情報が存在するかを返却します。
        //        /// 存在する場合は、ログイン情報クラスとしてout で返却します。
        //        /// </summary>
        //        /// <param name="page">ページオブジェクト</param>
        //        /// <returns>true:存在する / false:存在しない</returns>
        //        public static bool GetUserInfo(Page page)
        //        {
        //            UserInfo.clsUser _userinfo = null;
        //            return GetUserInfo(page.Session, out _userinfo);
        //        }

        //        /// <summary>
        //        /// Session変数にログインユーザー情報が存在するかを返却します。
        //        /// 存在する場合は、ログイン情報クラスとしてout で返却します。
        //        /// </summary>
        //        /// <param name="page">ページオブジェクト</param>
        //        /// <param name="_userinfo">ログイン情報クラス</param>
        //        /// <returns>true:存在する / false:存在しない</returns>
        //        public static bool GetUserInfo(Page page, out UserInfo.clsUser _userinfo)
        //        {
        //            return GetUserInfo(page.Session, out _userinfo);
        //        }

        /// <summary>
        /// Session変数にログインユーザー情報が存在するかを返却します。
        /// 存在する場合は、ログイン情報クラスとしてout で返却します。
        /// </summary>
        /// <param name="Session">セッション</param>
        /// <param name="_userinfo">ログイン情報クラス</param>
        /// <returns>true:存在する / false:存在しない</returns>
        public static bool GetUserInfo(HttpSessionState Session, out UserInfo.ClsUser userinfo)
        {
            bool ret = false;
            userinfo = null;
            if (Session[Constant.CsessionNameUserINFO] != null)
            {
                UserInfo.ClsUser tmpUserInfo = (UserInfo.ClsUser)Session[Constant.CsessionNameUserINFO];
                if (tmpUserInfo.USER_CD != null && tmpUserInfo.USER_CD != "")
                {
                    userinfo = tmpUserInfo;
                    ret = true;
                }
            }
            return ret;
        }
        //        #endregion

        //        #region コンボボックスへのデータバインド
        //        /// <summary>
        //        /// コンボボックスへデータをバインドします。
        //        /// </summary>
        //        /// <param name="cb">対象のコンボボックス</param>
        //        /// <param name="width">コンボボックスの幅</param>
        //        /// <param name="dt">バインドするDataTable</param>
        //        /// <param name="textField">DataTextFieldの名前</param>
        //        /// <param name="valueField">DataValueFieldの名前</param>
        //        /// <param name="space">空行の有無</param>
        //        public static void DataBindComboBox(AjaxControlToolkit.ComboBox cb, int width, DataTable dt, string textField, string valueField, bool space)
        //        {
        //            cb.Width = width;
        //            cb.DataTextField = textField;
        //            cb.DataValueField = valueField;
        //            cb.DataSource = dt;
        //            cb.DataBind();

        //            //1行目に空を追加
        //            if (space)
        //            {
        //                cb.Items.Insert(0, "");
        //            }
        //        }
        //        #endregion

        //        #region ドロップダウンリストへのデータバインド
        //        /// <summary>
        //        /// ドロップダウンリストへデータをバインドします。
        //        /// </summary>
        //        /// <param name="ddl">対象のドロップダウンリスト</param>
        //        /// <param name="dt">バインドするDataTable</param>
        //        /// <param name="textField">DataTextFieldの名前</param>
        //        /// <param name="valueField">DataValueFieldの名前</param>
        //        /// <param name="space">空行の有無</param>
        //        public static void DataBindDropDownList(DropDownList ddl, DataTable dt, string textField, string valueField, bool space)
        //        {
        //            ddl.DataTextField = textField;
        //            ddl.DataValueField = valueField;
        //            ddl.DataSource = dt;
        //            ddl.DataBind();

        //            //1行目に空を追加
        //            if (space)
        //            {
        //                ddl.Items.Insert(0, "");
        //            }
        //        }
        //        #endregion

        //#region エラー画面へ遷移 (+1)
        ///// <summary>
        ///// エラー画面へ遷移します。
        ///// </summary>
        ///// <param name="page">遷移元ページオブジェクト</param>
        ///// <param name="programId">遷移元プログラムID</param>
        //public static void transErrorAspx(Page page, string programId)
        //{
        //    transErrorAspx(page, programId, null, true);
        //}
        ///// <summary>
        ///// エラー画面へ遷移します。
        ///// </summary>
        ///// <param name="page">遷移元ページオブジェクト</param>
        ///// <param name="programId">遷移元プログラムID</param>
        ///// <param name="errLog">エラー画面で表示するエラーメッセージ</param>
        ///// <param name="OutLogOnOnly">
        ///// true:WebConfigのOUT_ERRLOGMSG_FLGが1の時のみメッセージを出力
        ///// false:WebConfigの値関係なくエラーメッセージを表示
        ///// </param>
        //public static void transErrorAspx(Page page, string programId, string errLog, bool OutLogOnOnly)
        //{
        //    if (errLog != null && errLog != "")
        //    {
        //        if (!OutLogOnOnly ||
        //            (OutLogOnOnly && WebConfigUtil.OUT_ERRLOGMSG_FLG == WebConfigUtil.cOUT_ERRLOGMSG_FLG_ON))
        //        {
        //            page.Session[Const.cSESSION_NAME_ERRMSG] = errLog;
        //        }
        //    }
        //    page.Response.Redirect(Const.cPATH_ERROR_ASPX + "?id=" + programId, false);
        //}
        //#endregion

        //        #region 対象のプログラムIDが子画面のプログラムIDかを確認
        //        /// <summary>
        //        /// 対象のプログラムIDが子画面のプログラムIDであるかを確認します。
        //        /// </summary>
        //        /// <param name="programId">対象のプログラムID</param>
        //        /// <returns>true:子画面 / false:子画面でない</returns>
        //        public static bool IsChildProgramId(string programId)
        //        {
        //            bool ret = false;
        //            string[] childIds = Common.Split(WebConfigUtil.CHILD_PROGRAM_ID, ",");
        //            if (childIds != null && childIds.Length > 0)
        //            {
        //                foreach (string id in childIds)
        //                {
        //                    if (id == programId)
        //                    {
        //                        ret = true;
        //                        break;
        //                    }
        //                }
        //            }
        //            return ret;
        //        }
        //        #endregion

        /// <summary>
        /// メール本文が書かれたテンプレートを文字列に変換して返却します。
        /// </summary>
        /// <param name="_mailTemplatePath">テンプレートが存在するパス</param>
        /// <returns>変換文字列。テンプレートファイルがなければ空。</returns>
        public static string GetMailBodyString(string _mailTemplatePath)
        {
            StringBuilder ret = new StringBuilder();

            StreamReader readtxt = new StreamReader(_mailTemplatePath, System.Text.Encoding.Default);
            while (readtxt.Peek() >= 0)
            {
                if (ret.Length > 0)
                {
                    ret.Append("\r\n");
                }
                ret.Append(readtxt.ReadLine());
            }

            readtxt.Close();
            return ret.ToString();
        }


        public static UserInformation GetLogonForm()
        {
            UserInformation loginModel = new UserInformation();

            if (HttpContext.Current.Session["LoginUserSession"] != null)
            {
                loginModel = (UserInformation)HttpContext.Current.Session["LoginUserSession"];
            }
            return loginModel;
        }

        public static void DownloadFileFromPath(string Filepath, string Filename)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(Filepath);
                var response = HttpContext.Current.Response;
                if (fileInfo.Exists)
                {
                    response.Clear();
                    response.AddHeader("Content-Disposition", "attachment; filename=" + (Filename));
                    //switch (fileInfo.FullName.Substring(fileInfo.FullName.LastIndexOf(".") + 1).ToLower())
                    //{
                    //    case "xlsx":
                    //        response.ContentType = "application/excel";
                    //        break;
                    //    case "csv":
                    //        response.ContentType = "text/comma-separated-values";
                    //        break;
                    //    //2018/10/05 FJ Upd Wait処理対応
                    //    case "zip":
                    //        response.ContentType = "application/zip";
                    //        break;
                    //        //2018/10/05 FJ Upd Wait処理対応
                    //}
                    response.AddHeader("Content-Length", fileInfo.Length.ToString());
                    response.ContentType = "application/octet-stream";
                    response.Flush();
                    response.TransmitFile(fileInfo.FullName);
                    response.End();
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }
        public static IEnumerable<SelectListItem> BindLookupValues(DataTable res)
        {
            //List<DataRow> list =res;
            List<DataRow> list = res.AsEnumerable().ToList();
            IEnumerable<SelectListItem> selectlist = from s in list
                                                     select new SelectListItem
                                                     {
                                                         Text = s["Text"].ToString(),
                                                         Value = s["Val"].ToString()
                                                     };
            return selectlist;
        }

        public static class LookupConstants
        {
            public const string CSNCONDITION = "CSNCONDITION";
            public const string MAKERNAMECONDITION = "MAKERNAMECONDITION";
            public const string CSCODECONDITION = "CSCODECONDITION";
            public const string BOTTLENUMCONDITION = "BOTTLENUMCONDITION";
            public const string ACCEPTCONDITION = "ACCEPTCONDITION";
            public const string MAKERNAME = "MAKERNAME";
            public const string AMOUNTUNITSIZE = "AMOUNTUNITSIZE";
            public const string STATUS = "STATUS";
            public const string INCLUDE = "INCLUDE";
            public const string DDLLOCATION = "DDLLOCATION";

            //ML lookup constant
            public const string CHEMNAMECONDITION = "CHEMNAMECONDITION";
            public const string PRODUCTNAME = "PRODUCTNAME";
            public const string CAS = "CAS";
            public const string REGISTRATIONNUMBER = "REGISTRATIONNUMBER";
            public const string STATE = "STATE";
            public const string REASON = "REASON";
            public const string TRANSACTIONVOLUME = "AMOUNTUNITSIZE";
            public const string WORKFREQUENCY = "WORKFREQUENCY";
            public const string DISASTERPOSSIBILITY = "DISASTERPOSSIBILITY";
            public const string CONTAINER = "CONTAINER";
            public const string MANUFACTURENAME = "MANUFACTURENAME";
            public const string UNITSIZEMASTERINFO = "UNITSIZEMASTERINFO";
            public const string USAGEMASTERINFO = "USAGEMASTERINFO";
        }
    }
}