﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;
using System.Drawing;
using ZyCoaG.Classes.util;

namespace ZyCoaG.Classes.util
{
    /// <summary>
    /// GridView操作用の共通メソッド群です。
    /// </summary>
    public class GridViewUtil
    {
        /// <summary>
        /// グリッドにスタイルを設定します。
        /// 全グリッド共通なので各画面必ず呼んでください。
        /// </summary>
        /// <param name="AutoGenerateColumns">
        /// true：呼び出し元で列を作成するとき、bindしか行わない時。
        /// false:呼び出し元が手動で列を作成する時。
        /// </param>
        /// <param name="gridWidth">グリッドの幅。特に指定しない場合は、grid.Widthを渡してください。</param>
        /// <param name="showFooter">true：フッター表示 / false：フッター非表示</param>
        /// <param name="bSort">true：並び替えあり / false：並び替えなし</param>
        /// <param name="grid">グリッド</param>
        public static void SetGridStyle( bool AutoGenerateColumns, Unit gridWidth, bool showFooter, bool bSort, GridView grid)
        {
            // Grid全体のスタイル設定
            grid.Columns.Clear();
            grid.AutoGenerateColumns = AutoGenerateColumns;
            grid.Width = gridWidth;
            grid.BorderStyle = BorderStyle.Solid;
            grid.BorderWidth = Unit.Pixel(1);
            grid.CellPadding = 4;
            grid.Font.Size = FontUnit.Small;
            grid.ForeColor = Color.Black;
            grid.BackColor = Color.White;
            grid.GridLines = GridLines.Vertical;
            grid.ShowHeaderWhenEmpty = true;

            // Gridヘッダーのスタイル設定
            grid.HeaderStyle.BackColor = Color.Maroon;
            grid.HeaderStyle.ForeColor = Color.White;
            grid.HeaderStyle.Font.Bold = true;
            grid.HeaderStyle.BorderStyle = BorderStyle.Solid;
            grid.HeaderStyle.BorderWidth = Unit.Pixel(2);
            grid.HeaderStyle.BorderColor = Color.White;
            grid.HeaderStyle.HorizontalAlign = HorizontalAlign.Center;

            // Grid交互行の設定
            grid.AlternatingRowStyle.BackColor = Color.SeaShell;

            grid.AllowSorting = bSort;
            grid.ShowFooter = showFooter;
            if (showFooter)
            {
                // Gridフッターのスタイル設定
                grid.FooterStyle.BackColor = grid.HeaderStyle.BackColor;
                grid.FooterStyle.ForeColor = grid.HeaderStyle.ForeColor;
                grid.FooterStyle.Font.Bold = grid.HeaderStyle.Font.Bold;
                grid.FooterStyle.BorderStyle = grid.HeaderStyle.BorderStyle;
                grid.FooterStyle.BorderWidth = grid.HeaderStyle.BorderWidth;
                grid.FooterStyle.BorderColor = grid.HeaderStyle.BorderColor;
                grid.FooterStyle.HorizontalAlign = HorizontalAlign.Center;
            }
        }

        #region TemplateFieldの列を追加 ( + 3)
        /// <summary>
        /// TemplateFieldの列を追加します。
        /// </summary>
        /// <param name="template">ItemTemplate</param>
        /// <param name="headerText">列名（表示）</param>
        public static void AddTemplateField(dynamic template, string headerText, bool bVisible, GridView grid)
        {
            AddTemplateField(template, headerText, bVisible, grid, (new TemplateField()).ItemStyle.HorizontalAlign, null);
        }
        /// <summary>
        /// TemplateFieldの列を追加します。
        /// </summary>
        /// <param name="template">ItemTemplate</param>
        /// <param name="headerText">列名（表示）</param>
        /// <param name="bVisible"></param>
        /// <param name="grid"></param>
        /// <param name="sort">SortExpression</param>
        public static void AddTemplateField(dynamic template, string headerText, bool bVisible, GridView grid, string sort)
        {
            AddTemplateField(template, headerText, bVisible, grid, (new TemplateField()).ItemStyle.HorizontalAlign, sort);
        }
        /// <summary>
        /// TemplateFieldの列を追加します。
        /// </summary>
        /// <param name="template">ItemTemplate</param>
        /// <param name="headerText">列名（表示）</param>
        /// <param name="bVisible"></param>
        /// <param name="grid"></param>
        /// <param name="align">水平方向の配置</param>
        public static void AddTemplateField(dynamic template, string headerText, bool bVisible, GridView grid, HorizontalAlign align)
        {
            AddTemplateField(template, headerText, bVisible, grid, align, null);
        }
        /// <summary>
        /// TemplateFieldの列を追加します。
        /// </summary>
        /// <param name="template">ItemTemplate</param>
        /// <param name="headerText">列名（表示）</param>
        /// <param name="bVisible"></param>
        /// <param name="grid"></param>
        /// <param name="align">水平方向の配置</param>
        /// <param name="sort">SortExpression</param>
        public static void AddTemplateField(dynamic template, string headerText, bool bVisible, GridView grid, HorizontalAlign align, string sort)
        {
            TemplateField field = new TemplateField();
            //if(template.GetType() == typeof(GridViewClass.ColumnCheckBox))
            //{
            //    var headerColumn = new GridViewClass.ColumnCheckBox("selectionCheck");
            //    field.HeaderTemplate = headerColumn;
            //}
            //else
            //{
                field.HeaderText = headerText;
                field.FooterText = headerText;
           // }
            field.ItemTemplate = template;
            field.Visible = bVisible;
            field.ItemStyle.HorizontalAlign = align;
            if (sort != null)
            {
                field.SortExpression = sort;
            }
            grid.Columns.Add(field);
        }
        #endregion

        #region BoundFieldの列を追加 ( + 5)
        /// <summary>
        /// BoundFieldの列を追加します。
        /// </summary>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        /// <param name="bVisible">true：表示 / false：非表示</param>
        /// <param name="grid">追加対象のGrid</param>
        public static void AddBoundField(string strHeaderName, string strDataName, bool bVisible, GridView grid)
        {
            AddBoundField(strHeaderName, strDataName, bVisible, grid, null);
        }
        /// <summary>
        /// BoundFieldの列を追加します。
        /// </summary>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        /// <param name="bVisible">true：表示 / false：非表示</param>
        /// <param name="grid">追加対象のGrid</param>
        /// <param name="format">表示フォーマット</param>
        public static void AddBoundField(string strHeaderName, string strDataName, bool bVisible, GridView grid, string format)
        {
            AddBoundField(strHeaderName, strDataName, bVisible, grid, format, (new BoundField()).ItemStyle.HorizontalAlign, false);
        }
        /// <summary>
        /// BoundFieldの列を追加します。
        /// </summary>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        /// <param name="bVisible">true：表示 / false：非表示</param>
        /// <param name="grid">追加対象のGrid</param>
        /// <param name="bSort">true：ソートあり / false：ソートなし</param>
        public static void AddBoundField(string strHeaderName, string strDataName, bool bVisible, GridView grid, bool bSort)
        {
            AddBoundField(strHeaderName, strDataName, bVisible, grid, null, (new BoundField()).ItemStyle.HorizontalAlign , bSort);
        }
        /// <summary>
        /// BoundFieldの列を追加します。
        /// </summary>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        /// <param name="bVisible">true：表示 / false：非表示</param>
        /// <param name="grid">追加対象のGrid</param>
        /// <param name="align">水平方向の配置</param>
        public static void AddBoundField(string strHeaderName, string strDataName, bool bVisible, GridView grid, HorizontalAlign align)
        {
            AddBoundField(strHeaderName, strDataName, bVisible, grid, null, align, false);
        }
        /// <summary>
        /// BoundFieldの列を追加します。
        /// </summary>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        /// <param name="bVisible">true：表示 / false：非表示</param>
        /// <param name="grid">追加対象のGrid</param>
        /// <param name="align">水平方向の配置</param>
        /// <param name="bSort">true：ソートあり / false：ソートなし</param>
        public static void AddBoundField(string strHeaderName, string strDataName, bool bVisible, GridView grid, HorizontalAlign align, bool bSort)
        {
            AddBoundField(strHeaderName, strDataName, bVisible, grid, null, align, bSort);
        }
        /// <summary>
        /// BoundFieldの列を追加します。
        /// </summary>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        /// <param name="bVisible">true：表示 / false：非表示</param>
        /// <param name="grid">追加対象のGrid</param>
        /// <param name="align">水平方向の配置</param>
        /// <param name="bSort">true：ソートあり / false：ソートなし</param>
        public static void AddBoundField(string strHeaderName, string strDataName, bool bVisible, GridView grid, string format, HorizontalAlign align, bool bSort)
        {
            BoundField field = new BoundField();
            field.HeaderText = strHeaderName;
            field.FooterText = strHeaderName;
            field.DataField = strDataName;
            if (format != null)
            {
                field.DataFormatString = format;
            }
            field.ItemStyle.HorizontalAlign = align;
            if (bSort)
            {
                field.SortExpression = strDataName;
            }
            field.Visible = bVisible;   // BoundFieldは、このタイミングでVisiblue=falseを設定すると値が取れなくなる為、注意！
            grid.Columns.Add(field);
        }
        /// <summary>
        /// BoundFieldの列を追加します。
        /// </summary>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        /// <param name="bVisible">true：表示 / false：非表示</param>
        /// <param name="grid">追加対象のGrid</param>
        /// <param name="align">水平方向の配置</param>
        /// <param name="format">表示フォーマット</param>
        public static void AddBoundField(string strHeaderName, string strDataName, bool bVisible, GridView grid, string format, HorizontalAlign align)
        {
            AddBoundField(strHeaderName, strDataName, bVisible, grid, format, align, false);
        }
        #endregion
        
        #region ImageFieldの列を追加
        /// <summary>
        /// ImageFieldの列を追加します。
        /// </summary>
        /// <param name="strHeaderName">列名（表示）</param>
        public static void AddImageField(string strHeaderName, string strDataName, GridView grid)
        {
            ImageField field = new ImageField();
            field.HeaderText = strHeaderName;
            field.FooterText = strHeaderName;
            field.DataImageUrlField = strDataName;
            field.ItemStyle.HorizontalAlign = HorizontalAlign.Center;
            grid.Columns.Add(field);
        }
        #endregion

        #region CheckBoxFieldの列を追加
        /// <summary>
        /// CheckBoxFieldの列を追加します。
        /// </summary>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        public static void AddCheckBoxField(string strHeaderName, string strDataName, bool bVisible, GridView grid)
        {
            CheckBoxField field = new CheckBoxField();
            field.HeaderText = strHeaderName;
            field.FooterText = strHeaderName;
            field.DataField = strDataName;
            field.Visible = bVisible;
            field.ReadOnly = false;
            grid.Columns.Add(field);
        }
        #endregion

        #region HyperLinkFieldの列を追加 ( + 1)
        /// <summary>
        /// HyperLinkFieldの列を追加します。
        /// </summary>
        /// <param name="NavigateDataName">URLにバインドする項目名（複数可）</param>
        /// <param name="DataNavigateUrlFormatString">リンクURL</param>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        /// <param name="bnewDisp">true：新しいWindowで開く / false：現在のWindowで開く</param>
        /// <param name="bVisible">true：列を表示 / false：列を非表示</param>
        /// <param name="grid">グリッド</param>
        public static void AddHyperLinkField(string[] NavigateDataName, 
                                                string DataNavigateUrlFormatString, 
                                                string strHeaderName, 
                                                string strDataName, 
                                                bool bnewDisp,
                                                bool bVisible, 
                                                GridView grid)
        {
            AddHyperLinkField(NavigateDataName, DataNavigateUrlFormatString, strHeaderName, strDataName, bnewDisp,bVisible, grid, false);
        }
        /// <summary>
        /// HyperLinkFieldの列を追加します。
        /// </summary>
        /// <param name="NavigateDataName">URLにバインドする項目名（複数可）</param>
        /// <param name="DataNavigateUrlFormatString">リンクURL</param>
        /// <param name="strHeaderName">列名（表示）</param>
        /// <param name="strDataName">データベース項目名</param>
        /// <param name="bnewDisp">true：新しいWindowで開く / false：現在のWindowで開く</param>
        /// <param name="bVisible">true：列を表示 / false：列を非表示</param>
        /// <param name="grid">グリッド</param>
        /// <param name="bSort">true：ソートあり / false：ソートなし</param>
        public static void AddHyperLinkField(string[] NavigateDataName,
                                                string DataNavigateUrlFormatString,
                                                string strHeaderName,
                                                string strDataName,
                                                bool bnewDisp,
                                                bool bVisible, 
                                                GridView grid,
                                                bool bSort)
        {
            HyperLinkField field = new HyperLinkField();
            field.DataNavigateUrlFields = NavigateDataName;
            field.DataNavigateUrlFormatString = DataNavigateUrlFormatString;
            field.Visible = bVisible;
            field.HeaderText = strHeaderName;
            field.FooterText = strHeaderName;
            field.DataTextField = strDataName;
            if (bnewDisp)
            {
                field.Target = "_blank";
            }
            if (bSort)
            {
                field.SortExpression = strDataName;
            }
            grid.Columns.Add(field);
        }
        #endregion

        #region 指定した列名の列Indexを取得
        /// <summary>
        /// 指定した列名の列Indexを取得
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="headerText">取得したい列のヘッダーテキスト</param>
        /// <returns>列Index</returns>
        public static int getColIndex_FromHeader(GridView grid,  string headerText)
        {
            return (from DataControlField col in grid.Columns
                    where col.HeaderText == headerText
                    select grid.Columns.IndexOf(col)).FirstOrDefault();
        }
        #endregion

        #region チェックされた行の指定した項目の値と行のIndexを取得
        /// <summary>
        /// チェックされた行の指定した項目の値と行のIndexを取得
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="chkName">選択列のコントロール名</param>
        /// <param name="headerText">取得したい列のヘッダーテキスト</param>
        /// <returns>取得した値</returns>
        public static Dictionary<int, string> getCheckedRowValuesWithRowIndex(GridView grid, string chkName, string headerText)
        {
            int columnIndex = getColIndex_FromHeader(grid, headerText);
            Dictionary<int, string> Values = (from GridViewRow row in grid.Rows
                                              where ((CheckBox)(row.FindControl(chkName))).Checked
                                              select new { index = row.RowIndex, value = Common.ConvertHtmlVal(row.Cells[columnIndex].Text, true) }).ToDictionary(a => a.index, b => b.value);
            return Values;
        }
        #endregion

        #region チェックされた行の指定した項目の値を取得
        /// <summary>
        /// チェックされた行の指定した項目の値を取得
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="chkName">選択列のコントロール名</param>
        /// <param name="headerText">取得したい列のヘッダーテキスト</param>
        /// <returns>取得した値</returns>
        public static List<string> getCheckedRowValues(GridView grid, string chkName, string headerText)
        {
            int columnIndex = getColIndex_FromHeader(grid, headerText);
            var Values = (from GridViewRow row in grid.Rows
                          where ((CheckBox)(row.FindControl(chkName))).Checked
                            select row.Cells[columnIndex].Text).ToList();
            for (int i = 0; i < Values.Count(); i++ )
            {
                Values[i] = Common.ConvertHtmlVal(Values[i], true);
            }
            return Values;
        }
        #endregion

        #region チェックされた行のIndexを取得
        /// <summary>
        /// チェックされた行のIndexを取得
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="chkName">選択列のコントロール名</param>
        /// <returns>取得した行Index</returns>
        public static List<int> getCheckedRowIndex(GridView grid, string chkName)
        {
            return (from GridViewRow row in grid.Rows
                          where ((CheckBox)(row.FindControl(chkName))).Checked
                          select row.RowIndex).ToList();
        }
        #endregion

        #region 指定したIndex行の指定した項目の値を取得( +3 )
        /// <summary>
        /// 指定したIndex行の指定した項目の値を取得（１項目の場合）
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="rowIndex">取得したい行Index</param>
        /// <param name="headerText">取得したい列のヘッダーテキスト</param>
        /// <param name="nbspEmpty">
        /// 指定文字列が&nbspの場合、半角スペースか空文字列かを指定します。指定なしは空文字列に変換。true：空文字列 / false：半角文字列
        /// </param>
        /// <returns>取得した値</returns>
        public static string getRowValue(GridView grid, int rowIndex, string headerText, bool nbspEmpty)
        {
            int columnIndex = getColIndex_FromHeader(grid, headerText);
            string value = grid.Rows[rowIndex].Cells[columnIndex].Text;
            return Common.ConvertHtmlVal(value, nbspEmpty);
        }

        /// <summary>
        /// 指定したIndex行の指定した項目の値を取得（１項目の場合）
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="rowIndex">取得したい行Index</param>
        /// <param name="headerText">取得したい列のヘッダーテキスト</param>
        /// <returns>取得した値</returns>
        public static string getRowValue(GridView grid, int rowIndex, string headerText)
        {
            return getRowValue(grid, rowIndex, headerText, true);
        }

        /// <summary>
        /// GridViewの指定したIndex行の指定した項目の値を取得（複数項目の場合）
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="rowIndex">取得したい行Index</param>
        /// <param name="headerText">取得したい列のヘッダーテキスト（複数）</param>
        /// <param name="nbspEmpty">
        /// 指定文字列が&nbspの場合、半角スペースか空文字列かを指定します。指定なしは空文字列に変換。true：空文字列 / false：半角文字列
        /// </param>
        /// <returns>取得した値</returns>
        public static string[] getRowValue(GridView grid, int rowIndex, string[] headerText, bool nbspEmpty)
        {
            ArrayList al = new ArrayList();
            foreach (string head in headerText)
            {
                al.Add(getRowValue(grid, rowIndex, head));
            }

            string[] ret = new string[al.Count];
            al.CopyTo(ret);

            return ret;
        }
        /// <summary>
        /// GridViewの指定したIndex行の指定した項目の値を取得（複数項目の場合）
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="rowIndex">取得したい行Index</param>
        /// <param name="headerText">取得したい列のヘッダーテキスト（複数）</param>
        /// <returns>取得した値</returns>
        public static string[] getRowValue(GridView grid, int rowIndex, string[] headerText)
        {
            return getRowValue(grid, rowIndex, headerText, true);
        }
        #endregion

        #region 変更された行のIDとRowIndexを取得
        /// <summary>
        /// 変更された行のIDとRowIndexを取得
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="checkText">変更を表すCheckBox列名</param>
        /// <param name="headerText">取得したいID列のヘッダーテキスト</param>
        /// <returns></returns>
        public static Dictionary<string, int> getUpdateRows(GridView grid, string checkText, string headerText)
        {
            int columnIndex = getColIndex_FromHeader(grid, headerText);

            Dictionary<string, int> updateRows = (from GridViewRow row in grid.Rows
                                                  where ((CheckBox)(row.FindControl(checkText))).Checked == true
                                                  select new { row.Cells[columnIndex].Text, row.RowIndex }).ToDictionary(row => row.Text, row => row.RowIndex);

            return updateRows;
        }
        #endregion

        #region 指定列の幅を設定します。
        /// <summary>
        /// 指定列の幅を設定します。
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="ColumnIndex">指定列Index</param>
        /// <param name="unit">幅（単位と数値）</param>
        public static void ColumnWidth(GridView grid, int ColumnIndex, Unit unit)
        {
            grid.Columns[ColumnIndex].ItemStyle.Width = unit;
        }
        #endregion

        #region グリッド内の指定した行から指定したコントロールを見つけて表示・非表示を設定します。(+1)
        /// <summary>
        /// グリッド内の指定した行から指定したコントロールを見つけて表示・非表示を設定します。
        /// </summary>
        /// <param name="grid">対象のGirdViewコントロール</param>
        /// <param name="rowIndex">指定した行</param>
        /// <param name="controlId">見つけたいコントロールID</param>
        /// <param name="visible">true：表示 / false：非表示</param>
        public static void ControlVisible(GridView grid, int rowIndex, string controlId, bool visible)
        {
            if (grid.Rows.Count >= rowIndex)
            {
                ControlVisible(grid.Rows[rowIndex], controlId, visible);
            }
        }
        /// <summary>
        /// １行のグリッド行から指定したコントロールを見つけて表示・非表示を設定します。
        /// </summary>
        /// <param name="dr">グリッド行</param>
        /// <param name="controlId">見つけたいコントロールID</param>
        /// <param name="visible">true：表示 / false：非表示</param>
        public static void ControlVisible(GridViewRow dr, string controlId, bool visible)
        {
            Control control = dr.FindControl(controlId);
            if (control != null)
            {
                if (control is TextBox)
                {
                    ((TextBox)control).Visible = visible;
                }
                else if (control is Button)
                {
                    ((Button)control).Visible = visible;
                }
                else if (control is CheckBox)
                {
                    ((CheckBox)control).Visible = visible;
                }
                else if (control is Label)
                {
                    ((Label)control).Visible = visible;
                }
                else if (control is DropDownList)
                {
                    ((DropDownList)control).Visible = visible;
                }
            }
        }

        #endregion
    }
}