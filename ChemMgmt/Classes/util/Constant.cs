﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.Classes.util
{
    public class Constant
    {
        public const string CcrMark = "\r\n";
        /// <summary>
        /// 検索方式：部分一致
        /// </summary>
        public const int CsearchTypeContains = 0;
        //SEARCH_TYPE_CONTAINS
        /// <summary>
        /// 検索方式：前方一致
        /// </summary>
        public const int CsearchTypeStartsWith = 1;
        //cSEARCH_TYPE_STARTSWITH
        /// <summary>
        /// 検索方式：完全一致
        /// </summary>
        public const int CsearchTypeEqualto = 2;
        //cSEARCH_TYPE_EQUALTO
        /// <summary>
        /// 検索方式：後方一致
        /// </summary>
        public const int CsearchTypeEndsWith = 3;
        //cSEARCH_TYPE_ENDSWITH
        /// <summary>
        /// 処理モード：登録
        /// </summary>
        public const int CexecModeIns = 0;
        //cEXEC_MODE_INS
        /// <summary>
        /// 処理モード：更新
        /// </summary>
        public const int CexecModeupdate= 1;
        //cEXEC_MODE_UPD
        /// <summary>
        /// 処理モード：削除
        /// </summary>
        public const int CexecModeDelete = 2;
        //cEXEC_MODE_DEL
        /// <summary>
        /// 処理モード：検索
        /// </summary>
        public const int CexecModeSearch = 3;
        //cEXEC_MODE_SEARCH
        /// <summary>
        /// 処理モード：コピー
        /// </summary>
        public const int CexecModeCopy = 4;
        //cEXEC_MODE_COPY
        /// <summary>
        /// 処理モード：作成　ver.2では仮受入画面で使用。ver.3未使用。
        /// </summary>
        public const int CexecModeCreate = 5;
        //cEXEC_MODE_CREATE
        /// <summary>
        /// 構造式検索方式：Substructure
        /// </summary>
        public const int CstructureSubstructure = 0;
        //cSTRUCTURE_SUBSTRUCTURE
        /// <summary>
        /// 構造式検索方式：Full Structure
        /// </summary>
        public const int CstructureFull = 1;
        //cSTRUCTURE_FULL
        /// <summary>
        /// 構造式検索方式：Exact Structure
        /// </summary>
        public const int CstructureExact = 2;
        //cSTRUCTURE_EXACT
        /// <summary>
        /// 完納フラグ（完了）
        /// </summary>
        public const int CdeliFlagFine = 1;
        //cDELI_FLG_FIN
        /// <summary>
        /// 鍵・重量管理：必須
        /// </summary>
        public const int CmanagementTypeRequired = 0;
        //cMANAGEMENT_TYPE_REQUIRED
        /// <summary>
        /// 鍵・重量管理：推奨
        /// </summary>
        public const int CmanagementTypeRecommendation = 1;
        //cMANAGEMENT_TYPE_RECOMMENDATION
        /// <summary>
        /// 形状：固体
        /// </summary>
        public const int CfigureTypeSolid = 0;
        //cFIGURE_TYPE_SOLID
        /// <summary>
        /// 形状：粉体
        /// </summary>
        public const int CfigureTypePowder = 1;
        //cFIGURE_TYPE_POWDER
        /// <summary>
        /// 形状：液体
        /// </summary>
        public const int CfigureTypeLiquid = 2;
        //cFIGURE_TYPE_LIQUID
        /// <summary>
        /// 形状：気体
        /// </summary>
        public const int CfigureTypeGas = 3;
        //cFIGURE_TYPE_GAS
        /// <summary>
        /// 成分：酸
        /// </summary>
        public const int CcomponentTypeAcid = 0;
        //cCOMPONENT_TYPE_ACID
        /// <summary>
        /// 成分：アルカリ
        /// </summary>
        public const int CcomponentTypeAlkali = 1;
        //cCOMPONENT_TYPE_ALKALI
        /// <summary>
        /// 成分：有機溶剤
        /// </summary>
        public const int CcomponentTypeOrganicSolvene = 2;
        //cCOMPONENT_TYPE_ORGANIC_SOLVENE
        /// <summary>
        /// セッション名：エラー画面へ表示するエラー内容
        /// </summary>
        public const string CsessionNameErrormessage = "ZC01090_MSG";
        //cSESSION_NAME_ERRMSG
        /// <summary>
        /// セッション名：ユーザー情報
        /// </summary>
        public const string CsessionNameUserINFO = "USER_INFO";
        //cSESSION_NAME_USER_INFO
        /// <summary>
        /// エラー画面プログラムID
        /// </summary>
        public const string CerrorProgramId = "ZC01090";
        //cERROR_PROGRAM_ID
        /// <summary>
        /// 相対パス：エラー画面
        /// </summary>
        public const string CpathError = "../ZC010/ZC01090.cshtml";
        //cPATH_ERROR_ASPX
        /// <summary>
        /// 相対パス：ログイン画面
        /// </summary>
        public const string CpathLoginAspx = "../ZC010/ZC01001.cshtml";
        //cPATH_LOGIN_ASPX
        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：Add Cartボタン
        /// </summary>
        public const string CbetweenHeadAddCart = "btnHeader_AddCart";
        //cBTN_HEAD_ADD_CART
        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：Go Cartボタン
        /// </summary>
        public const string CbetweenHeadGoCart = "btnHeader_GoCart";
        //cBTN_HEAD_GO_CART
        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：戻るボタン
        /// </summary>
        public const string CbetweenHeadBack = "btnHeader_Back";
        //cBTN_HEAD_BACK
        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：ログアウトボタン
        /// </summary>
        public const string CbetweenHeadLogOut = "btnHeader_LogOut";
        //cBTN_HEAD_LOG_OUT
        /// <summary>
        /// ユーザーコントロール　ヘッダーボタン名：参照権限ボタン
        /// </summary>
        public const string CbetweenReferenceAuthority = "btnHeader_LegerSearchTarget";
        //cBTN_REFERENCE_AUTHORITY
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン－１
        /// </summary>
        public const string CbetweenFooterm1 = "btnFooter_m1";
        //cBTN_FOOT_m1
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン－２
        /// </summary>
        public const string CbetweenFooterm2 = "btnFooter_m2";
        //cBTN_FOOT_m2
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン－１
        /// </summary>
        public const string CbetweenFooterm3 = "btnFooter_m3";
        //cBTN_FOOT_m3
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン１
        /// </summary>
        public const string CbetweenFooter1 = "btnFooter_1";
        //cBTN_FOOT_1
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン２
        /// </summary>
        public const string CbetweenFooter2 = "btnFooter_2";
        //cBTN_FOOT_2
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン３
        /// </summary>
        public const string CbetweenFooter3 = "btnFooter_3";
        //cBTN_FOOT_3
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン４
        /// </summary>
        public const string CbetweenFooter4 = "btnFooter_4";
        //cBTN_FOOT_4
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン５
        /// </summary>
        public const string CBetweenFooter5 = "btnFooter_5";
        //cBTN_FOOT_5
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン６
        /// </summary>
        public const string CbetweenFooter6 = "btnFooter_6";
        //cBTN_FOOT_6
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン７
        /// </summary>
        public const string CbetweenFooter7 = "btnFooter_7";
        //cBTN_FOOT_7
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン８
        /// </summary>
        public const string CbetweenFooter8 = "btnFooter_8";
        //cBTN_FOOT_8
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン９
        /// </summary>
        public const string CbetweenFooter9 = "btnFooter_9";
        //cBTN_FOOT_9
        /// <summary>
        /// ユーザーコントロール　フッターボタン名：ボタン１０
        /// </summary>
        public const string CbetweenFooter10 = "btnFooter_10";
        //cBTN_FOOT_10
        /// <summary>
        /// ユーザーコントロール　保管場所選択：保管場所選択ボタン
        /// </summary>
        public const string CbetweenLocationSelect = "btnLocationSel";
        //cBTN_LOCATION_SELECT
        /// <summary>
        /// ユーザーコントロール　保管場所選択：クリアボタン
        /// </summary>
        public const string CbetweenLocationClear = "btnLocationClr";
        //cBTN_LOCATION_CLEAR
        /// <summary>
        /// ユーザーコントロール　保管場所選択：入力項目
        /// </summary>
        public const string CtextLocationBarcode = "txtLocation";
        //cTXT_LOCATION_BARCODE
        /// <summary>
        /// T_ORDERテーブルステータス
        /// </summary>
        public enum CorderStatusId             /*cORDER_STATUS_ID*/
        {
            /// <summary>101:ショッピングカート</summary>
            Shopping= 101,
            /// <summary>102:発注申請中</summary>
            OrderApplied,
            //ORDER_APPLIED
            /// <summary>103:承認済</summary>
            OrderApprove,
            //ORDER_APPROVE,
            /// <summary>104:本発注</summary>
            OrderSet,
            //ORDER_SET,
            /// <summary>105:発注確認済</summary>
            //OrderCONF,
            Orderconfirmed,
            /// <summary>106:納品待</summary>
            //DELI_WAIT,
            DeliveryWait,
            /// <summary>107:納品済</summary>
            //DELI_FIN,
            DeliveryFine,
            /// <summary>108:完了</summary>
            //ORDER_FIN,
            OrderFinished,
            /// <summary>109:キャンセル</summary>
            //CANCEL,
            Cancel,
            /// <summary>110:否認</summary>
            //DENIAL,
            Denial,
            /// <summary>111:発注取消し</summary>
            //ORDER_CANCEL,
            OrderCancel,
            /// <summary>112:返品</summary>
            RETURN
        }

        /// <summary>
        /// T_ORDERテーブル注文タイプ
        /// </summary>
        public enum CorderOrderTypes                   /*cORDER_ORDER_TYPE*/
        {
            /// <summary>1:カタログ</summary>
            //CATALOG = 1,
            Catalog=1,
            /// <summary>2:在庫</summary>
            //STOCK,
            Stock,
            /// <summary>3:履歴</summary>
            //HISTORY,
            History,
            /// <summary>4:再発注</summary>
            //REORDER
            Reorder
        }

        /// <summary>
        /// T_STOCKテーブルのステータス
        /// </summary>
        public enum  cStockStatusIds             /*cSTOCK_STATUS_ID*/
        {
            /// <summary>200:納品待</summary>
            //DELI_WAIT = 200,
            DeliveryWait=200,
            /// <summary>201:納品済</summary>
            //DELI_FIN,
            DeliveredFine=200,
            /// <summary>202:受領済</summary>
            //REC_FIN,
            ReceivedFine,
            /// <summary>203:在庫</summary>
            //STOCK,
            Stock,
            /// <summary>204:貸出</summary>
            //BORROW,
            Borrow,
            /// <summary>205:廃棄</summary>
            //DISPOSE,
            Dispose,
            /// <summary>206:使切</summary>
            //EMPTY
            Empty
        }

        /// <summary>
        /// T_ACTION_HISTORYテーブルのACTION_TYPE
        /// </summary>
        public enum CActionHistoryTypes                            /*cACTION_HISTORY_TYPE*/
        {
            /// <summary>1:初回登録</summary>
            //FIRST_ADD = 1,
            First_Add = 1,
            /// <summary>2:貸出</summary>
            //BORROW,
            Borrow,
            /// <summary>3:返却</summary>
            //REPAY,
            Repay,
            /// <summary>4:廃棄</summary>
            //DISPOSE,
            Dispose,
            /// <summary>5:移管</summary>
            //MOVE,
            Move,
            /// <summary>6:使切</summary>
            //EMPTY,
            Empty,
            /// <summary>7:棚卸</summary>
            //INVENTORY
            Inventory
        }

        /// <summary>
        /// ユーザーコントロール（社員選択、法規制選択、保管場所選択）処理モード
        /// </summary>
        public enum CuserSelect                   /*cUSER_SELECT*/
        {
           /* SELECT = 0,*/
            Select=0,        // 選択
           /* CLOSE */    
            Close                 // 閉じる（またはブラウザの×ボタン）
        }

        /// <summary>
        /// 法規制・プロダクト名取得関数(oracle stored function)の引数
        /// </summary>
        public const string cREGULATORY_M_PRODUCT = "0";

        /// <summary>
        /// 法規制・プロダクト名取得関数(oracle stored function)の引数
        /// </summary>
        public const string cREGULATORY_T_STOCK = "1";

        /// <summary>
        /// 法規制・プロダクト名取得関数(oracle stored function)の引数
        /// </summary>
        public const string cREGULATORY_T_ORDER = "2";

        /// <summary>
        /// Modal表示する画面の幅（px）
        /// </summary>
        public const string CmodelDispWidth = "1276";
        //cMODAL_DISP_WIDTH
        /// <summary>
        /// Modal表示する画面の高さ（px）
        /// </summary>
        public const string CmodelDispHeight = "726";
        //cMODAL_DISP_HEIGHT
        /// <summary>
        /// 管理者フラグ：一般
        /// </summary>
        public const string CadminFlagNormal = "0";
        //cADMIN_FLG_NORMAL
        /// <summary>
        /// 管理者フラグ：管理者
        /// </summary>
        public const string CadminFlagAdmin = "1";
        //cADMIN_FLG_ADMIN
        /// <summary>
        /// 参照権限フラグ：一般ユーザー
        /// </summary>
        public const string CreferenceAuthrityFlagNormal = "0";
        //cREFERENCE_AUTHORITY_FLAG_NORMAL
        /// <summary>
        /// 参照権限フラグ：管理者
        /// </summary>
        public const string CreferenceAuthorityFlagAdmin = "1";
        //cREFERENCE_AUTHORITY_FLAG_ADMIN
        /// <summary>
        /// 管理者フラグ：受入
        /// </summary>
        public const string CAdminFlagAccept = "2";
        //cADMIN_FLG_ACCEPT
        /// <summary>
        /// 法規制パターンに対する権限
        /// </summary>
        public enum CgrantTypes                         /*cGRANT_TYPE*/
        {
            /// <summary>0:全権限あり</summary>
            //ADMIN = 0,
            Admin = 0,
            /// <summary>1:発注権限あり</summary>
            //ORDER,
            Order,
            /// <summary>2:使用権限あり</summary>
            //USE
            Use
        }

        /// <summary>
        /// 構造式BASE64の文字数の上限
        /// </summary>
        public const int ClimitBase64Length = 80000;                                   /*cLIMIT_BASE64_LENGTH = 80000;*/

        /// <summary>
        /// ステータスマスタ取得関数(selectStatus)の引数
        /// 1:発注ステータス
        /// </summary>
        public const int CSTATUS_CLASS_ID_T_ORDER = 1;                                   /*cSTATUS_CLASS_ID_T_ORDER = 1;*/

        /// <summary>
        /// ステータスマスタ取得関数(selectStatus)の引数
        /// 2:在庫ステータス
        /// </summary>
        public const int cSTATUS_CLASS_ID_T_STOCK = 2;

        /// <summary>
        /// 保管場所コード：使い切り
        /// </summary>
        public const string ClocBarcodeEmpty = "TKK";               /*cLOC_BARCODE_EMPTY = "TKK";*/

        /// <summary>
        /// 保管場所コード：廃棄
        /// </summary>
        public const string ClocBarcodeDispose = "HIK";               /*cLOC_BARCODE_DISPOSE*/

        /// <summary>
        /// 試薬バーコード：一般消耗品
        /// </summary>
        public const string CbarcodeGeneralConsumables = "SHO";          /* cBARCODE_GENERAL_CONSUMABLES*/

        /// <summary>
        /// 試薬バーコード：共通消耗品
        /// </summary>
        public const string CbarcodeCommonConsumables = "SHM";              /* cBARCODE_COMMON_CONSUMABLES*/

        /// <summary>
        /// 削除フラグ：削除
        /// </summary>
        public const string CdbDeleteFlag = "1";                               /* cDB_DELETE_FLG*/

        /// <summary>
        /// 削除フラグ：削除取消
        /// </summary>
        public const string CdbDeleteFlagInvalid = "0";                           /* cDB_DELETE_FLG_INVALID*/

        /// <summary>
        /// グリッドのハイパーリンクコントロールの文字色
        /// </summary>
        public const int   ChyperLinkColor = 0x0033cc;                                            /*cHYPER_LINK_COLOR*/

        /// <summary>
        /// ユーザーメーカー：新規追加
        /// </summary>
        public const string  CuserMakerAdd = "ADD";                                                  /* cUSER_MAKER_ADD*/

        /// <summary>
        /// ユーザーメーカー：変更
        /// </summary>
        public const string CUserMakerMod = "MOD";                                                     /*cUSER_MAKER_MOD*/

        /// <summary>
        /// 起動確認フラグ
        /// </summary>
        public const string  CstartupFlag= "ON";                                                        /*cSTARTUP_FLG*/

        /// <summary>
        /// カタログテーブルタイプ（M_PRODUCT,M_QUANTITY）
        /// </summary>
        public const int   CcatTblType = 0;                                                                          /*  cCAT_TBL_TYPE*/

        /// <summary>
        /// 検索処理：継続
        /// </summary>
        public const string CsearchOk  = "1";                                           /* cSEARCH_OK*/

        /// <summary>
        /// 警告ダイアログ表示
        /// </summary>
        public const string  CalertDialogDisp = "1";                                                      /*cAlert_Dialog_Disp*/
    }
}