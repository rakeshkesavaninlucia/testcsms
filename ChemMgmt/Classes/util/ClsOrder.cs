﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG.Classes.util;

namespace ZyCoaG.util
{
    /// <summary>
    /// 発注データメソッド群クラス
    /// </summary>
    public class ClsOrder
    {
        #region メンバー
        private string order_no = string.Empty;
        private string status_id = string.Empty;
        private string order_num = string.Empty;
        private string order_user_cd = string.Empty;
        private string room_id = string.Empty;
        private string compound_list_id = string.Empty;
        private string prd_seqno = string.Empty;
        private string qp_seqno = string.Empty;
        private string cat_cd = string.Empty;
        private string grade = string.Empty;
        private string unitsize = string.Empty;
        private string unitsize_id = string.Empty;
        private string maker_id = string.Empty;
        private string product_name_jp = string.Empty;
        private string product_name_en = string.Empty;
        private string product_name_search = string.Empty;
        private string curr = string.Empty;
        private string price = string.Empty;
        private string memo = string.Empty;
        private string project_cd = string.Empty;
        private string order_type = string.Empty;
        private string base_order_no = string.Empty;
        private string base_barcode = string.Empty;
        private string vendor_id = string.Empty;
        private string area_id = string.Empty;
        private string old_order_num = string.Empty;
        private string approver1 = string.Empty;
        private string approver2 = string.Empty;
        private string tel = string.Empty;
        private string email = string.Empty;
        private string preservation_condition = string.Empty;
        private string transport_condition = string.Empty;
        private string kanjyo_id = string.Empty;
        private string order_conf_flag = string.Empty;
        private string reg_date = string.Empty;
        private string upd_date = string.Empty;
        private string del_date = string.Empty;
        private string del_flag = string.Empty;
        private string deliFlag = string.Empty;
        private int intOrderCount = 0;
        #endregion

        #region プロパティ
        public string ORDER_NO { get { return this.order_no; } set { this.order_no = value; } }
        public string STATUS_ID { get { return this.status_id; } set { this.status_id = value; } }
        public string ORDER_NUM { get { return this.order_num; } set { this.order_num = value; } }
        public string ORDER_USER_CD { get { return this.order_user_cd; } set { this.order_user_cd = value; } }
        public string ROOM_ID { get { return this.room_id; } set { this.room_id = value; } }
        public string COMPOUND_LIST_ID { get { return this.compound_list_id; } set { this.compound_list_id = value; } }
        public string PRD_SEQNO { get { return this.prd_seqno; } set { this.prd_seqno = value; } }
        public string QP_SEQNO { get { return this.qp_seqno; } set { this.qp_seqno = value; } }
        public string CAT_CD { get { return this.cat_cd; } set { this.cat_cd = value; } }
        public string GRADE { get { return this.grade; } set { this.grade = value; } }
        public string UNITSIZE { get { return this.unitsize; } set { this.unitsize = value; } }
        public string UNITSIZE_ID { get { return this.unitsize_id; } set { this.unitsize_id = value; } }
        public string MAKER_ID { get { return this.maker_id; } set { this.maker_id = value; } }
        public string PRODUCT_NAME_JP { get { return this.product_name_jp; } set { this.product_name_jp = value; } }
        public string PRODUCT_NAME_EN { get { return this.product_name_en; } set { this.product_name_en = value; } }
        public string PRODUCT_NAME_SEARCH { get { return this.product_name_search; } set { this.product_name_search = value; } }
        public string CURR { get { return this.curr; } set { this.curr = value; } }
        public string PRICE { get { return this.price; } set { this.price = value; } }
        public string MEMO { get { return this.memo; } set { this.memo = value; } }
        public string PROJECT_CD { get { return this.project_cd; } set { this.project_cd = value; } }
        public string ORDER_TYPE { get { return this.order_type; } set { this.order_type = value; } }
        public string BASE_ORDER_NO { get { return this.base_order_no; } set { this.base_order_no = value; } }
        public string BASE_BARCODE { get { return this.base_barcode; } set { this.base_barcode = value; } }
        public string VENDOR_ID { get { return this.vendor_id; } set { this.vendor_id = value; } }
        public string AREA_ID { get { return this.area_id; } set { this.area_id = value; } }
        public string OLD_ORDER_NUM { get { return this.old_order_num; } set { this.old_order_num = value; } }
        public string APPROVER1 { get { return this.approver1; } set { this.approver1 = value; } }
        public string APPROVER2 { get { return this.approver2; } set { this.approver2 = value; } }
        public string TEL { get { return this.tel; } set { this.tel = value; } }
        public string EMAIL { get { return this.email; } set { this.email = value; } }
        public string PRESERVATION_CONDITION { get { return this.preservation_condition; } set { this.preservation_condition = value; } }
        public string TRANSPORT_CONDITION { get { return this.transport_condition; } set { this.transport_condition = value; } }
        public string KANJYO_ID { get { return this.kanjyo_id; } set { this.kanjyo_id = value; } }
        public string ORDER_CONF_FLAG { get { return this.order_conf_flag; } set { this.order_conf_flag = value; } }
        public string REG_DATE { get { return this.reg_date; } set { this.reg_date = value; } }
        public string UPD_DATE { get { return this.upd_date; } set { this.upd_date = value; } }
        public string DEL_DATE { get { return this.del_date; } set { this.del_date = value; } }
        public string DEL_FLAG { get { return this.del_flag; } set { this.del_flag = value; } }
        public string DELI_FLAG { get { return this.deliFlag; } set { this.deliFlag = value; } }
        public int ORDER_COUNT { get { return this.intOrderCount; } }
        #endregion

        #region 検索系処理
        /// <summary>
        /// T_ORDER.ORDER_NOをキーにデータを読み込みます
        /// </summary>
        /// <param name="orderNo"></param>
        public void GetOrderData(string orderNo)
        {
            string strSQL = "";
            Hashtable parm = new Hashtable();

            strSQL = " SELECT";
            strSQL += "    O.ORDER_NO";
            strSQL += "    , O.STATUS_ID";
            strSQL += "    , O.ORDER_NUM";
            strSQL += "    , O.ORDER_USER_CD";
            strSQL += "    , O.ROOM_ID";
            strSQL += "    , O.COMPOUND_LIST_ID";
            strSQL += "    , O.PRD_SEQNO";
            strSQL += "    , O.QP_SEQNO";
            strSQL += "    , O.CAT_CD";
            strSQL += "    , O.GRADE";
            strSQL += "    , O.UNITSIZE";
            strSQL += "    , O.UNITSIZE_ID";
            strSQL += "    , O.MAKER_ID";
            strSQL += "    , O.PRODUCT_NAME_JP";
            strSQL += "    , O.PRODUCT_NAME_EN";
            strSQL += "    , O.PRODUCT_NAME_SEARCH";
            strSQL += "    , O.CURR";
            strSQL += "    , O.PRICE";
            strSQL += "    , O.MEMO";
            strSQL += "    , O.PROJECT_CD";
            strSQL += "    , O.ORDER_TYPE";
            strSQL += "    , O.BASE_ORDER_NO";
            strSQL += "    , O.BASE_BARCODE";
            strSQL += "    , O.VENDOR_ID";
            strSQL += "    , O.AREA_ID";
            strSQL += "    , O.OLD_ORDER_NUM";
            strSQL += "    , O.APPROVER1";
            strSQL += "    , O.APPROVER2";
            strSQL += "    , O.TEL";
            strSQL += "    , O.EMAIL";
            strSQL += "    , O.PRESERVATION_CONDITION";
            strSQL += "    , O.TRANSPORT_CONDITION";
            strSQL += "    , O.KANJYO_ID";
            strSQL += "    , O.ORDER_CONF_FLAG";
            strSQL += "    , O.REG_DATE";
            strSQL += "    , O.UPD_DATE";
            strSQL += "    , O.DEL_DATE";
            strSQL += "    , O.DEL_FLAG";
            strSQL += "    , O.DELI_FLAG";
            strSQL += " FROM";
            strSQL += "    T_ORDER O";
            strSQL += " WHERE";
            strSQL += "    O.ORDER_NO = :ORDER_NO";
            strSQL += " AND";
            strSQL += "    O.DEL_FLAG = 0";

            parm["ORDER_NO"] = orderNo;

            //検索
            DataTable dt = DbUtil.Select(strSQL, parm);

            if (dt.Rows.Count == 0) return;

            setupResult(dt);
        }

        /// <summary>
        /// 検索結果をメンバに設定します
        /// </summary>
        /// <param name="dt"></param>
        private void setupResult(DataTable result)
        {
            DataRow row = result.Rows[0];

            this.order_no = Convert.ToString(row["ORDER_NO"]);
            this.status_id = Convert.ToString(row["STATUS_ID"]);
            this.order_num = Convert.ToString(row["ORDER_NUM"]);
            this.order_user_cd = Convert.ToString(row["ORDER_USER_CD"]);
            this.room_id = Convert.ToString(row["ROOM_ID"]);
            this.compound_list_id = Convert.ToString(row["COMPOUND_LIST_ID"]);
            this.prd_seqno = Convert.ToString(row["PRD_SEQNO"]);
            this.qp_seqno = Convert.ToString(row["QP_SEQNO"]);
            this.cat_cd = Convert.ToString(row["CAT_CD"]);
            this.grade = Convert.ToString(row["GRADE"]);
            this.unitsize = Convert.ToString(row["UNITSIZE"]);
            this.unitsize_id = Convert.ToString(row["UNITSIZE_ID"]);
            this.maker_id = Convert.ToString(row["MAKER_ID"]);
            this.product_name_jp = Convert.ToString(row["PRODUCT_NAME_JP"]);
            this.product_name_en = Convert.ToString(row["PRODUCT_NAME_EN"]);
            this.product_name_search = Convert.ToString(row["PRODUCT_NAME_SEARCH"]);
            this.curr = Convert.ToString(row["CURR"]);
            this.price = Convert.ToString(row["PRICE"]);
            this.memo = Convert.ToString(row["MEMO"]);
            this.project_cd = Convert.ToString(row["PROJECT_CD"]);
            this.order_type = Convert.ToString(row["ORDER_TYPE"]);
            this.base_order_no = Convert.ToString(row["BASE_ORDER_NO"]);
            this.base_barcode = Convert.ToString(row["BASE_BARCODE"]);
            this.vendor_id = Convert.ToString(row["VENDOR_ID"]);
            this.area_id = Convert.ToString(row["AREA_ID"]);
            this.old_order_num = Convert.ToString(row["OLD_ORDER_NUM"]);
            this.approver1 = Convert.ToString(row["APPROVER1"]);
            this.approver2 = Convert.ToString(row["APPROVER2"]);
            this.tel = Convert.ToString(row["TEL"]);
            this.email = Convert.ToString(row["EMAIL"]);
            this.preservation_condition = Convert.ToString(row["PRESERVATION_CONDITION"]);
            this.transport_condition = Convert.ToString(row["TRANSPORT_CONDITION"]);
            this.kanjyo_id = Convert.ToString(row["KANJYO_ID"]);
            this.order_conf_flag = Convert.ToString(row["ORDER_CONF_FLAG"]);
            this.reg_date = Convert.ToString(row["REG_DATE"]);
            this.upd_date = Convert.ToString(row["UPD_DATE"]);
            this.del_date = Convert.ToString(row["DEL_DATE"]);
            this.del_flag = Convert.ToString(row["DEL_FLAG"]);
            this.deliFlag = Convert.ToString(row["DELI_FLAG"]);
            this.intOrderCount = result.Rows.Count;
        }
        #endregion

        #region 受領画面更新処理
        /// <summary>
        /// T_ORDER.ORDER_NOをキーに注文データを更新します
        /// </summary>
        /// <param name="context">DbConnection</param>
        /// <returns>更新件数</returns>
        public void UpOrder_Receive(DbContext context)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql = new StringBuilder();
            sbSql.Append("UPDATE ");
            sbSql.Append("   T_ORDER ");
            sbSql.Append(" SET ");
            sbSql.Append("   PROJECT_CD = :PROJECT_CD");
            sbSql.Append("  ,KANJYO_ID = :KANJYO_ID");
            sbSql.Append("  ,STATUS_ID = :STATUS_ID");
            sbSql.Append(" WHERE");
            sbSql.Append("    ORDER_NO = :ORDER_NO");

            Hashtable param = new Hashtable();
            param = new Hashtable();
            param.Add("PROJECT_CD", this.project_cd);
            param.Add("KANJYO_ID", this.kanjyo_id);
            param.Add("STATUS_ID", this.status_id);
            param.Add("ORDER_NO", this.order_no);

            DbUtil.ExecuteUpdate(context, sbSql.ToString(), param);
        }
        #endregion
    }
}