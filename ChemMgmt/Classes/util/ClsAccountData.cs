﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG.Classes.util;

namespace ZyCoaG.util
{
    /// <summary>
    /// 会計データメソッド群クラス
    /// </summary>
    public class ClsAccountData
    {
        #region メンバー
        private string accountId = "";
        private string order_no = "";
        private string deliNum = "";
        private string deliFlag = "";
        private string price = "";
        private string orderUserName = "";
        private string catCd = "";
        private string grade = "";
        private string actualPrice = "";
        private string acceptFlag = "";
        private string memo = "";
        private string regDate = "";
        private string updDate = "";
        private string delDate = "";
        private string delFlag = "";
        //private DataTable orderAccountList;
        private int intAccountCount = 0;
        #endregion

        #region プロパティ
        public string ACCOUNT_ID { get { return this.accountId; } set { this.accountId = value; } }
        public string ORDER_NO { get { return this.order_no; } set { this.order_no = value; } }
        public string DELI_NUM { get { return this.deliNum; } set { this.deliNum = value; } }
        public string DELI_FLAG { get { return this.deliFlag; } set { this.deliFlag = value; } }
        public string PRICE { get { return this.price; } set { this.price = value; } }
        public string ORDER_USER_NAME { get { return this.orderUserName; } set { this.orderUserName = value; } }
        public string CAT_CD { get { return this.catCd; } set { this.catCd = value; } }
        public string GRADE { get { return this.grade; } set { this.grade = value; } }
        public string ACTUAL_PRICE { get { return this.actualPrice; } set { this.actualPrice = value; } }
        public string ACCEPT_FLAG { get { return this.acceptFlag; } set { this.acceptFlag = value; } }
        public string MEMO { get { return this.memo; } set { this.memo = value; } }
        public string REG_DATE { get { return this.regDate; } set { this.regDate = value; } }
        public string UPD_DATE { get { return this.updDate; } set { this.updDate = value; } }
        public string DEL_DATE { get { return this.delDate; } set { this.delDate = value; } }
        public string DEL_FLAG { get { return this.delFlag; } set { this.delFlag = value; } }
        //public DataTable ORDER_ACCOUNT_LIST { get { return this.orderAccountList; } set { this.orderAccountList = value; } }
        #endregion

        #region 検索系処理
        /// <summary>
        /// T_ACCOUNT_DATA.ORDER_NOをキーにデータを読み込みます
        /// </summary>
        /// <param name="orderNo">注文番号</param>
        public void GetAccountData(string orderNo)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" SELECT ");
            sbSql.Append("  ACCOUNT_ID ");
            sbSql.Append(" ,ORDER_NO ");
            sbSql.Append(" ,DELI_NUM ");
            sbSql.Append(" ,DELI_FLAG ");
            sbSql.Append(" ,PRICE ");
            sbSql.Append(" ,ORDER_USER_NAME ");
            sbSql.Append(" ,CAT_CD ");
            sbSql.Append(" ,GRADE ");
            sbSql.Append(" ,ACTUAL_PRICE ");
            sbSql.Append(" ,ACCEPT_FLAG ");
            sbSql.Append(" ,MEMO ");
            sbSql.Append(" ,REG_DATE ");
            sbSql.Append(" ,UPD_DATE ");
            sbSql.Append(" ,DEL_DATE ");
            sbSql.Append(" ,DEL_FLAG ");
            sbSql.Append(" FROM ");
            sbSql.Append("  T_ACCOUNT_DATA ");
            sbSql.Append(" WHERE ");
            sbSql.Append("   ORDER_NO = :ORDER_NO ");
            sbSql.Append(" AND ");
            sbSql.Append("   DEL_FLAG <> 1 ");

            Hashtable param = new Hashtable();
            param["ORDER_NO"] = orderNo;

            DataTable dtTable = DbUtil.Select(sbSql.ToString(), param);

            if (dtTable.Rows.Count == 0) return;

            setupResult(dtTable);
        }

        /// <summary>
        /// 会計データを取得する。（受領画面のListBox表示用））
        /// </summary>
        /// <param name="orderNo">注文番号</param>
        /// <returns>会計データ</returns>
        public DataTable GetAccountDataList(string orderNo)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" SELECT ");
            sbSql.Append("  DELI_NUM ");
            sbSql.Append(" ,CURR || PRICE PRICE ");
            sbSql.Append(" ,CURR || ACTUAL_PRICE ACTUAL_PRICE ");
            sbSql.Append(" ,TO_CHAR( REG_DATE,'YYYY/MM/DD') REG_DATE ");
            sbSql.Append(" ,REG_DATE REG_DATE2 ");
            sbSql.Append(" FROM ");
            sbSql.Append("  T_ACCOUNT_DATA ");
            sbSql.Append(" WHERE ");
            sbSql.Append("  ORDER_NO = :ORDER_NO ");
            sbSql.Append(" AND ");
            sbSql.Append("  DEL_FLAG <> 1 ");
            sbSql.Append(" ORDER BY REG_DATE2 ");

            Hashtable param = new Hashtable();
            param["ORDER_NO"] = orderNo;

            return DbUtil.Select(sbSql.ToString(), param);
        }

        /// <summary>
        /// T_ACCOUNT_DATA.ORDER_NOをキーに最大単価を取得する
        /// </summary>
        /// <param name="orderNo">注文番号</param>
        /// <returns name="strMaxPrice">最大単価</returns>
        public string GetMaxPrice(string orderNo)
        {
            string strMaxPrice = "";
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" SELECT ");
            sbSql.Append("  MAX(NVL(PRICE,0)) MAXPRICE ");
            sbSql.Append(" FROM ");
            sbSql.Append("  T_ACCOUNT_DATA ");
            sbSql.Append(" WHERE ");
            sbSql.Append("  ORDER_NO = :ORDER_NO ");
            sbSql.Append(" AND ");
            sbSql.Append("  DEL_FLAG <> 1 ");

            Hashtable param = new Hashtable();
            param["ORDER_NO"] = orderNo;

            DataTable dtTable = DbUtil.Select(sbSql.ToString(), param);
            if (dtTable.Rows.Count > 0)
            {
                strMaxPrice = Convert.ToString(dtTable.Rows[0]["MAXPRICE"]);
            }
            return strMaxPrice;
        }

        /// <summary>
        /// 検索結果をメンバに設定します
        /// </summary>
        /// <param name="dt"></param>
        private void setupResult(DataTable result)
        {
            DataRow row = result.Rows[0];
            this.accountId = row["ACCOUNT_ID"].ToString();
            this.order_no = row["ORDER_NO"].ToString();
            this.deliNum = row["DELI_NUM"].ToString();
            this.deliFlag = row["DELI_FLAG"].ToString();
            this.price = row["PRICE"].ToString();
            this.orderUserName = row["ORDER_USER_NAME"].ToString();
            this.catCd = row["CAT_CD"].ToString();
            this.grade = row["GRADE"].ToString();
            this.actualPrice = row["ACTUAL_PRICE"].ToString();
            this.acceptFlag = row["ACCEPT_FLAG"].ToString();
            this.memo = row["MEMO"].ToString();
            this.regDate = row["REG_DATE"].ToString();
            this.updDate = row["UPD_DATE"].ToString();
            this.delDate = row["DEL_DATE"].ToString();
            this.delFlag = row["DEL_FLAG"].ToString();

            this.intAccountCount = result.Rows.Count;
        }
        #endregion
    }
}