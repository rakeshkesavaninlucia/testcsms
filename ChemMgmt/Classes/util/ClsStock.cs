﻿using ChemMgmt.Models;
using ChemMgmt.Nikon;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG.Classes.util;

namespace ZyCoaG.util
{
    public class ClsStock
    {
        #region メンバー
        private string stockId = "";
        private string barcode = "";
        private string statusId = "";
        private string owner = "";
        private string compoundListId = "";
        private string catCd = "";
        private string unitsize = "";
        private string unitsizeId = "";
        private string productNameJp = "";
        private string productNameEn = "";
        private string productNameSearch = "";
        private string grade = "";
        private string areaId = "";
        private string makerId = "";
        private string borrower = "";
        private string brwDate = "";
        private string orderNo = "";
        private string locId = "";
        private string limitDate = "";
        private string manageWeight = "";
        private string vendorId = "";
        private string iniGrossWeightG = "";
        private string curGrossWeightG = "";
        private string bottleWeightG = "";
        private string memo = "";
        private string preservationCondition = "";
        private string transportCondition = "";
        private string regDate = "";
        private string updDate = "";
        private string delDate = "";
        private string delFlag = "";
        private string deliDate = "";
        private string finePoisonFlag = "";
        private string firstLocId = "";
        private string delMailDate = "";
        private string accountId = "";
        private string lastUpdDate = "";

        private string statusText = "";
        private string locName = "";
        private string orderUserName = "";
        private string makerName = "";
        private string unitSizeName = "";
        private string projectCd = "";
        private string kanjyoId = "";
        private string locBarcode = "";
        private string groupId = "";
        private string locCheckFlag = "";
        private string vendorOrderNo = "";
        private string deliflag = "";
        private long lngSameOrderStockCount = 0;
        private long lngSameOrderBarcodeCount = 0;
        // 2018/12/11 Add Start TPE Kometani
        private string actionUserNm = "";
        private string actionGroupName = "";
        private string actionGroupCd = "";
        // 2018/12/11 Add End TPE Kometani

        private int intStockCount = 0;
        #endregion

        #region プロパティ
        /// <summary>在庫管理番号</summary>
        public string STOCK_ID { get { return this.stockId; } set { this.stockId = value; } }
        /// <summary>バーコード番号</summary>
        public string BARCODE { get { return this.barcode; } set { this.barcode = value; } }
        /// <summary>ステータス</summary>
        public string STATUS_ID { get { return this.statusId; } set { this.statusId = value; } }
        /// <summary>オーナー（所有者）</summary>
        public string OWNER { get { return this.owner; } set { this.owner = value; } }
        /// <summary>化合物リストID</summary>
        public string COMPOUND_LIST_ID { get { return this.compoundListId; } set { this.compoundListId = value; } }
        /// <summary>カタログコード</summary>
        public string CAT_CD { get { return this.catCd; } set { this.catCd = value; } }
        /// <summary>容量（数値or文字列の両方を可能とする）</summary>
        public string UNITSIZE { get { return this.unitsize; } set { this.unitsize = value; } }
        /// <summary>単位（容量が数値の場合に使用する）</summary>
        public string UNITSIZE_ID { get { return this.unitsizeId; } set { this.unitsizeId = value; } }
        /// <summary>化学物質名（和名）</summary>
        public string PRODUCT_NAME_JP { get { return this.productNameJp; } set { this.productNameJp = value; } }
        /// <summary>化学物質名（英名）</summary>
        public string PRODUCT_NAME_EN { get { return this.productNameEn; } set { this.productNameEn = value; } }
        /// <summary>化学物質名検索用</summary>
        public string PRODUCT_NAME_SEARCH { get { return this.productNameSearch; } set { this.productNameSearch = value; } }
        /// <summary>規格</summary>
        public string GRADE { get { return this.grade; } set { this.grade = value; } }
        /// <summary>地域ID</summary>
        public string AREA_ID { get { return this.areaId; } set { this.areaId = value; } }
        /// <summary>メーカーID</summary>
        public string MAKER_ID { get { return this.makerId; } set { this.makerId = value; } }
        /// <summary>貸出中のユーザーID（返却後クリア）</summary>
        public string BORROWER { get { return this.borrower; } set { this.borrower = value; } }
        /// <summary>貸出日時（返却後クリア）</summary>
        public string BRW_DATE { get { return this.brwDate; } set { this.brwDate = value; } }
        /// <summary>注文番号</summary>
        public string ORDER_NO { get { return this.orderNo; } set { this.orderNo = value; } }
        /// <summary>保管場所ID</summary>
        public string LOC_ID { get { return this.locId; } set { this.locId = value; } }
        /// <summary>使用期限</summary>
        public string LIMIT_DATE { get { return this.limitDate; } set { this.limitDate = value; } }
        /// <summary>残量管理（0:残量管理非対称　1:残量管理対象）</summary>
        public string MANAGE_WEIGHT { get { return this.manageWeight; } set { this.manageWeight = value; } }
        /// <summary>代理店コード</summary>
        public string VENDOR_ID { get { return this.vendorId; } set { this.vendorId = value; } }
        /// <summary>試薬登録時の初期重量（風袋含む）単位はg</summary>
        public string INI_GROSS_WEIGHT_G { get { return this.iniGrossWeightG; } set { this.iniGrossWeightG = value; } }
        /// <summary>現時点の重量（風袋含む）単位はg</summary>
        public string CUR_GROSS_WEIGHT_G { get { return this.curGrossWeightG; } set { this.curGrossWeightG = value; } }
        /// <summary>風袋重量　単位はg</summary>
        public string BOTTLE_WEIGHT_G { get { return this.bottleWeightG; } set { this.bottleWeightG = value; } }
        /// <summary>備考</summary>
        public string MEMO { get { return this.memo; } set { this.memo = value; } }
        /// <summary>保存条件</summary>
        public string PRESERVATION_CONDITION { get { return this.preservationCondition; } set { this.preservationCondition = value; } }
        /// <summary>輸送条件</summary>
        public string TRANSPORT_CONDITION { get { return this.transportCondition; } set { this.transportCondition = value; } }
        /// <summary>登録日時</summary>
        public string REG_DATE { get { return this.regDate; } set { this.regDate = value; } }
        /// <summary>更新日時</summary>
        public string UPD_DATE { get { return this.updDate; } set { this.updDate = value; } }
        /// <summary>削除日時</summary>
        public string DEL_DATE { get { return this.delDate; } set { this.delDate = value; } }
        /// <summary>削除フラグ（1:削除）</summary>
        public string DEL_FLAG { get { return this.delFlag; } set { this.delFlag = value; } }
        /// <summary>納品日</summary>
        public string DELI_DATE { get { return this.deliDate; } set { this.deliDate = value; } }
        /// <summary>微毒フラグ</summary>
        public string FINE_POISON_FLAG { get { return this.finePoisonFlag; } set { this.finePoisonFlag = value; } }
        /// <summary>初回保管場所ID</summary>
        public string FIRST_LOC_ID { get { return this.firstLocId; } set { this.firstLocId = value; } }
        /// <summary>廃棄メール送信日</summary>
        public string DEL_MAIL_DATE { get { return this.delMailDate; } set { this.delMailDate = value; } }
        /// <summary>T_ACCOUNT_DATA.ACCOUNT_ID</summary>
        public string ACCOUNT_ID { get { return this.accountId; } set { this.accountId = value; } }
        /// <summary>最終所有（使用）日付</summary>
        public string LAST_UPD_DATE { get { return this.lastUpdDate; } set { this.lastUpdDate = value; } }

        /// <summary>ステータステキスト</summary>
        public string STATUS_TEXT { get { return this.statusText; } set { this.statusText = value; } }
        /// <summary>保管場所名称</summary>
        public string LOC_NAME { get { return this.locName; } set { this.locName = value; } }
        /// <summary>注文者名</summary>
        public string ORDER_USER_NAME { get { return this.orderUserName; } set { this.orderUserName = value; } }
        /// <summary>メーカー名</summary>
        public string MAKER_NAME { get { return this.makerName; } set { this.makerName = value; } }
        /// <summary>単位名</summary>
        public string UNIT_SIZE_NAME { get { return this.unitSizeName; } set { this.unitSizeName = value; } }
        /// <summary>指図コード</summary>
        public string PROJECT_CD { get { return this.projectCd; } set { this.projectCd = value; } }
        /// <summary>勘定科目ID</summary>
        public string KANJYO_ID { get { return this.kanjyoId; } set { this.kanjyoId = value; } }
        /// <summary>保管場所コード</summary>
        public string LOC_BARCODE { get { return this.locBarcode; } set { this.locBarcode = value; } }
        /// <summary>グループID</summary>
        public string GROUP_ID { get { return this.groupId; } set { this.groupId = value; } }
        /// <summary>保管場所登録必須フラグ</summary>
        public string LOCATION_CHECK_FLAG { get { return this.locCheckFlag; } set { this.locCheckFlag = value; } }
        /// <summary>代理店注文番号</summary>
        public string VENDOR_ORDER_NO { get { return this.vendorOrderNo; } set { this.vendorOrderNo = value; } }
        /// <summary>発注データ：完納フラグ</summary>
        public string DELI_FLAG { get { return this.deliflag; } set { this.deliflag = value; } }
        /// <summary>同一発注番号の在庫データ数</summary>
        public long SAME_ORDER_STOCK_COUNT { get { return this.lngSameOrderStockCount; } set { this.lngSameOrderStockCount = value; } }
        /// <summary>同一発注番号で試薬バーコード入力済の在庫データ数</summary>
        public long SAME_ORDER_BARCODE_COUNT { get { return this.lngSameOrderBarcodeCount; } set { this.lngSameOrderBarcodeCount = value; } }

        public int STOCK_COUNT { get { return this.intStockCount; } }
        // 2018/12/11 Add Start TPE Kometani
        /// <summary>使用履歴用ユーザー名</summary>
        public string ACTION_USER_NM { get { return this.actionUserNm; } set { this.actionUserNm = value; } }
        /// <summary>使用履歴用グループ名</summary>
        public string ACTION_GROUP_NAME { get { return this.actionGroupName; } set { this.actionGroupName = value; } }
        /// <summary>使用履歴用グループコード</summary>
        public string ACTION_GROUP_CD { get { return this.actionGroupCd; } set { this.actionUserNm = value; } }
        // 2018/12/11 Add End TPE Kometani
        #endregion

        #region 検索系処理
        /// <summary>
        /// T_STOCK.BARCODEをキーにデータを読み込みます
        /// </summary>
        /// <param name="barcode">試薬バーコード</param>
        public void GetStockData(string barcode)
        {
            string strSQL = "";
            Hashtable parm = new Hashtable();

            strSQL = " SELECT";
            strSQL += "    TS.STOCK_ID";
            strSQL += "    , TS.BARCODE";
            strSQL += "    , TS.STATUS_ID";
            strSQL += "    , TS.OWNER";
            strSQL += "    , TS.COMPOUND_LIST_ID";
            strSQL += "    , TS.CAT_CD";
            strSQL += "    , TS.UNITSIZE";
            strSQL += "    , TS.UNITSIZE_ID";
            strSQL += "    , TS.PRODUCT_NAME_JP";
            strSQL += "    , TS.PRODUCT_NAME_EN";
            strSQL += "    , TS.PRODUCT_NAME_SEARCH";
            strSQL += "    , TS.GRADE";
            strSQL += "    , TS.AREA_ID";
            strSQL += "    , TS.MAKER_ID";
            strSQL += "    , TS.BORROWER";
            strSQL += "    , TS.BRW_DATE";
            strSQL += "    , TS.ORDER_NO";
            strSQL += "    , TS.LOC_ID";
            strSQL += "    , TS.LIMIT_DATE";
            strSQL += "    , TS.MANAGE_WEIGHT";
            strSQL += "    , TS.VENDOR_ID";
            strSQL += "    , TS.INI_GROSS_WEIGHT_G";
            strSQL += "    , TS.CUR_GROSS_WEIGHT_G";
            strSQL += "    , TS.BOTTLE_WEIGHT_G";
            strSQL += "    , TS.MEMO";
            strSQL += "    , TS.PRESERVATION_CONDITION";
            strSQL += "    , TS.TRANSPORT_CONDITION";
            strSQL += "    , TS.REG_DATE";
            strSQL += "    , TS.UPD_DATE";
            strSQL += "    , TS.DEL_DATE";
            strSQL += "    , TS.DEL_FLAG";
            strSQL += "    , TS.DELI_DATE";
            strSQL += "    , TS.FINE_POISON_FLAG";
            strSQL += "    , TS.FIRST_LOC_ID";
            strSQL += "    , TS.DEL_MAIL_DATE";
            strSQL += "    , TS.ACCOUNT_ID";
            strSQL += "    , TS.LAST_UPD_DATE";
            strSQL += "    , O.VENDOR_ORDER_NO";
            strSQL += "    , O.PROJECT_CD";
            strSQL += "    , O.KANJYO_ID";
            strSQL += "    , ML.LOC_NAME";
            strSQL += "    , ML.LOC_BARCODE";
            strSQL += "    , MS.STATUS_TEXT";
            strSQL += "    , MU.USER_NAME ORDER_USER_NAME";
            strSQL += "    , MU.GROUP_ID";
            strSQL += "    , MM.MAKER_NAME";
            strSQL += "    , MUNIT.UNIT_NAME UNIT_SIZE_NAME";
            strSQL += "    , (ZYCOAG_SYSTEM.GetToLocation_Chk_Flg(TS.COMPOUND_LIST_ID)) LOC_CHECK_FLAG";
            strSQL += " FROM ";
            strSQL += "      T_STOCK TS";
            // 別名を「TO」にするとなぜかORAエラーが発生する為「O」に設定
            strSQL += "    , T_ORDER O";
            strSQL += "    , M_LOCATION ML";
            strSQL += "    , M_STATUS MS";
            strSQL += "    , M_USER MU";
            strSQL += "    , M_MAKER MM";
            strSQL += "    , M_UNITSIZE MUNIT";
            strSQL += " WHERE";
            strSQL += "    TS.DEL_FLAG = 0";
            strSQL += " AND";
            strSQL += "    UPPER(TS.BARCODE) = UPPER(:BARCODE)";
            strSQL += " AND";
            strSQL += "    TS.LOC_ID         = ML.LOC_ID(+)";
            strSQL += " AND";
            strSQL += "    TS.STATUS_ID      = MS.STATUS_ID(+)";
            strSQL += " AND";
            strSQL += "    TS.ORDER_NO       = O.ORDER_NO(+)";
            strSQL += " AND";
            strSQL += "    O.ORDER_USER_CD   = MU.USER_CD(+)";
            strSQL += " AND";
            strSQL += "    TS.MAKER_ID       = MM.MAKER_ID(+)";
            strSQL += " AND";
            strSQL += "    TS.UNITSIZE_ID    = MUNIT.UNITSIZE_ID(+)";

            parm["BARCODE"] = barcode;

            //検索
            DataTable dt = DbUtil.Select(strSQL, parm);

            if (dt.Rows.Count == 0) return;

            setupResult(dt);
        }

        /// <summary>
        /// T_STOCK.STOCK_IDをキーにデータを読み込みます
        /// </summary>
        /// <param name="stockId">STOCK_ID</param>
        /// <returns>取得結果</returns>
        public void GetStockDataByStockId(string stockId)
        {
            string strSQL = "";
            Hashtable parm = new Hashtable();

            strSQL = " SELECT";
            strSQL += "    TS.STOCK_ID";
            strSQL += "    , TS.BARCODE";
            strSQL += "    , TS.STATUS_ID";
            strSQL += "    , TS.OWNER";
            strSQL += "    , TS.COMPOUND_LIST_ID";
            strSQL += "    , TS.CAT_CD";
            strSQL += "    , TS.UNITSIZE";
            strSQL += "    , TS.UNITSIZE_ID";
            strSQL += "    , TS.PRODUCT_NAME_JP";
            strSQL += "    , TS.PRODUCT_NAME_EN";
            strSQL += "    , TS.PRODUCT_NAME_SEARCH";
            strSQL += "    , TS.GRADE";
            strSQL += "    , TS.AREA_ID";
            strSQL += "    , TS.MAKER_ID";
            strSQL += "    , TS.BORROWER";
            strSQL += "    , TS.BRW_DATE";
            strSQL += "    , TS.ORDER_NO";
            strSQL += "    , TS.LOC_ID";
            strSQL += "    , TS.LIMIT_DATE";
            strSQL += "    , TS.MANAGE_WEIGHT";
            strSQL += "    , TS.VENDOR_ID";
            strSQL += "    , TS.INI_GROSS_WEIGHT_G";
            strSQL += "    , TS.CUR_GROSS_WEIGHT_G";
            strSQL += "    , TS.BOTTLE_WEIGHT_G";
            strSQL += "    , TS.MEMO";
            strSQL += "    , TS.PRESERVATION_CONDITION";
            strSQL += "    , TS.TRANSPORT_CONDITION";
            strSQL += "    , TS.REG_DATE";
            strSQL += "    , TS.UPD_DATE";
            strSQL += "    , TS.DEL_DATE";
            strSQL += "    , TS.DEL_FLAG";
            strSQL += "    , TS.DELI_DATE";
            strSQL += "    , TS.FINE_POISON_FLAG";
            strSQL += "    , TS.FIRST_LOC_ID";
            strSQL += "    , TS.DEL_MAIL_DATE";
            strSQL += "    , TS.ACCOUNT_ID";
            strSQL += "    , TS.LAST_UPD_DATE";
            strSQL += "    , O.VENDOR_ORDER_NO";
            strSQL += "    , O.PROJECT_CD";
            strSQL += "    , O.KANJYO_ID";
            strSQL += "    , ML.LOC_NAME";
            strSQL += "    , ML.LOC_BARCODE";
            strSQL += "    , MS.STATUS_TEXT";
            strSQL += "    , MU.USER_NAME ORDER_USER_NAME";
            strSQL += "    , MU.GROUP_ID";
            strSQL += "    , MM.MAKER_NAME";
            strSQL += "    , MUNIT.UNIT_NAME UNIT_SIZE_NAME";
            strSQL += "    , (ZYCOAG_SYSTEM.GetToLocation_Chk_Flg(TS.COMPOUND_LIST_ID)) LOC_CHECK_FLAG";
            strSQL += " FROM ";
            strSQL += "      T_STOCK TS";
            // 別名を「TO」にするとなぜかORAエラーが発生する為「O」に設定
            strSQL += "    , T_ORDER O";
            strSQL += "    , M_LOCATION ML";
            strSQL += "    , M_STATUS MS";
            strSQL += "    , M_USER MU";
            strSQL += "    , M_MAKER MM";
            strSQL += "    , M_UNITSIZE MUNIT";
            strSQL += " WHERE";
            strSQL += "    TS.DEL_FLAG <> 1";
            strSQL += " AND";
            strSQL += "    TS.STOCK_ID       = :STOCK_ID";
            strSQL += " AND";
            strSQL += "    TS.LOC_ID         = ML.LOC_ID(+)";
            strSQL += " AND";
            strSQL += "    TS.STATUS_ID      = MS.STATUS_ID(+)";
            strSQL += " AND";
            strSQL += "    TS.ORDER_NO       = O.ORDER_NO(+)";
            strSQL += " AND";
            strSQL += "    O.ORDER_USER_CD   = MU.USER_CD(+)";
            strSQL += " AND";
            strSQL += "    TS.MAKER_ID       = MM.MAKER_ID(+)";
            strSQL += " AND";
            strSQL += "    TS.UNITSIZE_ID    = MUNIT.UNITSIZE_ID(+)";

            parm["STOCK_ID"] = stockId;

            //検索
            DataTable dt = DbUtil.Select(strSQL, parm);

            if (dt.Rows.Count == 0) return;

            setupResult(dt);
        }

        /// <summary>
        /// 別の注文番号で同一の試薬バーコードが存在するかチェック
        /// </summary>
        /// <returns>
        ///     true:存在する
        ///     false:存在しない
        /// </returns>
        public bool IsExistBarcode()
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" SELECT COUNT(STOCK_ID) CNT ");
            sbSql.Append("   FROM   T_STOCK ");
            sbSql.Append(" WHERE ");
            sbSql.Append("   BARCODE = :BARCODE ");
            sbSql.Append(" AND ");
            sbSql.Append("   ORDER_NO <> :ORDER_NO ");
            sbSql.Append(" AND ");
            sbSql.Append("   DEL_FLAG <> 1 ");

            Hashtable param = new Hashtable();
            param["BARCODE"] = this.barcode;
            param["ORDER_NO"] = this.orderNo;

            DataTable dtTable = DbUtil.Select(sbSql.ToString(), param);

            return Convert.ToInt32(dtTable.Rows[0]["CNT"]) > 0 ? true : false;
        }

        /// <summary>
        /// 同一注文番号のバーコードをリスト表示する。
        /// 受領済み以降のステータスのものは表示しない
        /// </summary>
        /// <returns>同一注文番号のバーコードリスト</returns>
        /// <remarks>
        ///     受領済み以降のステータスは以下の通り。
        ///     　202:受領済
        ///     　203:在庫
        ///     　204:貸出
        ///     　205:廃棄
        ///     　206:使切
        /// </remarks>
        public DataTable GetSameOrderList()
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("SELECT ");
            sbSql.Append("   BARCODE ");
            sbSql.Append("  ,STOCK_ID ");
            sbSql.Append(" FROM ");
            sbSql.Append("   T_STOCK ");
            sbSql.Append(" WHERE ");
            sbSql.Append("   BARCODE IS NOT NULL ");
            sbSql.Append(" AND ");
            sbSql.Append("   ORDER_NO = :ORDER_NO ");
            sbSql.Append(" AND ");
            sbSql.Append("   DEL_FLAG <> 1 ");
            sbSql.Append(" AND ");
            sbSql.Append("   STATUS_ID NOT IN (");
            // ????? 暫定対応 ????? STA
            sbSql.Append(Const.cSTOCK_STATUS_ID.REC_FIN.ToString("D"));
            sbSql.Append(",");
            sbSql.Append(Const.cSTOCK_STATUS_ID.STOCK.ToString("D"));
            sbSql.Append(",");
            sbSql.Append(Const.cSTOCK_STATUS_ID.BORROW.ToString("D"));
            sbSql.Append(",");
            sbSql.Append(Const.cSTOCK_STATUS_ID.DISPOSE.ToString("D"));
            sbSql.Append(",");
            sbSql.Append(Const.cSTOCK_STATUS_ID.EMPTY.ToString("D"));
            // ????? 暫定対応 ????? END
            sbSql.Append(")");

            Hashtable param = new Hashtable();
            param["ORDER_NO"] = this.orderNo;

            return DbUtil.Select(sbSql.ToString(), param);
        }

        /// <summary>
        /// 注文番号をキーに完納フラグが「1」の件数と
        /// 在庫ステータスが「200:納品待」または「201:納品済」の件数を取得する
        /// </summary>
        /// <param name="context">DbConnection</param>
        /// <returns>
        ///     O_CNT:完納フラグが「1」の件数
        ///     Z_CNT:在庫ステータスが「200:納品待」または「201:納品済」の件数
        /// </returns>
        public DataTable CheckDeliFlgFinAndNoReceiveCount(DbContext context)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("SELECT ");
            // 完納フラグが1の件数
            sbSql.Append("   TBL_O.CNT O_CNT");
            // 200:納品待 または 201:納品済の件数
            sbSql.Append("  ,TBL_Z.CNT Z_CNT");
            sbSql.Append(" FROM ");
            sbSql.Append(" ( SELECT COUNT(*) CNT FROM T_ORDER ");
            sbSql.Append("    WHERE  ");
            sbSql.Append("         DELI_FLAG = :DELI_FLAG ");
            sbSql.Append("      AND ORDER_NO = :ORDER_NO ");
            sbSql.Append("     AND DEL_FLAG <> 1 ");
            sbSql.Append("  ) TBL_O, ");
            sbSql.Append(" ( SELECT COUNT(*) CNT from T_STOCK ");
            sbSql.Append("    WHERE  ");
            sbSql.Append("         STATUS_ID IN (:DELI_WAIT, :DELI_FIN) ");
            sbSql.Append("      AND ORDER_NO = :ORDER_NO ");
            sbSql.Append("      AND DEL_FLAG <> 1 ");
            sbSql.Append(" ) TBL_Z ");

            Hashtable param = new Hashtable();
            param.Add("DELI_FLAG", Const.cDELI_FLG_FIN);
            param.Add("ORDER_NO", this.orderNo);
            param.Add("DELI_WAIT", Const.cSTOCK_STATUS_ID.DELI_WAIT.ToString("D"));
            param.Add("DELI_FIN", Const.cSTOCK_STATUS_ID.DELI_FIN.ToString("D"));

            return DbUtil.Select(context, sbSql.ToString(), param);
        }

        /// <summary>
        /// T_STOCK.STOCK_IDをキーに発注データのステータス更新を行うか否かを確認するデータを取得します。
        /// </summary>
        /// <param name="context">DbConnection</param>
        /// <returns>
        ///     DELI_FLAG:発注データの完納フラグ
        ///     Z_ALL_CNT:同一発注番号の在庫データの全件数
        ///     Z_BAR_CNT:同一発注番号の在庫データからバーコードが入っている件数
        /// </returns>
        public void GetCheckOrderStatus(DbContext context)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("select");
            sbSql.Append("  TBL1.DELI_FLAG");
            sbSql.Append(" ,");
            sbSql.Append("  TBL2.Z_CNT Z_ALL_CNT");
            sbSql.Append(" ,");
            sbSql.Append("  TBL3.Z_CNT Z_BAR_CNT");
            sbSql.Append(" from ");
            // TBL1 = 発注データの完納フラグを取得
            sbSql.Append("(select");
            sbSql.Append("     O.DELI_FLAG");
            sbSql.Append(" from");
            sbSql.Append("     T_ORDER O inner join T_STOCK Z on O.ORDER_NO = Z.ORDER_NO");
            sbSql.Append(" where");
            sbSql.Append("     Z.STOCK_ID = :STOCK_ID");
            sbSql.Append(" and O.DEL_FLAG <> 1");
            sbSql.Append(" and Z.DEL_FLAG <> 1");
            sbSql.Append(") TBL1,");
            // TBL2 = 同一発注番号の在庫データの全件数
            sbSql.Append("(select");
            sbSql.Append("     count(*) Z_CNT");
            sbSql.Append(" from");
            sbSql.Append("     T_STOCK Z inner join ");
            sbSql.Append("     ( select");
            sbSql.Append("         ORDER_NO");
            sbSql.Append("       from");
            sbSql.Append("         T_STOCK");
            sbSql.Append("       where");
            sbSql.Append("         STOCK_ID = :STOCK_ID");
            sbSql.Append("       and DEL_FLAG <> 1 ");
            sbSql.Append("     ) Z2 on Z.ORDER_NO = Z2.ORDER_NO");
            sbSql.Append(" where");
            sbSql.Append("     Z.DEL_FLAG <> 1");
            sbSql.Append(") TBL2,");
            // TBL3 = 同一発注番号の在庫データからバーコードが入っている件数
            sbSql.Append("(select");
            sbSql.Append("     count(*) Z_CNT");
            sbSql.Append(" from");
            sbSql.Append("     T_STOCK Z inner join ");
            sbSql.Append("     ( select");
            sbSql.Append("         ORDER_NO");
            sbSql.Append("       from");
            sbSql.Append("         T_STOCK");
            sbSql.Append("       where");
            sbSql.Append("         STOCK_ID = :STOCK_ID");
            sbSql.Append("       and DEL_FLAG <> 1 ");
            sbSql.Append("     ) Z2 on Z.ORDER_NO = Z2.ORDER_NO");
            sbSql.Append(" where");
            sbSql.Append("     Z.DEL_FLAG <> 1");
            sbSql.Append(" and Z.BARCODE is not null ");
            sbSql.Append(") TBL3");

            Hashtable param = new Hashtable();
            param["STOCK_ID"] = this.stockId;

            DataTable dt = DbUtil.Select(context, sbSql.ToString(), param);
            if (dt.Rows.Count > 0)
            {
                this.deliflag = Convert.ToString(dt.Rows[0]["DELI_FLAG"]);
                this.SAME_ORDER_STOCK_COUNT = Convert.ToInt64(dt.Rows[0]["Z_ALL_CNT"]);
                this.SAME_ORDER_BARCODE_COUNT = Convert.ToInt64(dt.Rows[0]["Z_BAR_CNT"]);
            }
        }

        /// <summary>
        /// 作成SQL Where句をキーにデータを読み込みます
        /// </summary>
        /// <param name="sqlwhere">作成SQL Where句</param>
        /// <param name="_params">バインド変数格納Hashtable</param>
        /// <returns>取得結果格納DataTable</returns>
        public DataTable GetStockData_Accept(string sqlwhere, Hashtable _params)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("select");
            sbSql.Append("  Z2.STOCK_ID");
            sbSql.Append(" ,Z2.BARCODE");
            sbSql.Append(" ,Z2.COMPOUND_LIST_ID");
            sbSql.Append(" ,Z2.KANJYO_NAME");
            sbSql.Append(" ,Z2.DELI_NUM");
            sbSql.Append(" ,Z2.ORDER_NO");
            sbSql.Append(" ,Z2.ORDER_USER_NAME");
            sbSql.Append(" ,Z2.GROUP_NAME");
            sbSql.Append(" ,Z2.PRODUCT_NAME");
            sbSql.Append(" ,Z2.CAT_CD");
            sbSql.Append(" ,Z2.MAKER_NAME");
            sbSql.Append(" ,Z2.MEMO");
            sbSql.Append(" ,Z2.ORDER_STATUS");
            sbSql.Append(" ,Z2.ZAIKO_STATUS");
            sbSql.Append(" ,REG_DISP_NAME ");
            sbSql.Append("from ");
            sbSql.Append("  (select ");
            sbSql.Append("    Z1.STOCK_ID");
            sbSql.Append("   ,Z1.BARCODE");
            sbSql.Append("   ,Z1.COMPOUND_LIST_ID");
            sbSql.Append("   ,K.KANJYO_NAME");
            sbSql.Append("   ,( select count(*) from T_STOCK where ORDER_NO = O.ORDER_NO and DEL_FLAG <> 1 ) DELI_NUM");
            sbSql.Append("   ,Z1.ORDER_NO");
            sbSql.Append("   ,U.USER_NAME ORDER_USER_NAME");
            sbSql.Append("   ,G.GROUP_NAME");
            sbSql.Append("   ,(ZYCOAG_SYSTEM.GetProductName( Z1.STOCK_ID," + Const.cREGULATORY_T_STOCK + " )) PRODUCT_NAME");
            sbSql.Append("   ,Z1.CAT_CD");
            sbSql.Append("   ,SUP.MAKER_NAME");
            sbSql.Append("   ,Z1.MEMO");
            sbSql.Append("   ,S1.STATUS_TEXT ORDER_STATUS");
            sbSql.Append("   ,S2.STATUS_TEXT ZAIKO_STATUS");
            sbSql.Append("   ,ZYCOAG_SYSTEM.GetRegulation(Z1.COMPOUND_LIST_ID) REG_DISP_NAME");
            sbSql.Append("  from T_STOCK Z1");
            sbSql.Append("        left join T_ORDER      O on Z1.ORDER_NO     = O.ORDER_NO   and O.DEL_FLAG <> 1");
            sbSql.Append("        left join M_KANJYO     K on O.KANJYO_ID     = K.KANJYO_ID  and K.DEL_FLAG <> 1");
            sbSql.Append("        left join M_USER       U on O.ORDER_USER_CD = U.USER_CD    and U.DEL_FLAG <> 1");
            sbSql.Append("        left join M_GROUP      G on U.GROUP_ID      = G.GROUP_ID   and G.DEL_FLAG <> 1");
            sbSql.Append("        left join M_STATUS    S1 on O.STATUS_ID     = S1.STATUS_ID and S1.DEL_FLAG <> 1");
            sbSql.Append("        left join M_STATUS    S2 on Z1.STATUS_ID    = S2.STATUS_ID and S2.DEL_FLAG <> 1");
            sbSql.Append("        left join M_MAKER    SUP on Z1.MAKER_ID     = SUP.MAKER_ID and SUP.DEL_FLAG <> 1");
            sbSql.Append("  where");
            sbSql.Append("     Z1.DEL_FLAG <> 1");
            sbSql.Append(sqlwhere);
            sbSql.Append("  ) Z2");
            sbSql.Append("     order by Z2.ORDER_NO, Z2.STOCK_ID");

            return DbUtil.Select(sbSql.ToString(), _params);
        }

        /// <summary>
        /// 検索結果をメンバに設定します
        /// </summary>
        /// <param name="dt"></param>
        private void setupResult(DataTable result)
        {
            DataRow row = result.Rows[0];
            this.stockId = row["STOCK_ID"].ToString();
            this.barcode = row["BARCODE"].ToString();
            this.statusId = row["STATUS_ID"].ToString();
            this.owner = row["OWNER"].ToString();
            this.compoundListId = row["COMPOUND_LIST_ID"].ToString();
            this.catCd = row["CAT_CD"].ToString();
            this.unitsize = row["UNITSIZE"].ToString();
            this.unitsizeId = row["UNITSIZE_ID"].ToString();
            this.productNameJp = row["PRODUCT_NAME_JP"].ToString();
            this.productNameEn = row["PRODUCT_NAME_EN"].ToString();
            this.productNameSearch = row["PRODUCT_NAME_SEARCH"].ToString();
            this.grade = row["GRADE"].ToString();
            this.areaId = row["AREA_ID"].ToString();
            this.makerId = row["MAKER_ID"].ToString();
            this.borrower = row["BORROWER"].ToString();
            this.brwDate = row["BRW_DATE"].ToString();
            this.orderNo = row["ORDER_NO"].ToString();
            this.locId = row["LOC_ID"].ToString();
            this.limitDate = row["LIMIT_DATE"].ToString();
            this.manageWeight = row["MANAGE_WEIGHT"].ToString();
            this.vendorId = row["VENDOR_ID"].ToString();
            this.iniGrossWeightG = row["INI_GROSS_WEIGHT_G"].ToString();
            this.curGrossWeightG = row["CUR_GROSS_WEIGHT_G"].ToString();
            this.bottleWeightG = row["BOTTLE_WEIGHT_G"].ToString();
            this.memo = row["MEMO"].ToString();
            this.preservationCondition = row["PRESERVATION_CONDITION"].ToString();
            this.transportCondition = row["TRANSPORT_CONDITION"].ToString();
            this.regDate = row["REG_DATE"].ToString();
            this.updDate = row["UPD_DATE"].ToString();
            this.delDate = row["DEL_DATE"].ToString();
            this.delFlag = row["DEL_FLAG"].ToString();
            this.deliDate = row["DELI_DATE"].ToString();
            this.finePoisonFlag = row["FINE_POISON_FLAG"].ToString();
            this.firstLocId = row["FIRST_LOC_ID"].ToString();
            this.delMailDate = row["DEL_MAIL_DATE"].ToString();
            this.accountId = row["ACCOUNT_ID"].ToString();
            this.lastUpdDate = row["LAST_UPD_DATE"].ToString();

            this.orderUserName = row["ORDER_USER_NAME"].ToString();
            this.makerName = row["MAKER_NAME"].ToString();
            this.unitSizeName = row["UNIT_SIZE_NAME"].ToString();
            this.projectCd = row["PROJECT_CD"].ToString();
            this.kanjyoId = row["KANJYO_ID"].ToString();
            this.locBarcode = row["LOC_BARCODE"].ToString();
            this.groupId = row["GROUP_ID"].ToString();
            this.locCheckFlag = row["LOC_CHECK_FLAG"].ToString();
            this.vendorOrderNo = row["VENDOR_ORDER_NO"].ToString();
            this.locName = row["LOC_NAME"].ToString();
            this.statusText = row["STATUS_TEXT"].ToString();

            // 2018/12/11 Add Start TPE Kometani
            StringBuilder sbSql = new StringBuilder();
            Hashtable _params = new Hashtable();
            _params.Add("USER_CD", this.borrower);

            sbSql.Append("SELECT mu.USER_NAME, mu.GROUP_CD, mg.GROUP_NAME ");
            sbSql.Append("from M_USER mu inner join V_GROUP mg ");
            sbSql.Append("on mg.GROUP_CD = mu.GROUP_CD ");
            sbSql.Append("where mu.USER_CD = :USER_CD");
            DataTable dt = DbUtil.Select(sbSql.ToString(), _params);
            DataRow row2 = dt.Rows[0];
            this.actionUserNm = row2["ACTION_USER_NM"].ToString();
            this.ACTION_GROUP_CD = row2["GROUP_CD"].ToString();
            this.ACTION_GROUP_NAME = row2["GROUP_NAME"].ToString();
            // 2018/12/11 Add End TPE Kometani

            this.intStockCount = result.Rows.Count;
        }

        /// <summary>
        /// 化学物質の在庫があるか
        /// </summary>
        /// <param name="CHEM_CD">カタログコード(化学物質コード)</param>
        /// <returns>true:ある/false:ない</returns>
        public bool HasChemStock(string CHEM_CD, string REG_NO)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.AppendLine("select");
            sbSql.AppendLine(" count(*)");
            sbSql.AppendLine("from ");
            sbSql.AppendLine(" " + nameof(T_STOCK));
            sbSql.AppendLine("where");
            sbSql.AppendLine(" " + nameof(T_STOCK.CAT_CD) + " = :" + nameof(T_STOCK.CAT_CD));
            sbSql.AppendLine(" and " + nameof(T_STOCK.STATUS_ID) + " <> :" + nameof(T_STOCK.STATUS_ID));
            sbSql.AppendLine(" and " + nameof(ChemStockModel.REG_NO) + " = :" + nameof(ChemStockModel.REG_NO));

            var paramaters = new Hashtable();
            paramaters.Add(nameof(T_STOCK.CAT_CD), CHEM_CD);
            paramaters.Add(nameof(T_STOCK.STATUS_ID), 205);
            paramaters.Add(nameof(ChemStockModel.REG_NO), REG_NO);

            var result = DbUtil.Select(sbSql.ToString(), paramaters);
            return ((int)result.Rows[0][0]) > 0;
        }
        #endregion

        #region 更新系処理
        /// <summary>
        /// T_STOCK.BARCODEをキーに貸出処理をします
        /// </summary>
        /// <param name="context"></param>
        public void UpStock_Borrow_All(DbContext context)
        {
            Hashtable param = new Hashtable();

            string strSQL = "";
            strSQL = " UPDATE";
            strSQL += "    T_STOCK TS";
            strSQL += " SET";
            strSQL += "    TS.STATUS_ID  = :STATUS_ID";
            strSQL += "    , TS.BORROWER = :BORROWER";
            strSQL += "    , TS.BRW_DATE = SYSDATE";
            strSQL += " WHERE";
            strSQL += "    TS.STOCK_ID = :STOCK_ID";

            param["STATUS_ID"] = this.statusId;
            param["BORROWER"] = this.borrower;
            param["STOCK_ID"] = this.stockId;
            DbUtil.ExecuteUpdate(context, strSQL, param);

            strSQL = "";
            strSQL = "INSERT INTO T_ACTION_HISTORY";
            strSQL += "(";
            strSQL += "   ACTION_TYPE_ID";
            strSQL += "   , BARCODE";
            strSQL += "   , USER_CD";
            strSQL += "   , STOCK_ID";
            // 2018/12/11 Add Start TPE Kometani
            strSQL += "   , ACTION_USER_NM";
            strSQL += "   , ACTION_GROUP_NAME";
            strSQL += "   , ATION_GROUP_CD";
            // 2018/12/11 Add End TPE Kometani
            strSQL += ")VALUES(";
            strSQL += "   :ACTION_TYPE_ID";
            strSQL += "   , :BARCODE";
            strSQL += "   , :USER_CD";
            strSQL += "   , :STOCK_ID";
            // 2018/12/11 Add Start TPE Kometani
            strSQL += "   , :ACTION_USER_NM";
            strSQL += "   , :ACTION_GROUP_NAME";
            strSQL += "   , :ATION_GROUP_CD";
            // 2018/12/11 Add End TPE Kometani
            strSQL += ")";

            param.Clear();
            param["ACTION_TYPE_ID"] = Const.cACTION_HISTORY_TYPE.BORROW.ToString("D");
            param["BARCODE"] = this.barcode;
            param["USER_CD"] = this.borrower;
            param["STOCK_ID"] = this.stockId;
            // 2018/12/11 Add Start TPE Kometani
            param["ACTION_USER_NM"] = this.actionUserNm;
            param["ACTION_GROUP_NAME"] = this.actionGroupName;
            param["ATION_GROUP_CD"] = this.actionGroupCd;
            // 2018/12/11 Add End TPE Kometani
            DbUtil.ExecuteUpdate(context, strSQL, param);
        }

        /// <summary>
        /// T_STOCK.BARCODEをキーに廃棄処理をします
        /// </summary>
        /// <param name="context"></param>
        public void UpStock_Dispose_All(DbContext context)
        {
            Hashtable param = new Hashtable();

            string strSQL = "";
            strSQL = " UPDATE";
            strSQL += "    T_STOCK TS";
            strSQL += " SET";
            strSQL += "    TS.STATUS_ID  = :STATUS_ID";
            strSQL += " WHERE";
            strSQL += "    TS.STOCK_ID = :STOCK_ID";

            param["STATUS_ID"] = this.statusId;
            param["STOCK_ID"] = this.stockId;
            DbUtil.ExecuteUpdate(context, strSQL, param);

            strSQL = "";
            strSQL = "INSERT INTO T_ACTION_HISTORY";
            strSQL += "(";
            strSQL += "   ACTION_TYPE_ID";
            strSQL += "   , BARCODE";
            strSQL += "   , USER_CD";
            strSQL += "   , STOCK_ID";
            // 2018/12/11 Add Start TPE Kometani
            strSQL += "   , ACTION_USER_NM";
            strSQL += "   , ACTION_GROUP_NAME";
            strSQL += "   , ATION_GROUP_CD";
            // 2018/12/11 Add End TPE Kometani
            strSQL += ")VALUES(";
            strSQL += "   :ACTION_TYPE_ID";
            strSQL += "   , :BARCODE";
            strSQL += "   , :USER_CD";
            strSQL += "   , :STOCK_ID";
            // 2018/12/11 Add Start TPE Kometani
            strSQL += "   , :ACTION_USER_NM";
            strSQL += "   , :ACTION_GROUP_NAME";
            strSQL += "   , :ATION_GROUP_CD";
            // 2018/12/11 Add End TPE Kometani
            strSQL += ")";

            param.Clear();
            param["ACTION_TYPE_ID"] = Const.cACTION_HISTORY_TYPE.DISPOSE.ToString("D");
            param["BARCODE"] = this.barcode;
            param["USER_CD"] = this.borrower;
            param["STOCK_ID"] = this.stockId;
            // 2018/12/11 Add Start TPE Kometani
            param["ACTION_USER_NM"] = this.actionUserNm;
            param["ACTION_GROUP_NAME"] = this.actionGroupName;
            param["ATION_GROUP_CD"] = this.actionGroupCd;
            // 2018/12/11 Add End TPE Kometani
            DbUtil.ExecuteUpdate(context, strSQL, param);
        }

        /// <summary>
        /// T_STOCK.BARCODEをキーに使切処理をします
        /// </summary>
        /// <param name="context"></param>
        public void UpStock_Empty_All(DbContext context)
        {
            Hashtable param = new Hashtable();

            string strSQL = "";
            strSQL = " UPDATE";
            strSQL += "    T_STOCK TS";
            strSQL += " SET";
            strSQL += "    TS.STATUS_ID  = :STATUS_ID";
            strSQL += "    , TS.CUR_GROSS_WEIGHT_G = :CUR_GROSS_WEIGHT_G";
            strSQL += " WHERE";
            strSQL += "    TS.STOCK_ID = :STOCK_ID";

            param["STATUS_ID"] = this.statusId;
            param["CUR_GROSS_WEIGHT_G"] = this.curGrossWeightG;
            param["STOCK_ID"] = this.stockId;
            DbUtil.ExecuteUpdate(context, strSQL, param);

            strSQL = "";
            strSQL = "INSERT INTO T_ACTION_HISTORY";
            strSQL += "(";
            strSQL += "   ACTION_TYPE_ID";
            strSQL += "   , BARCODE";
            strSQL += "   , USER_CD";
            strSQL += "   , STOCK_ID";
            // 2018/12/11 Add Start TPE Kometani
            strSQL += "   , ACTION_USER_NM";
            strSQL += "   , ACTION_GROUP_NAME";
            strSQL += "   , ATION_GROUP_CD";
            // 2018/12/11 Add End TPE Kometani
            strSQL += ")VALUES(";
            strSQL += "   :ACTION_TYPE_ID";
            strSQL += "   , :BARCODE";
            strSQL += "   , :USER_CD";
            strSQL += "   , :STOCK_ID";
            // 2018/12/11 Add Start TPE Kometani
            strSQL += "   , :ACTION_USER_NM";
            strSQL += "   , :ACTION_GROUP_NAME";
            strSQL += "   , :ATION_GROUP_CD";
            // 2018/12/11 Add End TPE Kometani
            strSQL += ")";

            param.Clear();
            param["ACTION_TYPE_ID"] = Const.cACTION_HISTORY_TYPE.EMPTY.ToString("D");
            param["BARCODE"] = this.barcode;
            param["USER_CD"] = this.borrower;
            param["STOCK_ID"] = this.stockId;
            // 2018/12/11 Add Start TPE Kometani
            param["ACTION_USER_NM"] = this.actionUserNm;
            param["ACTION_GROUP_NAME"] = this.actionGroupName;
            param["ATION_GROUP_CD"] = this.actionGroupCd;
            // 2018/12/11 Add End TPE Kometani
            DbUtil.ExecuteUpdate(context, strSQL, param);
        }

        /// <summary>
        /// T_STOCK.BARCODEをキーに移管処理をします
        /// </summary>
        /// <param name="context"></param>
        public void UpStock_Move_All(DbContext context)
        {
            string strSQL = "";
            strSQL = " UPDATE";
            strSQL += "    T_STOCK TS";
            strSQL += " SET";
            strSQL += "    TS.LOC_ID  = :LOC_ID";
            strSQL += " WHERE";
            strSQL += "    TS.STOCK_ID = :STOCK_ID";

            Hashtable param = new Hashtable();
            param["LOC_ID"] = this.locId;
            param["STOCK_ID"] = this.stockId;
            DbUtil.ExecuteUpdate(context, strSQL, param);

            strSQL = "";
            strSQL = "INSERT INTO T_ACTION_HISTORY";
            strSQL += "(";
            strSQL += "   ACTION_TYPE_ID";
            strSQL += "   , BARCODE";
            strSQL += "   , USER_CD";
            strSQL += "   , LOC_ID";
            strSQL += "   , STOCK_ID";
            // 2018/12/11 Add Start TPE Kometani
            strSQL += "   , ACTION_USER_NM";
            strSQL += "   , ACTION_GROUP_NAME";
            strSQL += "   , ATION_GROUP_CD";
            // 2018/12/11 Add End TPE Kometani
            strSQL += ")VALUES(";
            strSQL += "   :ACTION_TYPE_ID";
            strSQL += "   , :BARCODE";
            strSQL += "   , :USER_CD";
            strSQL += "   , :LOC_ID";
            strSQL += "   , :STOCK_ID";
            // 2018/12/11 Add Start TPE Kometani
            strSQL += "   , :ACTION_USER_NM";
            strSQL += "   , :ACTION_GROUP_NAME";
            strSQL += "   , :ATION_GROUP_CD";
            // 2018/12/11 Add End TPE Kometani
            strSQL += ")";

            param.Clear();
            param["ACTION_TYPE_ID"] = Const.cACTION_HISTORY_TYPE.MOVE.ToString("D");
            param["BARCODE"] = this.barcode;
            param["USER_CD"] = this.borrower;
            param["LOC_ID"] = this.locId;
            param["STOCK_ID"] = this.stockId;
            // 2018/12/11 Add Start TPE Kometani
            param["ACTION_USER_NM"] = this.actionUserNm;
            param["ACTION_GROUP_NAME"] = this.actionGroupName;
            param["ATION_GROUP_CD"] = this.actionGroupCd;
            // 2018/12/11 Add End TPE Kometani
            DbUtil.ExecuteUpdate(context, strSQL, param);
        }
        #endregion

        #region 受領画面更新処理
        /// <summary>
        /// T_STOCK.STOCK_IDをキーに更新処理をします
        /// </summary>
        /// <param name="context">DbConnection</param>
        /// <returns>更新件数</returns>
        public void UpStock_Receive(DbContext context)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("UPDATE ");
            sbSql.Append("  T_STOCK ");
            sbSql.Append("    SET ");
            sbSql.Append("     LOC_ID = :LOC_ID ");
            // 初回保管場所が空の場合
            if (this.firstLocId == "")
            {
                sbSql.Append(" ,FIRST_LOC_ID = :LOC_ID ");
            }
            sbSql.Append("     ,OWNER = :OWNER");
            sbSql.Append("     ,LAST_UPD_DATE = SYSDATE ");
            sbSql.Append("     ,STATUS_ID = :STATUS_ID ");
            sbSql.Append(" WHERE ");
            sbSql.Append("   STOCK_ID = :STOCK_ID ");

            Hashtable param = new Hashtable();
            param["LOC_ID"] = this.locId;
            param["OWNER"] = this.owner;
            param["STATUS_ID"] = this.statusId;
            param["STOCK_ID"] = this.stockId;

            DbUtil.ExecuteUpdate(context, sbSql.ToString(), param);
        }
        #endregion

        #region 受入画面更新処理
        /// <summary>
        /// T_STOCK.STOCK_IDをキーに論理削除処理をします
        /// </summary>
        /// <param name="context">DbConnection</param>
        /// <returns>削除件数</returns>
        public void DelStock_Accept(DbContext context)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("UPDATE ");
            sbSql.Append("  T_STOCK ");
            sbSql.Append("    SET ");
            sbSql.Append("      DEL_FLAG = :DEL_FLAG ");
            sbSql.Append(" WHERE ");
            sbSql.Append("   STOCK_ID = :STOCK_ID ");

            Hashtable param = new Hashtable();
            param["DEL_FLAG"] = Const.cDB_DELETE_FLG;
            param["STOCK_ID"] = this.stockId;

            DbUtil.ExecuteUpdate(context, sbSql.ToString(), param);
        }

        /// <summary>
        /// T_STOCK.STOCK_IDをキーにコピー処理(INSERT)をします
        /// </summary>
        /// <param name="context">DbConnection</param>
        /// <param name="CopyCnt">コピー件数</param>
        /// <returns>登録件数</returns>
        public void InsStock_Accept(DbContext context, int CopyCnt)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("INSERT INTO T_STOCK (");
            sbSql.Append("   STATUS_ID");
            sbSql.Append("  ,OWNER");
            sbSql.Append("  ,COMPOUND_LIST_ID");
            sbSql.Append("  ,CAT_CD");
            sbSql.Append("  ,UNITSIZE");
            sbSql.Append("  ,UNITSIZE_ID");
            sbSql.Append("  ,PRODUCT_NAME_JP");
            sbSql.Append("  ,PRODUCT_NAME_EN");
            sbSql.Append("  ,PRODUCT_NAME_SEARCH");
            sbSql.Append("  ,GRADE");
            sbSql.Append("  ,AREA_ID");
            sbSql.Append("  ,MAKER_ID");
            sbSql.Append("  ,BORROWER");
            sbSql.Append("  ,BRW_DATE");
            sbSql.Append("  ,ORDER_NO");
            sbSql.Append("  ,LOC_ID");
            sbSql.Append("  ,LIMIT_DATE");
            sbSql.Append("  ,MANAGE_WEIGHT");
            sbSql.Append("  ,VENDOR_ID");
            sbSql.Append("  ,INI_GROSS_WEIGHT_G");
            sbSql.Append("  ,CUR_GROSS_WEIGHT_G");
            sbSql.Append("  ,BOTTLE_WEIGHT_G");
            sbSql.Append("  ,PRESERVATION_CONDITION");
            sbSql.Append("  ,TRANSPORT_CONDITION");
            sbSql.Append("  ,DELI_DATE");
            sbSql.Append("  ,FINE_POISON_FLAG");
            sbSql.Append("  ,FIRST_LOC_ID");
            sbSql.Append("  ,DEL_MAIL_DATE");
            sbSql.Append("  ,ACCOUNT_ID");
            sbSql.Append("  ,LAST_UPD_DATE");
            sbSql.Append(") ");
            sbSql.Append(" SELECT ");
            sbSql.Append("   STATUS_ID");
            sbSql.Append("  ,OWNER");
            sbSql.Append("  ,COMPOUND_LIST_ID");
            sbSql.Append("  ,CAT_CD");
            sbSql.Append("  ,UNITSIZE");
            sbSql.Append("  ,UNITSIZE_ID");
            sbSql.Append("  ,PRODUCT_NAME_JP");
            sbSql.Append("  ,PRODUCT_NAME_EN");
            sbSql.Append("  ,PRODUCT_NAME_SEARCH");
            sbSql.Append("  ,GRADE");
            sbSql.Append("  ,AREA_ID");
            sbSql.Append("  ,MAKER_ID");
            sbSql.Append("  ,BORROWER");
            sbSql.Append("  ,BRW_DATE");
            sbSql.Append("  ,ORDER_NO");
            sbSql.Append("  ,LOC_ID");
            sbSql.Append("  ,LIMIT_DATE");
            sbSql.Append("  ,MANAGE_WEIGHT");
            sbSql.Append("  ,VENDOR_ID");
            sbSql.Append("  ,INI_GROSS_WEIGHT_G");
            sbSql.Append("  ,CUR_GROSS_WEIGHT_G");
            sbSql.Append("  ,BOTTLE_WEIGHT_G");
            sbSql.Append("  ,PRESERVATION_CONDITION");
            sbSql.Append("  ,TRANSPORT_CONDITION");
            sbSql.Append("  ,DELI_DATE");
            sbSql.Append("  ,FINE_POISON_FLAG");
            sbSql.Append("  ,FIRST_LOC_ID");
            sbSql.Append("  ,DEL_MAIL_DATE");
            sbSql.Append("  ,ACCOUNT_ID");
            sbSql.Append("  ,LAST_UPD_DATE");
            sbSql.Append(" FROM T_STOCK ");
            sbSql.Append("  WHERE STOCK_ID = :STOCK_ID ");

            Hashtable param = new Hashtable();
            param["STOCK_ID"] = this.stockId;

            for (int i = 0; i < CopyCnt; i++)
            {
                DbUtil.ExecuteUpdate(context, sbSql.ToString(), param);
            }
        }

        /// <summary>
        /// T_STOCK.STOCK_IDをキーに更新処理をします
        /// </summary>
        /// <param name="context">DbConnection</param>
        /// <returns>更新件数</returns>
        public void UpStock_Accept(DbContext context)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("UPDATE ");
            sbSql.Append("  T_STOCK ");
            sbSql.Append("    SET ");
            sbSql.Append("      BARCODE   = :BARCODE ");
            sbSql.Append("     ,DELI_DATE = :DELI_DATE ");
            sbSql.Append("     ,MEMO      = :MEMO ");
            sbSql.Append("     ,STATUS_ID = :STATUS_ID ");
            sbSql.Append(" WHERE ");
            sbSql.Append("   STOCK_ID = :STOCK_ID ");

            Hashtable param = new Hashtable();
            param["BARCODE"] = this.barcode;
            param["DELI_DATE"] = this.deliDate;
            param["MEMO"] = this.memo;
            param["STATUS_ID"] = this.statusId;
            param["STOCK_ID"] = this.stockId;

            DbUtil.ExecuteUpdate(context, sbSql.ToString(), param);
        }

        /// <summary>
        /// T_STOCK.STOCK_IDをキーに発注テーブル更新処理をします
        /// </summary>
        /// <param name="context">DbConnection</param>
        /// <returns>更新件数</returns>
        public void UpOrder_Accept(DbContext context)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("UPDATE ");
            sbSql.Append("  T_ORDER ");
            sbSql.Append("    SET ");
            sbSql.Append("      STATUS_ID = :STATUS_ID ");
            sbSql.Append(" WHERE ");
            sbSql.Append("      ORDER_NO = (SELECT ORDER_NO FROM T_STOCK WHERE STOCK_ID = :STOCK_ID AND DEL_FLAG <> 1) ");
            sbSql.Append(" AND ");
            sbSql.Append("      DEL_FLAG <> 1");

            Hashtable param = new Hashtable();
            param["STATUS_ID"] = Const.cORDER_STATUS_ID.DELI_FIN.ToString("D");
            param["STOCK_ID"] = this.stockId;

            DbUtil.ExecuteUpdate(context, sbSql.ToString(), param);
        }
        #endregion
    }
}