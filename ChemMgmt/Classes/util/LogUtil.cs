﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Configuration;
using System.IO;
using System.IO.Compression;
using System.Web.SessionState;
using ZyCoaG.Util;
using ZyCoaG.Classes.util;
using ZyCoaG.util;

namespace ChemMgmt.Classes.util
{
    public class LogUtil
    {
        //メンバ         
        protected string strLogPath;
        //プロパティ
        public string LogPath
        {
            set { this.strLogPath = value; }
            get { return this.strLogPath; }
        }

        // コンストラクタ
        public LogUtil()
        {
            //設定の取得
            this.LogPath = Common.webSiteRoot;
        }

        /// <summary>
        /// INFOログ出力処理
        /// </summary>
        /// <remarks>
        /// Web.Confiに設定されているパス、ファイル名でエラーログを出力します。
        /// </remarks>
        /// <param name="strPGID">プログラムID</param>
        /// <param name="strMessage">INFOメッセージ</param>
        public void OutInfoLog(string strPGID, string strMessage)
        {
            OutLog(strPGID, "INFO", strMessage);
        }
        /// <summary>
        /// WARNINGログ出力処理
        /// </summary>
        /// <remarks>
        /// Web.Confiに設定されているパス、ファイル名でエラーログを出力します。
        /// </remarks>
        /// <param name="strPGID">プログラムID</param>
        /// <param name="strMessage">警告メッセージ</param>
        public void OutWarLog(string strPGID, string strMessage)
        {
            OutLog(strPGID, "WARNING", strMessage);
        }
        /// <summary>
        /// ERRORログ出力処理
        /// </summary>
        /// <remarks>
        /// Web.Confiに設定されているパス、ファイル名でエラーログを出力します。
        /// </remarks>
        /// <param name="strPGID">プログラムID</param>
        /// <param name="strMessage">エラーメッセージ</param>
        public void OutErrLog(string strPGID, string strMessage)
        {
            OutLog(strPGID, "ERROR", strMessage);
        }

        /// <summary>
        /// ログの出力を行います。
        /// </summary>
        /// <param name="msgType">INFO or WARNING or ERROR</param>
        /// <param name="strPGID">プログラムID</param>
        /// <param name="strMessage">メッセージ</param>
        public void OutLog(string strPGID, string msgType, string strMessage)
        {
            HttpSessionState session = HttpContext.Current.Session;
            UserInfo.ClsUser userInfo = null;
            string loginUserCd = "";
            string ipAddress = "";

            if (Common.GetUserInfo(session, out userInfo))
            {
                loginUserCd = userInfo.USER_CD;
                ipAddress = HttpContext.Current.Request.UserHostAddress; // userInfo.IP_ADDRESS;
            }
            //string strFolderPath = this.LogPath + "\\..\\Log\\";

            string strFolderPath = @"C:\inetpub\wwwroot\CSMS_Ver1.0\Log\";

            if (!Directory.Exists(strFolderPath))
            {   
                Directory.CreateDirectory(strFolderPath);
            }
            string strFileName = strFolderPath + DateTime.Today.ToString("yyyyMMdd") + "_ChemMgmt.Web.log";

            if (!File.Exists(strFileName))
            {
                File.Create(strFileName);
            }

            if (msgType != null && msgType != "")
            {
                strMessage = "[" + msgType + "]:ProgramId[" + strPGID + "] LoginUserCode[" + loginUserCd + "] IP[" + ipAddress + "]:\r\n" + strMessage;
            }
            else
            {
                strMessage = "ProgramId[" + strPGID + "] LoginUserCode[" + loginUserCd + "] IP[" + ipAddress + "]:\r\n" + strMessage;
            }

            StreamWriter stWriter = new StreamWriter(strFileName, true, Encoding.GetEncoding("Shift_JIS"));
            stWriter.WriteLine(DateTime.Now.ToString() + " " + strMessage);
            stWriter.Close();
        }
    }
}