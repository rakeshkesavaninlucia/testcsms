﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Classes.util
{
    public static class DatabaseType
    {
        public static string Oracle { get; } = "Oracle";
        public static string SqlServer { get; } = "SqlServer";
    }
}