﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace ZyCoaG.Classes.util
{
    public class Compound
    {
        public Regulation SelectRegulation(string strCompoundListId)
        {
            Regulation _Regulation = new Regulation();

            string strSQL = "SELECT distinct R.REG_TYPE_ID,R.CLASS_ID,R.SECTION_ID,R.REG_FULL_TEXT,R.REG_ICON_PATH  ";
            strSQL += "FROM (SELECT * FROM M_COMPOUND_LIST WHERE DEL_FLAG = 0) CL ";
            strSQL += ",(SELECT * FROM M_COMPOUND WHERE DEL_FLAG = 0) C ";
            strSQL += ",(SELECT * FROM M_REGULATION_LIST WHERE DEL_FLAG = 0) RL ";
            strSQL += ",(SELECT * FROM M_REGULATION WHERE DEL_FLAG = 0) R ";
            strSQL += "WHERE CL.COMPOUND_LIST_ID = '" + strCompoundListId + "' ";
            strSQL += "AND   CL.COMPOUND_ID = C.COMPOUND_ID(+) ";
            strSQL += "AND   C.REGULATION_LIST_ID = RL.REGULATION_LIST_ID(+) ";
            strSQL += "AND   RL.REG_TYPE_ID = R.REG_TYPE_ID(+) ";
            strSQL += "AND   R.REG_TYPE_ID IS NOT NULL ";

            DataTable dt = DbUtil.Select(strSQL);

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    _Regulation.AddRegulation(Convert.ToString(dr["REG_TYPE_ID"]), Convert.ToString(dr["CLASS_ID"]), Convert.ToString(dr["SECTION_ID"]), Convert.ToString(dr["REG_FULL_TEXT"]), "");
                }
            }

            return _Regulation;
        }
    }
    public class Regulation
    {
        private List<RegulationList> items;

        public struct RegulationList
        {
            public string strRegtypeId;  // REG_TYPE_ID
            public string strClassId;    // CLASS_ID
            public string strSection;    // SECTION
            public string strRegText;    // REG_TEXT
            public string strIcon;    // REG_TEXT

            public RegulationList(string strRegtypeId, string strClassId, string strSection, string strRegText, string strIcon)
            {
                this.strRegtypeId = strRegtypeId;
                this.strClassId = strClassId;
                this.strSection = strSection;
                this.strRegText = strRegText;
                this.strIcon = strIcon;
            }
        };

        public List<RegulationList> Items
        {
            set { items = value; }
            get { return items; }
        }

        //コンストラクタ
        public Regulation()
        {
            Items = new List<RegulationList>();
        }

        //行へ追加
        public void AddRegulation(string strRegtypeId, string strClassId, string strSection, string strRegText, string strIcon)
        {
            Items.Add(new RegulationList(strRegtypeId, strClassId, strSection, strRegText, strIcon));
        }

    }
}