﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

namespace ZyCoaG.PrintLabel
{
    /// <summary>
    /// ラベル種類
    /// </summary>
    public enum LabelTypes
    {
        /// <summary>
        /// 瓶
        /// </summary>
        Bottle,

        /// <summary>
        /// ユーザー
        /// </summary>
        User,

        /// <summary>
        /// 保管場所または使用場所
        /// </summary>
        Location
    }

    /// <summary>
    /// バーコードデータのインターフェースです。
    /// </summary>
    public interface ILabelData
    {
        /// <summary>
        /// 番号
        /// </summary>
        string Number { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        string Name { get; set; }
    }

    [DataContract]
    public class LabelData : ILabelData
    {
        [DataMember]
        public string Name { get; set; }

        [DataMember]
        public string Number { get; set; }

        public LabelData()
        {
            Number = string.Empty;
            Name = string.Empty;
        }

        public LabelData(string number)
        {
            Number = number;
        }

        public LabelData(string number, string name)
        {
            Number = number;
            Name = name;
        }
    }

    /// <summary>
    /// バーコード印刷のインターフェースです。
    /// </summary>
    public interface ILabelPrint
    {
        LabelTypes LabelType { get; set; }
    }

    /// <summary>
    /// バーコード印刷の基底クラスです。
    /// </summary>
    public abstract class LabelPrintBase : ILabelPrint
    {
        public abstract LabelTypes LabelType { get; set; }

        public virtual string CreatePrintJavaScript(ILabelData data)
        {
            var datas = new List<ILabelData>();
            datas.Add(data);
            return CreatePrintJavaScript(datas);
        }

        public virtual string CreatePrintJavaScript(IEnumerable<ILabelData> datas)
        {
            var l = datas.ToList().ConvertAll(x => new LabelData(x.Number, x.Name));
            var json = string.Empty;

            using (var stream = new MemoryStream())
            {
                var serializer = new DataContractJsonSerializer(l.GetType());
                serializer.WriteObject(stream, l);
                json = Encoding.UTF8.GetString(stream.ToArray());
            }
            return CreatePrintJavaScriptEntity(json);
            // return AddJavaScriptTag(CreatePrintJavaScriptEntity(json));
        }

        protected virtual string AddJavaScriptTag(string script)
        {
            return "<script type=\"text/javascript\">" + script + "</script>";
        }

        protected abstract string CreatePrintJavaScriptEntity(string json);
    }

    /// <summary>
    /// PTouchのバーコード印刷クラスです。
    /// </summary>
    public class PTouchLabelPrint : LabelPrintBase, ILabelPrint
    {
        public override LabelTypes LabelType { get; set; }

        public PTouchLabelPrint(LabelTypes labelType)
        {
            LabelType = labelType;
        }

        protected override string CreatePrintJavaScriptEntity(string json)
        {
            //  return "DoPTouchLabelPrint('" + GetFileName() + "'," + json + ");";
              return GetFileName() + "," + json; 
        }

        private string GetFileName()
        {
            switch (LabelType)
            {
                case LabelTypes.Bottle:
                    return "Bottle.lbx";
                case LabelTypes.Location:
                    return "Location.lbx";
                case LabelTypes.User:
                    return "User.lbx";
                default:
                    throw new AggregateException();
            }
        }
    }
}