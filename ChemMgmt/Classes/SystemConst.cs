﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG
{
    /// <summary>
    /// システム定数を定義します。
    /// </summary>
    public static class SystemConst
    {
        /// <summary>
        /// システム名
        /// </summary>
        public static string SystemName { get; } = "化学物質管理システム";

        /// <summary>
        /// HTML半角スペース
        /// </summary>
        public static string HtmlHalfWidthSpace { get; } = "&nbsp;";

        /// <summary>
        /// スクリプトの改行
        /// </summary>
        public static string ScriptLinefeed { get; } = "\\n";

        /// <summary>
        /// HTMLの改行
        /// </summary>
        public static string HtmlLinefeed { get; } = "<br />";
        
        /// <summary>
        /// ダウンロード用フォルダーパス
        /// </summary>
        public static string DownloadFolderPath { get; } = "~/Download";

        /// <summary>
        /// 法規制イメージフォルダーパス
        /// </summary>
        public static string RegulationImageFolderName { get; } = "RegulationImage";

        /// <summary>
        /// 変換項目プレフィックス
        /// </summary>
        public static string ConvertColumnPrefix { get; } = "CONVERT_";

        /// <summary>
        /// 選択ボタンプレフィックス
        /// </summary>
        public static string SelectButtonPrefix { get; } = "SELECT_";

        /// <summary>
        /// チェック済みの文字
        /// </summary>
        public static string CheckedString { get; } = "レ";

        /// <summary>
        /// 未チェックの文字
        /// </summary>
        public static string NotCheckString { get; } = "";

        //2018/08/14 TPE.Sugimoto Add Sta
        /// <summary>
        /// チェック済みの文字
        /// </summary>
        public static string SelCheckedString { get; } = "選択可";
        //SEL_CheckedString
        /// <summary>
        /// 未チェックの文字
        /// </summary>
        public static string SelNotCheckString { get; } = "選択不可";
        //SEL_NotCheckString
        //2018/08/14 TPE.Sugimoto Add End

        //2019/03/04 TPE.Rin Add Start
        public static string ExaCheckedString { get; } = "該当";
        //EXA_CheckedString
        public static string ExaNotCheckString { get; } = "非該当";
        //EXA_NotCheckString
        public static string MeaCheckedString { get; } = "該当";
        //MEA_CheckedString
        public static string MEANotCheckString { get; } = "非該当";

        public static string RaCheckedString { get; } = "該当";

        public static string RaNotCheckString { get; } = "非該当";
        //2019/03/04 TPE.Rin Add End

        /// <summary>
        /// 必須項目のスタイルを定義します。
        /// </summary>
        public static string RequiredStyle { get; } = " required";

        /// <summary>
        /// バーコードファイル名のプレフィックスを定義します。
        /// </summary>
        public static string LabelFileNamePrefix { get; } = "";

        /// <summary>
        /// バーコードファイル名のサフィックスを定義します。
        /// </summary>
        public static string LabelFileNameSuffix { get; } = ".xlsx";

        /// <summary>
        /// パラメーターが不正な場合のメッセージを定義します。
        /// </summary>
        public static string ParameterErrorMessage { get; } = "パラメーターが不正です。";

        /// <summary>
        /// 複数の選択値を連結した文字列にする場合の区切り文字を定義します。
        /// </summary>
        public static string SelectionsDelimiter { get; } = ",";

        /// <summary>
        /// コードを連結した文字列にする場合の区切り文字を定義します。
        /// </summary>
        public static string CodeDelimiter { get; } = "|";

        /// <summary>
        /// 複数の値を1つの文字列にする場合の区切り文字を定義します。
        /// </summary>
        public static string ValuesDelimiter { get; } = "/";

        /// <summary>
        /// ダイアログ横サイズ(pixel)を定義します。
        /// </summary>
        public static string DialogHeight { get; } = "726";

        /// <summary>
        /// ダイアログ縦サイズ(pixel)を定義します。
        /// </summary>
        public static string DialogWidth { get; } = "1276";

        /// <summary>
        /// 選択表示の未選択時のスタイルを定義します。
        /// </summary>
        public static string SelectionViewUnsetStyle { get; } = "selectionViewUnset";

        /// <summary>
        /// 選択表示の未選択時の文字を定義します。
        /// </summary>
        public static string SelectionViewUnsetText { get; } = string.Empty;

        /// <summary>
        /// 選択表示の選択時のスタイルを定義します。
        /// </summary>
        public static string SelectionViewSetStyle { get; } = "";

        /// <summary>
        /// 選択表示の選択時の文字を定義します。
        /// </summary>
        public static string SelectionViewSetText { get; } = "設定済み";

        /// <summary>
        /// グループユーザービュー参照時のユーザーコードの接頭語を定義します。
        /// </summary>
        public static string GroupUserUserCodePrefix { get; } = "A";

        /// <summary>
        /// 複数の選択値を連結した文字列にする場合の区切り文字を定義します。
        /// </summary>
        public static string SelectionsSemicolon { get; } = ";";

    }

    /// <summary>
    /// 選択タイプ
    /// </summary>
    public enum SelectType
    {
        /// <summary>
        /// 単一
        /// </summary>
        One,
        /// <summary>
        /// 末端単一
        /// </summary>
        OneTerminal,
        /// <summary>
        /// 複数
        /// </summary>
        Multiple,
        /// <summary>
        /// 末端複数
        /// </summary>
        MultipleTerminal
    }

    public enum Mode
    {
        /// <summary>
        /// 新規登録
        /// </summary>
        New,
        /// <summary>
        /// 複写登録
        /// </summary>
        Copy,
        /// <summary>
        /// 編集
        /// </summary>
        Edit,
        /// <summary>
        /// 表示
        /// </summary>
        View,
        /// <summary>
        /// 抹消済み
        /// </summary>
        Deleted,
        /// <summary>
        /// 登録後参照
        /// </summary>
        ReferenceAfterRegistration,
        /// <summary>
        /// 廃止申請
        /// </summary>
        Abolition,
        /// <summary>
        /// 承認
        /// </summary>
        Approval,
        /// <summary>
        /// 一時保存（既存なし）
        /// </summary>
        TemporaryNew, // 20180723 FJ)Shinagawa Add
        /// <summary>
        /// 一時保存（既存あり）
        /// </summary>
        TemporaryEdit // 20180723 FJ)Shinagawa Add
    }

    /// <summary>
    /// 削除フラグ
    /// </summary>
    public enum DEL_FLAG
    {
        /// <summary>
        /// 未削除
        /// </summary>
        Undelete = 0,

        /// <summary>
        /// 削除済
        /// </summary>
        Deleted = 1,

        /// <summary>
        /// 一時保存
        /// </summary>
        Temporary = 2 // 20180723 FJ)Shinagawa Add
    }

    public enum Check
    {
        /// <summary>
        /// チェック無し
        /// </summary>
        NotCheck = 0,

        /// <summary>
        /// チェック有り
        /// </summary>
        Checked = 1
    }

    public enum WeightManagement
    {
        /// <summary>
        /// 無し
        /// </summary>
        None = 0,

        /// <summary>
        /// 推奨
        /// </summary>
        Recommendation = 1,

        /// <summary>
        /// 必須
        /// </summary>
        Required = 2
    }
    
    /// <summary>
    /// 作業記録管理
    /// </summary>
    public enum WorkingTimesManagement
    {
        /// <summary>
        /// 非該当
        /// </summary>
        NotTarget = 1,

        /// <summary>
        /// 該当
        /// </summary>
        Target = 2
    }

    /// <summary>
    /// ばく露作業報告
    /// </summary>
    public enum ExposureWorkReport
    {
        /// <summary>
        /// 非該当
        /// </summary>
        NotTarget = 1,

        /// <summary>
        /// 該当
        /// </summary>
        Target = 2
    }

    /// <summary>
    /// 法規制のCLASS_ID
    /// </summary>
    public enum CLASS_ID
    {
        /// <summary>
        /// 法規制
        /// </summary>
        Regulation = 1,
        /// <summary>
        /// GHS分類
        /// </summary>
        Gas = 2,
        /// <summary>
        /// 複数
        /// </summary>
        Fire = 3,
        /// <summary>
        /// 末端複数
        /// </summary>
        Office = 4
    }

    /// <summary>
    ///  保管場所
    /// </summary>
    public enum LOCATION
    {
        /// <summary>
        /// 保管場所
        /// </summary>
        StoringLocation = 1,

        /// <summary>
        /// 使用場所
        /// </summary>
        UsageLocation = 2
    }

    public static class DatabaseType
    {
        public static string Oracle { get; } = "Oracle";
        public static string SqlServer { get; } = "SqlServer";
    }
}