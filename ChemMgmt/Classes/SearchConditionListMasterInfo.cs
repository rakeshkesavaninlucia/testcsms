﻿using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Classes
{
    public enum SearchConditionType
    {
        /// <summary>
        /// 部分一致
        /// </summary>
        PartialMatching,

        /// <summary>
        /// 前方一致
        /// </summary>
        ForwardMatching,

        /// <summary>
        /// 後方一致
        /// </summary>
        BackwardMatching,

        /// <summary>
        /// 完全一致
        /// </summary>
        PerfectMatching
    }

    /// <summary>
    /// 検索条件を取得するクラスです。
    /// </summary>
    public class SearchConditionListMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        

        /// <summary>
        /// 選択項目に空白を追加するか
        /// </summary>
        protected override bool IsAddSpace { get; set; } = false;

        /// <summary>
        /// 全検索条件を取得します。
        /// </summary>
        /// <returns>全検索条件を<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var searchCondition = new List<CommonDataModel<int?>>();
            searchCondition.Add(new CommonDataModel<int?>((int)SearchConditionType.PartialMatching, "部分一致", 0));
            searchCondition.Add(new CommonDataModel<int?>((int)SearchConditionType.ForwardMatching, "前方一致", 1));
            searchCondition.Add(new CommonDataModel<int?>((int)SearchConditionType.BackwardMatching, "後方一致", 2));
            searchCondition.Add(new CommonDataModel<int?>((int)SearchConditionType.PerfectMatching, "完全一致", 3));
            return searchCondition;
        }
    }
}