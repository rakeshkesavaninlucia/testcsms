﻿using ChemMgmt.Models;
using ChemMgmt.Nikon;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ChemMgmt.Classes
{
    public class DropDownLists
    {
        CsmsCommonInfo dropdownlist = new CsmsCommonInfo();
        public CsmsCommonInfo ddlChemList()
        {
            List<SelectListItem> listFigure = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.FIGURE).GetSelectList().ToList().
               ForEach(x => listFigure.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupFigure = listFigure;

            List<SelectListItem> listClassification = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.CHEM_CAT).GetSelectList().ToList().
               ForEach(x => listClassification.Add(new SelectListItem
               { Value = x.Name, Text = x.Name}));
            dropdownlist.LookupClassification = listClassification;


            List<SelectListItem> listKeyMgmt = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.KEY_MGMT).GetSelectList().ToList().
               ForEach(x => listKeyMgmt.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupKeyMgmt = listKeyMgmt;


            List<SelectListItem> listWeightMgmt = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.WEIGHT_MGMT).GetSelectList().ToList().
               ForEach(x => listWeightMgmt.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupWeightMgmt = listWeightMgmt;


            List<SelectListItem> listChemSubCondition = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listChemSubCondition.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupChemSubCode = listChemSubCondition;


            List<SelectListItem> listChemNameCondition = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listChemNameCondition.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupChemNameCondition = listChemNameCondition;


            List<SelectListItem> listProductName = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listProductName.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupProductName = listProductName;


            List<SelectListItem> listCas = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listCas.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupCas = listCas;


            List<SelectListItem> listManufacturerName = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listManufacturerName.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupManufacturerName = listManufacturerName;


            List<SelectListItem> listSpeMedChekup = new List<SelectListItem> { };
            new ExaminationListMasterInfo().GetSelectList().ToList().
               ForEach(x => listSpeMedChekup.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupSpeMedChekup = listSpeMedChekup;


            List<SelectListItem> listWorkEnvMeasurement = new List<SelectListItem> { };
            new MeasurementListMasterInfo().GetSelectList().ToList().
               ForEach(x => listWorkEnvMeasurement.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupWorkEnvMeasurement = listWorkEnvMeasurement;

            List<SelectListItem> listContent = new List<SelectListItem> { };
            new IncludesContainedListMasterInfo().GetSelectList().ToList().
               ForEach(x => listContent.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupContent = listContent;

            List<SelectListItem> listState = new List<SelectListItem> { };
            new UsageStatusListMasterInfo().GetSelectList().ToList().
               ForEach(x => listState.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupState = listState;

            List<SelectListItem> listOfManufacturer = new List<SelectListItem> { };
            new MakerMasterInfo().GetSelectList().ToList().
                ForEach(x => listOfManufacturer.Add(new SelectListItem
                { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupOfManufacturer = listOfManufacturer;

            List<SelectListItem> unitSizeMasterInfo = new List<SelectListItem> { };
            new UnitSizeMasterInfo().GetSelectList().ToList().
                   ForEach(x => unitSizeMasterInfo.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupUnitSizeMasterInfo = unitSizeMasterInfo;
        

            return dropdownlist;
        }
    }
}