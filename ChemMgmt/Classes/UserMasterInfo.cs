﻿using COE.Common;
using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using ZyCoaG.Util;
using ZyCoaG;
using ChemMgmt.Models;
using ZyCoaG.Classes.util;

namespace ChemMgmt.Classes
{
    /// <summary>
    /// ユーザーマスター情報を取得するクラスです。
    /// </summary>
    public class UserMasterInfo : CommonDataModelMasterInfoBase<string>
    {
        /// <summary>
        /// データを全て取得します。
        /// </summary>
        /// <returns>データを<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<string>> GetAllData()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(M_USER.USER_CD) + ",");
            sql.AppendLine(" " + nameof(M_USER.USER_NAME) + ",");
            sql.AppendLine(" " + nameof(M_USER.DEL_FLAG));
            sql.AppendLine("from");
            sql.AppendLine(" " + nameof(M_USER));

            var parameters = new Hashtable();

            DbContext context = null;
            context = DbUtil.Open(true);
            DataTable records = DbUtil.Select(context, sql.ToString(), parameters);
            context.Close();

            var users = new List<CommonDataModel<string>>();
            foreach (DataRow record in records.Rows)
            {
                var user = new CommonDataModel<string>();
                user.Id = record[nameof(M_USER.USER_CD)].ToString();
                user.Name = record[nameof(M_USER.USER_NAME)].ToString();
                user.IsDeleted = Convert.ToInt32(record[nameof(M_USER.DEL_FLAG)].ToString()).ToBool();
                users.Add(user);
            }
            return users;
        }

        /// <summary>
        /// ユーザーマスターを取得する
        /// </summary>
        /// <param name="USER_CD">ユーザーコード</param>
        /// <returns>ユーザーマスター</returns>
        public M_USER GetUser(string USER_CD)
        {
            var sql = new StringBuilder();
            sql.AppendLine("select * from " + nameof(M_USER));
            sql.AppendLine(" where " + nameof(M_USER.USER_CD) + " = :" + nameof(M_USER.USER_CD));
            var parameters = new Hashtable();
            parameters.Add(nameof(M_USER.USER_CD), USER_CD);
            var context = DbUtil.Open(true);
            DataTable records = DbUtil.Select(context, sql.ToString(), parameters);
            context.Close();
            foreach (DataRow record in records.Rows)
            {
                var result = new M_USER();
                result.SetDataRow(record);
                return result;
            }
            return null;
        }
    }
}
