﻿using ChemMgmt.Models;
using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Classes
{
    public class GroupMasterInfo : CommonDataModelMasterInfoBase<string>
    {
        /// <summary>
        /// データを全て取得します。
        /// </summary>
        /// <returns>データを<see cref="IEnumerable{CommonDataModel{string}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<string>> GetAllData()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + nameof(V_GROUP.GROUP_ID) + ",");
            sql.AppendLine(" " + nameof(V_GROUP.GROUP_CD) + ",");
            sql.AppendLine(" " + nameof(V_GROUP.GROUP_NAME) + ",");
            sql.AppendLine(" " + nameof(V_GROUP.DEL_FLAG) + "");
            sql.AppendLine("from");
            sql.AppendLine(" " + nameof(V_GROUP));
            sql.AppendLine("order by");
            sql.AppendLine(" " + nameof(V_GROUP.GROUP_CD) + ",");
            sql.AppendLine(" " + nameof(V_GROUP.GROUP_NAME));

            var parameters = new Hashtable();

            DbContext context = null;
            context = DbUtil.Open(true);
            DataTable records = DbUtil.Select(context, sql.ToString(), parameters);
            context.Close();

            var groups = new List<CommonDataModel<string>>();
            var i = 1;
            foreach (DataRow record in records.Rows)
            {
                var group = new CommonDataModel<string>();
                group.Id = record[nameof(V_GROUP.GROUP_CD)].ToString();
                group.Name = record[nameof(V_GROUP.GROUP_NAME)].ToString();
                group.IsDeleted = Convert.ToInt32(record[nameof(V_GROUP.DEL_FLAG)].ToString()).ToBool();
                group.Order = i;
                groups.Add(group);
                i++;
            }
            return groups;
        }

        /// <summary>
        /// 上位部署の一覧を取得する
        /// </summary>
        /// <param name="GROUP_CD">部署CD</param>
        /// <param name="addMyGroup">自分自身の部署を含めるか</param>
        /// <returns>上位部署の一覧</returns>
        public IEnumerable<string> GetParentGroups(string GROUP_CD, bool addMyGroup = false, int? count = null)
        {
            var result = new List<string>();
            DbContext context = DbUtil.Open(true);
            try
            {
                GetLinkedGroups(result, GROUP_CD, count, context,
                    "select P_GROUP_CD FROM M_GROUP where DEL_FLAG = :DEL_FLAG and P_GROUP_CD is not null and GROUP_CD = :GROUP_CD",
                    "select P_GROUP_CD FROM M_GROUP where DEL_FLAG = :DEL_FLAG and P_GROUP_CD is not null and GROUP_CD in (");
            }
            finally
            {
                context.Close();
            }
            if (addMyGroup)
            {
                result.Add(GROUP_CD);
            }
            return result;
        }

        /// <summary>
        /// 下位部署コードを取得する
        /// </summary>
        /// <param name="GROUP_CD">部署CD</param>
        /// <param name="addMyGroup">自分自身の部署を含めるか</param>
        /// <returns>下位部署の一覧</returns>
        public IEnumerable<string> GetChildGroups(string GROUP_CD, bool addMyGroup = false, int? count = null)
        {
            var result = new List<string>();
            if (addMyGroup)
            {
                result.Add(GROUP_CD);
            }
            DbContext context = DbUtil.Open(true);
            try
            {
                GetLinkedGroups(result, GROUP_CD, count, context,
                    "select GROUP_CD from M_GROUP where DEL_FLAG = :DEL_FLAG and GROUP_CD is not null and P_GROUP_CD = :GROUP_CD",
                    "select GROUP_CD from M_GROUP where DEL_FLAG = :DEL_FLAG and GROUP_CD is not null and P_GROUP_CD in (");
            }
            finally
            {
                context.Close();
            }
            return result;
        }

        private static void GetLinkedGroups(List<string> result, string GROUP_CD, int? hierarchy,
            DbContext context, string baseSql, string levelSql)
        {
            var level = 1;
            var count = 0;
            var sql = new StringBuilder();
            var parametors = new Hashtable();
            parametors.Add(nameof(M_GROUP.DEL_FLAG), (int)DEL_FLAG.Undelete);
            parametors.Add(nameof(GROUP_CD), GROUP_CD);
            DataTable records = null;
            while (!hierarchy.HasValue || (hierarchy.HasValue && level <= hierarchy))
            {
                if (level == 1)
                {
                    sql.AppendLine(baseSql);
                }
                else
                {
                    sql.AppendLine("union");
                    for (var i = 1; i < level; i++)
                    {
                        sql.AppendLine(levelSql);
                    }
                    sql.AppendLine(baseSql);
                    for (var i = 1; i < level; i++)
                    {
                        sql.AppendLine(")");
                    }
                }
                records = DbUtil.Select(context, sql.ToString(), parametors);
                if (count == records.Rows.Count)
                {
                    break;
                }
                count = records.Rows.Count;
                level++;
            }
            if (records != null)
            {
                foreach (DataRow row in records.Rows)
                {
                    result.Add((string)(row[0]));
                }
            }
        }
    }
}