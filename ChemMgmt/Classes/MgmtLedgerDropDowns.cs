﻿using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Ledger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ChemMgmt.Classes
{
    public class MgmtLedgerDropDowns
    {
        //LedgerCommonInfo dropdownlist = new LedgerCommonInfo();
        LedgerUsageLocationModel dropdownlist1 = new LedgerUsageLocationModel();
        public LedgerCommonInfo ddlLedgerList(LedgerCommonInfo dropdownlist)
        {

            List<SelectListItem> listChemNameCondition = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listChemNameCondition.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupChemNameCondition = listChemNameCondition;


            List<SelectListItem> listProductName = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listProductName.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupProductName = listProductName;

            List<SelectListItem> listCas = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listCas.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupCas = listCas;

            List<SelectListItem> listRegistrationNo = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listRegistrationNo.Add(new SelectListItem
               { Value = x.Id.ToString().ToString(), Text = x.Name }));
            dropdownlist.LookupRegistrationNo = listRegistrationNo;

            List<SelectListItem> listState = new List<SelectListItem> { };
            new LedgerStatusListMasterInfo().GetSelectList().ToList().
               ForEach(x => listState.Add(new SelectListItem
               { Value = x.Id.ToString().ToString(), Text = x.Name }));
            dropdownlist.LookupState = listState;

            List<SelectListItem> listReason = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.REASON).GetSelectList().ToList().
               ForEach(x => listReason.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupReason = listReason;

            List<SelectListItem> listregtransactionvolume = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.REG_TRANSACTION_VOLUME).GetSelectList().ToList().
               ForEach(x => listregtransactionvolume.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupREG_TRANSACTION_VOLUME = listregtransactionvolume;

            List<SelectListItem> listregworkfrequency = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.REG_WORK_FREQUENCY).GetSelectList().ToList().
               ForEach(x => listregworkfrequency.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupREG_WORK_FREQUENCY = listregworkfrequency;

            List<SelectListItem> listregdisasterpossibility = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.REG_DISASTER_POSSIBILITY).GetSelectList().ToList().
               ForEach(x => listregdisasterpossibility.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupREG_DISASTER_POSSIBILITY = listregdisasterpossibility;

            List<SelectListItem> listsubcontainer = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetSelectList().ToList().
               ForEach(x => listsubcontainer.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupSUB_CONTAINER = listsubcontainer;

        List<SelectListItem> listOfManufacturer = new List<SelectListItem> { };
            new MakerMasterInfo().GetSelectList().ToList().
                ForEach(x => listOfManufacturer.Add(new SelectListItem
                { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupManufacturerName = listOfManufacturer;


            List<SelectListItem> unitSizeMasterInfo = new List<SelectListItem> { };
            new UnitSizeMasterInfo().GetSelectList().ToList().
                   ForEach(x => unitSizeMasterInfo.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupUnitSizeMasterInfo = unitSizeMasterInfo;

            List<SelectListItem> ishaUsageMasterInfo = new List<SelectListItem> { };
            new IshaUsageMasterInfo().GetSelectList().ToList().
                   ForEach(x => ishaUsageMasterInfo.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist.LookupUsageMasterInfo = ishaUsageMasterInfo;




            return dropdownlist;

        }

        public LedgerUsageLocationModel ddlUsageLocList(LedgerUsageLocationModel dropdownlist1)
        {

            List<SelectListItem> listChemNameCondition = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listChemNameCondition.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupChemNameCondition = listChemNameCondition;


            List<SelectListItem> listProductName = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listProductName.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupProductName = listProductName;

            List<SelectListItem> listCas = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listCas.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupCas = listCas;

            List<SelectListItem> listRegistrationNo = new List<SelectListItem> { };
            new SearchConditionListMasterInfo().GetSelectList().ToList().
               ForEach(x => listRegistrationNo.Add(new SelectListItem
               { Value = x.Id.ToString().ToString(), Text = x.Name }));
            dropdownlist1.LookupRegistrationNo = listRegistrationNo;

            List<SelectListItem> listState = new List<SelectListItem> { };
            new LedgerStatusListMasterInfo().GetSelectList().ToList().
               ForEach(x => listState.Add(new SelectListItem
               { Value = x.Id.ToString().ToString(), Text = x.Name }));
            dropdownlist1.LookupState = listState;

            List<SelectListItem> listReason = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.REASON).GetSelectList().ToList().
               ForEach(x => listReason.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupReason = listReason;

            List<SelectListItem> listregtransactionvolume = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.REG_TRANSACTION_VOLUME).GetSelectList().ToList().
               ForEach(x => listregtransactionvolume.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupREG_TRANSACTION_VOLUME = listregtransactionvolume;

            List<SelectListItem> listregworkfrequency = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.REG_WORK_FREQUENCY).GetSelectList().ToList().
               ForEach(x => listregworkfrequency.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupREG_WORK_FREQUENCY = listregworkfrequency;

            List<SelectListItem> listregdisasterpossibility = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.REG_DISASTER_POSSIBILITY).GetSelectList().ToList().
               ForEach(x => listregdisasterpossibility.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupREG_DISASTER_POSSIBILITY = listregdisasterpossibility;

            List<SelectListItem> listsubcontainer = new List<SelectListItem> { };
            new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetSelectList().ToList().
               ForEach(x => listsubcontainer.Add(new SelectListItem
               { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupSUB_CONTAINER = listsubcontainer;

            List<SelectListItem> listOfManufacturer = new List<SelectListItem> { };
            new MakerMasterInfo().GetSelectList().ToList().
                ForEach(x => listOfManufacturer.Add(new SelectListItem
                { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupManufacturerName = listOfManufacturer;

            List<SelectListItem> unitSizeMasterInfo = new List<SelectListItem> { };
            new UnitSizeMasterInfo().GetSelectList().ToList().
                   ForEach(x => unitSizeMasterInfo.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupUnitSizeMasterInfo = unitSizeMasterInfo;

            List<SelectListItem> ishaUsageMasterInfo = new List<SelectListItem> { };
            new IshaUsageMasterInfo().GetSelectList().ToList().
                   ForEach(x => ishaUsageMasterInfo.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
            dropdownlist1.LookupUsageMasterInfo = ishaUsageMasterInfo;

            return dropdownlist1;

        }
    }
}