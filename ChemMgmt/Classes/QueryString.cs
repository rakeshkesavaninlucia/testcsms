﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Classes
{
    /// <summary>
    /// 画面間のクエリ文字列を定義します。
    /// </summary>
    public class QueryString
    {
        /// <summary>
        /// 画面間でテーブル名を渡す際の名前を定義します。
        /// </summary>
        public const string TableNameKey = "TableName";

        /// <summary>
        /// 画面間で項目名を渡す際の名前を定義します。
        /// </summary>
        public const string ColumnNameKey = "ColumnName";

        /// <summary>
        /// 画面間で選択タイプを渡す際の名前を定義します。
        /// </summary>
        public const string SelectTypeKey = "SelectType";

        /// <summary>
        /// 画面間でメンテナンスのモードを渡す際の名前を定義します。
        /// </summary>
        public const string ModeKey = "Mode";

        /// <summary>
        /// 画面間でIdを渡す際の名前を定義します。
        /// </summary>
        public const string IdKey = "Id";

        /// <summary>
        /// 画面間で行識別Idを渡す際の名前を定義します。
        /// </summary>
        public const string RowIdKey = "RowId";

        /// <summary>
        /// 選択画面の親要素となる項目のキーを渡す際の名前を定義します。
        /// </summary>
        public const string RootKey = "Root";

        /// <summary>
        /// 選択画面の表示項目を絞るキーを渡す際の名前を定義します。
        /// </summary>
        public const string FilterKey = "Filter";

        /// <summary>
        /// 選択画面にセッション名を渡す際の名前を定義します。
        /// </summary>
        public const string SessionNameKey = "Session";

        /// <summary>
        /// 画面間で登録番号を渡す際の名前を定義します。
        /// </summary>
        public const string RegNoKey = "Regno";

        /// <summary>
        /// 画面間で登録番号を渡す際の名前を定義します。
        /// </summary>
        public const string LedgerFlowIdNoKey = "Id";

        /// <summary>
        /// 画面間でタイプを渡す際の名前を定義します。
        /// </summary>
        public const string TypeKey = "Type";

        /// <summary>
        /// 画面間で選択必須かを渡す際の名前を定義します。
        /// </summary>
        public const string IsSelectRequired = "IsSelectRequired";
        //isSelectRequired
        /// <summary>
        /// 画面間で削除項目を対象とするかを定義します。
        /// </summary>
        public const string isDelete = "IsDelete";

        /// <summary>
        /// 画面間でクラスID項目を対象とするかを定義します。
        /// </summary>
        public const string ClassId = "ClassId";


    }
}