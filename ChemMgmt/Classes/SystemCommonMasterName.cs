﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Classes
{
    public static class SystemCommonMasterName
    {

        /// <summary>
        /// 権限名
        /// </summary>
        public const string GrantName = "M_GRANT_NAME";

        public const string GrantUserName = "M_USER";
    }
}