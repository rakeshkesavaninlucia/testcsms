﻿using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using ChemMgmt.Classes.util;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.Util;
using ZyCoaG.Classes.util;

namespace ChemMgmt.Classes
{
    public class CommonMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        /// <summary>
        /// マスター名を取得します。
        /// </summary>
        public string MasterName { get; private set; }

        /// <summary>
        /// 引数を指定しないコンストラクタが呼び出されないように隠ぺいします。
        /// </summary>
        private CommonMasterInfo() { }

        /// <summary>
        /// 汎用マスターから参照するマスター名を指定し、<see cref="CommonMasterInfo"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        /// <param name="MasterName">汎用マスターから取得するマスター名を指定します。</param>
        /// <exception cref="NotImplementedException"><see cref="MasterName"/>がnullの場合に返します。</exception>
        public CommonMasterInfo(string masterName)
        {
            if (masterName?.Trim() == null) throw new ArgumentNullException(masterName);
            this.MasterName = masterName;
            DataArray = GetAllData();
        }




        
        /// <summary>
        /// 汎用マスターから指定したマスターのデータを全て取得します。
        /// </summary>
        /// <returns>データを<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            if (string.IsNullOrWhiteSpace(MasterName)) return new List<CommonDataModel<int?>>().AsEnumerable();

            var master = new List<CommonDataModel<int?>>();

            var stbsql = new StringBuilder();
            stbsql.AppendLine("select");
            stbsql.AppendLine(" KEY_VALUE,");
            stbsql.AppendLine(" DISPLAY_VALUE,");
            stbsql.AppendLine(" DISPLAY_ORDER,");
            stbsql.AppendLine(" DEL_FLAG");
            stbsql.AppendLine("from");
            stbsql.AppendLine(" M_COMMON");
            stbsql.AppendLine("where");
            stbsql.AppendLine(" TABLE_NAME = @TABLE_NAME");

            var parameters = new DynamicParameters();
            parameters.Add("@TABLE_NAME", MasterName);

            //DbContext context = null;
            SqlConnection connection = DbUtil.Open();  // with Transaction
            List<M_COMMON> records;
            try
            {
                records = DbUtil.Select<M_COMMON>(stbsql.ToString(), parameters);
                connection.BeginTransaction().Commit();
                connection.Close();
            }
            catch (Exception ex)
            {
                connection.BeginTransaction().Rollback();
                connection.Close();
                throw ex;
            }

            // DataTable records = DbUtil.select(context, sql.ToString(), parameters);


            var dataArray = new List<CommonDataModel<int?>>();
            foreach (var record in records)
            {
                var data = new CommonDataModel<int?>();
        data.Id = OrgConvert.ToNullableInt32(record.KEY_VALUE);
                data.Name = record.DISPLAY_VALUE;
                data.Order = record.DISPLAY_ORDER != null ? record.DISPLAY_ORDER.ToString().ToInt() : 0;
                data.IsDeleted = Convert.ToInt32(record.DEL_FLAG.ToString()).ToBool();
                //data.IsDeleted = record.DEL_FLAG;
                //if (del_flag!=null) {
                //    data.IsDeleted = del_flag.ToBool();
                //}
                //else
                //{
                //    data.IsDeleted = false;
                //}
                dataArray.Add(data);
            }

            return dataArray;
        }

/// <summary>
/// 複数選択可能な項目の場合、選択中のアイテムを取得します。
/// </summary>
/// <param name="IdSummary">選択中のIdの合計を指定します。</param>
/// <returns>選択中のアイテムを<see cref="CommonDataModel{int?}"/>で返します。</returns>
public IEnumerable<CommonDataModel<int?>> GetSelectingItems(int? IdSummary)
{
    var dataArray = DataArray.Where(x => x.Id > 0).OrderBy(x => x.Order).ThenBy(x => x.Name).ToList();
    var selectingItems = new List<CommonDataModel<int?>>();
    if (IdSummary == null || IdSummary == 0)
    {
        selectingItems.Add(DataArray.Where(x => x.Id == 0).Single());
        return selectingItems;
    }
    foreach (var data in dataArray.Where(x => x.Id != 0).ToList())
    {
        selectingItems.Add(data);
    }
    return selectingItems;
}

        /// <summary>
        /// 複数選択可能な項目の場合、選択中のアイテムを取得します。
        /// </summary>
        /// <param name="IdSummary">選択中のIdの合計を指定します。</param>
        /// <returns>選択中のアイテムを<see cref="CommonDataModel{int?}"/>で返します。</returns>
        //public string GetSelectingItemsMergedString(int? IdSummary)
        //{
        //    var dataArray = GetSelectingItems(IdSummary).Select(x => x.Name).ToList();
        //    return string.Join(SystemConst.SelectionsDelimiter, dataArray);
        //}

    }
}