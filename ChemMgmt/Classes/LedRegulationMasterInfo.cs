﻿using ChemMgmt.Models;
using COE.Common;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Classes
{
    public class LedRegulationMasterInfo : IDisposable
    {
        protected DbContext _Context;
        public DbContext Context
        {
            protected get
            {
                return _Context;
            }
            set
            {
                IsInteriorTransaction = false;
                _Context = value;
            }
        }

        protected virtual bool IsInteriorTransaction { get; set; } = true;

        public IEnumerable<V_REGULATION> dataArray { get; private set; }

        public LedRegulationMasterInfo()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" V_REGULATION_ALL.REG_TYPE_ID,");
            sql.AppendLine(" V_REGULATION_ALL.CLASS_ID,");
            sql.AppendLine(" V_REGULATION_ALL.REG_TEXT,");
            sql.AppendLine(" V_REGULATION_ALL.REG_TEXT_BASE,");
            sql.AppendLine(" V_REGULATION_ALL.REG_TEXT_LAST,");
            sql.AppendLine(" V_REGULATION_ALL.REG_ICON_PATH,");
            sql.AppendLine(" V_REGULATION_ALL.P_REG_TYPE_ID,");
            sql.AppendLine(" V_REGULATION_ALL.SORT_ORDER,");
            sql.AppendLine(" V_REGULATION_ALL.WORKTIME_MGMT,");
            sql.AppendLine(" V_REGULATION_ALL.EXPOSURE_REPORT,");
            sql.AppendLine(" V_REGULATION_ALL.DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" V_REGULATION_ALL");

            var parameters = new DynamicParameters();

            StartTransaction();
            List<V_REGULATION> records = DbUtil.Select<V_REGULATION>(sql.ToString(), parameters);
            Close();

            var dataArray = new List<V_REGULATION>();
            foreach (var record in records)
            {
                var data = new V_REGULATION();
                data.REG_TYPE_ID = Convert.ToInt32(record.REG_TYPE_ID.ToString());
                data.CLASS_ID = Convert.ToInt32(record.CLASS_ID.ToString());
                if (record.REG_TEXT.ToString() == null)
                {
                    data.REG_TEXT = string.Empty;
                }
                else
                {
                    data.REG_TEXT = record.REG_TEXT.ToString();
                }

                if (record.REG_TEXT_BASE.ToString() == null)
                {
                    data.REG_TEXT_BASE = string.Empty;
                }
                else
                {
                    data.REG_TEXT_BASE = record.REG_TEXT_BASE.ToString();
                }
                if (record.REG_TEXT_LAST.ToString() == null)
                {
                    data.REG_TEXT_LAST = string.Empty;
                }
                else
                {
                    data.REG_TEXT_LAST = record.REG_TEXT_LAST.ToString();
                }
                if (record.REG_ICON_PATH == null)
                {
                    data.REG_ICON_PATH = string.Empty;
                }
                else
                {
                    data.REG_ICON_PATH = record.REG_ICON_PATH.ToString();
                }
                data.P_REG_TYPE_ID = OrgConvert.ToNullableInt32(record.P_REG_TYPE_ID.ToString());
                data.SORT_ORDER = OrgConvert.ToNullableInt32(record.SORT_ORDER.ToString());
                data.WORKTIME_MGMT = OrgConvert.ToNullableInt32(record.WORKTIME_MGMT.ToString());
                data.EXPOSURE_REPORT = OrgConvert.ToNullableInt32(record.EXPOSURE_REPORT.ToString());
                data.DEL_FLAG = OrgConvert.ToNullableInt32(record.DEL_FLAG.ToString());
                dataArray.Add(data);
            }
            this.dataArray = dataArray;
        }

        /// <summary>
        /// <para>変換辞書を取得します。</para>
        /// <para>重複したデータが存在した場合、後のデータが優先されます。</para>
        /// <para>基本機能→各社用の用語などのために後のデータを優先しています。</para>
        /// </summary>
        /// <returns>変換辞書を<see cref="IDictionary{T,string}"/>で返します。</returns>
        public virtual IDictionary<int?, string> GetDictionary()
        {
            var dic = new Dictionary<int?, string>();
            foreach (var data in dataArray) dic.Add(data.REG_TYPE_ID, data.REG_TEXT);
            return dic;
        }

        public IEnumerable<V_REGULATION> GetSelectedItems(IEnumerable<int> ids)
        {
            return dataArray.Where(x => ids.Contains(x.REG_TYPE_ID)).OrderBy(x => x.SORT_ORDER).ThenBy(x => x.REG_TYPE_ID);
        }

        //2019/04/04 TPE.Rin Add
        public V_REGULATION GetSelectedOneItems(int ids)
        {
            V_REGULATION reg = new V_REGULATION();
            foreach (V_REGULATION data in dataArray)
            {
                if (data.REG_TYPE_ID == ids)
                {
                    reg = data;
                }

            }
            return reg;
        }

        protected void StartTransaction()
        {
            if (_Context == null)
            {
                _Context = DbUtil.Open(true);
            }
        }

        protected void Close()
        {
            if (IsInteriorTransaction)
            {
                _Context.Close();
                _Context = null;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                    dataArray = null;
                    if (IsInteriorTransaction)
                    {
                        if (_Context != null)
                        {
                            _Context.Rollback();
                            _Context.Close();
                        }
                    }
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~RegulationInfo() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}