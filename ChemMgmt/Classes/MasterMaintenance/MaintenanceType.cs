﻿
using ChemMgmt.Classes.util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;

namespace ZyCoaG.MasterMaintenance
{
    /// <summary>
    /// マスターメンテナンスのメンテナンス区分を定義します。
    /// </summary>
    public enum MaintenanceType
    {
        /// <summary>
        /// 新規登録
        /// </summary>
        New,

        /// <summary>
        /// 編集
        /// </summary>
        Edit,

        /// <summary>
        /// 削除
        /// </summary>
        Delete,

        /// <summary>
        /// 参照登録
        /// </summary>
        Reference

    }
}

//using ChemMgmt.Classes.util;
//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Data;
//using System.Drawing;
//using System.Linq;
//using System.Text;
//using System.Web;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using WebCommon.Models;
//using ZyCoaG.BaseCommon;
//using ZyCoaG.Util;
////using ZyCoaG.PrintLabel;
////using static ZyCoaG.BaseCommon.Control;

//namespace ZyCoaG.MasterMaintenance
//{
//    /// <summary>
//    /// ユーザー設定画面
//    /// </summary>
//    /// <remarks>
//    /// ・InputCheck
//    /// </remarks>
//    public partial class ZC01022
//    {
//        #region クラス
//        /// <summary>
//        /// 遷移用クラス
//        /// </summary>
//        public class clsTransfer
//        {
//            public string USER_CD { get; set; }

//            public string USER_NAME { get; set; }

//            public string USER_NAME_KANA { get; set; }
//            public string GROUP_CD { get; set; }
//            public string GROUP_NAME { get; set; }
//            public string TEL { get; set; }
//            public string EMAIL { get; set; }
//            public string APPROVER1 { get; set; }
//            public string APPROVER2 { get; set; }
//            public string APPROVER1_NAME { get; set; }
//            public string APPROVER2_NAME { get; set; }
//            public string ADMIN_FLAG { get; set; }
//            public string PASSWORD { get; set; }
//            public string FLOW_CD { get; set; }
//            public string FLOW_NAME { get; set; }
//            public bool DEL_FLAG { get; set; }
//            public bool BATCH_FLAG { get; set; }
//            //public MasterMaintenance.MaintenanceType MAINTENANCE_TYPE { get; set; }
//        }
//        #endregion

//        #region 構造体
//        // 列名
//        struct ColumnName
//        {
//            public string MAKER_ID { get { return "メーカーID"; } }
//            public string MAKER_NAME { get { return "メーカー名"; } }
//            public string MAKER_ADDRESS { get { return "住所"; } }
//            public string MAKER_OLD_NAME { get { return "旧メーカー名"; } }
//        }
//        private ColumnName colName = new ColumnName();

//        // ID
//        struct ColumnID
//        {
//            public string MAKER_ID { get { return "MAKER_ID"; } }
//            public string MAKER_NAME { get { return "MAKER_NAME"; } }
//            public string MAKER_ADDRESS { get { return "MAKER_ADDRESS"; } }
//            public string MAKER_OLD_NAME { get { return "MAKER_OLD_NAME"; } }
//        }
//        private ColumnID colID = new ColumnID();

//        #endregion

//        #region メンバ変数
//        private LogUtil LogUtil = new LogUtil();
//        private UserInfo.clsUser clsUser;
//        private int editMode = EditMode.SelfEdit;
//        private List<UserInfo.clsUser> GetUserList = new List<UserInfo.clsUser>();
//        #endregion

//        #region 定数
//        private const string PROGRAM_ID = "ZC01022";
//        public const string cSESSION_NAME_Trans = "ZC01022_Trans";
//        private const string SELECT_COL_NAME = "check";
//        private readonly string[] CSV_COLUMN_HEADER = { "MAKER_ID" };

//        /// <summary>
//        /// メンテナンスモード（SelfEdit：ログインユーザー自身　AdminEdit：管理者権限）
//        /// </summary>
//        private static class EditMode
//        {
//            public const int SelfEdit = 1;
//            public const int AdminEdit = 2;
//            public const int NewEntry = 3;
//        }
//        #endregion


//        /// <summary>
//        /// 指定したユーザーコードのユーザーマスター情報を取得
//        /// </summary>
//        /// <param name="userCd"></param>
//        /// <param name="userInfo"></param>
//        /// <returns></returns>
//        // modified by for dapper.
//        private bool GetUserInfo(string userCd)//, out UserInfo.clsUser userInfo)
//        {
//            // ユーザーIDからユーザーマスター情報取得
//            bool ret = false;
//            var masterService = new MasterService();
//            List<WebCommonInfo> dt = masterService.selectAllUserInfo(userCd);

//            if (dt.Count != 1)
//            {
//                throw new Exception();
//            }
//            else
//            {
//                ret = true;
//            }
//            //else
//            //{
//            //    // 検索結果を設定
//                //userInfo = new UserInfo.clsUser();
//            //    userInfo.USER_CD = Convert.ToString(dt.Rows[0]["USER_CD"]);
//            //    userInfo.USER_NAME = Convert.ToString(dt.Rows[0]["USER_NAME"]);
//            //    userInfo.USER_NAME_KANA = Convert.ToString(dt.Rows[0]["USER_NAME_KANA"]);
//            //    userInfo.APPROVER1 = Convert.ToString(dt.Rows[0]["APPROVER1"]);
//            //    userInfo.APPROVER2 = Convert.ToString(dt.Rows[0]["APPROVER2"]);
//            //    userInfo.APPROVER1_NAME = Convert.ToString(dt.Rows[0]["APPROVER1_NAME"]);
//            //    userInfo.APPROVER2_NAME = Convert.ToString(dt.Rows[0]["APPROVER2_NAME"]);
//            //    userInfo.TEL = Convert.ToString(dt.Rows[0]["TEL"]);
//            //    userInfo.EMAIL = Convert.ToString(dt.Rows[0]["EMAIL"]);
//            //    userInfo.GROUP_CD = Convert.ToString(dt.Rows[0]["GROUP_CD"]);
//            //    userInfo.GROUP_NAME = Convert.ToString(dt.Rows[0]["GROUP_NAME"]);
//            //    userInfo.ADMINFLG = Convert.ToString(dt.Rows[0]["ADMIN_FLAG"]);
//            //    userInfo.FLOW_CD = Convert.ToString(dt.Rows[0]["FLOW_CD"]);
//            //    userInfo.DEL_FLAG = Convert.ToString(dt.Rows[0]["DEL_FLAG"]) != "1" ? false : true;
//            //    userInfo.BATCH_FLAG = Convert.ToString(dt.Rows[0]["BATCH_FLAG"]) != "1" ? false : true;

//            //}
//            // ユーザーマスター情報を「UserInfo.clsUser」にセット


//            return ret;
//        }






//        /// <summary>
//        /// ユーザー情報クラス情報を画面入力項目に設定
//        /// </summary>
//        /// <param name="objClsTransfer"></param>
//        private bool SetDataInfoClassToClsUser(clsTransfer objClsTransfer, out UserInfo.clsUser userInfo)
//        {
//            userInfo = new UserInfo.clsUser();
//            userInfo.USER_CD = objClsTransfer.USER_CD;
//            userInfo.USER_NAME = objClsTransfer.USER_NAME;
//            userInfo.USER_NAME_KANA = objClsTransfer.USER_NAME_KANA;
//            userInfo.GROUP_CD = objClsTransfer.GROUP_CD;
//            userInfo.GROUP_NAME = objClsTransfer.GROUP_NAME;
//            userInfo.EMAIL = objClsTransfer.EMAIL;
//            userInfo.TEL = objClsTransfer.TEL;
//            userInfo.APPROVER1 = objClsTransfer.APPROVER1;
//            userInfo.APPROVER1_NAME = objClsTransfer.APPROVER1_NAME;
//            userInfo.APPROVER2 = objClsTransfer.APPROVER2;
//            userInfo.APPROVER2_NAME = objClsTransfer.APPROVER2_NAME;
//            userInfo.ADMINFLG = objClsTransfer.ADMIN_FLAG;
//            userInfo.FLOW_CD = objClsTransfer.FLOW_CD;
//            userInfo.DEL_FLAG = objClsTransfer.DEL_FLAG;
//            userInfo.BATCH_FLAG = objClsTransfer.BATCH_FLAG;

//            return true;
//        }





//        /// <summary>
//        /// CSV出力処理を行います。
//        /// </summary>
//        private void DoCsvOutput()
//        {
//            //string sql;
//            //Hashtable _params = new Hashtable();
//            //_params.Add("USER_CD", clsUser.USER_CD);
//            //_params.Add("PATTERN_NAME", drpPattern.SelectedItem.Text);

//            //// Sql取得
//            //sql = GetSqlMakerList();

//            //DataTable dt = DbUtil.select(sql, _params);
//            ////MAKER_IDのみ抽出
//            //DataView dv = new DataView(dt);
//            //string[] columns = { "MAKER_ID" };
//            //dt = dv.ToTable(false, columns);
//            //Common.DownloadCsvFile(Response, dt, drpPattern.SelectedItem.Text + ".csv");
//        }

//        /// <summary>
//        /// 削除処理を行います。
//        /// </summary>
//        private void DoDelete()
//        {
//            //DbContext ctx = null;
//            //string sql;
//            //Hashtable _params = new Hashtable();

//            //try
//            //{
//            //    _params.Add("USER_CD", clsUser.USER_CD);
//            //    _params.Add("PATTERN_NAME", drpPattern.SelectedItem.Text);

//            //    ctx = DbUtil.Open(true);

//            //    // 削除
//            //    sql = GetSql_Delete();
//            //    DbUtil.execUpdate(ctx, sql, _params);

//            //    ctx.Commit();
//            //    ctx.Close();

//            //    // 完了アラート出力
//            //    Common.MsgBox(this, Common.GetMessage("I003"));
//            //}
//            //catch (Exception ex)
//            //{
//            //    if (ctx != null)
//            //    {
//            //        ctx.Rollback();
//            //        ctx.Close();
//            //    }
//            //    throw ex;
//            //}
//        }


//        /// <summary>
//        /// M_MAKER MAKER_ID用SQLを取得します。
//        /// </summary>
//        /// <returns>作成SQL文</returns>
//        private string GetSqlMakerIdCnt()
//        {
//            StringBuilder sbSql = new StringBuilder();

//            sbSql.Append("SELECT");
//            sbSql.Append("    COUNT(MAKER_ID) CNT");
//            sbSql.Append(" FROM");
//            sbSql.Append("    M_MAKER");
//            sbSql.Append(" WHERE");
//            sbSql.Append("    MAKER_ID = :MAKER_ID");

//            return sbSql.ToString();
//        }

//        /// <summary>
//        /// M_USER_MAKER PATTERN_NAME用SQLを取得します。
//        /// </summary>
//        /// <returns>作成SQL文</returns>
//        private string GetSqlPatternName()
//        {
//            StringBuilder sbSql = new StringBuilder();

//            sbSql.Append("SELECT");
//            sbSql.Append("    DISTINCT PATTERN_NAME");
//            sbSql.Append(" FROM");
//            sbSql.Append("    M_USER_MAKER");
//            sbSql.Append(" WHERE");
//            sbSql.Append("    USER_CD = :USER_CD");
//            sbSql.Append("    AND DEL_FLAG <> 1");
//            sbSql.Append(" ORDER BY");
//            sbSql.Append("    PATTERN_NAME");

//            return sbSql.ToString();
//        }

//        /// <summary>
//        /// M_MAKER検索用SQLを取得します。
//        /// </summary>
//        /// <returns>作成SQL文</returns>
//        private string GetSqlMakerList()
//        {
//            StringBuilder sbSql = new StringBuilder();

//            sbSql.Append("SELECT");
//            sbSql.Append("    M.MAKER_ID");
//            sbSql.Append("    , M.MAKER_NAME");
//            sbSql.Append("    , M.MAKER_ADDRESS");
//            sbSql.Append("    , MS.MAKER_SYN_NAME MAKER_OLD_NAME");
//            sbSql.Append(" FROM");
//            sbSql.Append("    M_USER_MAKER UM");
//            sbSql.Append("    INNER JOIN M_MAKER M");
//            sbSql.Append("       ON UM.MAKER_ID = M.MAKER_ID");
//            sbSql.Append("    LEFT JOIN M_MAKER_SYNONYM MS");
//            sbSql.Append("       ON M.MAKER_ID = MS.MAKER_ID");
//            sbSql.Append("       AND MS.DEL_FLAG <> 1");
//            sbSql.Append("       AND MS.MAKER_SYN_NAME <> M.MAKER_NAME");
//            sbSql.Append(" WHERE");
//            sbSql.Append("    UM.DEL_FLAG <> 1");
//            sbSql.Append("    AND M.DEL_FLAG <> 1");
//            sbSql.Append("    AND UM.USER_CD = :USER_CD");
//            sbSql.Append("    AND UM.PATTERN_NAME = :PATTERN_NAME");
//            sbSql.Append(" ORDER BY");
//            sbSql.Append("    M.MAKER_ID");

//            return sbSql.ToString();
//        }

//        /// <summary>
//        /// M_USER_MAKER物理削除用SQLを取得します。
//        /// </summary>
//        /// <returns>作成SQL文</returns>
//        private string GetSql_Delete()
//        {
//            StringBuilder sbSql = new StringBuilder();

//            sbSql.Append("DELETE FROM");
//            sbSql.Append("    M_USER_MAKER");
//            sbSql.Append(" WHERE");
//            sbSql.Append("    USER_CD = :USER_CD");
//            sbSql.Append("    AND PATTERN_NAME = :PATTERN_NAME");

//            return sbSql.ToString();
//        }

//        /// <summary>
//        /// M_USER_MAKER登録用SQLを取得します。
//        /// </summary>
//        /// <returns>作成SQL文</returns>
//        private string GetSql_Insert()
//        {
//            StringBuilder sbSql = new StringBuilder();

//            sbSql.Append("INSERT INTO M_USER_MAKER");
//            sbSql.Append("    (PATTERN_NAME, USER_CD, MAKER_ID)");
//            sbSql.Append(" VALUES");
//            sbSql.Append("    (:PATTERN_NAME, :USER_CD, :MAKER_ID)");

//            return sbSql.ToString();
//        }

//        /// <summary>
//        /// グリッド表示
//        /// </summary>
//        private void SetGridFormat()
//        {
//            // グリッドのStyleを設定
//            //GridViewUtil.SetGridStyle(false, Unit.Percentage(98), true, false, grid);

//            //GridViewUtil.AddBoundField(colName.MAKER_ID, colID.MAKER_ID, false, grid);
//            //GridViewUtil.AddBoundField(colName.MAKER_NAME, colID.MAKER_NAME, true, grid);
//            //GridViewUtil.AddBoundField(colName.MAKER_ADDRESS, colID.MAKER_ADDRESS, true, grid);
//            //GridViewUtil.AddBoundField(colName.MAKER_OLD_NAME, colID.MAKER_OLD_NAME, true, grid);

//            //grid.DataSource = new DataTable();
//            //grid.DataBind();
//        }


//        /// <summary>
//        /// スクリプトを設定します。
//        /// </summary>
//        private void SetScript()
//        {
//            StringBuilder stbScript = new StringBuilder();

//            //変更ボタン
//            //stbScript.Append("showModalDialog('ZC01023.aspx?TYPE=" + Const.cUSER_MAKER_MOD + "'");
//            //stbScript.Append(",'','dialogHeight:780px;dialogWidth:1240px');");
//            //this.btnMakerMod.Attributes["onclick"] = stbScript.ToString();

//            ////新規追加ボタン
//            //stbScript.Clear();
//            //stbScript.Append("showModalDialog('ZC01023.aspx?TYPE=" + Const.cUSER_MAKER_ADD + "'");
//            //stbScript.Append(",'','dialogHeight:780px;dialogWidth:1240px');");
//            //this.btnMakerAdd.Attributes["onclick"] = stbScript.ToString();

//            ////削除ボタン
//            //this.btnMakerDel.Attributes["onclick"] = "return confirm('" + Common.GetMessage("Q006") + "');";
//        }

//        /// <summary>
//        /// IMEモードの制御
//        /// </summary>
//        private void SetImeModeControl()
//        {
//            // ime-mode = disabled（無効）
//            // Normalログインパスワード
//            //txtUserCd.Style.Add("ime-mode", "disabled");
//            //txtEmail.Style.Add("ime-mode", "disabled");
//            //txtTel.Style.Add("ime-mode", "disabled");
//            //txtPassword.Style.Add("ime-mode", "disabled");
//        }

//        /// <summary>
//        /// セッションをRemoveします。
//        /// </summary>
//        private void SessionRemove()
//        {
//            //Session.Remove(SessionConst.MaintenanceType);
//            //Session.Remove(SessionConst.MasterKeys);
//            //Session.Remove(SessionConst.IsSelfSettings);
//        }



//        /// <summary>
//        /// TextBoxにReadOnly属性設定
//        /// </summary>
//        /// <param name="objTextBox"></param>
//        private void SetTxtBoxReadOnly(TextBox objTextBox)
//        {
//            objTextBox.BackColor = ColorTranslator.FromHtml("#ffffe0");
//            objTextBox.ReadOnly = true;
//        }

//        /// <summary>
//        /// コンボボックスをReadOnly設定（登録済みの値から変更不可）
//        /// </summary>
//        /// <param name="objDropDownList"></param>
//        private void SetCmbBoxReadOnly(DropDownList objDropDownList)
//        {
//            objDropDownList.BackColor = ColorTranslator.FromHtml("#ffffe0");

//            // 選択中のレコード以外のデータを除去
//            for (int i = objDropDownList.Items.Count - 1; 0 <= i; i--)
//            {
//                if (objDropDownList.Items[i].Selected == false)
//                {
//                    objDropDownList.Items.RemoveAt(i);
//                }
//            }
//        }

//        /// <summary>
//        /// コンボボックスの設定
//        /// </summary>
//        private void SetComboBox()
//        {

//            MasterService ms = new MasterService();

//            //// 管理者フラグ
//            //var arrayList = new ArrayList();
//            //new CommonMasterInfo(SystemCommonMasterName.GRANT_NAME).GetSelectList().ToList().ForEach(x => arrayList.Add(new DictionaryEntry(x.Id, x.Name)));
//            //ddlAdminFlag.DataValueField = "Key";
//            //ddlAdminFlag.DataTextField = "Value";
//            //ddlAdminFlag.DataSource = arrayList;
//            //ddlAdminFlag.DataBind();

//            //// フロー名
//            //DataTable dtFlow = ms.selectWorkFlow();
//            //ddlFlowCd.DataValueField = "FLOW_CD";
//            //ddlFlowCd.DataTextField = "FLOW_NAME";
//            //dtFlow.Rows.InsertAt(dtFlow.NewRow(), 0);
//            //ddlFlowCd.DataSource = dtFlow;
//            //ddlFlowCd.DataBind();
//        }

//        /// <summary>
//        /// ユーザーメーカー登録系コントロールの入力可、不可制御
//        /// </summary>
//        /// <param name="boolEnable"></param>
//        private void SetUserMakerControlEnable(bool boolEnable)
//        {
//            //grid.Enabled = boolEnable;
//            //btnMakerAdd.Enabled = boolEnable;
//            //btnCsvInput.Enabled = boolEnable;
//            //pnlUserMaker.Visible = boolEnable;
//            //drpPattern.Enabled = boolEnable;
//            //btnMakerMod.Enabled = boolEnable;
//            //btnMakerDel.Enabled = boolEnable;
//            //btnCsvOutput.Enabled = boolEnable;
//        }

//        protected void txtApprover1_TextChanged(object sender, EventArgs e)
//        {
//            UserInfo.clsUser userInfo = null;
//            //userInfo = GetUserList.SingleOrDefault(x => x.USER_CD == txtApprover1.Text);
//            //if (userInfo != null)
//            //{
//            //    lblApprover1.Text = userInfo.USER_NAME;
//            //}
//            //else
//            //{
//            //    lblApprover1.Text = "";
//            //}

//            //txtApprover1.Focus();
//            //ClientScript.RegisterStartupScript(this.GetType(), "select", "$('#" + txtApprover1.ClientID + "').select()", true);
//        }

//        protected void txtApprover2_TextChanged(object sender, EventArgs e)
//        {
//            UserInfo.clsUser userInfo = null;
//            //userInfo = GetUserList.SingleOrDefault(x => x.USER_CD == txtApprover2.Text);
//            //if (userInfo != null)
//            //{
//            //    lblApprover2.Text = userInfo.USER_NAME;
//            //}
//            //else
//            //{
//            //    lblApprover2.Text = "";
//            //}

//            //txtApprover2.Focus();
//            //ClientScript.RegisterStartupScript(this.GetType(), "select", "$('#" + txtApprover2.ClientID + "').select()", true);
//        }

//        private void GetUserMasterList()
//        {
//            MasterService masterService = new MasterService();

//            List<M_USER> dt = masterService.selectUser();

//            //foreach (DataRow dr in dt.Rows)
//            //{
//                UserInfo.clsUser _user = new UserInfo.clsUser();
//                _user.USER_CD = dt[0].USER_CD;
//                _user.USER_NAME = dt[0].USER_NAME;
//                GetUserList.Add(_user);
//            //}

//        }
//    }
//}