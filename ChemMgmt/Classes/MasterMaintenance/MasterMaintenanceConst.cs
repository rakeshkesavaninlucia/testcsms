﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.MasterMaintenance
{
    /// <summary>
    /// マスターメンテナンスの定数を定義します。
    /// </summary>
    public static class MasterMaintenanceConst
    {
        /// <summary>
        /// 編集ボタンの名前を定義します。
        /// </summary>
        public const string EditButtonName = "編集";

        /// <summary>
        /// 編集ボタンのIDを定義します。
        /// </summary>
        public const string EditButtonID = "Edit_Row";

        /// <summary>
        /// 削除ボタンの名前を定義します。
        /// </summary>
        public const string DeleteButtonName = "削除";

        /// <summary>
        /// 削除ボタンのIDを定義します。
        /// </summary>
        public const string DeleteButtonID = "Delete_Row";
    }
}