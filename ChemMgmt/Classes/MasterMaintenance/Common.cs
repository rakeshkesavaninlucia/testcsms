﻿using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using ZyCoaG.Nikon;
using static ZyCoaG.BaseCommon.Control;
using System.IO;
using System.Configuration;//2019/02/18 TPE.Sugimoto Add
using Dapper;
using ChemMgmt.Models;
using ZyCoaG.Classes.util;
using ChemMgmt.Classes.util;
using ChemMgmt.Classes;
using ChemMgmt.Classes.Extensions;

namespace ZyCoaG.MasterMaintenance
{
    //2019/02/18 TPE.Sugimoto Add Sta
    class clsConfig
    {
        public static string MAKER_CHECK_TXT;
    }
    //2019/02/18 TPE.Sugimoto Add End

    /// <summary>
    /// マスターメンテナンス共通クラスです。
    /// </summary>
    public static class Common
    {

        public static List<M_LOCATION> SearchLocationMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, int classID, string locName, string plocName, string chemchief, string plocid, string selectability, string strStatus_2)
        {
            return SearchLocationMaster(TableInformation, TableColumnInformations, null, classID, locName, plocName, chemchief, plocid, selectability, strStatus_2);
        }

        public static List<M_GROUP> SearchGroupMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, string GroupCd, string GroupName, string strPGroupCd, string strStatus, string strBatchFlag)//2019/02/18 TPE.Sugimoto Upd
        {
            return SearchGroupMaster(TableInformation, TableColumnInformations, null, GroupCd, GroupName, strPGroupCd, strStatus, strBatchFlag);//2019/02/18 TPE.Sugimoto Upd
        }

        //public static DataTable SearchRegulationMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, string strStatus_2, string strcas, string selectcondition)
        //{
        //    return SearchRegulationMaster(TableInformation, TableColumnInformations, null, strStatus_2, strcas, selectcondition);
        //}

        /// <summary>
        /// テーブル管理情報とテーブル項目管理情報からマスターを指定された検索条件、またはキー項目で検索し、結果を返します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="KeyValues">キー項目を指定します。検索条件を使用した場合は<see cref="null"/>を指定します。</param>
        /// <returns>検索結果を<see cref="DataTable"/>で返します。</returns>
        //public static DataTable SearchCommonMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> KeyValues)
        //{
        //    return SearchCommonMaster(TableInformation, TableColumnInformations, KeyValues, null);
        //}

        //2018/08/08 TPE Add Start
        /// <summary>
        /// LOC_IDをキーにM_CHIEFを検索し、結果を返します。
        /// </summary>
        /// <returns>検索結果を<see cref="Dictonary"/>で返します。</returns>
        public static DataTable SearchMChief(Dictionary<string, string> locid)
        {
            var sql = new StringBuilder();
            var parameter = new Hashtable();

            foreach (KeyValuePair<string, string> loccode in locid)
            {
                sql.AppendLine("select ");
                sql.AppendLine("C.USER_CD ");
                sql.AppendLine(",U.USER_NAME ");
                sql.AppendLine("from ");
                sql.AppendLine("M_CHIEF as C");
                sql.AppendLine(",M_USER as U");
                sql.AppendLine("where ");
                sql.AppendLine("C.LOC_ID = ");
                sql.AppendLine(":" + loccode.Key);
                sql.AppendLine(" and ");
                sql.AppendLine("C.USER_CD = U.USER_CD ");

                parameter.Add(loccode.Key, loccode.Value);

            }

            var data = DbUtil.Select(sql.ToString(), parameter);

            return data;

        }
        //2018/08/08 TPE Add End

        //2018/08/15 TPE Add Start
        public static IQueryable<ChiefModel> GetMChief(Dictionary<string, string> locid)
        {
            var sql = new StringBuilder();
            var parameter = new Hashtable();

            foreach (KeyValuePair<string, string> loccode in locid)
            {
                sql.AppendLine("select ");
                sql.AppendLine("C.USER_CD ");
                sql.AppendLine(",U.USER_NAME ");
                sql.AppendLine("from ");
                sql.AppendLine("M_CHIEF as C");
                sql.AppendLine(",M_USER as U");
                sql.AppendLine("where 1=1");
                sql.AppendLine(" and ");
                sql.AppendLine("C.LOC_ID = ");
                sql.AppendLine(":" + loccode.Key);
                sql.AppendLine(" and ");
                sql.AppendLine("C.USER_CD = U.USER_CD ");

                parameter.Add(loccode.Key, loccode.Value);

            }

            DataTable records = DbUtil.Select(sql.ToString(), parameter);

            var dataArray = new List<ChiefModel>();

            foreach (DataRow record in records.Rows)
            {
                var model = new ChiefModel();

                model.USER_CD = record[nameof(model.USER_CD)].ToString();
                model.USER_NAME = record[nameof(model.USER_NAME)].ToString();

                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            return searchQuery;
        }

        private static List<M_LOCATION> SearchLocationMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> KeyValues, int classID, string locName, string plocName, string chemchief, string plocid, string selectability, string strStatus_2)//2019/02/18 TPE.Sugimoto Add
        {
            StringBuilder sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            List<S_TAB_COL_CONTROL> tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                if (tableColumnInformation.COLUMN_NAME == "USER_NAME") continue;
                //2018/10/01 Rin Add Start
                if (tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                {
                    sql.AppendLine(" " + TableInformation.TABLE_NAME + ".P_LOC_ID P_LOC_BASEID,");
                    continue;
                }
                if (tableColumnInformation.COLUMN_NAME == "SEL_FLAG")
                {
                    sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + " AS SELECTION_FLAG,");
                    continue;
                }
                //2018/10/01 Rin Add End
                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");

            }
            //2019/02/18 TPE.Sugimoto Add Sta
            sql.AppendLine(" STUFF((");
            sql.AppendLine(" SELECT ';' + M_REGULATION.REG_TEXT");
            sql.AppendLine(" from M_LOCATION");
            sql.AppendLine(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID");
            sql.AppendLine(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID");

            sql.AppendLine(" where U.LOC_ID = M_LOCATION.LOC_ID");
            sql.AppendLine(" and V.LOC_ID = M_LOCATION.LOC_ID");

            sql.AppendLine(" and M_LOCATION_HAZARD.HAZARD_CHECK = 1");
            sql.AppendLine(" FOR XML PATH('') ),1,1,''");
            sql.AppendLine(" ) AS REG_TEXT,");
            //2019/02/18 TPE.Sugimoto Add End

            sql.AppendLine(" case when len(U.USER_CD) > 0 then");
            sql.AppendLine(" left(U.USER_CD,charindex(';',U.USER_CD,len(U.USER_CD)) -1 )");
            sql.AppendLine(" else");
            sql.AppendLine(" null");
            sql.AppendLine(" end USER_CD,");

            sql.AppendLine(" case when len(V.USER_NAME) > 0 then");
            sql.AppendLine(" left(V.USER_NAME,charindex(';',V.USER_NAME,len(V.USER_NAME)) -1 )");
            sql.AppendLine(" else");
            sql.AppendLine(" null");
            sql.AppendLine(" end USER_NAME,");
            //2019/02/18 TPE.Sugimoto Add Sta
            sql.Append(" " + Environment.NewLine);
            sql.Append("IIF(M_LOCATION.DEL_FLAG = 0,'使用中','削除済') AS DEL_FLAGNAME,");
            sql.Append(" " + Environment.NewLine);

            sql.Append("(SELECT M_USER.USER_NAME FROM M_USER WHERE M_USER.USER_CD = M_LOCATION.DEL_USER_CD) AS DEL_USER_NM ");
            sql.Append(" " + Environment.NewLine);

            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);
            //2019/02/18 TPE.Sugimoto Add End
            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);

            sql.AppendLine(",(");
            sql.AppendLine(" select LOC_ID,");
            sql.AppendLine(" (select USER_CD + ';'");
            sql.AppendLine(" from M_CHIEF,M_LOCATION");
            sql.AppendLine(" where A.LOC_ID = M_CHIEF.LOC_ID");
            if (plocid != "")
            {
                sql.AppendLine(" and M_LOCATION.P_LOC_ID = @P_LOC_ID");
                parameter.Add("@P_LOC_ID", Convert.ToInt32(plocid));
            }
            sql.AppendLine(" group by USER_CD");
            sql.AppendLine(" for xml path('')) as USER_CD");
            sql.AppendLine(" from M_LOCATION A");
            sql.AppendLine("  ) U,");
            sql.AppendLine(" (select LI.LOC_ID,");
            sql.AppendLine(" (select U.USER_NAME + ';'");
            sql.AppendLine(" from M_LOCATION L,M_CHIEF C,M_USER U");
            sql.AppendLine(" where L.LOC_ID = C.LOC_ID");
            sql.AppendLine(" and C.USER_CD = U.USER_CD");
            sql.AppendLine(" and LI.LOC_ID = C.LOC_ID");
            sql.AppendLine(" order by L.LOC_ID,C.USER_CD");
            sql.AppendLine(" for xml path('')) as USER_NAME");
            sql.AppendLine(" from M_LOCATION LI");
            sql.AppendLine(" ) V");

            sql.AppendLine("where ");

            if (strStatus_2 == "0")
            {
                sql.AppendLine(" M_LOCATION.DEL_FLAG = " + strStatus_2);
                sql.AppendLine(" and  ");
            }
            else if (strStatus_2 == "1")
            {
                sql.AppendLine(" M_LOCATION.DEL_FLAG = " + strStatus_2);
                sql.AppendLine(" and  ");
            }
           
            if (locName != "")
            {
                sql.AppendLine(" M_LOCATION.LOC_NAME like CONCAT('%', @LOC_NAME, '%')");
                sql.AppendLine(" and  ");
                parameter.Add("@LOC_NAME", locName);
            }
            if (plocid != "")
            {
                sql.AppendLine(" M_LOCATION.P_LOC_ID " + " = @P_LOC_ID");
                sql.AppendLine(" and  ");
                parameter.Add("@P_LOC_ID", plocid);
            }
            if (classID != 0)
            {
                sql.AppendLine(" M_LOCATION.CLASS_ID " + " = @CLASS_ID");
                sql.AppendLine(" and  ");
                parameter.Add("@CLASS_ID", classID);
            }
            if (selectability != "")
            {
                sql.AppendLine(" M_LOCATION.SEL_FLAG " + " = @SEL_FLAG");
                sql.AppendLine(" and  ");
                parameter.Add("@SEL_FLAG", selectability);
            }

            sql.AppendLine(" U.LOC_ID = M_LOCATION.LOC_ID");
            sql.AppendLine(" and V.LOC_ID = M_LOCATION.LOC_ID");

            //2018/08/15 TPE Add Start
            if (chemchief != "")
            {
                sql.AppendLine(" and M_LOCATION.LOC_ID in (select M_CHIEF.LOC_ID from M_CHIEF where USER_CD = @USER_CD)");
            }
            //2018/08/15 TPE Add End

            parameter.Add("USER_CD", chemchief);

            //sql.Length -= 8;
            sql.Length -= 2;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;

            List<M_LOCATION> data = DbUtil.Select<M_LOCATION>(sql.ToString(), parameter);

            return data;
        }

   

        private static List<M_GROUP> SearchGroupMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> KeyValues, string GroupCd, string GroupName, string strPGroupCd, string strStatus, string strBatchFlag)//2019/02/18 TPE.Sugimoto Upd
        {
            StringBuilder sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            List<S_TAB_COL_CONTROL> tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }

            sql.Append("M_GROUP.P_GROUP_ID,");
            sql.Append(" " + Environment.NewLine);
            sql.Append("IIF(M_GROUP.DEL_FLAG = 0,'使用中','削除済') AS DEL_FLAGNAME,");
            sql.Append(" " + Environment.NewLine);
            sql.Append("IsNull(M_GROUP.BATCH_FLAG,0) AS BATCH_FLAG,");
            sql.Append(" " + Environment.NewLine);
            sql.Append("IIF(IsNull(M_GROUP.BATCH_FLAG,0) = 0,'対象','対象外') AS BATCH_FLAGNAME, ");
            sql.Append(" " + Environment.NewLine);
            sql.Append("(SELECT M_USER.USER_NAME FROM M_USER WHERE M_USER.USER_CD = M_GROUP.DEL_USER_CD) AS DEL_USER_NM, ");
            sql.Append(" " + Environment.NewLine);
            sql.Append("(SELECT M_USER.USER_NAME FROM M_USER WHERE M_USER.USER_CD = M_GROUP.REG_USER_CD) AS REG_USER_NM, ");
            sql.Append(" " + Environment.NewLine);
            sql.Append("(SELECT M_USER.USER_NAME FROM M_USER WHERE M_USER.USER_CD = M_GROUP.UPD_USER_CD) AS UPD_USER_NM ");
            sql.Append(" " + Environment.NewLine);
            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            if (strStatus != "2")
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + strStatus);
                sql.AppendLine(" and  ");
            }

            if (strBatchFlag == "2")//2019/02/18 TPE.Sugimoto Add
            {
                sql.AppendLine(" IsNull(M_GROUP.BATCH_FLAG,0) = 1");
                sql.AppendLine(" and  ");
            }
            if (strBatchFlag == "1")//2019/02/18 TPE.Sugimoto Add
            {
                sql.AppendLine(" IsNull(M_GROUP.BATCH_FLAG,0) = 0");
                sql.AppendLine(" and  ");
            }

            if (strPGroupCd != "" && strPGroupCd != null)
            {
                sql.AppendLine(" M_GROUP.P_GROUP_CD = @P_GROUP_CD");
                sql.AppendLine(" and  ");
                parameter.Add("P_GROUP_CD", strPGroupCd.Trim());
            }

            // キー項目の指定があればキー項目で検索します。
            // キー項目の指定がなければ検索条件で検索します。
            if (KeyValues != null)
            {
                foreach (KeyValuePair<string, string> keyValue in KeyValues)
                {
                    sql.AppendLine(" " + keyValue.Key + " = @" + keyValue.Key + " ");
                    sql.AppendLine(" and  ");
                    parameter.Add(keyValue.Key, keyValue.Value);
                }
            }
            if (GroupCd != "" && GroupCd != null)
            {
                sql.AppendLine(" M_GROUP.GROUP_CD  like CONCAT('%', @GROUP_CD, '%')");
                sql.AppendLine(" and  ");
                parameter.Add("GROUP_CD", GroupCd.Trim());
            }
            if (GroupName != "" && GroupName != null)
            {
                sql.AppendLine(" M_GROUP.GROUP_NAME like CONCAT('%', @GROUP_NAME, '%')");
                sql.AppendLine(" and  ");
                parameter.Add("GROUP_NAME", GroupName.Trim());
            }

            sql.Length -= 8;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;

            List<M_GROUP> data = DbUtil.Select<M_GROUP>(sql.ToString(), parameter);

            return data;
        }

        public static List<M_UNITSIZE> SearchUnitsizeMaster(M_MASTER m_master,S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations,  string strStatus_2)
        {
            var sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Append(" " + Environment.NewLine);
            sql.Append("IIF(M_UNITSIZE.DEL_FLAG = 0,'使用中','削除済') AS DEL_FLAG2,");
            sql.Append(" " + Environment.NewLine);
            sql.Append("(SELECT M_USER.USER_NAME FROM M_USER WHERE M_USER.USER_CD = M_UNITSIZE.DEL_USER_CD) AS DEL_USER_NM ");
            sql.Append(" " + Environment.NewLine);

            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            if (strStatus_2 != "2")
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + strStatus_2);
                sql.AppendLine(" and  ");
            }
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                // 検索条件を指定します。
                if (tableColumnInformation.IS_SEARCH && !string.IsNullOrEmpty(tableColumnInformation.InputValue?.Trim()))
                {
                    // 文字、数値の場合は部分一致、それ以外は完全一致で検索します。
                    if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character ||
                        tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
                    {
                        sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like :" + tableColumnInformation.COLUMN_NAME + DbUtil.LikeEscapeInfoAdd());
                        sql.AppendLine(" and  ");
                        parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(tableColumnInformation.InputValue) + "%");

                    }
                    else
                    {
                        sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = :" + tableColumnInformation.COLUMN_NAME);
                        sql.AppendLine(" and  ");
                        parameter.Add(tableColumnInformation.COLUMN_NAME, tableColumnInformation.InputValue);
                    }
                }
            }
            //}

            if (m_master.mUnitSize.UNIT_NAME != null)
            {
                sql.AppendLine(" " + "UNIT_NAME" + " like @" + "UNIT_NAME" + DbUtil.LikeEscapeInfoAdd());
                sql.AppendLine(" and  ");
                parameter.Add("@UNIT_NAME", "%" + DbUtil.ConvertIntoEscapeChar((m_master.mUnitSize.UNIT_NAME).Trim()) + "%");
            }

            sql.Length -= 8;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;

            List<M_UNITSIZE> udata = new List<M_UNITSIZE>();
            udata = DbUtil.Select<M_UNITSIZE>(sql.ToString(), parameter);

            return udata;
        }

        public static List<M_MAKER> SearchMakerMaster(M_MASTER m_master, S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, string strStatus_2)
        {
            var sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }

            sql.Append(" " + Environment.NewLine);
            sql.Append("IIF(M_MAKER.DEL_FLAG = 0,'使用中','削除済') AS DEL_FLAG2,");
            sql.Append(" " + Environment.NewLine);
            sql.Append("(SELECT M_USER.USER_NAME FROM M_USER WHERE M_USER.USER_CD = M_MAKER.DEL_USER_CD) AS DEL_USER_NM ");
            sql.Append(" " + Environment.NewLine);

            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            if (strStatus_2 != "2")
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + strStatus_2);
                sql.AppendLine(" and  ");
            }

            if (m_master.mMaker.MAKER_NAME != null)
            {
                sql.AppendLine(" " + "MAKER_NAME" + " like @" + "MAKER_NAME" + DbUtil.LikeEscapeInfoAdd());
                sql.AppendLine(" and  ");
                parameter.Add("@MAKER_NAME", "%" + DbUtil.ConvertIntoEscapeChar((m_master.mMaker.MAKER_NAME).Trim()) + "%");
            }

            sql.Length -= 8;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;

            List<M_MAKER> mdata = new List<M_MAKER>();
            mdata = DbUtil.Select<M_MAKER>(sql.ToString(), parameter);

            return mdata;
        }

       
        public static List<M_WORKFLOW> SearchWorkflowMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations,string flowCD, int flowID, string flowTypeID, string flowName, string strStatus_2)
        {
            StringBuilder sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            List<S_TAB_COL_CONTROL> tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }

            sql.Append(" " + Environment.NewLine);
            sql.Append("IIF(M_WORKFLOW.DEL_FLAG = 0,'使用中','削除済') AS DEL_FLAGNAME,");
            sql.Append(" " + Environment.NewLine);
            sql.Append("(SELECT M_USER.USER_NAME FROM M_USER WHERE M_USER.USER_CD = M_WORKFLOW.DEL_USER_CD) AS DEL_USER_NM ");
            sql.Append(" " + Environment.NewLine);

            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            if (strStatus_2 != "2")
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + strStatus_2);
                sql.AppendLine(" and  ");
            }
            if (flowTypeID != "0" && flowTypeID != "")
            {
                sql.AppendLine(" FLOW_TYPE_ID " + " = @FLOW_TYPE_ID");
                sql.AppendLine(" and  ");
                parameter.Add("@FLOW_TYPE_ID", flowTypeID);
            }
            if (flowName != "" && flowName != null)
            {
                sql.AppendLine(" USAGE like CONCAT('%', @USAGE, '%')");
                sql.AppendLine(" and  ");
                parameter.Add("USAGE", flowName.Trim());
            }
            if (flowCD != "" && flowCD != null)
            {
                sql.AppendLine(" FLOW_CD like CONCAT('%', @FLOW_CD, '%')");
                sql.AppendLine(" and  ");
                parameter.Add("FLOW_CD", flowCD.Trim());
            }

            sql.Length -= 8;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;

            var data = DbUtil.Select<M_WORKFLOW>(sql.ToString(), parameter);

            return data;
        }
        //2019/02/18 TPE.Sugimoto Add End

        //private static DataTable SearchRegulationMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> KeyValues, string strStatus_2, string strcas, string selectcondition)
        //{
        //    var sql = new StringBuilder();
        //    var parameter = new Hashtable();
        //    var tableColumnControls = new List<S_TAB_COL_CONTROL>();

        //    sql.AppendLine("select ");
        //    foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
        //    {
        //        sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
        //    }

        //    //2019/05/09 TPE.Rin Add Start
        //    if (TableInformation.TABLE_NAME == "M_REGULATION")
        //    {
        //        sql.AppendLine(" M_REGULATION.MARKS,");
        //    }
        //    //2019/05/09 TPE.Rin Add End

        //    sql.Append(" " + Environment.NewLine);
        //    sql.Append("IIF(M_REGULATION.DEL_FLAG = 0,'使用中','削除済') AS DEL_FLAG2,");
        //    sql.Append(" " + Environment.NewLine);
        //    sql.Append("IIF(M_REGULATION.RA_FLAG = 0,'非該当','該当') AS RA_FLAG,");
        //    sql.Append(" " + Environment.NewLine);
        //    sql.Append("MARKS,");
        //    sql.Append(" " + Environment.NewLine);
        //    sql.Append("(SELECT M_USER.USER_NAME FROM M_USER WHERE M_USER.USER_CD = M_REGULATION.DEL_USER_CD) AS DEL_USER_NM ");
        //    sql.Append(" " + Environment.NewLine);

        //    sql.Length -= 3;
        //    sql.Append(" " + Environment.NewLine);

        //    sql.AppendLine("from ");
        //    sql.AppendLine(" " + TableInformation.TABLE_NAME);
        //    if (!string.IsNullOrEmpty(strcas))
        //    {
        //        sql.AppendLine(" ,M_CAS_REGULATION");
        //    }

        //    sql.AppendLine("where ");
        //    // 削除フラグの指定があれば削除フラグを条件にします。
        //    if (strStatus_2 != "2")
        //    {
        //        sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + strStatus_2);
        //        sql.AppendLine(" and  ");
        //    }

        //    // キー項目の指定があればキー項目で検索します。
        //    // キー項目の指定がなければ検索条件で検索します。
        //    if (KeyValues != null)
        //    {
        //        foreach (KeyValuePair<string, string> keyValue in KeyValues)
        //        {
        //            sql.AppendLine(" " + keyValue.Key + " = :" + keyValue.Key + " ");
        //            sql.AppendLine(" and  ");
        //            parameter.Add(keyValue.Key, keyValue.Value);
        //        }
        //    }
        //    else
        //    {
        //        foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
        //        {

        //            // 検索条件を指定します。
        //            if (tableColumnInformation.IS_SEARCH && !string.IsNullOrEmpty(tableColumnInformation.InputValue?.Trim()))
        //            {
        //                // 文字、数値の場合は部分一致、それ以外は完全一致で検索します。
        //                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character ||
        //                    tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
        //                {

        //                    if (tableColumnInformation.COLUMN_NAME == "REG_TEXT")
        //                    {
        //                        switch (selectcondition)
        //                        {
        //                            case "0":
        //                                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like :" + tableColumnInformation.COLUMN_NAME + DbUtil.LikeEscapeInfoAdd());
        //                                parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(tableColumnInformation.InputValue) + "%");
        //                                break;

        //                            case "1":
        //                                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like :" + tableColumnInformation.COLUMN_NAME + DbUtil.LikeEscapeInfoAdd());
        //                                parameter.Add(tableColumnInformation.COLUMN_NAME, DbUtil.ConvertIntoEscapeChar(tableColumnInformation.InputValue) + "%");
        //                                break;

        //                            case "2":
        //                                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like :" + tableColumnInformation.COLUMN_NAME + DbUtil.LikeEscapeInfoAdd());
        //                                parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(tableColumnInformation.InputValue));
        //                                break;

        //                            case "3":
        //                                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = :" + tableColumnInformation.COLUMN_NAME);
        //                                parameter.Add(tableColumnInformation.COLUMN_NAME, DbUtil.ConvertIntoEscapeChar(tableColumnInformation.InputValue));
        //                                break;

        //                        }
        //                        sql.AppendLine(" and  ");
        //                    }
        //                    else
        //                    {
        //                        sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like :" + tableColumnInformation.COLUMN_NAME + DbUtil.LikeEscapeInfoAdd());
        //                        sql.AppendLine(" and  ");
        //                        parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(tableColumnInformation.InputValue) + "%");

        //                    }

        //                }
        //                else
        //                {
        //                    sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = :" + tableColumnInformation.COLUMN_NAME);
        //                    sql.AppendLine(" and  ");
        //                    parameter.Add(tableColumnInformation.COLUMN_NAME, tableColumnInformation.InputValue);
        //                }
        //            }
        //        }
        //    }

        //    //M_CAS_REGULATIONとの条件
        //    if (!string.IsNullOrEmpty(strcas))
        //    {
        //        sql.AppendLine("M_REGULATION.REG_TYPE_ID = M_CAS_REGULATION.REG_TYPE_ID");
        //        sql.AppendLine(" and  ");
        //        sql.AppendLine("COLMUN_ID = '1'");
        //        sql.AppendLine(" and  ");
        //        sql.AppendLine("VAL_STRING=:CAS");
        //        sql.AppendLine(" and  ");

        //        parameter.Add("CAS", strcas);

        //    }

        //    sql.Length -= 8;

        //    sql.Append(" " + Environment.NewLine);

        //    sql.AppendLine("order by ");
        //    foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
        //    {
        //        if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
        //    }
        //    sql.Length -= 3;

        //    var data = DbUtil.Select(sql.ToString(), parameter);

        //    return data;
        //}

        public static List<M_STATUS> SearchStatusMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, M_MASTER m_master, int? DeleteFlag)
        {
            var sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");

            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }

            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);

            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            if (DeleteFlag != null)
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + DeleteFlag.ToString());
                sql.AppendLine(" and  ");
            }
            if (m_master.masterMaintenanceScreen == "M_STATUS")
            {
                if (m_master.mStatus.CLASS_ID != 0)
                {
                    sql.AppendLine(" " + "CLASS_ID" + " like @" + "CLASS_ID" + DbUtil.LikeEscapeInfoAdd());
                    sql.AppendLine(" and  ");
                    parameter.Add("@CLASS_ID", "%" + DbUtil.ConvertIntoEscapeChar((m_master.mStatus.CLASS_ID.ToString()).Trim()) + "%");
                }
                if (m_master.mStatus.STATUS_TEXT != null)
                {
                    sql.AppendLine(" " + "STATUS_TEXT" + " like @" + "STATUS_TEXT" + DbUtil.LikeEscapeInfoAdd());
                    sql.AppendLine(" and  ");
                    parameter.Add("@STATUS_TEXT", "%" + DbUtil.ConvertIntoEscapeChar((m_master.mStatus.STATUS_TEXT).Trim()) + "%");
                }
            }
            else if (m_master.masterMaintenanceScreen == "M_ACTION_TYPE")
            {
                if (m_master.mActionType.ACTION_TEXT != null)
                {
                    sql.AppendLine(" " + "ACTION_TEXT" + " like @" + "ACTION_TEXT" + DbUtil.LikeEscapeInfoAdd());
                    sql.AppendLine(" and  ");
                    parameter.Add("@ACTION_TEXT", "%" + DbUtil.ConvertIntoEscapeChar((m_master.mActionType.ACTION_TEXT.ToString()).Trim()) + "%");
                }
            }

            sql.Length -= 8;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;


            List<M_STATUS> data = new List<M_STATUS>();
                data = DbUtil.Select<M_STATUS>(sql.ToString(), parameter);
            return data;
        }

        public static List<M_ACTION> SearchActionMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, M_MASTER m_master, int? DeleteFlag)
        {
            var sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");

            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }

            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);

            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            if (DeleteFlag != null)
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + DeleteFlag.ToString());
                sql.AppendLine(" and  ");
            }

            if (m_master.masterMaintenanceScreen == "M_ACTION_TYPE")
            {
                if (m_master.mActionType.ACTION_TEXT != null)
                {
                    sql.AppendLine(" " + "ACTION_TEXT" + " like @" + "ACTION_TEXT" + DbUtil.LikeEscapeInfoAdd());
                    sql.AppendLine(" and  ");
                    parameter.Add("@ACTION_TEXT", "%" + DbUtil.ConvertIntoEscapeChar((m_master.mActionType.ACTION_TEXT.ToString()).Trim()) + "%");
                }
            }

            sql.Length -= 8;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;

            List<M_ACTION> data = new List<M_ACTION>();
            data = DbUtil.Select<M_ACTION>(sql.ToString(), parameter);

            return data;
        }
       

        /// <summary>
        /// テーブル項目管理情報から<see cref="DataTable"/>のコード情報などを名称に変換した列を追加します。
        /// </summary>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="ConvertTable">変換列を追加する対象のテーブルを指定します。</param>
        public static List<M_LOCATION> ConvertLocationMaster(List<S_TAB_COL_CONTROL> TableColumnInformations, List<M_LOCATION> ConvertTable)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {

                switch (tableColumnInformation.DATA_TYPE)
                {
                   
                    case S_TAB_COL_CONTROL.DataType.Select:
                        for (int i = 0; i < ConvertTable.Count; i++)
                        {
                            string value;
                            if (tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                            {
                                foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                                {
                                    if (convertData.Key == ConvertTable[i].P_LOC_BASEID)
                                    {
                                        ConvertTable[i].P_LOC_NAME = convertData.Value;
                                    }
                                }
                                continue;
                            }

                            // 選択の場合はコードを名称に変換します。
                            if (tableColumnInformation.COLUMN_NAME == "CLASS_ID")
                            {
                                tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].CLASS_ID),
                                                                                  out value);
                                ConvertTable[i].CLASS_NAME = value;
                            }
                            else if (tableColumnInformation.COLUMN_NAME == "LOC_ID")
                            {
                                tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].LOC_ID),
                                                                                  out value);
                                ConvertTable[i].LOC_NAME = value;
                            }
                            else if (tableColumnInformation.COLUMN_NAME == "SEL_FLAG")
                            {
                                if (ConvertTable[i].SELECTION_FLAG != null)
                                {
                                    ConvertTable[i].SEL_FLAGNAME = ConvertTable[i].SELECTION_FLAG.Equals("1") ? "選択可" : "選択不可";
                                }
                                else
                                {
                                    ConvertTable[i].SEL_FLAGNAME = "選択不可";
                                }
                            }
                            else if (tableColumnInformation.COLUMN_NAME == "OVER_CHECK_FLAG")
                            {
                                tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].OVER_CHECK_FLAG),
                                                                                  out value);
                                ConvertTable[i].OVER_CHECK_FLAGNAME = value;
                            }
                            else if (tableColumnInformation.COLUMN_NAME == "REG_USER_CD")
                            {
                                tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].REG_USER_CD),
                                                                                  out value);
                                ConvertTable[i].REG_USER_NM = value;
                            }
                            else if (tableColumnInformation.COLUMN_NAME == "UPD_USER_CD")
                            {
                                tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].UPD_USER_CD),
                                                                                  out value);
                                ConvertTable[i].UPD_USER_NM = value;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
            return ConvertTable;
        }

        public static List<M_MAKER> ConvertMakerMaster(List<S_TAB_COL_CONTROL> TableColumnInformations, List<M_MAKER> maker)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_USER_CD")
                {
                    for (int i = 0; i < maker.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(maker[i].REG_USER_CD),
                                                                              out value);
                        maker[i].REG_USER_CD = value;
                    }
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "UPD_USER_CD")
                {
                    for (int i = 0; i < maker.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(maker[i].UPD_USER_CD),
                                                                              out value);
                        maker[i].UPD_USER_CD = value;
                    }
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "DEL_USER_CD")
                {
                    for (int i = 0; i < maker.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(maker[i].DEL_USER_CD),
                                                                              out value);
                        maker[i].DEL_USER_CD = value;
                    }
                }
            }
            return maker;
        }
        public static List<M_UNITSIZE> ConvertUnitMaster(List<S_TAB_COL_CONTROL> TableColumnInformations, List<M_UNITSIZE> unit)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_USER_CD")
                {
                    for (int i = 0; i < unit.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(unit[i].REG_USER_CD),
                                                                              out value);
                        unit[i].REG_USER_CD = value;
                    }
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "UPD_USER_CD")
                {
                    for (int i = 0; i < unit.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(unit[i].UPD_USER_CD),
                                                                              out value);
                        unit[i].UPD_USER_CD = value;
                    }
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "DEL_USER_CD")
                {
                    for (int i = 0; i < unit.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(unit[i].DEL_USER_CD),
                                                                              out value);
                        unit[i].DEL_USER_CD = value;
                    }
                }
            }
            return unit;
        }

        public static List<M_STATUS> ConvertStatusMaster(List<S_TAB_COL_CONTROL> TableColumnInformations, List<M_STATUS> status)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_USER_CD")
                {
                    for (int i = 0; i < status.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(status[i].REG_USER_CD),
                                                                              out value);
                        status[i].REG_USER_CD = value;
                    }
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "UPD_USER_CD")
                {
                    for (int i = 0; i < status.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(status[i].UPD_USER_CD),
                                                                              out value);
                        status[i].UPD_USER_CD = value;
                    }
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "DEL_USER_CD")
                {
                    for (int i = 0; i < status.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(status[i].DEL_USER_CD),
                                                                              out value);
                        status[i].DEL_USER_CD = value;
                    }
                }
            }
            return status;
        }
        public static List<M_ACTION> ConvertActionMaster(List<S_TAB_COL_CONTROL> TableColumnInformations, List<M_ACTION> action)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_USER_CD")
                {
                    for (int i = 0; i < action.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(action[i].REG_USER_CD),
                                                                              out value);
                        action[i].REG_USER_CD = value;
                    }
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "UPD_USER_CD")
                {
                    for (int i = 0; i < action.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(action[i].UPD_USER_CD),
                                                                              out value);
                        action[i].UPD_USER_CD = value;
                    }
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "DEL_USER_CD")
                {
                    for (int i = 0; i < action.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(action[i].DEL_USER_CD),
                                                                              out value);
                        action[i].DEL_USER_CD = value;
                    }
                }
            }
            return action;
        }
        /// <summary>
        /// テーブル項目管理情報から<see cref="DataTable"/>のコード情報などを名称に変換した列を追加します。
        /// </summary>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="ConvertTable">変換列を追加する対象のテーブルを指定します。</param>
        public static List<M_WORKFLOW> ConvertWorkflowMaster(List<S_TAB_COL_CONTROL> TableColumnInformations, List<M_WORKFLOW> ConvertTable)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "FLOW_TYPE_ID")
                {
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].FLOW_TYPE_ID),
                                                                              out value);
                        ConvertTable[i].FLOW_NAME = value;
                    }
                }
                else if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_USER_CD")
                {
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].REG_USER_CD),
                                                                              out value);
                        ConvertTable[i].REG_USER_NM = value;
                    }
                }
                else if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "UPD_USER_CD")
                {
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].UPD_USER_CD),
                                                                              out value);
                        ConvertTable[i].UPD_USER_NM = value;
                    }
                }
                else if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "DEL_USER_CD")
                {
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;
                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].DEL_USER_CD),
                                                                              out value);
                        ConvertTable[i].DEL_USER_NM = value;
                    }
                }
            }
            return ConvertTable;
        }
            /// <summary>
            /// テーブル項目管理情報から<see cref="DataTable"/>のコード情報などを名称に変換した列を追加します。
            /// </summary>
            /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
            /// <param name="ConvertTable">変換列を追加する対象のテーブルを指定します。</param>
            public static List<M_GROUP> ConvertGroupMaster(List<S_TAB_COL_CONTROL> TableColumnInformations, List<M_GROUP> ConvertTable)
        {
            // newConvertTable = ConvertTable;
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "P_GROUP_CD")
                {
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].P_GROUP_CD),
                                                                              out value);
                        ConvertTable[i].P_GROUP_NAME = value;

                    }
                }
            }
            return ConvertTable;
        }

        /// <summary>
        /// テーブル管理情報とテーブル項目管理情報とマスターデータからマスターにデータを登録します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="InsertDatas">登録するマスターのデータを指定します。</param>
        /// <param name="userCode">登録者のユーザーコード。</param>
            public static void InsertCommonMaster(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> InsertDatas, string userCode)
             {
                 var databaseContext = DbUtil.Open(true);
                 var sql = new StringBuilder();
                 var parameter = InsertDatas.ToHashtable();
                 parameter.Add(nameof(SystemColumn.REG_USER_CD), userCode);
                 var tableColumnControls = new List<S_TAB_COL_CONTROL>();

                 if (TableInformation.TABLE_NAME == "M_WORKFLOW")
                 {
                     parameter["FLOW_CD"] = getNewSequence();
                 }

                 sql.AppendLine("insert into ");
                 sql.AppendLine(" " + TableInformation.TABLE_NAME);
                 sql.AppendLine(" (");

                 foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations.Where(x => !x.IS_IDENTITY).ToList())
                 {
                     if (IsSystemColumn(tableColumnInformation)) continue;
                     sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
                 }
                 if (TableInformation.TABLE_NAME == "M_GROUP")
                 {
                     sql.AppendLine("DEL_USER_CD,");
                     sql.AppendLine("DEL_DATE,");
                     sql.AppendLine("DEL_FLAG,");
                     sql.AppendLine("BATCH_FLAG,");
                 }
                 //2019/02/18 TPE.Sugimoto Add Sta
                 if (TableInformation.TABLE_NAME == "M_MAKER" ||
                     TableInformation.TABLE_NAME == "M_UNITSIZE" ||
                     TableInformation.TABLE_NAME == "M_WORKFLOW")
                 {
                     sql.AppendLine("DEL_USER_CD,");
                     sql.AppendLine("DEL_DATE,");
                     sql.AppendLine("DEL_FLAG,");
                 }
                 //2019/02/18 TPE.Sugimoto Add End
                 sql.AppendLine(" " + nameof(SystemColumn.REG_USER_CD));
                 sql.AppendLine(" )");

                 sql.AppendLine("values ");
                 sql.AppendLine(" (");
                 foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations.Where(x => !x.IS_IDENTITY).ToList())
                 {
                     if (IsSystemColumn(tableColumnInformation)) continue;
                     sql.AppendLine(" :" + tableColumnInformation.COLUMN_NAME + ",");
                 }
                 if (TableInformation.TABLE_NAME == "M_GROUP")
                 {
                     sql.AppendLine(" :DEL_USER_CD,");
                     sql.AppendLine(" :DEL_DATE,");
                     sql.AppendLine(" :DEL_FLAG,");
                     sql.AppendLine(" :BATCH_FLAG,");
                 }
                 //2019/02/18 TPE.Sugimoto Add Sta
                 if (TableInformation.TABLE_NAME == "M_MAKER" ||
                     TableInformation.TABLE_NAME == "M_UNITSIZE" ||
                     TableInformation.TABLE_NAME == "M_WORKFLOW")
                 {
                     sql.AppendLine(" :DEL_USER_CD,");
                     sql.AppendLine(" :DEL_DATE,");
                     sql.AppendLine(" :DEL_FLAG,");
                 }
                 //2019/02/18 TPE.Sugimoto Add End
                 sql.AppendLine(" :" + nameof(SystemColumn.REG_USER_CD));
                 sql.AppendLine(" )");

                 var data = DbUtil.ExecuteUpdate(databaseContext, sql.ToString(), parameter);
                 databaseContext.Commit();
                 databaseContext.Close();
             } 


        //2018/08/04 TPE Add
        /// <summary>
        /// テーブル管理情報とテーブル項目管理情報とマスターデータからマスターにデータを登録します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="InsertDatas">登録するマスターのデータを指定します。</param>
        /// <param name="userCode">登録者のユーザーコード。</param>
        public static void InsertLocation(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> InsertDatas, string userCode, Dictionary<string, string> chemChief)
        {
            var databaseContext = DbUtil.Open(true);
            var sql = new StringBuilder();
            var parameter = InsertDatas.ToHashtable();
            parameter.Add(nameof(SystemColumn.REG_USER_CD), userCode);
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("insert into ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            sql.AppendLine(" (");

            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations.Where(x => !x.IS_IDENTITY).ToList())
            {
                if (tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                if (tableColumnInformation.COLUMN_NAME == "USER_NAME") continue;
                if (tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID") continue; //2018/10/03 Rin Add
                if (IsSystemColumn(tableColumnInformation)) continue;
                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.AppendLine(" " + nameof(SystemColumn.REG_USER_CD));
            sql.AppendLine(", " + nameof(SystemColumn.DEL_FLAG));//2019/02/18 TPE.Sugimoto Add
            sql.AppendLine(" )");

            sql.AppendLine("values ");
            sql.AppendLine(" (");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations.Where(x => !x.IS_IDENTITY).ToList())
            {
                if (tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                if (tableColumnInformation.COLUMN_NAME == "USER_NAME") continue;
                if (tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID") continue; //2018/10/03 Rin Add
                if (IsSystemColumn(tableColumnInformation)) continue;
                sql.AppendLine(" :" + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.AppendLine(" :" + nameof(SystemColumn.REG_USER_CD));
            sql.AppendLine(", :" + nameof(SystemColumn.DEL_FLAG));//2019/02/18 TPE.Sugimoto Add
            sql.AppendLine(" )");
            sql.AppendLine(";SELECT CAST(SCOPE_IDENTITY() AS INT)");

            //自動採番された場所CDを取得
            var locid = DbUtil.ExecuteUpdateReturn(databaseContext, sql.ToString(), parameter);

            parameter.Add("LOC_ID", locid);

            int i = 0;

            if (chemChief != null)
            {
                foreach (KeyValuePair<string, string> chemchief in chemChief)
                {
                    i++;

                    parameter.Add("USER_CD_" + i, chemchief.Key);
                    var sql2 = new StringBuilder();
                    sql2.AppendLine("insert into M_CHIEF(LOC_ID,USER_CD,REG_USER_CD) ");
                    sql2.AppendLine("values ");
                    sql2.AppendLine("(");
                    sql2.AppendLine(":LOC_ID");
                    sql2.AppendLine(",:USER_CD_" + i);
                    sql2.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));
                    sql2.AppendLine(" )");

                    var data = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameter);
                }
            }

            //使用可能危険物の新規登録処理
            //2019/02/18 TPE.Sugimoto Add Sta
            var sql_1 = new StringBuilder();
            sql_1.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_1.AppendLine("values ");
            sql_1.AppendLine("(");
            sql_1.AppendLine(":LOC_ID");
            sql_1.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第一類' )");
            sql_1.AppendLine("," + parameter["HAZARD_CHECK1"]);
            sql_1.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));
            sql_1.AppendLine(" )");
            var data_1 = DbUtil.ExecuteUpdate(databaseContext, sql_1.ToString(), parameter);

            var sql_2 = new StringBuilder();
            sql_2.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_2.AppendLine("values ");
            sql_2.AppendLine("(");
            sql_2.AppendLine(":LOC_ID");
            sql_2.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第二類' )");
            sql_2.AppendLine("," + parameter["HAZARD_CHECK2"]);
            sql_2.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));
            sql_2.AppendLine(" )");
            var data_2 = DbUtil.ExecuteUpdate(databaseContext, sql_2.ToString(), parameter);

            var sql_3 = new StringBuilder();
            sql_3.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_3.AppendLine("values ");
            sql_3.AppendLine("(");
            sql_3.AppendLine(":LOC_ID");
            sql_3.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第三類' )");
            sql_3.AppendLine("," + parameter["HAZARD_CHECK3"]);
            sql_3.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));
            sql_3.AppendLine(" )");
            var data_3 = DbUtil.ExecuteUpdate(databaseContext, sql_3.ToString(), parameter);

            var sql_4 = new StringBuilder();
            sql_4.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_4.AppendLine("values ");
            sql_4.AppendLine("(");
            sql_4.AppendLine(":LOC_ID");
            sql_4.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第四類' )");
            sql_4.AppendLine("," + parameter["HAZARD_CHECK4"]);
            sql_4.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));
            sql_4.AppendLine(" )");
            var data_4 = DbUtil.ExecuteUpdate(databaseContext, sql_4.ToString(), parameter);

            var sql_5 = new StringBuilder();
            sql_5.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_5.AppendLine("values ");
            sql_5.AppendLine("(");
            sql_5.AppendLine(":LOC_ID");
            sql_5.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第五類' )");
            sql_5.AppendLine("," + parameter["HAZARD_CHECK5"]);
            sql_5.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));
            sql_5.AppendLine(" )");
            var data_5 = DbUtil.ExecuteUpdate(databaseContext, sql_5.ToString(), parameter);

            var sql_6 = new StringBuilder();
            sql_6.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_6.AppendLine("values ");
            sql_6.AppendLine("(");
            sql_6.AppendLine(":LOC_ID");
            sql_6.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第六類' )");
            sql_6.AppendLine("," + parameter["HAZARD_CHECK6"]);
            sql_6.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));
            sql_6.AppendLine(" )");
            var data_6 = DbUtil.ExecuteUpdate(databaseContext, sql_6.ToString(), parameter);

            var sql_7 = new StringBuilder();
            sql_7.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_7.AppendLine("values ");
            sql_7.AppendLine("(");
            sql_7.AppendLine(":LOC_ID");
            sql_7.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '指定可燃物' )");
            sql_7.AppendLine("," + parameter["HAZARD_CHECK7"]);
            sql_7.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));
            sql_7.AppendLine(" )");
            var data_7 = DbUtil.ExecuteUpdate(databaseContext, sql_7.ToString(), parameter);
            //2019/02/18 TPE.Sugimoto Add End

            databaseContext.Commit();
            databaseContext.Close();

        }

        //2019/02/24 TPE Add
        /// <summary>
        /// テーブル管理情報とテーブル項目管理情報とマスターデータからマスターにデータを登録します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="InsertDatas">登録するマスターのデータを指定します。</param>
        /// <param name="userCode">登録者のユーザーコード。</param>
        public static void InsertCAS(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, M_MASTER InsertDatas, string userCode, List<CasRegModel> cas)
        {
            var databaseContext = DbUtil.Open(true);
            var sql = new StringBuilder();
            var parameter = new DynamicParameters();
            //parameter.Add(nameof(SystemColumn.REG_USER_CD), userCode);
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();
            var parameters = new Hashtable();

            sql.AppendLine("insert into ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            sql.AppendLine(" (");

            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations.Where(x => !x.IS_IDENTITY).ToList())
            {
                if (IsSystemColumn(tableColumnInformation)) continue;
                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.AppendLine(" RA_FLAG,");
            sql.AppendLine(" MARKS,");
            sql.AppendLine(" DEL_FLAG,");

            sql.AppendLine(" " + nameof(SystemColumn.REG_USER_CD));
            sql.AppendLine(", REG_DATE,");
            sql.AppendLine(" UPD_DATE,");
            sql.AppendLine(" DEL_DATE");
            sql.AppendLine(" )");

            sql.AppendLine("values ");
            sql.AppendLine(" (");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations.Where(x => !x.IS_IDENTITY).ToList())
            {
                if (IsSystemColumn(tableColumnInformation)) continue;

                sql.AppendLine(" @" + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.AppendLine(" @RA_FLAG,");
            sql.AppendLine(" @MARKS,");
            sql.AppendLine(" @DEL_FLAG,");
            sql.AppendLine(" @" + nameof(SystemColumn.REG_USER_CD));
            sql.AppendLine(", @REG_DATE,");
            sql.AppendLine(" @UPD_DATE,");
            sql.AppendLine(" @DEL_DATE");
            sql.AppendLine(" )");
            sql.AppendLine(";SELECT CAST(SCOPE_IDENTITY() AS INT)");
            parameter.Add("@CLASS_ID", InsertDatas.mRegulation.CLASS_ID);
            parameter.Add("@REG_TEXT", InsertDatas.mRegulation.REG_TEXT);
            parameter.Add("@REG_ACRONYM_TEXT", InsertDatas.mRegulation.REG_ACRONYM_TEXT);
            parameter.Add("@REG_ICON_PATH", InsertDatas.mRegulation.REG_ICON_PATH);
            parameter.Add("@FIRE_SIZE", InsertDatas.mRegulation.FIRE_SIZE);
            parameter.Add("@UNITSIZE_ID", InsertDatas.mRegulation.UNITSIZE_ID);
            parameter.Add("@SORT_ORDER", InsertDatas.mRegulation.SORT_ORDER);
            parameter.Add("@KEY_MGMT", InsertDatas.mRegulation.KEY_MGMT);
            parameter.Add("@WEIGHT_MGMT", InsertDatas.mRegulation.WEIGHT_MGMT);
            parameter.Add("@WORKTIME_MGMT", InsertDatas.mRegulation.WORKTIME_MGMT);
            parameter.Add("@P_REG_TYPE_ID", InsertDatas.mRegulation.P_REG_TYPE_ID);
            parameter.Add("@EXPOSURE_REPORT", InsertDatas.mRegulation.EXPOSURE_REPORT);
            parameter.Add("@EXAMINATION_FLAG", InsertDatas.mRegulation.EXAMINATION_FLAG_VALUE);
            parameter.Add("@MEASUREMENT_FLAG", InsertDatas.mRegulation.MEASUREMENT_FLAG_VALUE);
            parameter.Add("@RA_FLAG", InsertDatas.mRegulation.RA_FLAG2);
            parameter.Add("@MARKS", InsertDatas.mRegulation.MARKS);
            parameter.Add("@DEL_FLAG", InsertDatas.mRegulation.DEL_FLG);
            parameter.Add("@REG_USER_CD", userCode);
            parameter.Add("@REG_DATE", DateTime.Now);
            parameter.Add("@UPD_DATE", DateTime.Now);
            parameter.Add("@DEL_DATE", DateTime.Now);



            //自動採番された場所CDを取得
            var regtypeid = DbUtil.ExecuteUpdateReturn(sql.ToString(), parameter);


            parameters.Add("REG_TYPE_ID", regtypeid);
            parameters.Add("REG_USER_CD", userCode);

            //M_COMMONからデータ取得
            DbContext context = null;
            context = DbUtil.Open(false);

            var sqltemp = new System.Text.StringBuilder();
            sqltemp.AppendLine("select");
            sqltemp.AppendLine(" KEY_VALUE ");
            sqltemp.AppendLine(", DISPLAY_VALUE ");
            sqltemp.AppendLine("from");
            sqltemp.AppendLine(" M_COMMON");
            sqltemp.AppendLine("where");
            sqltemp.AppendLine(" TABLE_NAME = 'M_CAS_REGULATION'");
            sqltemp.AppendLine(" and");
            sqltemp.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

            var records = DbUtil.Select<M_COMMON>(sqltemp.ToString(), null);

            context.Close();

            var dataArray = new List<M_COMMON>();

            foreach (var record in records)
            {
                var model = new M_COMMON();

                model.KEY_VALUE = int.Parse(record.KEY_VALUE.ToString());
                model.DISPLAY_VALUE = record.DISPLAY_VALUE.ToString();

                dataArray.Add(model);
            }

            for (int x = 0; x < dataArray.Count; x++)
            {
                parameters.Add(dataArray[x].DISPLAY_VALUE + "COLUMN", dataArray[x].KEY_VALUE);
            }

            for (int i = 0; i < cas.Count; i++)
            {
                //casno
                parameters.Add("CAS_NO" + i, cas[i].casno);
                parameters.Add("ROW_ID" + i, i);
                var sql2 = new StringBuilder();

                sql2.AppendLine("insert into M_CAS_REGULATION");
                sql2.AppendLine("(");
                sql2.AppendLine("REG_TYPE_ID");
                sql2.AppendLine(", ROW_ID");
                sql2.AppendLine(", COLMUN_ID");
                sql2.AppendLine(", VAL_STRING");
                sql2.AppendLine(",REG_USER_CD");
                sql2.AppendLine(")");
                sql2.AppendLine("values ");
                sql2.AppendLine("(");
                sql2.AppendLine("@REG_TYPE_ID");                            //REG_TYOE_ID
                sql2.AppendLine(",@ROW_ID" + i);                                //ROW_ID
                sql2.AppendLine(",@CAS_NOCOLUMN");                          //COLUMN_ID
                sql2.AppendLine(",@CAS_NO" + i);                                //VAL_STRING
                sql2.AppendLine(",@" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                sql2.AppendLine(")");

                var data = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameters);
                //for (int j = 0; j < parameter.Count; j++)
                //{

                //    if (parameter.ParameterNames.Contains("CAS_NO"))
                //    {
                //        parameter.re
                //    }
                //    prop.re("CAS_NO" + i);
                //}
                parameters.Remove("CAS_NO" + i);

                //上限
                parameters.Add("MAXIMUM" + i, cas[i].maximum);
                sql2.Clear();
                sql2.AppendLine("insert into M_CAS_REGULATION");
                sql2.AppendLine("(");
                sql2.AppendLine("REG_TYPE_ID");
                sql2.AppendLine(", ROW_ID");
                sql2.AppendLine(", COLMUN_ID");
                sql2.AppendLine(", VAL_NUMBER");
                sql2.AppendLine(",REG_USER_CD");
                sql2.AppendLine(")");
                sql2.AppendLine("values ");
                sql2.AppendLine("(");
                sql2.AppendLine("@REG_TYPE_ID");                            //REG_TYOE_ID
                sql2.AppendLine(",@ROW_ID" + i);                                //ROW_ID
                sql2.AppendLine(",@上限COLUMN");                            //COLMUN_ID
                sql2.AppendLine(",@MAXIMUM" + i);                               //VAL_NUMBER
                sql2.AppendLine(",@" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                sql2.AppendLine(")");

                data = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameters);
                parameters.Remove("MAXIMUM" + i);

                //下限
                parameters.Add("MINIMUM" + i, cas[i].minimum);
                sql2.Clear();
                sql2.AppendLine("insert into M_CAS_REGULATION");
                sql2.AppendLine("(");
                sql2.AppendLine("REG_TYPE_ID");
                sql2.AppendLine(", ROW_ID");
                sql2.AppendLine(", COLMUN_ID");
                sql2.AppendLine(", VAL_NUMBER");
                sql2.AppendLine(",REG_USER_CD");
                sql2.AppendLine(")");
                sql2.AppendLine("values ");
                sql2.AppendLine("(");
                sql2.AppendLine("@REG_TYPE_ID");                            //REG_TYOE_ID
                sql2.AppendLine(",@ROW_ID" + i);                                //ROW_ID
                sql2.AppendLine(",@下限COLUMN");                            //COLMUN_ID
                sql2.AppendLine(",@MINIMUM" + i);                               //VAL_NUMBER
                sql2.AppendLine(",@" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                sql2.AppendLine(")");

                data = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameters);
                parameters.Remove("MINIMUM" + i);

                if (cas[i].shape != null)
                {
                    //形状
                    string[] arr = cas[i].shape.Split(';');
                    int y = 0;
                    foreach (string shape in arr)
                    {
                        parameters.Add("SHAPE_VALUE" + i + y, shape);

                        sql2.Clear();
                        sql2.AppendLine("select KEY_VALUE from M_COMMON");
                        sql2.AppendLine(" where DISPLAY_VALUE=@SHAPE_VALUE" + i + y);
                        sql2.AppendLine(" and TABLE_NAME = 'M_FIGURE'");

                        DataTable tempdata = DbUtil.Select(databaseContext, sql2.ToString(), parameters);
                        parameters.Remove("SHAPE_VALUE" + i + y);

                        var shapevalue = "";
                        foreach (DataRow Tempdata in tempdata.Rows)
                        {
                            shapevalue = Tempdata["KEY_VALUE"].ToString();
                        }

                        parameters.Add("SHAPE" + i + y, shapevalue);
                        sql2.Clear();
                        sql2.AppendLine("insert into M_CAS_REGULATION");
                        sql2.AppendLine("(");
                        sql2.AppendLine("REG_TYPE_ID");
                        sql2.AppendLine(", ROW_ID");
                        sql2.AppendLine(", COLMUN_ID");
                        sql2.AppendLine(", VAL_STRING");
                        sql2.AppendLine(",REG_USER_CD");
                        sql2.AppendLine(")");
                        sql2.AppendLine("values ");
                        sql2.AppendLine("(");
                        sql2.AppendLine("@REG_TYPE_ID");                            //REG_TYOE_ID
                        sql2.AppendLine(",@ROW_ID" + i);                                //ROW_ID
                        sql2.AppendLine(",@形状COLUMN");                            //COLMUN_ID
                        sql2.AppendLine(",@SHAPE" + i + y);                              //VAL_NUMBER
                        sql2.AppendLine(",@" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                        sql2.AppendLine(")");

                        data = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameters);
                        parameters.Remove("SHAPE" + i + y);

                        y++;
                    }
                }
                else
                {
                    //形状
                    //string[] arr = cas[i].shape.Split(';');
                    int y = 0;
                    var shape = "";

                    parameters.Add("SHAPE_VALUE" + i + y, shape);

                    sql2.Clear();
                    sql2.AppendLine("select KEY_VALUE from M_COMMON");
                    sql2.AppendLine(" where DISPLAY_VALUE=@SHAPE_VALUE" + i + y);
                    sql2.AppendLine(" and TABLE_NAME = 'M_FIGURE'");

                    DataTable tempdata = DbUtil.Select(databaseContext, sql2.ToString(), parameters);
                    parameters.Remove("SHAPE_VALUE" + i + y);

                    var shapevalue = "";
                    foreach (DataRow Tempdata in tempdata.Rows)
                    {
                        shapevalue = Tempdata["KEY_VALUE"].ToString();
                    }

                    parameters.Add("SHAPE" + i + y, shapevalue);
                    sql2.Clear();
                    sql2.AppendLine("insert into M_CAS_REGULATION");
                    sql2.AppendLine("(");
                    sql2.AppendLine("REG_TYPE_ID");
                    sql2.AppendLine(", ROW_ID");
                    sql2.AppendLine(", COLMUN_ID");
                    sql2.AppendLine(", VAL_STRING");
                    sql2.AppendLine(",REG_USER_CD");
                    sql2.AppendLine(")");
                    sql2.AppendLine("values ");
                    sql2.AppendLine("(");
                    sql2.AppendLine("@REG_TYPE_ID");                            //REG_TYOE_ID
                    sql2.AppendLine(",@ROW_ID" + i);                                //ROW_ID
                    sql2.AppendLine(",@形状COLUMN");                            //COLMUN_ID
                    sql2.AppendLine(",@SHAPE" + i + y);                              //VAL_NUMBER
                    sql2.AppendLine(",@" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                    sql2.AppendLine(")");

                    data = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameters);
                    parameters.Remove("SHAPE" + i + y);

                    y++;

                }

                if (cas[i].terms != null)
                {
                    //使用条件
                    string[] arr2 = cas[i].terms.Split(';');
                    int z = 0;

                    foreach (string terms in arr2)
                    {
                        parameters.Add("TERMS_VALUE" + i + z, terms);

                        sql2.Clear();
                        sql2.AppendLine("select KEY_VALUE from M_COMMON");
                        sql2.AppendLine(" where DISPLAY_VALUE=@TERMS_VALUE" + i + z);
                        sql2.AppendLine(" and TABLE_NAME = 'M_ISHA_USAGE'");

                        DataTable tempdata = DbUtil.Select(sql2.ToString(), parameters);
                        parameters.Remove("TERMS_VALUE" + i + z);

                        var termsvalue = "";
                        foreach (DataRow Tempdata in tempdata.Rows)
                        {
                            termsvalue = Tempdata["KEY_VALUE"].ToString();
                        }

                        parameters.Add("TERMS" + i + z, termsvalue);
                        sql2.Clear();
                        sql2.AppendLine("insert into M_CAS_REGULATION");
                        sql2.AppendLine("(");
                        sql2.AppendLine("REG_TYPE_ID");
                        sql2.AppendLine(", ROW_ID");
                        sql2.AppendLine(", COLMUN_ID");
                        sql2.AppendLine(", VAL_STRING");
                        sql2.AppendLine(",REG_USER_CD");
                        sql2.AppendLine(")");
                        sql2.AppendLine("values ");
                        sql2.AppendLine("(");
                        sql2.AppendLine("@REG_TYPE_ID");                            //REG_TYOE_ID
                        sql2.AppendLine(",@ROW_ID" + i);                                //ROW_ID
                        sql2.AppendLine(",@使用条件COLUMN");                        //COLMUN_ID
                        sql2.AppendLine(",@TERMS" + i + z);                               //VAL_NUMBER
                        sql2.AppendLine(",@" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                        sql2.AppendLine(")");

                        data = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameters);
                        parameters.Remove("TERMS" + i + z);

                        z++;
                    }
                }
                else
                {
                    //使用条件
                    // string[] arr2 = cas[i].terms.Split(';');
                    int z = 0;

                    var terms = "";

                    parameters.Add("TERMS_VALUE" + i + z, terms);

                    sql2.Clear();
                    sql2.AppendLine("select KEY_VALUE from M_COMMON");
                    sql2.AppendLine(" where DISPLAY_VALUE=@TERMS_VALUE" + i + z);
                    sql2.AppendLine(" and TABLE_NAME = 'M_ISHA_USAGE'");

                    DataTable tempdata = DbUtil.Select(sql2.ToString(), parameters);
                    parameters.Remove("TERMS_VALUE" + i + z);

                    var termsvalue = "";
                    foreach (DataRow Tempdata in tempdata.Rows)
                    {
                        termsvalue = Tempdata["KEY_VALUE"].ToString();
                    }

                    parameters.Add("TERMS" + i + z, termsvalue);
                    sql2.Clear();
                    sql2.AppendLine("insert into M_CAS_REGULATION");
                    sql2.AppendLine("(");
                    sql2.AppendLine("REG_TYPE_ID");
                    sql2.AppendLine(", ROW_ID");
                    sql2.AppendLine(", COLMUN_ID");
                    sql2.AppendLine(", VAL_STRING");
                    sql2.AppendLine(",REG_USER_CD");
                    sql2.AppendLine(")");
                    sql2.AppendLine("values ");
                    sql2.AppendLine("(");
                    sql2.AppendLine("@REG_TYPE_ID");                            //REG_TYOE_ID
                    sql2.AppendLine(",@ROW_ID" + i);                                //ROW_ID
                    sql2.AppendLine(",@使用条件COLUMN");                        //COLMUN_ID
                    sql2.AppendLine(",@TERMS" + i + z);                               //VAL_NUMBER
                    sql2.AppendLine(",@" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                    sql2.AppendLine(")");

                    data = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameters);
                    parameters.Remove("TERMS" + i + z);

                    z++;

                }
                parameters.Remove("ROW_ID" + i);

            }

            databaseContext.Commit();
            databaseContext.Close();

        }

        /// <summary>
        /// テーブル管理情報とマスターデータからマスターのデータを更新します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="UpdateKeys">更新するマスターのキーを指定します。</param>
        /// <param name="UpdateDatas">更新するマスターのデータを指定します。</param>
        /// <param name="userCode">更新者のユーザーコード。</param>
        public static void UpdateCommonMaster(S_TAB_CONTROL TableInformation, Dictionary<string, string> UpdateKeys, Dictionary<string, string> UpdateDatas, string userCode)
        {
            var databaseContext = DbUtil.Open(true);
            var sql = new StringBuilder();
            var userCodeColumn = string.Empty;
            var parameter = UpdateDatas.ToHashtable();
            if (parameter.ContainsKey(nameof(SystemColumn.DEL_FLAG)) && parameter[nameof(SystemColumn.DEL_FLAG)].ToString() == ((int)DEL_FLAG.Deleted).ToString())
            {
                userCodeColumn = nameof(SystemColumn.DEL_USER_CD);
            }
            else
            {
                userCodeColumn = nameof(SystemColumn.UPD_USER_CD);
            }

            if (TableInformation.TABLE_NAME != "M_GROUP"
                //2019/02/18 TPE.Sugimoto Add Sta
                && TableInformation.TABLE_NAME != "M_LOCATION"
                && TableInformation.TABLE_NAME != "M_MAKER"
                && TableInformation.TABLE_NAME != "M_UNITSIZE"
                && TableInformation.TABLE_NAME != "M_WORKFLOW"
                && TableInformation.TABLE_NAME != "M_REGULATION"    //2019/03/01 TPE.Rin Add
                                                                    //2019/02/18 TPE.Sugimoto Add End
                )
            {
                parameter.Add(userCodeColumn, userCode);
            }
            //else if (TableInformation.TABLE_NAME == "M_GROUP" && userCodeColumn != nameof(SystemColumn.DEL_USER_CD))//2019/02/18 TPE.Sugimoto Del
            //2019/02/18 TPE.Sugimoto Add Sta
            else if ((TableInformation.TABLE_NAME == "M_GROUP" && userCodeColumn != nameof(SystemColumn.DEL_USER_CD))
                || (TableInformation.TABLE_NAME == "M_LOCATION" && userCodeColumn != nameof(SystemColumn.DEL_USER_CD))
                || (TableInformation.TABLE_NAME == "M_MAKER" && userCodeColumn != nameof(SystemColumn.DEL_USER_CD))
                || (TableInformation.TABLE_NAME == "M_UNITSIZE" && userCodeColumn != nameof(SystemColumn.DEL_USER_CD))
                || (TableInformation.TABLE_NAME == "M_WORKFLOW" && userCodeColumn != nameof(SystemColumn.DEL_USER_CD))
                || (TableInformation.TABLE_NAME == "M_REGULATION" && userCodeColumn != nameof(SystemColumn.DEL_USER_CD)))   //2019/03/01 TPE.Rin Add
            //2019/02/18 TPE.Sugimoto Add End
            {
                parameter.Add(userCodeColumn, userCode);
            }

            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("update ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);

            sql.AppendLine("set ");
            foreach (var updateData in UpdateDatas)
            {
                if (TableInformation.TABLE_NAME == "M_GROUP" && updateData.Key == nameof(SystemColumn.DEL_USER_CD) && userCodeColumn == nameof(SystemColumn.DEL_USER_CD)) continue;
                //2019/02/18 TPE.Sugimoto Add Sta
                if (TableInformation.TABLE_NAME == "M_LOCATION" && updateData.Key == nameof(SystemColumn.DEL_USER_CD) && userCodeColumn == nameof(SystemColumn.DEL_USER_CD)) continue;
                if (TableInformation.TABLE_NAME == "M_MAKER" && updateData.Key == nameof(SystemColumn.DEL_USER_CD) && userCodeColumn == nameof(SystemColumn.DEL_USER_CD)) continue;
                if (TableInformation.TABLE_NAME == "M_UNITSIZE" && updateData.Key == nameof(SystemColumn.DEL_USER_CD) && userCodeColumn == nameof(SystemColumn.DEL_USER_CD)) continue;
                if (TableInformation.TABLE_NAME == "M_WORKFLOW" && updateData.Key == nameof(SystemColumn.DEL_USER_CD) && userCodeColumn == nameof(SystemColumn.DEL_USER_CD)) continue;
                if (TableInformation.TABLE_NAME == "M_REGULATION" && updateData.Key == nameof(SystemColumn.DEL_USER_CD) && userCodeColumn == nameof(SystemColumn.DEL_USER_CD)) continue;    //2019/03/01 TPE.Rin Add
                //2019/02/18 TPE.Sugimoto Add End

                sql.AppendLine(" " + updateData.Key + " = :" + updateData.Key + ",");

            }

            sql.AppendLine(" " + userCodeColumn + " = :" + userCodeColumn);
            sql.Append(" " + Environment.NewLine);


            sql.AppendLine("where ");
            foreach (var updateKey in UpdateKeys)
            {
                sql.AppendLine(" " + updateKey.Key + " = :" + updateKey.Key);
                sql.AppendLine(" and  ");
                parameter.Add(updateKey.Key, updateKey.Value);
            }

            sql.Length -= 8;
            var data = DbUtil.ExecuteUpdate(databaseContext, sql.ToString(), parameter);

            databaseContext.Commit();
            databaseContext.Close();
        }

        //2018/08/04 TPE Add
        /// <summary>
        /// テーブル管理情報とマスターデータからマスターのデータを更新します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="UpdateKeys">更新するマスターのキーを指定します。</param>
        /// <param name="UpdateDatas">更新するマスターのデータを指定します。</param>
        /// <param name="userCode">更新者のユーザーコード。</param>
        public static void UpdateLocation(S_TAB_CONTROL TableInformation, Dictionary<string, string> UpdateKeys, Dictionary<string, string> UpdateDatas, string userCode, Dictionary<string, string> chemChief)
        {
            var databaseContext = DbUtil.Open(true);
            var sql = new StringBuilder();
            var userCodeColumn = string.Empty;
            var parameter = UpdateDatas.ToHashtable();
            // DynamicParameters parameter = new DynamicParameters();//UpdateDatas.ToHashtable();
            // foreach (var pair in UpdateDatas) parameter.Add(pair.Key, pair.Value);
            if (parameter.ContainsKey(nameof(SystemColumn.DEL_FLAG)) && parameter[nameof(SystemColumn.DEL_FLAG)].ToString() == ((int)DEL_FLAG.Deleted).ToString())
            {
                userCodeColumn = nameof(SystemColumn.DEL_USER_CD);
            }
            else
            {
                userCodeColumn = nameof(SystemColumn.UPD_USER_CD);
            }
            //2019/02/18 TPE.Sugimoto Upd Sta
            if (parameter.ContainsKey(nameof(SystemColumn.DEL_FLAG)) && parameter[nameof(SystemColumn.DEL_FLAG)].ToString() == ((int)DEL_FLAG.Deleted).ToString())
            {
            }
            else
            {
                parameter.Add(userCodeColumn, userCode);
            }
            //2019/02/18 TPE.Sugimoto Upd End
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("update ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);

            sql.AppendLine("set ");
            foreach (var updateData in UpdateDatas)
            {
                if (updateData.Key == "USER_CD") continue;
                if (updateData.Key == "USER_NAME") continue;
                if (updateData.Key == "P_LOC_BASEID") continue; //2018/10/03 Rin Add
                //2019/02/18 TPE.Sugimoto Add Sta
                if (updateData.Key == "HAZARD_CHECK1") continue;
                if (updateData.Key == "HAZARD_CHECK2") continue;
                if (updateData.Key == "HAZARD_CHECK3") continue;
                if (updateData.Key == "HAZARD_CHECK4") continue;
                if (updateData.Key == "HAZARD_CHECK5") continue;
                if (updateData.Key == "HAZARD_CHECK6") continue;
                if (updateData.Key == "HAZARD_CHECK7") continue;
                //2019/02/18 TPE.Sugimoto Add End

                sql.AppendLine(" " + updateData.Key + " = :" + updateData.Key + ",");
            }
            //2019/02/18 TPE.Sugimoto Upd Sta
            if (parameter.ContainsKey(nameof(SystemColumn.DEL_FLAG)) && parameter[nameof(SystemColumn.DEL_FLAG)].ToString() == ((int)DEL_FLAG.Deleted).ToString())
            {
                sql.Length -= 3;
            }
            else
            {
                sql.AppendLine(" " + userCodeColumn + " = :" + userCodeColumn);
            }
            //2019/02/18 TPE.Sugimoto Upd End
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("where ");
            foreach (var updateKey in UpdateKeys)
            {
                sql.AppendLine(" " + updateKey.Key + " = :" + updateKey.Key);
                sql.AppendLine(" and  ");
                parameter.Add(updateKey.Key, updateKey.Value);
            }
            sql.Length -= 8;

            var data = DbUtil.ExecuteUpdate(databaseContext, sql.ToString(), parameter);

            var sql2 = new StringBuilder();
            sql2.AppendLine("delete from M_CHIEF where ");

            foreach (var updateKey in UpdateKeys)
            {
                sql2.AppendLine(" " + updateKey.Key + " = :" + updateKey.Key);

            }
            var data2 = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameter);

            int i = 0;

            if (chemChief != null)
            {
                foreach (KeyValuePair<string, string> chemchief in chemChief)
                {
                    i++;


                    parameter.Add("USER_CD_" + i, chemchief.Key);
                    var sql3 = new StringBuilder();
                    sql3.AppendLine("insert into M_CHIEF(LOC_ID,USER_CD,REG_USER_CD) ");
                    sql3.AppendLine("values ");
                    sql3.AppendLine("(");
                    sql3.AppendLine(":LOC_ID");
                    sql3.AppendLine(",:USER_CD_" + i);
                    sql3.AppendLine(",:" + userCodeColumn);
                    sql3.AppendLine(" )");

                    var data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameter);
                }
            }
            //使用可能危険物の更新処理
            //2019/02/18 TPE.Sugimoto Add Sta
            var sql4 = new StringBuilder();
            sql4.AppendLine("delete from M_LOCATION_HAZARD where LOC_ID = :LOC_ID");
            var data4 = DbUtil.ExecuteUpdate(databaseContext, sql4.ToString(), parameter);

            var sql_1 = new StringBuilder();
            sql_1.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_1.AppendLine("values ");
            sql_1.AppendLine("(");
            sql_1.AppendLine(":LOC_ID");
            sql_1.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第一類' )");
            sql_1.AppendLine("," + parameter["HAZARD_CHECK1"]);
            sql_1.AppendLine(",:" + userCodeColumn);
            sql_1.AppendLine(" )");
            var data_1 = DbUtil.ExecuteUpdate(databaseContext, sql_1.ToString(), parameter);

            var sql_2 = new StringBuilder();
            sql_2.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_2.AppendLine("values ");
            sql_2.AppendLine("(");
            sql_2.AppendLine(":LOC_ID");
            sql_2.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第二類' )");
            sql_2.AppendLine("," + parameter["HAZARD_CHECK2"]);
            sql_2.AppendLine(",:" + userCodeColumn);
            sql_2.AppendLine(" )");
            var data_2 = DbUtil.ExecuteUpdate(databaseContext, sql_2.ToString(), parameter);

            var sql_3 = new StringBuilder();
            sql_3.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_3.AppendLine("values ");
            sql_3.AppendLine("(");
            sql_3.AppendLine(":LOC_ID");
            sql_3.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第三類' )");
            sql_3.AppendLine("," + parameter["HAZARD_CHECK3"]);
            sql_3.AppendLine(",:" + userCodeColumn);
            sql_3.AppendLine(" )");
            var data_3 = DbUtil.ExecuteUpdate(databaseContext, sql_3.ToString(), parameter);

            var sql_4 = new StringBuilder();
            sql_4.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_4.AppendLine("values ");
            sql_4.AppendLine("(");
            sql_4.AppendLine(":LOC_ID");
            sql_4.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第四類' )");
            sql_4.AppendLine("," + parameter["HAZARD_CHECK4"]);
            sql_4.AppendLine(",:" + userCodeColumn);
            sql_4.AppendLine(" )");
            var data_4 = DbUtil.ExecuteUpdate(databaseContext, sql_4.ToString(), parameter);

            var sql_5 = new StringBuilder();
            sql_5.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_5.AppendLine("values ");
            sql_5.AppendLine("(");
            sql_5.AppendLine(":LOC_ID");
            sql_5.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第五類' )");
            sql_5.AppendLine("," + parameter["HAZARD_CHECK5"]);
            sql_5.AppendLine(",:" + userCodeColumn);
            sql_5.AppendLine(" )");
            var data_5 = DbUtil.ExecuteUpdate(databaseContext, sql_5.ToString(), parameter);

            var sql_6 = new StringBuilder();
            sql_6.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_6.AppendLine("values ");
            sql_6.AppendLine("(");
            sql_6.AppendLine(":LOC_ID");
            sql_6.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '第六類' )");
            sql_6.AppendLine("," + parameter["HAZARD_CHECK6"]);
            sql_6.AppendLine(",:" + userCodeColumn);
            sql_6.AppendLine(" )");
            var data_6 = DbUtil.ExecuteUpdate(databaseContext, sql_6.ToString(), parameter);

            var sql_7 = new StringBuilder();
            sql_7.AppendLine("insert into M_LOCATION_HAZARD(LOC_ID,REG_TYPE_ID,HAZARD_CHECK,REG_USER_CD) ");
            sql_7.AppendLine("values ");
            sql_7.AppendLine("(");
            sql_7.AppendLine(":LOC_ID");
            sql_7.AppendLine(",(SELECT REG_TYPE_ID FROM M_REGULATION WHERE P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and REG_TEXT = '指定可燃物' )");
            sql_7.AppendLine("," + parameter["HAZARD_CHECK7"]);
            sql_7.AppendLine(",:" + userCodeColumn);
            sql_7.AppendLine(" )");
            var data_7 = DbUtil.ExecuteUpdate(databaseContext, sql_7.ToString(), parameter);
            //2019/02/18 TPE.Sugimoto Add End


            databaseContext.Commit();
            databaseContext.Close();
        }

        //2019/03/01 TPE.Rin Add Start
        /// <summary>
        /// テーブル管理情報とマスターデータからマスターのデータを更新します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="UpdateKeys">更新するマスターのキーを指定します。</param>
        /// <param name="UpdateDatas">更新するマスターのデータを指定します。</param>
        /// <param name="userCode">更新者のユーザーコード。</param>
        public static void UpdateCAS(S_TAB_CONTROL TableInformation, Dictionary<string, string> UpdateKeys, Dictionary<string, string> UpdateDatas, string userCode, List<CasRegModel> cas)
        {
            var databaseContext = DbUtil.Open(true);
            var sql = new StringBuilder();
            var userCodeColumn = string.Empty;
            var parameter = UpdateDatas.ToHashtable();
            //if (parameter.ContainsKey(nameof(SystemColumn.DEL_FLAG)) && parameter[nameof(SystemColumn.DEL_FLAG)].ToString() == ((int)DEL_FLAG.Deleted).ToString())
            //{
            //    userCodeColumn = nameof(SystemColumn.DEL_USER_CD);
            //}
            //else
            //{
            //    userCodeColumn = nameof(SystemColumn.UPD_USER_CD);
            //}
            //parameter.Add(userCodeColumn, userCode);
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("update ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);

            sql.AppendLine("set ");
            foreach (var updateData in UpdateDatas)
            {
                if (updateData.Key == "USER_CD") continue;
                if (updateData.Key == "USER_NAME") continue;
                if (updateData.Key == "P_LOC_BASEID") continue; //2018/10/03 Rin Add

                sql.AppendLine(" " + updateData.Key + " = :" + updateData.Key + ",");
            }
            //sql.AppendLine(" " + userCodeColumn + " = :" + userCodeColumn);

            //2019/05/11 TPE.Rin Add
            //最後のカンマを削除
            sql.Length -= 3;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("where ");
            foreach (var updateKey in UpdateKeys)
            {
                sql.AppendLine(" " + updateKey.Key + " = :" + updateKey.Key);
                sql.AppendLine(" and  ");
                parameter.Add(updateKey.Key, updateKey.Value);
            }
            sql.Length -= 8;

            var data = DbUtil.ExecuteUpdate(databaseContext, sql.ToString(), parameter);

            var sql2 = new StringBuilder();
            sql2.AppendLine("delete from M_CAS_REGULATION where ");

            foreach (var updateKey in UpdateKeys)
            {

                sql2.AppendLine(" " + updateKey.Key + " = :" + updateKey.Key);

            }
            var data2 = DbUtil.ExecuteUpdate(databaseContext, sql2.ToString(), parameter);

            //M_COMMONからデータ取得
            DbContext context = null;
            context = DbUtil.Open(false);

            var sqltemp = new System.Text.StringBuilder();
            sqltemp.AppendLine("select");
            sqltemp.AppendLine(" KEY_VALUE ");
            sqltemp.AppendLine(", DISPLAY_VALUE ");
            sqltemp.AppendLine("from");
            sqltemp.AppendLine(" M_COMMON");
            sqltemp.AppendLine("where");
            sqltemp.AppendLine(" TABLE_NAME = 'M_CAS_REGULATION'");
            sqltemp.AppendLine(" and");
            sqltemp.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

            var records = DbUtil.Select(context, sqltemp.ToString());

            context.Close();

            var dataArray = new List<M_COMMON>();

            foreach (DataRow record in records.Rows)
            {
                var model = new M_COMMON();

                model.KEY_VALUE = int.Parse(record[nameof(model.KEY_VALUE)].ToString());
                model.DISPLAY_VALUE = record[nameof(model.DISPLAY_VALUE)].ToString();

                dataArray.Add(model);
            }

            for (int x = 0; x < dataArray.Count; x++)
            {
                parameter.Add(dataArray[x].DISPLAY_VALUE + "COLUMN", dataArray[x].KEY_VALUE);
            }

            for (int i = 0; i < cas.Count; i++)
            {
                //casno
                parameter.Add("CAS_NO" + i, cas[i].casno);
                parameter.Add("ROW_ID" + i, i);
                var sql3 = new StringBuilder();

                sql3.AppendLine("insert into M_CAS_REGULATION");
                sql3.AppendLine("(");
                sql3.AppendLine("REG_TYPE_ID");
                sql3.AppendLine(", ROW_ID");
                sql3.AppendLine(", COLMUN_ID");
                sql3.AppendLine(", VAL_STRING");
                sql3.AppendLine(",REG_USER_CD");
                sql3.AppendLine(")");
                sql3.AppendLine("values ");
                sql3.AppendLine("(");
                sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                sql3.AppendLine(",:CAS_NOCOLUMN");                          //COLUMN_ID
                sql3.AppendLine(",:CAS_NO" + i);                                //VAL_STRING
                sql3.AppendLine(",:" + nameof(SystemColumn.UPD_USER_CD));   //REG_USER_CD
                sql3.AppendLine(")");

                var data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameter);
                parameter.Remove("CAS_NO" + i);

                //上限
                parameter.Add("MAXIMUM" + i, cas[i].maximum);
                sql3.Clear();
                sql3.AppendLine("insert into M_CAS_REGULATION");
                sql3.AppendLine("(");
                sql3.AppendLine("REG_TYPE_ID");
                sql3.AppendLine(", ROW_ID");
                sql3.AppendLine(", COLMUN_ID");
                sql3.AppendLine(", VAL_NUMBER");
                sql3.AppendLine(",REG_USER_CD");
                sql3.AppendLine(")");
                sql3.AppendLine("values ");
                sql3.AppendLine("(");
                sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                sql3.AppendLine(",:上限COLUMN");                            //COLMUN_ID
                sql3.AppendLine(",:MAXIMUM" + i);                               //VAL_NUMBER
                sql3.AppendLine(",:" + nameof(SystemColumn.UPD_USER_CD));   //REG_USER_CD
                sql3.AppendLine(")");

                data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameter);
                parameter.Remove("MAXIMUM" + i);

                //下限
                parameter.Add("MINIMUM" + i, cas[i].minimum);
                sql3.Clear();
                sql3.AppendLine("insert into M_CAS_REGULATION");
                sql3.AppendLine("(");
                sql3.AppendLine("REG_TYPE_ID");
                sql3.AppendLine(", ROW_ID");
                sql3.AppendLine(", COLMUN_ID");
                sql3.AppendLine(", VAL_NUMBER");
                sql3.AppendLine(",REG_USER_CD");
                sql3.AppendLine(")");
                sql3.AppendLine("values ");
                sql3.AppendLine("(");
                sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                sql3.AppendLine(",:下限COLUMN");                            //COLMUN_ID
                sql3.AppendLine(",:MINIMUM" + i);                               //VAL_NUMBER
                sql3.AppendLine(",:" + nameof(SystemColumn.UPD_USER_CD));   //REG_USER_CD
                sql3.AppendLine(")");

                data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameter);
                parameter.Remove("MINIMUM" + i);

                //形状
                string[] arr = cas[i].shape.Split(';');
                int y = 0;
                foreach (string shape in arr)
                {
                    parameter.Add("SHAPE_VALUE" + i + y, shape);

                    sql3.Clear();
                    sql3.AppendLine("select KEY_VALUE from M_COMMON");
                    sql3.AppendLine(" where DISPLAY_VALUE=:SHAPE_VALUE" + i + y);
                    sql3.AppendLine(" and TABLE_NAME = 'M_FIGURE'");

                    DataTable tempdata = DbUtil.Select(databaseContext, sql3.ToString(), parameter);
                    parameter.Remove("SHAPE_VALUE" + i + y);

                    var shapevalue = "";
                    foreach (DataRow Tempdata in tempdata.Rows)
                    {
                        shapevalue = Tempdata["KEY_VALUE"].ToString();
                    }

                    parameter.Add("SHAPE" + i + y, shapevalue);
                    sql3.Clear();
                    sql3.AppendLine("insert into M_CAS_REGULATION");
                    sql3.AppendLine("(");
                    sql3.AppendLine("REG_TYPE_ID");
                    sql3.AppendLine(", ROW_ID");
                    sql3.AppendLine(", COLMUN_ID");
                    sql3.AppendLine(", VAL_STRING");
                    sql3.AppendLine(",REG_USER_CD");
                    sql3.AppendLine(")");
                    sql3.AppendLine("values ");
                    sql3.AppendLine("(");
                    sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                    sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                    sql3.AppendLine(",:形状COLUMN");                            //COLMUN_ID
                    sql3.AppendLine(",:SHAPE" + i + y);                              //VAL_NUMBER
                    sql3.AppendLine(",:" + nameof(SystemColumn.UPD_USER_CD));   //REG_USER_CD
                    sql3.AppendLine(")");

                    data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameter);
                    parameter.Remove("SHAPE" + i + y);

                    y++;
                }

                //使用条件
                string[] arr2 = cas[i].terms.Split(';');
                int z = 0;

                foreach (string terms in arr2)
                {
                    parameter.Add("TERMS_VALUE" + i + z, terms);

                    sql3.Clear();
                    sql3.AppendLine("select KEY_VALUE from M_COMMON");
                    sql3.AppendLine(" where DISPLAY_VALUE=:TERMS_VALUE" + i + z);
                    sql3.AppendLine(" and TABLE_NAME = 'M_ISHA_USAGE'");

                    DataTable tempdata = DbUtil.Select(databaseContext, sql3.ToString(), parameter);
                    parameter.Remove("TERMS_VALUE" + i + z);

                    var termsvalue = "";
                    foreach (DataRow Tempdata in tempdata.Rows)
                    {
                        termsvalue = Tempdata["KEY_VALUE"].ToString();
                    }

                    parameter.Add("TERMS" + i + z, termsvalue);
                    sql3.Clear();
                    sql3.AppendLine("insert into M_CAS_REGULATION");
                    sql3.AppendLine("(");
                    sql3.AppendLine("REG_TYPE_ID");
                    sql3.AppendLine(", ROW_ID");
                    sql3.AppendLine(", COLMUN_ID");
                    sql3.AppendLine(", VAL_STRING");
                    sql3.AppendLine(",REG_USER_CD");
                    sql3.AppendLine(")");
                    sql3.AppendLine("values ");
                    sql3.AppendLine("(");
                    sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                    sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                    sql3.AppendLine(",:使用条件COLUMN");                        //COLMUN_ID
                    sql3.AppendLine(",:TERMS" + i + z);                               //VAL_NUMBER
                    sql3.AppendLine(",:" + nameof(SystemColumn.UPD_USER_CD));   //REG_USER_CD
                    sql3.AppendLine(")");

                    data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameter);
                    parameter.Remove("TERMS" + i + z);

                    z++;
                }
                parameter.Remove("ROW_ID" + i);

            }

            databaseContext.Commit();
            databaseContext.Close();
        }

        public static void UpdateCAS(S_TAB_CONTROL TableInformation, Dictionary<string, string> UpdateKeys, M_MASTER UpdateDatas, string userCode, List<CasRegModel> cas)

        {
            var databaseContext = DbUtil.Open(true);
            var sql = new StringBuilder();
            var userCodeColumn = string.Empty;
            var parameter = new DynamicParameters();
            var parameters = new Hashtable();

            //if (parameters.ContainsKey(nameof(SystemColumn.DEL_FLAG)) && parameters[nameof(SystemColumn.DEL_FLAG)].ToString() == ((int)DEL_FLAG.Deleted).ToString())
            //{
            //    userCodeColumn = "@DEL_USER_CD";
            //}
            //else
            //{
            //    userCodeColumn = "@UPD_USER_CD";
            //}
            //parameters.Add(userCodeColumn, userCode);
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("update ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);

            sql.AppendLine("set ");
            //foreach (var updateData in UpdateDatas)
            //{
            //    if (updateData.Key == "USER_CD") continue;
            //    if (updateData.Key == "USER_NAME") continue;
            //    if (updateData.Key == "P_LOC_BASEID") continue; //2018/10/03 Rin Add

            sql.AppendLine("CLASS_ID = @CLASS_ID, REG_TEXT = @REG_TEXT, REG_ACRONYM_TEXT = @REG_ACRONYM_TEXT, REG_ICON_PATH = @REG_ICON_PATH, FIRE_SIZE = @FIRE_SIZE, UNITSIZE_ID = @UNITSIZE_ID, SORT_ORDER = @SORT_ORDER, KEY_MGMT = @KEY_MGMT, WEIGHT_MGMT = @WEIGHT_MGMT, WORKTIME_MGMT = @WORKTIME_MGMT, P_REG_TYPE_ID = @P_REG_TYPE_ID, EXPOSURE_REPORT = @EXPOSURE_REPORT, EXAMINATION_FLAG = @EXAMINATION_FLAG, MEASUREMENT_FLAG = @MEASUREMENT_FLAG, RA_FLAG = @RA_FLAG, MARKS = @MARKS, DEL_FLAG = @DEL_FLAG, UPD_USER_CD = @UPD_USER_CD");

            //sql.AppendLine(" " + updateData.Key + " = @" + updateData.Key + ",");
            //}
            //sql.AppendLine(" " + userCodeColumn + " = :" + userCodeColumn);

            //2019/05/11 TPE.Rin Add
            //最後のカンマを削除
            //sql.Length -= 3;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("where ");
            foreach (var updateKey in UpdateKeys)
            {
                sql.AppendLine(" " + updateKey.Key + " = @" + updateKey.Key);
                sql.AppendLine(" and  ");
                parameter.Add(updateKey.Key, updateKey.Value);
            }
            parameter.Add("@CLASS_ID", UpdateDatas.mRegulation.CLASS_ID);
            parameter.Add("@REG_TEXT", UpdateDatas.mRegulation.REG_TEXT);
            parameter.Add("@REG_ACRONYM_TEXT", UpdateDatas.mRegulation.REG_ACRONYM_TEXT);
            parameter.Add("@REG_ICON_PATH", UpdateDatas.mRegulation.REG_ICON_PATH);
            parameter.Add("@FIRE_SIZE", UpdateDatas.mRegulation.FIRE_SIZE);
            parameter.Add("@UNITSIZE_ID", UpdateDatas.mRegulation.UNITSIZE_ID);
            parameter.Add("@SORT_ORDER", UpdateDatas.mRegulation.SORT_ORDER);
            parameter.Add("@KEY_MGMT", UpdateDatas.mRegulation.KEY_MGMT);
            parameter.Add("@WEIGHT_MGMT", UpdateDatas.mRegulation.WEIGHT_MGMT);
            parameter.Add("@WORKTIME_MGMT", UpdateDatas.mRegulation.WORKTIME_MGMT);
            parameter.Add("@P_REG_TYPE_ID", UpdateDatas.mRegulation.P_REG_TYPE_ID);
            parameter.Add("@EXPOSURE_REPORT", UpdateDatas.mRegulation.EXPOSURE_REPORT);
            parameter.Add("@EXAMINATION_FLAG", UpdateDatas.mRegulation.EXAMINATION_FLAG);
            parameter.Add("@MEASUREMENT_FLAG", UpdateDatas.mRegulation.MEASUREMENT_FLAG);
            parameter.Add("@RA_FLAG", UpdateDatas.mRegulation.RA_FLAG);
            parameter.Add("@MARKS", UpdateDatas.mRegulation.MARKS);
            parameter.Add("@DEL_FLAG", UpdateDatas.mRegulation.DEL_FLAG);
            parameter.Add("@UPD_USER_CD", userCode);
            sql.Length -= 8;



            var data = DbUtil.ExecuteUpdate(sql.ToString(), parameter);

            var sql2 = new StringBuilder();
            sql2.AppendLine("delete from M_CAS_REGULATION where ");

            foreach (var updateKey in UpdateKeys)
            {

                sql2.AppendLine(" " + updateKey.Key + " = @" + updateKey.Key);
                parameter.Add(updateKey.Key, updateKey.Value);

            }
            var data2 = DbUtil.ExecuteUpdate(sql2.ToString(), parameter);

            //M_COMMONからデータ取得
            DbContext context = null;
            context = DbUtil.Open(false);

            var sqltemp = new System.Text.StringBuilder();
            sqltemp.AppendLine("select");
            sqltemp.AppendLine(" KEY_VALUE ");
            sqltemp.AppendLine(", DISPLAY_VALUE ");
            sqltemp.AppendLine("from");
            sqltemp.AppendLine(" M_COMMON");
            sqltemp.AppendLine("where");
            sqltemp.AppendLine(" TABLE_NAME = 'M_CAS_REGULATION'");
            sqltemp.AppendLine(" and");
            sqltemp.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

            var records = DbUtil.Select(context, sqltemp.ToString());

            context.Close();

            var dataArray = new List<M_COMMON>();

            foreach (DataRow record in records.Rows)
            {
                var model = new M_COMMON();

                model.KEY_VALUE = int.Parse(record[nameof(model.KEY_VALUE)].ToString());
                model.DISPLAY_VALUE = record[nameof(model.DISPLAY_VALUE)].ToString();

                dataArray.Add(model);
            }

            for (int x = 0; x < dataArray.Count; x++)
            {
                parameters.Add(dataArray[x].DISPLAY_VALUE + "COLUMN", dataArray[x].KEY_VALUE);
            }

            for (int i = 0; i < cas.Count; i++)
            {
                //casno
                parameters.Add("REG_TYPE_ID", UpdateDatas.mRegulation.REG_TYPE_ID);
                parameters.Add("REG_USER_CD", userCode);
                parameters.Add("CAS_NO" + i, cas[i].casno);
                parameters.Add("ROW_ID" + i, i);
                var sql3 = new StringBuilder();

                sql3.AppendLine("insert into M_CAS_REGULATION");
                sql3.AppendLine("(");
                sql3.AppendLine("REG_TYPE_ID");
                sql3.AppendLine(", ROW_ID");
                sql3.AppendLine(", COLMUN_ID");
                sql3.AppendLine(", VAL_STRING");
                sql3.AppendLine(",REG_USER_CD");
                sql3.AppendLine(")");
                sql3.AppendLine("values ");
                sql3.AppendLine("(");
                sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                sql3.AppendLine(",:CAS_NOCOLUMN");                          //COLUMN_ID
                sql3.AppendLine(",:CAS_NO" + i);                                //VAL_STRING
                sql3.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                sql3.AppendLine(")");

                var data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameters);
                parameters.Remove("CAS_NO" + i);

                //上限
                parameters.Add("MAXIMUM" + i, cas[i].maximum);
                sql3.Clear();
                sql3.AppendLine("insert into M_CAS_REGULATION");
                sql3.AppendLine("(");
                sql3.AppendLine("REG_TYPE_ID");
                sql3.AppendLine(", ROW_ID");
                sql3.AppendLine(", COLMUN_ID");
                sql3.AppendLine(", VAL_NUMBER");
                sql3.AppendLine(",REG_USER_CD");
                sql3.AppendLine(")");
                sql3.AppendLine("values ");
                sql3.AppendLine("(");
                sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                sql3.AppendLine(",:上限COLUMN");                            //COLMUN_ID
                sql3.AppendLine(",:MAXIMUM" + i);                               //VAL_NUMBER
                sql3.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                sql3.AppendLine(")");

                data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameters);
                parameters.Remove("MAXIMUM" + i);

                //下限
                parameters.Add("MINIMUM" + i, cas[i].minimum);
                sql3.Clear();
                sql3.AppendLine("insert into M_CAS_REGULATION");
                sql3.AppendLine("(");
                sql3.AppendLine("REG_TYPE_ID");
                sql3.AppendLine(", ROW_ID");
                sql3.AppendLine(", COLMUN_ID");
                sql3.AppendLine(", VAL_NUMBER");
                sql3.AppendLine(",REG_USER_CD");
                sql3.AppendLine(")");
                sql3.AppendLine("values ");
                sql3.AppendLine("(");
                sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                sql3.AppendLine(",:下限COLUMN");                            //COLMUN_ID
                sql3.AppendLine(",:MINIMUM" + i);                               //VAL_NUMBER
                sql3.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                sql3.AppendLine(")");

                data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameters);
                parameters.Remove("MINIMUM" + i);


                if (cas[i].shape != null)
                {
                    //形状
                    string[] arr = cas[i].shape.Split(';');
                    int y = 0;
                    foreach (string shape in arr)
                    {
                        parameters.Add("SHAPE_VALUE" + i + y, shape);

                        sql3.Clear();
                        sql3.AppendLine("select KEY_VALUE from M_COMMON");
                        sql3.AppendLine(" where DISPLAY_VALUE=:SHAPE_VALUE" + i + y);
                        sql3.AppendLine(" and TABLE_NAME = 'M_FIGURE'");

                        DataTable tempdata = DbUtil.Select(databaseContext, sql3.ToString(), parameters);
                        parameters.Remove("SHAPE_VALUE" + i + y);

                        var shapevalue = "";
                        foreach (DataRow Tempdata in tempdata.Rows)
                        {
                            shapevalue = Tempdata["KEY_VALUE"].ToString();
                        }

                        parameters.Add("SHAPE" + i + y, shapevalue);
                        sql3.Clear();
                        sql3.AppendLine("insert into M_CAS_REGULATION");
                        sql3.AppendLine("(");
                        sql3.AppendLine("REG_TYPE_ID");
                        sql3.AppendLine(", ROW_ID");
                        sql3.AppendLine(", COLMUN_ID");
                        sql3.AppendLine(", VAL_STRING");
                        sql3.AppendLine(",REG_USER_CD");
                        sql3.AppendLine(")");
                        sql3.AppendLine("values ");
                        sql3.AppendLine("(");
                        sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                        sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                        sql3.AppendLine(",:形状COLUMN");                            //COLMUN_ID
                        sql3.AppendLine(",:SHAPE" + i + y);                              //VAL_NUMBER
                        sql3.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                        sql3.AppendLine(")");

                        data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameters);
                        parameters.Remove("SHAPE" + i + y);

                        y++;
                    }
                }
                else
                {
                    int y = 0;
                    var shape = "";

                    parameters.Add("SHAPE_VALUE" + i + y, shape);

                    sql3.Clear();
                    sql3.AppendLine("select KEY_VALUE from M_COMMON");
                    sql3.AppendLine(" where DISPLAY_VALUE=:SHAPE_VALUE" + i + y);
                    sql3.AppendLine(" and TABLE_NAME = 'M_FIGURE'");

                    DataTable tempdata = DbUtil.Select(databaseContext, sql3.ToString(), parameters);
                    parameters.Remove("SHAPE_VALUE" + i + y);

                    var shapevalue = "";
                    foreach (DataRow Tempdata in tempdata.Rows)
                    {
                        shapevalue = Tempdata["KEY_VALUE"].ToString();
                    }

                    parameters.Add("SHAPE" + i + y, shapevalue);
                    sql3.Clear();
                    sql3.AppendLine("insert into M_CAS_REGULATION");
                    sql3.AppendLine("(");
                    sql3.AppendLine("REG_TYPE_ID");
                    sql3.AppendLine(", ROW_ID");
                    sql3.AppendLine(", COLMUN_ID");
                    sql3.AppendLine(", VAL_STRING");
                    sql3.AppendLine(",REG_USER_CD");
                    sql3.AppendLine(")");
                    sql3.AppendLine("values ");
                    sql3.AppendLine("(");
                    sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                    sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                    sql3.AppendLine(",:形状COLUMN");                            //COLMUN_ID
                    sql3.AppendLine(",:SHAPE" + i + y);                              //VAL_NUMBER
                    sql3.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                    sql3.AppendLine(")");

                    data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameters);
                    parameters.Remove("SHAPE" + i + y);

                    y++;

                }

                if (cas[i].terms != null)
                {
                    //使用条件
                    string[] arr2 = cas[i].terms.Split(';');
                    int z = 0;

                    foreach (string terms in arr2)
                    {
                        parameters.Add("TERMS_VALUE" + i + z, terms);

                        sql3.Clear();
                        sql3.AppendLine("select KEY_VALUE from M_COMMON");
                        sql3.AppendLine(" where DISPLAY_VALUE=:TERMS_VALUE" + i + z);
                        sql3.AppendLine(" and TABLE_NAME = 'M_ISHA_USAGE'");

                        DataTable tempdata = DbUtil.Select(databaseContext, sql3.ToString(), parameters);
                        parameters.Remove("TERMS_VALUE" + i + z);

                        var termsvalue = "";
                        foreach (DataRow Tempdata in tempdata.Rows)
                        {
                            termsvalue = Tempdata["KEY_VALUE"].ToString();
                        }

                        parameters.Add("TERMS" + i + z, termsvalue);
                        sql3.Clear();
                        sql3.AppendLine("insert into M_CAS_REGULATION");
                        sql3.AppendLine("(");
                        sql3.AppendLine("REG_TYPE_ID");
                        sql3.AppendLine(", ROW_ID");
                        sql3.AppendLine(", COLMUN_ID");
                        sql3.AppendLine(", VAL_STRING");
                        sql3.AppendLine(",REG_USER_CD");
                        sql3.AppendLine(")");
                        sql3.AppendLine("values ");
                        sql3.AppendLine("(");
                        sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                        sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                        sql3.AppendLine(",:使用条件COLUMN");                        //COLMUN_ID
                        sql3.AppendLine(",:TERMS" + i + z);                               //VAL_NUMBER
                        sql3.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                        sql3.AppendLine(")");

                        data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameters);
                        parameters.Remove("TERMS" + i + z);

                        z++;
                    }
                }
                else
                {
                    int z = 0;

                    var terms = "";
                    parameters.Add("TERMS_VALUE" + i + z, terms);

                    sql3.Clear();
                    sql3.AppendLine("select KEY_VALUE from M_COMMON");
                    sql3.AppendLine(" where DISPLAY_VALUE=:TERMS_VALUE" + i + z);
                    sql3.AppendLine(" and TABLE_NAME = 'M_ISHA_USAGE'");

                    DataTable tempdata = DbUtil.Select(databaseContext, sql3.ToString(), parameters);
                    parameters.Remove("TERMS_VALUE" + i + z);

                    var termsvalue = "";
                    foreach (DataRow Tempdata in tempdata.Rows)
                    {
                        termsvalue = Tempdata["KEY_VALUE"].ToString();
                    }

                    parameters.Add("TERMS" + i + z, termsvalue);
                    sql3.Clear();
                    sql3.AppendLine("insert into M_CAS_REGULATION");
                    sql3.AppendLine("(");
                    sql3.AppendLine("REG_TYPE_ID");
                    sql3.AppendLine(", ROW_ID");
                    sql3.AppendLine(", COLMUN_ID");
                    sql3.AppendLine(", VAL_STRING");
                    sql3.AppendLine(",REG_USER_CD");
                    sql3.AppendLine(")");
                    sql3.AppendLine("values ");
                    sql3.AppendLine("(");
                    sql3.AppendLine(":REG_TYPE_ID");                            //REG_TYOE_ID
                    sql3.AppendLine(",:ROW_ID" + i);                                //ROW_ID
                    sql3.AppendLine(",:使用条件COLUMN");                        //COLMUN_ID
                    sql3.AppendLine(",:TERMS" + i + z);                               //VAL_NUMBER
                    sql3.AppendLine(",:" + nameof(SystemColumn.REG_USER_CD));   //REG_USER_CD
                    sql3.AppendLine(")");

                    data3 = DbUtil.ExecuteUpdate(databaseContext, sql3.ToString(), parameters);
                    parameters.Remove("TERMS" + i + z);

                    z++;

                }
                parameters.Remove("ROW_ID" + i);

            }

            databaseContext.Commit();
            databaseContext.Close();
        }
        /// <summary>
        /// テーブル管理情報とテーブル項目管理情報からマスターを指定された条件で検索し、結果を返します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="DeleteFlag">削除フラグの状態を<see cref="SystemConst.DEL_FLAG"/>から選択します。</param>
        /// <returns>検索結果を<see cref="DataTable"/>で返します。</returns>
        public static List<M_REGULATION> SearchCommonMasterNew(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, DEL_FLAG DeleteFlag, int id)
        {
            return SearchCommonMasterNew(TableInformation, TableColumnInformations, null, (int)DeleteFlag, id);
        }


        /// <summary>
        /// テーブル管理情報とテーブル項目管理情報からマスターを指定された検索条件、またはキー項目で検索し、結果を返します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="KeyValues">キー項目を指定します。検索条件を使用した場合は<see cref="null"/>を指定します。</param>
        /// <param name="DeleteFlag">削除フラグの状態を<see cref="SystemConst.DEL_FLAG"/>から<see cref="int"/>で指定します。削除フラグを指定しない場合は<see cref="null"/>を指定します。</param>
        /// <returns>検索結果を<see cref="DataTable"/>で返します。</returns>
        private static List<M_REGULATION> SearchCommonMasterNew(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> KeyValues, int? DeleteFlag, int id)
        {
            var sql = new StringBuilder();
            var parameter = new DynamicParameters();
            var tableColumnControls = new List<S_TAB_COL_CONTROL>();
            M_MASTER master = new M_MASTER();
            sql.AppendLine("select ");
            //2019/02/18 TPE.Sugimoto Add Sta
            if (TableInformation.TABLE_NAME == "M_LOCATION")
            {
                sql.AppendLine("distinct ");
            }
            //2019/02/18 TPE.Sugimoto Add End
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                //2018/08/15 TPE Add Start
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_NAME") continue;
                //2018/08/15 TPE Add End

                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID") continue;    //2018/10/03 Rin Add

                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }

            //2019/05/09 TPE.Rin Add Start
            if (TableInformation.TABLE_NAME == "M_REGULATION")
            {
                sql.AppendLine(" M_REGULATION.RA_FLAG,");
                sql.AppendLine(" M_REGULATION.MARKS,");
            }
            //2019/05/09 TPE.Rin Add End

            //2019/02/18 TPE.Sugimoto Add Sta
            if (TableInformation.TABLE_NAME == "M_LOCATION")
            {
                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = @LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第一類') HAZARD_CHECK1, "); //2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = @LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第二類') HAZARD_CHECK2, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = @LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第三類') HAZARD_CHECK3, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = @LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第四類') HAZARD_CHECK4, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = @LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第五類') HAZARD_CHECK5, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = @LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第六類') HAZARD_CHECK6, ");

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = @LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '指定可燃物') HAZARD_CHECK7     ");
            }
            //2019/02/18 TPE.Sugimoto Add End
            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            //2019/02/18 TPE.Sugimoto Add Sta
            if (TableInformation.TABLE_NAME == "M_LOCATION")
            {
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
            }
            //2019/02/18 TPE.Sugimoto Add End
            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            if (DeleteFlag != null)
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + DeleteFlag.ToString());
                sql.AppendLine(" and  ");
            }
            if (master != null)
            {
                sql.AppendLine(" " + "M_REGULATION.REG_TYPE_ID" + " = " + id.ToString());
                sql.AppendLine(" and  ");
            }

            // キー項目の指定があればキー項目で検索します。
            // キー項目の指定がなければ検索条件で検索します。
            if (KeyValues != null)
            {
                foreach (KeyValuePair<string, string> keyValue in KeyValues)
                {
                    //2019/02/18 TPE.Sugimoto Add Sta
                    if (TableInformation.TABLE_NAME == "M_LOCATION")
                    {
                        sql.AppendLine(" M_LOCATION." + keyValue.Key + " = @" + keyValue.Key + " ");
                        sql.AppendLine(" and  ");
                        parameter.Add(keyValue.Key, keyValue.Value);
                    }
                    else
                    {
                        //2019/02/18 TPE.Sugimoto Add End
                        sql.AppendLine(" " + keyValue.Key + " = @" + keyValue.Key + " ");
                        sql.AppendLine(" and  ");
                        parameter.Add(keyValue.Key, keyValue.Value);
                    }//2019/02/18 TPE.Sugimoto Add
                }
            }
            else
            {
                foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
                {
                    //2018/08/15 TPE Add Start
                    if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                    //2018/08/15 TPE Add End

                    // 検索条件を指定します。
                    if (tableColumnInformation.IS_SEARCH && !string.IsNullOrEmpty(tableColumnInformation.InputValue?.Trim()))
                    {
                        // 文字、数値の場合は部分一致、それ以外は完全一致で検索します。
                        if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character ||
                            tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
                        {
                            sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like @" + tableColumnInformation.COLUMN_NAME + Classes.util.DbUtil.LikeEscapeInfoAdd());
                            sql.AppendLine(" and  ");
                            parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(tableColumnInformation.InputValue) + "%");

                        }
                        else
                        {
                            sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = @" + tableColumnInformation.COLUMN_NAME);
                            sql.AppendLine(" and  ");
                            parameter.Add(tableColumnInformation.COLUMN_NAME, tableColumnInformation.InputValue);
                        }
                    }
                }
            }

            sql.Length -= 8;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;

            List<M_REGULATION> regData = new List<M_REGULATION>();
            M_REGULATION m_REGULATION = new M_REGULATION();

            regData = DbUtil.Select<M_REGULATION>(sql.ToString(), parameter);
            //m_master.listRegulationDetails = regData;

            return regData;

        }

        /// <summary>
        /// テーブル項目管理情報から検索条件コントロールを設定します。
        /// </summary>
        /// <param name="SearchPanel">検索条件設定を行うパネルを指定します。</param>
        /// <param name="TableColumnInformations">検索条件設定を行うテーブル項目管理情報を指定します。</param>
        /// <param name="IsValueOutput">入力値を出力する場合は<see cref="true"/>を、出力しない場合は<see cref="false"/>を指定します。</param>
        public static void SetSearchCriteria(Panel SearchPanel, List<S_TAB_COL_CONTROL> TableColumnInformations, bool IsValueOutput = false)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_SEARCH)
                {
                    SetControl(SearchPanel, tableColumnInformation, IsSearch: true, IsValueOutput: IsValueOutput);
                }
            }
        }

        /// <summary>
        /// テーブル項目管理情報からデータ入出力用コントロールを設定します。
        /// </summary>
        /// <param name="IO_Panel">入出力設定を行うパネルを指定します。</param>
        /// <param name="TableColumnInformations">検索条件設定を行うテーブル項目管理情報を指定します。</param>
        /// <param name="MaintenanceType">メンテナンスの区分を<see cref="MaintenanceType"/>から指定します。</param>
        public static void SetIO_Control(Panel IO_Panel, List<S_TAB_COL_CONTROL> TableColumnInformations, MaintenanceType MaintenanceType)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                //2018/08/15 TPE Add Start
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_NAME") continue;
                //2018/08/15 TPE Add End
                if (tableColumnInformation.TABLE_NAME == "M_GROUP" && tableColumnInformation.COLUMN_NAME == "P_GROUP_CD") continue;
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID") continue;    //2018/10/03 Rin Add

                if (IsSystemColumn(tableColumnInformation)) continue;
                if (IsHierarchyColumn(tableColumnInformation)) continue;
                switch (MaintenanceType)
                {
                    case MaintenanceType.New:
                        // キー項目の場合は読み取り専用にします。
                        SetControl(IO_Panel, tableColumnInformation, false, tableColumnInformation.IS_IDENTITY);
                        break;
                    case MaintenanceType.Edit:
                        // キー項目の場合は読み取り専用にします。
                        SetControl(IO_Panel, tableColumnInformation, false, tableColumnInformation.IS_KEY || tableColumnInformation.IS_IDENTITY);
                        break;
                    case MaintenanceType.Delete:
                        SetControl(IO_Panel, tableColumnInformation, false, true);
                        break;
                }
            }
        }

        /// <summary>
        /// テーブル項目管理情報からデータ入出力用コントロールを設定します。(保管場所マスター用)
        /// 
        /// </summary>
        /// <param name="IO_Panel">入出力設定を行うパネルを指定します。</param>
        /// <param name="TableColumnInformations">検索条件設定を行うテーブル項目管理情報を指定します。</param>
        /// <param name="MaintenanceType">メンテナンスの区分を<see cref="MaintenanceType"/>から指定します。</param>
        public static void SetIO_Control_Location(Panel IO_Panel, Panel IO_PanelGD, List<S_TAB_COL_CONTROL> TableColumnInformations, MaintenanceType MaintenanceType)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (IsSystemColumn(tableColumnInformation)) continue;
                if (IsHierarchyColumn(tableColumnInformation)) continue;
                switch (MaintenanceType)
                {
                    case MaintenanceType.New:
                        // キー項目の場合は読み取り専用にします。
                        SetControl(IO_Panel, tableColumnInformation, false, tableColumnInformation.IS_IDENTITY);
                        break;
                    case MaintenanceType.Edit:
                        // キー項目の場合は読み取り専用にします。
                        SetControl(IO_Panel, tableColumnInformation, false, tableColumnInformation.IS_KEY || tableColumnInformation.IS_IDENTITY);
                        break;
                    case MaintenanceType.Delete:
                        SetControl(IO_Panel, tableColumnInformation, false, true);
                        break;
                }
            }
            switch (MaintenanceType)
            {
                case MaintenanceType.New:

                    IO_PanelGD.Controls.Add(new LiteralControl("<table border=\"1\">" +
                    "<tr><td><b>Category ID</b></td>" +
                    "<td><b>Category Name</b></td></tr>"));

                    break;
                case MaintenanceType.Edit:
                    IO_PanelGD.Controls.Add(new LiteralControl("<table border=\"1\">" +
                    "<tr><td><b>Category ID</b></td>" +
                    "<td><b>Category Name</b></td></tr>"));
                    break;
            }
        }




        /// <summary>
        /// テーブル項目管理情報からデータ出力用コントロールを設定します。
        /// </summary>
        /// <param name="OutputPanel">出力設定を行うパネルを指定します。</param>
        /// <param name="TableColumnInformations">検索条件設定を行うテーブル項目管理情報を指定します。</param>
        public static void SetOutputControl(Panel OutputPanel, List<S_TAB_COL_CONTROL> TableColumnInformations)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                //2018/08/15 TPE Add Start
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_NAME") continue;
                //2018/08/15 TPE Add End
                if (tableColumnInformation.TABLE_NAME == "M_GROUP" && tableColumnInformation.COLUMN_NAME == "P_GROUP_CD") continue;
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID") continue;    //2018/10/03 Rin Add

                if (IsSystemColumn(tableColumnInformation)) continue;
                SetControl(OutputPanel, tableColumnInformation, true);
            }
        }

        /// <summary>
        /// 汎用の入力コントロールを設定します。
        /// </summary>
        /// <param name="TargetPanel">検索条件設定を行うパネルを指定します。</param>
        /// <param name="TableColumnInformations">検索条件設定を行うテーブル項目管理情報のレコードを指定します。</param>
        /// <param name="IsReadOnly">読み取り専用の場合は<see cref="true"/>を、入力可能な場合は<see cref="false"/>を指定します。</param>
        /// <param name="IsOutput">値をラベル出力する場合は<see cref="true"/>を、そうでない場合は<see cref="false"/>を指定します。</param>
        /// <param name="IsSearch">検索条件を設定する場合は<see cref="true"/>を、そうでない場合は<see cref="false"/>を指定します。</param>
        /// <param name="IsValueOutput">入力値を出力する場合は<see cref="true"/>を、出力しない場合は<see cref="false"/>を指定します。</param>
        private static void SetControl(Panel TargetPanel, S_TAB_COL_CONTROL TableColumnInformation, bool IsOutput = false, bool IsReadOnly = false, bool IsSearch = false, bool IsValueOutput = false)
        {
            var addLabel = new Label();
            var halfWidth = string.Empty;
            if (TableColumnInformation.IS_HALFWIDTH || TableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number ||
                TableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Date)
            {
                halfWidth = "(半角)";
            }
            addLabel.ID = "lbl" + TableColumnInformation.COLUMN_DISPLAY_NAME;
            addLabel.Text = TableColumnInformation.COLUMN_DISPLAY_NAME + halfWidth + "：";
            addLabel.CssClass = "CommonLabel";
            TargetPanel.Controls.Add(addLabel);

            if (IsOutput)
            {
                // 出力専用のコントロールを組み立てます。
                switch (TableColumnInformation.DATA_TYPE)
                {
                    case S_TAB_COL_CONTROL.DataType.Character:
                    case S_TAB_COL_CONTROL.DataType.Number:
                    case S_TAB_COL_CONTROL.DataType.Date:
                        var addOutputTextBox = new TextBox();
                        addOutputTextBox.ID = TableColumnInformation.COLUMN_NAME;
                        addOutputTextBox.CssClass = "CommonOutput";
                        addOutputTextBox.ReadOnly = true;
                        TargetPanel.Controls.Add(addOutputTextBox);
                        TargetPanel.Controls.Add(new LiteralControl("<br />"));
                        break;
                    case S_TAB_COL_CONTROL.DataType.Check:
                        var addOutputCheckBox = new CheckBox();
                        addOutputCheckBox.ID = TableColumnInformation.COLUMN_NAME;
                        addOutputCheckBox.CssClass = "CommonOutput";
                        addOutputCheckBox.Enabled = false;
                        TargetPanel.Controls.Add(addOutputCheckBox);
                        TargetPanel.Controls.Add(new LiteralControl("<br />"));
                        break;
                    case S_TAB_COL_CONTROL.DataType.Select:
                        var addDropDownList = new DropDownList();
                        addDropDownList.ID = TableColumnInformation.COLUMN_NAME;
                        addDropDownList.CssClass = "CommonOutput";

                        // 変換データを設定します。
                        addDropDownList.Items.Add(new ListItem(" ", " "));
                        foreach (KeyValuePair<string, string> convertData in TableColumnInformation.SelectData)
                        {
                            addDropDownList.Items.Add(new ListItem(convertData.Value, convertData.Key));
                        }

                        TargetPanel.Controls.Add(addDropDownList);
                        TargetPanel.Controls.Add(new LiteralControl("<br />"));
                        break;
                    case S_TAB_COL_CONTROL.DataType.NIDInput:
                        var nid = new TextBox()
                        {
                            ID = TableColumnInformation.COLUMN_NAME,
                            CssClass = "textBoxShort",
                            ReadOnly = true,
                            AutoPostBack = true,
                        };
                        if (!IsSearch && TableColumnInformation.IS_REQUIRED && !nid.ReadOnly)
                        {
                            nid.CssClass += SystemConst.RequiredStyle;
                        }
                        var name = new Label()
                        {
                            ID = "lab" + TableColumnInformation.COLUMN_NAME + "_NAME",
                        };
                        TargetPanel.Controls.Add(nid);
                        TargetPanel.Controls.Add(name);
                        TargetPanel.Controls.Add(new LiteralControl("<br />"));
                        break;
                }
            }
            else
            {
                // 入出力用のコントロールを組み立てます。
                switch (TableColumnInformation.DATA_TYPE)
                {
                    case S_TAB_COL_CONTROL.DataType.Character:
                    case S_TAB_COL_CONTROL.DataType.Number:
                    case S_TAB_COL_CONTROL.DataType.Date:

                        //2019/03/02 TPE.Rin Add Start
                        if (TargetPanel.ClientID == "pnlSearchCriteria" && TableColumnInformation.TABLE_NAME == "M_REGULATION" && TableColumnInformation.COLUMN_NAME == "REG_TEXT")
                        {
                            var addDropDownListCheck = new DropDownList();
                            addDropDownListCheck.ID = TableColumnInformation.COLUMN_NAME + "_CONDITION";
                            addDropDownListCheck.CssClass = "searchCondition";
                            if (IsReadOnly) addDropDownListCheck.Enabled = false;

                            addDropDownListCheck.Items.Add(new ListItem("部分一致", "0"));
                            addDropDownListCheck.Items.Add(new ListItem("前方一致", "1"));
                            addDropDownListCheck.Items.Add(new ListItem("後方一致", "2"));
                            addDropDownListCheck.Items.Add(new ListItem("完全一致", "3"));

                            TargetPanel.Controls.Add(addDropDownListCheck);
                            TargetPanel.Controls.Add(new LiteralControl("&nbsp;"));
                        }
                        //2019/03/02 TPE.Rin Add End

                        var addTextBox = new TextBox();
                        addTextBox.ID = TableColumnInformation.COLUMN_NAME;
                        //2018/08/07 TPE Add
                        if (TableColumnInformation.COLUMN_DISPLAY_NAME == "化学物質取扱責任者(NID)")
                        {
                            addTextBox.CssClass = "TextBoxSearchShort";
                            addTextBox.AutoPostBack = true;
                        }
                        //2019/03/02 TPE.Rin Add Start
                        else if (TargetPanel.ClientID == "pnlSearchCriteria" && TableColumnInformation.TABLE_NAME == "M_REGULATION" && TableColumnInformation.COLUMN_NAME == "REG_TEXT")
                        {
                            addTextBox.CssClass = "TextBoxSearchShort";
                        }
                        //2019/03/02 TPE.Rin Add End
                        else
                        {
                            addTextBox.CssClass = "CommonInput";
                        }
                        if (!IsSearch && TableColumnInformation.IS_REQUIRED) addTextBox.CssClass += SystemConst.RequiredStyle;
                        if (IsReadOnly) addTextBox.ReadOnly = true;
                        if (TableColumnInformation.TABLE_NAME == "M_WORKFLOW" && TableColumnInformation.COLUMN_NAME == "FLOW_CD" && !IsSearch)
                        {
                            addTextBox.BackColor = ColorTranslator.FromHtml("#ffffe0");
                            addTextBox.ReadOnly = true;
                        }
                        if (IsValueOutput) addTextBox.Text = TableColumnInformation.InputValue;
                        TargetPanel.Controls.Add(addTextBox);
                        if (TableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number ||
                            TableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Date)
                        {
                            addTextBox.Style.Add("ime-mode", "disabled");
                            addTextBox.MaxLength = TableColumnInformation.DATA_LENGTH + TableColumnInformation.DEC_DATA_LENGTH + 1; //2019/02/18 TPE.Sugimoto Add
                        }
                        else
                        {
                            if (TableColumnInformation.IS_HALFWIDTH) addTextBox.Style.Add("ime-mode", "disabled");
                        }
                        //2018/08/07 TPE Add
                        if (TableColumnInformation.COLUMN_DISPLAY_NAME == "化学物質取扱責任者(NID)")
                        {
                            TargetPanel.Controls.Add(new LiteralControl("&nbsp;"));
                        }
                        AddSelectButton(TargetPanel, TableColumnInformation.TABLE_NAME, TableColumnInformation.COLUMN_NAME, addTextBox);
                        //2018/08/07 TPE Add
                        if (TableColumnInformation.COLUMN_DISPLAY_NAME == "化学物質取扱責任者(NID)")
                        {
                            var addLabel2 = new Label();
                            addLabel2.ID = "lblChemChief";
                            TargetPanel.Controls.Add(addLabel2);
                        }

                        TargetPanel.Controls.Add(new LiteralControl("<br />"));
                        break;
                    case S_TAB_COL_CONTROL.DataType.Check:
                        if (IsSearch)
                        {
                            var addDropDownListCheck = new DropDownList();
                            addDropDownListCheck.ID = TableColumnInformation.COLUMN_NAME;
                            addDropDownListCheck.CssClass = "CommonCheckInput";
                            if (IsReadOnly) addDropDownListCheck.Enabled = false;

                            //2018/08/13 TPE Delete Start
                            // チェック用データを設定します。
                            //addDropDownListCheck.Items.Add(new ListItem("指定しない", " "));
                            //addDropDownListCheck.Items.Add(new ListItem("チェック無し", Convert.ToString((int)Check.NotCheck)));
                            //addDropDownListCheck.Items.Add(new ListItem("チェック有り", Convert.ToString((int)Check.Checked)));
                            //2018/08/13 TPE Delete End

                            //2018/08/13 TPE.Sugimoto Add Sta
                            if (TableColumnInformation.COLUMN_NAME == "SEL_FLAG")
                            {
                                addDropDownListCheck.Items.Add(new ListItem("全て", " "));
                                addDropDownListCheck.Items.Add(new ListItem("選択可", Convert.ToString((int)Check.Checked)));
                                addDropDownListCheck.Items.Add(new ListItem("選択不可", Convert.ToString((int)Check.NotCheck)));
                            }
                            else
                            {
                                // チェック用データを設定します。
                                addDropDownListCheck.Items.Add(new ListItem("指定しない", " "));
                                addDropDownListCheck.Items.Add(new ListItem("チェック無し", Convert.ToString((int)Check.NotCheck)));
                                addDropDownListCheck.Items.Add(new ListItem("チェック有り", Convert.ToString((int)Check.Checked)));
                            }
                            //2018/08/13 TPE.Sugimoto Add End

                            //2019/03/01 TPE.Rin Delete
                            //2019/02/05 TPE.Rin Add Start
                            //switch (TableColumnInformation.COLUMN_NAME)
                            //{
                            //    case "SEL_FLAG":
                            //        addDropDownListCheck.Items.Add(new ListItem("全て", " "));
                            //        addDropDownListCheck.Items.Add(new ListItem("選択可", Convert.ToString((int)Check.Checked)));
                            //        addDropDownListCheck.Items.Add(new ListItem("選択不可", Convert.ToString((int)Check.NotCheck)));
                            //        break;
                            //    case "DEL_FLAG":
                            //        addDropDownListCheck.Items.Add(new ListItem("全て", " "));
                            //        addDropDownListCheck.Items.Add(new ListItem("使用中", Convert.ToString((int)Check.Checked)));
                            //        addDropDownListCheck.Items.Add(new ListItem("削除済", Convert.ToString((int)Check.NotCheck)));
                            //        break;
                            //    default:
                            //        // チェック用データを設定します。
                            //        addDropDownListCheck.Items.Add(new ListItem("指定しない", " "));
                            //        addDropDownListCheck.Items.Add(new ListItem("チェック無し", Convert.ToString((int)Check.NotCheck)));
                            //        addDropDownListCheck.Items.Add(new ListItem("チェック有り", Convert.ToString((int)Check.Checked)));
                            //        break;
                            //}
                            //2019/02/05 TPE.Rin Add End

                            // 入力値を設定します。
                            if (IsValueOutput)
                            {
                                if (string.IsNullOrEmpty(TableColumnInformation.InputValue))
                                {
                                    addDropDownListCheck.SelectedIndex = 0;
                                }
                                else
                                {
                                    addDropDownListCheck.SelectedValue = TableColumnInformation.InputValue;
                                }
                            }

                            TargetPanel.Controls.Add(addDropDownListCheck);
                            TargetPanel.Controls.Add(new LiteralControl("<br />"));
                        }
                        else
                        {
                            var addCheckBox = new CheckBox();
                            addCheckBox.ID = TableColumnInformation.COLUMN_NAME;
                            addCheckBox.CssClass = "CommonInput";
                            if (IsReadOnly) addCheckBox.Enabled = false;

                            // 入力値を設定します。
                            if (IsValueOutput)
                            {
                                switch ((Check)Convert.ToInt32(TableColumnInformation.InputValue))
                                {
                                    case (Check.NotCheck):
                                        addCheckBox.Checked = false;
                                        break;
                                    case (Check.Checked):
                                        addCheckBox.Checked = true;
                                        break;
                                }
                            }

                            TargetPanel.Controls.Add(addCheckBox);

                            TargetPanel.Controls.Add(new LiteralControl("<br />"));
                        }
                        break;
                    case S_TAB_COL_CONTROL.DataType.Select:
                        var addDropDownList = new DropDownList();
                        addDropDownList.ID = TableColumnInformation.COLUMN_NAME;
                        addDropDownList.CssClass = "CommonInput";
                        if (!IsSearch && TableColumnInformation.IS_REQUIRED) addDropDownList.CssClass += SystemConst.RequiredStyle;
                        if (IsReadOnly) addDropDownList.Enabled = false;

                        // 変換データを設定します。

                        //2018/08/19 TPE Delete Start
                        //addDropDownList.Items.Add(new ListItem(" ", " "));
                        //2018/08/19 TPE Delete End

                        //2018/08/19 TPE.Sugimoto Add Sta
                        if (TableColumnInformation.COLUMN_NAME != "SEL_FLAG" && TableColumnInformation.COLUMN_NAME != "OVER_CHECK_FLAG")//2019/02/18 TPE.Sugimoto Upd
                        {
                            addDropDownList.Items.Add(new ListItem(" ", " "));
                        }
                        else
                        {
                            if (IsSearch == true)
                            {
                                addDropDownList.Items.Add(new ListItem("全て", " "));
                            }
                        }
                        //2018/08/19 TPE.Sugimoto Add End
                        foreach (KeyValuePair<string, string> convertData in TableColumnInformation.SelectData)
                        {
                            addDropDownList.Items.Add(new ListItem(convertData.Value, convertData.Key));
                        }

                        // 入力値を設定します。
                        if (IsValueOutput)
                        {
                            if (string.IsNullOrEmpty(TableColumnInformation.InputValue))
                            {
                                addDropDownList.SelectedIndex = 0;
                            }
                            else
                            {
                                addDropDownList.SelectedValue = TableColumnInformation.InputValue;
                            }
                        }

                        TargetPanel.Controls.Add(addDropDownList);
                        AddSelectButton(TargetPanel, TableColumnInformation.TABLE_NAME, TableColumnInformation.COLUMN_NAME, addDropDownList);
                        TargetPanel.Controls.Add(new LiteralControl("<br />"));
                        break;
                    case S_TAB_COL_CONTROL.DataType.NIDInput:
                        var nid = new TextBox()
                        {
                            ID = TableColumnInformation.COLUMN_NAME,
                            CssClass = "textBoxShort",
                            ReadOnly = IsReadOnly,
                            AutoPostBack = true,
                        };
                        if (!IsSearch && TableColumnInformation.IS_REQUIRED)
                        {
                            nid.CssClass += SystemConst.RequiredStyle;
                        }
                        var name = new Label()
                        {
                            ID = "lab" + TableColumnInformation.COLUMN_NAME + "_NAME",
                        };
                        TargetPanel.Controls.Add(nid);
                        TargetPanel.Controls.Add(name);
                        TargetPanel.Controls.Add(new LiteralControl("<br />"));
                        break;
                }

            }

        }

        /// <summary>
        /// 選択ボタンの追加対象のテーブル/項目の場合、選択ボタンを追加します。
        /// </summary>
        /// <param name="TargetPanel">追加するパネルを指定します。</param>
        /// <param name="TableName">判定するテーブル名を指定します。</param>
        /// <param name="ColumnName">判定する項目名を指定します。</param>
        /// <param name="valueControl">値を設定するコントロールを指定します。</param>
        private static void AddSelectButton(Panel TargetPanel, string TableName, string ColumnName, Control valueControl)
        {
            if (IsSelectButtonTarget(TableName, ColumnName))
            {
                TargetPanel.Controls.Add(CreateSelectButton(TableName, ColumnName, valueControl));
            }
        }

        /// <summary>
        /// 選択ボタンを作成します。
        /// </summary>
        /// <param name="TableName">テーブル名を指定します。</param>
        /// <param name="ColumnName">項目名を指定します。</param>
        /// <param name="valueControl">値を設定するコントロールを指定します。</param>
        /// <returns>選択ボタンを返します。</returns>
        private static Button CreateSelectButton(string TableName, string ColumnName, Control valueControl)
        {
            var selectButton = new Button();
            selectButton.ID = SystemConst.SelectButtonPrefix + ColumnName;
            selectButton.Text = "選択";
            /*
            selectButton.Attributes.Add("onclick", Common.CreateJavaScript("ShowModal", new object[] { "../ZC010/ZC01081.aspx" + 
                                                                                                            "?" + QueryString.TableNameKey + "=" + TableName +
                                                                                                            "&" + QueryString.ColumnNameKey + "=" + ColumnName + 
                                                                                                            "&" + QueryString.SelectTypeKey + "=" + Convert.ToString((int)SelectType.One +
                                                                                                            "&" + QueryString.IdKey + "=\" + document.getElementById(\"" + valueControl.ClientID + "\").options[document.getElementById(\"" + valueControl.ClientID + "\").selectedIndex].value\""),
                                                                                                            Constant.cMODAL_DISP_HEIGHT, Constant.cMODAL_DISP_WIDTH }));
            */
            selectButton.OnClientClick = "showModalDialog('../ZC010/ZC01081.aspx" +
                                                          "?" + QueryString.TableNameKey + "=" + TableName +
                                                          "&" + QueryString.ColumnNameKey + "=" + ColumnName +
                                                          "&" + QueryString.SelectTypeKey + "=" + Convert.ToString((int)SelectType.One) +
                                                          "&" + QueryString.IdKey + "=' + document.getElementById(\"" + valueControl.ClientID + "\").options[document.getElementById(\"" + valueControl.ClientID + "\").selectedIndex].value," +
                                                          "''," +
                                                          "'dialogHeight: " + SystemConst.DialogHeight + "px; dialogWidth: " + SystemConst.DialogWidth + "px')";
            selectButton.CssClass = "CommonButtonShort";
            return selectButton;
        }

        /// <summary>
        /// 入力必須項目ラベルを作成します。
        /// </summary>
        /// <returns>入力必須項目ラベルを返します。</returns>
        private static Label CreateRequiredLabel()
        {
            var requiredLabel = new Label();
            requiredLabel.Text = "※入力必須";
            requiredLabel.CssClass = "CommonRequired";
            return requiredLabel;
        }

        /// <summary>
        /// 画面の検索項目からデータを取得します。
        /// </summary>
        /// <param name="TargetPanel">データを取得するパネルを指定します。</param>
        /// <param name="TableColumnInformations">データを取得するテーブル項目管理情報を指定します。</param>
        public static void GetSearchData(Panel TargetPanel, List<S_TAB_COL_CONTROL> TableColumnInformations)
        {
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (IsSystemColumn(tableColumnInformation)) continue;
                tableColumnInformation.InputValue = GetControlData(TargetPanel, tableColumnInformation.COLUMN_NAME);
            }
        }


        /// <summary>
        /// 画面の入力項目からデータを取得します。
        /// </summary>
        /// <param name="TargetPanel">データを取得するパネルを指定します。</param>
        /// <param name="TableColumnInformations">データを取得するテーブル項目管理情報を指定します。</param>
        /// <returns><see cref="Dictionary{TKey, TValue}"/>で値を返します。TKeyには項目名、TValueには値が入ります。</returns>
        public static Dictionary<string, string> GetInputData(Panel TargetPanel, List<S_TAB_COL_CONTROL> TableColumnInformations)
        {
            var Data = new Dictionary<string, string>();

            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (IsSystemColumn(tableColumnInformation)) continue;
                Data.Add(tableColumnInformation.COLUMN_NAME, GetControlData(TargetPanel, tableColumnInformation.COLUMN_NAME));
            }

            if (TableColumnInformations[0].TABLE_NAME.Equals("M_WORKFLOW"))
            {
                Data["FLOW_HIERARCHY"] = GetFlowHierarchy(Data);
            }

            return Data;
        }

        /// <summary>
        /// ワークフローマスターのフロー階層を追加します。
        /// </summary>
        /// <param name="Data"></param>
        /// <returns></returns>
        public static string GetFlowHierarchy(Dictionary<string, string> Data)
        {
            int hierarchy = 1;
            foreach (KeyValuePair<string, string> d in Data)
            {
                if (d.Key.Equals("APPROVAL_USER_CD" + hierarchy.ToString()))
                {
                    if (d.Value != string.Empty)
                    {
                        hierarchy++;
                        if (d.Key.Equals("APPROVAL_USER_CD6"))
                        {
                            Data["FLOW_HIERARCHY"] = (hierarchy -= 1).ToString();
                            break;
                        }
                    }
                    else
                    {
                        Data["FLOW_HIERARCHY"] = (hierarchy -= 1).ToString();
                        break;
                    }
                }
            }

            return Data["FLOW_HIERARCHY"];
        }

        /// <summary>
        /// 画面の項目からデータを取得します。
        /// </summary>
        /// <param name="TargetPanel">データを取得するパネルを指定します。</param>
        /// <param name="ControlID">データを取得するコントーるIDを指定します。</param>
        /// <returns>コントロールに入力された値を返します。</returns>
        private static string GetControlData(Panel TargetPanel, string ControlID)
        {
            {
                // コントロールがテキストボックスの場合はテキストを格納します。
                var targetControl = TargetPanel.FindControl(ControlID) as TextBox;
                if (targetControl != null)
                {
                    return targetControl.Text?.Trim();
                }
            }
            {
                // コントロールがチェックボックスの場合はチェック状態を格納します。
                var targetControl = TargetPanel.FindControl(ControlID) as CheckBox;
                if (targetControl != null)
                {
                    if (targetControl.Checked)
                    {
                        return Convert.ToString((int)Check.Checked);
                    }
                    else
                    {
                        return Convert.ToString((int)Check.NotCheck);
                    }
                }
            }
            {
                // コントロールがドロップダウンリストの場合は選択値を格納します。
                var targetControl = TargetPanel.FindControl(ControlID) as DropDownList;
                if (targetControl != null)
                {
                    return targetControl.SelectedValue?.Trim();
                }
            }
            return null;
        }

        /// <summary>
        /// 画面の出力項目にマスターデータを表示します。
        /// </summary>
        /// <param name="ViewPanel">出力項目を表示する<see cref="Panel"/>を指定します。</param>
        /// <param name="MasterData">出力項目に表示するマスターデータを指定します。</param>
        /// <param name="IsResult">結果出力の場合は<see cref="true"/>を、そうでない場合は<see cref="false"/>を指定します。</param>
        public static void ViewData(Panel ViewPanel, Dictionary<string, string> MasterData, bool IsResult = false)
        {
            foreach (KeyValuePair<string, string> masterData in MasterData)
            {
                // ラベルを検索する。
                {
                    var targetControl = ViewPanel.FindControl(masterData.Key) as Label;
                    if (targetControl != null)
                    {
                        targetControl.Text = masterData.Value;
                        continue;
                    }
                }
                // テキストボックスを検索する。
                {
                    var targetControl = ViewPanel.FindControl(masterData.Key) as TextBox;
                    if (targetControl != null)
                    {
                        targetControl.Text = masterData.Value;
                        continue;
                    }
                }

                // チェックボックスを検索する。
                {
                    var targetControl = ViewPanel.FindControl(masterData.Key) as CheckBox;
                    if (targetControl != null)
                    {
                        switch ((Check)Convert.ToInt32(masterData.Value))
                        {
                            case (Check.NotCheck):
                                targetControl.Checked = false;
                                break;
                            case (Check.Checked):
                                targetControl.Checked = true;
                                break;
                        }
                        continue;
                    }
                }
                // ドロップダウンリストを検索する。
                {
                    var targetControl = ViewPanel.FindControl(masterData.Key) as DropDownList;
                    if (targetControl != null)
                    {
                        var searchListItem = targetControl.Items.FindByValue(masterData.Value);
                        if (searchListItem != null)
                        {
                            targetControl.SelectedValue = masterData.Value;
                        }
                        else
                        {
                            targetControl.SelectedIndex = 0;
                        }
                        // 結果表示用の場合は選択されているアイテムのみにする。
                        if (IsResult)
                        {
                            var selectedListItem = targetControl.SelectedItem;
                            targetControl.Items.Clear();
                            targetControl.Items.Add(selectedListItem);
                        }
                        continue;
                    }
                }
            }
        }

        /// <summary>
        /// マスター項目名がシステム用項目かどうかを判断します。
        /// </summary>
        /// <param name="TableColumnInformation">システム用項目か判断するテーブル管理情報のレコードを指定します。</param>
        /// <returns>システム用項目の場合は<see cref="true"/>、そうでない場合は<see cref="false"/>を返します。</returns>
        public static bool IsSystemColumn(S_TAB_COL_CONTROL TableColumnInformation)
        {
            if (TableColumnInformation.TABLE_NAME == "M_WORKFLOW" && TableColumnInformation.COLUMN_NAME == "FLOW_CD")
            {
                return IsSystemColumn(TableColumnInformation, false);
            }
            return IsSystemColumn(TableColumnInformation, true);
        }

        /// <summary>
        /// マスター項目名がシステム用項目かどうかを判断します。
        /// </summary>
        /// <param name="TableColumnInformation">システム用項目か判断するテーブル管理情報のレコードを指定します。</param>
        /// <param name="isKeyTarget">キー項目をシステム用項目に含めるか。</param>
        /// <returns>システム用項目の場合は<see cref="true"/>、そうでない場合は<see cref="false"/>を返します。</returns>
        public static bool IsSystemColumn(S_TAB_COL_CONTROL TableColumnInformation, bool isKeyTarget)
        {
            if (TableColumnInformation.IS_KEY && isKeyTarget) return true;
            switch (TableColumnInformation.COLUMN_NAME.ToUpper())
            {
                case (nameof(SystemColumn.REG_USER_CD)):
                case (nameof(SystemColumn.UPD_USER_CD)):
                case (nameof(SystemColumn.DEL_USER_CD)):
                case (nameof(SystemColumn.REG_DATE)):
                case (nameof(SystemColumn.UPD_DATE)):
                case (nameof(SystemColumn.DEL_DATE)):
                case (nameof(SystemColumn.DEL_FLAG)):
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// フロー階層項目かどうかを判断します。
        /// </summary>
        /// <param name="TableColumnInformation">フロー階層項目か判断するテーブル管理情報のレコードを指定します。</param>
        /// <returns>フロー階層項目の場合は<see cref="true"/>、そうでない場合は<see cref="false"/>を返します。</returns>
        public static bool IsHierarchyColumn(S_TAB_COL_CONTROL TableColumnInformation)
        {
            if (TableColumnInformation.COLUMN_NAME == "FLOW_HIERARCHY") return true;

            return false;
        }

        /// <summary>
        /// マスター項目名が削除用項目かどうかを判断します。
        /// </summary>
        /// <param name="TableColumnInformation">削除用項目か判断するテーブル管理情報のレコードを指定します。</param>
        /// <returns>削除項目の場合は<see cref="true"/>、そうでない場合は<see cref="false"/>を返します。</returns>
        public static bool IsDeleteColumn(S_TAB_COL_CONTROL TableColumnInformation)
        {
            switch (TableColumnInformation.COLUMN_NAME.ToUpper())
            {
                case (nameof(SystemColumn.DEL_USER_CD)):
                case (nameof(SystemColumn.DEL_DATE)):
                case (nameof(SystemColumn.DEL_FLAG)):
                    return true;
                default:
                    return false;
            }
        }

        public static IEnumerable<string> ValidateInputValue(IEnumerable<S_TAB_COL_CONTROL> tableColumnInfos, Dictionary<string, string> masterDatas)//2019/02/18 TPE.Sugimoto Upd
        {
            var datas = new Dictionary<string, string>();

            foreach (var tableColumnInfo in tableColumnInfos)
            {
                datas.Add(tableColumnInfo.COLUMN_NAME, tableColumnInfo.InputValue);
            }

            return ValidateInputValue(tableColumnInfos, datas, null);//2019/02/18 TPE.Sugimoto Upd
        }
        
        public static IEnumerable<string> ValidateInputValue(IEnumerable<S_TAB_COL_CONTROL> tableColumnInfos, Dictionary<string, string> datas, Dictionary<string, string> masterDatas)//2019/02/18 TPE.Sugimoto Upd
        {
            var errorMessages = new List<string>();
            bool isToothless = false;
            bool isToothlessAdded = false;

            foreach (var data in datas)
            {
                var value = data.Value != null ? data.Value : string.Empty;

                var tableColumnInfo = tableColumnInfos.Where(x => x.COLUMN_NAME == data.Key).Single();

                //2018/09/25 TPE Add
                if (tableColumnInfo.TABLE_NAME == "M_LOCATION" && (tableColumnInfo.COLUMN_NAME == "USER_CD" || tableColumnInfo.COLUMN_NAME == "USER_NAME"))
                {
                    string[] arr = value.Split(';');
                    foreach (string usr in arr)
                    {
                        if (tableColumnInfo.DATA_LENGTH > 0 && tableColumnInfo.DATA_LENGTH < usr.ToString().Length)
                        {
                            yield return string.Format(WarningMessages.MaximumLength, tableColumnInfo.COLUMN_DISPLAY_NAME, tableColumnInfo.DATA_LENGTH.ToString());
                        }

                        var matchCollection = System.Text.RegularExpressions.Regex.Matches(usr, @"[a-zA-Z0-9!-/:-@\[-`{-~]+$");
                        if (tableColumnInfo.IS_HALFWIDTH && !string.IsNullOrEmpty(usr) && (matchCollection.Count != 1 || !(usr.Length == matchCollection[0].Length)))
                        {
                            yield return string.Format(WarningMessages.NotHalfwidth, tableColumnInfo.COLUMN_DISPLAY_NAME);
                        }

                        // 半角英字のみかチェックします。
                        if (tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character && tableColumnInfo.SUB_1 && !string.IsNullOrEmpty(usr))
                        {
                            if (!usr.IsAlphabetOrNumber())
                            {
                                yield return string.Format(WarningMessages.NotAlphabetOrNumber, tableColumnInfo.COLUMN_DISPLAY_NAME);
                            }
                        }

                        if (!string.IsNullOrEmpty(usr) && tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character)
                        {
                            var p = usr.ExtractProhibited();
                            if (p.Count() > 0)
                            {
                                yield return string.Format(WarningMessages.ContainProhibitionCharacter, tableColumnInfo.COLUMN_DISPLAY_NAME, string.Join(" ", p).Replace(@"\", @"\\"));
                            }
                        }

                    }
                }
                else
                {
                    if (tableColumnInfo.IS_REQUIRED && string.IsNullOrWhiteSpace(value))
                    {
                        yield return string.Format(WarningMessages.Required, tableColumnInfo.COLUMN_DISPLAY_NAME);
                    }

                    if (tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character || tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Date)
                    {
                        if (tableColumnInfo.DATA_LENGTH > 0 && tableColumnInfo.DATA_LENGTH < value.ToString().Length)
                        {
                            yield return string.Format(WarningMessages.MaximumLength, tableColumnInfo.COLUMN_DISPLAY_NAME, tableColumnInfo.DATA_LENGTH.ToString());
                        }
                    }

                    var matchCollection = System.Text.RegularExpressions.Regex.Matches(value, @"[a-zA-Z0-9!-/:-@\[-`{-~]+$");
                    if (tableColumnInfo.TABLE_NAME == "M_USER" && tableColumnInfo.COLUMN_NAME == "TEL")
                    {
                        matchCollection = System.Text.RegularExpressions.Regex.Matches(value, @"[a-zA-Z0-9 !-/:-@\[-`{-~]+$");
                    }

                    if (tableColumnInfo.IS_HALFWIDTH && !string.IsNullOrEmpty(value) && (matchCollection.Count != 1 || !(value.Length == matchCollection[0].Length)))
                    {
                        yield return string.Format(WarningMessages.NotHalfwidth, tableColumnInfo.COLUMN_DISPLAY_NAME);
                    }

                    // 半角英字のみかチェックします。
                    if (tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character && tableColumnInfo.SUB_1 && !string.IsNullOrEmpty(value))
                    {
                        if (!value.IsAlphabetOrNumber())
                        {
                            yield return string.Format(WarningMessages.NotAlphabetOrNumber, tableColumnInfo.COLUMN_DISPLAY_NAME);
                        }
                    }

                    var isValidNumber = true;
                    if (!string.IsNullOrWhiteSpace(value) && tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
                    {
                        var dec1 = default(decimal);
                        var dec2 = default(decimal);
                        var str1 = string.Empty;
                        var str2 = string.Empty;
                        if (value.Length > 28)
                        {
                            str1 = value.Substring(0, 28);
                            str2 = value.Substring(28);
                        }
                        else
                        {
                            str1 = value;
                            str2 = "0";

                        }
                        if (!decimal.TryParse(str1, out dec1) || !decimal.TryParse(str2, out dec2))
                        {
                            isValidNumber = false;
                            yield return string.Format(WarningMessages.NotNumber, tableColumnInfo.COLUMN_DISPLAY_NAME);
                        }

                        //2019/7/23 TPE Add 不正データ登録チェックを追加
                        if (value.IndexOf(',') >= 0 && isValidNumber == true)
                        {
                            isValidNumber = false;
                            yield return string.Format(WarningMessages.NotNumber, tableColumnInfo.COLUMN_DISPLAY_NAME);
                        }

                    }

                    if (tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
                    {
                        if (isValidNumber)
                        {
                            decimal dec = default(decimal);
                            if (decimal.TryParse(value, out dec))
                            {
                                var numberArray = value.Replace("-", string.Empty).Replace(",", string.Empty).Split(".".ToCharArray());
                                if (tableColumnInfo.DATA_LENGTH > 0 && numberArray[0].Length > tableColumnInfo.DATA_LENGTH)
                                {
                                    yield return string.Format(WarningMessages.PositiveNumberLength, tableColumnInfo.COLUMN_DISPLAY_NAME, tableColumnInfo.DATA_LENGTH.ToString());
                                }
                                if (tableColumnInfo.DEC_DATA_LENGTH == 0 && numberArray.Length == 2)
                                {
                                    yield return string.Format(WarningMessages.PositiveNumber, tableColumnInfo.COLUMN_DISPLAY_NAME);
                                }
                                else if (numberArray.Length == 2 && numberArray[1].Length > tableColumnInfo.DEC_DATA_LENGTH)
                                {
                                    yield return string.Format(WarningMessages.DecimalNumberLength, tableColumnInfo.COLUMN_DISPLAY_NAME, tableColumnInfo.DEC_DATA_LENGTH.ToString());
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(value) && tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character)
                    {
                        var p = value.ExtractProhibited();
                        if (p.Count() > 0)
                        {
                            yield return string.Format(WarningMessages.ContainProhibitionCharacter, tableColumnInfo.COLUMN_DISPLAY_NAME, string.Join(" ", p).Replace(@"\", @"\\"));
                        }
                    }

                    // メールアドレスの形式であるかチェックします。
                    if (tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character && tableColumnInfo.SUB_2 && !string.IsNullOrEmpty(value))
                    {
                        if (!value.IsMailAddressFormat())
                        {
                            yield return string.Format(WarningMessages.NotMailAddressFormat, tableColumnInfo.COLUMN_DISPLAY_NAME);
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(value) && tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Date)
                    {
                        DateTime date = default(DateTime);
                        if (!DateTime.TryParse(value, out date))
                        {
                            yield return string.Format(WarningMessages.NotDate, tableColumnInfo.COLUMN_DISPLAY_NAME);
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(value) && tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.NIDInput)
                    {
                        var userDic = new UserMasterInfo().GetDictionary();

                        if (!userDic.ContainsKey(value))
                        {
                            yield return string.Format(WarningMessages.NotFound, tableColumnInfo.COLUMN_DISPLAY_NAME + "(NID)");
                        }

                    }

                    if (!string.IsNullOrWhiteSpace(value) && tableColumnInfo.IS_UNIQUE)
                    {
                        DbContext context = null;
                        context = DbUtil.Open(false);

                        var parameters = new Hashtable();

                        var keyWhere = new StringBuilder();
                        var keyColumns = tableColumnInfos.Where(x => x.IS_KEY);
                        foreach (var keyColumn in keyColumns)
                        {
                            if (!datas.ContainsKey(keyColumn.COLUMN_NAME) || string.IsNullOrEmpty(datas[keyColumn.COLUMN_NAME].ToString())) continue;
                            keyWhere.AppendLine(" " + keyColumn.COLUMN_NAME + "= :" + keyColumn.COLUMN_NAME);
                            if (keyColumn != keyColumns.Last()) keyWhere.AppendLine(" and");
                            parameters.Add(keyColumn.COLUMN_NAME, datas[keyColumn.COLUMN_NAME]);
                        }
                        var sql = new StringBuilder();
                        sql.AppendLine("select");
                        sql.AppendLine(" " + tableColumnInfo.COLUMN_NAME);
                        sql.AppendLine(" from");
                        sql.AppendLine(" " + tableColumnInfo.TABLE_NAME);
                        sql.AppendLine("where");
                        if (keyWhere.Length > 0)
                        {
                            sql.AppendLine(" not(" + keyWhere.ToString() + ")");
                            sql.AppendLine(" and");
                        }

                        //2018/08/10 TPE Delete start
                        //sql.AppendLine(" " + tableColumnInfo.COLUMN_NAME + " = :" + tableColumnInfo.COLUMN_NAME);

                        //if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value);
                        //2018/08/10 TPE Delete End

                        //2018/08/10 TPE.Sugimoto Add Sta
                        if (tableColumnInfo.COLUMN_NAME == "UNIT_NAME" ||
                            tableColumnInfo.COLUMN_NAME == "USAGE")
                        {
                            sql.AppendLine(" REPLACE(" + tableColumnInfo.COLUMN_NAME + ",' ','') = :" + tableColumnInfo.COLUMN_NAME);
                            if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value.Replace(" ", "").Replace("　", ""));
                        }
                        //2018/09/20 TPE.Kometani Add Sta
                        else if (tableColumnInfo.COLUMN_NAME == "MAKER_NAME")
                        {
                            sql.AppendLine(" REPLACE(" + tableColumnInfo.COLUMN_NAME + ",' ','') = :" + tableColumnInfo.COLUMN_NAME);
                            sql.AppendLine(" COLLATE Japanese_CI_AS_KS ");
                            if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value.Replace(" ", "").Replace("　", ""));
                        }
                        //2018/09/20 TPE.Kometani Add End
                        //2018/10/31 TPE.Sugimoto Add Sta
                        else if (tableColumnInfo.COLUMN_NAME == "LOC_BARCODE")
                        {
                            sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");
                            sql.AppendLine(" and");
                            sql.AppendLine(" " + tableColumnInfo.COLUMN_NAME + " = :" + tableColumnInfo.COLUMN_NAME);
                            if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value);
                        }
                        //2018/10/31 TPE.Sugimoto Add End
                        else
                        {
                            sql.AppendLine(" " + tableColumnInfo.COLUMN_NAME + " = :" + tableColumnInfo.COLUMN_NAME);
                            if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value);
                        }
                        //2018/08/10 TPE.Sugimoto Add End


                        var records = DbUtil.Select(context, sql.ToString(), parameters);

                        if (records.Rows.Count > 0)
                        {
                            yield return string.Format(WarningMessages.NotUnique, tableColumnInfo.COLUMN_DISPLAY_NAME, value);
                        }

                        context.Close();
                    }
                    //2018/08/23 TPE.Sugimoto Add Sta
                    //管理台帳または在庫で使用中の保管場所について、選択可否のプルダウンを選択不可に変更して更新しようとした場合
                    if (!string.IsNullOrWhiteSpace(value)
                        && tableColumnInfo.TABLE_NAME == "M_LOCATION"
                        && tableColumnInfo.COLUMN_NAME == "LOC_ID"
                        && datas["SEL_FLAG"] == "0"
                        )
                    {
                        if (isLocationExist(value))
                        {
                            yield return string.Format(WarningMessages.UsedLocationError, tableColumnInfo.COLUMN_DISPLAY_NAME);
                        }
                    }
                    //2018/08/23 TPE.Sugimoto Add End

                    //2019/02/18 TPE.Sugimoto Add Sta
                    //管理台帳または在庫で使用中の保管場所について、保管可能危険物の対応する分類のチェックを外して更新しようとした場合

                    if (!string.IsNullOrWhiteSpace(value)
                        && tableColumnInfo.TABLE_NAME == "M_LOCATION"
                        && tableColumnInfo.COLUMN_NAME == "LOC_ID"
                        && masterDatas["CLASS_ID"] == "1"
                        && masterDatas["HAZARD_CHECK1"] == "0"
                        )
                    {
                        if (isLocationHazardExist1(value))
                        {
                            yield return string.Format(WarningMessages.UsedLocationHazardError, "第一類");
                        }
                        if (isStockLocationHazardExist1(value))
                        {
                            yield return string.Format(WarningMessages.UsedStockLocationHazardError, "第一類");
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(value)
                        && tableColumnInfo.TABLE_NAME == "M_LOCATION"
                        && tableColumnInfo.COLUMN_NAME == "LOC_ID"
                        && masterDatas["CLASS_ID"] == "1"
                        && masterDatas["HAZARD_CHECK2"] == "0"
                        )
                    {
                        if (isLocationHazardExist2(value))
                        {
                            yield return string.Format(WarningMessages.UsedLocationHazardError, "第二類");
                        }
                        if (isStockLocationHazardExist2(value))
                        {
                            yield return string.Format(WarningMessages.UsedStockLocationHazardError, "第二類");
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(value)
                        && tableColumnInfo.TABLE_NAME == "M_LOCATION"
                        && tableColumnInfo.COLUMN_NAME == "LOC_ID"
                        && masterDatas["CLASS_ID"] == "1"
                        && masterDatas["HAZARD_CHECK3"] == "0"
                        )
                    {
                        if (isLocationHazardExist3(value))
                        {
                            yield return string.Format(WarningMessages.UsedLocationHazardError, "第三類");
                        }
                        if (isStockLocationHazardExist3(value))
                        {
                            yield return string.Format(WarningMessages.UsedStockLocationHazardError, "第三類");
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(value)
                        && tableColumnInfo.TABLE_NAME == "M_LOCATION"
                        && tableColumnInfo.COLUMN_NAME == "LOC_ID"
                        && masterDatas["CLASS_ID"] == "1"
                        && masterDatas["HAZARD_CHECK4"] == "0"
                        )
                    {
                        if (isLocationHazardExist4(value))
                        {
                            yield return string.Format(WarningMessages.UsedLocationHazardError, "第四類");
                        }
                        if (isStockLocationHazardExist4(value))
                        {
                            yield return string.Format(WarningMessages.UsedStockLocationHazardError, "第四類");
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(value)
                        && tableColumnInfo.TABLE_NAME == "M_LOCATION"
                        && tableColumnInfo.COLUMN_NAME == "LOC_ID"
                        && masterDatas["CLASS_ID"] == "1"
                        && masterDatas["HAZARD_CHECK5"] == "0"
                        )
                    {
                        if (isLocationHazardExist5(value))
                        {
                            yield return string.Format(WarningMessages.UsedLocationHazardError, "第五類");
                        }
                        if (isStockLocationHazardExist5(value))
                        {
                            yield return string.Format(WarningMessages.UsedStockLocationHazardError, "第五類");
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(value)
                        && tableColumnInfo.TABLE_NAME == "M_LOCATION"
                        && tableColumnInfo.COLUMN_NAME == "LOC_ID"
                        && masterDatas["CLASS_ID"] == "1"
                        && masterDatas["HAZARD_CHECK6"] == "0"
                        )
                    {
                        if (isLocationHazardExist6(value))
                        {
                            yield return string.Format(WarningMessages.UsedLocationHazardError, "第六類");
                        }
                        if (isStockLocationHazardExist6(value))
                        {
                            yield return string.Format(WarningMessages.UsedStockLocationHazardError, "第六類");
                        }
                    }
                    if (!string.IsNullOrWhiteSpace(value)
                        && tableColumnInfo.TABLE_NAME == "M_LOCATION"
                        && tableColumnInfo.COLUMN_NAME == "LOC_ID"
                        && masterDatas["CLASS_ID"] == "1"
                        && masterDatas["HAZARD_CHECK7"] == "0"
                        )
                    {
                        if (isLocationHazardExist7(value))
                        {
                            yield return string.Format(WarningMessages.UsedLocationHazardError, "指定可燃物");
                        }
                        if (isStockLocationHazardExist7(value))
                        {
                            yield return string.Format(WarningMessages.UsedStockLocationHazardError, "指定可燃物");
                        }
                    }
                    //2019/02/18 TPE.Sugimoto Add End

                    string keyColumnName;
                    string valueColumnName;
                    string iconPathColumnName;
                    string sortOrderColumnName;
                    string classColumnName;
                    string selFlgName;//2018/08/19 TPE.Sugimoto Add
                    string classId;

                    // 親子関係があるテーブル且つ分類がある場合
                    //2018/08/19 TPE Delete Start
                    //if (IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                    //                                                                                  out sortOrderColumnName, out classColumnName))
                    //2018/08/19 TPE Delete End

                    //2018/08/19 TPE Add Start
                    if (IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                                                                                                      out sortOrderColumnName, out classColumnName, out selFlgName))
                    //2018/08/19 TPE Add End
                    //if (IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                    //                                                                                  out sortOrderColumnName, out classColumnName, out selFlgName,out classId))    //2019/02/09 TPE.Rin Add
                    {
                        var table = getParentChildTable(tableColumnInfo.TABLE_NAME, keyColumnName, classColumnName, tableColumnInfo.COLUMN_NAME);

                        // 上位カテゴリーに自身が存在しないか確認します。
                        var keyValue = string.Empty;
                        if (datas.ContainsKey(keyColumnName)) keyValue = datas[keyColumnName];
                        if (!string.IsNullOrEmpty(keyValue) && !string.IsNullOrEmpty(value))
                        {
                            if (isSelfUsed(table, value, keyValue))
                            {
                                yield return string.Format(WarningMessages.ParentOwnExists, tableColumnInfo.COLUMN_DISPLAY_NAME);
                            }
                        }

                        // 最上位カテゴリーとの分類に矛盾がないか確認します。
                        if (!string.IsNullOrEmpty(classColumnName) && !string.IsNullOrEmpty(value))
                        {
                            var parent = getParentData(table, value);
                            //var classId = datas.GetValue(classColumnName);
                            var classid = datas.GetValue(classColumnName);  //2019/02/09 TPE.Rin Mod
                            //if (parent != null && parent.Class != classId)
                            if (parent != null && parent.Class != classid)
                            {
                                var classDisplayName = tableColumnInfos.Single(x => x.COLUMN_NAME == classColumnName).COLUMN_DISPLAY_NAME;
                                yield return string.Format(WarningMessages.Contradiction, classDisplayName, tableColumnInfo.COLUMN_DISPLAY_NAME);
                            }
                        }
                    }

                    // フロー階層に歯抜けがある場合
                    if (tableColumnInfo.TABLE_NAME == "M_WORKFLOW" && !isToothlessAdded)
                    {
                        if (tableColumnInfo.COLUMN_NAME.Contains("APPROVAL_USER_CD"))
                        {
                            if (string.IsNullOrWhiteSpace(value) && isToothless == false)
                            {
                                isToothless = true;

                            }
                            else if (!string.IsNullOrWhiteSpace(value) && isToothless == true)
                            {
                                yield return string.Format(WarningMessages.Toothless);
                                isToothlessAdded = true;
                            }
                        }
                    }
                    //2019/02/18 TPE.Sugimoto Add Sta
                    //メーカーマスターでメーカー名を登録・更新しようとした場合
                    if (tableColumnInfo.TABLE_NAME == "M_MAKER" && tableColumnInfo.COLUMN_NAME == "MAKER_NAME")
                    {
                        // メーカー名チェック定義ファイルが存在しないとき、エラーを出力する
                        SetConfig();
                        string strMakerCheckFilePath = clsConfig.MAKER_CHECK_TXT;
                        if (!System.IO.File.Exists(strMakerCheckFilePath))
                        {
                            yield return string.Format(WarningMessages.NotFoundFile);
                            break;
                        }
                        // メーカー名チェック定義ファイルの読み込み
                        using (var sr = new System.IO.StreamReader(clsConfig.MAKER_CHECK_TXT, System.Text.Encoding.GetEncoding("shift_jis")))
                        {
                            List<string> list = new List<string>();
                            string line = "";

                            while (!sr.EndOfStream)
                            {
                                line = sr.ReadLine();
                                if (line.Length == 0)  // 空行が発生した時点で抜ける
                                {
                                    break;
                                }
                                string[] spLine = line.Split(',');

                                if (string.IsNullOrEmpty(value))
                                {
                                    yield break;
                                }
                                IEnumerable<string> characters = spLine;

                                foreach (var c in characters)
                                {
                                    if (value.Contains(c))
                                    {
                                        yield return string.Format(WarningMessages.ContainProhibitionCharacter_Maker, tableColumnInfo.COLUMN_DISPLAY_NAME, c);
                                    }
                                }
                            }
                        }
                    }
                    //2019/02/18 TPE.Sugimoto Add End
                }
            }
        }
        //2019/02/18 TPE.Sugimoto Add Sta
        /// <summary>
        /// メーカー名チェック定義ファイルの値取得
        /// </summary>
        private static void SetConfig()
        {
            clsConfig.MAKER_CHECK_TXT = Convert.ToString(ConfigurationManager.AppSettings["MAKER_CHECK_TXT"]);
        }
        //2019/02/18 TPE.Sugimoto Add End

        /// <summary>
        /// 親子関係のあるデータ
        /// </summary>
        private class parentChildData
        {
            /// <summary>
            /// ID
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// 分類
            /// </summary>
            public string Class { get; set; }

            /// <summary>
            /// 親ID
            /// </summary>
            public string ParentId { get; set; }
        }

        /// <summary>
        /// 親子関係のあるテーブルのデータを取得します。
        /// </summary>
        /// <param name="tableName">テーブル名。</param>
        /// <param name="idColumnName">ID項目名。</param>
        /// <param name="classColumnName">クラス項目名。</param>
        /// <param name="parentIdColumnName">親ID項目名。</param>
        /// <returns></returns>
        private static IEnumerable<parentChildData> getParentChildTable(string tableName, string idColumnName, string classColumnName, string parentIdColumnName)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + idColumnName + ",");
            if (!string.IsNullOrEmpty(classColumnName)) sql.AppendLine(" " + classColumnName + ",");
            sql.AppendLine(" " + parentIdColumnName);
            sql.AppendLine("from");
            sql.AppendLine(" " + tableName);
            sql.AppendLine("where");
            sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

            var records = DbUtil.Select(context, sql.ToString());

            foreach (DataRow r in records.Rows)
            {
                var d = new parentChildData();
                d.Id = r[idColumnName].ToString();
                if (!string.IsNullOrEmpty(classColumnName)) d.Class = r[classColumnName].ToString();
                d.ParentId = r[parentIdColumnName].ToString();
                yield return d;
            }

            context.Close();
        }

        /// <summary>
        /// 再帰処理で親カテゴリーを取得します。
        /// </summary>
        /// <param name="dataList">親子関係テーブルのデータ。</param>
        /// <param name="id">検索するID。</param>
        /// <returns>親カテゴリーを取得します。</returns>
        private static parentChildData getParentData(IEnumerable<parentChildData> dataList, string id)
        {
            var data = dataList.SingleOrDefault(x => x.Id == id);
            if (data == null) return data;
            if (string.IsNullOrEmpty(data.ParentId)) return data;
            return getParentData(dataList, data.ParentId);
        }

        /// <summary>
        /// 親カテゴリーに自身のIDが使用されているか。
        /// </summary>
        /// <param name="dataList">親子関係テーブルのデータ。</param>
        /// <param name="id">検索の起点となる親ID。</param>
        /// <param name="id">検索する自身のID。</param>
        /// <returns>使用されている場合はtrue、使用されていない場合はfalse。</returns>
        private static bool isSelfUsed(IEnumerable<parentChildData> dataList, string parentId, string selfId)
        {
            if (parentId == selfId) return true;
            var data = dataList.SingleOrDefault(x => x.Id == parentId);
            if (data == null) return false;
            if (string.IsNullOrEmpty(data.ParentId)) return false;
            return isSelfUsed(dataList, data.ParentId, selfId);
        }

        /// <summary>
        /// ワークフロー用にシーケンス番号を取得します。
        /// </summary>
        public static string getNewFlowcdSequence()
        {
            return getNewSequence();
        }

        /// <summary>
        /// ワークフロー用にシーケンス番号を取得します。
        /// </summary>
        private static string getNewSequence()
        {
            while (true)
            {
                DbContext context = null;
                context = DbUtil.Open(false);

                var sql = string.Empty;
                //  if (Common.DatabaseType == DatabaseType.SqlServer)
                //  {
                sql = "select next value for FLOWCODE_SEQ";
                //  }

                var newFlowcodeTable = DbUtil.Select(context, sql);
                DataRow row = newFlowcodeTable.Rows[0];

                var flowcodeString = NikonConst.FlowCodePrefix + Convert.ToInt32(row[0].ToString()).ToString("D5");

                // フローコードが使用されていないかチェックし、使用されていればもう一度発行します。
                sql = "select FLOW_CD from M_WORKFLOW where FLOW_CD = :FLOW_CD";
                var parameters = new Hashtable();
                parameters.Add("FLOW_CD", flowcodeString);
                DataTable flowcodeExists;

                flowcodeExists = DbUtil.Select(context, sql, parameters);

                context.Close();
                if (flowcodeExists.Rows.Count == 0)
                {
                    return flowcodeString;
                }
            }
        }

        /// <summary>
        /// ユーザーマスターでワークフローが設定されていないか確認します。
        /// </summary>
        public static bool isUsedByUser(Dictionary<string, string> Keys)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            var sql = new StringBuilder();
            var parameter = new Hashtable();
            sql.AppendLine("select count(*) from " + nameof(M_USER));
            sql.AppendLine(" where ");
            sql.AppendLine(" DEL_FLAG <> 1 ");
            sql.AppendLine(" and " + nameof(M_USER) + "." + nameof(M_USER.FLOW_CD) + " = :" + nameof(M_USER.FLOW_CD));
            parameter.Add(nameof(M_USER.FLOW_CD), Keys[nameof(M_USER.FLOW_CD)]);
            var ret = DbUtil.Select(context, sql.ToString(), parameter);
            context.Close();
            return (int)(ret.Rows[0][0]) > 0;
        }

        /// <summary>
        /// 管理台帳申請フローで使用されていないか確認します。
        /// </summary>
        public static bool isRegistByFlow(Dictionary<string, string> Keys)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            var sql = new StringBuilder();
            var parameter = new Hashtable();

            sql.AppendLine("select count(*) from " + nameof(T_LEDGER_WORK));
            sql.AppendLine(" where ");
            sql.AppendLine(" " + nameof(T_LEDGER_WORK.FLOW_CD) + " = :" + nameof(T_LEDGER_WORK.FLOW_CD));
            parameter.Add(nameof(M_USER.FLOW_CD), Keys[nameof(M_USER.FLOW_CD)]);

            var ret = DbUtil.Select(context, sql.ToString(), parameter);
            context.Close();
            return (int)(ret.Rows[0][0]) > 0;
        }

        /// <summary>
        /// 保管場所が管理台帳で申請中または登録されていないか確認します。
        /// </summary>
        public static bool isLocationExist(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            var tablenames = new string[] { "T_STOCK", "T_MGMT_LED_LOC", "T_MGMT_LED_USAGE_LOC", "T_LED_WORK_LOC", "T_LED_WORK_USAGE_LOC" };
            bool ret = false;
            foreach (var tablename in tablenames)
            {
                var sql = new StringBuilder();
                var parameters = new Hashtable();

                sql.AppendLine("select");
                sql.AppendLine(" LOC_ID ");
                sql.AppendLine("from");
                sql.AppendLine(" " + tablename);
                sql.AppendLine("where");

                sql.AppendLine(" LOC_ID " + " = :" + nameof(M_LOCATION.LOC_ID));
                if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

                var records = DbUtil.Select(context, sql.ToString(), parameters);

                if (records.Rows.Count > 0)
                {
                    ret = true;
                    break;
                }
            }

            context.Close();
            return ret;
        }

        /// <summary>
        /// 対応する各保管可能危険物の分類が設定されている化学物質が管理台帳と在庫で使用されていないか確認します。
        /// </summary>
        /// //2019/02/18 TPE.Sugimoto Add Sta
        public static bool isLocationHazardExist1(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" tlw.LEDGER_FLOW_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_LEDGER_WORK tlw");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  tlw.CHEM_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join T_LED_WORK_LOC tlwl on  tlw.LEDGER_FLOW_ID = tlwl.LEDGER_FLOW_ID");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  tlwl.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第一類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第一類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第一類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");

            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            var sql_2 = new StringBuilder();

            sql_2.AppendLine("select");
            sql_2.AppendLine(" tml.REG_NO");
            sql_2.AppendLine(" from");
            sql_2.AppendLine("  T_MGMT_LEDGER tml");
            sql_2.AppendLine(" left join M_CHEM_REGULATION mcr on  tml.CHEM_CD = mcr.CHEM_CD");
            sql_2.AppendLine(" left join T_MGMT_LED_LOC tmll on  tml.REG_NO = tmll.REG_NO");
            sql_2.AppendLine(" left join M_LOCATION_HAZARD mlh on  tmll.LOC_ID = mlh.LOC_ID");
            sql_2.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql_2.AppendLine(" where");
            sql_2.AppendLine(" mlh.REG_TYPE_ID = ");
            sql_2.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("  and REG_TEXT = '第一類')");
            sql_2.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" (");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("    and REG_TEXT = '第一類')");
            sql_2.AppendLine("  )");
            sql_2.AppendLine("  or");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("     and REG_TEXT = '第一類')");
            sql_2.AppendLine("    )");
            sql_2.AppendLine("   )");
            sql_2.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records_2 = DbUtil.Select(context, sql_2.ToString(), parameters);

            if (records.Rows.Count > 0 || records_2.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isStockLocationHazardExist1(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" t.STOCK_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_STOCK t");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  t.CAT_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  t.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第一類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第一類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第一類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            if (records.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isLocationHazardExist2(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" tlw.LEDGER_FLOW_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_LEDGER_WORK tlw");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  tlw.CHEM_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join T_LED_WORK_LOC tlwl on  tlw.LEDGER_FLOW_ID = tlwl.LEDGER_FLOW_ID");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  tlwl.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第二類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第二類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第二類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            var sql_2 = new StringBuilder();

            sql_2.AppendLine("select");
            sql_2.AppendLine(" tml.REG_NO");
            sql_2.AppendLine(" from");
            sql_2.AppendLine("  T_MGMT_LEDGER tml");
            sql_2.AppendLine(" left join M_CHEM_REGULATION mcr on  tml.CHEM_CD = mcr.CHEM_CD");
            sql_2.AppendLine(" left join T_MGMT_LED_LOC tmll on  tml.REG_NO = tmll.REG_NO");
            sql_2.AppendLine(" left join M_LOCATION_HAZARD mlh on  tmll.LOC_ID = mlh.LOC_ID");
            sql_2.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql_2.AppendLine(" where");
            sql_2.AppendLine(" mlh.REG_TYPE_ID = ");
            sql_2.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("  and REG_TEXT = '第二類')");
            sql_2.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" (");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("    and REG_TEXT = '第二類')");
            sql_2.AppendLine("  )");
            sql_2.AppendLine("  or");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("     and REG_TEXT = '第二類')");
            sql_2.AppendLine("    )");
            sql_2.AppendLine("   )");
            sql_2.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records_2 = DbUtil.Select(context, sql_2.ToString(), parameters);

            if (records.Rows.Count > 0 || records_2.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isStockLocationHazardExist2(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" t.STOCK_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_STOCK t");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  t.CAT_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  t.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第二類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第二類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第二類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            if (records.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isLocationHazardExist3(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" tlw.LEDGER_FLOW_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_LEDGER_WORK tlw");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  tlw.CHEM_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join T_LED_WORK_LOC tlwl on  tlw.LEDGER_FLOW_ID = tlwl.LEDGER_FLOW_ID");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  tlwl.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第三類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第三類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第三類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            var sql_2 = new StringBuilder();

            sql_2.AppendLine("select");
            sql_2.AppendLine(" tml.REG_NO");
            sql_2.AppendLine(" from");
            sql_2.AppendLine("  T_MGMT_LEDGER tml");
            sql_2.AppendLine(" left join M_CHEM_REGULATION mcr on  tml.CHEM_CD = mcr.CHEM_CD");
            sql_2.AppendLine(" left join T_MGMT_LED_LOC tmll on  tml.REG_NO = tmll.REG_NO");
            sql_2.AppendLine(" left join M_LOCATION_HAZARD mlh on  tmll.LOC_ID = mlh.LOC_ID");
            sql_2.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql_2.AppendLine(" where");
            sql_2.AppendLine(" mlh.REG_TYPE_ID = ");
            sql_2.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("  and REG_TEXT = '第三類')");
            sql_2.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" (");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("    and REG_TEXT = '第三類')");
            sql_2.AppendLine("  )");
            sql_2.AppendLine("  or");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("     and REG_TEXT = '第三類')");
            sql_2.AppendLine("    )");
            sql_2.AppendLine("   )");
            sql_2.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records_2 = DbUtil.Select(context, sql_2.ToString(), parameters);

            if (records.Rows.Count > 0 || records_2.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isStockLocationHazardExist3(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" t.STOCK_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_STOCK t");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  t.CAT_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  t.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第三類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第三類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第三類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            if (records.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isLocationHazardExist4(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" tlw.LEDGER_FLOW_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_LEDGER_WORK tlw");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  tlw.CHEM_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join T_LED_WORK_LOC tlwl on  tlw.LEDGER_FLOW_ID = tlwl.LEDGER_FLOW_ID");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  tlwl.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第四類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第四類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第四類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            var sql_2 = new StringBuilder();

            sql_2.AppendLine("select");
            sql_2.AppendLine(" tml.REG_NO");
            sql_2.AppendLine(" from");
            sql_2.AppendLine("  T_MGMT_LEDGER tml");
            sql_2.AppendLine(" left join M_CHEM_REGULATION mcr on  tml.CHEM_CD = mcr.CHEM_CD");
            sql_2.AppendLine(" left join T_MGMT_LED_LOC tmll on  tml.REG_NO = tmll.REG_NO");
            sql_2.AppendLine(" left join M_LOCATION_HAZARD mlh on  tmll.LOC_ID = mlh.LOC_ID");
            sql_2.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql_2.AppendLine(" where");
            sql_2.AppendLine(" mlh.REG_TYPE_ID = ");
            sql_2.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("  and REG_TEXT = '第四類')");
            sql_2.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" (");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("    and REG_TEXT = '第四類')");
            sql_2.AppendLine("  )");
            sql_2.AppendLine("  or");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("     and REG_TEXT = '第四類')");
            sql_2.AppendLine("    )");
            sql_2.AppendLine("   )");
            sql_2.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records_2 = DbUtil.Select(context, sql_2.ToString(), parameters);

            if (records.Rows.Count > 0 || records_2.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isStockLocationHazardExist4(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" t.STOCK_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_STOCK t");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  t.CAT_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  t.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第四類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第四類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第四類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            if (records.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isLocationHazardExist5(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" tlw.LEDGER_FLOW_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_LEDGER_WORK tlw");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  tlw.CHEM_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join T_LED_WORK_LOC tlwl on  tlw.LEDGER_FLOW_ID = tlwl.LEDGER_FLOW_ID");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  tlwl.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第五類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第五類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第五類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            var sql_2 = new StringBuilder();

            sql_2.AppendLine("select");
            sql_2.AppendLine(" tml.REG_NO");
            sql_2.AppendLine(" from");
            sql_2.AppendLine("  T_MGMT_LEDGER tml");
            sql_2.AppendLine(" left join M_CHEM_REGULATION mcr on  tml.CHEM_CD = mcr.CHEM_CD");
            sql_2.AppendLine(" left join T_MGMT_LED_LOC tmll on  tml.REG_NO = tmll.REG_NO");
            sql_2.AppendLine(" left join M_LOCATION_HAZARD mlh on  tmll.LOC_ID = mlh.LOC_ID");
            sql_2.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql_2.AppendLine(" where");
            sql_2.AppendLine(" mlh.REG_TYPE_ID = ");
            sql_2.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("  and REG_TEXT = '第五類')");
            sql_2.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" (");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("    and REG_TEXT = '第五類')");
            sql_2.AppendLine("  )");
            sql_2.AppendLine("  or");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("     and REG_TEXT = '第五類')");
            sql_2.AppendLine("    )");
            sql_2.AppendLine("   )");
            sql_2.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records_2 = DbUtil.Select(context, sql_2.ToString(), parameters);

            if (records.Rows.Count > 0 || records_2.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isStockLocationHazardExist5(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" t.STOCK_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_STOCK t");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  t.CAT_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  t.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第五類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第五類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第五類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            if (records.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isLocationHazardExist6(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" tlw.LEDGER_FLOW_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_LEDGER_WORK tlw");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  tlw.CHEM_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join T_LED_WORK_LOC tlwl on  tlw.LEDGER_FLOW_ID = tlwl.LEDGER_FLOW_ID");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  tlwl.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第六類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第六類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第六類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            var sql_2 = new StringBuilder();

            sql_2.AppendLine("select");
            sql_2.AppendLine(" tml.REG_NO");
            sql_2.AppendLine(" from");
            sql_2.AppendLine("  T_MGMT_LEDGER tml");
            sql_2.AppendLine(" left join M_CHEM_REGULATION mcr on  tml.CHEM_CD = mcr.CHEM_CD");
            sql_2.AppendLine(" left join T_MGMT_LED_LOC tmll on  tml.REG_NO = tmll.REG_NO");
            sql_2.AppendLine(" left join M_LOCATION_HAZARD mlh on  tmll.LOC_ID = mlh.LOC_ID");
            sql_2.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql_2.AppendLine(" where");
            sql_2.AppendLine(" mlh.REG_TYPE_ID = ");
            sql_2.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("  and REG_TEXT = '第六類')");
            sql_2.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" (");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("    and REG_TEXT = '第六類')");
            sql_2.AppendLine("  )");
            sql_2.AppendLine("  or");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("     and REG_TEXT = '第六類')");
            sql_2.AppendLine("    )");
            sql_2.AppendLine("   )");
            sql_2.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records_2 = DbUtil.Select(context, sql_2.ToString(), parameters);

            if (records.Rows.Count > 0 || records_2.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isStockLocationHazardExist6(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" t.STOCK_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_STOCK t");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  t.CAT_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  t.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '第六類')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '第六類')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '第六類')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            if (records.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isLocationHazardExist7(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" tlw.LEDGER_FLOW_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_LEDGER_WORK tlw");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  tlw.CHEM_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join T_LED_WORK_LOC tlwl on  tlw.LEDGER_FLOW_ID = tlwl.LEDGER_FLOW_ID");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  tlwl.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '指定可燃物')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '指定可燃物')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '指定可燃物')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            var sql_2 = new StringBuilder();

            sql_2.AppendLine("select");
            sql_2.AppendLine(" tml.REG_NO");
            sql_2.AppendLine(" from");
            sql_2.AppendLine("  T_MGMT_LEDGER tml");
            sql_2.AppendLine(" left join M_CHEM_REGULATION mcr on  tml.CHEM_CD = mcr.CHEM_CD");
            sql_2.AppendLine(" left join T_MGMT_LED_LOC tmll on  tml.REG_NO = tmll.REG_NO");
            sql_2.AppendLine(" left join M_LOCATION_HAZARD mlh on  tmll.LOC_ID = mlh.LOC_ID");
            sql_2.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql_2.AppendLine(" where");
            sql_2.AppendLine(" mlh.REG_TYPE_ID = ");
            sql_2.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("  and REG_TEXT = '指定可燃物')");
            sql_2.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql_2.AppendLine(" and");
            sql_2.AppendLine(" (");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("    and REG_TEXT = '指定可燃物')");
            sql_2.AppendLine("  )");
            sql_2.AppendLine("  or");
            sql_2.AppendLine("  mcr.REG_TYPE_ID in");
            sql_2.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql_2.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql_2.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql_2.AppendLine("     and REG_TEXT = '指定可燃物')");
            sql_2.AppendLine("    )");
            sql_2.AppendLine("   )");
            sql_2.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records_2 = DbUtil.Select(context, sql_2.ToString(), parameters);

            if (records.Rows.Count > 0 || records_2.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        public static bool isStockLocationHazardExist7(string id)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            bool ret = false;
            var sql = new StringBuilder();
            var parameters = new Hashtable();

            sql.AppendLine("select");
            sql.AppendLine(" t.STOCK_ID");
            sql.AppendLine(" from");
            sql.AppendLine("  T_STOCK t");
            sql.AppendLine(" left join M_CHEM_REGULATION mcr on  t.CAT_CD = mcr.CHEM_CD");
            sql.AppendLine(" left join M_LOCATION_HAZARD mlh on  t.LOC_ID = mlh.LOC_ID");
            sql.AppendLine(" left join M_REGULATION mr on  mlh.REG_TYPE_ID = mr.REG_TYPE_ID");
            sql.AppendLine(" where");
            sql.AppendLine(" mlh.REG_TYPE_ID = ");
            sql.AppendLine(" (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("  and REG_TEXT = '指定可燃物')");
            sql.AppendLine(" and mlh.LOC_ID = :" + nameof(M_LOCATION.LOC_ID));
            sql.AppendLine(" and");
            sql.AppendLine(" mlh.HAZARD_CHECK = 1");
            sql.AppendLine(" and");
            sql.AppendLine(" (");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("    and REG_TEXT = '指定可燃物')");
            sql.AppendLine("  )");
            sql.AppendLine("  or");
            sql.AppendLine("  mcr.REG_TYPE_ID in");
            sql.AppendLine("  (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID in");
            sql.AppendLine("   (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("    (select REG_TYPE_ID from M_REGULATION where P_REG_TYPE_ID =");
            sql.AppendLine("     (select REG_TYPE_ID from M_REGULATION where REG_TEXT = '消防法' and CLASS_ID = 3) ");
            sql.AppendLine("     and REG_TEXT = '指定可燃物')");
            sql.AppendLine("    )");
            sql.AppendLine("   )");
            sql.AppendLine("  )");
            if (!parameters.ContainsKey(nameof(M_LOCATION.LOC_ID))) parameters.Add(nameof(M_LOCATION.LOC_ID), id);

            var records = DbUtil.Select(context, sql.ToString(), parameters);

            if (records.Rows.Count > 0)
            {
                ret = true;
            }

            context.Close();
            return ret;
        }
        //2019/02/18 TPE.Sugimoto Add End

        /// <summary>
        /// <para>データテーブルのヘッダ情報をCSVファイルに出力します。 </para>
        /// </summary>
        /// <param name="dt">CSVファイルに出力する情報</param>
        /// <param name="fileFullPath">CSVファイルパス</param>
        public static void TemplateCSV(DataTable dt, string fileFullPath)
        {
            var header = new List<string>();
            foreach (DataColumn col in dt.Columns)
            {
                    header.Add("\"" + col.Caption + "\"");
            }
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                csv.WriteLine(string.Join(",", header).Replace(SystemConst.HtmlHalfWidthSpace, string.Empty));
                csv.Close();
            }
        }

        /// <summary>
        /// テーブル項目管理情報から<see cref="DataTable"/>のコード情報などを名称に変換した列を追加します。
        /// </summary>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="ConvertTable">変換列を追加する対象のテーブルを指定します。</param>
        public static List<M_COMMON> ConvertCommonMaster(List<S_TAB_COL_CONTROL> TableColumnInformations, List<M_COMMON> ConvertTable)
        {
            // newConvertTable = ConvertTable;
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "TABLE_NAME")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].TABLE_NAME),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].TABLENAME = ConvertTable[i].TABLE_NAME;
                        ConvertTable[i].TABLE_NAME = value;


                    }

                    // newConvertTable = ConvertTable;
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "DISPLAY_VALUE")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].DISPLAY_VALUE),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].DISPLAY_VALUE = value;

                    }

                    // newConvertTable = ConvertTable;
                }

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "DISPLAY_ORDER")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].DISPLAY_ORDER),
                //                                                              out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        ConvertTable[i].DISPLAY_ORDER = value;

                //    }
                //}

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "KEY_VALUE")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].KEY_VALUE),
                //                                                              out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        ConvertTable[i].KEY_VALUE = value;

                //    }
                //}

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "WEIGHT_MGMT")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].WEIGHT_MGMT),
                //                                                              out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        ConvertTable[i].WEIGHT_MGMT_NAME = value;

                //        if (ConvertTable[i].RA_FLAG == "1")
                //        {
                //            ConvertTable[i].RA_FLAG = SystemConst.RaCheckedString;
                //            ConvertTable[i].RA_FLAG2 = true;
                //        }
                //        else
                //        {
                //            ConvertTable[i].RA_FLAG = SystemConst.RaNotCheckString;
                //            ConvertTable[i].RA_FLAG2 = false;
                //        }

                //    }
                //}

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "WORKTIME_MGMT")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].WORKTIME_MGMT),
                //                                                              out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        ConvertTable[i].WORKTIME_MGMT_NAME = value;

                //    }
                //}

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "P_REG_TYPE_ID")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].P_REG_TYPE_ID),
                //                                                              out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        ConvertTable[i].P_REG_TYPE_VALUE = value;

                //    }
                //}

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "EXPOSURE_REPORT")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].EXPOSURE_REPORT),
                //                                                              out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        ConvertTable[i].EXPOSURE_REPORT_VALUE = value;

                //    }
                //}

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Check && tableColumnInformation.COLUMN_NAME == "EXAMINATION_FLAG")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        //string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        //tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].EXAMINATION_FLAG_VALUE), out value);
                //        //if (Convert.ToString(ConvertTable[i].EXAMINATION_FLAG_VALUE) != "false")
                //        //{
                //        //    ConvertTable[i].EXAMINATION_FLAG = SystemConst.ExaCheckedString;
                //        //}else
                //        //{
                //        //    ConvertTable[i].EXAMINATION_FLAG = SystemConst.ExaNotCheckString;
                //        //}


                //        if (ConvertTable[i].EXAMINATION_FLAG == "1")
                //        {
                //            ConvertTable[i].EXAMINATION_FLAG = SystemConst.ExaCheckedString;
                //            ConvertTable[i].EXAMINATION_FLAG_VALUE = true;
                //        }
                //        else
                //        {
                //            ConvertTable[i].EXAMINATION_FLAG = SystemConst.ExaNotCheckString;
                //            ConvertTable[i].EXAMINATION_FLAG_VALUE = false;
                //        }



                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        // ConvertTable[i].EXAMINATION_FLAG = value;

                //    }
                //}

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Check && tableColumnInformation.COLUMN_NAME == "MEASUREMENT_FLAG")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        //tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].MEASUREMENT_FLAG_VALUE),
                //        //                                                      out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        //ConvertTable[i].MEASUREMENT_FLAG = value;


                //        if (ConvertTable[i].MEASUREMENT_FLAG == "1")
                //        {
                //            ConvertTable[i].MEASUREMENT_FLAG = SystemConst.MeaCheckedString;
                //            ConvertTable[i].MEASUREMENT_FLAG_VALUE = true;
                //        }
                //        else
                //        {
                //            ConvertTable[i].MEASUREMENT_FLAG = SystemConst.MEANotCheckString;
                //            ConvertTable[i].MEASUREMENT_FLAG_VALUE = false;
                //        }

                //    }
                //}

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Check && tableColumnInformation.COLUMN_NAME == "RA_FLAG")
                //{


                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        //tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].MEASUREMENT_FLAG_VALUE),
                //        //                                                      out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        //ConvertTable[i].MEASUREMENT_FLAG = value;


                //        if (Convert.ToString(ConvertTable[i].RA_FLAG2) != "false")
                //        {
                //            ConvertTable[i].RA_FLAG = SystemConst.RaCheckedString;
                //        }
                //        else
                //        {
                //            ConvertTable[i].RA_FLAG = SystemConst.RaNotCheckString;
                //        }

                //    }

                //}

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_USER_CD")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].REG_USER_CD),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].REG_USER_NAME = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "UPD_USER_CD")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].UPD_USER_CD),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].UPD_USER_NAME = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "DEL_USER_CD")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;



                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].DEL_USER_CD),
                                                                              out value);

                        ConvertTable[i].DEL_USER_NAME = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Check && tableColumnInformation.COLUMN_NAME == "DEL_FLAG")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。

                        if (ConvertTable[i].DEL_FLAG == 1)
                        {
                            //ConvertTable[i].DEL_FLAG2 = SystemConst.RaCheckedString;
                            ConvertTable[i].DEL_FLG = true;
                        }
                        else
                        {
                            //ConvertTable[i].DEL_FLAG2 = SystemConst.RaNotCheckString;
                            ConvertTable[i].DEL_FLG = false;
                        }


                        //tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].DEL_FLAG), out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        //ConvertTable[i].DEL_FLAG_VALUE = value;

                    }
                }

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_TYPE_ID")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].REG_TYPE_ID),
                //                                                              out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        ConvertTable[i].P_REG_TYPE_VALUE = value;

                //    }
                //}


            }

            return ConvertTable;
        }


        //2019/03/04 TPE.Rin Add
        public static IQueryable<CasRegModel> GetMCasReg(int id)
        {
            var sql = new StringBuilder();
            var parameter = new DynamicParameters();


            sql.AppendLine("select * from V_CAS_REGULATION");
            sql.AppendLine(" where REG_TYPE_ID = " + id);
            sql.AppendLine(" order by ROW_ID ");

            parameter.Add("@REG_TYPE_ID", id);

            List<V_CAS_REGULATION> records = new List<V_CAS_REGULATION>();

            records = DbUtil.Select<V_CAS_REGULATION>(sql.ToString(), parameter);

            var dataArray = new List<CasRegModel>();

            foreach (var record in records)
            {
                var model = new CasRegModel();

                model.casno = record.casno.ToString();
                model.ROW_ID = record.ROW_ID;
                if (!string.IsNullOrEmpty(record.maximum.ToString()))
                {
                    model.maximum = (decimal)record.maximum;

                }
                else
                {
                    model.maximum = null;
                }
                if (!string.IsNullOrEmpty(record.minimum.ToString()))
                {
                    model.minimum = (decimal)record.minimum;

                }
                else
                {
                    model.minimum = null;
                }

                //キーを値に変換
                var masterService = new MasterService();

                if (!string.IsNullOrEmpty(record.shapekey.ToString()))
                {
                    DataTable dtshape = masterService.SelectMcommon("M_FIGURE", record.shapekey.ToString());

                    if (dtshape.Rows.Count > 0)
                    {
                        string shape = "";
                        foreach (DataRow tempshape in dtshape.Rows)
                        {
                            shape += Convert.ToString(tempshape["DISPLAY_VALUE"]) + ";";
                        }
                        shape = shape.Trim(';');
                        model.shapekey = record.shapekey.ToString();
                        model.shape = shape;
                    }
                    else
                    {
                        model.shapekey = "";
                        model.shape = "";
                    }

                }
                else
                {
                    model.shapekey = "";
                    model.shape = "";
                }

                if (!string.IsNullOrEmpty(record.termskey.ToString()))
                {

                    DataTable dtterms = masterService.SelectMcommon("M_ISHA_USAGE", record.termskey.ToString());

                    if (dtterms.Rows.Count > 0)
                    {
                        string terms = "";
                        foreach (DataRow temptermse in dtterms.Rows)
                        {
                            terms += Convert.ToString(temptermse["DISPLAY_VALUE"]) + ";";
                        }
                        terms = terms.Trim(';');
                        model.termskey = record.termskey.ToString();
                        model.terms = terms;
                    }
                    else
                    {
                        model.termskey = "";
                        model.terms = "";
                    }

                }
                else
                {
                    model.termskey = "";
                    model.terms = "";
                }



                dataArray.Add(model);
            }

            var searchQuery = dataArray.AsQueryable();

            return searchQuery;
        }

        // M_Regulation Master
        public static List<M_REGULATION> SearchRegulationMasterRecord(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, M_MASTER master)//2019/02/18 TPE.Sugimoto Upd
        {
            return SearchRegulationMasterRecord(TableInformation, TableColumnInformations, null, master);//2019/02/18 TPE.Sugimoto Upd
        }

        // M_Common Master
        public static List<M_COMMON> SearchCommonMasterRecord(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, DEL_FLAG DeleteFlag, M_MASTER master)//2019/02/18 TPE.Sugimoto Upd
        {
            return SearchCommonMasterRecord(TableInformation, TableColumnInformations, null, (int)DeleteFlag, master);//2019/02/18 TPE.Sugimoto Upd
        }

        public static List<M_COMMON> SearchCommonMasterRecordUpdate(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, DEL_FLAG DeleteFlag, int id)//2019/02/18 TPE.Sugimoto Upd
        {
            return SearchCommonMasterRecordUpdate(TableInformation, TableColumnInformations, null, (int)DeleteFlag, id);//2019/02/18 TPE.Sugimoto Upd
        }


        private static List<M_REGULATION> SearchRegulationMasterRecord(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> KeyValues, M_MASTER m_master)//2019/02/18 TPE.Sugimoto Upd
        {
            List<M_REGULATION> regData = new List<M_REGULATION>();
            M_REGULATION m_REGULATION = new M_REGULATION();
            StringBuilder sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            List<S_TAB_COL_CONTROL> tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }

            //2019/05/09 TPE.Rin Add Start
            if (TableInformation.TABLE_NAME == "M_REGULATION")
            {
                sql.AppendLine(" M_REGULATION.MARKS,");
            }
            //2019/05/09 TPE.Rin Add End

            sql.Append(" " + Environment.NewLine);
            sql.Append("IIF(M_REGULATION.DEL_FLAG = 0,'使用中','削除済') AS DEL_FLAG2,");
            sql.Append(" " + Environment.NewLine);
            sql.Append("IIF(M_REGULATION.RA_FLAG = 0,'非該当','該当') AS RA_FLAG,");
            sql.Append(" " + Environment.NewLine);
            sql.Append("MARKS,");
            sql.Append(" " + Environment.NewLine);
            sql.Append("(SELECT M_USER.USER_NAME FROM M_USER WHERE M_USER.USER_CD = M_REGULATION.DEL_USER_CD) AS DEL_USER_NM ");
            sql.Append(" " + Environment.NewLine);

            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            if (!string.IsNullOrEmpty(m_master.mRegulation.CAS_NO))
            {
                sql.AppendLine(" ,M_CAS_REGULATION");
            }



            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            if (m_master.mRegulation.STATUS != "2")
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + m_master.mRegulation.STATUS);
                sql.AppendLine(" and  ");
            }

            // キー項目の指定があればキー項目で検索します。
            // キー項目の指定がなければ検索条件で検索します。
            if (KeyValues != null)
            {
                foreach (KeyValuePair<string, string> keyValue in KeyValues)
                {
                    sql.AppendLine(" " + keyValue.Key + " = :" + keyValue.Key + " ");
                    sql.AppendLine(" and  ");
                    parameter.Add(keyValue.Key, keyValue.Value);
                }
            }
            else
            {
                foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
                {
                    //2018/08/15 TPE Add Start
                    if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                    //2018/08/15 TPE Add End

                    // 検索条件を指定します。
                    //if (tableColumnInformation.IS_SEARCH && !string.IsNullOrEmpty(tableColumnInformation.InputValue?.Trim()))
                    //{
                    //if (TableInformation.TABLE_NAME == "M_REGULATION" && tableColumnInformation.COLUMN_NAME == "REG_TEXT")
                    //{
                    // 文字、数値の場合は部分一致、それ以外は完全一致で検索します。
                    if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character ||
                tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
                    {

                        if (TableInformation.TABLE_NAME == "M_REGULATION" && tableColumnInformation.COLUMN_NAME == "REG_TEXT")
                        {
                            if (m_master.mRegulation.REG_TEXT != null)
                            {
                                switch (m_master.mRegulation.REG_TEXT_CONDITION)
                                {
                                    case "0":
                                        sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like @" + tableColumnInformation.COLUMN_NAME + Classes.util.DbUtil.LikeEscapeInfoAdd());
                                        parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(m_master.mRegulation.REG_TEXT) + "%");
                                        break;

                                    case "1":
                                        sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like @" + tableColumnInformation.COLUMN_NAME + Classes.util.DbUtil.LikeEscapeInfoAdd());
                                        parameter.Add(tableColumnInformation.COLUMN_NAME, DbUtil.ConvertIntoEscapeChar(m_master.mRegulation.REG_TEXT) + "%");
                                        break;

                                    case "2":
                                        sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like @" + tableColumnInformation.COLUMN_NAME + Classes.util.DbUtil.LikeEscapeInfoAdd());
                                        parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(m_master.mRegulation.REG_TEXT));
                                        break;

                                    case "3":
                                        sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = @" + tableColumnInformation.COLUMN_NAME);
                                        parameter.Add(tableColumnInformation.COLUMN_NAME, DbUtil.ConvertIntoEscapeChar(m_master.mRegulation.REG_TEXT));
                                        break;

                                }
                                sql.AppendLine(" and  ");
                            }
                        }
                        else
                        {

                            if (m_master.mRegulation.REG_TEXT == "null")
                            {
                                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like @" + tableColumnInformation.COLUMN_NAME + Classes.util.DbUtil.LikeEscapeInfoAdd());
                                sql.AppendLine(" and  ");
                                m_master.mRegulation.REG_TEXT = "";
                                parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(m_master.mRegulation.REG_TEXT) + "%");
                            }

                        }

                    }
                    else
                    {
                        if (tableColumnInformation.COLUMN_NAME == "REG_ICON_PATH")
                        {
                            if (m_master.mRegulation.REG_ICON_PATH != "0")
                            {
                                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = @" + tableColumnInformation.COLUMN_NAME);
                                sql.AppendLine(" and  ");

                                parameter.Add(tableColumnInformation.COLUMN_NAME, m_master.mRegulation.REG_ICON_PATH);
                            }
                        }

                        else if (tableColumnInformation.COLUMN_NAME == "P_REG_TYPE_ID")
                        {
                            if (m_master.mRegulation.P_REG_TYPE_ID != null)
                            {
                                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = @" + tableColumnInformation.COLUMN_NAME);
                                sql.AppendLine(" and  ");
                                parameter.Add(tableColumnInformation.COLUMN_NAME, m_master.mRegulation.P_REG_TYPE_ID);
                            }
                        }
                        else if (tableColumnInformation.COLUMN_NAME == "CLASS_ID")

                        {
                            if (m_master.mRegulation.CLASS_ID != 0)
                            {
                                sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = @" + tableColumnInformation.COLUMN_NAME);
                                sql.AppendLine(" and  ");
                                parameter.Add(tableColumnInformation.COLUMN_NAME, m_master.mRegulation.CLASS_ID);
                            }
                        }
                        //parameter.Add(tableColumnInformation.COLUMN_NAME, tableColumnInformation.InputValue);
                    }
                    //}
                    //}
                }

                //M_CAS_REGULATIONとの条件
                if (!string.IsNullOrEmpty(m_master.mRegulation.CAS_NO))
                {
                    sql.AppendLine("M_REGULATION.REG_TYPE_ID = M_CAS_REGULATION.REG_TYPE_ID");
                    sql.AppendLine(" and  ");
                    sql.AppendLine("COLMUN_ID = '1'");
                    sql.AppendLine(" and  ");
                    sql.AppendLine("VAL_STRING=@CAS");
                    sql.AppendLine(" and  ");

                    parameter.Add("@CAS", m_master.mRegulation.CAS_NO);

                }

                sql.Length -= 8;

                sql.Append(" " + Environment.NewLine);

                sql.AppendLine("order by ");
                foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
                {
                    if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
                }
                sql.Length -= 3;



                regData = DbUtil.Select<M_REGULATION>(sql.ToString(), parameter);
                //m_master.listRegulationDetails = regData;


            }

            return regData;
        }

        /// <summary>
        /// テーブル管理情報とテーブル項目管理情報からマスターを指定された検索条件、またはキー項目で検索し、結果を返します。
        /// </summary>
        /// <param name="TableInformation">テーブル管理情報を指定します。</param>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="KeyValues">キー項目を指定します。検索条件を使用した場合は<see cref="null"/>を指定します。</param>
        /// <param name="DeleteFlag">削除フラグの状態を<see cref="SystemConst.DEL_FLAG"/>から<see cref="int"/>で指定します。削除フラグを指定しない場合は<see cref="null"/>を指定します。</param>
        /// <returns>検索結果を<see cref="DataTable"/>で返します。</returns>
        private static List<M_COMMON> SearchCommonMasterRecord(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> KeyValues, int? DeleteFlag, M_MASTER m_master)
        {
            List<M_COMMON> commonData = new List<M_COMMON>();
            M_COMMON m_COMMON = new M_COMMON();
            var sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            List<S_TAB_COL_CONTROL> tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");
            //2019/02/18 TPE.Sugimoto Add Sta
            if (TableInformation.TABLE_NAME == "M_LOCATION")
            {
                sql.AppendLine("distinct ");
            }
            //2019/02/18 TPE.Sugimoto Add End
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                //2018/08/15 TPE Add Start
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_NAME") continue;
                //2018/08/15 TPE Add End

                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID") continue;    //2018/10/03 Rin Add

                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }

            //2019/05/09 TPE.Rin Add Start
            if (TableInformation.TABLE_NAME == "M_REGULATION")
            {
                sql.AppendLine(" M_REGULATION.RA_FLAG,");
                sql.AppendLine(" M_REGULATION.MARKS,");
            }
            //2019/05/09 TPE.Rin Add End

            //2019/02/18 TPE.Sugimoto Add Sta
            if (TableInformation.TABLE_NAME == "M_LOCATION")
            {
                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第一類') HAZARD_CHECK1, "); //2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第二類') HAZARD_CHECK2, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第三類') HAZARD_CHECK3, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第四類') HAZARD_CHECK4, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第五類') HAZARD_CHECK5, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第六類') HAZARD_CHECK6, ");

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '指定可燃物') HAZARD_CHECK7     ");
            }
            //2019/02/18 TPE.Sugimoto Add End
            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            //2019/02/18 TPE.Sugimoto Add Sta
            if (TableInformation.TABLE_NAME == "M_LOCATION")
            {
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
            }
            //2019/02/18 TPE.Sugimoto Add End
            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            //if (m_COMMON.DEL_FLAG != null)
            //{
            //    sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + m_COMMON.DEL_FLAG.ToString());
            //    sql.AppendLine(" and  ");
            //}

            if (DeleteFlag != null)
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + DeleteFlag.ToString());
                sql.AppendLine(" and  ");
            }


            // キー項目の指定があればキー項目で検索します。
            // キー項目の指定がなければ検索条件で検索します。
            if (KeyValues != null)
            {
                foreach (KeyValuePair<string, string> keyValue in KeyValues)
                {
                    //2019/02/18 TPE.Sugimoto Add Sta
                    if (TableInformation.TABLE_NAME == "M_LOCATION")
                    {
                        sql.AppendLine(" M_LOCATION." + keyValue.Key + " = :" + keyValue.Key + " ");
                        sql.AppendLine(" and  ");
                        parameter.Add(keyValue.Key, keyValue.Value);
                    }
                    else
                    {
                        //2019/02/18 TPE.Sugimoto Add End
                        sql.AppendLine(" " + keyValue.Key + " = :" + keyValue.Key + " ");
                        sql.AppendLine(" and  ");
                        parameter.Add(keyValue.Key, keyValue.Value);
                    }//2019/02/18 TPE.Sugimoto Add
                }
            }
            else
            {
                foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
                {
                    //2018/08/15 TPE Add Start
                    if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                    //2018/08/15 TPE Add End

                    // 検索条件を指定します。
                    if (tableColumnInformation.IS_SEARCH && !string.IsNullOrEmpty(tableColumnInformation.InputValue?.Trim()))
                    {
                        // 文字、数値の場合は部分一致、それ以外は完全一致で検索します。
                        if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character ||
                            tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
                        {
                            sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like :" + tableColumnInformation.COLUMN_NAME + DbUtil.LikeEscapeInfoAdd());
                            sql.AppendLine(" and  ");
                            parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(tableColumnInformation.InputValue) + "%");

                        }
                        else
                        {
                            sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = :" + tableColumnInformation.COLUMN_NAME);
                            sql.AppendLine(" and  ");
                            parameter.Add(tableColumnInformation.COLUMN_NAME, tableColumnInformation.InputValue);
                        }
                    }
                }
            }


            if (m_master.mCommon.TABLE_NAME != "" && m_master.mCommon.TABLE_NAME != null && m_master.mCommon.TABLE_NAME != "0")
            {
                sql.AppendLine(" M_COMMON.TABLE_NAME  like CONCAT('%', @TABLE_NAME, '%')");
                sql.AppendLine(" and  ");
                parameter.Add("TABLE_NAME", m_master.mCommon.TABLE_NAME.Trim());
            }

            if (m_master.mCommon.KEY_VALUE != null)
            {
                sql.AppendLine(" M_COMMON.KEY_VALUE like CONCAT('%', @KEY_VALUE, '%')");
                sql.AppendLine(" and  ");
                parameter.Add("KEY_VALUE", m_master.mCommon.KEY_VALUE);
            }

            if (m_master.mCommon.DISPLAY_VALUE != null)
            {
                sql.AppendLine(" M_COMMON.DISPLAY_VALUE like CONCAT('%', @DISPLAY_VALUE, '%')");
                sql.AppendLine(" and  ");
                parameter.Add("DISPLAY_VALUE", m_master.mCommon.DISPLAY_VALUE.Trim());
            }

            if (m_master.mCommon.COMMON_ID != 0)
            {
                sql.AppendLine(" M_COMMON.COMMON_ID = @COMMON_ID");
                sql.AppendLine(" and  ");
                parameter.Add("COMMON_ID", m_master.mCommon.COMMON_ID);
            }

            sql.Length -= 8;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;

            commonData = DbUtil.Select<M_COMMON>(sql.ToString(), parameter);

            return commonData;
        }


        private static List<M_COMMON> SearchCommonMasterRecordUpdate(S_TAB_CONTROL TableInformation, List<S_TAB_COL_CONTROL> TableColumnInformations, Dictionary<string, string> KeyValues, int? DeleteFlag, int id)
        {
            List<M_COMMON> commonData = new List<M_COMMON>();
            M_COMMON m_COMMON = new M_COMMON();
            var sql = new StringBuilder();
            DynamicParameters parameter = new DynamicParameters();
            List<S_TAB_COL_CONTROL> tableColumnControls = new List<S_TAB_COL_CONTROL>();

            sql.AppendLine("select ");
            //2019/02/18 TPE.Sugimoto Add Sta
            if (TableInformation.TABLE_NAME == "M_LOCATION")
            {
                sql.AppendLine("distinct ");
            }
            //2019/02/18 TPE.Sugimoto Add End
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                //2018/08/15 TPE Add Start
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_NAME") continue;
                //2018/08/15 TPE Add End

                if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID") continue;    //2018/10/03 Rin Add

                sql.AppendLine(" " + TableInformation.TABLE_NAME + "." + tableColumnInformation.COLUMN_NAME + ",");
            }

            //2019/05/09 TPE.Rin Add Start
            if (TableInformation.TABLE_NAME == "M_REGULATION")
            {
                sql.AppendLine(" M_REGULATION.RA_FLAG,");
                sql.AppendLine(" M_REGULATION.MARKS,");
            }
            //2019/05/09 TPE.Rin Add End

            //2019/02/18 TPE.Sugimoto Add Sta
            if (TableInformation.TABLE_NAME == "M_LOCATION")
            {
                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第一類') HAZARD_CHECK1, "); //2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第二類') HAZARD_CHECK2, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第三類') HAZARD_CHECK3, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第四類') HAZARD_CHECK4, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第五類') HAZARD_CHECK5, ");//2019/06/07 TPE.Rin Mod

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '第六類') HAZARD_CHECK6, ");

                sql.Append(" (select M_LOCATION_HAZARD.HAZARD_CHECK from M_LOCATION ");
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
                sql.Append(" where M_LOCATION.LOC_ID = :LOC_ID ");
                sql.Append(" and  M_REGULATION.P_REG_TYPE_ID = (SELECT REG_TYPE_ID FROM M_REGULATION WHERE REG_TEXT = '消防法' and CLASS_ID = 3) and M_REGULATION.REG_TEXT = '指定可燃物') HAZARD_CHECK7     ");
            }
            //2019/02/18 TPE.Sugimoto Add End
            sql.Length -= 3;
            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("from ");
            sql.AppendLine(" " + TableInformation.TABLE_NAME);
            //2019/02/18 TPE.Sugimoto Add Sta
            if (TableInformation.TABLE_NAME == "M_LOCATION")
            {
                sql.Append(" left join M_LOCATION_HAZARD on M_LOCATION.LOC_ID = M_LOCATION_HAZARD.LOC_ID ");
                sql.Append(" left join M_REGULATION on M_LOCATION_HAZARD.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID ");
            }
            //2019/02/18 TPE.Sugimoto Add End
            sql.AppendLine("where ");
            // 削除フラグの指定があれば削除フラグを条件にします。
            //if (m_COMMON.DEL_FLAG != null)
            //{
            //    sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + m_COMMON.DEL_FLAG.ToString());
            //    sql.AppendLine(" and  ");
            //}

            if (DeleteFlag != null)
            {
                sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = " + DeleteFlag.ToString());
                sql.AppendLine(" and  ");
            }


            // キー項目の指定があればキー項目で検索します。
            // キー項目の指定がなければ検索条件で検索します。
            if (KeyValues != null)
            {
                foreach (KeyValuePair<string, string> keyValue in KeyValues)
                {
                    //2019/02/18 TPE.Sugimoto Add Sta
                    if (TableInformation.TABLE_NAME == "M_LOCATION")
                    {
                        sql.AppendLine(" M_LOCATION." + keyValue.Key + " = :" + keyValue.Key + " ");
                        sql.AppendLine(" and  ");
                        parameter.Add(keyValue.Key, keyValue.Value);
                    }
                    else
                    {
                        //2019/02/18 TPE.Sugimoto Add End
                        sql.AppendLine(" " + keyValue.Key + " = :" + keyValue.Key + " ");
                        sql.AppendLine(" and  ");
                        parameter.Add(keyValue.Key, keyValue.Value);
                    }//2019/02/18 TPE.Sugimoto Add
                }
            }
            else
            {
                foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
                {
                    //2018/08/15 TPE Add Start
                    if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "USER_CD") continue;
                    //2018/08/15 TPE Add End

                    // 検索条件を指定します。
                    if (tableColumnInformation.IS_SEARCH && !string.IsNullOrEmpty(tableColumnInformation.InputValue?.Trim()))
                    {
                        // 文字、数値の場合は部分一致、それ以外は完全一致で検索します。
                        if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character ||
                            tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
                        {
                            sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " like :" + tableColumnInformation.COLUMN_NAME + DbUtil.LikeEscapeInfoAdd());
                            sql.AppendLine(" and  ");
                            parameter.Add(tableColumnInformation.COLUMN_NAME, "%" + DbUtil.ConvertIntoEscapeChar(tableColumnInformation.InputValue) + "%");

                        }
                        else
                        {
                            sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + " = :" + tableColumnInformation.COLUMN_NAME);
                            sql.AppendLine(" and  ");
                            parameter.Add(tableColumnInformation.COLUMN_NAME, tableColumnInformation.InputValue);
                        }
                    }
                }
            }


            //if (m_master.mCommon.TABLE_NAME != "" && m_master.mCommon.TABLE_NAME != null && m_master.mCommon.TABLE_NAME != "0")
            //{
            //    sql.AppendLine(" M_COMMON.TABLE_NAME  like CONCAT('%', @TABLE_NAME, '%')");
            //    sql.AppendLine(" and  ");
            //    parameter.Add("TABLE_NAME", m_master.mCommon.TABLE_NAME.Trim());
            //}

            //if (m_master.mCommon.KEY_VALUE != null)
            //{
            //    sql.AppendLine(" M_COMMON.KEY_VALUE like CONCAT('%', @KEY_VALUE, '%')");
            //    sql.AppendLine(" and  ");
            //    parameter.Add("KEY_VALUE", m_master.mCommon.KEY_VALUE);
            //}

            //if (m_master.mCommon.DISPLAY_VALUE != null)
            //{
            //    sql.AppendLine(" M_COMMON.DISPLAY_VALUE like CONCAT('%', @DISPLAY_VALUE, '%')");
            //    sql.AppendLine(" and  ");
            //    parameter.Add("DISPLAY_VALUE", m_master.mCommon.DISPLAY_VALUE.Trim());
            //}

            if (id != 0)
            {
                sql.AppendLine(" M_COMMON.COMMON_ID = @COMMON_ID");
                sql.AppendLine(" and  ");
                parameter.Add("COMMON_ID", id);
            }

            sql.Length -= 8;

            sql.Append(" " + Environment.NewLine);

            sql.AppendLine("order by ");
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {
                if (tableColumnInformation.IS_KEY) sql.AppendLine(" " + tableColumnInformation.COLUMN_NAME + ",");
            }
            sql.Length -= 3;

            commonData = DbUtil.Select<M_COMMON>(sql.ToString(), parameter);

            return commonData;
        }

        /// <summary>
        /// テーブル項目管理情報から<see cref="DataTable"/>のコード情報などを名称に変換した列を追加します。
        /// </summary>
        /// <param name="TableColumnInformations">テーブル項目管理情報を指定します。</param>
        /// <param name="ConvertTable">変換列を追加する対象のテーブルを指定します。</param>
        public static List<M_REGULATION> ConvertRegulationMaster(List<S_TAB_COL_CONTROL> TableColumnInformations, List<M_REGULATION> ConvertTable)
        {
            // newConvertTable = ConvertTable;
            foreach (S_TAB_COL_CONTROL tableColumnInformation in TableColumnInformations)
            {

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "UNITSIZE_ID")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].UNITSIZE_ID),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].UNITSIZE_NAME = value;

                    }

                    // newConvertTable = ConvertTable;
                }
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "CLASS_ID")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].CLASS_ID),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].CLASSIFICATION = value;

                    }

                    // newConvertTable = ConvertTable;
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_ICON_PATH")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].REG_ICON_PATH),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].REG_ICON_NAME = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "KEY_MGMT")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].KEY_MGMT),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].KEY_MGMT_NAME = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "WEIGHT_MGMT")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].WEIGHT_MGMT),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].WEIGHT_MGMT_NAME = value;

                        if (ConvertTable[i].RA_FLAG == "1")
                        {
                            ConvertTable[i].RA_FLAG = SystemConst.RaCheckedString;
                            ConvertTable[i].RA_FLAG2 = true;
                        }
                        else
                        {
                            ConvertTable[i].RA_FLAG = SystemConst.RaNotCheckString;
                            ConvertTable[i].RA_FLAG2 = false;
                        }

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "WORKTIME_MGMT")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].WORKTIME_MGMT),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].WORKTIME_MGMT_NAME = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "P_REG_TYPE_ID")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].P_REG_TYPE_ID),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].P_REG_TYPE_VALUE = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "EXPOSURE_REPORT")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].EXPOSURE_REPORT),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].EXPOSURE_REPORT_VALUE = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Check && tableColumnInformation.COLUMN_NAME == "EXAMINATION_FLAG")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        //string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        //tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].EXAMINATION_FLAG_VALUE), out value);
                        //if (Convert.ToString(ConvertTable[i].EXAMINATION_FLAG_VALUE) != "false")
                        //{
                        //    ConvertTable[i].EXAMINATION_FLAG = SystemConst.ExaCheckedString;
                        //}else
                        //{
                        //    ConvertTable[i].EXAMINATION_FLAG = SystemConst.ExaNotCheckString;
                        //}


                        if (ConvertTable[i].EXAMINATION_FLAG == "1")
                        {
                            ConvertTable[i].EXAMINATION_FLAG = SystemConst.ExaCheckedString;
                            ConvertTable[i].EXAMINATION_FLAG_VALUE = true;
                        }
                        else
                        {
                            ConvertTable[i].EXAMINATION_FLAG = SystemConst.ExaNotCheckString;
                            ConvertTable[i].EXAMINATION_FLAG_VALUE = false;
                        }



                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        // ConvertTable[i].EXAMINATION_FLAG = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Check && tableColumnInformation.COLUMN_NAME == "MEASUREMENT_FLAG")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        //tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].MEASUREMENT_FLAG_VALUE),
                        //                                                      out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        //ConvertTable[i].MEASUREMENT_FLAG = value;


                        if (ConvertTable[i].MEASUREMENT_FLAG == "1")
                        {
                            ConvertTable[i].MEASUREMENT_FLAG = SystemConst.MeaCheckedString;
                            ConvertTable[i].MEASUREMENT_FLAG_VALUE = true;
                        }
                        else
                        {
                            ConvertTable[i].MEASUREMENT_FLAG = SystemConst.MEANotCheckString;
                            ConvertTable[i].MEASUREMENT_FLAG_VALUE = false;
                        }

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Check && tableColumnInformation.COLUMN_NAME == "RA_FLAG")
                {


                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        //tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].MEASUREMENT_FLAG_VALUE),
                        //                                                      out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        //ConvertTable[i].MEASUREMENT_FLAG = value;


                        if (Convert.ToString(ConvertTable[i].RA_FLAG2) != "false")
                        {
                            ConvertTable[i].RA_FLAG = SystemConst.RaCheckedString;
                        }
                        else
                        {
                            ConvertTable[i].RA_FLAG = SystemConst.RaNotCheckString;
                        }

                    }

                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_USER_CD")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].REG_USER_CD),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].REG_USER_NAME = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "UPD_USER_CD")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].UPD_USER_CD),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].UPD_USER_NAME = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "DEL_USER_CD")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].DEL_USER_CD),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].DEL_USER_NAME = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Check && tableColumnInformation.COLUMN_NAME == "DEL_FLAG")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。

                        if (ConvertTable[i].DEL_FLAG == 1)
                        {
                            //ConvertTable[i].DEL_FLAG2 = SystemConst.RaCheckedString;
                            ConvertTable[i].DEL_FLG = true;
                        }
                        else
                        {
                            //ConvertTable[i].DEL_FLAG2 = SystemConst.RaNotCheckString;
                            ConvertTable[i].DEL_FLG = false;
                        }


                        //tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].DEL_FLAG), out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        //ConvertTable[i].DEL_FLAG_VALUE = value;

                    }
                }

                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select && tableColumnInformation.COLUMN_NAME == "REG_TYPE_ID")
                {
                    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                    for (int i = 0; i < ConvertTable.Count; i++)
                    {
                        string value;

                        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                        //{
                        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //    continue;
                        //}

                        // 選択の場合はコードを名称に変換します。
                        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].REG_TYPE_ID),
                                                                              out value);
                        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                        //newConvertTable[i].P_GROUP_NAME = "dummy";
                        ConvertTable[i].P_REG_TYPE_VALUE = value;

                    }
                }

                //if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.SELECT && tableColumnInformation.COLUMN_NAME == "REG_TYPE_ID")
                //{
                //    // ConvertTable.Columns.Add(SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME);
                //    for (int i = 0; i < ConvertTable.Count; i++)
                //    {
                //        string value;

                //        //Select型はコンバートしないとCSVにでない。ただ、P_LOC_BASEIDはコンバート前のデータを出したいので下記処理とする。
                //        //if (tableColumnInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "P_LOC_BASEID")
                //        //{
                //        //    value = Convert.ToString(ConvertTable[i].P_GROUP_CD);
                //        //    ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //    continue;
                //        //}

                //        // 選択の場合はコードを名称に変換します。
                //        tableColumnInformation.ConvertData.TryGetValueNullFix(Convert.ToString(ConvertTable[i].DEL_FLAG_VALUE),
                //                                                              out value);
                //        //  ConvertTable.Rows[i][SystemConst.ConvertColumnPrefix + tableColumnInformation.COLUMN_NAME] = value;
                //        //newConvertTable[i].P_GROUP_NAME = "dummy";
                //        ConvertTable[i].DEL_FLAG_VALUE = value;

                //    }
                //}


                // newConvertTable = ConvertTable;
            }
            //else
            //{
            //    continue;
            //}
            return ConvertTable;
        }
    }
}