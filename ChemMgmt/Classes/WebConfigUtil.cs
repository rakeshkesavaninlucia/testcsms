﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace ZyCoaG.Util
{

    /// <summary>
    /// WebConfigの操作Utilクラスです。
    /// </summary>
    public class WebConfigUtil
    {
        #region 定数
        // WebConfig内のタグの種類を表すIndex
        private const int CtagIndexConnectionString = 0;

        //cTAGINDEX_CONNECTION_STRING

        private const int CtagIndexAppSettings = 1;

        //cTAGINDEX_APP_SETTINGS
        public const string CoutErrorLogMsgFlagOff = "0";

        //cOUT_ERRLOGMSG_FLG_OFF

        public const string CoutErrorLogMsgFlagOn = "1";
        //cOUT_ERRLOGMSG_FLG_ON
        #endregion

        #region Property
        /// <summary>
        /// SqlServerConnectionString
        /// </summary>
        public static string SqlServerConnectionString  /*SQLSERVER_CONNECTION_STRING*/
        {
            get { return GetWebConfig("SqlServerConnectionString", CtagIndexConnectionString).ToString(); }
        }
        /// <summary>
        /// OracleConnectionString
        /// </summary>
        public static string OracleConnectionString /*ORACLE_CONNECTION_STRING*/
        {
            get
            {
                return GetWebConfig("OracleConnectionString", CtagIndexConnectionString).ToString();
            }
        }
        /// <summary>
        /// データベースの種類
        /// </summary>
        public static string  DatabaseType           /*DATABASE_TYPE*/
        {
            get { return GetWebConfig("DatabaseType", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// グリッドのテンプレート格納フォルダ
        /// </summary>
        public static string GridTemplateFolder       /*GRID_TEMPLATE_FOLDER*/
        {
            get { return GetWebConfig("GridTemplateFolder", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// 試薬管理重量管理フラグ 0:重量管理なし 1:重量管理あり
        /// </summary>
        /// <remarks>
        ///  左から初回、貸出、返却、廃棄、使切
        /// </remarks>
        public static string ManagementWeight   /*MANAGE_WEIGHT*/
        {
            get
            {
                return GetWebConfig("MANAGE_WEIGHT", CtagIndexAppSettings).ToString();
            }
        }
        /// <summary>
        /// LDAP：接続先
        /// </summary>
        public static string LDAPConnectionString /*LDAP_CONNECTION_STRING*/
        {
            get { return GetWebConfig("LDAPConnectionString", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// LDAP：接続文字列
        /// </summary>
        public static string UserBaseDNString  /*USER_BASEDN_STRING*/
        {
            get { return GetWebConfig("UserBaseDNString", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// LDAP：認証方式
        /// </summary>
        public static string AuthenticationType     /*AUTHENTICATION_TYPE*/
        {
            get { return GetWebConfig("authenticationType", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// 検索確認件数：購入試薬検索
        /// </summary>
        public static long AlertResultCountCatalog    /*ALERT_RESULT_COUNT_CATALOG*/
        {
            get { return Convert.ToInt64(GetWebConfig("ALERT_RESULT_COUNT_CATALOG", CtagIndexAppSettings)); }
        }
        /// <summary>
        /// 検索確認件数：在庫検索
        /// </summary>
        public static long AlertResultCountStock            /*ALERT_RESULT_COUNT_STOCK*/
        {
            get { return Convert.ToInt64(GetWebConfig("ALERT_RESULT_COUNT_STOCK", CtagIndexAppSettings)); }
        }
        /// <summary>
        /// 検索確認件数：発注履歴検索
        /// </summary>
        public static long AlertResultCountHistory       /*ALERT_RESULT_COUNT_HISTORY*/
        {
            get { return Convert.ToInt64(GetWebConfig("ALERT_RESULT_COUNT_HISTORY", CtagIndexAppSettings)); }
        }
        /// <summary>
        /// 検索最大件数：購入試薬検索
        /// </summary>
        public static long LimitResultCountCatalog      /*LIMIT_RESULT_COUNT_CATALOG*/
        {
            get { return Convert.ToInt64(GetWebConfig("LIMIT_RESULT_COUNT_CATALOG", CtagIndexAppSettings)); }
        }
        /// <summary>
        /// 検索最大件数：在庫検索
        /// </summary>
        public static long LimitResultCountStock            /*LIMIT_RESULT_COUNT_STOCK*/
        {
            get { return Convert.ToInt64(GetWebConfig("LIMIT_RESULT_COUNT_STOCK", CtagIndexAppSettings)); }
        }
        /// <summary>
        /// 検索最大件数：発注履歴検索
        /// </summary>
        public static long LimitResultCountHistory                                  /*LIMIT_RESULT_COUNT_HISTORY*/
        {
            get { return Convert.ToInt64(GetWebConfig("LIMIT_RESULT_COUNT_HISTORY", CtagIndexAppSettings)); }
        }
        /// <summary>
        /// 検索確認ダイアログ表示あり・なし
        /// </summary>
        public static string AlertResultDialog                            /*ALERT_RESULT_DIALOG*/
        {
            get { return GetWebConfig("ALERT_RESULT_DIALOG", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// 上限到達検索確認ダイアログ表示あり・なし
        /// </summary>
        public static string LimitResultDialog                /*LIMIT_RESULT_DIALOG*/
        {
            get { return GetWebConfig("LIMIT_RESULT_DIALOG", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// メール：タイトル（共通）
        /// </summary>
        public static string MailSub                        /*MAIL_SUB*/
        {
            get { return GetWebConfig("MAIL_SUB", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// メール：タイトル（ショッピングカート）
        /// </summary>
        public static string MAIL_SUB_ZC02101
        {
            get { return GetWebConfig("MAIL_SUB_ZC02101", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// メール：本文存在パス
        /// </summary>
        public static string MailTxtPath                  /*MAIL_TXT_PATH*/
        {
            get { return GetWebConfig("MAIL_TXT_PATH", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// メール：本文存在パス（ショッピングカート)
        /// </summary>
        public static string MAIL_BODY_ZC02101
        {
            get { return GetWebConfig("MAIL_BODY_ZC02101", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// UTF8(html)
        /// </summary>
        public static string CodePageUTF8                   /*CODEPAGE_UTF8*/
        {
            get { return GetWebConfig("CODEPAGE_UTF8", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// SJIS
        /// </summary>
        public static string CodePageSJIS                          /*CODEPAGE_SJIS*/
        {
            get { return GetWebConfig("CODEPAGE_SJIS", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// 初期値：指図コード
        /// </summary>
        public static string ProjectNameInitText       /*PROJECT_NAME_INIT_TEXT*/
        {
            get { return GetWebConfig("PROJECT_NAME_INIT_TEXT", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// 勘定科目名1：受領画面
        /// </summary>
        public static string KanjyoName1                             /*KANJYO_NAME1*/
        {
            get { return GetWebConfig("KANJYO_NAME1", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// 勘定科目名2：受領画面
        /// </summary>
        public static string KanjyoName2                           /*KANJYO_NAME2*/
        {
            get { return GetWebConfig("KANJYO_NAME2", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// メーカー名：その他
        /// </summary>
        public static string MakerNameOthers                              /*MAKER_NAME_OTHERS*/
        {
            get { return GetWebConfig("MAKER_NAME_OTHERS", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// 発注代理店：その他
        /// </summary>
        public static string VENDOR_ID_ETC
        {
            get { return GetWebConfig("VENDOR_ID_ETC", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// 通貨の半角、全角変換（：で区切った左側が半角、右側が全角とする）
        /// </summary>
        public static string CONV_CURR
        {
            get { return GetWebConfig("CONV_CURR", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// CRAISチェッカー：設定レベル
        /// </summary>
        public static string  CraisCheckLevel               /*CRAIS_CHECKLEVEL*/
        {
            get { return GetWebConfig("CRAIS_CHECKLEVEL", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// CRAISチェッカー：APP名
        /// </summary>
        public static string  CraisAppName                                 /*CRAIS_APPNAME*/
        {
            get { return GetWebConfig("CRAIS_APPNAME", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// CRAISチェッカー：キー名
        /// </summary>
        public static string   CraisKeyField                                          /*CRAIS_KEYFIELD*/
        {
            get { return GetWebConfig("CRAIS_KEYFIELD", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// CRAISチェッカー：URL
        /// </summary>
        public static string CraisUrl
        {
            get { return GetWebConfig("CRAIS_URL", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// 子画面プログラムID
        /// </summary>
        public static string ChildProgramId                                     /*CHILD_PROGRAM_ID*/
        {
            get { return GetWebConfig("CHILD_PROGRAM_ID", CtagIndexAppSettings).ToString(); }
        }
        /// <summary>
        /// エラーログメッセージ出力フラグ。このフラグによって出力する、しないを決定するメッセージのみ対応。
        /// </summary>
        public static string OutErrLogMsgFlag   /*OUT_ERRLOGMSG_FLG*/
        {
            get { return GetWebConfig("OUT_ERRLOGMSG_FLG", CtagIndexAppSettings).ToString(); }
        }

        /// <summary>
        /// 消防法の計算用の閾値。
        /// </summary>
        public static string  FireCheckThreshold                                     /*FIRE_CHECK_THRESHOLD*/
        {
            get { return GetWebConfig("FIRE_CHECK_THRESHOLD", CtagIndexAppSettings).ToString(); }
        }
        #endregion

        #region Method
        /// <summary>
        /// WebConfigから指定した項目の値を返却します。
        /// </summary>
        /// <param name="itemName">項目名</param>
        /// <param name="tagIndex"> WebConfig内のタグの種類</param>
        /// <returns>取得した値</returns>
        private static object GetWebConfig(string itemName, int tagIndex)
        {
            object ret = null;

            switch (tagIndex)
            {
                case CtagIndexConnectionString:
                    ret = ConfigurationManager.ConnectionStrings[itemName];
                    break;
                case CtagIndexAppSettings:
                    ret = ConfigurationManager.AppSettings[itemName];
                    break;
            }

            if (ret == null)
            {
                ret = "";
            }

            return ret;
        }
        #endregion
    }
}