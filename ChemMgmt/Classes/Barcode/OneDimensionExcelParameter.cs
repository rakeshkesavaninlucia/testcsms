﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using ZXing;

namespace ZyCoaG.Barcode
{
    /// <summary>
    /// 1次元バーコードのExcel出力情報を指定します。
    /// </summary>
    public class OneDimensionExcelParameter : ExcelParameter
    {
        /// <summary>
        /// <see cref="OneDimensionExcelParameter"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        public OneDimensionExcelParameter()
        {
            StartRow = 1;
            RowInterval = 4;
            BarcodeRow = 3;
            BarcodeOutputColumn1 = 1;
            BarcodeOutputColumn2 = 20;
            TextOutputColumn1 = 8;
            TextOutputColumn2 = 27;
            MaxRow = 58;
        }
    }
}