﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZXing;

namespace ZyCoaG.Barcode
{
    /// <summary>
    /// 1次元バーコード情報を指定します。
    /// </summary>
    public class OneDimensionBarcodeParameter : BarcodeParameter
    {
        /// <summary>
        /// <see cref="OneDimensionBarcodeParameter"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        public OneDimensionBarcodeParameter()
        {
            Height = 40;
            Width = 200;
            Margin = 10;
            IsText = true;
            Format = BarcodeFormat.CODE_128;
        }
    }
}