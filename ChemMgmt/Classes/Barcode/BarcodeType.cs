﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG.Barcode
{
    /// <summary>
    /// バーコードの種類を定義します。
    /// </summary>
    public enum BarcodeType
    {
        /// <summary>
        /// 使用しない
        /// </summary>
        DoNotUse = 0,
        /// <summary>
        /// 1次元バーコード
        /// </summary>
        OneDimension = 1,
        /// <summary>
        /// 2次元バーコード
        /// </summary>
        TwoDimension = 2
    }
}