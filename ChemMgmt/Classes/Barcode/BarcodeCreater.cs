﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using ZXing;
using System.IO;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;

namespace ZyCoaG.Barcode
{
    /// <summary>
    /// バーコード作成機能を提供するクラスです。
    /// </summary>
    public class BarcodeCreater : IDisposable
    {
        /// <summary>
        /// バーコード作成用
        /// </summary>
        private BarcodeWriter BarcodeWriter { get; set; }

        /// <summary>
        /// バーコード種類
        /// </summary>
        private BarcodeType BarcodeType { get; set; }

        /// <summary>
        /// バーコード作成用パラメーター実体
        /// </summary>
        /// 
        private BarcodeParameter barcodeParameter;
        /// <summary>
        /// バーコード作成用パラメーター
        /// </summary>
        public BarcodeParameter BarcodeParameter
        {
            get
            {
                return barcodeParameter;
            }
            set
            {
                barcodeParameter = value;

                BarcodeWriter = new BarcodeWriter();

                BarcodeWriter.Format = barcodeParameter.Format;

                BarcodeWriter.Options.Height = barcodeParameter.Height;
                BarcodeWriter.Options.Width = barcodeParameter.Width;

                BarcodeWriter.Options.Margin = barcodeParameter.Margin;

                BarcodeWriter.Options.PureBarcode = !barcodeParameter.IsText;
            }
        }

        /// <summary>
        /// Excel出力用パラメーター
        /// </summary>
        public ExcelParameter ExcelParameter { get; set; }

        /// <summary>
        /// <see cref="Barcode"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        /// <param name="barcodeType">バーコードの種類を<see cref="BarcodeType"/>から指定します。</param>
        public BarcodeCreater(BarcodeType barcodeType)
        {
            BarcodeType = barcodeType;
            switch (barcodeType)
            {
                case (BarcodeType.OneDimension):
                    BarcodeParameter = new OneDimensionBarcodeParameter();
                    ExcelParameter = new OneDimensionExcelParameter();
                    break;
                case (BarcodeType.TwoDimension):
                    BarcodeParameter = new TwoDimensionBarcodeParameter();
                    ExcelParameter = new TwoDimensionExcelParameter();
                    break;
            }
        }

        /// <summary>
        /// <paramref name="Code"/>で指定されたバーコードを<see cref="Bitmap"/>形式で作成します。
        /// </summary>
        /// <param name="Code">バーコードを作成するコードを指定します。</param>
        /// <returns><paramref name="Bitmap"/>形式のバーコードを返します。</returns>
        public Bitmap CreateToBitmap(string code)
        {
            if (BarcodeType == BarcodeType.DoNotUse) throw new ApplicationException();

            return BarcodeWriter.Write(code);
        }

        /// <summary>
        /// <paramref name="Code"/>で指定されたバーコードの<see cref="Bitmap"/>ファイルを作成します。
        /// </summary>
        /// <param name="Code">バーコードを作成するコードを指定します。</param>
        /// <returns>作成されたファイルのパスを返します。</returns>
        public string CreateToBitmapFile(string code)
        {
            if (BarcodeType == BarcodeType.DoNotUse) throw new ApplicationException();

            using (var Bitmap = BarcodeWriter.Write(code))
            {
                var filePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                filePath += ".bmp";
                Bitmap.Save(filePath);
                return filePath;
            }
        }

        /// <summary>
        /// バーコードExcelファイルを作成します。
        /// </summary>
        /// <param name="Code">バーコードを作成するコードを指定します。</param>
        /// <returns>作成されたExcelファイルのパスを返します。</returns>
        public string CreateToExcel(string code)
        {
            if (BarcodeType == BarcodeType.DoNotUse) throw new ApplicationException();

            var codeList = new List<string>();
            codeList.Add(code);
            return CreateToExcel(codeList);
        }

        /// <summary>
        /// 複数のバーコードExcelファイルを作成します。
        /// </summary>
        /// <param name="CodeList">バーコードを作成するコードを<see cref="IEnumerable{string}"/>で指定します。</param>
        /// <returns>作成されたExcelファイルのパスを返します。</returns>
        public string CreateToExcel(IEnumerable<string> codeList)
        {
            if (BarcodeType == BarcodeType.DoNotUse) throw new ApplicationException();

            var workBook = new XSSFWorkbook();
            var workSheet = workBook.CreateSheet("Barcode");
            {
                for (var i = 0; i < 128; i++)
                {
                    workSheet.SetColumnWidth(i, 576);
                }
            }

            var outputRow = ExcelParameter.StartRow;
            var barcodeOutputCol = ExcelParameter.BarcodeOutputColumn1;
            var dataOutputCol = ExcelParameter.TextOutputColumn1;
            var pageCount = 1;
            foreach(string code in codeList)
            {
                // バーコードファイルを作成します。
                var filePath = CreateToBitmapFile(code);

                // バーコードをExcelに展開します。
                byte[] pictureBytes;
                using (var pictureRead = new FileStream(filePath, FileMode.Open, FileAccess.Read))
                {
                    pictureBytes = new byte[pictureRead.Length];
                    pictureRead.Read(pictureBytes, 0, pictureBytes.Length);
                }
                var pictureIndex = workBook.AddPicture(pictureBytes, PictureType.BMP);
                var helper = workBook.GetCreationHelper();
                var patriarch = workSheet.CreateDrawingPatriarch();
                var anchor = helper.CreateClientAnchor();
                anchor.AnchorType = AnchorType.MoveDontResize;
                anchor.Col1 = barcodeOutputCol;
                anchor.Row1 = outputRow;
                var picture = patriarch.CreatePicture(anchor, pictureIndex);
                // 1次元バーコードと2次元バーコードで画像サイズを調整します。
                if(BarcodeType == BarcodeType.OneDimension)
                {
                    picture.Resize(11.7, 2.2);
                }
                else
                {
                    picture.Resize(6.6, 6.6);
                }

                var dataRow = workSheet.GetRow(outputRow);
                if (dataRow == null) dataRow = workSheet.CreateRow(outputRow);
                var dataCell = dataRow.CreateCell(dataOutputCol);
                // 1次元バーコードと2次元バーコードで付随データ位置を調整します。
                if (BarcodeType == BarcodeType.TwoDimension)
                {
                    dataCell.SetCellValue(code);
                }

                // 次のバーコード出力位置を計算します。
                if (barcodeOutputCol == ExcelParameter.BarcodeOutputColumn1)
                {
                    barcodeOutputCol = ExcelParameter.BarcodeOutputColumn2;
                    dataOutputCol = ExcelParameter.TextOutputColumn2;
                }
                else
                {
                    barcodeOutputCol = ExcelParameter.BarcodeOutputColumn1;
                    dataOutputCol = ExcelParameter.TextOutputColumn1;
                    outputRow += ExcelParameter.RowInterval;
                    if (ExcelParameter.MaxRow < (outputRow + ExcelParameter.BarcodeRow) - (pageCount - 1 ) * ExcelParameter.MaxRow)
                    {
                        outputRow = pageCount * ExcelParameter.MaxRow + ExcelParameter.StartRow + 1;
                        pageCount += 1;
                    }
                }
            }

            // ファイルを保存します。
            var excelFilePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            using (var fileStream = new FileStream(excelFilePath, FileMode.OpenOrCreate, FileAccess.Write))
            {
                workBook.Write(fileStream);
            }

            workSheet = null;
            workBook = null;

            return excelFilePath;
        }

        #region IDisposable Support
        private bool disposedValue = false; // 重複する呼び出しを検出するには

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: マネージ状態を破棄します (マネージ オブジェクト)。
                }

                // TODO: アンマネージ リソース (アンマネージ オブジェクト) を解放し、下のファイナライザーをオーバーライドします。
                // TODO: 大きなフィールドを null に設定します。
                BarcodeWriter = null;
                barcodeParameter = null;
                ExcelParameter = null;

                disposedValue = true;
            }
        }

        // TODO: 上の Dispose(bool disposing) にアンマネージ リソースを解放するコードが含まれる場合にのみ、ファイナライザーをオーバーライドします。
        // ~Barcode() {
        //   // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
        //   Dispose(false);
        // }

        // このコードは、破棄可能なパターンを正しく実装できるように追加されました。
        public void Dispose()
        {
            // このコードを変更しないでください。クリーンアップ コードを上の Dispose(bool disposing) に記述します。
            Dispose(true);
            // TODO: 上のファイナライザーがオーバーライドされる場合は、次の行のコメントを解除してください。
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}