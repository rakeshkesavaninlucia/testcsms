﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZXing;

namespace ZyCoaG.Barcode
{
    /// <summary>
    /// Excel出力用パラメーター
    /// </summary>
    public class ExcelParameter
    {
        /// <summary>
        /// バーコード出力初期行
        /// </summary>
        public int StartRow { get; set; }

        /// <summary>
        /// 次行に出力する間隔
        /// </summary>
        public int RowInterval { get; set; }

        /// <summary>
        /// バーコード出力に必要な行数
        /// </summary>
        public int BarcodeRow { get; set; }

        /// <summary>
        /// バーコード出力列位置1
        /// </summary>
        public int BarcodeOutputColumn1 { get; set; }

        /// <summary>
        /// バーコード出力列位置2
        /// </summary>
        public int BarcodeOutputColumn2 { get; set; }

        /// <summary>
        /// 1ページの最大行数
        /// </summary>
        public int MaxRow { get; set; }

        /// <summary>
        /// テキスト出力列位置1
        /// </summary>
        public int TextOutputColumn1 { get; set; }

        /// <summary>
        /// テキスト出力列位置2
        /// </summary>
        public int TextOutputColumn2 { get; set; }
    }
}