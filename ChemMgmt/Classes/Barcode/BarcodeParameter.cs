﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZXing;

namespace ZyCoaG.Barcode
{
    /// <summary>
    /// バーコード作成用パラメーター
    /// </summary>
    public class BarcodeParameter
    {
        /// <summary>
        /// 高さ
        /// </summary>
        public int Height { get; set; }

        /// <summary>
        /// 幅
        /// </summary>
        public int Width { get; set; }

        /// <summary>
        /// 余白
        /// </summary>
        public int Margin { get; set; }

        /// <summary>
        /// テキストを出力するか
        /// </summary>
        public bool IsText { get; set; }

        /// <summary>
        /// バーコードフォーマット
        /// </summary>
        public BarcodeFormat Format { get; set; }
    }
}