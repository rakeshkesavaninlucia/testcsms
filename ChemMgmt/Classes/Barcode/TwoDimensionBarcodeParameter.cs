﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZXing;

namespace ZyCoaG.Barcode
{
    /// <summary>
    /// 2次元バーコード情報を指定します。
    /// </summary>
    public class TwoDimensionBarcodeParameter : BarcodeParameter
    {
        /// <summary>
        /// <see cref="TwoDimensionBarcodeParameter"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        public TwoDimensionBarcodeParameter()
        {
            Height = 120;
            Width = 120;
            Margin = 5;
            IsText = true;
            Format = BarcodeFormat.QR_CODE;
        }
    }
}