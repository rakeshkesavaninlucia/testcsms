﻿using ChemMgmt.Classes;
using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ZyCoaG
{
    /// <summary>
    /// 操作種類情報を取得するクラスです。
    /// </summary>
    public class ActionTypeMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        /// <summary>
        /// データを全て取得します。
        /// </summary>
        /// <returns>データを<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" M_ACTION_TYPE.ACTION_TYPE_ID,");
            sql.AppendLine(" M_ACTION_TYPE.ACTION_TEXT,");
            sql.AppendLine(" M_ACTION_TYPE.DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" M_ACTION_TYPE");

            var parameters = new Hashtable();

            DbContext context = null;
            context = DbUtil.Open(true);
            DataTable records = DbUtil.Select(context, sql.ToString(), parameters);
            context.Close();

            var actions = new List<CommonDataModel<int?>>();
            foreach (DataRow record in records.Rows)
            {
                var action = new CommonDataModel<int?>();
                action.Id = OrgConvert.ToNullableInt32(record["ACTION_TYPE_ID"].ToString());
                action.Name = record["ACTION_TEXT"].ToString();
                action.IsDeleted = Convert.ToInt32(record["DEL_FLAG"].ToString()).ToBool();
                actions.Add(action);
            }
            return actions;
        }
    }
}
