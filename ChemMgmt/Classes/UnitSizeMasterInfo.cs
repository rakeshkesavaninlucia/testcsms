﻿using ChemMgmt.Models;
using COE.Common;
using COE.Common.BaseExtension;
using Dapper;
using System;
using System.Collections.Generic;
using System.Text;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Classes
{
    public class UnitSizeMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        /// <summary>
        /// データを全て取得します。
        /// </summary>
        /// <returns>データを<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" M_UNITSIZE.UNITSIZE_ID,");
            sql.AppendLine(" M_UNITSIZE.UNIT_NAME,");
            sql.AppendLine(" M_UNITSIZE.DEL_FLAG");
            sql.AppendLine("from");
            sql.AppendLine(" M_UNITSIZE");

            var parameters = new DynamicParameters();

            DbContext context = null;
            context = DbUtil.Open(true);
            List<M_UNITSIZE> records = DbUtil.Select<M_UNITSIZE>(sql.ToString(), parameters);
            context.Close();

            var unitSize = new List<CommonDataModel<int?>>();
            foreach (var record in records)
            {
                var data = new CommonDataModel<int?>();
                data.Id = OrgConvert.ToNullableInt32(record.UNITSIZE_ID.ToString());
                data.Name = record.UNIT_NAME.ToString();
                data.IsDeleted = Convert.ToInt32(record.DEL_FLAG.ToString()).ToBool();  
                unitSize.Add(data);
            }
            return unitSize;
        }

    }
}