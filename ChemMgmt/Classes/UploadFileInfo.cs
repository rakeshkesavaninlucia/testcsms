﻿using System.IO;

namespace ZyCoaG.Classes
{
    public class UploadFileInfo
    {
        public string OriginalFileName { get; }

        public FileInfo File { get; }

        public UploadFileInfo(string fileName)
        {
            File = new FileInfo(fileName);
            OriginalFileName = File.Name;
        }

        public UploadFileInfo(string fileName, string originalFileName)
        {
            File = new FileInfo(fileName);
            OriginalFileName = originalFileName;
        }
    }
}