﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Chem;
using ChemMgmt.Nikon.Models;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.BaseCommon;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.Stock;
using ZyCoaG.util;
using ZyCoaG.Util;

namespace ChemMgmt.Repository
{
    public class InventoryDao : IInventoryRepo
    {

        private UserInfo.ClsUser _clsUser = new UserInfo.ClsUser();      
        public List<InventoryManagement> getStock(InventoryManagement _clsInputSearchStock, int? maxResultCount = null)
        {
            _clsUser.USER_CD = Common.GetLogonForm().Id;
            _clsUser.REFERENCE_AUTHORITY = Common.GetLogonForm().ADMIN_FLAG;
            string strSQL = "";
            string strWhere = "";

            List<InventoryManagement> dttable = new List<InventoryManagement>();
            DynamicParameters _params = new DynamicParameters();        
            try
            {               
                makeWhereConditionsStock(_clsInputSearchStock, ref strWhere, ref _params);
                strSQL = "select ";
                if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer && maxResultCount.HasValue)
                {
                    strSQL += " top(" + maxResultCount.ToString() + ") ";
                }
                strSQL += "  s.STOCK_ID,c.CHEM_NAME PRODUCT_NAME,m.MAKER_ID,m.MAKER_NAME,v.VENDOR_ID,v.VENDOR_NAME,s.CAT_CD,c.CAS_NO,format(s.UNITSIZE,'0.#########') + us.UNIT_NAME DISPLAY_SIZE ";               
                if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
                {
                    strSQL += " ,format(s.UNITSIZE,'0.#########') UNITSIZE ";
                }
                strSQL += " ,s.UNITSIZE_ID,us.UNIT_NAME,s.GRADE ";
                if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
                {
                    strSQL += " ,(case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (convert(nvarchar,s.CUR_GROSS_WEIGHT_G - IsNull(s.BOTTLE_WEIGHT_G, 0)) + 'g') end) CONTENT_WEIGHT ,(case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (convert(nvarchar,s.CUR_GROSS_WEIGHT_G - IsNull(s.BOTTLE_WEIGHT_G, 0))) end) CONTENT_WEIGHT_VOLUME,(case when s.CUR_GROSS_WEIGHT_G is NULL then '' else 'g' end) CONTENT_WEIGHT_UNITSIZE,convert(nvarchar,s.DELI_DATE,111) DELI_DATE ";                 
                }
                strSQL += " ,s.ORDER_NO,s.BARCODE,gc.GHSCAT_NAME GHSCAT,r.REG_TEXT REGULATORY,cf.FIRECAT_NAME FIRE_NAME,co.OFFICECAT_NAME OFFICE_NAME,o.PRICE,u.USER_NAME ORDER_USER,o.ORDER_USER_CD,g.GROUP_NAME,v.VENDOR_NAME,k.KANJYO_NAME,st.STATUS_TEXT STATUS_NAME,sl.LOC_NAME STORING_LOC_NAME ";
                strSQL += "from  T_STOCK s left outer join T_ORDER o on s.ORDER_NO = o.ORDER_NO left outer join M_CHEM c on s.CAT_CD = c.CHEM_CD left outer join  M_MAKER m on s.MAKER_ID = m.MAKER_ID left outer join M_USER u on o.ORDER_USER_CD = u.USER_CD left outer join M_UNITSIZE us on s.UNITSIZE_ID = us.UNITSIZE_ID ";
                strSQL += " left outer join M_GROUP g on u.GROUP_CD = g.GROUP_CD left outer join M_VENDOR v on o.VENDOR_ID = v.VENDOR_ID left outer join M_KANJYO k on o.KANJYO_ID = k.KANJYO_ID left outer join ";
                strSQL += " V_CHEM_GHSCAT gc on s.CAT_CD = gc.CHEM_CD left outer join V_CHEM_REGULATION r on s.CAT_CD = r.CHEM_CD left outer join V_CHEM_FIRE cf on s.CAT_CD = cf.CHEM_CD left outer join V_CHEM_OFFICE co on s.CAT_CD = co.CHEM_CD left outer join  M_STATUS st on s.STATUS_ID = st.STATUS_ID left outer join V_LOCATION sl on s.LOC_ID = sl.LOC_ID ";             
                if (_clsUser.REFERENCE_AUTHORITY != null && _clsUser.REFERENCE_AUTHORITY.Equals(Const.cREFERENCE_AUTHORITY_FLAG_NORMAL))
                {
                    strSQL += " left outer join T_MGMT_LEDGER ml on s.REG_NO = ml.REG_NO";
                }
                strSQL += " where s.DEL_FLAG <> 1 ";
                strSQL += strWhere;
                strSQL += " order by s.DELI_DATE desc ";
                dttable = DbUtil.Select<InventoryManagement>(strSQL, _params);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return dttable;
        }

        private void makeWhereConditionsStock(InventoryManagement _clsInputSearchStock, ref string strWhere, ref DynamicParameters _params)
        {
            //var chemStock = new ChemStockDataInfo();
            if (_clsInputSearchStock.PRODUCT_NAME != null && _clsInputSearchStock.PRODUCT_NAME.Length > 0)
            {
                var parameter = _clsInputSearchStock.PRODUCT_NAME.Trim();
                var conditionName = string.Empty;
                strWhere += " and s.CAT_CD in (select distinct(M_CHEM_ALIAS.CHEM_CD) from M_CHEM_ALIAS where";
                switch (_clsInputSearchStock.PRODUCT_NAME_TYPE)
                {
                    case Const.cSEARCH_TYPE_CONTAINS:
                        strWhere += "   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd();
                        conditionName = "部分一致";
                        break;
                    case Const.cSEARCH_TYPE_STARTSWITH:
                        strWhere += "   upper(M_CHEM_ALIAS.CHEM_NAME) like @CHEM_NAME + '%' " + DbUtil.LikeEscapeInfoAdd();
                        conditionName = "前方一致";
                        break;
                    case Const.cSEARCH_TYPE_ENDSWITH:
                        strWhere += "   upper(M_CHEM_ALIAS.CHEM_NAME) like '%' + @CHEM_NAME " + DbUtil.LikeEscapeInfoAdd();
                        conditionName = "後方一致";
                        break;
                    case Const.cSEARCH_TYPE_EQUALTO:
                        strWhere += "   upper(M_CHEM_ALIAS.CHEM_NAME) = @CHEM_NAME";
                        conditionName = "完全一致";
                        break;
                }
                strWhere += " )";
                if (_clsInputSearchStock.PRODUCT_NAME_TYPE == Const.cSEARCH_TYPE_EQUALTO)
                {
                    _params.Add("CHEM_NAME", parameter.ToUpper());
                }
                else
                {
                    _params.Add("CHEM_NAME", DbUtil.ConvertIntoEscapeChar(parameter.ToUpper()));
                }
            }

            // メーカー名
            if (_clsInputSearchStock.FAVORITES_MAKER != null && _clsInputSearchStock.FAVORITES_MAKER != "")
            {
                strWhere += " and s.MAKER_ID in (select UM.MAKER_ID from M_USER_MAKER UM where  UM.USER_CD = @USER_CD and  UM.PATTERN_NAME = @PATTERN_NAME) ";
                _params.Add("@USER_CD", this._clsUser.USER_CD);
                _params.Add("@PATTERN_NAME", _clsInputSearchStock.FAVORITES_MAKER.Trim());
            }
            else
            {
                List<M_MAKER> statuses = new MasterService().selectMakerList();
                var parameter = string.Empty;
                foreach (var row in statuses)
                {
                    if (row.MAKER_ID.ToString() == _clsInputSearchStock.MAKER_ID.ToString())
                    {
                        parameter = row.MAKER_NAME.ToString();
                    }
                }

                var conditionName = string.Empty;
                if (parameter != null && parameter.Length > 0)
                {
                    switch (_clsInputSearchStock.MAKER_NAME_TYPE)
                    {
                        case Const.cSEARCH_TYPE_CONTAINS:
                            strWhere += " and m.MAKER_NAME like '%'+@MAKER_NAME+'%' " + DbUtil.LikeEscapeInfoAdd();
                            parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                            conditionName = "部分一致";
                            break;
                        case Const.cSEARCH_TYPE_STARTSWITH:
                            strWhere += " and m.MAKER_NAME like @MAKER_NAME+'%' " + DbUtil.LikeEscapeInfoAdd();
                            parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                            conditionName = "前方一致";
                            break;
                        case Const.cSEARCH_TYPE_ENDSWITH:
                            strWhere += " and m.MAKER_NAME like '%'+@MAKER_NAME " + DbUtil.LikeEscapeInfoAdd();
                            parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                            conditionName = "後方一致";
                            break;
                        case Const.cSEARCH_TYPE_EQUALTO:
                            strWhere += " and m.MAKER_NAME = @MAKER_NAME ";
                            conditionName = "完全一致";
                            break;
                    }
                    _params.Add("MAKER_NAME", parameter);
                }
            }

            // 代理店名
            if (_clsInputSearchStock.VENDOR_ID != null && _clsInputSearchStock.VENDOR_ID.Length > 0)
            {
                strWhere += " and s.VENDOR_ID = @VENDOR_ID ";
                _params.Add("@VENDOR_ID", _clsInputSearchStock.VENDOR_ID);
            }

            // カタログNO
            if (_clsInputSearchStock.CAT_CD != null && _clsInputSearchStock.CAT_CD.Length > 0)
            {
                var parameter = _clsInputSearchStock.CAT_CD.Trim();
                var conditionName = string.Empty;
                switch (_clsInputSearchStock.CAT_CD_TYPE)
                {
                    case Const.cSEARCH_TYPE_CONTAINS:
                        strWhere += " and s.CAT_CD like '%'+@CAT_CD+'%' " + DbUtil.LikeEscapeInfoAdd();
                        parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                        conditionName = "部分一致";
                        break;
                    case Const.cSEARCH_TYPE_STARTSWITH:
                        strWhere += " and s.CAT_CD like @CAT_CD+'%' " + DbUtil.LikeEscapeInfoAdd();
                        parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                        conditionName = "前方一致";
                        break;
                    case Const.cSEARCH_TYPE_ENDSWITH:
                        strWhere += " and s.CAT_CD like '%'+@CAT_CD " + DbUtil.LikeEscapeInfoAdd();
                        parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                        conditionName = "後方一致";
                        break;
                    case Const.cSEARCH_TYPE_EQUALTO:
                        strWhere += " and s.CAT_CD = @CAT_CD ";
                        conditionName = "完全一致";
                        break;
                }
                _params.Add("CAT_CD", parameter);
            }
            //2019/02/18 TPE.Sugimoto Add Sta
            // CAS#(半角)
            if (_clsInputSearchStock.CAS_NO != null && _clsInputSearchStock.CAS_NO.Length > 0)
            {
                var parameter = _clsInputSearchStock.CAS_NO.Trim();
                strWhere += " and c.CAS_NO = @CAS_NO ";
                _params.Add("@CAS_NO", parameter);
            }
            //2019/02/18 TPE.Sugimoto Add End
            // 商品QRコード
            if (_clsInputSearchStock.BARCODE != null && _clsInputSearchStock.BARCODE.Length > 0)
            {
                var parameter = _clsInputSearchStock.BARCODE.Trim();
                var conditionName = string.Empty;
                switch (_clsInputSearchStock.BARCODE_TYPE)
                {
                    case Const.cSEARCH_TYPE_CONTAINS:
                        strWhere += " and s.BARCODE like '%'+@BARCODE+'%' " + DbUtil.LikeEscapeInfoAdd();
                        parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                        conditionName = "部分一致";
                        break;
                    case Const.cSEARCH_TYPE_STARTSWITH:
                        strWhere += " and s.BARCODE like @BARCODE+'%' " + DbUtil.LikeEscapeInfoAdd();
                        parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                        conditionName = "前方一致";
                        break;
                    case Const.cSEARCH_TYPE_ENDSWITH:
                        strWhere += " and s.BARCODE like '%'+@BARCODE " + DbUtil.LikeEscapeInfoAdd();
                        parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                        conditionName = "後方一致";
                        break;
                    case Const.cSEARCH_TYPE_EQUALTO:
                        strWhere += " and s.BARCODE = @BARCODE ";
                        conditionName = "完全一致";
                        break;
                }
                _params.Add("BARCODE", parameter);
            }

            // 発注番号
            if (_clsInputSearchStock.ORDER_NO != null && _clsInputSearchStock.ORDER_NO.Length > 0)
            {
                strWhere += " and s.ORDER_NO = @ORDER_NO ";
                _params.Add("@ORDER_NO", _clsInputSearchStock.ORDER_NO.Trim());
            }

            // ステータス
            if (_clsInputSearchStock.STATUS != null && _clsInputSearchStock.STATUS.Length > 0)
            {
                strWhere += " and s.STATUS_ID = @STATUS_ID ";
                _params.Add("@STATUS_ID", _clsInputSearchStock.STATUS.Trim());

                List<M_STATUS> statuses = new MasterService().selectStatus(2);
                var statusName = string.Empty;
                foreach (var row in statuses)
                {
                    if (row.STATUS_ID.ToString() == _clsInputSearchStock.STATUS)
                    {
                        statusName = row.STATUS_TEXT.ToString();
                    }
                }
            }
            else
            {
                strWhere += " and (s.STATUS_ID in (@STATUS_ID1, @STATUS_ID2, @STATUS_ID3)) ";
                _params.Add("@STATUS_ID1", Const.cSTOCK_STATUS_ID.STOCK.ToString("D"));
                _params.Add("@STATUS_ID2", Const.cSTOCK_STATUS_ID.BORROW.ToString("D"));
                _params.Add("@STATUS_ID3", Const.cSTOCK_STATUS_ID.EMPTY.ToString("D"));
            }

            // 発注者名
            if (_clsInputSearchStock.ORDER_USER != null && _clsInputSearchStock.ORDER_USER.Length > 0)
            {
                var parameter = _clsInputSearchStock.ORDER_USER.Trim();
                var conditionName = string.Empty;
                switch (_clsInputSearchStock.ORDER_USER_TYPE)
                {
                    case Const.cSEARCH_TYPE_CONTAINS:
                        strWhere += " and u.USER_NAME like '%'+@USER_NAME+'%' " + DbUtil.LikeEscapeInfoAdd();
                        parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                        conditionName = "部分一致";
                        break;
                    case Const.cSEARCH_TYPE_STARTSWITH:
                        strWhere += " and u.USER_NAME like @USER_NAME+'%' " + DbUtil.LikeEscapeInfoAdd();
                        parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                        conditionName = "前方一致";
                        break;
                    case Const.cSEARCH_TYPE_ENDSWITH:
                        strWhere += " and u.USER_NAME like '%'+@USER_NAME " + DbUtil.LikeEscapeInfoAdd();
                        parameter = DbUtil.ConvertIntoEscapeChar(parameter);
                        conditionName = "後方一致";
                        break;
                    case Const.cSEARCH_TYPE_EQUALTO:
                        strWhere += " and u.USER_NAME = @USER_NAME ";
                        conditionName = "完全一致";
                        break;
                }
                _params.Add("USER_NAME", parameter);
            }

            // 納品日from
            if (_clsInputSearchStock.DELI_DATE_FROM != null && _clsInputSearchStock.DELI_DATE_FROM.Length > 0)
            {
                DateTime date = DateTime.Parse(_clsInputSearchStock.DELI_DATE_FROM.Trim());
                strWhere += " and s.DELI_DATE >= @DELI_DATE_FROM ";
                _params.Add("@DELI_DATE_FROM", date);
            }

            // 納品日to
            if (_clsInputSearchStock.DELI_DATE_TO != null && _clsInputSearchStock.DELI_DATE_TO.Length > 0)
            {
                DateTime date = DateTime.Parse(_clsInputSearchStock.DELI_DATE_TO.Trim());
                date = date.AddDays(1);
                strWhere += " and s.DELI_DATE <= @DELI_DATE_TO ";
                _params.Add("@DELI_DATE_TO", date);
            }

            // 法規制
            if (_clsInputSearchStock.hdnRegulationIds != null && _clsInputSearchStock.hdnRegulationIds.Length != 0)
            {
                int count = 0;
                string paraRegTypeId = "";
                foreach (string regulationId in _clsInputSearchStock.hdnRegulationIds.Split(','))
                {
                    paraRegTypeId += "@REG_TYPE_ID" + count.ToString();
                    paraRegTypeId += ",";
                    _params.Add("@REG_TYPE_ID" + count.ToString(), regulationId);
                    count++;
                }
                paraRegTypeId = paraRegTypeId.Remove(paraRegTypeId.Length - 1);

                strWhere += " and s.CAT_CD in  (select CHEM_CD from M_CHEM_REGULATION where REG_TYPE_ID in (" + paraRegTypeId + "))";             
            }

            // 保管場所
            if (_clsInputSearchStock.hdnLocationIds != null && _clsInputSearchStock.hdnLocationIds.Length != 0)
            {
                int count = 0;
                string paraLocationId = "";
                foreach (string locationId in _clsInputSearchStock.hdnLocationIds.Split(','))
                {
                    paraLocationId += "@LOC_ID" + count.ToString();
                    paraLocationId += ",";
                    _params.Add("@LOC_ID" + count.ToString(), locationId);
                    count++;
                }
                paraLocationId = paraLocationId.Remove(paraLocationId.Length - 1);
                strWhere += " and s.LOC_ID in(" + paraLocationId + ")";
            }

            // 構造式
            if (_clsInputSearchStock.STRUCTURE_BASE64 != null && _clsInputSearchStock.STRUCTURE_BASE64.Length > 0)
            {
                string chunkStructure = "";
                int offset = 0;
                int chunkSize = 666;
                while (offset < _clsInputSearchStock.STRUCTURE_BASE64.Length)
                {
                    if (offset + chunkSize >= _clsInputSearchStock.STRUCTURE_BASE64.Length)
                    {
                        chunkStructure += ",'" + _clsInputSearchStock.STRUCTURE_BASE64.Substring(offset) + "'";
                    }
                    else
                    {
                        chunkStructure += ",'" + _clsInputSearchStock.STRUCTURE_BASE64.Substring(offset, chunkSize) + "'";
                    }
                    offset += chunkSize;
                }
                chunkStructure = chunkStructure.Substring(1);   //先頭のカンマを削除

                strWhere += " and s.COMPOUND_LIST_ID in (select COMPOUND_LIST_ID from  M_COMPOUND_LIST CL,(select COMPOUND_ID from M_COMPOUND C,(select STRUCTURE_LIST_ID from";
                strWhere += "  M_STRUCTURE_LIST SL,(select S.STRUCTURE_CD from M_STRUCTURE S where S.DEL_FLAG <> 1 and CsCartridge.MoleculeContains(S.STRUCTURE, CsCartridge.XARRAY(" + chunkStructure + "), @OPTIONS) = 1) S";
                strWhere += "  where SL.DEL_FLAG <> 1 and SL.STRUCTURE_CD in S.STRUCTURE_CD) SL where C.DEL_FLAG <> 1 and C.STRUCTURE_LIST_ID = SL.STRUCTURE_LIST_ID) C where CL.DEL_FLAG <> 1 and CL.COMPOUND_ID = C.COMPOUND_ID)";

                string strOption = "";
                switch (_clsInputSearchStock.STRUCTURE_TYPE)
                {
                    case Const.cSTRUCTURE_EXACT:
                        strOption = "IDENTITY=YES";
                        break;
                    case Const.cSTRUCTURE_FULL:
                        strOption = "FULL=YES";
                        break;
                    case Const.cSTRUCTURE_SUBSTRUCTURE:
                        strOption = "FULL=NO";
                        break;
                    default:
                        strOption = "IDENTITY=YES";
                        break;
                }

                _params.Add("OPTIONS", strOption);
            }
            // 参照権限が一般ユーザーの場合
            if (_clsUser.REFERENCE_AUTHORITY != null && _clsUser.REFERENCE_AUTHORITY.Equals(Const.cREFERENCE_AUTHORITY_FLAG_NORMAL))
            {
                strWhere += " and (ml.GROUP_CD in((select G1.GROUP_CD from M_USER U1 left outer join M_GROUP G1 on G1.GROUP_CD = U1.GROUP_CD where U1.USER_CD = '" + _clsUser.USER_CD + "') ,(select G2.GROUP_CD from M_GROUP G2 where G2.GROUP_CD = ";
                strWhere += "           (select G5.P_GROUP_CD from M_GROUP G5 left outer join M_USER U5 on G5.GROUP_CD = U5.GROUP_CD where U5.USER_CD = '" + _clsUser.USER_CD + "')) ";
                strWhere += "       ,(select G3.GROUP_CD from M_USER U3 left outer join M_GROUP G3 on U3.PREVIOUS_GROUP_CD = G3.GROUP_CD where U3.USER_CD = '" + _clsUser.USER_CD + "' and G3.DEL_FLAG = 1) ";
                strWhere += "       ,(select G4.GROUP_CD from M_GROUP G4 where G4.GROUP_CD = ";
                strWhere += "           (select G6.P_GROUP_CD from M_USER U6 left outer join M_GROUP G6 on U6.PREVIOUS_GROUP_CD = G6.GROUP_CD where U6.USER_CD = '" + _clsUser.USER_CD + "' and G6.DEL_FLAG = 1 )) ";
                strWhere += "   ) or s.REG_DATE <= '" + NikonConst.Phase2StartDate + "')";
                strWhere += " )";
            }
            //searchConditionCsv = chemStock.Csv;
            //chemStock.Dispose();
        }


        public Int64 getStockCount(InventoryManagement _clsInputSearchStock)
        {
            string strSQL = "";
            string strWhere = "";
            DynamicParameters _params = new DynamicParameters();
            List<int> dt = new List<int>();
            try
            {
                makeWhereConditionsStock(_clsInputSearchStock, ref strWhere, ref _params);
                strSQL = "select count(s.STOCK_ID) cnt from T_STOCK s left outer join M_CHEM c on s.CAT_CD = c.CHEM_CD left outer join T_ORDER o on s.ORDER_NO = o.ORDER_NO  left outer join M_MAKER m on s.MAKER_ID = m.MAKER_ID left outer join M_USER u on o.ORDER_USER_CD = u.USER_CD left outer join M_UNITSIZE us on s.UNITSIZE_ID = us.UNITSIZE_ID ";

                if (Common.GetLogonForm().REFERENCE_AUTHORITY.Equals(Const.cREFERENCE_AUTHORITY_FLAG_NORMAL))
                {
                    strSQL += " left outer join T_MGMT_LEDGER ml on s.REG_NO = ml.REG_NO ";
                }
                strSQL += "where s.DEL_FLAG <> 1  ";
                strSQL += strWhere;

                dt = DbUtil.Select<int>(strSQL, _params);
                return Convert.ToInt64(dt[0]);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return 0;
        }

        /// <summary>
        /// 在庫IDをキーにT_STOCKデータを取得する
        /// </summary>
        /// <param name="intStockID">在庫ID</param>
        public List<ViewInventoryDetails> getStockInformation(int? intStockID)
        {
            List<ViewInventoryDetails> dt = new List<ViewInventoryDetails>();
            StringBuilder sb = new StringBuilder();
            sb.Append(" select s.STOCK_ID, s.COMPOUND_LIST_ID, s.BARCODE,s.REG_NO,s.ORDER_NO, c.CHEM_NAME PRODUCT_NAME_EN,c.CHEM_NAME PRODUCT_NAME_JP,c.CHEM_NAME PRODUCT_NAME_SEARCH,c.CAS_NO, s.MAKER_ID,s.CAT_CD,s.GRADE,");
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
            {
                sb.Append("        s.UNITSIZE,");
            }
            if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
            {
                sb.Append("        format(s.UNITSIZE,'0.#########') UNITSIZE,");
            }
            sb.Append("        s.UNITSIZE_ID,s.FINE_POISON_FLAG,s.LOC_ID,s.MEMO,s.INI_GROSS_WEIGHT_G,s.FIRST_LOC_ID,");
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
            {
                sb.Append("        (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (to_char(s.CUR_GROSS_WEIGHT_G - nvl(s.BOTTLE_WEIGHT_G, 0))) end) CUR_GROSS_WEIGHT_G,");
            }
            if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
            {
                sb.Append("        (case when s.CUR_GROSS_WEIGHT_G is NULL then '' else (convert(nvarchar,s.CUR_GROSS_WEIGHT_G - IsNull(s.BOTTLE_WEIGHT_G, 0))) end) CUR_GROSS_WEIGHT_G,");
            }
            sb.Append("        s.BOTTLE_WEIGHT_G, s.STATUS_ID,s.PRESERVATION_CONDITION,s.TRANSPORT_CONDITION, ");
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
            {
                sb.Append("        TO_CHAR( s.LIMIT_DATE,'YYYY/MM/DD') LIMIT_DATE,");
                sb.Append("        TO_CHAR( s.DELI_DATE,'YYYY/MM/DD') DELI_DATE,");
                sb.Append("        TO_CHAR( s.DEL_MAIL_DATE,'YYYY/MM/DD') DEL_MAIL_DATE,");
            }
            if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
            {
                sb.Append("        convert(nvarchar,s.LIMIT_DATE,111) LIMIT_DATE,convert(nvarchar,s.DELI_DATE,111) DELI_DATE,convert(nvarchar,s.DEL_MAIL_DATE,111) DEL_MAIL_DATE,");
            }
            sb.Append("        s.ACCOUNT_ID, s.REG_DATE,s.UPD_DATE,s.DEL_FLAG, ");
            if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
            {
                sb.Append("        TO_CHAR( s.BRW_DATE,'YYYY/MM/DD') BRW_DATE,");
            }
            if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
            {
                sb.Append("         convert(nvarchar,s.BRW_DATE,111) BRW_DATE,");
            }
            sb.Append(" mst.STATUS_TEXT, ml.LOC_NAME,mus.UNIT_NAME, s.MAKER_ID,mmk.MAKER_NAME,mu1.USER_NAME ORDER_NAME,mu2.USER_NAME BORROWER_NAME,fml.LOC_NAME FIRST_LOC_NAME ");
            sb.Append(" from   T_STOCK s left join T_ORDER ord on s.ORDER_NO = ord.ORDER_NO left join M_CHEM c on s.CAT_CD = c.CHEM_CD left join M_STATUS mst on s.STATUS_ID = mst.STATUS_ID ");
            sb.Append(" left join V_LOCATION ml on s.LOC_ID = ml.LOC_ID left join M_MAKER mmk on s.MAKER_ID = mmk.MAKER_ID left join M_UNITSIZE mus on s.UNITSIZE_ID = mus.UNITSIZE_ID left join M_USER mu1 on ord.ORDER_USER_CD = mu1.USER_CD ");
            sb.Append(" left join M_USER mu2 on s.BORROWER = mu2.USER_CD left join V_LOCATION fml on s.FIRST_LOC_ID = fml.LOC_ID where  s.STOCK_ID = @STOCK_ID and    s.DEL_FLAG <> 1 ");

            //検索パラメーター設定
            DynamicParameters _params = new DynamicParameters();
            _params.Add("@STOCK_ID", intStockID);

            //検索処理
            dt = DbUtil.Select<ViewInventoryDetails>(sb.ToString(), _params);

            if (dt.Count == 0)
            {
                dt = new List<ViewInventoryDetails>();
            }
            else
            {
                setupResult(dt);
            }
            return dt;
        }

        /// <summary>
        /// 検索結果をメンバに設定する
        /// </summary>
        /// <param name="result">在庫検索結果</param>
        public ViewInventoryDetails setupResult(List<ViewInventoryDetails> dt)
        {
            //ResetResult();
            ViewInventoryDetails inventoryResult = new ViewInventoryDetails();
            inventoryResult.STATUS_TEXT = dt[0].STATUS_TEXT;
            inventoryResult.REG_NO = Convert.ToString(dt[0].REG_NO);
            inventoryResult.REG_NO = Convert.ToString(dt[0].REG_NO);
            inventoryResult.ORDER_NO = Convert.ToString(dt[0].ORDER_NO);
            inventoryResult.BARCODE = Convert.ToString(dt[0].BARCODE);
            inventoryResult.PRODUCT_NAME_JP = Convert.ToString(dt[0].PRODUCT_NAME_JP);
            inventoryResult.PRODUCT_NAME_EN = Convert.ToString(dt[0].PRODUCT_NAME_EN);

            inventoryResult.LOC_NAME = Convert.ToString(dt[0].LOC_NAME);
            inventoryResult.CAT_CD = Convert.ToString(dt[0].CAT_CD);
            inventoryResult.CAS_NO = Convert.ToString(dt[0].CAS_NO);//2019/02/18 TPE.Sugimoto Add
            inventoryResult.MAKER_NAME = Convert.ToString(dt[0].MAKER_NAME);
            if (inventoryResult.MAKER_NAME != null)
            {
                var list = new MakerMasterInfo().GetSelectList();
                if (!string.IsNullOrEmpty(Convert.ToString(dt[0].MAKER_ID)) && list.Count(x => x.Id?.ToString() == Convert.ToString(dt[0].MAKER_ID)) == 1)
                {
                    inventoryResult.MAKER_ID = Convert.ToString(dt[0].MAKER_ID);
                }
                else
                {
                    inventoryResult.MAKER_ID = "";
                }
            }
            inventoryResult.GRADE = Convert.ToString(dt[0].GRADE);
            inventoryResult.labUnitSize = Convert.ToString(dt[0].UNITSIZE);
            inventoryResult.UNITSIZE = Convert.ToString(dt[0].UNITSIZE);
            inventoryResult.labUnitName = Convert.ToString(dt[0].UNIT_NAME);
            if (inventoryResult.labUnitName != null)
            {
                var list = new UnitSizeMasterInfo().GetSelectList();
                if (!string.IsNullOrEmpty(Convert.ToString(dt[0].UNITSIZE_ID)) && list.Count(x => x.Id?.ToString() == Convert.ToString(dt[0].UNITSIZE_ID)) == 1)
                {
                    inventoryResult.UNITSIZE_ID = Convert.ToString(dt[0].UNITSIZE_ID);
                }
                else
                {
                    inventoryResult.UNITSIZE_ID = "";
                }
            }
            inventoryResult.DELI_DATE = Convert.ToString(dt[0].DELI_DATE);
            inventoryResult.ORDER_NAME = Convert.ToString(dt[0].ORDER_NAME);
            // 残量
            inventoryResult.CUR_GROSS_WEIGHT_G = Convert.ToString(dt[0].CUR_GROSS_WEIGHT_G);
            if (Convert.ToString(dt[0].CUR_GROSS_WEIGHT_G) == "" || Convert.ToString(dt[0].CUR_GROSS_WEIGHT_G) == null)
            {
                inventoryResult.imgWeight = false;
            }
            else
            {
                inventoryResult.imgWeight = true;
            }
            inventoryResult.BRW_DATE = Convert.ToString(dt[0].BRW_DATE);
            inventoryResult.BORROWER_NAME = Convert.ToString(dt[0].BORROWER_NAME);
            inventoryResult.PRESERVATION_CONDITION = Convert.ToString(dt[0].PRESERVATION_CONDITION);
            inventoryResult.LIMIT_DATE = Convert.ToString(dt[0].LIMIT_DATE);
            inventoryResult.labMemo = Convert.ToString(dt[0].MEMO);
            inventoryResult.MEMO = Convert.ToString(dt[0].MEMO);
            // 初期重量
            inventoryResult.INI_GROSS_WEIGHT_G = Convert.ToString(dt[0].INI_GROSS_WEIGHT_G);


            if (inventoryResult.INI_GROSS_WEIGHT_G == null || inventoryResult.INI_GROSS_WEIGHT_G == "")
            {
                inventoryResult.imgIni_weight = false;
            }
            else
            {
                inventoryResult.imgIni_weight = true;
            }
            // 風袋重量
            inventoryResult.BOTTLE_WEIGHT_G = Convert.ToString(dt[0].BOTTLE_WEIGHT_G);


            if (Convert.ToString(dt[0].BOTTLE_WEIGHT_G) == null || Convert.ToString(dt[0].BOTTLE_WEIGHT_G) == "")
            {
                inventoryResult.imgbottle_weight = false;
            }
            else
            {
                inventoryResult.imgbottle_weight = true;
            }

            inventoryResult.FIRST_LOC_NAME = Convert.ToString(dt[0].FIRST_LOC_NAME);
            inventoryResult.TRANSPORT_CONDITION = Convert.ToString(dt[0].TRANSPORT_CONDITION);
            inventoryResult.DEL_MAIL_DATE = Convert.ToString(dt[0].DEL_MAIL_DATE);
            return inventoryResult;
        }


        public IEnumerable<V_GHSCAT> GetGhsCategory(string value)
        {
            var list = new List<V_GHSCAT>().AsQueryable();
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var chemGhsCategoryInfo = new ChemRegulationDataInfo(ClassIdType.GhsCategory);
                    var condition = new M_CHEM_REGULATION();
                    condition.CHEM_CD = value;
                    var records = chemGhsCategoryInfo.GetSearchResult(condition);
                    var ids = records.Select(x => x.REG_TYPE_ID).ToList();
                    var ghsCategoryInfo = new GhsCategoryMasterInfo();
                    list = ghsCategoryInfo.GetSelectedItems(ids).AsQueryable();
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return list;
        }


        /// <summary>
        /// 法規制のSelectMethodで選択済みの法規制を取得します。
        /// </summary>
        /// <param name="value">表示されている化学物質コードを抽出します。</param>
        /// <returns>選択済みの法規制を取得します。</returns>
        public IEnumerable<V_REGULATION> GetRegulation(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var chemRegulationInfo = new ChemRegulationDataInfo(ClassIdType.Regulation);
                    var condition = new M_CHEM_REGULATION();
                    condition.CHEM_CD = value;
                    var records = chemRegulationInfo.GetSearchResult(condition);
                    var ids = records.Select(x => x.REG_TYPE_ID).ToList();
                    var regulationInfo = new Classes.RegulationMasterInfo();
                    return regulationInfo.GetSelectedItems(ids).AsQueryable();
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return new List<V_REGULATION>().AsQueryable();
        }

        /// <summary>
        /// 消防法分類のSelectMethodで選択済みのGHS分類を取得します。
        /// </summary>
        /// <param name="value">表示されている化学物質コードを抽出します。</param>
        /// <returns>選択済みのGHS分類を取得します。</returns>
        public IEnumerable<V_FIRE> GetFireCategory(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var chemFireCategoryInfo = new ChemRegulationDataInfo(ClassIdType.Fire);
                    var condition = new M_CHEM_REGULATION();
                    condition.CHEM_CD = value;
                    var records = chemFireCategoryInfo.GetSearchResult(condition);
                    var ids = records.Select(x => x.REG_TYPE_ID).ToList();
                    var FireMasterInfo = new FireMasterInfo();
                    return FireMasterInfo.GetSelectedItems(ids).AsQueryable();
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return new List<V_FIRE>().AsQueryable();
        }

        /// <summary>
        /// 社内ルール分類のSelectMethodで選択済みのGHS分類を取得します。
        /// </summary>
        /// <param name="value">表示されている化学物質コードを抽出します。</param>
        /// <returns>選択済みのGHS分類を取得します。</returns>
        public IEnumerable<V_OFFICE> GetOfficeCategory(string value)
        {
            try
            {
                if (!string.IsNullOrEmpty(value))
                {
                    var chemOfficeCategoryInfo = new ChemRegulationDataInfo(ClassIdType.Office);
                    var condition = new M_CHEM_REGULATION();
                    condition.CHEM_CD = value;
                    var records = chemOfficeCategoryInfo.GetSearchResult(condition);
                    var ids = records.Select(x => x.REG_TYPE_ID).ToList();
                    var OfficeMasterInfo = new OfficeMasterInfo();
                    return OfficeMasterInfo.GetSelectedItems(ids).AsQueryable();
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return new List<V_OFFICE>().AsQueryable();
        }


        /// <summary>
        /// バーコードよりT_LEDGERとM_GROUPの検索
        /// </summary>
        public string GetSqlForLedgerGroup()
        {
            StringBuilder ret = new StringBuilder();
            ret.Append("select ML.GROUP_CD from T_MGMT_LEDGER ML left outer join T_STOCK S on ML.REG_NO = S.REG_NO ");
            ret.Append(" where ML.GROUP_CD in ( ");
            ret.Append("     (select G1.GROUP_CD from M_USER U1 left outer join M_GROUP G1 on G1.GROUP_CD = U1.GROUP_CD where U1.USER_CD = @USER_CD), ");
            ret.Append("     (select G2.GROUP_CD from M_GROUP G2 where G2.GROUP_CD = (select G5.P_GROUP_CD from M_GROUP G5 left outer join M_USER U5 on G5.GROUP_CD = U5.GROUP_CD where U5.USER_CD = @USER_CD)), ");
            ret.Append("     (select G3.GROUP_CD from M_USER U3 left outer join M_GROUP G3 on U3.PREVIOUS_GROUP_CD = G3.GROUP_CD where U3.USER_CD = @USER_CD and G3.DEL_FLAG = 1), ");
            ret.Append("     (select G4.GROUP_CD from M_GROUP G4 where G4.GROUP_CD = (select G6.P_GROUP_CD from M_USER U6 left outer join M_GROUP G6 on U6.PREVIOUS_GROUP_CD = G6.GROUP_CD where U6.USER_CD = @USER_CD and G6.DEL_FLAG = 1 )) ");
            ret.Append(" ) and (S.BARCODE = @BARCODE OR S.STOCK_ID = @STOCK_ID);");
            return ret.ToString();
        }

        public bool checkGroup(string keyword, StockSearchTarget type)
        {

            string USER_CD = Common.GetLogonForm().Id;
            DynamicParameters _params = new DynamicParameters();
            _params.Add("@USER_CD", USER_CD);
            switch (type)
            {
                case StockSearchTarget.barcode:
                    _params.Add("@BARCODE", keyword);
                    _params.Add("@STOCK_ID", null);
                    break;
                case StockSearchTarget.stockId:
                    _params.Add("@STOCK_ID", keyword);
                    _params.Add("@BARCODE", null);
                    break;
            }

            List<string> dtGroup = DbUtil.Select<string>(GetSqlForLedgerGroup(), _params);
            if (dtGroup.Count > 0) return true;
            return false;
        }


        public List<FireDataModel> DoSearchFireData()
        {
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbSql = new StringBuilder();
            DynamicParameters _params = new DynamicParameters();
            //DataTable dt;

            sbSql.Append(" SELECT V1.FIRE_ID,M1.FIRE_SIZE,SUBSTRING(V1.FIRE_NAME,0,CHARINDEX('類/',V1.FIRE_NAME) + 1) AS REG_TEXT,SUBSTRING(V1.FIRE_NAME,CHARINDEX('類/',V1.FIRE_NAME) + 2,100) AS REG_TEXT2 ");
            sbSql.Append(" FROM V_FIRE V1 LEFT JOIN M_REGULATION M1 ON V1.FIRE_ID = M1.REG_TYPE_ID LEFT JOIN M_UNITSIZE U ON M1.UNITSIZE_ID = U.UNITSIZE_ID WHERE V1.FIRE_NAME LIKE '%第%類%'");
            sbSql.Append(" AND M1.FIRE_SIZE IS NOT NULL AND V1.DEL_FLAG = 0 ORDER BY REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SUBSTRING(V1.FIRE_NAME,0,CHARINDEX('類/',V1.FIRE_NAME) + 1),'一',1),'二','2'),'三','3'),'四','4'),'五','5'),'六','6'),M1.FIRE_SIZE,V1.SORT_ORDER");

            List<FireDataModel> dt = DbUtil.Select<FireDataModel>(sbSql.ToString(), _params);

            return dt;
        }

        public List<InventoryManagement> DoSearchSheet1(InventoryManagement inventory)
        {
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbSql = new StringBuilder();
            DynamicParameters _params = new DynamicParameters();
            Dictionary<string, string> dic = (Dictionary<string, string>)HttpContext.Current.Session["CONVERT_UNIT"];

            string strSQL_KG = string.Empty;
            string strSQL_L = string.Empty;
            foreach (KeyValuePair<string, string> item in dic)
            {
                if (item.Value.ToUpper() == "KG")
                {
                    strSQL_KG += " U1.UNIT_NAME = '" + item.Key + "' or ";
                }
                else if (item.Value.ToUpper() == "L")
                {
                    strSQL_L += " U1.UNIT_NAME = '" + item.Key + "' or ";
                }
            }
            strSQL_KG += "U1.UNIT_NAME = 'Kg'";
            strSQL_L += "U1.UNIT_NAME = 'L'";


            // SELECT SQL生成
            sbSql.Append(" SELECT  DISTINCT L1.REG_NO,FORMAT(L1.LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID");
            sbSql.Append(",VLOC1.LOC_NAME AS LOC_NAME");
            sbSql.Append(",VLOC1.LOC_BARCODE AS LOC_BARCODE");
            sbSql.Append(",MC1.CHEM_NAME");
            sbSql.Append(",SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT");
            sbSql.Append(",SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2");
            sbSql.Append(",ISNULL(MR1.FIRE_SIZE,(SELECT MR2.FIRE_SIZE FROM M_REGULATION MR2 WHERE MR2.REG_TYPE_ID = MR1.P_REG_TYPE_ID)) AS FIRE_SIZE");
            sbSql.Append(",FORMAT(convert(float,LOC1.AMOUNT),'0.00') AS AMOUNT_BASE");
            sbSql.Append(",IIF(UPPER(U2.UNIT_NAME) = 'KG',");
            sbSql.Append("CASE ");
            sbSql.Append(" WHEN  " + strSQL_L + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append("END ");
            sbSql.Append(",");
            sbSql.Append("CASE");
            sbSql.Append(" WHEN " + strSQL_KG + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append(" END");
            sbSql.Append(")");
            sbSql.Append(" AS AMOUNT");
            sbSql.Append(" ,U1.UNIT_NAME");
            sbSql.Append(" ,U2.UNIT_NAME AS REGULATION_UNITNAME");
            sbSql.Append(" ,G1.GROUP_NAME");
            sbSql.Append(" ,(SELECT US1.USER_NAME + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS USER_NAME");
            sbSql.Append(" ,(SELECT US1.TEL + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS TEL");
            sbSql.Append(" FROM T_LEDGER_WORK L1");
            sbSql.Append(" LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD");
            sbSql.Append(" LEFT JOIN T_LED_WORK_LOC LOC1 ON L1.LEDGER_FLOW_ID = LOC1.LEDGER_FLOW_ID");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD");
            sbSql.Append(" LEFT JOIN T_LEDGER_WORK_REGULATION TLR1 ON TLR1.LEDGER_FLOW_ID = L1.LEDGER_FLOW_ID");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID");
            sbSql.Append(" LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0");
            sbSql.Append(" WHERE");
            sbSql.Append(" L1.DEL_FLAG = 0 AND L1.TEMP_SAVED_FLAG = 0");
            sbSql.Append(" AND");
            sbSql.Append(" VR1.FIRE_NAME LIKE '%第%類%'");

            // SELECT SQL生成
            //sbSql.Append(" SELECT  DISTINCT L1.REG_NO,FORMAT(L1.LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID,VLOC1.LOC_NAME,VLOC1.LOC_BARCODE,MC1.CHEM_NAME,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT ");
            //sbSql.Append(" ,SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2 ,ISNULL(MR1.FIRE_SIZE,(SELECT MR2.FIRE_SIZE FROM M_REGULATION MR2 WHERE MR2.REG_TYPE_ID = MR1.P_REG_TYPE_ID)) AS FIRE_SIZE");
            //sbSql.Append(" ,IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float,LOC1.AMOUNT),'0.00'),FORMAT(convert(float,US1.AMOUNT),'0.00')) AS AMOUNT_BASE,IIF(UPPER(U2.UNIT_NAME) = 'KG', ");
            //sbSql.Append("CASE WHEN  " + strSQL_L + " THEN IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00'), ");
            //sbSql.Append("   FORMAT(convert(float, US1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00'))");
            //sbSql.Append(" ELSE IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00'),FORMAT(convert(float, US1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00'))");
            //sbSql.Append("END ,CASE WHEN " + strSQL_KG + " THEN IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00'), ");
            //sbSql.Append("   FORMAT(convert(float, US1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')) ELSE IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00'),");
            //sbSql.Append("   FORMAT(convert(float, US1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')) END ) AS AMOUNT");
            //sbSql.Append(" ,U1.UNIT_NAME,G1.GROUP_NAME ,(SELECT US1.USER_NAME + ';' FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD WHERE C1.LOC_ID = LOC1.LOC_ID FOR XML PATH('') ) AS USER_NAME ");
            //sbSql.Append(" ,(SELECT US1.TEL + ';' FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD WHERE C1.LOC_ID = LOC1.LOC_ID FOR XML PATH('') ) AS TEL FROM T_LEDGER_WORK L1 LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD ");
            //sbSql.Append(" LEFT JOIN T_LED_WORK_LOC LOC1 ON L1.LEDGER_FLOW_ID = LOC1.LEDGER_FLOW_ID INNER JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID LEFT JOIN T_LED_WORK_USAGE_LOC US1 ON L1.LEDGER_FLOW_ID = US1.LEDGER_FLOW_ID ");
            //sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0 LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD LEFT JOIN T_LEDGER_WORK_REGULATION TLR1 ON TLR1.LEDGER_FLOW_ID = L1.LEDGER_FLOW_ID ");
            //sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0 LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0 ");
            //sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0 WHERE L1.DEL_FLAG = 0  AND VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND US1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ") AND US1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND VLOC1.LOC_BARCODE = @LOC_BARCODE_WORK");
                _params.Add("@LOC_BARCODE_WORK", inventory.txtBarcode);
            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds.ToString()) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%'+@REPORT_NAME_WORK+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like @REPORT_NAME_WORK+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%' +@REPORT_NAME_WORK" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME = @REPORT_NAME_WORK");
                    _params.Add("@REPORT_NAME_WORK", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
            }

            sbSql.Append(" UNION ");

            sbSql.Append(" SELECT  DISTINCT L1.REG_NO,FORMAT(L1.LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID");
            sbSql.Append(",VLOC1.LOC_NAME AS LOC_NAME");
            sbSql.Append(",VLOC1.LOC_BARCODE AS LOC_BARCODE");
            sbSql.Append(",MC1.CHEM_NAME");
            sbSql.Append(",SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT");
            sbSql.Append(",SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2");
            sbSql.Append(",ISNULL(MR1.FIRE_SIZE,(SELECT MR2.FIRE_SIZE FROM M_REGULATION MR2 WHERE MR2.REG_TYPE_ID = MR1.P_REG_TYPE_ID)) AS FIRE_SIZE");
            sbSql.Append(",FORMAT(convert(float,LOC1.AMOUNT),'0.00') AS AMOUNT_BASE");
            sbSql.Append(",IIF(UPPER(U2.UNIT_NAME) = 'KG',");
            sbSql.Append("CASE ");
            sbSql.Append(" WHEN  " + strSQL_L + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append("END ");
            sbSql.Append(",");
            sbSql.Append("CASE");
            sbSql.Append(" WHEN " + strSQL_KG + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append(" END");
            sbSql.Append(")");
            sbSql.Append(" AS AMOUNT");
            sbSql.Append(" ,U1.UNIT_NAME");
            sbSql.Append(" ,U2.UNIT_NAME AS REGULATION_UNITNAME");
            sbSql.Append(" ,G1.GROUP_NAME");
            sbSql.Append(" ,(SELECT US1.USER_NAME + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS USER_NAME");
            sbSql.Append(" ,(SELECT US1.TEL + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS TEL");
            sbSql.Append(" FROM T_LEDGER_WORK L1");
            sbSql.Append(" LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD");
            sbSql.Append(" LEFT JOIN T_LED_WORK_USAGE_LOC LOC1 ON L1.LEDGER_FLOW_ID = LOC1.LEDGER_FLOW_ID");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD");
            sbSql.Append(" LEFT JOIN T_LEDGER_WORK_REGULATION TLR1 ON TLR1.LEDGER_FLOW_ID = L1.LEDGER_FLOW_ID");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID");
            sbSql.Append(" LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0");
            sbSql.Append(" WHERE");
            sbSql.Append(" L1.DEL_FLAG = 0 AND L1.TEMP_SAVED_FLAG = 0 ");
            sbSql.Append(" AND");
            sbSql.Append(" VR1.FIRE_NAME LIKE '%第%類%'");

            //sbSql.Append(" UNION ALL SELECT  DISTINCT L1.REG_NO,FORMAT(L1.RECENT_LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID,VLOC1.LOC_NAME,VLOC1.LOC_BARCODE,MC1.CHEM_NAME,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT ");
            //sbSql.Append(" ,SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2,ISNULL(MR1.FIRE_SIZE,(SELECT MR2.FIRE_SIZE FROM M_REGULATION MR2 WHERE MR2.REG_TYPE_ID = MR1.P_REG_TYPE_ID)) AS FIRE_SIZE");
            //sbSql.Append(" ,IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float,LOC1.AMOUNT),'0.00'),FORMAT(convert(float,US1.AMOUNT),'0.00')) AS AMOUNT_BASE ,IIF(UPPER(U2.UNIT_NAME) = 'KG',CASE WHEN  " + strSQL_L + " THEN");
            //sbSql.Append("   IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00'),FORMAT(convert(float, US1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')) ");
            //sbSql.Append(" ELSE IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00'),FORMAT(convert(float, US1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')) ");
            //sbSql.Append(" END,CASE WHEN  " + strSQL_KG + " THEN IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00'), ");
            //sbSql.Append("   FORMAT(convert(float, US1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00'))");
            //sbSql.Append(" ELSE IIF(LOC1.AMOUNT IS NOT NULL,FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00'),FORMAT(convert(float, US1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')) END) ");
            //sbSql.Append(" AS AMOUNT,U1.UNIT_NAME,G1.GROUP_NAME ,(SELECT US1.USER_NAME + ';' FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD WHERE C1.LOC_ID = LOC1.LOC_ID");
            //sbSql.Append(" FOR XML PATH('') ) AS USER_NAME ,(SELECT US1.TEL + ';' FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD WHERE C1.LOC_ID = LOC1.LOC_ID FOR XML PATH('') ) AS TEL");

            //sbSql.Append(" FROM T_MGMT_LEDGER L1 LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD LEFT JOIN T_MGMT_LED_LOC LOC1 ON L1.REG_NO = LOC1.REG_NO INNER JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID");
            //sbSql.Append(" LEFT JOIN T_MGMT_LED_USAGE_LOC US1 ON L1.REG_NO = US1.REG_NO LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0 LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD ");
            //sbSql.Append(" LEFT JOIN T_LED_REGULATION TLR1 ON TLR1.REG_NO = L1.REG_NO LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0 ");
            //sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0 LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0 ");
            //sbSql.Append(" WHERE L1.DEL_FLAG = 0 AND VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND US1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND (LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ") OR US1.LOC_ID IN  (" + inventory.hdnLocationIds + "))");
            }
            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND VLOC1.LOC_BARCODE = @LOC_BARCODE");
                _params.Add("@LOC_BARCODE", inventory.txtBarcode);
            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds.ToString()) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%'+@REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like @REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%' @REPORT_NAME" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME = @REPORT_NAME");
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));
                }
            }

            sbSql.Append(" and not exists ");
            sbSql.Append(" (select 1 ");
            sbSql.Append(" FROM T_LEDGER_WORK L1");
            sbSql.Append(" LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD");
            sbSql.Append(" LEFT JOIN T_LED_WORK_LOC LOC1 ON L1.LEDGER_FLOW_ID = LOC1.LEDGER_FLOW_ID");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD");
            sbSql.Append(" LEFT JOIN T_LEDGER_WORK_REGULATION TLR1 ON TLR1.LEDGER_FLOW_ID = L1.LEDGER_FLOW_ID");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID");
            sbSql.Append(" LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0");
            sbSql.Append(" WHERE");
            sbSql.Append(" L1.DEL_FLAG = 0 AND L1.TEMP_SAVED_FLAG = 0 ");
            sbSql.Append(" AND");
            sbSql.Append(" VR1.FIRE_NAME LIKE '%第%類%'");


            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND US1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND (LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ") OR US1.LOC_ID IN  (" + inventory.hdnLocationIds + "))");
            }
            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND VLOC1.LOC_BARCODE = @LOC_BARCODE");
                _params.Add("@LOC_BARCODE", inventory.txtBarcode);
            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds.ToString()) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%'+@REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like @REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%' @REPORT_NAME" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME = @REPORT_NAME");
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));
                }
            }

            sbSql.Append(" ) ");

            sbSql.Append(" UNION ALL ");
            sbSql.Append(" SELECT  DISTINCT L1.REG_NO,FORMAT(L1.RECENT_LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID");
            sbSql.Append(",VLOC1.LOC_NAME AS LOC_NAME");
            sbSql.Append(",VLOC1.LOC_BARCODE AS LOC_BARCODE");
            sbSql.Append(" ,MC1.CHEM_NAME");
            sbSql.Append(" ,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT");
            sbSql.Append(" ,SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2");
            sbSql.Append(" ,ISNULL(MR1.FIRE_SIZE,(SELECT MR2.FIRE_SIZE FROM M_REGULATION MR2 WHERE MR2.REG_TYPE_ID = MR1.P_REG_TYPE_ID)) AS FIRE_SIZE");
            sbSql.Append("     ,FORMAT(convert(float,LOC1.AMOUNT),'0.00') AS AMOUNT_BASE");
            sbSql.Append(",IIF(UPPER(U2.UNIT_NAME) = 'KG',");
            sbSql.Append("CASE ");
            sbSql.Append(" WHEN  " + strSQL_L + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append("END ");
            sbSql.Append(",");
            sbSql.Append("CASE");
            sbSql.Append(" WHEN  " + strSQL_KG + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append(" END");
            sbSql.Append(")");
            sbSql.Append(" AS AMOUNT");
            sbSql.Append(" ,U1.UNIT_NAME");
            sbSql.Append(" ,U2.UNIT_NAME AS REGULATION_UNITNAME");
            sbSql.Append(" ,G1.GROUP_NAME");
            sbSql.Append(" ,(SELECT US1.USER_NAME + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS USER_NAME");
            sbSql.Append(" ,(SELECT US1.TEL + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS TEL");
            sbSql.Append(" FROM T_MGMT_LEDGER L1");
            sbSql.Append(" LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD");
            sbSql.Append(" LEFT JOIN T_MGMT_LED_LOC LOC1 ON L1.REG_NO = LOC1.REG_NO");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD");
            sbSql.Append(" LEFT JOIN T_LED_REGULATION TLR1 ON TLR1.REG_NO = L1.REG_NO");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID");
            sbSql.Append(" LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0");
            sbSql.Append(" WHERE");
            sbSql.Append(" L1.DEL_FLAG = 0");
            sbSql.Append(" AND");
            sbSql.Append(" VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND US1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND (LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ") OR US1.LOC_ID IN  (" + inventory.hdnLocationIds + "))");
            }
            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND VLOC1.LOC_BARCODE = @LOC_BARCODE");
                _params.Add("@LOC_BARCODE", inventory.txtBarcode);
            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds.ToString()) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%'+@REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like @REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%' @REPORT_NAME" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME = @REPORT_NAME");
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));
                }
            }

            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 0 )");
            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 1 AND T_LEDGER_WORK.APPLI_CLASS = 1)");


            sbSql.Append(" UNION ");

            sbSql.Append(" SELECT  DISTINCT L1.REG_NO,FORMAT(L1.RECENT_LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID");
            sbSql.Append(",VLOC1.LOC_NAME AS LOC_NAME");
            sbSql.Append(",VLOC1.LOC_BARCODE AS LOC_BARCODE");
            sbSql.Append(" ,MC1.CHEM_NAME");
            sbSql.Append(" ,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT");
            sbSql.Append(" ,SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2");
            sbSql.Append(" ,ISNULL(MR1.FIRE_SIZE,(SELECT MR2.FIRE_SIZE FROM M_REGULATION MR2 WHERE MR2.REG_TYPE_ID = MR1.P_REG_TYPE_ID)) AS FIRE_SIZE");
            sbSql.Append("     ,FORMAT(convert(float,LOC1.AMOUNT),'0.00') AS AMOUNT_BASE");
            sbSql.Append(",IIF(UPPER(U2.UNIT_NAME) = 'KG',");
            sbSql.Append("CASE ");
            sbSql.Append(" WHEN  " + strSQL_L + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append("END ");
            sbSql.Append(",");
            sbSql.Append("CASE");
            sbSql.Append(" WHEN  " + strSQL_KG + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append(" END");
            sbSql.Append(")");
            sbSql.Append(" AS AMOUNT");
            sbSql.Append(" ,U1.UNIT_NAME");
            sbSql.Append(" ,U2.UNIT_NAME AS REGULATION_UNITNAME");
            sbSql.Append(" ,G1.GROUP_NAME");
            sbSql.Append(" ,(SELECT US1.USER_NAME + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS USER_NAME");
            sbSql.Append(" ,(SELECT US1.TEL + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS TEL");
            sbSql.Append(" FROM T_MGMT_LEDGER L1");
            sbSql.Append(" LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD");
            sbSql.Append(" LEFT JOIN T_MGMT_LED_USAGE_LOC LOC1 ON L1.REG_NO = LOC1.REG_NO");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD");
            sbSql.Append(" LEFT JOIN T_LED_REGULATION TLR1 ON TLR1.REG_NO = L1.REG_NO");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID");
            sbSql.Append(" LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0");
            sbSql.Append(" WHERE");
            sbSql.Append(" L1.DEL_FLAG = 0");
            sbSql.Append(" AND");
            sbSql.Append(" VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND US1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND (LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ") OR US1.LOC_ID IN  (" + inventory.hdnLocationIds + "))");
            }
            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND VLOC1.LOC_BARCODE = @LOC_BARCODE");
                _params.Add("@LOC_BARCODE", inventory.txtBarcode);
            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds.ToString()) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%'+@REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like @REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%' @REPORT_NAME" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME = @REPORT_NAME");
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));
                }
            }

            sbSql.Append(" and not exists ");
            sbSql.Append(" (select 1 ");
            sbSql.Append(" FROM T_MGMT_LEDGER L1");
            sbSql.Append(" LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD");
            sbSql.Append(" LEFT JOIN T_MGMT_LED_LOC LOC1 ON L1.REG_NO = LOC1.REG_NO");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD");
            sbSql.Append(" LEFT JOIN T_LED_REGULATION TLR1 ON TLR1.REG_NO = L1.REG_NO");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID");
            sbSql.Append(" LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0");
            sbSql.Append(" WHERE");
            sbSql.Append(" L1.DEL_FLAG = 0");
            sbSql.Append(" AND");
            sbSql.Append(" VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND US1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND (LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ") OR US1.LOC_ID IN  (" + inventory.hdnLocationIds + "))");
            }
            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND VLOC1.LOC_BARCODE = @LOC_BARCODE");
                _params.Add("@LOC_BARCODE", inventory.txtBarcode);
            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds.ToString()) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%'+@REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like @REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%' @REPORT_NAME" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME = @REPORT_NAME");
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName));
                }
            }

            sbSql.Append(" ) ");

            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 0 )");
            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 1 AND T_LEDGER_WORK.APPLI_CLASS = 1)");
            var dt = DbUtil.Select<InventoryManagement>(sbSql.ToString(), _params);
            return dt;
        }

        public List<InventoryManagement> DoSearchSheet2(InventoryManagement inventory)
        {
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbSql = new StringBuilder();
            DynamicParameters _params = new DynamicParameters();
            //DataTable dt;
            Dictionary<string, string> dic = (Dictionary<string, string>)HttpContext.Current.Session["CONVERT_UNIT"];

            string strSQL_KG = string.Empty;
            string strSQL_L = string.Empty;

            foreach (KeyValuePair<string, string> item in dic)
            {
                if (item.Value.ToUpper() == "KG")
                {
                    strSQL_KG += " U1.UNIT_NAME = '" + item.Key + "' or ";
                }
                else if (item.Value.ToUpper() == "L")
                {
                    strSQL_L += " U1.UNIT_NAME = '" + item.Key + "' or ";
                }
            }
            strSQL_KG += "U1.UNIT_NAME = 'Kg'";
            strSQL_L += "U1.UNIT_NAME = 'L'";


            // SELECT SQL生成
            sbSql.Append(" SELECT  MAIN.FIRE_ID AS REG_TYPE_ID,MAIN.REG_TEXT,MAIN.REG_TEXT2,FORMAT(convert(float,sum(cast(MAIN.AMOUNT as float))),'0.00') AS AMOUNT FROM ( SELECT  DISTINCT L1.REG_NO,FORMAT(L1.LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID ");
            sbSql.Append(",VLOC1.LOC_NAME AS LOC_NAME,VLOC1.LOC_BARCODE AS LOC_BARCODE,MC1.CHEM_NAME,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT,SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2");

            sbSql.Append(" ,MR1.FIRE_SIZE AS FIRE_SIZE,VR1.FIRE_ID,IIF(UPPER(U2.UNIT_NAME) = 'KG',CASE WHEN  " + strSQL_L + " THEN FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00') END ,");
            sbSql.Append("CASE WHEN  " + strSQL_KG + " THEN FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00') ELSE ");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00') END) ");
            sbSql.Append(" AS AMOUNT,U1.UNIT_NAME,G1.GROUP_NAME,(SELECT US1.USER_NAME + ';' FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD WHERE C1.LOC_ID = LOC1.LOC_ID FOR XML PATH('')) AS USER_NAME ");
            sbSql.Append(" ,(SELECT US1.TEL + ';' FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD WHERE C1.LOC_ID = LOC1.LOC_ID FOR XML PATH('')) AS TEL");
            sbSql.Append(" FROM T_LEDGER_WORK L1 LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD LEFT JOIN T_LED_WORK_LOC LOC1 ON L1.LEDGER_FLOW_ID = LOC1.LEDGER_FLOW_ID LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID ");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0 LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD LEFT JOIN T_LEDGER_WORK_REGULATION TLR1 ON TLR1.LEDGER_FLOW_ID = L1.LEDGER_FLOW_ID");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0 LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0 WHERE ");
            sbSql.Append(" L1.DEL_FLAG = 0 AND L1.TEMP_SAVED_FLAG = 0 AND VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ")");
            }

            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND VLOC1.LOC_BARCODE = @LOC_BARCODE_WORK");
                _params.Add("@LOC_BARCODE_WORK", inventory.txtBarcode.Trim());

            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds.Trim()) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%'+@REPORT_NAME_WORK+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like @REPORT_NAME_WORK+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%' +@REPORT_NAME_WORK" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME = @REPORT_NAME_WORK");
                    _params.Add("@REPORT_NAME_WORK", inventory.txtReportName.Trim());

                }
            }

            sbSql.Append(" UNION ");

            sbSql.Append(" SELECT  DISTINCT L1.REG_NO,FORMAT(L1.LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID,VLOC1.LOC_NAME AS LOC_NAME,VLOC1.LOC_BARCODE AS LOC_BARCODE,MC1.CHEM_NAME,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT");
            sbSql.Append(" ,SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2,MR1.FIRE_SIZE AS FIRE_SIZE,VR1.FIRE_ID,IIF(UPPER(U2.UNIT_NAME) = 'KG',CASE");
            sbSql.Append(" WHEN  " + strSQL_L + " THEN FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00') ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00') END,CASE WHEN  " + strSQL_KG + " THEN ");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00') END)");
            sbSql.Append(" AS AMOUNT,U1.UNIT_NAME,G1.GROUP_NAME,(SELECT US1.USER_NAME + ';' FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD WHERE C1.LOC_ID = LOC1.LOC_ID FOR XML PATH('') ) AS USER_NAME ");
            sbSql.Append(" ,(SELECT US1.TEL + ';' FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD WHERE C1.LOC_ID = LOC1.LOC_ID FOR XML PATH('') ) AS TEL ");
            sbSql.Append(" FROM T_LEDGER_WORK L1 LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD LEFT JOIN T_LED_WORK_USAGE_LOC LOC1 ON L1.LEDGER_FLOW_ID = LOC1.LEDGER_FLOW_ID LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID ");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0 LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD LEFT JOIN T_LEDGER_WORK_REGULATION TLR1 ON TLR1.LEDGER_FLOW_ID = L1.LEDGER_FLOW_ID");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0 LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0 ");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0 WHERE L1.DEL_FLAG = 0 AND L1.TEMP_SAVED_FLAG = 0 AND VR1.FIRE_NAME LIKE '%第%類%' ");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ")");
            }

            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND VLOC1.LOC_BARCODE = @LOC_BARCODE_WORK2");
                _params.Add("@LOC_BARCODE_WORK2", inventory.txtBarcode.Trim());

            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%'+@REPORT_NAME_WORK2+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK2", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like @REPORT_NAME_WORK2+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK2", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%' +@REPORT_NAME_WORK2" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK2", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME = @REPORT_NAME_WORK2");
                    _params.Add("@REPORT_NAME_WORK2", inventory.txtReportName.Trim());
                }
            }

            sbSql.Append(" and not exists (select 1 FROM T_LEDGER_WORK L1 LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD LEFT JOIN T_LED_WORK_LOC LOC1 ON L1.LEDGER_FLOW_ID = LOC1.LEDGER_FLOW_ID ");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0 LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD ");
            sbSql.Append(" LEFT JOIN T_LEDGER_WORK_REGULATION TLR1 ON TLR1.LEDGER_FLOW_ID = L1.LEDGER_FLOW_ID LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0 ");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0 LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0 ");
            sbSql.Append(" WHERE L1.DEL_FLAG = 0 AND L1.TEMP_SAVED_FLAG = 0 AND VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ")");
            }

            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND VLOC1.LOC_BARCODE = @LOC_BARCODE_WORK3");
                _params.Add("@LOC_BARCODE_WORK3", inventory.txtBarcode.Trim());

            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%'+@REPORT_NAME_WORK3+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK3", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like @REPORT_NAME_WORK3+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK3", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME like '%' +@REPORT_NAME_WORK3" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME_WORK3", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND FIR_REP.REPORT_NAME = @REPORT_NAME_WORK3");
                    _params.Add("@REPORT_NAME_WORK3", inventory.txtReportName.Trim());

                }
            }
            sbSql.Append(" ) ");

            sbSql.Append(" UNION ALL ");

            sbSql.Append(" SELECT  DISTINCT L1.REG_NO,FORMAT(L1.RECENT_LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID ,VLOC1.LOC_NAME AS LOC_NAME,VLOC1.LOC_BARCODE AS LOC_BARCODE,MC1.CHEM_NAME,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT ");
            sbSql.Append(" ,SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2,MR1.FIRE_SIZE AS FIRE_SIZE,VR1.FIRE_ID,IIF(UPPER(U2.UNIT_NAME) = 'KG',CASE");
            sbSql.Append(" WHEN  " + strSQL_L + " THEN FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00') ELSE ");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00') END ,CASE WHEN  " + strSQL_KG + " THEN FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00') ");
            sbSql.Append(" ELSE FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00') END) AS AMOUNT ,U1.UNIT_NAME,G1.GROUP_NAME ,(SELECT US1.USER_NAME + ';' ");
            sbSql.Append(" FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD WHERE C1.LOC_ID = LOC1.LOC_ID FOR XML PATH('')) AS USER_NAME ,(SELECT US1.TEL + ';' FROM M_CHIEF C1 LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD ");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID FOR XML PATH('')) AS TEL FROM T_MGMT_LEDGER L1 LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD LEFT JOIN T_MGMT_LED_LOC LOC1 ON L1.REG_NO = LOC1.REG_NO");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0 LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD");
            sbSql.Append(" LEFT JOIN T_LED_REGULATION TLR1 ON TLR1.REG_NO = L1.REG_NO LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0 LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0");
            sbSql.Append(" WHERE L1.DEL_FLAG = 0 AND VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ")");
            }
            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND");
                sbSql.Append(" VLOC1.LOC_BARCODE = @LOC_BARCODE");
                _params.Add("@LOC_BARCODE", inventory.txtBarcode.Trim());
            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND");
                sbSql.Append(" G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%'+@REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like @REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%' +@REPORT_NAME" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME = @REPORT_NAME");
                    _params.Add("@REPORT_NAME", inventory.txtReportName.Trim());

                }
            }

            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 0 )");
            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 1 AND T_LEDGER_WORK.APPLI_CLASS = 1)");

            sbSql.Append(" UNION ");

            sbSql.Append(" SELECT  DISTINCT L1.REG_NO,FORMAT(L1.RECENT_LEDGER_FLOW_ID,'0000000000') AS RECENT_LEDGER_FLOW_ID");
            sbSql.Append(",VLOC1.LOC_NAME AS LOC_NAME");
            sbSql.Append(",VLOC1.LOC_BARCODE AS LOC_BARCODE");
            sbSql.Append(" ,MC1.CHEM_NAME");
            sbSql.Append(" ,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT");
            sbSql.Append(" ,SUBSTRING(VR1.FIRE_NAME,CHARINDEX('類/',VR1.FIRE_NAME) + 2,100) AS REG_TEXT2");
            sbSql.Append(" ,MR1.FIRE_SIZE AS FIRE_SIZE,VR1.FIRE_ID");
            sbSql.Append(",IIF(UPPER(U2.UNIT_NAME) = 'KG',");
            sbSql.Append("CASE ");
            sbSql.Append(" WHEN  " + strSQL_L + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) * convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append("END ");
            sbSql.Append(",");
            sbSql.Append("CASE");
            sbSql.Append(" WHEN  " + strSQL_KG + " THEN");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)) / convert(float, ISNULL(MC1.DENSITY, 1)), '0.00')");
            sbSql.Append(" ELSE");
            sbSql.Append("   FORMAT(convert(float, LOC1.AMOUNT) * convert(float, ISNULL(U1.FACTOR, 1)), '0.00')");
            sbSql.Append(" END");
            sbSql.Append(")");
            sbSql.Append(" AS AMOUNT");
            sbSql.Append(" ,U1.UNIT_NAME,G1.GROUP_NAME");
            sbSql.Append(" ,(SELECT US1.USER_NAME + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS USER_NAME");
            sbSql.Append(" ,(SELECT US1.TEL + ';'");
            sbSql.Append(" FROM M_CHIEF C1");
            sbSql.Append(" LEFT JOIN M_USER US1 ON C1.USER_CD = US1.USER_CD");
            sbSql.Append(" WHERE C1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" FOR XML PATH('')");
            sbSql.Append(" ) AS TEL");
            sbSql.Append(" FROM T_MGMT_LEDGER L1");
            sbSql.Append(" LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD");
            sbSql.Append(" LEFT JOIN T_MGMT_LED_USAGE_LOC LOC1 ON L1.REG_NO = LOC1.REG_NO");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD");
            sbSql.Append(" LEFT JOIN T_LED_REGULATION TLR1 ON TLR1.REG_NO = L1.REG_NO");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID");
            sbSql.Append(" LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0");
            sbSql.Append(" WHERE");
            sbSql.Append(" L1.DEL_FLAG = 0");
            sbSql.Append(" AND");
            sbSql.Append(" VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ")");
            }
            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND");
                sbSql.Append(" VLOC1.LOC_BARCODE = @LOC_BARCODE2");
                _params.Add("@LOC_BARCODE2", inventory.txtBarcode.Trim());
            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND");
                sbSql.Append(" G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%'+@REPORT_NAME2+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME2", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like @REPORT_NAME2+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME2", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%' +@REPORT_NAME2" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME2", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME = @REPORT_NAME2");
                    _params.Add("@REPORT_NAME2", inventory.txtReportName.Trim());

                }
            }

            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 ");
            sbSql.Append(" FROM T_MGMT_LEDGER L1");
            sbSql.Append(" LEFT JOIN M_CHEM MC1 ON L1.CHEM_CD = MC1.CHEM_CD");
            sbSql.Append(" LEFT JOIN T_MGMT_LED_LOC LOC1 ON L1.REG_NO = LOC1.REG_NO");
            sbSql.Append(" LEFT JOIN V_LOCATION VLOC1 ON VLOC1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U1 ON LOC1.UNITSIZE_ID = U1.UNITSIZE_ID AND U1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN V_GROUP G1 ON L1.GROUP_CD = G1.GROUP_CD");
            sbSql.Append(" LEFT JOIN T_LED_REGULATION TLR1 ON TLR1.REG_NO = L1.REG_NO");
            sbSql.Append(" LEFT JOIN V_FIRE VR1 ON TLR1.REG_TYPE_ID = VR1.FIRE_ID");
            sbSql.Append(" LEFT JOIN M_REGULATION MR1 ON TLR1.REG_TYPE_ID = MR1.REG_TYPE_ID AND MR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U2 ON U2.UNITSIZE_ID = MR1.UNITSIZE_ID AND U2.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FIR1 ON FIR1.LOC_ID = VLOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FIR_REP ON FIR_REP.REPORT_ID = FIR1.REPORT_ID AND FIR_REP.DEL_FLAG = 0");
            sbSql.Append(" WHERE");
            sbSql.Append(" L1.DEL_FLAG = 0");
            sbSql.Append(" AND");
            sbSql.Append(" VR1.FIRE_NAME LIKE '%第%類%'");

            if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "0")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "1")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC1.LOC_ID IN  (" + inventory.hdnLocationIds + ")");
            }
            else if (inventory.hdnLocationIds != "" && inventory.ddlLocation == "2")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC1.LOC_ID IN (" + inventory.hdnLocationIds + ")");
            }
            if (inventory.txtBarcode != null)
            {
                sbSql.Append(" AND");
                sbSql.Append(" VLOC1.LOC_BARCODE = @LOC_BARCODE3");
                _params.Add("@LOC_BARCODE3", inventory.txtBarcode.Trim());
            }

            if (inventory.hdnGroupIds != "")
            {
                sbSql.Append(" AND");
                sbSql.Append(" G1.GROUP_CD IN  (" + JoinSingle(inventory.hdnGroupIds) + ")");
            }

            if (inventory.txtReportName != null)
            {
                if (inventory.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%'+@REPORT_NAME3+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME3", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "1")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like @REPORT_NAME3+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME3", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "2")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME like '%' +@REPORT_NAME3" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME3", DbUtil.ConvertIntoEscapeChar(inventory.txtReportName.Trim()));

                }
                else if (inventory.ddlType == "3")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FIR_REP.REPORT_NAME = @REPORT_NAME3");
                    _params.Add("REPORT_NAME3", inventory.txtReportName.Trim());

                }
            }
            sbSql.Append(" ) ");

            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 0 )");
            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 1 AND T_LEDGER_WORK.APPLI_CLASS = 1)");

            sbSql.Append(" ) MAIN ");
            sbSql.Append(" GROUP BY MAIN.FIRE_ID,MAIN.REG_TEXT,MAIN.REG_TEXT2");

            var dt1 = DbUtil.Select<InventoryManagement>(sbSql.ToString(), _params);

            return dt1;
        }

        public List<InventoryManagement> DoSearchSheet3(InventoryManagement inventoryManagement)
        {
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbSql = new StringBuilder();
            DynamicParameters _params = new DynamicParameters();
            //DataTable dt;

            // SELECT SQL生成
            sbSql.Append(" SELECT A1.FIRE_ID,A1.FIRE_SIZE,A1.REG_TEXT,A1.REG_TEXT2,SUM(A1.FIRE_TOTAL) AS FIRE_TOTAL FROM");
            sbSql.Append(" (");
            sbSql.Append(" SELECT DISTINCT  T1.FIRE_ID,T1.FIRE_SIZE,T1.REG_TEXT,T1.REG_TEXT2,T1.REPORT_ID,T1.FIRE_TOTAL");
            sbSql.Append(" FROM");
            sbSql.Append(" (SELECT M1.FIRE_ID,M1.FIRE_SIZE,M1.REG_TEXT,M1.REG_TEXT2,FR1.REPORT_ID,SUM(FL1.FIRE_SIZE) AS FIRE_TOTAL");
            sbSql.Append(" FROM");
            sbSql.Append(" (SELECT V1.FIRE_ID,M1.FIRE_SIZE,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT");
            sbSql.Append(" ,SUBSTRING(V1.FIRE_NAME,CHARINDEX('類/',V1.FIRE_NAME) + 2,100) AS REG_TEXT2,M1.SORT_ORDER");
            sbSql.Append(" FROM");
            sbSql.Append(" V_FIRE V1");
            sbSql.Append(" LEFT JOIN M_REGULATION M1 ON V1.FIRE_ID = M1.REG_TYPE_ID");
            sbSql.Append(" WHERE");
            sbSql.Append(" V1.DEL_FLAG = 0");
            sbSql.Append(" AND");
            sbSql.Append(" V1.FIRE_NAME LIKE '%第%類%'");
            sbSql.Append(" AND M1.FIRE_SIZE IS NOT NULL");
            sbSql.Append(" ) M1");
            sbSql.Append(" LEFT JOIN");
            sbSql.Append(" (SELECT F.REPORT_ID,F.REG_TYPE_ID,F.FIRE_SIZE");
            sbSql.Append(" FROM M_FIRE_SIZE F");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION L ON F.REPORT_ID = L.REPORT_ID");
            sbSql.Append(" GROUP BY F.REPORT_ID,F.REG_TYPE_ID,F.FIRE_SIZE");
            sbSql.Append(" ) FL1");
            sbSql.Append(" ON M1.FIRE_ID = FL1.REG_TYPE_ID");
            sbSql.Append(" INNER JOIN M_FIRE_REPORT FR1 ON FL1.REPORT_ID = FR1.REPORT_ID AND FR1.DEL_FLAG = 0");
            sbSql.Append(" GROUP BY M1.FIRE_ID,M1.FIRE_SIZE,M1.REG_TEXT,M1.REG_TEXT2,FR1.REPORT_ID");
            sbSql.Append(" ) T1");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FR1 ON T1.REPORT_ID = FR1.REPORT_ID AND FR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FL1 ON FR1.REPORT_ID = FL1.REPORT_ID");
            sbSql.Append(" LEFT JOIN T_LED_WORK_LOC LOC1 ON FL1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_LOCATION LOC3 ON LOC3.LOC_ID = LOC1.LOC_ID AND LOC3.CLASS_ID = 1");
            sbSql.Append(" LEFT JOIN T_LED_WORK_USAGE_LOC LOC2 ON FL1.LOC_ID = LOC2.LOC_ID");
            sbSql.Append(" LEFT JOIN M_LOCATION LOC4 ON LOC4.LOC_ID = LOC2.LOC_ID AND LOC4.CLASS_ID = 2");
            sbSql.Append(" LEFT JOIN T_LEDGER_WORK L1 ON L1.LEDGER_FLOW_ID = LOC1.LEDGER_FLOW_ID");
            sbSql.Append(" LEFT JOIN M_GROUP MG1 ON L1.GROUP_CD = MG1.GROUP_CD AND MG1.DEL_FLAG = 0");

            sbSql.Append(" INNER JOIN (");
            sbSql.Append(" SELECT DISTINCT T_LEDGER_WORK.LEDGER_FLOW_ID");
            sbSql.Append(" FROM T_LEDGER_WORK");
            sbSql.Append(" LEFT JOIN");
            sbSql.Append(" (SELECT LEDGER_FLOW_ID,LOC_ID,AMOUNT,UNITSIZE_ID FROM T_LED_WORK_LOC");
            sbSql.Append(" UNION ");
            sbSql.Append(" SELECT LEDGER_FLOW_ID,LOC_ID,AMOUNT,UNITSIZE_ID FROM T_LED_WORK_USAGE_LOC");
            sbSql.Append(" ) UNION_LOC ");
            sbSql.Append(" ON UNION_LOC.LEDGER_FLOW_ID = T_LEDGER_WORK.LEDGER_FLOW_ID");

            if (inventoryManagement.hdnLocationIds != "")
            {
                sbSql.Append(" WHERE UNION_LOC.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + ")");

            }
            sbSql.Append(") LED1 ON LED1.LEDGER_FLOW_ID = L1.LEDGER_FLOW_ID");

            sbSql.Append(" WHERE L1.DEL_FLAG = 0 ");

            if (inventoryManagement.hdnLocationIds != string.Empty && inventoryManagement.ddlLocation == "0")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC1.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + ")");
            }
            else if (inventoryManagement.hdnLocationIds != string.Empty && inventoryManagement.ddlLocation == "1")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC2.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + ")");
            }
            else if (inventoryManagement.hdnLocationIds != string.Empty && inventoryManagement.ddlLocation == "2")
            {
                sbSql.Append(" AND");
                sbSql.Append(" (LOC1.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + ")");
                sbSql.Append(" OR ");
                sbSql.Append(" LOC2.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + "))");
            }


            if (inventoryManagement.txtBarcode != null)
            {
                sbSql.Append(" AND");
                sbSql.Append(" (LOC3.LOC_BARCODE = @LOC_BARCODE1 OR LOC4.LOC_BARCODE = @LOC_BARCODE2)");
                _params.Add("@LOC_BARCODE1", inventoryManagement.txtBarcode.Trim());
                _params.Add("@LOC_BARCODE2", inventoryManagement.txtBarcode.Trim());
            }

            if (inventoryManagement.hdnGroupIds != "")
            {
                sbSql.Append(" AND");
                sbSql.Append(" MG1.GROUP_CD IN  (" + JoinSingle(inventoryManagement.hdnGroupIds.ToString()) + ")");
            }

            if (inventoryManagement.txtReportName != null)
            {
                if (inventoryManagement.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FR1.REPORT_NAME like '%'+@REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventoryManagement.txtReportName.Trim()));

                }
                else if (inventoryManagement.ddlType == "1")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FR1.REPORT_NAME like @REPORT_NAME+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventoryManagement.txtReportName.Trim()));

                }
                else if (inventoryManagement.ddlType == "2")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FR1.REPORT_NAME like '%' + @REPORT_NAME" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventoryManagement.txtReportName.Trim()));

                }
                else if (inventoryManagement.ddlType == "3")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FR1.REPORT_NAME = @REPORT_NAME");
                    _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(inventoryManagement.txtReportName.Trim()));

                }
            }

            sbSql.Append(" UNION ");

            sbSql.Append(" SELECT DISTINCT T1.FIRE_ID,T1.FIRE_SIZE,T1.REG_TEXT,T1.REG_TEXT2,T1.REPORT_ID,T1.FIRE_TOTAL");
            sbSql.Append(" FROM");
            sbSql.Append(" (SELECT M1.FIRE_ID,M1.FIRE_SIZE,M1.REG_TEXT,M1.REG_TEXT2,FR1.REPORT_ID,SUM(FL1.FIRE_SIZE) AS FIRE_TOTAL");
            sbSql.Append(" FROM");
            sbSql.Append(" (SELECT V1.FIRE_ID,M1.FIRE_SIZE,SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1) AS REG_TEXT");
            sbSql.Append(" ,SUBSTRING(V1.FIRE_NAME,CHARINDEX('類/',V1.FIRE_NAME) + 2,100) AS REG_TEXT2,M1.SORT_ORDER");
            sbSql.Append(" FROM");
            sbSql.Append(" V_FIRE V1");
            sbSql.Append(" LEFT JOIN M_REGULATION M1 ON V1.FIRE_ID = M1.REG_TYPE_ID");
            sbSql.Append(" WHERE");
            sbSql.Append(" V1.DEL_FLAG = 0");
            sbSql.Append(" AND");
            sbSql.Append(" V1.FIRE_NAME LIKE '%第%類%'");
            sbSql.Append(" AND M1.FIRE_SIZE IS NOT NULL");
            sbSql.Append(" ) M1");
            sbSql.Append(" LEFT JOIN");
            sbSql.Append(" (SELECT F.REPORT_ID,F.REG_TYPE_ID,F.FIRE_SIZE");
            sbSql.Append(" FROM M_FIRE_SIZE F");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION L ON F.REPORT_ID = L.REPORT_ID");
            sbSql.Append(" GROUP BY F.REPORT_ID,F.REG_TYPE_ID,F.FIRE_SIZE");
            sbSql.Append(" ) FL1");
            sbSql.Append(" ON M1.FIRE_ID = FL1.REG_TYPE_ID");
            sbSql.Append(" INNER JOIN M_FIRE_REPORT FR1 ON FL1.REPORT_ID = FR1.REPORT_ID AND FR1.DEL_FLAG = 0");
            sbSql.Append(" GROUP BY M1.FIRE_ID,M1.FIRE_SIZE,M1.REG_TEXT,M1.REG_TEXT2,FR1.REPORT_ID");
            sbSql.Append(" ) T1");
            sbSql.Append(" LEFT JOIN M_FIRE_REPORT FR1 ON T1.REPORT_ID = FR1.REPORT_ID AND FR1.DEL_FLAG = 0");
            sbSql.Append(" LEFT JOIN M_FIRE_LOCATION FL1 ON FR1.REPORT_ID = FL1.REPORT_ID");
            sbSql.Append(" LEFT JOIN T_MGMT_LED_LOC LOC1 ON FL1.LOC_ID = LOC1.LOC_ID");
            sbSql.Append(" LEFT JOIN M_LOCATION LOC3 ON LOC3.LOC_ID = LOC1.LOC_ID AND LOC3.CLASS_ID = 1");
            sbSql.Append(" LEFT JOIN T_MGMT_LED_USAGE_LOC LOC2 ON FL1.LOC_ID = LOC2.LOC_ID");
            sbSql.Append(" LEFT JOIN M_LOCATION LOC4 ON LOC4.LOC_ID = LOC2.LOC_ID AND LOC4.CLASS_ID = 2");
            sbSql.Append(" LEFT JOIN T_MGMT_LEDGER L1 ON L1.REG_NO = LOC1.REG_NO");
            sbSql.Append(" LEFT JOIN M_GROUP MG1 ON L1.GROUP_CD = MG1.GROUP_CD AND MG1.DEL_FLAG = 0");
            sbSql.Append(" INNER JOIN (");
            sbSql.Append(" SELECT DISTINCT T_MGMT_LEDGER.REG_NO");
            sbSql.Append(" FROM T_MGMT_LEDGER");
            sbSql.Append(" LEFT JOIN");
            sbSql.Append(" (SELECT REG_NO,LOC_ID,AMOUNT,UNITSIZE_ID FROM T_MGMT_LED_LOC ");
            sbSql.Append(" UNION ");
            sbSql.Append(" SELECT REG_NO,LOC_ID,AMOUNT,UNITSIZE_ID FROM T_MGMT_LED_USAGE_LOC");
            sbSql.Append(" ) UNION_LOC ");
            sbSql.Append(" ON UNION_LOC.REG_NO = T_MGMT_LEDGER.REG_NO");

            if (inventoryManagement.hdnLocationIds != "")
            {
                sbSql.Append(" WHERE UNION_LOC.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + ")");
            }
            sbSql.Append(") LED1 ON LED1.REG_NO = L1.REG_NO");
            sbSql.Append(" WHERE L1.DEL_FLAG = 0 ");

            if (inventoryManagement.hdnLocationIds != string.Empty && inventoryManagement.ddlLocation == "0")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC1.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + ")");
            }
            else if (inventoryManagement.hdnLocationIds != string.Empty && inventoryManagement.ddlLocation == "1")
            {
                sbSql.Append(" AND");
                sbSql.Append(" LOC2.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + ")");
            }
            else if (inventoryManagement.hdnLocationIds != string.Empty && inventoryManagement.ddlLocation == "2")
            {
                sbSql.Append(" AND");
                sbSql.Append(" (LOC1.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + ")");
                sbSql.Append(" OR ");
                sbSql.Append(" LOC2.LOC_ID IN  (" + inventoryManagement.hdnLocationIds + "))");
            }

            if (inventoryManagement.txtBarcode != null)
            {
                sbSql.Append(" AND");
                sbSql.Append(" (LOC3.LOC_BARCODE = @LOC_BARCODE3 OR LOC4.LOC_BARCODE = @LOC_BARCODE4)");
                _params.Add("@LOC_BARCODE3", inventoryManagement.txtBarcode.Trim());
                _params.Add("@LOC_BARCODE4", inventoryManagement.txtBarcode.Trim());
            }

            if (inventoryManagement.hdnGroupIds != "")
            {
                sbSql.Append(" AND");
                sbSql.Append(" MG1.GROUP_CD IN  (" + JoinSingle(inventoryManagement.hdnGroupIds.ToString()) + ")");
            }

            if (inventoryManagement.txtReportName != null)
            {
                if (inventoryManagement.ddlType == "0")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FR1.REPORT_NAME like '%'+@REPORT_NAME2+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME2", DbUtil.ConvertIntoEscapeChar(inventoryManagement.txtReportName.Trim()));

                }
                else if (inventoryManagement.ddlType == "1")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FR1.REPORT_NAME like @REPORT_NAME2+'%'" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME2", DbUtil.ConvertIntoEscapeChar(inventoryManagement.txtReportName.Trim()));

                }
                else if (inventoryManagement.ddlType == "2")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FR1.REPORT_NAME like '%' +@REPORT_NAME2" + DbUtil.LikeEscapeInfoAdd());
                    _params.Add("@REPORT_NAME2", DbUtil.ConvertIntoEscapeChar(inventoryManagement.txtReportName.Trim()));

                }
                else if (inventoryManagement.ddlType == "3")
                {
                    sbSql.Append(" AND");
                    sbSql.Append("  FR1.REPORT_NAME = @REPORT_NAME2");
                    _params.Add("@REPORT_NAME2", DbUtil.ConvertIntoEscapeChar(inventoryManagement.txtReportName.Trim()));

                }
            }
            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 0 )");
            sbSql.Append(" and not exists");
            sbSql.Append(" (select 1 from T_LEDGER_WORK where T_LEDGER_WORK.REG_NO = L1.REG_NO AND T_LEDGER_WORK.TEMP_SAVED_FLAG = 1 AND T_LEDGER_WORK.APPLI_CLASS = 1)");

            sbSql.Append(" ) A1");
            sbSql.Append(" GROUP BY A1.FIRE_ID,A1.FIRE_SIZE,A1.REG_TEXT,A1.REG_TEXT2");

            List<InventoryManagement> dt = DbUtil.Select<InventoryManagement>(sbSql.ToString(), _params);

            return dt;
        }
        private string JoinSingle(string strText)
        {
            string strJoinText = string.Empty;
            if (strText != null)
            {
                string[] strSplit = strText.Split(',');

                foreach (string str in strSplit)
                {
                    strJoinText += ",'" + str + "'";
                }

                if (strJoinText.Length > 0)
                {
                    strJoinText = strJoinText.Substring(1);
                }
            }
            return strJoinText;
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~InventoryDao() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

        public InventoryManagement getLookupValues()
        {
            InventoryManagement inventorymanagement = new InventoryManagement();
            try
            {
                // CHEM_SUB_NAME Condition
                List<SelectListItem> listCSNCondition = new List<SelectListItem> { };
                new SearchConditionListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listCSNCondition.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventorymanagement.LookUpCSNCondition = listCSNCondition;

                // MAKER_NAME Condition
                List<SelectListItem> listMakerCondition = new List<SelectListItem> { };
                new SearchConditionListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listMakerCondition.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventorymanagement.LookUpMakerNameCondition = listMakerCondition;

                // CHEM_SUB_CODE Condition
                List<SelectListItem> listCSCodeCondition = new List<SelectListItem> { };
                new SearchConditionListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listCSCodeCondition.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventorymanagement.LookUpCSCodeCondition = listCSCodeCondition;

                // BOTTLE_NUM Condition
                List<SelectListItem> listBottleNumCondition = new List<SelectListItem> { };
                new SearchConditionListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listBottleNumCondition.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventorymanagement.LookUpBottleNumCondition = listBottleNumCondition;

                // ACCEPTED_BY Condition
                List<SelectListItem> listAcceptCondition = new List<SelectListItem> { };
                new SearchConditionListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listAcceptCondition.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventorymanagement.LookUpAcceptCondition = listAcceptCondition;

                // INVENTORY_MakerName
                List<M_MAKER> dtMaker = InitMakerComboBox();
                inventorymanagement.LookUpMakerName = makerBindLookupValues(dtMaker);

                // INVENTORY_Status
                List<M_STATUS> dtStatus = InitStatusList();
                inventorymanagement.LookUpStatus = BindLookupValues(dtStatus);

                List<SelectListItem> listunitsize = new List<SelectListItem> { };
                new UnitSizeMasterInfo().GetSelectList().ToList().
                  ForEach(x => listunitsize.Add(new SelectListItem
                  { Value = x.Id.ToString(), Text = x.Name }));
                inventorymanagement.LookUpAmountUnitSize = listunitsize;


                List<SelectListItem> lookupincludes = new List<SelectListItem> { };
                new IncludesContainedListMasterInfo().GetSelectList().ToList().
                   ForEach(x => lookupincludes.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventorymanagement.LookUpInclude = lookupincludes;

                DataTable dt = new DataTable();
                dt.Columns.Add("Text");
                dt.Columns.Add("Val");
                dt.Rows.Add("保管場所", "0");
                dt.Rows.Add("使用場所", "1");
                dt.Rows.Add("全て", "2");
                inventorymanagement.LookUpDDLLocation = Common.BindLookupValues(dt);


            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return inventorymanagement;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019
        /// Description : This  Method is for getting status list
        /// <summary>
        public List<M_STATUS> InitStatusList()
        {
            MasterService masterService = new MasterService();
            List<M_STATUS> dtStatus = masterService.selectStatusList(2);
            return dtStatus;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019
        /// Description : This  Method is for getting maker list
        /// <summary>
        public List<M_USER_MAKER> InitUserMakerList(string userCD)
        {
            MasterService masterService = new MasterService();
            List<M_USER_MAKER> dtMaker = masterService.selectUserMakerList(userCD);
            return dtMaker;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019
        /// Description : This  Method is for getting combobox list
        /// <summary>
        public List<M_MAKER> InitMakerComboBox()
        {
            MasterService masterService = new MasterService();
            List<M_MAKER> dtMaker = masterService.selectMakerList();
            return dtMaker;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019
        /// Description : This  Method is for converting model list to Ienumerable list
        /// <summary>
        public IEnumerable<SelectListItem> BindLookupValues(List<M_STATUS> status)
        {
            IEnumerable<SelectListItem> selectlist = from s in status
                                                     select new SelectListItem
                                                     {
                                                         Text = s.STATUS_TEXT,
                                                         Value = s.STATUS_ID.ToString()
                                                     };

            return selectlist;
        }

        public IEnumerable<SelectListItem> makerBindLookupValues(List<M_MAKER> maker)
        {
            IEnumerable<SelectListItem> selectlist = from s in maker
                                                     select new SelectListItem
                                                     {
                                                         Text = s.MAKER_NAME,
                                                         Value = s.MAKER_ID.ToString()
                                                     };

            return selectlist;
        }

    }
}