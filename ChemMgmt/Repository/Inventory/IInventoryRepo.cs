﻿using ChemMgmt.Models;
using ChemMgmt.Nikon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;
using ZyCoaG.Nikon;

namespace ChemMgmt.Repository
{
    public interface IInventoryRepo : IDisposable
    {
        List<InventoryManagement> getStock(InventoryManagement _clsInputSearchStock, int? maxResultCount = null);
        Int64 getStockCount(InventoryManagement _clsInputSearchStock);
        List<ViewInventoryDetails> getStockInformation(int? intStockID);
        ViewInventoryDetails setupResult(List<ViewInventoryDetails> dt);     
        IEnumerable<V_FIRE> GetFireCategory(string value);
        IEnumerable<V_REGULATION> GetRegulation(string value);
        IEnumerable<V_GHSCAT> GetGhsCategory(string value);
        IEnumerable<V_OFFICE> GetOfficeCategory(string value);
        bool checkGroup(string keyword, StockSearchTarget type);
        string GetSqlForLedgerGroup();
        List<FireDataModel> DoSearchFireData();
        List<InventoryManagement> DoSearchSheet1(InventoryManagement inventory);
        List<InventoryManagement> DoSearchSheet2(InventoryManagement inventory);
        List<InventoryManagement> DoSearchSheet3(InventoryManagement inventoryManagement);
    }
       
}