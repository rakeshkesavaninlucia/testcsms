﻿using Dapper;
using Microsoft.VisualBasic.FileIO;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.BaseCommon;
using static ZyCoaG.MasterMaintenance.Common;
using ZyCoaG.Classes.util;
using System.Data;
using COE.Common.BaseExtension;
using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ZyCoaG.util;
using ChemMgmt.Classes;
using ChemMgmt.Classes.Extensions;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Drawing;
using ZyCoaG.Nikon;
using ChemMgmt.Nikon.Models;

namespace ChemMgmt.Repository
{
    public class MasterMaintenanceRepository
    {
        WebCommonRepo webRepo = new WebCommonRepo();
        private Dictionary<string, string> inputParam;
        private LogUtil _logoututil = new LogUtil();
        private const string ProgramId = "ZC050";
        //2018/08/16 TPE Add Start
        private const string statusInsert = "INSERT";
        private const string statusUpdate = "UPDATE";
        public virtual IEnumerable<CommonDataModel<string>> GetUserList() => new UserMasterInfo().GetSelectList();
        public List<UserInfo.ClsUser> getUserList = new List<UserInfo.ClsUser>();

        private UserInfo.ClsUser _clsUser = new UserInfo.ClsUser();

        private static int searchEnabled = 0;

        List<tblFireClass> tblFireList = new List<tblFireClass>();

        List<tblFireClass> tblFireClsList = new List<tblFireClass>();


        public Table tblFire { get; set; }

        public KeyValuePair<string,string> InitDisp()
        {
            StringBuilder stbSQL = new StringBuilder();
            stbSQL.Append("select ");
            stbSQL.Append("  MSG_ID");
            stbSQL.Append(" ,MSG_TEXT");
            stbSQL.Append(" from M_MESSAGE");
            stbSQL.Append(" where ");
            stbSQL.Append("  MSG_ID = (");
            stbSQL.Append("select ");
            stbSQL.Append("  MAX(MSG_ID)");
            stbSQL.Append(" from M_MESSAGE");
            stbSQL.Append(" where DEL_FLAG <> 1)");

            DataTable dt = DbUtil.Select(stbSQL.ToString());
            if (dt.Rows.Count > 0)
            {
                return new KeyValuePair<string, string>(dt.Rows[0].ItemArray[0].ToString(), dt.Rows[0].ItemArray[1].ToString());
            }
            else
            {
                return new KeyValuePair<string, string>("", "");
            }
        }

        /// <summary>
        /// テーブル項目情報からリスト出力するフォーマットを作成します。
        /// </summary>
        /// <param name="tableColumnInfos">テーブル項目情報一覧。</param>
        /// <returns>リスト出力フォーマットの <see cref="DataTable"/> 。</returns>
        public DataTable CreateListFormat(IEnumerable<S_TAB_COL_CONTROL> tableColumnInfos)
        {
            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)HttpContext.Current.Session[SessionConst.TableInformation];
            DataTable dt = new DataTable();
            if (tableInformation.TABLE_NAME == "M_GROUP"
    || tableInformation.TABLE_NAME == "M_LOCATION"
    || tableInformation.TABLE_NAME == "M_MAKER"
    || tableInformation.TABLE_NAME == "M_UNITSIZE"
    || tableInformation.TABLE_NAME == "M_WORKFLOW"
    || tableInformation.TABLE_NAME == "M_REGULATION")
            {
                dt.Columns.Add(new DataColumn("DEL_FLAGNAME") { Caption = "*状態" });
            }
            foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInfos.Where(x => !IsDeleteColumn(x)).ToList())
            {
                var prefix = string.Empty;
                if (tableColumnInformation.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select) prefix = SystemConst.ConvertColumnPrefix;

                //2019/03/09 TPE.Rin Add
                if (tableInformation.TABLE_NAME == "M_REGULATION" && tableColumnInformation.COLUMN_NAME == "EXAMINATION_FLAG" ||
                tableInformation.TABLE_NAME == "M_REGULATION" && tableColumnInformation.COLUMN_NAME == "MEASUREMENT_FLAG") continue;
                //2019/02/18 TPE.Sugimoto Add Sta
                if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInformation.COLUMN_NAME == "OVER_CHECK_FLAG")
                {
                    //保管可能危険物
                    dt.Columns.Add(new DataColumn("REG_TEXT") { Caption = "保管可能危険物" });
                }
                //2019/02/18 TPE.Sugimoto Add End
                if (tableInformation.TABLE_NAME == "M_REGULATION" && tableColumnInformation.COLUMN_NAME == "WEIGHT_MGMT")
                {
                    //重量管理
                    dt.Columns.Add(new DataColumn(prefix + tableColumnInformation.COLUMN_NAME) { Caption = tableColumnInformation.DisplayNameAndRequiredMark });

                    //特殊健康診断
                    dt.Columns.Add(new DataColumn(SystemConst.ConvertColumnPrefix + "EXAMINATION_FLAG") { Caption = "特殊健康診断" });

                    //作業環境測定
                    dt.Columns.Add(new DataColumn(SystemConst.ConvertColumnPrefix + "MEASUREMENT_FLAG") { Caption = "作業環境測定" });

                    //リスクアセスメント
                    dt.Columns.Add(new DataColumn("RA_FLAG") { Caption = "リスクアセスメント対象" });

                    //点数
                    dt.Columns.Add(new DataColumn("MARKS") { Caption = "点数" });
                }
                else
                {
                    dt.Columns.Add(new DataColumn(prefix + tableColumnInformation.COLUMN_NAME) { Caption = tableColumnInformation.DisplayNameAndRequiredMark });
                }
            }

            if (tableInformation.TABLE_NAME == "M_GROUP")
            {
                dt.Columns.Add(new DataColumn("DEL_USER_NM") { Caption = "削除者" });
                dt.Columns.Add(new DataColumn("DEL_DATE") { Caption = "削除日時" });
                dt.Columns.Add(new DataColumn("BATCH_FLAGNAME") { Caption = "バッチ更新対象外" });
            }
            else if (tableInformation.TABLE_NAME == "M_LOCATION"
                    || tableInformation.TABLE_NAME == "M_MAKER"
                    || tableInformation.TABLE_NAME == "M_UNITSIZE"
                    || tableInformation.TABLE_NAME == "M_WORKFLOW"
                    || tableInformation.TABLE_NAME == "M_REGULATION")
            {
                dt.Columns.Add(new DataColumn("DEL_USER_NM") { Caption = "削除者" });
                dt.Columns.Add(new DataColumn("DEL_DATE") { Caption = "削除日時" });
            }
            return dt;
        }

        /// <summary>
        /// 選択ボタンの追加対象か、また上位カテゴリーのキーと値の項目情報を返します。
        /// </summary>
        /// <param name="tableName">判定するテーブル名を指定します。</param>
        /// <param name="columnName">判定する項目名を指定します。</param>
        /// <param name="keyColumnName">上位カテゴリーのキー項目を返します。</param>
        /// <param name="valueColumnName">上位カテゴリーの値項目を返します。</param>
        /// <param name="iconPathColumnName">アイコンの値項目を返します。</param>
        /// <param name="sortOrderColumnName">ソート順項目を返します。</param>
        /// <param name="classColumnName">区分項目を返します。</param>
        /// <returns>追加対象の場合は<see cref="true"/>、そうでない場合は<see cref="false"/>を返します。</returns>
        public bool IsSelectButtonTarget(string tableName, string columnName, out string keyColumnName, out string valueColumnName, out string iconPathColumnName,
                                                out string sortOrderColumnName, out string classColumnName, out string selFlagName)//2018/08/19 TPE.Sugimoto Upd
        {
            bool isTarget = false;
            keyColumnName = null;
            valueColumnName = null;
            iconPathColumnName = null;
            sortOrderColumnName = null;
            classColumnName = null;
            selFlagName = null;

            // ユーザーマスター(上位無し)
            if (tableName == "V_GROUP_USER" && columnName == "PID")
            {
                isTarget = true;
                keyColumnName = "ID";
                valueColumnName = "NAME";
            }
            // 部署マスターの上位部署CD
            if (tableName == nameof(M_GROUP) && columnName == nameof(M_GROUP.P_GROUP_CD))
            {
                isTarget = true;
                keyColumnName = nameof(M_GROUP.GROUP_CD);
                valueColumnName = nameof(M_GROUP.GROUP_NAME);
                sortOrderColumnName = nameof(M_GROUP.GROUP_CD);
            }
            // 保管場所マスターの上位保管場所ID
            if ((tableName == nameof(M_LOCATION) || tableName == nameof(M_STORING_LOCATION) || tableName == nameof(M_USAGE_LOCATION)) &&
                 columnName == nameof(M_LOCATION.P_LOC_ID))
            {
                isTarget = true;
                keyColumnName = nameof(M_LOCATION.LOC_ID);
                valueColumnName = nameof(M_LOCATION.LOC_NAME);
                sortOrderColumnName = nameof(M_LOCATION.SORT_LEVEL);
                classColumnName = nameof(M_LOCATION.CLASS_ID);
                selFlagName = nameof(M_LOCATION.SEL_FLAG); //2018/08/19 TPE.Sugimoto Upd
            }
            // 法規制マスター(法規制のみ)の上位法規制種類ID
            if (tableName == nameof(M_REG) && columnName == nameof(M_REG.P_REG_TYPE_ID))
            {
                isTarget = true;
                keyColumnName = nameof(M_REG.REG_TYPE_ID);
                valueColumnName = nameof(M_REG.REG_TEXT);
                iconPathColumnName = nameof(M_REG.REG_ICON_PATH);
                sortOrderColumnName = nameof(M_REG.SORT_ORDER);
            }
            // 法規制マスター(GHS分類・法規制)の上位法規制種類ID
            if (tableName == "M_REGULATION" && columnName == nameof(M_REGULATION.P_REG_TYPE_ID))
            {
                isTarget = true;
                keyColumnName = nameof(M_REGULATION.REG_TYPE_ID);
                valueColumnName = nameof(M_REGULATION.REG_TEXT);
                iconPathColumnName = nameof(M_REGULATION.REG_ICON_PATH);
                sortOrderColumnName = nameof(M_REGULATION.SORT_ORDER);
                classColumnName = nameof(M_REGULATION.CLASS_ID);
            }
            // GHS分類マスターの上位GHS分類ID
            if (tableName == "M_GHSCAT" && columnName == "P_GHSCAT_ID")
            {
                isTarget = true;
                keyColumnName = "GHSCAT_ID";
                valueColumnName = "GHSCAT_NAME";
                iconPathColumnName = "GHSCAT_ICON_PATH";
                sortOrderColumnName = "SORT_ORDER";
            }
            //2019/02/19 TPE.Rin Add Start
            // M_COMMON
            if (tableName == "M_COMMON")
            {
                isTarget = true;
                keyColumnName = nameof(M_COMMON.KEY_VALUE);
                valueColumnName = nameof(M_COMMON.DISPLAY_VALUE);
                sortOrderColumnName = nameof(M_COMMON.DISPLAY_ORDER);
                classColumnName = nameof(M_COMMON.TABLE_NAME);
            }
            //2019/02/19 TPE.Rin Add End
            //2019/04/04 TPE.Rin Add Start
            // 消防法マスターの上位GHS分類ID
            if (tableName == "M_FIRE" && columnName == "P_FIRE_ID")
            {
                isTarget = true;
                keyColumnName = "FIRE_ID";
                valueColumnName = "FIRE_NAME";
                iconPathColumnName = "FIRE_ICON_PATH";
                sortOrderColumnName = "SORT_ORDER";
            }
            // 社内ルールマスターの上位GHS分類ID
            if (tableName == "M_OFFICE" && columnName == "P_OFFICE_ID")
            {
                isTarget = true;
                keyColumnName = "OFFICE_ID";
                valueColumnName = "OFFICE_NAME";
                iconPathColumnName = "OFFICE_ICON_PATH";
                sortOrderColumnName = "SORT_ORDER";
            }
            //2019/04/04 TPE.Rin Add End
            return isTarget;
        }

        public string DoRegist(string ID, string Text)
        {
        StringBuilder stbSQL = new StringBuilder();
        Hashtable _params = new Hashtable();
        DbContext ctx = null;
        UserInfo.ClsUser userInfo = null;
        Common.GetUserInfo(HttpContext.Current.Session, out userInfo);
                _params.Add("LOGIN_USER_CD", userInfo.USER_CD);
                    try
                    {
                        if (ID == "")
                        {
                            stbSQL.Append("insert into M_MESSAGE (");
                            stbSQL.Append(" MSG_TEXT");
                            stbSQL.Append(",DEL_FLAG");
                            stbSQL.Append(",REG_USER_CD");
                            stbSQL.Append(") values (");
                            stbSQL.Append(" :MSG_TEXT");
                            stbSQL.Append(",:DELETE_FLG");
                            stbSQL.Append(",:LOGIN_USER_CD)");

                            _params.Add("MSG_TEXT", Text);
                            _params.Add("DELETE_FLG", "0");

                        }
                        else
                        {
                            stbSQL.Append("update M_MESSAGE set");
                            stbSQL.Append(" MSG_TEXT = :MSG_TEXT, ");
                            stbSQL.Append(" UPD_USER_CD = :LOGIN_USER_CD ");
                            stbSQL.Append("where");
                            stbSQL.Append(" MSG_ID = :MSG_ID");

                            _params.Add("MSG_TEXT", Text);
                            _params.Add("MSG_ID", ID);
                        }

                        ctx = DbUtil.Open(true);
                        int cnt = DbUtil.ExecuteUpdate(ctx, stbSQL.ToString(), _params);
                        ctx.Commit();
                        return Common.GetMessage("I001");
                    }
                    catch (Exception ex)
                    {
                        ctx.Rollback();
                        ctx.Close();
                    return Common.GetMessage("E001");
                        throw ex;
                    }
                    finally
                    {
                        if (ctx != null)
                        {
                            ctx.Close();
                        }
                    }
               }
        public List<M_GROUP> IsHighLevelDepartment(string GROUP_CD)
        {
            
            StringBuilder sql = new StringBuilder();
            sql.AppendLine("select GROUP_CD from M_GROUP where P_GROUP_CD = '" + GROUP_CD + "'");
            sql.AppendLine(" and " + nameof(SystemColumn.DEL_FLAG) + " = 0");

            List<M_GROUP> records = DbUtil.Select<M_GROUP>(sql.ToString(), null);
            return records;
        }

        public List<M_LOCATION> IsUpperStorageLocation(string LOC_ID)
        {

            StringBuilder sql = new StringBuilder();
            sql.AppendLine("select LOC_ID from M_LOCATION where P_LOC_ID = '" + LOC_ID + "'");
            sql.AppendLine(" and " + nameof(SystemColumn.DEL_FLAG) + " = 0");

            List<M_LOCATION> records = DbUtil.Select<M_LOCATION>(sql.ToString(), null);
            return records;
        }

        public void SetSqlWhere(ref StringBuilder sbWhere, ref DynamicParameters _params, WebCommonInfo mUser)
        {
            if (mUser.ADMIN_FLAG == null)
            {
                sbWhere.Append(" where");
                sbWhere.Append(" U0.DEL_FLAG=@DEL_FLAG");
                _params.Add("@DEL_FLAG", 0);
            }
            else if (mUser.DEL_FLAG == 1)
            {
                sbWhere.Append(" where");
                sbWhere.Append("  U0.DEL_FLAG=@DEL_FLAG");
                _params.Add("@DEL_FLAG", 1);
            }
            else
            {
                sbWhere.Append(" where");
                sbWhere.Append("  U0.DEL_FLAG in (0,1)");
            }
            if (mUser.BATCH_FLAG == "1")
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  IsNull(U0.BATCH_FLAG,0)=@BATCH_FLAG");
                _params.Add("@BATCH_FLAG", 0);
            }
            else if (mUser.BATCH_FLAG == "2")
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  IsNull(U0.BATCH_FLAG,0)=@BATCH_FLAG");
                _params.Add("@BATCH_FLAG", 1);
            }

            if (mUser.USER_CD != null)
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  UPPER(U0.USER_CD)=@USER_CD");
                _params.Add("@USER_CD", mUser.USER_CD.ToUpper());
            }

            if (mUser.USER_NAME != null)
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  U0.USER_NAME like '%'@USER_NAME'%'" + DbUtil.LikeEscapeInfoAdd());
                _params.Add("@USER_NAME", DbUtil.ConvertIntoEscapeChar(mUser.USER_NAME));
            }

            if (mUser.USER_NAME_KANA != null)
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  U0.USER_NAME_KANA like '%'@USER_NAME_KANA'%'" + DbUtil.LikeEscapeInfoAdd());
                _params.Add("@USER_NAME_KANA", DbUtil.ConvertIntoEscapeChar(mUser.USER_NAME_KANA));
            }

            if (mUser.GROUP_CD != null)
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  U0.GROUP_CD=@GROUP_CD");
                _params.Add("@GROUP_CD", mUser.GROUP_CD);
            }
        }


        /// <summary>
        /// 取込処理を行います。
        /// </summary>
        public void DoImport(string fileName, out int importedCount, out string errMessage)
        {
            importedCount = 0;
            errMessage = "";

            if (HttpContext.Current.Session[SessionConst.TableColumnInformations] == null) return;
            var tableInformation = (S_TAB_CONTROL)HttpContext.Current.Session[SessionConst.TableInformation];

            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)HttpContext.Current.Session[SessionConst.TableColumnInformations];
            TextFieldParser csvParser = new TextFieldParser(fileName, Encoding.GetEncoding("Shift_JIS"));
            csvParser.TextFieldType = FieldType.Delimited;
            csvParser.SetDelimiters(SystemConst.SelectionsDelimiter);

            // csvヘッダー行（1行目）を読み込んで テンプレートのヘッダーと比較する
            List<string> csvHeaders = csvParser.ReadFields().ToList().ConvertAll(x => x.RemoveRequiredMark());
            //IEnumerable<string> templateHeaders = tableColumnInformations.Select(x => x.COLUMN_DISPLAY_NAME);

            IEnumerable<string> templateHeaders = tableColumnInformations.Where(x => !IsDeleteColumn(x)).Select(x => x.COLUMN_DISPLAY_NAME);
            if (tableInformation.TABLE_NAME == "M_USER")
            {
                if (string.Join(string.Empty, csvHeaders) != "状態NID社員名社員名（ローマ字）パスワード部署名上長代理者内線番号メールアドレス前回部署フロー名登録者登録日時更新者更新日時管理者権限削除者削除日時バッチ更新対象外")
                {
                    errMessage = "ヘッダー行の形式が不正です。<br/>リスト出力もしくはテンプレート出力したファイルを使用してください。";
                    return;
                }
            }
            if (tableInformation.TABLE_NAME == "M_GROUP")
            {
                if (string.Join(string.Empty, csvHeaders) != "状態部署ID部署コード部署名上位部署名登録者登録日時更新者更新日時削除者削除日時バッチ更新対象外")
                {
                    errMessage = "ヘッダー行の形式が不正です。<br/>リスト出力もしくはテンプレート出力したファイルを使用してください。";
                    return;
                }
            }
            else if (tableInformation.TABLE_NAME == "M_MAKER")
            {
                if (string.Join(string.Empty, csvHeaders) != "状態メーカーIDメーカー名URL電話番号メールアドレス1メールアドレス2メールアドレス3住所登録者登録日時更新者更新日時削除者削除日時")
                {
                    errMessage = "ヘッダー行の形式が不正です。<br/>リスト出力もしくはテンプレート出力したファイルを使用してください。";
                    return;
                }
            }
            else if (tableInformation.TABLE_NAME == "M_UNITSIZE")
            {
                if (string.Join(string.Empty, csvHeaders) != "状態単位ID単位名称変換係数登録者登録日時更新者更新日時削除者削除日時")
                {
                    errMessage = "ヘッダー行の形式が不正です。<br/>リスト出力もしくはテンプレート出力したファイルを使用してください。";
                    return;
                }
            }
            else if (tableInformation.TABLE_NAME == "M_WORKFLOW")
            {
                if (string.Join(string.Empty, csvHeaders) != "状態フローIDフローコードフロー種別フロー名フロー階層承認階層1承認者1代行者1承認階層2承認者2代行者2承認階層3承認者3代行者3承認階層4承認者4代行者4承認階層5承認者5代行者5承認階層6承認者6代行者6登録者登録日時更新者更新日時削除者削除日時")
                {
                    errMessage = "ヘッダー行の形式が不正です。<br/>リスト出力もしくはテンプレート出力したファイルを使用してください。";
                    return;
                }
            }
            //2019/02/18 TPE.Sugimoto Add End
            else
            {
                string headers = string.Join(string.Empty, templateHeaders);
                string theaders = string.Join(string.Empty, csvHeaders);
                if (headers != theaders)
                {
                    errMessage = "ヘッダー行の形式が不正です。<br/>リスト出力もしくはテンプレート出力したファイルを使用してください。";
                    return;
                }
            }

            int rowIndex = 1;
            Dictionary<string, string> updateContainer = new Dictionary<string, string>();

            if (tableInformation.TABLE_NAME == "M_GROUP")
            {
                updateContainer.Add("DEL_FLAG", string.Empty);
                updateContainer.Add("GROUP_ID", string.Empty);
                updateContainer.Add("GROUP_CD", string.Empty);
                updateContainer.Add("GROUP_NAME", string.Empty);
                updateContainer.Add("P_GROUP_CD", string.Empty);
                updateContainer.Add("REG_USER_CD", string.Empty);
                updateContainer.Add("REG_DATE", string.Empty);
                updateContainer.Add("UPD_USER_CD", string.Empty);
                updateContainer.Add("UPD_DATE", string.Empty);
                updateContainer.Add("DEL_USER_CD", string.Empty);
                updateContainer.Add("DEL_DATE", string.Empty);
                updateContainer.Add("BATCH_FLAG", string.Empty);
            }
            else if (tableInformation.TABLE_NAME == "M_MAKER")
            {
                updateContainer.Add("DEL_FLAG", string.Empty);
                updateContainer.Add("MAKER_ID", string.Empty);
                updateContainer.Add("MAKER_NAME", string.Empty);
                updateContainer.Add("MAKER_URL", string.Empty);
                updateContainer.Add("MAKER_TEL", string.Empty);
                updateContainer.Add("MAKER_EMAIL1", string.Empty);
                updateContainer.Add("MAKER_EMAIL2", string.Empty);
                updateContainer.Add("MAKER_EMAIL3", string.Empty);
                updateContainer.Add("MAKER_ADDRESS", string.Empty);
                updateContainer.Add("REG_USER_CD", string.Empty);
                updateContainer.Add("REG_DATE", string.Empty);
                updateContainer.Add("UPD_USER_CD", string.Empty);
                updateContainer.Add("UPD_DATE", string.Empty);
                updateContainer.Add("DEL_USER_CD", string.Empty);
                updateContainer.Add("DEL_DATE", string.Empty);
            }
            else if (tableInformation.TABLE_NAME == "M_UNITSIZE")
            {
                updateContainer.Add("DEL_FLAG", string.Empty);
                updateContainer.Add("UNITSIZE_ID", string.Empty);
                updateContainer.Add("UNIT_NAME", string.Empty);
                updateContainer.Add("FACTOR", string.Empty);
                updateContainer.Add("REG_USER_CD", string.Empty);
                updateContainer.Add("REG_DATE", string.Empty);
                updateContainer.Add("UPD_USER_CD", string.Empty);
                updateContainer.Add("UPD_DATE", string.Empty);
                updateContainer.Add("DEL_USER_CD", string.Empty);
                updateContainer.Add("DEL_DATE", string.Empty);
            }
            else if (tableInformation.TABLE_NAME == "M_WORKFLOW")
            {
                updateContainer.Add("DEL_FLAG", string.Empty);
                updateContainer.Add("FLOW_ID", string.Empty);
                updateContainer.Add("FLOW_CD", string.Empty);
                updateContainer.Add("FLOW_TYPE_ID", string.Empty);
                updateContainer.Add("USAGE", string.Empty);
                updateContainer.Add("FLOW_HIERARCHY", string.Empty);
                updateContainer.Add("APPROVAL_TARGET1", string.Empty);
                updateContainer.Add("APPROVAL_USER_CD1", string.Empty);
                updateContainer.Add("SUBSITUTE_USER_CD1", string.Empty);
                updateContainer.Add("APPROVAL_TARGET2", string.Empty);
                updateContainer.Add("APPROVAL_USER_CD2", string.Empty);
                updateContainer.Add("SUBSITUTE_USER_CD2", string.Empty);
                updateContainer.Add("APPROVAL_TARGET3", string.Empty);
                updateContainer.Add("APPROVAL_USER_CD3", string.Empty);
                updateContainer.Add("SUBSITUTE_USER_CD3", string.Empty);
                updateContainer.Add("APPROVAL_TARGET4", string.Empty);
                updateContainer.Add("APPROVAL_USER_CD4", string.Empty);
                updateContainer.Add("SUBSITUTE_USER_CD4", string.Empty);
                updateContainer.Add("APPROVAL_TARGET5", string.Empty);
                updateContainer.Add("APPROVAL_USER_CD5", string.Empty);
                updateContainer.Add("SUBSITUTE_USER_CD5", string.Empty);
                updateContainer.Add("APPROVAL_TARGET6", string.Empty);
                updateContainer.Add("APPROVAL_USER_CD6", string.Empty);
                updateContainer.Add("SUBSITUTE_USER_CD6", string.Empty);
                updateContainer.Add("REG_USER_CD", string.Empty);
                updateContainer.Add("REG_DATE", string.Empty);
                updateContainer.Add("UPD_USER_CD", string.Empty);
                updateContainer.Add("UPD_DATE", string.Empty);
                updateContainer.Add("DEL_USER_CD", string.Empty);
                updateContainer.Add("DEL_DATE", string.Empty);
            }
            //2019/02/18 TPE.Sugimoto Add End
            else
            {
                foreach (var csvHeader in csvHeaders)
                {
                    var tableColumnInfo = tableColumnInformations.SingleOrDefault(x => x.COLUMN_DISPLAY_NAME == csvHeader && !IsSystemColumn(x, false));
                    if (tableColumnInfo != null) updateContainer.Add(tableColumnInfo.COLUMN_NAME, string.Empty);
                }
            }
            while (!csvParser.EndOfData)
            {
                // 1行ずつ読み込んで配列にセット
                List<string> row = csvParser.ReadFields().ToList();
                var i = 0;
                var updateData = new Dictionary<string, string>();
                foreach (var key in updateContainer.Keys)
                {
                    updateData.Add(key, row[i] != null ? row[i] : string.Empty);
                    i++;
                }

                string rowErrorMessage = "";
                inputParam = updateData;

                if (tableInformation.TABLE_NAME == "M_LOCATION")
                {
                    importedCount += ImportLocation(out rowErrorMessage);
                }
                else if (tableInformation.TABLE_NAME == "M_GROUP")
                {
                    importedCount += ImportGroup(out rowErrorMessage);
                }
                //2019/02/18 TPE.Sugimoto Add Sta
                else if (tableInformation.TABLE_NAME == "M_MAKER")
                {
                    importedCount += ImportMaker(out rowErrorMessage);
                }
                else if (tableInformation.TABLE_NAME == "M_UNITSIZE")
                {
                    importedCount += ImportUnitsize(out rowErrorMessage);
                }
                else if (tableInformation.TABLE_NAME == "M_WORKFLOW")
                {
                    importedCount += ImportWorkflow(out rowErrorMessage);
                }
                //2019/02/18 TPE.Sugimoto Add End
                else
                {
                    importedCount += Import(out rowErrorMessage);
                }

                // 呼び出し先でcatchしたエラーを溜めておく
                if (rowErrorMessage != "")
                {
                    errMessage += rowIndex.ToString() + "行目：" + SystemConst.HtmlLinefeed + rowErrorMessage + "<br/>";
                }
                rowIndex++;
            }

        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 24-10-2019
        /// Description : Get the sql to update the records
        /// <summary>
        public int Import(out string errMessage)
        {
            errMessage = "";
            int cnt = 0;

            try
            {
                // データのエラーチェック
                if (!DataCheck(out errMessage)) return cnt;

                string sql = "";
                Hashtable _params = new Hashtable();
                string status = "";
                if (GetSqlUpdate(ref sql, ref _params, ref status))
                {
                    DoSql(sql, _params, status);
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }

            return cnt;
        }

        /// <summary>
        /// M_GROUPインポート用
        /// </summary>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        private int ImportGroup(out string errMessage)
        {
            errMessage = "";
            int cnt = 0;

            try
            {
                // データのエラーチェック
                if (!DataCheckGroup(out errMessage)) return cnt;

                string sql = "";
                Hashtable _params = new Hashtable();
                string status = "";

                if (GetSqlUpdateMGroup(ref sql, ref _params, ref status))
                {
                    DoSql(sql, _params, status);
                    cnt++;
                }

            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }

            return cnt;
        }
        //2019/02/18 TPE.Sugimoto Add Sta
        /// <summary>
        /// M_MAKERインポート用
        /// </summary>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        private int ImportMaker(out string errMessage)
        {
            errMessage = "";
            int cnt = 0;

            try
            {
                // データのエラーチェック
                if (!DataCheckMaker(out errMessage)) return cnt;

                string sql = "";
                Hashtable _params = new Hashtable();
                string status = "";

                if (GetSqlUpdateMMaker(ref sql, ref _params, ref status))
                {
                    DoSql(sql, _params, status);
                    cnt++;
                }

            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }

            return cnt;
        }
        /// <summary>
        /// M_UNITSIZEインポート用
        /// </summary>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        private int ImportUnitsize(out string errMessage)
        {
            errMessage = "";
            int cnt = 0;

            try
            {
                // データのエラーチェック
                if (!DataCheckUnitsize(out errMessage)) return cnt;

                string sql = "";
                Hashtable _params = new Hashtable();
                string status = "";

                if (GetSqlUpdateMUnitsize(ref sql, ref _params, ref status))
                {
                    DoSql(sql, _params, status);
                    cnt++;
                }

            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }

            return cnt;
        }
        /// <summary>
        /// M_WORKFLOWインポート用
        /// </summary>
        /// <param name="errMessage"></param>
        /// <returns></returns>
        private int ImportWorkflow(out string errMessage)
        {
            errMessage = "";
            int cnt = 0;

            try
            {
                // データのエラーチェック
                if (!DataCheckWorkflow(out errMessage)) return cnt;

                string sql = "";
                Hashtable _params = new Hashtable();
                string status = "";

                if (GetSqlUpdateMWorkflow(ref sql, ref _params, ref status))
                {
                    DoSql(sql, _params, status);
                    cnt++;
                }

            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }

            return cnt;
        }
        //2019/02/18 TPE.Sugimoto Add End
        //2018/08/31 TPE Add
        private int ImportLocation(out string errMessage)
        {
            errMessage = "";
            int cnt = 0;

            try
            {
                // データのエラーチェック
                if (!DataCheckLocation(out errMessage)) return cnt;

                string sql = "";
                Hashtable _params = new Hashtable();
                string status = "";

                if (GetSqlUpdate(ref sql, ref _params, ref status))
                {
                    DoSql(sql, _params, status);
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }
            return cnt;
        }

        /// <summary>
        /// SQLを実行します。
        /// </summary>
        /// <param name="sql">SQL文</param>
        /// <param name="_params">バインド変数格納HashTable</param>
        private int DoSql(string sql, Hashtable _params, string status)
        {
            var tableInformation = (S_TAB_CONTROL)HttpContext.Current.Session[SessionConst.TableInformation];

            int cnt = 0;
            int locid;
            DbContext ctx = null;
            try
            {
                ctx = DbUtil.Open(true);
                if (tableInformation.TABLE_NAME == "M_LOCATION")
                {
                    switch (status)
                    {
                        case statusInsert:
                            locid = DbUtil.ExecuteUpdateReturn(ctx, sql, _params);
                            cnt++;
                            GetSqlDelInsMChief(ctx, locid, _params["USER_CD"].ToString(), _params["UPD_USER"].ToString());
                            break;

                        case statusUpdate:
                            cnt = DbUtil.ExecuteUpdate(ctx, sql, _params);
                            GetSqlDelInsMChief(ctx, Convert.ToInt32(_params["LOC_ID"]), _params["USER_CD"].ToString(), _params["UPD_USER"].ToString());
                            break;

                    }

                }
                else
                {
                    cnt = DbUtil.ExecuteUpdate(ctx, sql, _params);
                }
                ctx.Commit();
                ctx.Close();
            }
            catch (Exception ex)
            {
                if (ctx != null)
                {
                    ctx.Rollback();
                    ctx.Close();
                }
                throw ex;
            }

            return cnt;
        }


        /// <summary>
        /// 検索処理を行います。
        /// </summary>
        /// <param name="IsSetSession">true：検索条件をセッションへ設定します。 / false：設定しない。</param>
        public List<M_FIRE> DoSearch(M_FIRE mFire)
        {
            _clsUser.USER_CD = Common.GetLogonForm().Id;
            _clsUser.REFERENCE_AUTHORITY = Common.GetLogonForm().ADMIN_FLAG;
            string strSQL = "";
            string strWhere = "";
            M_FIRE m_FIRE = new M_FIRE();
            //M_FIRE m_Fire = new M_FIRE();
            List<M_FIRE> dttable = new List<M_FIRE>();
            string sql = null;
            DynamicParameters _params = new DynamicParameters();
            DataTable dt;

            M_FIRE m_Fire = new M_FIRE();

            // 検索条件をセッションに保持
            if (mFire.IsSetSession)
            {
                m_Fire = SetInputSearch(mFire);
                // セッション検索条件を画面に再設定
                //SetSearchCondition();
            }
            else
            {
                if (!mFire.IsPostBack)
                {

                    //m_FIRE = SetInputSearch(mFire);
                    // セッション検索条件を画面に再設定
                    //m_FIRE = SetSearchCondition();
                }
            }

            // Sql取得
            string sqlStmt = GetSql(m_Fire, ref sql, ref _params);


            dttable = DbUtil.Select<M_FIRE>(sqlStmt, _params);

            //m_FIRE.listMFire = dttable;
            //m

            ////dt = DbUtil.select(sql, _params);
            //grid.DataSource = dt;
            //grid.DataBind();

            ////if (dt.Rows.Count > 0)//2019/02/18 TPE.Sugimoto Del
            if (dttable.Count >= 0)//2019/02/18 TPE.Sugimoto Add
            {
                // 検索条件をセッションに保持
                m_Fire = SetInputSearch(mFire);
                // ソートのために検索結果をsession変数に保持しておく
                HttpContext.Current.Session[SessionConst.cSESSION_NAME_SEARCH_RESULT] = m_Fire;
            }
            ////2019/02/18 TPE.Sugimoto Del Sta
            ////else
            ////{
            ////    if (IsSetSession)
            ////    {
            ////        Util.Common.MsgBox(this, string.Format(WarningMessages.NotFound, "消防法届出"));
            ////    }
            ////}
            ////2019/02/18 TPE.Sugimoto Del End

            return dttable;
        }


        /// <summary>
        /// 検索条件を設定し、セッションへ格納します。
        /// </summary>
        private M_FIRE SetInputSearch(M_FIRE mFire)
        {
            // 検索条件設定
            M_FIRE clsInpSearch = new M_FIRE();
            if (mFire.REPORT_TYPE != null)
            {
                clsInpSearch.REPORT_TYPE = mFire.REPORT_TYPE.ToString();
            }
            if (mFire.txtReportName != null)
            {
                clsInpSearch.REPORT_NAME = mFire.txtReportName;
            }
            if (mFire.REPORT_PLACE_KEY != null)
            {
                clsInpSearch.REPORT_PLACE_KEY = mFire.REPORT_PLACE_KEY.ToString();
            }
            if (mFire.REPORT_LOCATION != null)
            {
                clsInpSearch.REPORT_LOCATION = mFire.REPORT_LOCATION.ToString();
            }
            if (mFire.hdnLocationId != null)
            {
                clsInpSearch.REPORT_LOCATIONID = mFire.hdnLocationId;
            }
            if (mFire.DEL_FLAG != null)
            {
                clsInpSearch.DEL_FLAG = mFire.DEL_FLAG;
            }

            HttpContext.Current.Session[SessionConst.cSESSION_NAME_InputSearch] = clsInpSearch;

            return clsInpSearch;
        }

        /// 検索用SQLを取得します。
        /// </summary>
        /// <param name="sql">作成SQL文</param>
        /// <param name="_params">バインド変数格納Hashtable</param>
        private string GetSql(M_FIRE mFire, ref string sql, ref DynamicParameters _params)
        {
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbSql = new StringBuilder();

            // 検索条件生成
            string sqlWhere = SetSqlWhere(mFire, ref sbWhere, ref _params);

            // SELECT SQL生成
            sbSql.Append(" SELECT * FROM (select DISTINCT");
            sbSql.Append("   U0.REPORT_ID");
            sbSql.Append("  ,U0.REPORT_NAME");
            sbSql.Append("  ,CONVERT(NVARCHAR, U0.REPORT_YMD, 111) AS REPORT_YMD");
            sbSql.Append("  ,U0.REPORT_SUBMIT");
            sbSql.Append("  ,C0.DISPLAY_VALUE AS REPORT_PLACE");
            sbSql.Append("  ,(SELECT ");
            sbSql.Append("  FORMAT(SUM(convert(float,ROUND((F.FIRE_SIZE / R.FIRE_SIZE),4))),'0.0000') AS SIZE");
            sbSql.Append("  FROM M_FIRE_SIZE F");
            sbSql.Append("  inner join M_REGULATION R ON R.REG_TYPE_ID = F.REG_TYPE_ID");
            sbSql.Append("  inner join V_FIRE V ON V.FIRE_ID = F.REG_TYPE_ID AND V.FIRE_NAME NOT LIKE '%指定可燃物%'");
            sbSql.Append("  WHERE F.REPORT_ID = U0.REPORT_ID");
            sbSql.Append("  GROUP BY F.REPORT_ID");
            sbSql.Append("  ) AS FIRE_SIZE");
            sbSql.Append("  ,IIF(U0.DEL_FLAG = 0,N'使用中',N'削除済') AS STATUS");
            sbSql.Append("  ,U1.USER_NAME AS REG_USER_NAME");
            sbSql.Append("  ,U0.REG_DATE");
            sbSql.Append("  ,U2.USER_NAME AS UPD_USER_NAME");
            sbSql.Append("  ,U0.UPD_DATE");
            sbSql.Append("  ,U3.USER_NAME AS DEL_USER_NAME");
            sbSql.Append("  ,U0.DEL_DATE");
            //sbSql.Append("  ,(");
            //sbSql.Append("  SELECT TOP 1 V0.LOC_NAME + '   '");
            //sbSql.Append("  , '(' + convert(varchar, (SELECT COUNT(*)");
            //sbSql.Append("  FROM V_LOCATION V0");
            //sbSql.Append("  inner join M_FIRE_LOCATION L  on L.LOC_ID = V0.LOC_ID");
            //sbSql.Append("  inner join M_FIRE_REPORT F  on F.REPORT_ID = L.REPORT_ID");
            //sbSql.Append("  WHERE F.REPORT_ID = U0.REPORT_ID)) +'件)'");
            //sbSql.Append("  FROM V_LOCATION V0");
            //sbSql.Append("  right join M_FIRE_LOCATION L on L.LOC_ID = V0.LOC_ID");
            //sbSql.Append("  right join M_FIRE_REPORT F  on F.REPORT_ID = L.REPORT_ID");
            //sbSql.Append("  WHERE F.REPORT_ID = U0.REPORT_ID");
            //sbSql.Append("  FOR XML PATH('')");
            //sbSql.Append("  ) AS REPORT_LOCATION ");
            sbSql.Append("  ,(");
            sbSql.Append("  SELECT V0.LOC_NAME + ';'");
            sbSql.Append("  FROM V_LOCATION V0");
            sbSql.Append("  right join M_FIRE_LOCATION L  on L.LOC_ID  =   V0.LOC_ID");
            sbSql.Append("  right join M_FIRE_REPORT F  on F.REPORT_ID  = L.REPORT_ID");
            sbSql.Append("  WHERE F.REPORT_ID = U0.REPORT_ID");
            sbSql.Append("  FOR XML PATH('')");
            sbSql.Append("  ) AS REPORT_LOCATION ");
            sbSql.Append(" from");
            sbSql.Append("   M_FIRE_REPORT U0");
            sbSql.Append("   left join M_COMMON C0  on U0.REPORT_PLACE_KEY  = C0.KEY_VALUE AND C0.TABLE_NAME = 'M_PLACE'");
            sbSql.Append("   left join M_FIRE_LOCATION L0  on U0.REPORT_ID  = L0.REPORT_ID");
            //sbSql.Append("   left join M_LOCATION M0  on L0.LOC_ID  = M0.LOC_ID");
            sbSql.Append("   left join M_USER  U1 on U0.REG_USER_CD = U1.USER_CD");
            sbSql.Append("   left join M_USER  U2 on U0.UPD_USER_CD = U2.USER_CD");
            sbSql.Append("   left join M_USER  U3 on U0.DEL_USER_CD = U3.USER_CD");
            //if (_params.Count > 0)
            //{
            sbSql.Append(sqlWhere);
            //}
            sbSql.Append(") AS A order by");
            sbSql.Append("  A.REPORT_NAME");
            sql = sbSql.ToString();

            return sql;
        }

        // Where句生成
        private string SetSqlWhere(M_FIRE mFire, ref StringBuilder sbWhere, ref DynamicParameters _params)
        {

            if (mFire.DEL_FLAG.ToString() == "0")
            {
                sbWhere.Append(" WHERE");
                sbWhere.Append("  U0.DEL_FLAG=@DEL_FLAG");
                _params.Add("@DEL_FLAG", 0);
            }
            else if (mFire.DEL_FLAG.ToString() == "1")
            {
                sbWhere.Append(" WHERE");
                sbWhere.Append("  U0.DEL_FLAG=@DEL_FLAG");
                _params.Add("@DEL_FLAG", 1);
            }
            else if (mFire.DEL_FLAG.ToString()
                == "2")
            {
                sbWhere.Append(" WHERE");
                sbWhere.Append("  U0.DEL_FLAG >= 0");

            }
            if (mFire.REPORT_NAME != null)
            {
                if (mFire.REPORT_NAME.Trim() != "")
                {
                    if (mFire.REPORT_TYPE.ToString() == "0")
                    {
                        sbWhere.Append(" AND");
                        sbWhere.Append("  U0.REPORT_NAME like '%'+ @REPORT_NAME +'%'" + ZyCoaG.Classes.util.DbUtil.LikeEscapeInfoAdd());
                        _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(mFire.REPORT_NAME));

                    }
                    else if (mFire.REPORT_TYPE.ToString() == "1")
                    {
                        sbWhere.Append(" AND");
                        sbWhere.Append("  U0.REPORT_NAME like @REPORT_NAME+'%'" + ZyCoaG.Classes.util.DbUtil.LikeEscapeInfoAdd());
                        _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(mFire.REPORT_NAME));

                    }
                    else if (mFire.REPORT_TYPE.ToString() == "2")
                    {
                        sbWhere.Append(" AND");
                        sbWhere.Append("  U0.REPORT_NAME like '%'+@REPORT_NAME" + ZyCoaG.Classes.util.DbUtil.LikeEscapeInfoAdd());
                        _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(mFire.REPORT_NAME));

                    }
                    else if (mFire.REPORT_TYPE.ToString() == "3")
                    {
                        sbWhere.Append(" AND");
                        sbWhere.Append("  U0.REPORT_NAME = @REPORT_NAME");
                        _params.Add("@REPORT_NAME", DbUtil.ConvertIntoEscapeChar(mFire.REPORT_NAME));

                    }
                }
            }
            if (mFire.REPORT_PLACE_KEY != null)
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  U0.REPORT_PLACE_KEY = @REPORT_PLACE_KEY");
                _params.Add("@REPORT_PLACE_KEY", mFire.REPORT_PLACE_KEY);

            }
            if (mFire.REPORT_LOCATIONID != null)
            {

                if (mFire.REPORT_LOCATIONID != "")
                {
                    int count = 0;
                    string paraLocationId = "";
                    foreach (string locationId in mFire.REPORT_LOCATIONID.Split(','))
                    {
                        paraLocationId += "@LOC_ID" + count.ToString();
                        paraLocationId += ",";
                        _params.Add("@LOC_ID" + count.ToString(), locationId);
                        count++;
                    }
                    paraLocationId = paraLocationId.Remove(paraLocationId.Length - 1);

                    sbWhere.Append(" AND U0.REPORT_ID IN (SELECT DISTINCT L0.REPORT_ID FROM M_FIRE_LOCATION L0 WHERE L0.LOC_ID in (" + paraLocationId + "))");

                }
            }
            return sbWhere.ToString();


        }

        


        /// <summary>
        /// 更新、参照登録時のデータ取得
        /// </summary>
        public M_FIRE SetFireData(M_FIRE mFire)
        {
            // M_FIRE mFire = new M_FIRE();
            mFire = GetReportData(mFire);
            //mFire = GetFireData(mFire);
            return mFire;

        }


        public M_FIRE GetReportData(M_FIRE mFire)
        {
            string sql = null;
            DynamicParameters _params = new DynamicParameters();
            List<M_FIRE> dt;


            // Sql取得
            GetReportSql(ref sql, ref _params);

            dt = DbUtil.Select<M_FIRE>(sql, _params);

            mFire.REPORT_NAME = Convert.ToString(dt[0].REPORT_NAME);
            mFire.REPORT_YMD = Convert.ToString(dt[0].REPORT_YMD);
            mFire.REPORT_SUBMIT = Convert.ToString(dt[0].REPORT_SUBMIT);
            mFire.REPORT_PLACE_KEY = Convert.ToString(dt[0].REPORT_PLACE_KEY);
            if (Convert.ToString(dt[0].REPORT_LOCATION1).Length > 0)
            {
                mFire.hdnLocationId1 = Convert.ToString(dt[0].REPORT_LOCATION1).Substring(0, Convert.ToString(dt[0].REPORT_LOCATION1).Length - 1);
            }
            if (Convert.ToString(dt[0].REPORT_LOCATION2).Length > 0)
            {
                mFire.hdnLocationId2 = Convert.ToString(dt[0].REPORT_LOCATION2).Substring(0, Convert.ToString(dt[0].REPORT_LOCATION2).Length - 1);
            }
            string[] strLocName1 = Convert.ToString(dt[0].REPORT_LOCATION_NAME1).Split(',');
            string[] strLocName2 = Convert.ToString(dt[0].REPORT_LOCATION_NAME2).Split(',');
            List<string> lstLocation1 = new List<string>();
            foreach (string str in strLocName1)
            {
                if (str != "")
                {
                    lstLocation1.Add(str);
                    mFire.locname1 += str.ToString() + "\n";
                }
                mFire.lstLocation1 = lstLocation1;

            }


            List<string> lstLocation2 = new List<string>();
            foreach (string str in strLocName2)
            {
                if (str != "")
                {
                    lstLocation2.Add(str);
                    mFire.locname2 += str.ToString() + "\n";
                }
                mFire.lstLocation2 = lstLocation2;

            }
            return mFire;
        }

        /// 検索用SQLを取得します。
        /// </summary>
        /// <param name="sql">作成SQL文</param>
        /// <param name="_params">バインド変数格納Hashtable</param>
        private void GetReportSql(ref string sql, ref DynamicParameters _params)
        {
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbSql = new StringBuilder();

            // SELECT SQL生成
            sbSql.Append(" 　select DISTINCT");
            sbSql.Append("   U0.REPORT_ID");
            sbSql.Append("  ,U0.REPORT_NAME");
            sbSql.Append("  ,CONVERT(NVARCHAR, U0.REPORT_YMD, 111) AS REPORT_YMD");
            sbSql.Append("  ,U0.REPORT_SUBMIT");
            sbSql.Append("  ,U0.REPORT_PLACE_KEY");
            sbSql.Append("  ,U0.REG_DATE");
            sbSql.Append("  ,U0.UPD_DATE");
            sbSql.Append("  ,U0.DEL_DATE");
            sbSql.Append("  ,(");
            sbSql.Append("  SELECT CONVERT(VARCHAR,V0.LOC_ID) + ','");
            sbSql.Append("  FROM V_LOCATION V0");
            sbSql.Append("  right join M_FIRE_LOCATION L  on L.LOC_ID  = V0.LOC_ID AND L.CLASS_ID = 1 ");
            sbSql.Append("  right join M_FIRE_REPORT F  on F.REPORT_ID = L.REPORT_ID");
            sbSql.Append("  WHERE F.REPORT_ID = U0.REPORT_ID");
            sbSql.Append("  FOR XML PATH('')");
            sbSql.Append("  ) AS REPORT_LOCATION1 ");
            sbSql.Append("  ,(");
            sbSql.Append("  SELECT CONVERT(VARCHAR,V0.LOC_ID) + ','");
            sbSql.Append("  FROM V_LOCATION V0");
            sbSql.Append("  right join M_FIRE_LOCATION L  on L.LOC_ID  = V0.LOC_ID AND L.CLASS_ID = 2 ");
            sbSql.Append("  right join M_FIRE_REPORT F  on F.REPORT_ID  = L.REPORT_ID");
            sbSql.Append("  WHERE F.REPORT_ID = U0.REPORT_ID");
            sbSql.Append("  FOR XML PATH('')");
            sbSql.Append("  ) AS REPORT_LOCATION2 ");
            sbSql.Append("  ,(");
            sbSql.Append("  SELECT V0.LOC_NAME + ','");
            sbSql.Append("  FROM V_LOCATION V0");
            sbSql.Append("  right join M_FIRE_LOCATION L  on L.LOC_ID  = V0.LOC_ID AND L.CLASS_ID = 1 ");
            sbSql.Append("  right join M_FIRE_REPORT F  on F.REPORT_ID = L.REPORT_ID");
            sbSql.Append("  WHERE F.REPORT_ID = U0.REPORT_ID");
            sbSql.Append("  FOR XML PATH('')");
            sbSql.Append("  ) AS REPORT_LOCATION_NAME1 ");
            sbSql.Append("  ,(");
            sbSql.Append("  SELECT V0.LOC_NAME + ','");
            sbSql.Append("  FROM V_LOCATION V0");
            sbSql.Append("  right join M_FIRE_LOCATION L  on L.LOC_ID  = V0.LOC_ID AND L.CLASS_ID = 2");
            sbSql.Append("  right join M_FIRE_REPORT F  on F.REPORT_ID  = L.REPORT_ID");
            sbSql.Append("  WHERE F.REPORT_ID = U0.REPORT_ID");
            sbSql.Append("  FOR XML PATH('')");
            sbSql.Append("  ) AS REPORT_LOCATION_NAME2 ");
            sbSql.Append(" from");
            sbSql.Append("   M_FIRE_REPORT U0");
            sbSql.Append("   left join M_COMMON C0  on U0.REPORT_PLACE_KEY  = C0.KEY_VALUE AND C0.TABLE_NAME = 'M_PLACE'");
            sbSql.Append("   left join M_FIRE_LOCATION L0  on U0.REPORT_ID  = L0.REPORT_ID");
            sbSql.Append(" where");
            sbSql.Append("   U0.REPORT_ID = " + Convert.ToString(HttpContext.Current.Session[SessionConst.MasterKeys]) + "");
            sql = sbSql.ToString();
        }

        public bool DoSqlInsert(string strUserCd, M_FIRE mFIRE)
        {
            bool ret = false;
            Hashtable _params1 = new Hashtable();
            Hashtable _params2 = new Hashtable();
            Hashtable _params3 = new Hashtable();
            StringBuilder sbSql1 = new StringBuilder();
            StringBuilder sbSql2 = new StringBuilder();
            StringBuilder sbSql3 = new StringBuilder();

            DbContext ctx = null;
            try
            {
                //パラメター設定(M_FIRE_REPORT)
                _params1.Add("REPORT_NAME", mFIRE.REPORT_NAME);
                _params1.Add("REPORT_YMD", mFIRE.REPORT_YMD);
                _params1.Add("REPORT_SUBMIT", mFIRE.REPORT_SUBMIT);
                if (mFIRE.REPORT_PLACE_KEY == "-1")
                {
                    _params1.Add("REPORT_PLACE_KEY", null);
                }
                else
                {
                    _params1.Add("REPORT_PLACE_KEY", mFIRE.REPORT_PLACE_KEY);
                }
                _params1.Add("REG_USER_CD", strUserCd);

                // SQL生成(M_FIRE_REPORT)
                sbSql1.Append("INSERT INTO");
                sbSql1.Append(" M_FIRE_REPORT");
                sbSql1.Append("(");
                sbSql1.Append("  REPORT_NAME");
                sbSql1.Append(" ,REPORT_YMD");
                sbSql1.Append(" ,REPORT_SUBMIT");
                sbSql1.Append(" ,REPORT_PLACE_KEY");
                sbSql1.Append(" ,REG_USER_CD");
                sbSql1.Append(") VALUES (");
                sbSql1.Append("  :REPORT_NAME");
                sbSql1.Append(" ,:REPORT_YMD");
                sbSql1.Append(" ,:REPORT_SUBMIT");
                sbSql1.Append(" ,:REPORT_PLACE_KEY");
                sbSql1.Append(" ,:REG_USER_CD");
                sbSql1.Append(")");

                ctx = DbUtil.Open(true);
                DbUtil.ExecuteUpdate(ctx, sbSql1.ToString(), _params1);  //M_FIRE_REPORT

                long lngReportId = GetId(ctx); //REPORT_IDを取得

                //パラメター設定(M_FIRE_LOCATION)
                _params2.Add("REPORT_ID", lngReportId);
                _params2.Add("CLASS_ID", 1);
                _params2.Add("REG_USER_CD", strUserCd);

                // SQL生成(M_FIRE_LOCATION)
                sbSql2.Append("INSERT INTO");
                sbSql2.Append(" M_FIRE_LOCATION");
                sbSql2.Append("(");
                sbSql2.Append("  REPORT_ID");
                sbSql2.Append(" ,CLASS_ID");
                sbSql2.Append(" ,LOC_ID");
                sbSql2.Append(" ,REG_USER_CD");
                sbSql2.Append(") VALUES (");
                sbSql2.Append("  :REPORT_ID");
                sbSql2.Append(" ,:CLASS_ID");
                sbSql2.Append(" ,:LOC_ID");
                sbSql2.Append(" ,:REG_USER_CD");
                sbSql2.Append(")");

                if (mFIRE.hdnLocationId1 != null)
                {

                    if (mFIRE.hdnLocationId1.Trim() != string.Empty)
                    {
                        string[] strLOC_ID = mFIRE.hdnLocationId1.Split(',');

                        foreach (string locid in strLOC_ID)
                        {
                            _params2.Add("LOC_ID", locid);

                            DbUtil.ExecuteUpdate(ctx, sbSql2.ToString(), _params2); //M_FIRE_LOCATION

                            _params2.Remove("LOC_ID");
                        }
                    }
                }

                _params2 = new Hashtable();

                _params2.Add("REPORT_ID", lngReportId);
                _params2.Add("CLASS_ID", 2);
                _params2.Add("REG_USER_CD", strUserCd);
                if (mFIRE.hdnLocationId2 != null)
                {
                    if (mFIRE.hdnLocationId2.Trim() != string.Empty)
                    {
                        string[] strLOC_ID = mFIRE.hdnLocationId2.Split(',');

                        foreach (string locid in strLOC_ID)
                        {
                            _params2.Add("LOC_ID", locid);

                            DbUtil.ExecuteUpdate(ctx, sbSql2.ToString(), _params2); //M_FIRE_LOCATION

                            _params2.Remove("LOC_ID");
                        }
                    }
                }

                //パラメター設定(M_FIRE_SIZE)
                _params3.Add("REPORT_ID", lngReportId);
                _params3.Add("REG_USER_CD", strUserCd);

                // SQL生成(M_FIRE_SIZE)
                sbSql3.Append("INSERT INTO");
                sbSql3.Append(" M_FIRE_SIZE");
                sbSql3.Append("(");
                sbSql3.Append("  REPORT_ID");
                sbSql3.Append(" ,REG_TYPE_ID");
                sbSql3.Append(" ,FIRE_SIZE");
                sbSql3.Append(" ,REG_USER_CD");
                sbSql3.Append(") VALUES (");
                sbSql3.Append("  :REPORT_ID");
                sbSql3.Append(" ,:REG_TYPE_ID");
                sbSql3.Append(" ,:FIRE_SIZE");
                sbSql3.Append(" ,:REG_USER_CD");
                sbSql3.Append(")");

                List<tblFireClass> tblFire = new List<tblFireClass>();

                if (mFIRE.TYPE == "New")
                {
                    if (HttpContext.Current.Session["TBLFIRECLASS"] != null)
                    {

                        tblFire = (List<tblFireClass>)HttpContext.Current.Session["TBLFIRECLASS"];
                        for (int i = 0; i < tblFire.Count - 1; i++)
                        {
                            if (tblFire[i].txtFireSize != null && tblFire[i].hdnRegTypeId != null)
                            {
                                if (tblFire[i].txtFireSize != string.Empty && tblFire[i].hdnRegTypeId != string.Empty)
                                {
                                    _params3.Add("FIRE_SIZE", tblFire[i].txtFireSize);
                                    _params3.Add("REG_TYPE_ID", tblFire[i].hdnRegTypeId);


                                    DbUtil.ExecuteUpdate(ctx, sbSql3.ToString(), _params3); //M_FIRE_SIZE

                                    _params3.Remove("REG_TYPE_ID");
                                    _params3.Remove("FIRE_SIZE");
                                }
                            }
                        }
                    }
                }
                else
                {

                    //List<tblFireClass> tblFire = new List<tblFireClass>();
                    if (HttpContext.Current.Session["TblFireListUpd"] != null)
                    {

                        tblFire = (List<tblFireClass>)HttpContext.Current.Session["TblFireListUpd"];
                        for (int i = 0; i < tblFire.Count; i++)
                        {
                            //TextBox _txtFireSize = (TextBox)tblFire.FindControl("txtFireSize" + i);
                            //HiddenField _hdnRegTypeId = (HiddenField)tblFire.FindControl("hdnRegTypeId" + i);

                            if (tblFire[i].txtFireSize != null && tblFire[i].hdnRegTypeId != null)
                            {
                                if (tblFire[i].txtFireSize != string.Empty && tblFire[i].hdnRegTypeId != string.Empty)
                                {
                                    _params3.Add("FIRE_SIZE", tblFire[i].txtFireSize);
                                    _params3.Add("REG_TYPE_ID", tblFire[i].hdnRegTypeId);


                                    DbUtil.ExecuteUpdate(ctx, sbSql3.ToString(), _params3); //M_FIRE_SIZE

                                    _params3.Remove("REG_TYPE_ID");
                                    _params3.Remove("FIRE_SIZE");
                                }
                            }
                        }
                    }
                }



                ctx.Commit();
                ctx.Close();
                ret = true;
            }
            catch (Exception ex)
            {
                ctx.Rollback();
                ctx.Close();
                throw ex;
            }

            return ret;
        }

        private long GetId(DbContext ctx)
        {
            string sql = null;
            Hashtable _params = new Hashtable();
            DataTable dt;

            sql = "select @@IDENTITY AS ID";

            dt = DbUtil.Select(ctx, sql, _params);

            return Convert.ToInt64(dt.Rows[0]["ID"]);
        }

        


        ///// <summary>
        ///// 消防法側届出数量入力時
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //public JsonResult txt_TextChanged(string txtFireSize, string id, string txtFireSizeTotal, string lblMultiTotal, string regTypeId, string lblMultipleValue, string hdnFireSizeValue)
        //{

        //    //tblFireClass tblFireCls = new tblFireClass();
        //    List<string> result = new List<string>();
        //    decimal oldTxtValue = 0;
        //    decimal oldLblValue = 0;
        //    var txtId = Convert.ToUInt32(id);
        //    var tblId = 31;
        //    if ((ZyCoaG.MasterMaintenance.MaintenanceType)HttpContext.Current.Session[SessionConst.MaintenanceType] == ZyCoaG.MasterMaintenance.MaintenanceType.New)
        //    {
        //        tblFireClass tblFireCls = new tblFireClass();
        //        List<tblFireClass> tblFireClass = new List<tblFireClass>();
        //        if (HttpContext.Current.Session["TBLFIRECLASS"] != null)
        //        {
        //            tblFireClass = (List<tblFireClass>)HttpContext.Current.Session["TBLFIRECLASS"];

        //            foreach (var item in tblFireClass)
        //            {
        //                if (item.id == id.Trim())
        //                {
        //                    string lblMultiple = null;
        //                    if (Common.IsNumber(txtFireSize) == false)
        //                    {
        //                        result.Add("Number");
        //                        result.Add(null);
        //                        result.Add(null);
        //                        result.Add(null);
        //                        return Json(result, JsonRequestBehavior.AllowGet);
        //                    }
        //                    oldTxtValue = Convert.ToDecimal(item.txtFireSize);
        //                    //oldLblValue = Convert.ToDecimal(item.);
        //                    if (txtFireSize.Trim() == "")
        //                    {

        //                    }
        //                    else
        //                    {
        //                        txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
        //                        item.txtFireSize = txtFireSize;
        //                        if (item.hdnFireSize == "")
        //                        {

        //                        }
        //                        else
        //                        {
        //                            lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(item.hdnFireSize), 4, MidpointRounding.AwayFromZero).ToString("F4"));
        //                        }
        //                    }

        //                    decimal dblTotal = 0;
        //                    decimal dblTotal2 = 0;
        //                    decimal dblSubTxt = 0;
        //                    decimal dblSubLbl = 0;

        //                    if (txtFireSize != null && lblMultiple != null)
        //                    {
        //                        if (Common.IsNumber(txtFireSize) == true)
        //                        {
        //                            //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
        //                            dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
        //                            if (txtFireSizeTotal != "")
        //                            {
        //                                if (regTypeId == item.hdnRegTypeId)
        //                                {
        //                                    if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
        //                                    {
        //                                        dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
        //                                        dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
        //                                    }
        //                                    else
        //                                    {
        //                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        if (Common.IsNumber(lblMultiple) == true)
        //                        {
        //                            if (lblMultipleValue == "")
        //                            {
        //                                oldLblValue = 0;
        //                            }
        //                            else
        //                            {
        //                                oldLblValue = Convert.ToDecimal(lblMultipleValue);
        //                            }

        //                            dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
        //                            if (lblMultiTotal != "")
        //                            {
        //                                if (regTypeId == item.hdnRegTypeId)
        //                                {
        //                                    if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
        //                                    {
        //                                        dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
        //                                        dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
        //                                    }
        //                                    else
        //                                    {
        //                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                                    }
        //                                }

        //                                //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (txtFireSizeTotal == "" && lblMultiTotal == "")
        //                        {
        //                            txtFireSizeTotal = "0";
        //                            lblMultiTotal = "0";
        //                        }
        //                        dblTotal = Convert.ToDecimal(txtFireSizeTotal);
        //                        dblTotal2 = Convert.ToDecimal(lblMultiTotal);
        //                    }



        //                    string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
        //                    string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));


        //                    result.Add(lblMultiple);
        //                    result.Add(txtFireSize);
        //                    result.Add(fireSizeTotal);
        //                    result.Add(lblMultipleTotal);


        //                    item.hdnRegTypeId = regTypeId;
        //                    item.hdnFireSize = hdnFireSizeValue;
        //                    item.mulFirSize = lblMultiple;
        //                    //tblFireClass.Add(item);
        //                    break;
        //                }

        //            }

        //        }
        //        HttpContext.Current.Session["TBLFIRECLASS"] = tblFireClass;
        //    }
        //    else
        //    {
        //        tblFireClass tblFires = new tblFireClass();
        //        List<tblFireClass> tblFireClass = new List<tblFireClass>();
        //        int i;
        //        int val = Convert.ToInt32(id);
        //        if (txtFireSize == "")
        //        {
        //            if (HttpContext.Current.Session["TblFireListUpd"] != null)
        //            {
        //                tblFireClass = (List<tblFireClass>)HttpContext.Current.Session["TblFireListUpd"];
        //                for (i = 0; i < tblFireClass.Count; i++)
        //                {
        //                    if (val < tblFireClass.Count)
        //                    {
        //                        if (tblFireClass[val].hdnRegTypeId == regTypeId.Trim())
        //                        {
        //                            string lblMultiple = null;

        //                            oldTxtValue = Convert.ToDecimal(tblFireClass[val].txtFireSize);

        //                            if (txtFireSize.Trim() == "")
        //                            {
        //                                txtFireSize = "0";
        //                                lblMultiple = "0";
        //                            }
        //                            else
        //                            {
        //                                txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
        //                                tblFireClass[val].txtFireSize = txtFireSize;
        //                                lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(tblFireClass[val].hdnFireSize), 4, MidpointRounding.AwayFromZero).ToString("F4"));
        //                            }

        //                            decimal dblTotal = 0;
        //                            decimal dblTotal2 = 0;
        //                            decimal dblSubTxt = 0;
        //                            decimal dblSubLbl = 0;


        //                            if (txtFireSize != null && lblMultiple != null)
        //                            {
        //                                if (Common.IsNumber(txtFireSize) == true)
        //                                {
        //                                    //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
        //                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
        //                                    if (txtFireSizeTotal != "")
        //                                    {
        //                                        if (regTypeId == tblFireClass[val].hdnRegTypeId)
        //                                        {
        //                                            if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
        //                                            {
        //                                                dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
        //                                                dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
        //                                            }
        //                                            else
        //                                            {
        //                                                dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
        //                                            }
        //                                        }
        //                                    }


        //                                }

        //                                if (Common.IsNumber(lblMultiple) == true)
        //                                {
        //                                    if (lblMultipleValue == "")
        //                                    {
        //                                        oldLblValue = 0;
        //                                    }
        //                                    else
        //                                    {
        //                                        oldLblValue = Convert.ToDecimal(lblMultipleValue);
        //                                    }

        //                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
        //                                    if (lblMultiTotal != "")
        //                                    {
        //                                        if (regTypeId == tblFireClass[val].hdnRegTypeId)
        //                                        {
        //                                            if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
        //                                            {
        //                                                dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
        //                                                dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
        //                                            }
        //                                            else
        //                                            {
        //                                                dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                                            }
        //                                        }

        //                                    }
        //                                }
        //                            }
        //                            //}

        //                            string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
        //                            string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));
        //                            lblMultiple = string.Empty;
        //                            txtFireSize = string.Empty;

        //                            result.Add(lblMultiple);
        //                            result.Add(txtFireSize);
        //                            result.Add(fireSizeTotal);
        //                            result.Add(lblMultipleTotal);


        //                            tblFireClass[val].hdnRegTypeId = regTypeId;
        //                            tblFireClass[val].hdnFireSize = hdnFireSizeValue;
        //                            tblFireClass[val].mulFirSize = lblMultiple;

        //                            break;
        //                        }

        //                        else
        //                        {
        //                            string lblMultiple = null;
        //                            if (Common.IsNumber(txtFireSize) == false)
        //                            {
        //                                result.Add("Number");
        //                                result.Add(null);
        //                                result.Add(null);
        //                                result.Add(null);
        //                                return Json(result, JsonRequestBehavior.AllowGet);
        //                            }
        //                            foreach (var item in tblFireClass)
        //                            {
        //                                if (item.hdnRegTypeId == regTypeId)
        //                                {
        //                                    oldTxtValue = Convert.ToDecimal(item.txtFireSize);
        //                                }
        //                            }


        //                            if (txtFireSize.Trim() == "")
        //                            {

        //                            }
        //                            else
        //                            {
        //                                txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
        //                                tblFires.txtFireSize = txtFireSize;
        //                                lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
        //                            }

        //                            decimal dblTotal = 0;
        //                            decimal dblTotal2 = 0;
        //                            decimal dblSubTxt = 0;
        //                            decimal dblSubLbl = 0;


        //                            if (txtFireSize != null && lblMultiple != null)
        //                            {
        //                                if (Common.IsNumber(txtFireSize) == true)
        //                                {
        //                                    //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
        //                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
        //                                    if (txtFireSizeTotal != "")
        //                                    {
        //                                        foreach (var item in tblFireClass)
        //                                        {
        //                                            if (regTypeId == item.hdnRegTypeId)
        //                                            {
        //                                                if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
        //                                                {
        //                                                    dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
        //                                                    dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
        //                                                }

        //                                            }
        //                                        }
        //                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);

        //                                    }

        //                                }

        //                                if (Common.IsNumber(lblMultiple) == true)
        //                                {
        //                                    if (lblMultipleValue == "")
        //                                    {
        //                                        oldLblValue = 0;
        //                                    }
        //                                    else
        //                                    {
        //                                        oldLblValue = Convert.ToDecimal(lblMultipleValue);
        //                                    }

        //                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
        //                                    if (lblMultiTotal != "")
        //                                    {
        //                                        foreach (var item in tblFireClass)
        //                                        {
        //                                            if (regTypeId == item.hdnRegTypeId)
        //                                            {
        //                                                if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
        //                                                {
        //                                                    dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
        //                                                    dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
        //                                                }
        //                                            }
        //                                        }

        //                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                                    }
        //                                }
        //                            }

        //                            string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
        //                            string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

        //                            result.Add(lblMultiple);
        //                            result.Add(txtFireSize);
        //                            result.Add(fireSizeTotal);
        //                            result.Add(lblMultipleTotal);

        //                            tblFires.hdnRegTypeId = regTypeId;
        //                            tblFires.hdnFireSize = hdnFireSizeValue;
        //                            tblFires.mulFirSize = lblMultiple;

        //                            tblFireClass.Add(tblFires);
        //                            break;
        //                        }
        //                    }

        //                    else
        //                    {
        //                        string lblMultiple = null;
        //                        if (Common.IsNumber(txtFireSize) == false)
        //                        {
        //                            result.Add("Number");
        //                            result.Add(null);
        //                            result.Add(null);
        //                            result.Add(null);
        //                            return Json(result, JsonRequestBehavior.AllowGet);
        //                        }
        //                        oldTxtValue = Convert.ToDecimal(tblFireClass[i].txtFireSize);

        //                        if (txtFireSize.Trim() == "")
        //                        {

        //                        }
        //                        else
        //                        {
        //                            txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
        //                            tblFires.txtFireSize = txtFireSize;
        //                            lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
        //                        }

        //                        decimal dblTotal = 0;
        //                        decimal dblTotal2 = 0;
        //                        decimal dblSubTxt = 0;
        //                        decimal dblSubLbl = 0;


        //                        if (txtFireSize != null && lblMultiple != null)
        //                        {
        //                            if (Common.IsNumber(txtFireSize) == true)
        //                            {
        //                                dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
        //                                if (txtFireSizeTotal != "")
        //                                {
        //                                    if (regTypeId == tblFireClass[i].hdnRegTypeId)
        //                                    {
        //                                        if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
        //                                        {
        //                                            dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
        //                                            dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
        //                                        }
        //                                        else
        //                                        {
        //                                            dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
        //                                    }
        //                                }
        //                            }

        //                            if (Common.IsNumber(lblMultiple) == true)
        //                            {
        //                                if (lblMultipleValue == "")
        //                                {
        //                                    oldLblValue = 0;
        //                                }
        //                                else
        //                                {
        //                                    oldLblValue = Convert.ToDecimal(lblMultipleValue);
        //                                }

        //                                dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
        //                                if (lblMultiTotal != "")
        //                                {
        //                                    if (regTypeId == tblFireClass[i].hdnRegTypeId)
        //                                    {
        //                                        if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
        //                                        {
        //                                            dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
        //                                            dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
        //                                        }
        //                                        else
        //                                        {
        //                                            dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                                        }
        //                                    }
        //                                    else
        //                                    {
        //                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
        //                        string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

        //                        result.Add(lblMultiple);
        //                        result.Add(txtFireSize);
        //                        result.Add(fireSizeTotal);
        //                        result.Add(lblMultipleTotal);

        //                        tblFires.hdnRegTypeId = regTypeId;
        //                        tblFires.hdnFireSize = hdnFireSizeValue;
        //                        tblFires.mulFirSize = lblMultiple;

        //                        tblFireClass.Add(tblFires);
        //                        break;
        //                    }

        //                }

        //            }
        //            return Json(result, JsonRequestBehavior.AllowGet);

        //        }

        //        if (HttpContext.Current.Session["TblFireListUpd"] != null)
        //        {
        //            tblFireClass = (List<tblFireClass>)HttpContext.Current.Session["TblFireListUpd"];
        //            for (i = 0; i < tblFireClass.Count; i++)
        //            {
        //                if (val < tblFireClass.Count)
        //                {
        //                    if (tblFireClass[val].hdnRegTypeId == regTypeId.Trim())
        //                    {
        //                        string lblMultiple = null;
        //                        if (Common.IsNumber(txtFireSize) == false)
        //                        {
        //                            result.Add("Number");
        //                            result.Add(null);
        //                            result.Add(null);
        //                            result.Add(null);
        //                            return Json(result, JsonRequestBehavior.AllowGet);
        //                        }
        //                        oldTxtValue = Convert.ToDecimal(tblFireClass[val].txtFireSize);

        //                        if (txtFireSize.Trim() == "")
        //                        {

        //                        }
        //                        else
        //                        {
        //                            txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
        //                            tblFireClass[val].txtFireSize = txtFireSize;
        //                            lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(tblFireClass[val].hdnFireSize), 4, MidpointRounding.AwayFromZero).ToString("F4"));
        //                        }

        //                        decimal dblTotal = 0;
        //                        decimal dblTotal2 = 0;
        //                        decimal dblSubTxt = 0;
        //                        decimal dblSubLbl = 0;


        //                        if (txtFireSize != null && lblMultiple != null)
        //                        {
        //                            if (Common.IsNumber(txtFireSize) == true)
        //                            {
        //                                dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
        //                                if (txtFireSizeTotal != "")
        //                                {
        //                                    if (regTypeId == tblFireClass[val].hdnRegTypeId)
        //                                    {
        //                                        if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
        //                                        {
        //                                            dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
        //                                            dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
        //                                        }
        //                                        else
        //                                        {
        //                                            dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
        //                                        }
        //                                    }
        //                                }
        //                            }

        //                            if (Common.IsNumber(lblMultiple) == true)
        //                            {
        //                                if (lblMultipleValue == "")
        //                                {
        //                                    oldLblValue = 0;
        //                                }
        //                                else
        //                                {
        //                                    oldLblValue = Convert.ToDecimal(lblMultipleValue);
        //                                }

        //                                dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
        //                                if (lblMultiTotal != "")
        //                                {
        //                                    if (regTypeId == tblFireClass[val].hdnRegTypeId)
        //                                    {
        //                                        if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
        //                                        {
        //                                            dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
        //                                            dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
        //                                        }
        //                                        else
        //                                        {
        //                                            dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                                        }
        //                                    }
        //                                }
        //                            }
        //                        }

        //                        string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
        //                        string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

        //                        result.Add(lblMultiple);
        //                        result.Add(txtFireSize);
        //                        result.Add(fireSizeTotal);
        //                        result.Add(lblMultipleTotal);


        //                        tblFireClass[val].hdnRegTypeId = regTypeId;
        //                        tblFireClass[val].hdnFireSize = hdnFireSizeValue;
        //                        tblFireClass[val].mulFirSize = lblMultiple;
        //                        break;
        //                    }

        //                    else
        //                    {
        //                        string lblMultiple = null;
        //                        if (Common.IsNumber(txtFireSize) == false)
        //                        {
        //                            result.Add("Number");
        //                            result.Add(null);
        //                            result.Add(null);
        //                            result.Add(null);
        //                            return Json(result, JsonRequestBehavior.AllowGet);
        //                        }
        //                        foreach (var item in tblFireClass)
        //                        {
        //                            if (item.hdnRegTypeId == regTypeId)
        //                            {
        //                                oldTxtValue = Convert.ToDecimal(item.txtFireSize);
        //                            }
        //                        }


        //                        if (txtFireSize.Trim() == "")
        //                        {

        //                        }
        //                        else
        //                        {
        //                            txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
        //                            tblFires.txtFireSize = txtFireSize;
        //                            lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
        //                        }

        //                        decimal dblTotal = 0;
        //                        decimal dblTotal2 = 0;
        //                        decimal dblSubTxt = 0;
        //                        decimal dblSubLbl = 0;


        //                        if (txtFireSize != null && lblMultiple != null)
        //                        {
        //                            if (Common.IsNumber(txtFireSize) == true)
        //                            {
        //                                dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
        //                                if (txtFireSizeTotal != "")
        //                                {
        //                                    foreach (var item in tblFireClass)
        //                                    {
        //                                        if (regTypeId == item.hdnRegTypeId)
        //                                        {
        //                                            if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
        //                                            {
        //                                                dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
        //                                                dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
        //                                            }
        //                                        }
        //                                    }
        //                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);

        //                                }
        //                            }

        //                            if (Common.IsNumber(lblMultiple) == true)
        //                            {
        //                                if (lblMultipleValue == "")
        //                                {
        //                                    oldLblValue = 0;
        //                                }
        //                                else
        //                                {
        //                                    oldLblValue = Convert.ToDecimal(lblMultipleValue);
        //                                }

        //                                dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
        //                                if (lblMultiTotal != "")
        //                                {
        //                                    foreach (var item in tblFireClass)
        //                                    {
        //                                        if (regTypeId == item.hdnRegTypeId)
        //                                        {
        //                                            if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
        //                                            {
        //                                                dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
        //                                                dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
        //                                            }
        //                                        }
        //                                    }

        //                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                                }
        //                            }
        //                        }

        //                        string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
        //                        string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

        //                        result.Add(lblMultiple);
        //                        result.Add(txtFireSize);
        //                        result.Add(fireSizeTotal);
        //                        result.Add(lblMultipleTotal);

        //                        tblFires.hdnRegTypeId = regTypeId;
        //                        tblFires.hdnFireSize = hdnFireSizeValue;
        //                        tblFires.mulFirSize = lblMultiple;

        //                        tblFireClass.Add(tblFires);
        //                        break;
        //                    }
        //                }
        //                else
        //                {
        //                    string lblMultiple = null;
        //                    if (Common.IsNumber(txtFireSize) == false)
        //                    {
        //                        result.Add("Number");
        //                        result.Add(null);
        //                        result.Add(null);
        //                        result.Add(null);
        //                        return Json(result, JsonRequestBehavior.AllowGet);
        //                    }
        //                    oldTxtValue = Convert.ToDecimal(tblFireClass[i].txtFireSize);

        //                    if (txtFireSize.Trim() == "")
        //                    {

        //                    }
        //                    else
        //                    {
        //                        txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
        //                        tblFires.txtFireSize = txtFireSize;
        //                        if (hdnFireSizeValue == "")
        //                        {

        //                        }
        //                        else
        //                        {
        //                            lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
        //                        }
        //                    }

        //                    decimal dblTotal = 0;
        //                    decimal dblTotal2 = 0;
        //                    decimal dblSubTxt = 0;
        //                    decimal dblSubLbl = 0;


        //                    if (txtFireSize != null && lblMultiple != null)
        //                    {
        //                        if (Common.IsNumber(txtFireSize) == true)
        //                        {
        //                            dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
        //                            if (txtFireSizeTotal != "")
        //                            {
        //                                if (regTypeId == tblFireClass[i].hdnRegTypeId)
        //                                {
        //                                    if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
        //                                    {
        //                                        dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
        //                                        dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
        //                                    }
        //                                    else
        //                                    {
        //                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
        //                                }
        //                            }
        //                        }

        //                        if (Common.IsNumber(lblMultiple) == true)
        //                        {
        //                            if (lblMultipleValue == "")
        //                            {
        //                                oldLblValue = 0;
        //                            }
        //                            else
        //                            {
        //                                oldLblValue = Convert.ToDecimal(lblMultipleValue);
        //                            }

        //                            dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
        //                            if (lblMultiTotal != "")
        //                            {
        //                                if (regTypeId == tblFireClass[i].hdnRegTypeId)
        //                                {
        //                                    if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
        //                                    {
        //                                        dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
        //                                        dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
        //                                    }
        //                                    else
        //                                    {
        //                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                                    }
        //                                }
        //                                else
        //                                {
        //                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        if (txtFireSizeTotal == "" && lblMultiTotal == "")
        //                        {
        //                            txtFireSizeTotal = "0";
        //                            lblMultiTotal = "0";
        //                        }
        //                        dblTotal = Convert.ToDecimal(txtFireSizeTotal);
        //                        dblTotal2 = Convert.ToDecimal(lblMultiTotal);
        //                    }
        //                    //}

        //                    string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
        //                    string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

        //                    result.Add(lblMultiple);
        //                    result.Add(txtFireSize);
        //                    result.Add(fireSizeTotal);
        //                    result.Add(lblMultipleTotal);

        //                    if (regTypeId == tblFireClass[i].hdnRegTypeId)
        //                    {
        //                        tblFireClass.RemoveAt(i);
        //                    }
        //                    tblFires.hdnRegTypeId = regTypeId;
        //                    tblFires.hdnFireSize = hdnFireSizeValue;
        //                    tblFires.mulFirSize = lblMultiple;

        //                    tblFireClass.Add(tblFires);
        //                    break;
        //                }

        //            }
        //        }
        //        else
        //        {
        //            string lblMultiple = null;
        //            if (Common.IsNumber(txtFireSize) == false)
        //            {
        //                result.Add("Number");
        //                result.Add(null);
        //                result.Add(null);
        //                result.Add(null);
        //                return Json(result, JsonRequestBehavior.AllowGet);
        //            }
        //            oldTxtValue = Convert.ToDecimal(txtFireSize);
        //            //oldLblValue = Convert.ToDecimal(item.);
        //            if (txtFireSize.Trim() == "")
        //            {

        //            }
        //            else
        //            {
        //                txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
        //                tblFires.txtFireSize = txtFireSize;
        //                lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
        //            }

        //            decimal dblTotal = 0;
        //            decimal dblTotal2 = 0;
        //            decimal dblSubTxt = 0;
        //            decimal dblSubLbl = 0;

        //            if (txtFireSize != null && lblMultiple != null)
        //            {
        //                if (Common.IsNumber(txtFireSize) == true)
        //                {
        //                    //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
        //                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
        //                    if (txtFireSizeTotal != "")
        //                    {

        //                        if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
        //                        {
        //                            dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
        //                            dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
        //                        }
        //                        else
        //                        {
        //                            dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
        //                        }
        //                        //}
        //                    }
        //                }

        //                if (Common.IsNumber(lblMultiple) == true)
        //                {
        //                    if (lblMultipleValue == "")
        //                    {
        //                        oldLblValue = 0;
        //                    }
        //                    else
        //                    {
        //                        oldLblValue = Convert.ToDecimal(lblMultipleValue);
        //                    }

        //                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
        //                    if (lblMultiTotal != "")
        //                    {

        //                        if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
        //                        {
        //                            dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
        //                            dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
        //                        }
        //                        else
        //                        {
        //                            dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                        }

        //                        //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
        //                    }
        //                }
        //            }
        //            //}

        //            string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
        //            string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

        //            result.Add(lblMultiple);
        //            result.Add(txtFireSize);
        //            result.Add(fireSizeTotal);
        //            result.Add(lblMultipleTotal);

        //            tblFires.hdnRegTypeId = regTypeId;
        //            tblFires.hdnFireSize = hdnFireSizeValue;
        //            tblFires.mulFirSize = lblMultiple;


        //            tblFireClass.Add(tblFires);

        //        }
        //        HttpContext.Current.Session["TblFireListUpd"] = tblFireClass;
        //    }

        //    return Json(result, JsonRequestBehavior.AllowGet);
        //}

        




        public bool DoSqlUpdate(string strUserCd)
        {
            bool ret = false;
            Hashtable _params1 = new Hashtable();
            Hashtable _params2 = new Hashtable();
            Hashtable _params3 = new Hashtable();
            StringBuilder sbSql1 = new StringBuilder();
            StringBuilder sbSql2 = new StringBuilder();
            StringBuilder sbSql3 = new StringBuilder();
            M_FIRE mFire = new M_FIRE();
            DbContext ctx = null;
            try
            {
                if (HttpContext.Current.Session["ZC05032UpdateData"] != null)
                {
                    mFire = (M_FIRE)HttpContext.Current.Session["ZC05032UpdateData"];
                    //パラメター設定(M_FIRE_REPORT)
                    _params1.Add("REPORT_NAME", mFire.REPORT_NAME);
                    _params1.Add("REPORT_YMD", mFire.REPORT_YMD);
                    _params1.Add("REPORT_SUBMIT", mFire.REPORT_SUBMIT);


                    if (mFire.REPORT_PLACE_KEY == "-1")
                    {
                        _params1.Add("REPORT_PLACE_KEY", null);
                    }
                    else
                    {
                        _params1.Add("REPORT_PLACE_KEY", mFire.REPORT_PLACE_KEY);
                    }
                }
                _params1.Add("UPD_USER_CD", strUserCd);
                _params1.Add("UPD_DATE", DateTime.Now);
                _params1.Add("REPORT_ID", Convert.ToString(HttpContext.Current.Session[SessionConst.MasterKeys]));

                // SQL生成(M_FIRE_REPORT)
                sbSql1.Append("UPDATE");
                sbSql1.Append("    M_FIRE_REPORT");
                sbSql1.Append(" SET");
                sbSql1.Append("   REPORT_NAME = :REPORT_NAME");
                sbSql1.Append("  ,REPORT_YMD = :REPORT_YMD");
                sbSql1.Append("  ,REPORT_SUBMIT = :REPORT_SUBMIT");
                sbSql1.Append("  ,REPORT_PLACE_KEY = :REPORT_PLACE_KEY");
                sbSql1.Append("  ,UPD_USER_CD = :UPD_USER_CD");
                sbSql1.Append("  ,UPD_DATE = :UPD_DATE");
                sbSql1.Append(" WHERE");
                sbSql1.Append("    REPORT_ID = :REPORT_ID");

                ctx = DbUtil.Open(true);
                DbUtil.ExecuteUpdate(ctx, sbSql1.ToString(), _params1);  //M_FIRE_REPORT

                //パラメター設定(M_FIRE_LOCATION)
                _params2.Add("REPORT_ID", Convert.ToString(HttpContext.Current.Session[SessionConst.MasterKeys]));

                // SQL生成(M_FIRE_LOCATION)
                sbSql2.Append("DELETE FROM ");
                sbSql2.Append(" M_FIRE_LOCATION");
                sbSql2.Append(" WHERE");
                sbSql2.Append("    REPORT_ID = :REPORT_ID");
                DbUtil.ExecuteUpdate(ctx, sbSql2.ToString(), _params2);  //M_FIRE_LOCATION

                _params2 = new Hashtable();

                //パラメター設定(M_FIRE_LOCATION)
                _params2.Add("REPORT_ID", Convert.ToString(HttpContext.Current.Session[SessionConst.MasterKeys]));
                _params2.Add("CLASS_ID", 1);
                _params2.Add("REG_USER_CD", strUserCd);

                // SQL生成(M_FIRE_LOCATION)
                sbSql2.Clear();
                sbSql2.Append("INSERT INTO");
                sbSql2.Append(" M_FIRE_LOCATION");
                sbSql2.Append("(");
                sbSql2.Append("  REPORT_ID");
                sbSql2.Append(" ,CLASS_ID");
                sbSql2.Append(" ,LOC_ID");
                sbSql2.Append(" ,REG_USER_CD");
                sbSql2.Append(") VALUES (");
                sbSql2.Append("  :REPORT_ID");
                sbSql2.Append(" ,:CLASS_ID");
                sbSql2.Append(" ,:LOC_ID");
                sbSql2.Append(" ,:REG_USER_CD");
                sbSql2.Append(")");
                if (mFire.hdnLocationId1 != null)
                {
                    if (mFire.hdnLocationId1.Trim() != string.Empty)
                    {
                        string[] strLOC_ID = mFire.hdnLocationId1.Split(',');

                        foreach (string locid in strLOC_ID)
                        {
                            _params2.Add("LOC_ID", locid);

                            DbUtil.ExecuteUpdate(ctx, sbSql2.ToString(), _params2); //M_FIRE_LOCATION

                            _params2.Remove("LOC_ID");
                        }
                    }
                }

                _params2 = new Hashtable();

                _params2.Add("REPORT_ID", Convert.ToString(HttpContext.Current.Session[SessionConst.MasterKeys]));
                _params2.Add("CLASS_ID", 2);
                _params2.Add("REG_USER_CD", strUserCd);

                if (mFire.hdnLocationId2.Trim() != string.Empty)
                {
                    string[] strLOC_ID = mFire.hdnLocationId2.Split(',');

                    foreach (string locid in strLOC_ID)
                    {
                        _params2.Add("LOC_ID", locid);

                        DbUtil.ExecuteUpdate(ctx, sbSql2.ToString(), _params2); //M_FIRE_LOCATION

                        _params2.Remove("LOC_ID");
                    }
                }

                //パラメター設定(M_FIRE_SIZE)
                _params3.Add("REPORT_ID", Convert.ToString(HttpContext.Current.Session[SessionConst.MasterKeys]));

                // SQL生成(M_FIRE_SIZE)
                sbSql3.Append("DELETE FROM ");
                sbSql3.Append(" M_FIRE_SIZE");
                sbSql3.Append(" WHERE");
                sbSql3.Append("    REPORT_ID = :REPORT_ID");

                DbUtil.ExecuteUpdate(ctx, sbSql3.ToString(), _params3); //M_FIRE_SIZE

                //パラメター設定(M_FIRE_SIZE)
                _params3 = new Hashtable();
                _params3.Add("REPORT_ID", Convert.ToString(HttpContext.Current.Session[SessionConst.MasterKeys]));
                _params3.Add("REG_USER_CD", strUserCd);

                // SQL生成(M_FIRE_SIZE)
                sbSql3.Clear();
                sbSql3.Append("INSERT INTO");
                sbSql3.Append(" M_FIRE_SIZE");
                sbSql3.Append("(");
                sbSql3.Append("  REPORT_ID");
                sbSql3.Append(" ,REG_TYPE_ID");
                sbSql3.Append(" ,FIRE_SIZE");
                sbSql3.Append(" ,REG_USER_CD");
                sbSql3.Append(") VALUES (");
                sbSql3.Append("  :REPORT_ID");
                sbSql3.Append(" ,:REG_TYPE_ID");
                sbSql3.Append(" ,:FIRE_SIZE");
                sbSql3.Append(" ,:REG_USER_CD");
                sbSql3.Append(")");

                List<tblFireClass> tblFire = new List<tblFireClass>();
                if (HttpContext.Current.Session["TblFireListUpd"] != null)
                {
                    tblFire = (List<tblFireClass>)HttpContext.Current.Session["TblFireListUpd"];
                    for (int i = 0; i < tblFire.Count; i++)
                    {
                        if (tblFire[i].txtFireSize != null && tblFire[i].hdnRegTypeId != null)
                        {
                            if (tblFire[i].txtFireSize != string.Empty && tblFire[i].hdnRegTypeId != string.Empty)
                            {
                                _params3.Add("FIRE_SIZE", tblFire[i].txtFireSize);
                                _params3.Add("REG_TYPE_ID", tblFire[i].hdnRegTypeId);


                                DbUtil.ExecuteUpdate(ctx, sbSql3.ToString(), _params3); //M_FIRE_SIZE

                                _params3.Remove("REG_TYPE_ID");
                                _params3.Remove("FIRE_SIZE");
                            }
                        }
                    }
                }
                ctx.Commit();
                ctx.Close();
                ret = true;
            }
            catch (Exception ex)
            {
                ctx.Rollback();
                ctx.Close();
                throw ex;
            }

            return ret;
        }

        public bool DoSqlDelete(string strUserCd)
        {
            bool ret = false;
            Hashtable _params1 = new Hashtable();
            StringBuilder sbSql1 = new StringBuilder();

            DbContext ctx = null;
            try
            {
                //パラメター設定(M_FIRE_REPORT)
                _params1.Add("DEL_USER_CD", strUserCd);
                _params1.Add("DEL_DATE", DateTime.Now);
                _params1.Add("DEL_FLAG", Convert.ToInt32(DEL_FLAG.Deleted));
                _params1.Add("REPORT_ID", Convert.ToString(HttpContext.Current.Session[SessionConst.MasterKeys]));

                // SQL生成(M_FIRE_REPORT)
                sbSql1.Append("UPDATE");
                sbSql1.Append("    M_FIRE_REPORT");
                sbSql1.Append(" SET");
                sbSql1.Append("  DEL_USER_CD = :DEL_USER_CD,");
                sbSql1.Append("  DEL_DATE = :DEL_DATE,");
                sbSql1.Append("  DEL_FLAG = :DEL_FLAG");
                sbSql1.Append(" WHERE");
                sbSql1.Append("    REPORT_ID = :REPORT_ID");

                ctx = DbUtil.Open(true);
                DbUtil.ExecuteUpdate(ctx, sbSql1.ToString(), _params1);  //M_FIRE_REPORT

                ctx.Commit();
                ctx.Close();
                ret = true;
            }
            catch (Exception ex)
            {
                ctx.Rollback();
                ctx.Close();
                throw ex;
            }

            return ret;
        }


        /// <summary>
        /// メーカーコンボボックスの初期化
        /// </summary>
        public List<M_COMMON> InitHandlingOfficeComboBox()
        {
            string sql = null;
            DynamicParameters _params = new DynamicParameters();

            //MasterService masterService = new MasterService();
            string sqlStr = GetPlaceSql(ref sql, ref _params);
            List<M_COMMON> dtCommon = DbUtil.Select<M_COMMON>(sqlStr, _params);

            return dtCommon;
            //Common.DataBindComboBox(cbMaker_STOCK, 230, dt, colID.MAKER, colID.MAKERID, true);
        }

        /// <summary>
        /// 取扱所一覧を取得
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="_params"></param>
        private string GetPlaceSql(ref string sql, ref DynamicParameters _params)
        {
            StringBuilder sbSql = new StringBuilder();

            // SELECT SQL生成
            sbSql.Append(" SELECT");
            sbSql.Append("   KEY_VALUE ");
            sbSql.Append("  ,DISPLAY_VALUE");
            sbSql.Append(" FROM");
            sbSql.Append(" M_COMMON");
            sbSql.Append(" WHERE");
            sbSql.Append(" TABLE_NAME = 'M_PLACE'");
            sbSql.Append(" order by");
            sbSql.Append("  DISPLAY_ORDER");
            sql = sbSql.ToString();


            //DynamicParameters _params = new DynamicParameters();


            return sql;

        }


        private bool GetSqlUpdateMGroup(ref string sql, ref Hashtable _params, ref string status)
        {
            UserInfo.ClsUser userInfo = null;
            Common.GetUserInfo(HttpContext.Current.Session, out userInfo);

            //新規か更新か判断
            Hashtable _para = new Hashtable();
            var createSql = new StringBuilder();
            createSql.AppendLine("select * FROM M_GROUP WHERE GROUP_ID = :GROUP_ID");
            _para.Add("GROUP_ID", inputParam["GROUP_ID"]);
            DataTable dt = DbUtil.Select(createSql.ToString(), _para);

            createSql.Clear();

            if (dt.Rows.Count == 0)
            {
                //新規
                createSql.AppendLine("INSERT INTO M_GROUP(");
                createSql.AppendLine("GROUP_CD");
                createSql.AppendLine(",GROUP_NAME");
                createSql.AppendLine(",P_GROUP_CD");
                createSql.AppendLine(",REG_USER_CD");
                createSql.AppendLine(",REG_DATE");
                createSql.AppendLine(",BATCH_FLAG");

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",DEL_USER_CD");
                    createSql.AppendLine(",DEL_DATE");
                    createSql.AppendLine(",DEL_FLAG");
                }
                createSql.AppendLine(") ");
                createSql.AppendLine("VALUES (");
                createSql.AppendLine(":GROUP_CD");
                _params.Add("GROUP_CD", inputParam["GROUP_CD"]);
                createSql.AppendLine(",:GROUP_NAME");
                _params.Add("GROUP_NAME", inputParam["GROUP_NAME"]);

                createSql.AppendLine(",:P_GROUP_CD");
                _para.Clear();
                _para.Add("P_GROUP_NAME", inputParam["P_GROUP_CD"]);
                dt = DbUtil.Select("SELECT GROUP_CD FROM V_GROUP WHERE GROUP_NAME = :P_GROUP_NAME", _para);
                _params.Add("P_GROUP_CD", dt.Rows.Count == 0 ? null : dt.Rows[0]["GROUP_CD"]);

                createSql.AppendLine(",:REG_USER_CD");
                _params.Add("REG_USER_CD", userInfo.USER_CD);
                createSql.AppendLine(",:REG_DATE");
                _params.Add("REG_DATE", DateTime.Now);
                createSql.AppendLine(",:BATCH_FLAG");
                _params.Add("BATCH_FLAG", inputParam["BATCH_FLAG"] == "対象外" ? 1 : 0);

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",:DEL_USER_CD");
                    _params.Add("DEL_USER_CD", userInfo.USER_CD);
                    createSql.AppendLine(",:DEL_DATE");
                    _params.Add("DEL_DATE", DateTime.Now);
                    createSql.AppendLine(",:DEL_FLAG");
                    _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);
                }
                createSql.AppendLine(") ");
            }
            else
            {
                //更新
                createSql.AppendLine("UPDATE M_GROUP ");
                createSql.AppendLine("SET GROUP_CD = :GROUP_CD");
                _params.Add("GROUP_CD", inputParam["GROUP_CD"]);
                createSql.AppendLine(",GROUP_NAME = :GROUP_NAME");
                _params.Add("GROUP_NAME", inputParam["GROUP_NAME"]);

                createSql.AppendLine(",P_GROUP_CD = :P_GROUP_CD");
                _para.Clear();
                _para.Add("P_GROUP_NAME", inputParam["P_GROUP_CD"]);
                dt = DbUtil.Select("SELECT GROUP_CD FROM V_GROUP WHERE GROUP_NAME = :P_GROUP_NAME", _para);
                _params.Add("P_GROUP_CD", dt.Rows.Count == 0 ? null : dt.Rows[0]["GROUP_CD"]);

                createSql.AppendLine(",UPD_USER_CD = :UPD_USER_CD");
                _params.Add("UPD_USER_CD", userInfo.USER_CD);

                createSql.AppendLine(",UPD_DATE = :UPD_DATE");
                _params.Add("UPD_DATE", DateTime.Now);

                createSql.AppendLine(",BATCH_FLAG = :BATCH_FLAG");
                _params.Add("BATCH_FLAG", inputParam["BATCH_FLAG"] == "対象外" ? 1 : 0);

                createSql.AppendLine(",DEL_USER_CD = :DEL_USER_CD");
                _params.Add("DEL_USER_CD", inputParam["DEL_FLAG"] == "削除済" ? userInfo.USER_CD : null);
                createSql.AppendLine(",DEL_DATE = :DEL_DATE");
                _params.Add("DEL_DATE", inputParam["DEL_FLAG"] == "削除済" ? DateTime.Now.ToString() : null);

                createSql.AppendLine(",DEL_FLAG = :DEL_FLAG");
                _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);

                createSql.AppendLine(" WHERE ");
                createSql.AppendLine(" GROUP_ID = :GROUP_ID ");
                _params.Add("GROUP_ID", inputParam["GROUP_ID"]);
            }

            sql = createSql.ToString();
            return true;
        }
        //2019/02/18 TPE.Sugimoto Add Sta
        private bool GetSqlUpdateMMaker(ref string sql, ref Hashtable _params, ref string status)
        {
            UserInfo.ClsUser userInfo = null;
            Common.GetUserInfo(HttpContext.Current.Session, out userInfo);

            //新規か更新か判断
            Hashtable _para = new Hashtable();
            var createSql = new StringBuilder();
            createSql.AppendLine("select * FROM M_MAKER WHERE MAKER_ID = :MAKER_ID");
            _para.Add("MAKER_ID", inputParam["MAKER_ID"]);
            DataTable dt = DbUtil.Select(createSql.ToString(), _para);

            createSql.Clear();

            if (dt.Rows.Count == 0)
            {
                //新規
                createSql.AppendLine("INSERT INTO M_MAKER(");
                createSql.AppendLine("MAKER_NAME");
                createSql.AppendLine(",MAKER_URL");
                createSql.AppendLine(",MAKER_TEL");
                createSql.AppendLine(",MAKER_EMAIL1");
                createSql.AppendLine(",MAKER_EMAIL2");
                createSql.AppendLine(",MAKER_EMAIL3");
                createSql.AppendLine(",MAKER_ADDRESS");
                createSql.AppendLine(",REG_USER_CD");
                createSql.AppendLine(",REG_DATE");

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",DEL_USER_CD");
                    createSql.AppendLine(",DEL_DATE");
                    createSql.AppendLine(",DEL_FLAG");
                }
                createSql.AppendLine(") ");
                createSql.AppendLine("VALUES (");
                createSql.AppendLine(":MAKER_NAME");
                _params.Add("MAKER_NAME", inputParam["MAKER_NAME"]);
                createSql.AppendLine(",:MAKER_URL");
                _params.Add("MAKER_URL", inputParam["MAKER_URL"]);
                createSql.AppendLine(",:MAKER_TEL");
                _params.Add("MAKER_TEL", inputParam["MAKER_TEL"]);
                createSql.AppendLine(",:MAKER_EMAIL1");
                _params.Add("MAKER_EMAIL1", inputParam["MAKER_EMAIL1"]);
                createSql.AppendLine(",:MAKER_EMAIL2");
                _params.Add("MAKER_EMAIL2", inputParam["MAKER_EMAIL2"]);
                createSql.AppendLine(",:MAKER_EMAIL3");
                _params.Add("MAKER_EMAIL3", inputParam["MAKER_EMAIL3"]);
                createSql.AppendLine(",:MAKER_ADDRESS");
                _params.Add("MAKER_ADDRESS", inputParam["MAKER_ADDRESS"]);

                createSql.AppendLine(",:REG_USER_CD");
                _params.Add("REG_USER_CD", userInfo.USER_CD);
                createSql.AppendLine(",:REG_DATE");
                _params.Add("REG_DATE", DateTime.Now);

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",:DEL_USER_CD");
                    _params.Add("DEL_USER_CD", userInfo.USER_CD);
                    createSql.AppendLine(",:DEL_DATE");
                    _params.Add("DEL_DATE", DateTime.Now);
                    createSql.AppendLine(",:DEL_FLAG");
                    _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);
                }
                createSql.AppendLine(") ");
            }
            else
            {
                //更新
                createSql.AppendLine("UPDATE M_MAKER ");
                createSql.AppendLine("SET MAKER_NAME = :MAKER_NAME");
                _params.Add("MAKER_NAME", inputParam["MAKER_NAME"]);
                createSql.AppendLine(",MAKER_URL = :MAKER_URL");
                _params.Add("MAKER_URL", inputParam["MAKER_URL"]);
                createSql.AppendLine(",MAKER_TEL = :MAKER_TEL");
                _params.Add("MAKER_TEL", inputParam["MAKER_TEL"]);
                createSql.AppendLine(",MAKER_EMAIL1 = :MAKER_EMAIL1");
                _params.Add("MAKER_EMAIL1", inputParam["MAKER_EMAIL1"]);
                createSql.AppendLine(",MAKER_EMAIL2 = :MAKER_EMAIL2");
                _params.Add("MAKER_EMAIL2", inputParam["MAKER_EMAIL2"]);
                createSql.AppendLine(",MAKER_EMAIL3 = :MAKER_EMAIL3");
                _params.Add("MAKER_EMAIL3", inputParam["MAKER_EMAIL3"]);
                createSql.AppendLine(",MAKER_ADDRESS = :MAKER_ADDRESS");
                _params.Add("MAKER_ADDRESS", inputParam["MAKER_ADDRESS"]);

                createSql.AppendLine(",UPD_USER_CD = :UPD_USER_CD");
                _params.Add("UPD_USER_CD", userInfo.USER_CD);

                createSql.AppendLine(",UPD_DATE = :UPD_DATE");
                _params.Add("UPD_DATE", DateTime.Now);

                createSql.AppendLine(",DEL_USER_CD = :DEL_USER_CD");
                _params.Add("DEL_USER_CD", inputParam["DEL_FLAG"] == "削除済" ? userInfo.USER_CD : null);
                createSql.AppendLine(",DEL_DATE = :DEL_DATE");
                _params.Add("DEL_DATE", inputParam["DEL_FLAG"] == "削除済" ? DateTime.Now.ToString() : null);

                createSql.AppendLine(",DEL_FLAG = :DEL_FLAG");
                _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);

                createSql.AppendLine(" WHERE ");
                createSql.AppendLine(" MAKER_ID = :MAKER_ID ");
                _params.Add("MAKER_ID", inputParam["MAKER_ID"]);
            }

            sql = createSql.ToString();
            return true;
        }
        private bool GetSqlUpdateMUnitsize(ref string sql, ref Hashtable _params, ref string status)
        {
            UserInfo.ClsUser userInfo = null;
            Common.GetUserInfo(HttpContext.Current.Session, out userInfo);

            //新規か更新か判断
            Hashtable _para = new Hashtable();
            var createSql = new StringBuilder();
            createSql.AppendLine("select * FROM M_UNITSIZE WHERE UNITSIZE_ID = :UNITSIZE_ID");
            _para.Add("UNITSIZE_ID", inputParam["UNITSIZE_ID"]);
            DataTable dt = DbUtil.Select(createSql.ToString(), _para);

            createSql.Clear();

            if (dt.Rows.Count == 0)
            {
                //新規
                createSql.AppendLine("INSERT INTO M_UNITSIZE(");
                createSql.AppendLine("UNIT_NAME");
                createSql.AppendLine(",FACTOR");
                createSql.AppendLine(",REG_USER_CD");
                createSql.AppendLine(",REG_DATE");

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",DEL_USER_CD");
                    createSql.AppendLine(",DEL_DATE");
                    createSql.AppendLine(",DEL_FLAG");
                }
                createSql.AppendLine(") ");
                createSql.AppendLine("VALUES (");
                createSql.AppendLine(":UNIT_NAME");
                _params.Add("UNIT_NAME", inputParam["UNIT_NAME"]);
                createSql.AppendLine(",:FACTOR");
                _params.Add("FACTOR", inputParam["FACTOR"]);

                createSql.AppendLine(",:REG_USER_CD");
                _params.Add("REG_USER_CD", userInfo.USER_CD);
                createSql.AppendLine(",:REG_DATE");
                _params.Add("REG_DATE", DateTime.Now);

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",:DEL_USER_CD");
                    _params.Add("DEL_USER_CD", userInfo.USER_CD);
                    createSql.AppendLine(",:DEL_DATE");
                    _params.Add("DEL_DATE", DateTime.Now);
                    createSql.AppendLine(",:DEL_FLAG");
                    _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);
                }
                createSql.AppendLine(") ");
            }
            else
            {
                //更新
                createSql.AppendLine("UPDATE M_UNITSIZE ");
                createSql.AppendLine("SET UNIT_NAME = :UNIT_NAME");
                _params.Add("UNIT_NAME", inputParam["UNIT_NAME"]);
                createSql.AppendLine(",FACTOR = :FACTOR");
                _params.Add("FACTOR", inputParam["FACTOR"]);

                createSql.AppendLine(",UPD_USER_CD = :UPD_USER_CD");
                _params.Add("UPD_USER_CD", userInfo.USER_CD);

                createSql.AppendLine(",UPD_DATE = :UPD_DATE");
                _params.Add("UPD_DATE", DateTime.Now);

                createSql.AppendLine(",DEL_USER_CD = :DEL_USER_CD");
                _params.Add("DEL_USER_CD", inputParam["DEL_FLAG"] == "削除済" ? userInfo.USER_CD : null);
                createSql.AppendLine(",DEL_DATE = :DEL_DATE");
                _params.Add("DEL_DATE", inputParam["DEL_FLAG"] == "削除済" ? DateTime.Now.ToString() : null);

                createSql.AppendLine(",DEL_FLAG = :DEL_FLAG");
                _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);

                createSql.AppendLine(" WHERE ");
                createSql.AppendLine(" UNITSIZE_ID = :UNITSIZE_ID ");
                _params.Add("UNITSIZE_ID", inputParam["UNITSIZE_ID"]);
            }

            sql = createSql.ToString();
            return true;
        }
        private bool GetSqlUpdateMWorkflow(ref string sql, ref Hashtable _params, ref string status)
        {
            UserInfo.ClsUser userInfo = null;
            Common.GetUserInfo(HttpContext.Current.Session, out userInfo);

            //新規か更新か判断
            Hashtable _para = new Hashtable();
            var createSql = new StringBuilder();
            createSql.AppendLine("select * FROM M_WORKFLOW WHERE FLOW_ID = :FLOW_ID");
            _para.Add("FLOW_ID", inputParam["FLOW_ID"]);
            DataTable dt = DbUtil.Select(createSql.ToString(), _para);

            createSql.Clear();

            if (dt.Rows.Count == 0)
            {
                //新規
                createSql.AppendLine("INSERT INTO M_WORKFLOW(");
                createSql.AppendLine("FLOW_CD");
                createSql.AppendLine(",FLOW_TYPE_ID");
                createSql.AppendLine(",USAGE");
                createSql.AppendLine(",FLOW_HIERARCHY");
                createSql.AppendLine(",APPROVAL_TARGET1");
                createSql.AppendLine(",APPROVAL_USER_CD1");
                createSql.AppendLine(",SUBSITUTE_USER_CD1");
                createSql.AppendLine(",APPROVAL_TARGET2");
                createSql.AppendLine(",APPROVAL_USER_CD2");
                createSql.AppendLine(",SUBSITUTE_USER_CD2");
                createSql.AppendLine(",APPROVAL_TARGET3");
                createSql.AppendLine(",APPROVAL_USER_CD3");
                createSql.AppendLine(",SUBSITUTE_USER_CD3");
                createSql.AppendLine(",APPROVAL_TARGET4");
                createSql.AppendLine(",APPROVAL_USER_CD4");
                createSql.AppendLine(",SUBSITUTE_USER_CD4");
                createSql.AppendLine(",APPROVAL_TARGET5");
                createSql.AppendLine(",APPROVAL_USER_CD5");
                createSql.AppendLine(",SUBSITUTE_USER_CD5");
                createSql.AppendLine(",APPROVAL_TARGET6");
                createSql.AppendLine(",APPROVAL_USER_CD6");
                createSql.AppendLine(",SUBSITUTE_USER_CD6");
                createSql.AppendLine(",REG_USER_CD");
                createSql.AppendLine(",REG_DATE");

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",DEL_USER_CD");
                    createSql.AppendLine(",DEL_DATE");
                    createSql.AppendLine(",DEL_FLAG");
                }
                createSql.AppendLine(") ");
                createSql.AppendLine("VALUES (");
                createSql.AppendLine(":FLOW_CD");
                _params["FLOW_CD"] = getNewFlowcdSequence();
                createSql.AppendLine(",(SELECT KEY_VALUE FROM  M_COMMON WHERE  TABLE_NAME = 'M_FLOW_TYPE' and DISPLAY_VALUE = :FLOW_TYPE_ID) ");
                _params.Add("FLOW_TYPE_ID", inputParam["FLOW_TYPE_ID"]);
                createSql.AppendLine(",:USAGE");
                _params.Add("USAGE", inputParam["USAGE"]);
                createSql.AppendLine(",:FLOW_HIERARCHY");
                _params.Add("FLOW_HIERARCHY", inputParam["FLOW_HIERARCHY"]);
                createSql.AppendLine(",:APPROVAL_TARGET1");
                _params.Add("APPROVAL_TARGET1", inputParam["APPROVAL_TARGET1"]);
                createSql.AppendLine(",:APPROVAL_USER_CD1");
                _params.Add("APPROVAL_USER_CD1", inputParam["APPROVAL_USER_CD1"]);
                createSql.AppendLine(",:SUBSITUTE_USER_CD1");
                _params.Add("SUBSITUTE_USER_CD1", inputParam["SUBSITUTE_USER_CD1"]);
                createSql.AppendLine(",:APPROVAL_TARGET2");
                _params.Add("APPROVAL_TARGET2", inputParam["APPROVAL_TARGET2"]);
                createSql.AppendLine(",:APPROVAL_USER_CD2");
                _params.Add("APPROVAL_USER_CD2", inputParam["APPROVAL_USER_CD2"]);
                createSql.AppendLine(",:SUBSITUTE_USER_CD2");
                _params.Add("SUBSITUTE_USER_CD2", inputParam["SUBSITUTE_USER_CD2"]);
                createSql.AppendLine(",:APPROVAL_TARGET3");
                _params.Add("APPROVAL_TARGET3", inputParam["APPROVAL_TARGET3"]);
                createSql.AppendLine(",:APPROVAL_USER_CD3");
                _params.Add("APPROVAL_USER_CD3", inputParam["APPROVAL_USER_CD3"]);
                createSql.AppendLine(",:SUBSITUTE_USER_CD3");
                _params.Add("SUBSITUTE_USER_CD3", inputParam["SUBSITUTE_USER_CD3"]);
                createSql.AppendLine(",:APPROVAL_TARGET4");
                _params.Add("APPROVAL_TARGET4", inputParam["APPROVAL_TARGET4"]);
                createSql.AppendLine(",:APPROVAL_USER_CD4");
                _params.Add("APPROVAL_USER_CD4", inputParam["APPROVAL_USER_CD4"]);
                createSql.AppendLine(",:SUBSITUTE_USER_CD4");
                _params.Add("SUBSITUTE_USER_CD4", inputParam["SUBSITUTE_USER_CD4"]);
                createSql.AppendLine(",:APPROVAL_TARGET5");
                _params.Add("APPROVAL_TARGET5", inputParam["APPROVAL_TARGET5"]);
                createSql.AppendLine(",:APPROVAL_USER_CD5");
                _params.Add("APPROVAL_USER_CD5", inputParam["APPROVAL_USER_CD5"]);
                createSql.AppendLine(",:SUBSITUTE_USER_CD5");
                _params.Add("SUBSITUTE_USER_CD5", inputParam["SUBSITUTE_USER_CD5"]);
                createSql.AppendLine(",:APPROVAL_TARGET6");
                _params.Add("APPROVAL_TARGET6", inputParam["APPROVAL_TARGET6"]);
                createSql.AppendLine(",:APPROVAL_USER_CD6");
                _params.Add("APPROVAL_USER_CD6", inputParam["APPROVAL_USER_CD6"]);
                createSql.AppendLine(",:SUBSITUTE_USER_CD6");
                _params.Add("SUBSITUTE_USER_CD6", inputParam["SUBSITUTE_USER_CD6"]);

                createSql.AppendLine(",:REG_USER_CD");
                _params.Add("REG_USER_CD", userInfo.USER_CD);
                createSql.AppendLine(",:REG_DATE");
                _params.Add("REG_DATE", DateTime.Now);

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",:DEL_USER_CD");
                    _params.Add("DEL_USER_CD", userInfo.USER_CD);
                    createSql.AppendLine(",:DEL_DATE");
                    _params.Add("DEL_DATE", DateTime.Now);
                    createSql.AppendLine(",:DEL_FLAG");
                    _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);
                }
                createSql.AppendLine(") ");
            }
            else
            {
                //更新
                createSql.AppendLine("UPDATE M_WORKFLOW ");
                createSql.AppendLine("SET ");
                createSql.AppendLine("FLOW_TYPE_ID = (SELECT KEY_VALUE FROM  M_COMMON WHERE  TABLE_NAME = 'M_FLOW_TYPE' and DISPLAY_VALUE = :FLOW_TYPE_ID) ");
                _params.Add("FLOW_TYPE_ID", inputParam["FLOW_TYPE_ID"]);
                createSql.AppendLine(",USAGE = :USAGE");
                _params.Add("USAGE", inputParam["USAGE"]);
                createSql.AppendLine(",FLOW_HIERARCHY = :FLOW_HIERARCHY");
                _params.Add("FLOW_HIERARCHY", inputParam["FLOW_HIERARCHY"]);
                createSql.AppendLine(",APPROVAL_TARGET1 = :APPROVAL_TARGET1");
                _params.Add("APPROVAL_TARGET1", inputParam["APPROVAL_TARGET1"]);
                createSql.AppendLine(",APPROVAL_USER_CD1 = :APPROVAL_USER_CD1");
                _params.Add("APPROVAL_USER_CD1", inputParam["APPROVAL_USER_CD1"]);
                createSql.AppendLine(",SUBSITUTE_USER_CD1 = :SUBSITUTE_USER_CD1");
                _params.Add("SUBSITUTE_USER_CD1", inputParam["SUBSITUTE_USER_CD1"]);
                createSql.AppendLine(",APPROVAL_TARGET2 = :APPROVAL_TARGET2");
                _params.Add("APPROVAL_TARGET2", inputParam["APPROVAL_TARGET2"]);
                createSql.AppendLine(",APPROVAL_USER_CD2 = :APPROVAL_USER_CD2");
                _params.Add("APPROVAL_USER_CD2", inputParam["APPROVAL_USER_CD2"]);
                createSql.AppendLine(",SUBSITUTE_USER_CD2 = :SUBSITUTE_USER_CD2");
                _params.Add("SUBSITUTE_USER_CD2", inputParam["SUBSITUTE_USER_CD2"]);
                createSql.AppendLine(",APPROVAL_TARGET3 = :APPROVAL_TARGET3");
                _params.Add("APPROVAL_TARGET3", inputParam["APPROVAL_TARGET3"]);
                createSql.AppendLine(",APPROVAL_USER_CD3 = :APPROVAL_USER_CD3");
                _params.Add("APPROVAL_USER_CD3", inputParam["APPROVAL_USER_CD3"]);
                createSql.AppendLine(",SUBSITUTE_USER_CD3 = :SUBSITUTE_USER_CD3");
                _params.Add("SUBSITUTE_USER_CD3", inputParam["SUBSITUTE_USER_CD3"]);
                createSql.AppendLine(",APPROVAL_TARGET4 = :APPROVAL_TARGET4");
                _params.Add("APPROVAL_TARGET4", inputParam["APPROVAL_TARGET4"]);
                createSql.AppendLine(",APPROVAL_USER_CD4 = :APPROVAL_USER_CD4");
                _params.Add("APPROVAL_USER_CD4", inputParam["APPROVAL_USER_CD4"]);
                createSql.AppendLine(",SUBSITUTE_USER_CD4 = :SUBSITUTE_USER_CD4");
                _params.Add("SUBSITUTE_USER_CD4", inputParam["SUBSITUTE_USER_CD4"]);
                createSql.AppendLine(",APPROVAL_TARGET5 = :APPROVAL_TARGET5");
                _params.Add("APPROVAL_TARGET5", inputParam["APPROVAL_TARGET5"]);
                createSql.AppendLine(",APPROVAL_USER_CD5 = :APPROVAL_USER_CD5");
                _params.Add("APPROVAL_USER_CD5", inputParam["APPROVAL_USER_CD5"]);
                createSql.AppendLine(",SUBSITUTE_USER_CD5 = :SUBSITUTE_USER_CD5");
                _params.Add("SUBSITUTE_USER_CD5", inputParam["SUBSITUTE_USER_CD5"]);
                createSql.AppendLine(",APPROVAL_TARGET6 = :APPROVAL_TARGET6");
                _params.Add("APPROVAL_TARGET6", inputParam["APPROVAL_TARGET6"]);
                createSql.AppendLine(",APPROVAL_USER_CD6 = :APPROVAL_USER_CD6");
                _params.Add("APPROVAL_USER_CD6", inputParam["APPROVAL_USER_CD6"]);
                createSql.AppendLine(",SUBSITUTE_USER_CD6 = :SUBSITUTE_USER_CD6");
                _params.Add("SUBSITUTE_USER_CD6", inputParam["SUBSITUTE_USER_CD6"]);

                createSql.AppendLine(",UPD_USER_CD = :UPD_USER_CD");
                _params.Add("UPD_USER_CD", userInfo.USER_CD);

                createSql.AppendLine(",UPD_DATE = :UPD_DATE");
                _params.Add("UPD_DATE", DateTime.Now);

                createSql.AppendLine(",DEL_USER_CD = :DEL_USER_CD");
                _params.Add("DEL_USER_CD", inputParam["DEL_FLAG"] == "削除済" ? userInfo.USER_CD : null);
                createSql.AppendLine(",DEL_DATE = :DEL_DATE");
                _params.Add("DEL_DATE", inputParam["DEL_FLAG"] == "削除済" ? DateTime.Now.ToString() : null);

                createSql.AppendLine(",DEL_FLAG = :DEL_FLAG");
                _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);

                createSql.AppendLine(" WHERE ");
                createSql.AppendLine(" FLOW_CD = :FLOW_CD ");
                _params.Add("FLOW_CD", inputParam["FLOW_CD"]);
            }

            sql = createSql.ToString();
            return true;
        }

        /// <summary>
        /// 更新用SQLを取得します。
        /// </summary>
        /// <param name="sql">作成SQL文</param>
        private bool GetSqlUpdate(ref string sql, ref Hashtable _params, ref string status)
        {
            var tableInformation = (S_TAB_CONTROL)HttpContext.Current.Session[SessionConst.TableInformation];
            var tableColumnInformations = ((List<S_TAB_COL_CONTROL>)HttpContext.Current.Session[SessionConst.TableColumnInformations]).Where(x => !(IsSystemColumn(x, false))).ToList();
            var tableKeyColumnInfos = tableColumnInformations.Where(x => x.IS_KEY).ToList();

            var createSql = new StringBuilder();
            createSql.AppendLine("select ");
            createSql.AppendLine(" *");
            createSql.AppendLine("from ");
            createSql.AppendLine(" " + tableInformation.TABLE_NAME + " ");
            createSql.AppendLine("where ");
            foreach (var tableColumnInfo in tableKeyColumnInfos)
            {
                createSql.AppendLine(" " + tableColumnInfo.COLUMN_NAME + " = :" + tableColumnInfo.COLUMN_NAME);
                if (tableColumnInfo != tableKeyColumnInfos.Last()) createSql.AppendLine(" AND");
            }

            var keyParam = new Dictionary<string, string>();
            foreach (var tableColumnInfo in tableKeyColumnInfos) keyParam.Add(tableColumnInfo.COLUMN_NAME, inputParam[tableColumnInfo.COLUMN_NAME]);
            DataTable data = DbUtil.Select(createSql.ToString(), keyParam.ToHashtable());
            var passwordColumn = tableKeyColumnInfos.SingleOrDefault(x => x.IS_PASSWORD);

            _params = inputParam.ToHashtable();


            UserInfo.ClsUser userInfo = null;
            Common.GetUserInfo(HttpContext.Current.Session, out userInfo);
            _params.Add("UPD_USER", userInfo.USER_CD);

            if (!tableInformation.IS_INSERT && data.Rows.Count == 0) return false;
            if (!tableInformation.IS_UPDATE && data.Rows.Count != 0) return false;

            if (data.Rows.Count == 0)
            {
                status = statusInsert;
                createSql.Clear();

                if (passwordColumn != null)
                {
                    if (string.IsNullOrWhiteSpace(inputParam[passwordColumn.COLUMN_NAME]))
                    {
                        throw new Exception(string.Format(WarningMessages.Required, passwordColumn.COLUMN_DISPLAY_NAME));
                    }
                    else
                    {
                        inputParam[passwordColumn.COLUMN_NAME] = Common.GetSHA1Hash(inputParam[passwordColumn.COLUMN_NAME].ToString());
                    }
                }
                if (tableInformation.TABLE_NAME == "M_WORKFLOW")
                {
                    foreach (var tablecColumn in tableColumnInformations)
                    {
                        // フローコードを更新可能にする
                        if (tablecColumn.COLUMN_NAME == "FLOW_CD")
                        {
                            tablecColumn.IS_IDENTITY = false;
                        }
                    }
                    _params["FLOW_CD"] = getNewFlowcdSequence();
                    _params["FLOW_HIERARCHY"] = GetFlowHierarchy(inputParam);
                }

                createSql.AppendLine("insert into");
                createSql.AppendLine(" " + tableInformation.TABLE_NAME);
                createSql.AppendLine("(");
                createSql.AppendLine(" REG_USER_CD,");
                foreach (var tableColumnInfo in tableColumnInformations.Where(x => !x.IS_IDENTITY).ToList())
                {
                    //2018/08/15 TPE Add Start
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "USER_CD") continue;
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "USER_NAME") continue;
                    //2018/08/15 TPE Add End

                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "P_LOC_BASEID") continue; //2018/10/04 Rin Add

                    createSql.AppendLine(" " + tableColumnInfo.COLUMN_NAME);

                    //2018/08/15 TPE Add Start
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "SEL_FLAG") continue;
                    //2018/08/15 TPE Add End

                    if (tableColumnInfo != tableColumnInformations.Last()) createSql.Append(",");
                }
                createSql.AppendLine(")");
                createSql.AppendLine("values");
                createSql.AppendLine("(");
                createSql.AppendLine(" :UPD_USER,");
                foreach (var tableColumnInfo in tableColumnInformations.Where(x => !x.IS_IDENTITY).ToList())
                {
                    //2018/08/15 TPE Add Start
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "USER_CD") continue;
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "USER_NAME") continue;
                    //2018/08/15 TPE Add End

                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "P_LOC_BASEID") continue;  //2018/10/04 Rin Add

                    createSql.AppendLine(" :" + tableColumnInfo.COLUMN_NAME);

                    //2018/08/15 TPE Add Start
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "SEL_FLAG") continue;
                    //2018/08/15 TPE Add End

                    if (tableColumnInfo != tableColumnInformations.Last()) createSql.Append(",");
                }
                createSql.AppendLine(")");

                //2018/08/15 TPE Add Start
                if (tableInformation.TABLE_NAME == "M_LOCATION")
                {
                    createSql.AppendLine(";SELECT CAST(SCOPE_IDENTITY() AS INT)");
                }
                //2018/08/15 TPE Add End

                sql = createSql.ToString();

                return true;
            }
            else
            {
                status = statusUpdate;

                createSql.Clear();

                createSql.AppendLine("update");
                createSql.AppendLine(" " + tableInformation.TABLE_NAME + " ");
                createSql.AppendLine("set ");

                if (passwordColumn != null)
                {
                    if (!string.IsNullOrWhiteSpace(inputParam[passwordColumn.COLUMN_NAME]))
                    {
                        createSql.AppendLine(" " + passwordColumn.COLUMN_NAME + " = :" + passwordColumn.COLUMN_NAME + ",");
                        inputParam[passwordColumn.COLUMN_NAME] = Common.GetSHA1Hash(inputParam[passwordColumn.COLUMN_NAME].ToString());
                    }
                    else
                    {
                        inputParam.Remove(passwordColumn.COLUMN_NAME);
                    }
                }
                if (tableInformation.TABLE_NAME == "M_WORKFLOW")
                {
                    foreach (var tablecColumn in tableColumnInformations)
                    {
                        // フローコードを更新不可にする
                        if (tablecColumn.COLUMN_NAME == "FLOW_CD")
                        {
                            tablecColumn.IS_IDENTITY = true;
                        }
                    }
                    _params["FLOW_HIERARCHY"] = GetFlowHierarchy(inputParam);
                }

                createSql.AppendLine(" UPD_USER_CD = :UPD_USER,");
                foreach (var tableColumnInfo in tableColumnInformations.Where(x => !x.IS_IDENTITY && x.IS_PASSWORD == false).ToList())
                {
                    //2018/08/15 TPE Add Start
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "USER_CD") continue;
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "USER_NAME") continue;
                    //2018/08/15 TPE Add End
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "P_LOC_BASEID") continue;  //2018/10/04 Rin Add

                    createSql.AppendLine(" " + tableColumnInfo.COLUMN_NAME + " = :" + tableColumnInfo.COLUMN_NAME);

                    //2018/08/15 TPE Add Start
                    if (tableInformation.TABLE_NAME == "M_LOCATION" && tableColumnInfo.COLUMN_NAME == "SEL_FLAG") continue;
                    //2018/08/15 TPE Add End

                    if (tableColumnInfo != tableColumnInformations.Last()) createSql.Append(",");
                }
                createSql.AppendLine("where ");
                foreach (var tableColumnInfo in tableKeyColumnInfos)
                {
                    createSql.AppendLine(" " + tableColumnInfo.COLUMN_NAME + " = :" + tableColumnInfo.COLUMN_NAME);
                    if (tableColumnInfo != tableKeyColumnInfos.Last()) createSql.AppendLine(" AND");
                }

                sql = createSql.ToString();

                return true;
            }
        }

        /// <summary>
        /// 入力チェックを行います。
        /// </summary>
        /// <returns>true：正常 / false：異常</returns>
        private bool DataCheck(out string errMessage)
        {
            errMessage = "";

            var errorMessages = new List<string>();

            var tableColumnInformations = (List<S_TAB_COL_CONTROL>)HttpContext.Current.Session[SessionConst.TableColumnInformations];

            // 名称→コード変換を行う。
            var convertErrors = new List<string>();
            foreach (var tableColumnInfo in tableColumnInformations.Where(x => !(IsSystemColumn(x, false)) && x.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select))
            {
                if (!string.IsNullOrWhiteSpace(inputParam[tableColumnInfo.COLUMN_NAME].ToString()))
                {
                    if (!tableColumnInfo.ConvertData.ContainsValue(inputParam[tableColumnInfo.COLUMN_NAME]))
                    {
                        convertErrors.Add(string.Format(WarningMessages.CanNotConvert, tableColumnInfo.COLUMN_DISPLAY_NAME));
                        inputParam[tableColumnInfo.COLUMN_NAME] = "0";
                    }
                    else
                    {
                        inputParam[tableColumnInfo.COLUMN_NAME] = tableColumnInfo.ConvertData.Single(x => x.Value == inputParam[tableColumnInfo.COLUMN_NAME]).Key;
                    }
                }

            }

            errorMessages = ValidateInputValue(tableColumnInformations, inputParam, null).ToList();//2019/02/18 TPE.Sugimoto Upd
            errorMessages.AddRange(convertErrors);

            if (errorMessages.Count > 0)
            {
                errMessage = string.Join(SystemConst.HtmlLinefeed, errorMessages);
                return false;
            }

            return true;
        }

        //2018/08/15 TPE Add Start
        /// <summary>
        /// M_CHIEFをDelteするSQL
        /// </summary>
        /// <param name="sql"></param>
        /// <param name="_params"></param>
        /// <param name="locid"></param>
        /// <returns></returns>
        private static void GetSqlDelInsMChief(DbContext ctx, int loc_id, string user_cd, string reg_user_cd)
        {
            var parameter = new Hashtable();
            var createSql = new StringBuilder();
            createSql.AppendLine("delete");
            createSql.AppendLine(" M_CHIEF");
            createSql.AppendLine(" where");
            createSql.AppendLine(" LOC_ID = :LOC_ID");

            parameter.Add("LOC_ID", loc_id);

            var data = DbUtil.ExecuteUpdate(ctx, createSql.ToString(), parameter);

            //M_CHIEFへInsertする
            //NIDがNULLだったら、Insertしない
            //Deleteだけさせる

            //2018/08/31 TPE Add Start (IF文のみ)
            if (user_cd != "")
            {
                string[] arr = user_cd.Split(';');
                int i = 1;
                int arrcount = arr.Length;

                createSql.Clear();
                createSql.AppendLine("insert into");
                createSql.AppendLine(" M_CHIEF");
                createSql.AppendLine("(");
                createSql.AppendLine("LOC_ID,");
                createSql.AppendLine("USER_CD,");
                createSql.AppendLine("REG_USER_CD");
                createSql.AppendLine(")");
                createSql.AppendLine("values");
                foreach (string usrcd in arr)
                {
                    createSql.AppendLine("(");
                    createSql.AppendLine(":LOC_ID,");
                    createSql.AppendLine(":USER_CD_" + i + ",");
                    createSql.AppendLine(":REG_USER_CD");
                    createSql.AppendLine(")");
                    if (i != arrcount)
                    {
                        createSql.AppendLine(",");
                    }
                    parameter.Add("USER_CD_" + i, usrcd);
                    i++;

                }
                parameter.Add("REG_USER_CD", reg_user_cd);

                var data2 = DbUtil.ExecuteUpdate(ctx, createSql.ToString(), parameter);
            }
        }


        /// <summary>
        /// 入力チェックを行います。(M_GROUP用)
        /// </summary>
        /// <returns>true：正常 / false：異常</returns>
        private bool DataCheckGroup(out string errMessage)
        {
            errMessage = "";
            var errorMessages = new List<string>();

            DataCheckBase _DataCheck = new DataCheckBase();

            //状態
            errMessage = _DataCheck.CheckDelFlagGroup(inputParam["DEL_FLAG"], inputParam["GROUP_CD"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //部署コード
            errMessage = _DataCheck.CheckGroupCD(inputParam["GROUP_CD"], inputParam["GROUP_ID"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //部署名
            errMessage = _DataCheck.CheckGroupName(inputParam["GROUP_NAME"], false);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //上位部署名
            errMessage = _DataCheck.CheckPGroupName(inputParam["P_GROUP_CD"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //バッチ更新対象外
            errMessage = _DataCheck.CheckBatchFlag(inputParam["BATCH_FLAG"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            if (errorMessages.Count() > 0)
            {
                errMessage = string.Join(SystemConst.HtmlLinefeed, errorMessages);
                return false;
            }

            return true;
        }
        //2019/02/18 TPE.Sugimoto Add Sta
        /// <summary>
        /// 入力チェックを行います。(M_MAKER用)
        /// </summary>
        /// <returns>true：正常 / false：異常</returns>
        private bool DataCheckMaker(out string errMessage)
        {
            errMessage = "";
            var errorMessages = new List<string>();

            DataCheckBase _DataCheck = new DataCheckBase();

            //状態
            errMessage = _DataCheck.CheckDelFlagMaker(inputParam["DEL_FLAG"], inputParam["MAKER_ID"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //メーカー名
            errMessage = _DataCheck.CheckMakerName(inputParam["MAKER_NAME"], false, inputParam["MAKER_ID"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //URL
            errMessage = _DataCheck.CheckMakerUrl(inputParam["MAKER_URL"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //電話番号
            errMessage = _DataCheck.CheckMakerTel(inputParam["MAKER_TEL"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //メールアドレス1
            errMessage = _DataCheck.CheckMakerEmail1(inputParam["MAKER_EMAIL1"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //メールアドレス2
            errMessage = _DataCheck.CheckMakerEmail2(inputParam["MAKER_EMAIL2"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //メールアドレス3
            errMessage = _DataCheck.CheckMakerEmail3(inputParam["MAKER_EMAIL3"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //住所
            errMessage = _DataCheck.CheckMakerAddress(inputParam["MAKER_ADDRESS"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            if (errorMessages.Count() > 0)
            {
                errMessage = string.Join(SystemConst.HtmlLinefeed, errorMessages);
                return false;
            }

            return true;
        }
        /// <summary>
        /// 入力チェックを行います。(M_UNITSIZE用)
        /// </summary>
        /// <returns>true：正常 / false：異常</returns>
        private bool DataCheckUnitsize(out string errMessage)
        {
            errMessage = "";
            var errorMessages = new List<string>();

            DataCheckBase _DataCheck = new DataCheckBase();

            //状態
            errMessage = _DataCheck.CheckDelFlagUnitsize(inputParam["DEL_FLAG"], inputParam["UNITSIZE_ID"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //単位名称
            errMessage = _DataCheck.CheckUnitName(inputParam["UNIT_NAME"], false, inputParam["UNITSIZE_ID"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //変換係数
            if (inputParam["FACTOR"] != "")
            {
                errMessage = _DataCheck.CheckFactor(inputParam["FACTOR"]);
                if (errMessage != "")
                {
                    errorMessages.Add(errMessage);
                }
                errMessage = "";
            }

            if (errorMessages.Count() > 0)
            {
                errMessage = string.Join(SystemConst.HtmlLinefeed, errorMessages);
                return false;
            }

            return true;
        }
        /// <summary>
        /// 入力チェックを行います。(M_WORKFLOW用)
        /// </summary>
        /// <returns>true：正常 / false：異常</returns>
        private bool DataCheckWorkflow(out string errMessage)
        {
            errMessage = "";
            var errorMessages = new List<string>();

            DataCheckBase _DataCheck = new DataCheckBase();

            //状態
            errMessage = _DataCheck.CheckDelFlagWorkflow(inputParam["DEL_FLAG"], inputParam["FLOW_ID"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //フローコード
            errMessage = _DataCheck.CheckFlowCD(inputParam["FLOW_CD"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //フロー種別
            errMessage = _DataCheck.CheckFlowTypeID(inputParam["FLOW_TYPE_ID"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //フロー名
            errMessage = _DataCheck.CheckUsage(inputParam["USAGE"], false, inputParam["FLOW_ID"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //フロー階層
            errMessage = _DataCheck.CheckFlowHierarchy(inputParam["FLOW_HIERARCHY"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認階層1
            errMessage = _DataCheck.CheckApprovalTarget1(inputParam["APPROVAL_TARGET1"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認者1
            errMessage = _DataCheck.CheckApprovalUserCD1(inputParam["APPROVAL_USER_CD1"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //代行者1
            errMessage = _DataCheck.CheckSubsituteUserCD1(inputParam["SUBSITUTE_USER_CD1"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認階層2
            errMessage = _DataCheck.CheckApprovalTarget2(inputParam["APPROVAL_TARGET2"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認者2
            errMessage = _DataCheck.CheckApprovalUserCD2(inputParam["APPROVAL_USER_CD2"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //代行者2
            errMessage = _DataCheck.CheckSubsituteUserCD2(inputParam["SUBSITUTE_USER_CD2"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認階層3
            errMessage = _DataCheck.CheckApprovalTarget3(inputParam["APPROVAL_TARGET3"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認者3
            errMessage = _DataCheck.CheckApprovalUserCD3(inputParam["APPROVAL_USER_CD3"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //代行者3
            errMessage = _DataCheck.CheckSubsituteUserCD3(inputParam["SUBSITUTE_USER_CD3"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認階層4
            errMessage = _DataCheck.CheckApprovalTarget4(inputParam["APPROVAL_TARGET4"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認者4
            errMessage = _DataCheck.CheckApprovalUserCD4(inputParam["APPROVAL_USER_CD4"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //代行者4
            errMessage = _DataCheck.CheckSubsituteUserCD4(inputParam["SUBSITUTE_USER_CD4"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認階層5
            errMessage = _DataCheck.CheckApprovalTarget5(inputParam["APPROVAL_TARGET5"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認者5
            errMessage = _DataCheck.CheckApprovalUserCD5(inputParam["APPROVAL_USER_CD5"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //代行者5
            errMessage = _DataCheck.CheckSubsituteUserCD5(inputParam["SUBSITUTE_USER_CD5"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認階層6
            errMessage = _DataCheck.CheckApprovalTarget6(inputParam["APPROVAL_TARGET6"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //承認者6
            errMessage = _DataCheck.CheckApprovalUserCD6(inputParam["APPROVAL_USER_CD6"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //代行者6
            errMessage = _DataCheck.CheckSubsituteUserCD6(inputParam["SUBSITUTE_USER_CD6"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            if (errorMessages.Count() > 0)
            {
                errMessage = string.Join(SystemConst.HtmlLinefeed, errorMessages);
                return false;
            }

            return true;
        }

        /// <summary>
        /// 入力チェックを行います。(M_LOCATION用)
        /// </summary>
        /// <returns>true：正常 / false：異常</returns>
        private bool DataCheckLocation(out string errMessage)
        {
            errMessage = "";

            var errorMessages = new List<string>();

            var tableColumnInformations = (List<S_TAB_COL_CONTROL>)HttpContext.Current.Session[SessionConst.TableColumnInformations];

            string plocname = string.Empty;  //2018/10/03 Rin Add
            string username = string.Empty;  //2018/10/03 Rin Add
            string plocid = string.Empty;  //2018/10/04 Rin Add

            // 名称→コード変換を行う。
            var convertErrors = new List<string>();
            foreach (var tableColumnInfo in tableColumnInformations.Where(x => !(IsSystemColumn(x, false)) && x.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Select))
            {
                //2018/10/01 Rin Add Start
                //上位保管場所についてはここでチェックをさせない
                if (tableColumnInfo.TABLE_NAME == "M_LOCATION" && (tableColumnInfo.COLUMN_NAME == "P_LOC_ID" || tableColumnInfo.COLUMN_NAME == "P_LOC_BASEID"))
                {
                    continue;
                }
                //2018/10/01 Rin Add End

                if (!string.IsNullOrWhiteSpace(inputParam[tableColumnInfo.COLUMN_NAME].ToString()))
                {
                    if (!tableColumnInfo.ConvertData.ContainsValue(inputParam[tableColumnInfo.COLUMN_NAME]))
                    {
                        convertErrors.Add(string.Format(WarningMessages.CanNotConvert, tableColumnInfo.COLUMN_DISPLAY_NAME));
                        inputParam[tableColumnInfo.COLUMN_NAME] = "0";
                    }
                    else
                    {
                        inputParam[tableColumnInfo.COLUMN_NAME] = tableColumnInfo.ConvertData.Single(x => x.Value == inputParam[tableColumnInfo.COLUMN_NAME]).Key;
                    }
                }

            }

            foreach (var tableColumnInfo in tableColumnInformations)
            {
                //2018/10/03 Rin Add Start
                //上位保管場所IDの確認
                if (tableColumnInfo.COLUMN_NAME == "P_LOC_BASEID")
                {

                    if (!string.IsNullOrWhiteSpace(inputParam[tableColumnInfo.COLUMN_NAME].ToString()))
                    {
                        //まずはIDが存在するか確認
                        if (!tableColumnInfo.ConvertData.ContainsKey(inputParam[tableColumnInfo.COLUMN_NAME]))
                        {
                            convertErrors.Add(string.Format(WarningMessages.NotFound, tableColumnInfo.COLUMN_DISPLAY_NAME));
                            inputParam[tableColumnInfo.COLUMN_NAME] = "0";
                        }
                        else
                        {
                            //上位保管場所名と比較させるための変数に格納
                            plocname = tableColumnInfo.ConvertData[inputParam[tableColumnInfo.COLUMN_NAME]];
                            plocid = inputParam[tableColumnInfo.COLUMN_NAME].ToString();
                        }

                    }

                }
                //上位保管場所名の確認
                if (tableColumnInfo.COLUMN_NAME == "P_LOC_ID")
                {

                    if (plocname != inputParam[tableColumnInfo.COLUMN_NAME])
                    {
                        convertErrors.Add(string.Format("上位保管場所のIDと名前が一致しません。", tableColumnInfo.COLUMN_DISPLAY_NAME));
                    }
                    else
                    {
                        //P_LOC_IDにIDを入れる
                        inputParam[tableColumnInfo.COLUMN_NAME] = plocid;
                    }
                }
                //2018/10/03 Rin Add End



                ////USER_CDのチェック
                if (tableColumnInfo.COLUMN_NAME == "USER_CD")
                {
                    var usercd = inputParam[tableColumnInfo.COLUMN_NAME];
                    //値が入力されている
                    if (usercd != "")
                    {
                        string[] arr = usercd.Split(';');
                        foreach (string usrcd in arr)
                        {
                            var userInfo = GetUserList().SingleOrDefault(x => x.Id == usrcd);
                            if (userInfo == null)
                            {
                                convertErrors.Add(string.Format(WarningMessages.NotFound, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                inputParam[tableColumnInfo.COLUMN_NAME] = "0";
                            }
                            //2018/10/03 Rin Add Start
                            else
                            {
                                //化学物資取扱責任者の名前と比較する用
                                username = username + userInfo.Name + ";";
                            }
                            //2018/10/03 Rin Add End
                        }
                        username = username.Trim(';');  //2018/10/03 Rin Add
                    }
                }

                //2018/10/03 Rin Add Start
                if (tableColumnInfo.COLUMN_NAME == "USER_NAME")
                {
                    if (username != inputParam[tableColumnInfo.COLUMN_NAME])
                    {
                        convertErrors.Add(string.Format("化学物質取扱責任者のNIDと氏名が一致しません。", tableColumnInfo.COLUMN_DISPLAY_NAME));
                    }
                }

                //2018/09/14 TPE Add Start
                //SEL_FLGのチェック
                if (tableColumnInfo.COLUMN_NAME == "SEL_FLAG")
                {
                    var selflg = inputParam[tableColumnInfo.COLUMN_NAME];
                    //値が入力されていない
                    if (selflg == "")
                    {
                        convertErrors.Add(string.Format(WarningMessages.Required, tableColumnInfo.COLUMN_DISPLAY_NAME));
                        //inputParam[tableColumnInfo.COLUMN_NAME] = "0";

                    }
                }
                //2018/09/14 TPE Add End
            }

            errorMessages = ValidateInputValue(tableColumnInformations, inputParam, null).ToList();//2019/02/18 TPE.Sugimoto Upd
            errorMessages.AddRange(convertErrors);

            if (errorMessages.Count > 0)
            {
                errMessage = string.Join(SystemConst.HtmlLinefeed, errorMessages);
                return false;
            }

            return true;
        }


        public void LocationMasterSearch(M_MASTER master, S_TAB_CONTROL tableInformation, List<S_TAB_COL_CONTROL> tableColumnInformations)
        {
            List<M_LOCATION> SearchResult;
            string Plocid = master.mLocation.P_LOC_ID.ToString() != null ? master.mLocation.P_LOC_ID.ToString() : "";
            string Chemchief = master.mLocation.USER_CD != null ? master.mLocation.USER_CD : "";
            string locName = master.mLocation.LOC_NAME != null ? master.mLocation.LOC_NAME.Trim() : "";
            string plocName = master.mLocation.P_LOC_NAME != null ? master.mLocation.P_LOC_NAME : "";
            int classID = (int)master.mLocation.CLASS_ID;
            string selectability = master.mLocation.SELECTION_FLAG != null ? master.mLocation.SELECTION_FLAG : "";

            if (master.mLocation.DEL_FLAG == 1)
            {
                master.mLocation.DEL_FLG = true;
            }
            else
            {
                master.mLocation.DEL_FLG = false;
            }
            SearchResult = SearchLocationMaster(tableInformation, tableColumnInformations, classID, locName, plocName, Chemchief, Plocid, selectability, master.mLocation.DEL_FLAG.ToString()); //
            SearchResult = ConvertLocationMaster(tableColumnInformations, SearchResult);
            // if data exists set in session and enable the search parameter
            if (SearchResult.Count >= 0)
            {
                HttpContext.Current.Session[SessionConst.WebGridCount] = SearchResult.Count;
                HttpContext.Current.Session[SessionConst.WebGrid] = SearchResult;
                searchEnabled = 1;
            }
        }

        public void GroupMasterSearch(M_MASTER master, S_TAB_CONTROL tableInformation, List<S_TAB_COL_CONTROL> tableColumnInformations)
        {
            List<M_GROUP> SearchResult;
            SearchResult = SearchGroupMaster(tableInformation, tableColumnInformations, master.mGroup.GROUP_CD, master.mGroup.GROUP_NAME, master.mGroup.P_GROUP_CD, master.mGroup.Status, master.mGroup.BATCH_FLAG);
            SearchResult = ConvertGroupMaster(tableColumnInformations, SearchResult);
            // if data exists set in session and enable the search parameter
            if (SearchResult.Count >= 0)
            {
                HttpContext.Current.Session[SessionConst.WebGridCount] = SearchResult.Count;
                HttpContext.Current.Session[SessionConst.WebGrid] = SearchResult;
                searchEnabled = 1;
            }
        }

        public void WorkFlowMasterSearch(M_MASTER master, S_TAB_CONTROL tableInformation, List<S_TAB_COL_CONTROL> tableColumnInformations)
        {
            List<M_WORKFLOW> SearchResult;
            master.mWorkFlow.FLOW_CD = master.mWorkFlow.FLOW_CD != null ? master.mWorkFlow.FLOW_CD : "";
            //master.mWorkFlow.FLOW_ID = master.mWorkFlow.FLOW_ID != null ? master.mWorkFlow.FLOW_ID : "";
            master.mWorkFlow.FLOW_NAME = master.mWorkFlow.FLOW_NAME != null ? master.mWorkFlow.FLOW_NAME : "";
            master.mWorkFlow.FLOW_TYPE_ID = master.mWorkFlow.FLOW_TYPE_ID != null ? master.mWorkFlow.FLOW_TYPE_ID : "";
            SearchResult = SearchWorkflowMaster(tableInformation, tableColumnInformations, master.mWorkFlow.FLOW_CD, master.mWorkFlow.FLOW_ID, master.mWorkFlow.FLOW_TYPE_ID, master.mWorkFlow.FLOW_NAME, master.mWorkFlow.DEL_FLAG.ToString());
            SearchResult = ConvertWorkflowMaster(tableColumnInformations, SearchResult);
            // if data exists set in session and enable the search parameter
            if (SearchResult.Count >= 0)
            {
                HttpContext.Current.Session[SessionConst.WebGridCount] = SearchResult.Count;
                HttpContext.Current.Session[SessionConst.WebGrid] = SearchResult;
                searchEnabled = 1;
            }
        }

        public void MakerMasterSearch(M_MASTER master, S_TAB_CONTROL tableInformation, List<S_TAB_COL_CONTROL> tableColumnInformations)
        {
            List<M_MAKER> SearchResult;
            string del_Flag = (master.mMaker.DEL_FLAG).ToString();
            SearchResult = SearchMakerMaster(master, tableInformation, tableColumnInformations, del_Flag);
            SearchResult = ConvertMakerMaster(tableColumnInformations, SearchResult);
            //    SearchResult = ConvertGroupMaster(tableColumnInformations, SearchResult);
            // if data exists set in session and enable the search parameter
            if (SearchResult.Count >= 0)
            {
                HttpContext.Current.Session[SessionConst.WebGridCount] = SearchResult.Count;
                HttpContext.Current.Session[SessionConst.WebGrid] = SearchResult;
                searchEnabled = 1;
            }
        }

        public void UnitMasterSearch(M_MASTER master, S_TAB_CONTROL tableInformation, List<S_TAB_COL_CONTROL> tableColumnInformations)
        {
            List<M_UNITSIZE> SearchResult;
            string del_Flag = (master.mUnitSize.DEL_FLAG).ToString();
            SearchResult = SearchUnitsizeMaster(master, tableInformation, tableColumnInformations, del_Flag);
            SearchResult = ConvertUnitMaster(tableColumnInformations, SearchResult);
            //    SearchResult = ConvertGroupMaster(tableColumnInformations, SearchResult);
            // if data exists set in session and enable the search parameter
            if (SearchResult.Count >= 0)
            {
                HttpContext.Current.Session[SessionConst.WebGridCount] = SearchResult.Count;
                HttpContext.Current.Session[SessionConst.WebGrid] = SearchResult;
                searchEnabled = 1;
            }
        }

        public void StatusMasterSearch(M_MASTER master, S_TAB_CONTROL tableInformation, List<S_TAB_COL_CONTROL> tableColumnInformations)
        {
            List<M_STATUS> SearchResult;
            //string del_Flag = ;
            SearchResult = SearchStatusMaster(tableInformation, tableColumnInformations, master, master.mStatus.DEL_FLAG);
            SearchResult = ConvertStatusMaster(tableColumnInformations, SearchResult);
            // if data exists set in session and enable the search parameter
            if (SearchResult.Count >= 0)
            {
                HttpContext.Current.Session[SessionConst.WebGridCount] = SearchResult.Count;
                HttpContext.Current.Session[SessionConst.WebGrid] = SearchResult;
                searchEnabled = 1;
            }
        }

        public void ActionMasterSearch(M_MASTER master, S_TAB_CONTROL tableInformation, List<S_TAB_COL_CONTROL> tableColumnInformations)
        {
            List<M_ACTION> SearchResult;
            //string del_Flag = ;
            SearchResult = SearchActionMaster(tableInformation, tableColumnInformations, master, master.mActionType.DEL_FLAG);
            SearchResult = ConvertActionMaster(tableColumnInformations, SearchResult);
            // if data exists set in session and enable the search parameter
            if (SearchResult.Count >= 0)
            {
                HttpContext.Current.Session[SessionConst.WebGridCount] = SearchResult.Count;
                HttpContext.Current.Session[SessionConst.WebGrid] = SearchResult;
                searchEnabled = 1;
            }
        }

        // M_Regulation Master
        public List<M_REGULATION> RegulationMasterSearch(M_MASTER master, S_TAB_CONTROL tableInformation, List<S_TAB_COL_CONTROL> tableColumnInformations)
        {
            List<M_REGULATION> SearchResult;
            SearchResult = SearchRegulationMasterRecord(tableInformation, tableColumnInformations, master);
            SearchResult = ConvertRegulationMaster(tableColumnInformations, SearchResult);

            if (SearchResult.Count >= 0)
            {
                HttpContext.Current.Session[SessionConst.WebGridCount] = SearchResult.Count;
                HttpContext.Current.Session[SessionConst.WebGrid] = SearchResult;
                searchEnabled = 1;
            }

            return SearchResult;
        }

        // M_Common Master
        public List<M_COMMON> CommonMasterSearch(M_MASTER master, S_TAB_CONTROL tableInformation, List<S_TAB_COL_CONTROL> tableColumnInformations)
        {
            List<M_COMMON> SearchResult;
            //SearchCommonMaster(tableInformation, tableColumnInformations, DEL_FLAG.Undelete);

            SearchResult = SearchCommonMasterRecord(tableInformation, tableColumnInformations, DEL_FLAG.Undelete, master);
            SearchResult = ConvertCommonMaster(tableColumnInformations, SearchResult);

            if (SearchResult.Count >= 0)
            {
                HttpContext.Current.Session[SessionConst.WebGridCount] = SearchResult.Count;
                HttpContext.Current.Session[SessionConst.WebGrid] = SearchResult;
                searchEnabled = 1;
            }

            return SearchResult;
        }



        public void CheckCasData(ref List<string> errorlist, string CAS, string MAX, string MIN, string SHAPESELECTION, string TERMSSELECTION)
        {
            List<CasRegModel> casregmodel = new List<CasRegModel>();
            if (HttpContext.Current.Session[SessionConst.SessionCas] != null)
            {
                casregmodel = (List<CasRegModel>)HttpContext.Current.Session[SessionConst.SessionCas];
            }
            decimal tempmax = 0;
            decimal tempmin = 0;

            if (CAS != "")
            {
                var matchCollection = System.Text.RegularExpressions.Regex.Matches(CAS, @"[a-zA-Z0-9!-/:-@\[-`{-~]+$");
                if (!string.IsNullOrWhiteSpace(CAS) && (matchCollection.Count != 1 || !(CAS.Length == matchCollection[0].Length)))
                {
                    errorlist.Add(string.Format(WarningMessages.NotHalfwidth, "CAS#"));
                }
            }

            //0～100の確認
            if (!string.IsNullOrWhiteSpace(MAX))
            {
                var decmax = Convert.ToDecimal(MAX);
                if (decmax < Convert.ToDecimal(0) || Convert.ToDecimal(100) < decmax)
                {
                    errorlist.Add(string.Format(WarningMessages.ThresholdValueRangeOver, "上限", "0", "100"));
                }
                else
                {
                    //cas2.maximum = decmax;
                    tempmax = decmax;
                }
            }
            if (!string.IsNullOrWhiteSpace(MIN))
            {
                var decmin = Convert.ToDecimal(MIN);
                if (decmin < Convert.ToDecimal(0) || Convert.ToDecimal(100) < decmin)
                {
                    errorlist.Add(string.Format(WarningMessages.ThresholdValueRangeOver, "下限", "0", "100"));
                }
                else
                {
                    //cas2.minimum = decmin;
                    tempmin = decmin;
                }
            }
            if (!string.IsNullOrWhiteSpace(MAX) && !string.IsNullOrWhiteSpace(MIN))
            {
                //上限>=下限の確認
                if (tempmax < tempmin)
                {
                    errorlist.Add(string.Format(WarningMessages.ValueIsLarge, "下限", "上限"));
                }
            }
            foreach (var tempcas in casregmodel)
            {
                if (tempcas.casno == CAS)
                {
                    if ((tempcas.maximum ?? 0) == tempmax)
                    {
                        if ((tempcas.minimum ?? 0) == tempmin)
                        {
                            if (tempcas.shape == SHAPESELECTION)
                            {
                                if (tempcas.terms == TERMSSELECTION)
                                {
                                    errorlist.Add(string.Format("既にリストへ追加されています。同一の情報は追加が出来ません。"));
                                }
                            }
                        }
                    }
                }
            }
        }

        public string searchEntity(string CAS)
        {
            string EntityReturnStatus = null;

            var parameter = new DynamicParameters();
            parameter.Add("@casno", CAS);

            var sql = new System.Text.StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" CHEM_CD,CHEM_NAME ");
            sql.AppendLine("from");
            sql.AppendLine(" M_CHEM ");
            sql.AppendLine("where");
            sql.AppendLine(" CAS_NO = @casno");

            var records = DbUtil.Select<M_CHEM>(sql.ToString(), parameter);

            //context.Close();

            if (records.Count > 0)
            {
                string message = "化学物質マスタに";
                for (int i = 0; i < records.Count; i++)
                {
                    message += "化学物質コード：" + records[i].CHEM_CD + ",化学物質名：" + records[i].CHEM_NAME;
                }
                message += "で登録されています。リストに追加しますか？";
                EntityReturnStatus = message + "," + "SearchExecuteButton";
            }
            else
            {
                EntityReturnStatus = "化学物質マスタには未登録です。リストに追加しますか？" + "," + "SearchExecuteButton";
            }
            return EntityReturnStatus;

        }



        /// <summary>
        /// 親カテゴリーに自身のIDが使用されているか。
        /// </summary>
        /// <param name="dataList">親子関係テーブルのデータ。</param>
        /// <param name="id">検索の起点となる親ID。</param>
        /// <param name="id">検索する自身のID。</param>
        /// <returns>使用されている場合はtrue、使用されていない場合はfalse。</returns>
        public bool IsSelfUsed(IEnumerable<parentChildData> dataList, string parentId, string selfId)
        {
            if (parentId == selfId) return true;
            var data = dataList.SingleOrDefault(x => x.Id == parentId);
            if (data == null) return false;
            if (string.IsNullOrEmpty(data.ParentId)) return false;
            return IsSelfUsed(dataList, data.ParentId, selfId);
        }


        /// <summary>
        /// 親子関係のあるテーブルのデータを取得します。
        /// </summary>
        /// <param name="tableName">テーブル名。</param>
        /// <param name="idColumnName">ID項目名。</param>
        /// <param name="classColumnName">クラス項目名。</param>
        /// <param name="parentIdColumnName">親ID項目名。</param>
        /// <returns></returns>
        public IEnumerable<parentChildData> GetParentChildTable(string tableName, string idColumnName, string classColumnName, string parentIdColumnName)
        {
            DbContext context = null;
            context = DbUtil.Open(false);

            var sql = new StringBuilder();
            sql.AppendLine("select");
            sql.AppendLine(" " + idColumnName + ",");
            if (!string.IsNullOrEmpty(classColumnName)) sql.AppendLine(" " + classColumnName + ",");
            sql.AppendLine(" " + parentIdColumnName);
            sql.AppendLine("from");
            sql.AppendLine(" " + tableName);
            sql.AppendLine("where");
            sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

            var records = DbUtil.Select(context, sql.ToString());

            foreach (DataRow r in records.Rows)
            {
                var d = new parentChildData();
                d.Id = r[idColumnName].ToString();
                if (!string.IsNullOrEmpty(classColumnName)) d.Class = r[classColumnName].ToString();
                d.ParentId = r[parentIdColumnName].ToString();
                yield return d;
            }

            context.Close();
        }

        public class parentChildData
        {
            /// <summary>
            /// ID
            /// </summary>
            public string Id { get; set; }

            /// <summary>
            /// 分類
            /// </summary>
            public string Class { get; set; }

            /// <summary>
            /// 親ID
            /// </summary>
            public string ParentId { get; set; }
        }

        /// <summary>
        /// 再帰処理で親カテゴリーを取得します。
        /// </summary>
        /// <param name="dataList">親子関係テーブルのデータ。</param>
        /// <param name="id">検索するID。</param>
        /// <returns>親カテゴリーを取得します。</returns>
        public parentChildData GetParentData(IEnumerable<parentChildData> dataList, string id)
        {
            var data = dataList.SingleOrDefault(x => x.Id == id);
            if (data == null) return data;
            if (string.IsNullOrEmpty(data.ParentId)) return data;
            return GetParentData(dataList, data.ParentId);
        }
    }
}