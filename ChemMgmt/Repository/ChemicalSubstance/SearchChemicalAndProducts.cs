﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.Extensions;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using Dapper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.ModelBinding;
using ZyCoaG.BaseCommon;
using ZyCoaG.Classes.util;
using ZyCoaG.Extensions;
using ZyCoaG.Nikon;
using ZyCoaG.util;
using ZyCoaG.Util;



namespace ChemMgmt.Repository
{
    public class SearchChemicalAndProducts : NikonSearchPageBase
    {

        CsmsCommonInfo data = new CsmsCommonInfo();
        /// <summary>
        /// 検索結果の表示件数を取得します。
        /// </summary>
        protected override int GetMaxSearchResultCount() => new SystemParameters().MaxChemResultCount;
        public ChemDataInfo dataInfo;
       
        public List<CsmsCommonInfo> searchEntity(ChemSearchModel objChem, bool IsSearchButton, bool isListOutputProcess)
        {            
            return GetSearchResult(objChem, isListOutputProcess);
        }

       
        public List<int> GetSearchResultCount(ChemSearchModel condition)
        {
            SearchConditionCsv = string.Empty;
            using (var chemDataInfo = new ChemDataInfo())
            {
                condition.CHEM_NAME = condition.CHEM_NAME.UnityWidth();
                condition.IsOnlyProduct = true;
                var searchResultCount = chemDataInfo.GetSearchResultCount(condition).ToList();
                if (searchResultCount.Count() > 0)
                {
                    ViewSearchResultCount(searchResultCount[0]);
                    SearchConditionCsv = chemDataInfo.Csv;
                    HttpContext.Current.Session["SearchConditions"] = SearchConditionCsv;
                    HttpContext.Current.Session["SearchCount"] = Convert.ToInt32(searchResultCount[0]);
                    return searchResultCount;

                }
                else
                {
                    ViewSearchResultCount(0);
                    HttpContext.Current.Session["SearchCount"] = Convert.ToInt32(searchResultCount[0]);
                }
            }
            return new List<int>().AsQueryable().ToList();
        }

        public List<CsmsCommonInfo> GetSearchResult(ChemSearchModel condition, bool isListOutputProcess)
        {
            SearchConditionCsv = string.Empty;            
            using (var chemDataInfo = new ChemDataInfo())
            {
                condition.CHEM_NAME = condition.CHEM_NAME.UnityWidth();
                condition.IsOnlyProduct = true;
                var searchResultCount = chemDataInfo.GetSearchResultCount(condition).ToList();

                int total = 0;                
                if (searchResultCount.Count > 0)
                {
                    total = searchResultCount[0];
                    HttpContext.Current.Session["SearchCount"] = searchResultCount[0];
                }
                else
                {
                    HttpContext.Current.Session["SearchCount"] = total;
                }
               
                if (!isListOutputProcess)
                {
                    ViewSearchResultCount(total);
                    condition.MaxSearchResult = MaxSearchResultCount;
                    HttpContext.Current.Session["MaxSearchCount"] = MaxSearchResultCount;
                    if (IsMaxSerchResultCountOver)
                    {
                        return new List<CsmsCommonInfo>().AsQueryable().ToList();
                    }
                }
                else
                {
                    condition.MaxSearchResult = null;
                }
                if (total > 0)
                {
                    SearchConditionCsv = chemDataInfo.Csv;
                    return chemDataInfo.GetSearchResult(condition);
                }
            }
            
            ViewSearchResultCount(0);
            return new List<CsmsCommonInfo>().AsQueryable().ToList();
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 30-9-2019  
        /// Description :this Method is used to download the CSV File
        /// <summary>

        public void ListOutput(List<CsmsCommonInfo> Records)
        {
            string SearchConditionCSV = null;
            if (HttpContext.Current.Session["SearchConditions"] != null)
            {
                SearchConditionCSV = HttpContext.Current.Session["SearchConditions"].ToString();
            }

            var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            if (!string.IsNullOrEmpty(SearchConditionCSV))
            {
                using (var csv = new StreamWriter(csvFileFullPath, false, Encoding.GetEncoding("shift_jis")))
                {
                    csv.Write(SearchConditionCSV);
                    csv.Close();
                }
            }
            Classes.Extensions.GridViewExtension.OutputCSV(Records, csvFileFullPath);
            string replaceCsvFileName = CsvFileName.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
            Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
        }



        /// <summary>
        /// Author: MSCGI
        /// Created date: 30-9-2019  
        /// Description :this Method is used to Create the Sql Query to get the Chemical SubStances Details
        /// <summary>
        private StringBuilder createListSQL(List<CsmsCommonInfo> resultGridView)
        {

            var condition = (ChemSearchModel)HttpContext.Current.Session[nameof(ChemSearchModel)];
            Hashtable _para = new Hashtable();
            var createSql = new StringBuilder();

            List<String> ChemCDList = new List<string>();
            String SearchChemCDStr;
            List<String> ChemProdList = new List<string>();


            foreach (var item in resultGridView)
            {
                if (condition.CHEM_SEARCH_TARGET == 0)
                {
                    ChemCDList.Add("'" + item.CHEM_CD + "'");
                }
                else
                {

                    ChemCDList.Add("'" + item.CHEM_CD + item.PRODUCT_SEQ + "'");
                }
            }
            SearchChemCDStr = string.Join(",", ChemCDList);
            createSql.AppendLine("select ");
            createSql.AppendLine(" M_CHEM.CHEM_CD,");
            createSql.AppendLine(" M_CHEM.CHEM_NAME,");
            createSql.AppendLine(" M_CHEM.CHEM_CAT,");
            createSql.AppendLine(" M_CHEM.CAS_NO,");
            createSql.AppendLine(" com.DISPLAY_VALUE AS KEY_MGMT_NAME,");
            createSql.AppendLine(" com2.DISPLAY_VALUE AS WEIGHT_MGMT_NAME,");
            createSql.AppendLine(" com3.DISPLAY_VALUE AS FIGURE_NAME,");
            createSql.AppendLine(" M_CHEM.DENSITY,");
            createSql.AppendLine(" M_CHEM.FLASH_POINT,");
            createSql.AppendLine(" M_CHEM.SDS_FILENAME AS SDS_FILENAME,");
            createSql.AppendLine(" M_CHEM.SDS_URL AS SDS_URL2,");
            createSql.AppendLine(" M_CHEM.SDS_UPD_DATE AS SDS_UPD_DATE2,");
            createSql.AppendLine(" M_CHEM.MEMO,");
            createSql.AppendLine(" IIF(M_CHEM.DEL_FLAG = 0,'使用中',IIF(M_CHEM.DEL_FLAG = 1,'削除済み','保存中')) AS USAGE_STATUS_NAME,");
            createSql.AppendLine(" M_CHEM.OPTIONAL1,");
            createSql.AppendLine(" M_CHEM.OPTIONAL2,");
            createSql.AppendLine(" M_CHEM.OPTIONAL3,");
            createSql.AppendLine(" M_CHEM.OPTIONAL4,");
            createSql.AppendLine(" M_CHEM.OPTIONAL5,");

            if (condition.CHEM_SEARCH_TARGET == 0)
            {
                createSql.AppendLine(" '' AS MAKER_NAME,");
                createSql.AppendLine(" '' AS PRODUCT_NAME,");
                createSql.AppendLine(" '' AS UNITSIZE_NAME_JOIN,");
                createSql.AppendLine(" '' AS UNITSIZE,");
                createSql.AppendLine(" '' AS PRODUCT_BARCODE,");
                createSql.AppendLine(" '' AS SDS_URL,");
                createSql.AppendLine(" '' AS SDS_UPD_DATE,");
            }
            else
            {
                createSql.AppendLine(" M_MAKER.MAKER_NAME,");
                createSql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_NAME,");
                createSql.AppendLine(" M_CHEM_PRODUCT.UNITSIZE AS UNITSIZE_NAME_JOIN,");
                createSql.AppendLine(" M_UNITSIZE.UNIT_NAME AS UNITSIZE,");
                createSql.AppendLine(" M_CHEM_PRODUCT.PRODUCT_BARCODE,");
                createSql.AppendLine(" M_CHEM_PRODUCT.SDS_URL,");
                createSql.AppendLine(" M_CHEM_PRODUCT.SDS_UPD_DATE,");
            }



            createSql.AppendLine(" M_CHEM.EXPIRATION_DATE,");
            createSql.AppendLine("STUFF((");
            createSql.AppendLine("select ','+CONVERT(VARCHAR,M_REGULATION.REG_ACRONYM_TEXT)");

            createSql.AppendLine(" from M_CHEM_REGULATION,M_REGULATION");
            createSql.AppendLine(" where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" AND M_CHEM_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID");
            createSql.AppendLine(" AND M_CHEM_REGULATION.CLASS_ID = 1 --法規制");
            createSql.AppendLine(" order by M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" FOR XML PATH('')), 1, 1, '') AS 'REGULATION',");
            createSql.AppendLine("STUFF((");
            createSql.AppendLine("select ','+CONVERT(VARCHAR,IIF(M_CHEM_REGULATION.INPUT_FLAG = 0,'自動入力','手入力'))");
            createSql.AppendLine(" from M_CHEM_REGULATION");
            createSql.AppendLine(" where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" AND M_CHEM_REGULATION.CLASS_ID = 1 --法規制");
            createSql.AppendLine(" order by M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" FOR XML PATH('')), 1, 1, '') AS 'REGULATION_Division',");
            createSql.AppendLine(" STUFF((");
            createSql.AppendLine("select ','+CONVERT(VARCHAR,M_REGULATION.REG_ACRONYM_TEXT)");
            createSql.AppendLine(" from M_CHEM_REGULATION,M_REGULATION");
            createSql.AppendLine(" where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" AND M_CHEM_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID");
            createSql.AppendLine(" AND M_CHEM_REGULATION.CLASS_ID = 3 --消防法");
            createSql.AppendLine(" order by M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" FOR XML PATH('')), 1, 1, '') AS 'Fire',");
            createSql.AppendLine(" STUFF((");
            createSql.AppendLine("select ','+CONVERT(VARCHAR,IIF(M_CHEM_REGULATION.INPUT_FLAG = 0,'自動入力','手入力'))");
            createSql.AppendLine(" from M_CHEM_REGULATION");
            createSql.AppendLine(" where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" AND M_CHEM_REGULATION.CLASS_ID = 3 --消防法");
            createSql.AppendLine(" order by M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" FOR XML PATH('')), 1, 1, '') AS 'Fire_Division',");
            createSql.AppendLine(" CONVERT(VARCHAR,IIF(M_CHEM.FIRE_FLAG = 0,'該当','非該当')) AS 'FIREFLAG',");


            createSql.AppendLine(" STUFF((");
            createSql.AppendLine("select ','+CONVERT(VARCHAR,M_REGULATION.REG_ACRONYM_TEXT)");
            createSql.AppendLine(" from M_CHEM_REGULATION,M_REGULATION");
            createSql.AppendLine(" where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" AND M_CHEM_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID");
            createSql.AppendLine(" AND M_CHEM_REGULATION.CLASS_ID = 4 --社内ルール");
            createSql.AppendLine(" order by M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" FOR XML PATH('')), 1, 1, '') AS 'Office',");
            createSql.AppendLine(" STUFF((");
            createSql.AppendLine("select ','+CONVERT(VARCHAR,IIF(M_CHEM_REGULATION.INPUT_FLAG = 0,'自動入力','手入力'))");
            createSql.AppendLine(" from M_CHEM_REGULATION");
            createSql.AppendLine(" where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" AND M_CHEM_REGULATION.CLASS_ID = 4 --社内ルール");
            createSql.AppendLine(" order by M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" FOR XML PATH('')), 1, 1, '') AS 'Office_Division',");
            createSql.AppendLine("STUFF((");
            createSql.AppendLine("select ','+CONVERT(VARCHAR,M_REGULATION.REG_ACRONYM_TEXT)");
            createSql.AppendLine(" from M_CHEM_REGULATION,M_REGULATION");
            createSql.AppendLine(" where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" AND M_CHEM_REGULATION.REG_TYPE_ID = M_REGULATION.REG_TYPE_ID");
            createSql.AppendLine(" AND M_CHEM_REGULATION.CLASS_ID = 2 --GHS分類");
            createSql.AppendLine(" order by M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" FOR XML PATH('')), 1, 1, '') AS 'GHS',");
            createSql.AppendLine(" STUFF((");
            createSql.AppendLine("select ','+CONVERT(VARCHAR,IIF(M_CHEM_REGULATION.INPUT_FLAG = 0,'自動入力','手入力'))");
            createSql.AppendLine(" from M_CHEM_REGULATION");
            createSql.AppendLine(" where M_CHEM.CHEM_CD = M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" AND M_CHEM_REGULATION.CLASS_ID = 2 --GHS分類");
            createSql.AppendLine(" order by M_CHEM_REGULATION.CHEM_CD");
            createSql.AppendLine(" FOR XML PATH('')), 1, 1, '') AS 'GHS_Division',");
            createSql.AppendLine(" M_CHEM.REF_CHEM_CD,");
            createSql.AppendLine(" user1.USER_NAME AS 'REG_USER_CD',");
            createSql.AppendLine(" M_CHEM.REG_DATE,");
            createSql.AppendLine(" user2.USER_NAME AS 'UPD_USER_CD',");
            createSql.AppendLine(" M_CHEM.UPD_DATE,");
            createSql.AppendLine(" user3.USER_NAME AS 'DEL_USER_CD',");
            createSql.AppendLine(" M_CHEM.DEL_DATE");
            createSql.AppendLine("from ");
            createSql.AppendLine("M_CHEM");
            createSql.AppendLine("left join M_USER user1 ON M_CHEM.REG_USER_CD = user1.USER_CD");
            createSql.AppendLine("left join M_USER user2 ON M_CHEM.UPD_USER_CD = user2.USER_CD");
            createSql.AppendLine("left join M_USER user3 ON M_CHEM.DEL_USER_CD = user3.USER_CD");
            createSql.AppendLine(",");
            createSql.AppendLine("M_COMMON com,");
            createSql.AppendLine("M_COMMON com2,");
            createSql.AppendLine("M_COMMON com3");

            if (condition.CHEM_SEARCH_TARGET == 0)
            {
                createSql.AppendLine("where");
                createSql.AppendLine("M_CHEM.CHEM_CD in (" + SearchChemCDStr + ")");
            }
            else
            {
                createSql.AppendLine(",M_CHEM_PRODUCT,");
                createSql.AppendLine("M_MAKER,");
                createSql.AppendLine("M_UNITSIZE");
                createSql.AppendLine("where");
                createSql.AppendLine("M_CHEM.CHEM_CD+CONVERT(VARCHAR,M_CHEM_PRODUCT.PRODUCT_SEQ) in (" + SearchChemCDStr + ")");
            }

            createSql.AppendLine("AND M_CHEM.KEY_MGMT = com.KEY_VALUE");
            createSql.AppendLine("AND com.TABLE_NAME = 'M_KEY_MGMT'");
            createSql.AppendLine("AND M_CHEM.WEIGHT_MGMT = com2.KEY_VALUE");
            createSql.AppendLine("AND com2.TABLE_NAME = 'M_WEIGHT_MGMT'");
            createSql.AppendLine("AND M_CHEM.FIGURE = com3.KEY_VALUE");
            createSql.AppendLine("AND com3.TABLE_NAME = 'M_FIGURE'");

            if (condition.CHEM_SEARCH_TARGET == 0)
            {
            }
            else
            {
                createSql.AppendLine("AND M_CHEM.CHEM_CD = M_CHEM_PRODUCT.CHEM_CD");
                createSql.AppendLine("AND M_CHEM_PRODUCT.MAKER_ID = M_MAKER.MAKER_ID");
                createSql.AppendLine("AND M_CHEM_PRODUCT.UNITSIZE_ID = M_UNITSIZE.UNITSIZE_ID");
            }
            createSql.AppendLine("order by M_CHEM.CHEM_CD");
            return createSql;
        }

        /// <summary>
        /// <para>検索条件のSelectMethodで検索条件を取得します。</para>
        /// <para>セッションに検索条件が格納されている場合はセッションの値を、格納されていない場合は初期値を返します。</para>
        /// </summary>
        /// <param name="condition">セッションに格納されている検索条件を抽出します。</param>
        /// <returns>検索条件を取得します。</returns>
        public ChemSearchModel GetSearchInfo([Session(nameof(ChemSearchModel))]ChemSearchModel condition)
        {
            UserInfo.ClsUser userInfo = null;
            Common.GetUserInfo(null, out userInfo);
            var isAdmin = userInfo != null ? userInfo.ADMINFLG == Const.cADMIN_FLG_ADMIN : false;
            return null;

        }

        /// <summary>
        /// <para>編集ボタンクリックイベントです。</para>
        /// <para>選択している化学物質を編集する化学物質マスターメンテナンス画面を開きます。</para>
        /// </summary>
        public List<CsmsCommonInfo> Edit(string chemSubstanceCode)
        {
            var code = chemSubstanceCode;
            if (code == null) return null;

            var condition = new ChemSearchModel();
            condition.CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching;
            condition.CHEM_CD = code;
            condition.USAGE_STATUS = (int)Classes.UsageStatusCondtionType.All;
            var d = new ChemDataInfo();
            return d.GetSearchResult(condition).ToList();
        }
        public CsmsCommonInfo getData(string chemSubstanceCode)
        {
            List<CsmsCommonInfo> lstEdit = Edit(chemSubstanceCode);

            foreach (CsmsCommonInfo records in lstEdit)
            {
                data.CHEM_CD = records.CHEM_CD;
                data.CHEM_NAME = records.CHEM_NAME;
                data.CAS_NO = records.CAS_NO;
                int Cat = 0;
                if (records.CHEM_CAT.Equals(string.Empty))
                {
                    data.CHEM_CAT = Cat.ToString();
                }
                else if (records.CHEM_CAT.Contains("単体"))
                {
                    Cat = 1;
                    data.CHEM_CAT = Cat.ToString();
                }
                else if (records.CHEM_CAT.Contains("混合物"))
                {
                    Cat = 2;
                    data.CHEM_CAT = Cat.ToString();
                }
                data.DENSITY = records.DENSITY;
                data.FLASH_POINT = records.FLASH_POINT;
                data.EXPIRATION_DATE = records.EXPIRATION_DATE;
                data.SDS_URL2 = records.SDS_URL2;
                data.MEMO = records.MEMO;
                data.OPTIONAL1 = records.OPTIONAL1;
                data.OPTIONAL2 = records.OPTIONAL2;
                data.OPTIONAL3 = records.OPTIONAL3;
                data.OPTIONAL4 = records.OPTIONAL4;
                data.OPTIONAL5 = records.OPTIONAL5;
                if (data.UPD_DATE != null)
                {
                    data.SDS_UPD_DATE2 = data.UPD_DATE.Value;
                }
                data.SDS_UPD_DATE = records.SDS_UPD_DATE;
                //data.UPD_DATE_DISPLAY = records.UPD_DATE_DISPLAY;
                data.UPD_USER_NAME = records.UPD_USER_NAME;
                data.FIGURE = records.FIGURE;
                data.KEY_MGMT = records.KEY_MGMT;
                data.WEIGHT_MGMT = records.WEIGHT_MGMT;
                data.SDS_URL2 = records.SDS_URL2;

            }
            return data;
        }

    }
}