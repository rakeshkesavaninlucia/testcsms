﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.util;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Chem;
using ChemMgmt.Nikon.Models;
using IdentityManagement.Entities;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using ZyCoaG.BaseCommon;
using ZyCoaG.Extensions;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.Chem;
using Dapper;
using System.Linq;
using ChemMgmt.Classes.Extensions;
using ZyCoaG;
using ChemMgmt.Models;

namespace ChemMgmt.Repository
{
    public class NewRegistration
    {
        private LogUtil _logoututil = new LogUtil();
        private const string PROGRAM_ID = "ZC110";

        private List<V_REGULATION> gridreg = new List<V_REGULATION>();

        private List<V_FIRE> gridfire = new List<V_FIRE>();

        private List<V_OFFICE> gridoffice = new List<V_OFFICE>();

        private List<V_GHSCAT> gridghs = new List<V_GHSCAT>();
        //2019/04/05 TPE.Rin Add End

        private bool regulationFlg = false;
        private bool ghsFlg = false;
        private bool fireFlg = false;
        private bool officeFlg = false;

        //2019/04/11 TPE.Rin
        private List<string> regtypeid = new List<string>();
        private List<string> firetypeid = new List<string>();
        private List<string> ghstypeid = new List<string>();
        private List<string> officetypeid = new List<string>();

        /// <summary>
        /// セッション変数に格納する製品別名一覧の名前を定義します。
        /// </summary>
        private const string sessionAliases = "Aliases";

        /// <summary>
        /// セッション変数に格納する含有物一覧の名前を定義します。
        /// </summary>
        private const string sessionContaineds = "Containeds";

        /// <summary>
        /// セッション変数に格納するGHS分類ID'sの名前を定義します。
        /// </summary>
        private const string sessionGhsCategoryIds = "GhsCategoryIds";

        /// <summary>
        /// セッション変数に格納する法規制ID'sの名前を定義します。
        /// </summary>
        private const string sessionRegulationIds = "RegulationIds";

        //2019/03/11 TPE.Rin Add Stert
        /// <summary>
        /// セッション変数に格納する消防法ID'sの名前を定義します。
        /// </summary>
        private const string sessionFireIds = "FireIds";

        /// <summary>
        /// セッション変数に格納する社内ルールID'sの名前を定義します。
        /// </summary>
        private const string sessionOfficeIds = "OfficeIds";
        //2019/03/11 TPE.Rin Add End

        /// <summary>
        /// セッション変数に格納する商品一覧の名前を定義します。
        /// </summary>
        private const string sessionProducts = "Products";

        //2019/03/19 TPE.Rin Add Start
        /// <summary>
        /// 法規制反映で反映された法規制を定義します。
        /// </summary>
        private const string sessionReflection = "Reflection";

        /// <summary>
        /// 更新後に表示する名称を格納するセッション名を取得します。
        /// </summary>
        private string sessionAfterUpdateType { get; } = "AfterUpdateType";



        /// <summary>
        /// 更新後の更新種類を取得、格納します。
        /// </summary>
        private UpdateType afterUpdateType
        {
            get
            {
                if (HttpContext.Current.Session[sessionAfterUpdateType] == null)
                {
                    return default(UpdateType);
                }
                return (UpdateType)HttpContext.Current.Session[sessionAfterUpdateType];
            }
            set
            {
                HttpContext.Current.Session.Add(sessionAfterUpdateType, value);
            }
        }
        /// <summary>
        /// 更新の種類
        /// </summary>
        protected enum UpdateType
        {
            /// <summary>
            /// 登録
            /// </summary>
            Register,

            /// <summary>
            /// 更新
            /// </summary>
            Update,

            /// <summary>
            /// 抹消
            /// </summary>
            Remove,

            /// <summary>
            /// 抹消取消
            /// </summary>
            UndoRemove,

            /// <summary>
            /// 一時保存（登録）
            /// </summary>
            TemporaryRegister, // 20180723 FJ)Shinagawa Add

            /// <summary>
            /// 一時保存（更新）
            /// </summary>
            TemporaryUpdate, // 20180723 FJ)Shinagawa Add

            /// <summary>
            /// 保存状態の登録
            /// </summary>
            TemporaryRegisterFix, // 20180723 FJ)Shinagawa Add

            /// <summary>
            /// 保存状態の更新
            /// </summary>
            TemporaryUpdateFix, // 20180723 FJ)Shinagawa Add

            /// <summary>
            /// 物理抹消
            /// </summary>
            RemovePhysical // 20180723 FJ)Shinagawa Add
        }


        /// <summary>
        /// <para>保存状態の登録ボタンの処理です。</para>
        /// <para>画面に表示されている化学物質管理台帳情報を登録、更新します。</para>
        /// </summary>


        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-9-2019  
        /// Description :This Method is used to Register the Chemical Substances
        /// <summary>

        public void Register(CsmsCommonInfo ChemControlValues)
        {
            //List<string> Errormessage = ValidateEntity(UpdateType.Register, ChemControlValues);
            //if (Errormessage.Count > 0)
            //{
            //    HttpContext.Current.Session["ErrorMessage"] = Errormessage;
            //}
            //else
            //{
            DoUpdateForProcess(UpdateType.Register, ChemControlValues);
            //}

        }

        /// <summary>
        /// 検証処理の実体です。
        /// </summary>
        /// <param name="updateType">更新の種類。</param>
        /// 

        ///// <summary>
        ///// Author: MSCGI
        ///// Created date: 16-9-2019  
        ///// Description :Common Method is used to validate the data
        ///// <summary>
        //private List<string> ValidateEntity(UpdateType updateType, CsmsCommonInfo ChemControlValues)
        //{

        //    getInputData(ChemControlValues);
        //    var errorMessages = new List<string>();
        //    var isUsedCasNumber = false;
        //    int mode = 0;
        //    if (updateType == UpdateType.Register)
        //    {
        //        mode = 0;
        //    }
        //    else if (updateType == UpdateType.Update)
        //    {
        //        mode = 2;
        //    }

        //    var chem = (ChemModel)HttpContext.Current.Session[nameof(ChemModel)];


        //    switch (updateType)
        //    {
        //        case UpdateType.Register:
        //        case UpdateType.Update:
        //        case UpdateType.TemporaryRegisterFix:
        //        case UpdateType.TemporaryUpdateFix:

        //            ChemDataInfo chemDataInfo = new ChemDataInfo();
        //            chemDataInfo.ValidateDataSingle(chem, "化学物質名", (int)mode);
        //            chemDataInfo.IsUsedChemName(chem);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }

        //            isUsedCasNumber = chemDataInfo.IsUsedCasNumber(chem);

        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingle(chem, "CAS#", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }


        //            ChemAliasDataInfo chemAliasDataInfo = new ChemAliasDataInfo();

        //            var chemInputedAliases = (List<ChemAliasModel>)HttpContext.Current.Session[sessionAliases];

        //            chemAliasDataInfo.ValidateData(chemInputedAliases);
        //            List<string> chemName = new List<string>();
        //            foreach (var chemInputed in chemInputedAliases)
        //            {
        //                string strChemName = chemInputed.CHEM_NAME.UnityWidth();
        //                if (chemName.Contains(strChemName) == true)
        //                {
        //                    chemAliasDataInfo.ErrorMessages.Add(string.Format(WarningMessages.NotUnique, "化学物質別名", strChemName));
        //                }
        //                chemName.Add(strChemName);
        //            }

        //            if (chemAliasDataInfo.ErrorMessages.Count == 0)
        //            {
        //                // 異なるCHEM_CDの別名をDBから取得し比較
        //                foreach (var chemInputed in chemInputedAliases)
        //                {
        //                    var chemAlias = new ChemAliasModel();
        //                    chemAlias.CHEM_CD = chem.CHEM_CD;
        //                    chemAlias.CHEM_NAME = chemInputed.CHEM_NAME.UnityWidth();
        //                    chemAliasDataInfo.IsUsedChemAliasName(chemAlias, chem);
        //                }
        //            }
        //            if (chemAliasDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemAliasDataInfo.ErrorMessages);
        //            }


        //            chemDataInfo = new ChemDataInfo();

        //            chemDataInfo.ValidateDataSingle(chem, "分類", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();


        //            chemDataInfo = new ChemDataInfo();

        //            chemDataInfo.ValidateDataSingle(chem, "形状", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();


        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingle(chem, "比重", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();


        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingle(chem, "引火点", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();

        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingle(chem, "開封後有効期限", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();

        //            if (HttpContext.Current.Session[sessionContaineds] != null)
        //            {

        //                ChemContainedDataInfo chemContainedDataInfomodel = new ChemContainedDataInfo();
        //                List<ChemContainedModel> chemContained = (List<ChemContainedModel>)HttpContext.Current.Session[sessionContaineds];
        //                chemContainedDataInfomodel.ValidateData(chemContained);
        //                if (chemContainedDataInfomodel.ErrorMessages.Count > 0)
        //                {
        //                    errorMessages.AddRange(chemContainedDataInfomodel.ErrorMessages);
        //                }
        //            }
        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingle(chem, "鍵管理", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();

        //            ChemDataInfo chemDataInfomodel = new ChemDataInfo();

        //            chemDataInfomodel.ValidateDataSingle(chem, "重量管理", (int)mode);
        //            if (chemDataInfomodel.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfomodel.ErrorMessages);
        //            }
        //            chemDataInfomodel.Dispose();




        //            if (HttpContext.Current.Session[sessionProducts] != null)
        //            {
        //                ChemProductDataInfo chemProductsDataInfomodel = new ChemProductDataInfo();
        //                List<ChemProductModel> chemProduct = (List<ChemProductModel>)HttpContext.Current.Session[sessionProducts];
        //                if (chemProduct != null)
        //                {
        //                    chemProductsDataInfomodel.ValidateData(chemProduct);
        //                    if (chemProductsDataInfomodel.ErrorMessages.Count > 0)
        //                    {
        //                        errorMessages.AddRange(chemProductsDataInfomodel.ErrorMessages);
        //                    }
        //                }
        //            }



        //            chemDataInfo = new ChemDataInfo();

        //            chemDataInfo.ValidateDataSingle(chem, "備考", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();


        //            chemDataInfo = new ChemDataInfo();

        //            chemDataInfo.ValidateDataSingle(chem, "任意項目1", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }

        //            chemDataInfo.Dispose();

        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingle(chem, "任意項目2", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();


        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingle(chem, "任意項目3", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }

        //            chemDataInfo.Dispose();

        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingle(chem, "任意項目4", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();

        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingle(chem, "任意項目5", (int)mode);
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            chemDataInfo.Dispose();


        //            //消防法非該当チェックがOFFのとき、消防法が引き当てられてない場合はメッセージを出す
        //            //chemDataInfo = new ChemDataInfo();


        //            //if (ChemControlValues.FIRE_FLAG == false)
        //            //{
        //            //    if (ChemControlValues.FireModel.Count == 0)
        //            //    {
        //            //        chemDataInfo.ErrorMessages.Add("消防法非該当がオフになっています。消防法を入力してください。");
        //            //    }
        //            //}
        //            //if (chemDataInfo.ErrorMessages.Count > 0)
        //            //{
        //            //    errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            //}
        //            chemDataInfo.Dispose();
        //            break;
        //        case UpdateType.TemporaryRegister:
        //        case UpdateType.TemporaryUpdate:
        //            //一時保存時入力チェック
        //            chemDataInfo = new ChemDataInfo();

        //            chemDataInfo.ValidateDataSingleSimple(chem, "化学物質名");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }


        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "CAS#");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }



        //            ChemAliasDataInfo chemAliasData = new ChemAliasDataInfo();

        //            List<ChemAliasModel> chemInputedAlias = (List<ChemAliasModel>)HttpContext.Current.Session[sessionAliases];

        //            chemAliasData.ValidateDataSimple(chemInputedAlias);
        //            if (chemAliasData.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemAliasData.ErrorMessages);
        //            }


        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "分類");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }



        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "形状");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }



        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "比重");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }



        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "引火点");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }



        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "開封後有効期限");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }




        //            ChemContainedDataInfo chemContainedDataInfo = new ChemContainedDataInfo();

        //            var chemContaineds = (List<ChemContainedModel>)HttpContext.Current.Session[sessionContaineds];

        //            chemContainedDataInfo.ValidateDataSimple(chemContaineds);
        //            if (chemContainedDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemContainedDataInfo.ErrorMessages);
        //            }


        //            chemDataInfo = new ChemDataInfo();

        //            chemDataInfo.ValidateDataSingleSimple(chem, "鍵管理");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }

        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "重量管理");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }


        //            ChemProductDataInfo chemProductsDataInfo = new ChemProductDataInfo();

        //            var chemProducts = (List<ChemProductModel>)HttpContext.Current.Session[sessionProducts];

        //            chemProductsDataInfo.ValidateDataSimple(chemProducts);
        //            if (chemProductsDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemProductsDataInfo.ErrorMessages);
        //            }


        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "備考");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }



        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "任意項目1");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }



        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "任意項目2");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }



        //            chemDataInfo = new ChemDataInfo();


        //            chemDataInfo.ValidateDataSingleSimple(chem, "任意項目3");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }


        //            chemDataInfo = new ChemDataInfo();

        //            chemDataInfo.ValidateDataSingleSimple(chem, "任意項目4");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }



        //            chemDataInfo = new ChemDataInfo();

        //            chemDataInfo.ValidateDataSingleSimple(chem, "任意項目5");
        //            if (chemDataInfo.ErrorMessages.Count > 0)
        //            {
        //                errorMessages.AddRange(chemDataInfo.ErrorMessages);
        //            }
        //            break;
        //    }


        //    if (errorMessages.Count > 0)
        //    {
        //        //OutputWarningMessage(errorMessages);
        //    }
        //    else
        //    {

        //        if (isUsedCasNumber)
        //        {
        //            //OutputConfirmMessage(string.Format(InfoMessages.DuplicateConfirm, "CAS#", "化学物質"), button);
        //        }
        //        else
        //        {
        //            //ClientButtonClick(button);
        //        }
        //    }
        //    return errorMessages;
        //}


        // 20180723 FJ)Shinagawa Add Start ->
        /// <summary>
        /// <para>一時保存（登録）ボタンの処理です。</para>
        /// <para>画面に表示されている化学物質管理台帳情報を一時保存します。</para>
        /// </summary>

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019  
        /// Description :This Method is for Temporary Registration
        /// <summary>
        public void TemproryRegister(CsmsCommonInfo chemControlValues)
        {            
            DoUpdateForProcess(UpdateType.TemporaryRegister, chemControlValues);            

        }

        /// <summary>
        /// <para>更新ボタンの処理です。</para>
        /// <para>画面に表示されている化学物質管理台帳情報を更新します。</para>
        /// </summary>

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019  
        /// Description :This Method is for Record Updation
        /// <summary>
        public void updateRecords(CsmsCommonInfo chemControlValues)
        {           
            DoUpdateForProcess(UpdateType.Update, chemControlValues);           

        }



        /// <summary>
        /// <para>抹消ボタンの処理です。</para>
        /// <para>画面に表示されている化学物質管理台帳情報を抹消します。</para>
        /// </summary>


        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019  
        /// Description :This Method is for Removing of the Chemical Substances
        /// <summary>
        public void Remove(CsmsCommonInfo chemControlValues)
        {
            DoUpdateForProcess(UpdateType.Remove, chemControlValues);
        }

        public void RemovePhysical(CsmsCommonInfo chemControlValues)
        {
            DoUpdateForProcess(UpdateType.RemovePhysical, chemControlValues);
        }


        /// <summary>
        /// <para>抹消取消ボタンの処理です。</para>
        /// <para>画面に表示されている化学物質管理台帳情報を復活します。</para>
        /// </summary>

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019  
        /// Description :This Method is for undoing Removed of the Chemical Substances
        /// <summary>
        public void UndoRemove(CsmsCommonInfo chemControlValues)
        {
            DoUpdateForProcess(UpdateType.UndoRemove, chemControlValues);
        }

    
        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019  
        /// Description :Common Method for Chemical Substances
        /// <summary>
        protected void DoUpdateForProcess(UpdateType updateType, CsmsCommonInfo chemControlValues)
        {

            getInputData(chemControlValues);
            var chem = (ChemModel)HttpContext.Current.Session[nameof(ChemModel)];
            chem.CHEM_NAME = chem.CHEM_NAME.UnityWidth();
            try
            {
                using (var chemDataInfo = new ChemDataInfo())
                {
                    UserInformation userInfo = null;
                    if (HttpContext.Current.Session["LoginUserSession"] != null)
                    {
                        userInfo = (UserInformation)HttpContext.Current.Session["LoginUserSession"];
                    }

                    chem.REG_USER_CD = userInfo.User_CD;
                    chem.UPD_USER_CD = userInfo.User_CD;
                    chem.DEL_USER_CD = userInfo.User_CD;


                    bool fireflag = chem.FIRE_FLAG;
                    chem.FIRE_FLAG = fireflag;
                    switch (updateType)
                    {
                        case UpdateType.Register:
                            chem.REF_CHEM_CD = chem.CHEM_CD;
                            chem.CHEM_CD = chemDataInfo.GetNewChemCode();
                            chem.DEL_FLAG = (int)DEL_FLAG.Undelete;
                            chemDataInfo.Add(chem);
                            break;
                        case UpdateType.Update:
                            chemDataInfo.Save(chem);
                            break;
                        case UpdateType.Remove:
                            chemDataInfo.Remove(chem);
                            break;
                        case UpdateType.UndoRemove:
                            chemDataInfo.Undo(chem);
                            break;
                        case UpdateType.TemporaryRegister:
                            chem.REF_CHEM_CD = chem.CHEM_CD;
                            chem.CHEM_CD = chemDataInfo.GetNewRefChemCode();
                            chem.DEL_FLAG = (int)DEL_FLAG.Temporary;
                            chemDataInfo.Add(chem, true);
                            break;
                        case UpdateType.TemporaryUpdate:
                            chemDataInfo.Save(chem, true);
                            break;
                        //case UpdateType.TemporaryRegisterFix:
                        //    if (!RemoveGridData(chem)) { return; }
                        //    chemDataInfo.Remove(chem, true);

                        //    chem.CHEM_CD = chemDataInfo.GetNewChemCode();
                        //    chem.DEL_FLAG = (int)DEL_FLAG.Undelete;
                        //    chemDataInfo.Add(chem);
                        //    break;
                        //case UpdateType.TemporaryUpdateFix:
                        //    if (!RemoveGridData(chem)) { return; }
                        //    chemDataInfo.Remove(chem, true);

                        //    chem.CHEM_CD = chem.REF_CHEM_CD;
                        //    chemDataInfo.Save(chem);
                        //    break;
                        case UpdateType.RemovePhysical:
                            chemDataInfo.ChemDeletePhysical(chem);
                            break;
                    }
                    if (!chemDataInfo.IsValid)
                    {
                        //OutputWarningMessage(chemDataInfo.ErrorMessages);
                        return;
                    }
                }
                if (updateType == UpdateType.Register || updateType == UpdateType.Update || updateType == UpdateType.TemporaryRegister || updateType == UpdateType.TemporaryUpdate || updateType == UpdateType.TemporaryRegisterFix || updateType == UpdateType.TemporaryUpdateFix)
                {
                    using (var chemAliasDataInfo = new ChemAliasDataInfo())
                    {
                        if (updateType == UpdateType.Update || updateType == UpdateType.TemporaryUpdate || updateType == UpdateType.TemporaryUpdateFix)
                        {
                            var condition = new ChemAliasSearchModel();
                            condition.CHEM_CD = chem.CHEM_CD;
                            var deleteDataArray = chemAliasDataInfo.GetSearchResult(condition);
                            chemAliasDataInfo.Remove(deleteDataArray, true);
                        }
                        if (HttpContext.Current.Session[sessionAliases] != null)
                        {

                            var chemInputedAliases = (IEnumerable<ChemAliasModel>)HttpContext.Current.Session[sessionAliases];
                            var chemAliases = new List<ChemAliasModel>();
                            chemAliases.Add(new ChemAliasModel() { CHEM_CD = chem.CHEM_CD, CHEM_NAME_SEQ = 0, CHEM_NAME = chem.CHEM_NAME });
                            var i = 1;
                            foreach (var chemInputedAlias in chemInputedAliases)
                            {
                                var chemAlias = new ChemAliasModel();
                                chemAlias.CHEM_CD = chem.CHEM_CD;
                                chemAlias.CHEM_NAME_SEQ = i;
                                chemAlias.CHEM_NAME = chemInputedAlias.CHEM_NAME.UnityWidth();
                                chemAliases.Add(chemAlias);
                                i++;
                            }                           
                            if (updateType == UpdateType.TemporaryRegister || updateType == UpdateType.TemporaryUpdate)
                            {
                                chemAliasDataInfo.Add(chemAliases, true);
                            }
                            else
                            {
                                chemAliasDataInfo.Add(chemAliases);
                            }                           
                            if (!chemAliasDataInfo.IsValid)
                            {
                                //OutputWarningMessage(chemAliasDataInfo.ErrorMessages);
                                //return;
                            }
                        }
                    }

                    using (var chemContainedDataInfo = new ChemContainedDataInfo())
                    {
                        if (updateType == UpdateType.Update ||
                            updateType == UpdateType.TemporaryUpdate ||
                            updateType == UpdateType.TemporaryUpdateFix)

                        {
                            var condition = new ChemContainedSearchModel();
                            condition.CHEM_CD = chem.CHEM_CD;
                            var deleteDataArray = chemContainedDataInfo.GetSearchResult(condition);
                            chemContainedDataInfo.Remove(deleteDataArray, true);
                        }
                        if (HttpContext.Current.Session[sessionContaineds] != null)
                        {
                            List<ChemContainedModel> chemContaineds = (List<ChemContainedModel>)HttpContext.Current.Session["Contained"];
                            for (int i = 0; i < chemContaineds.Count; i++)
                            {
                                chemContaineds[i].CHEM_CD = chem.CHEM_CD;
                            }

                            if (updateType == UpdateType.TemporaryRegister || updateType == UpdateType.TemporaryUpdate)
                            {
                                chemContainedDataInfo.Add(chemContaineds, true);
                            }
                            else
                            {
                                chemContainedDataInfo.Add(chemContaineds);
                            }

                            if (!chemContainedDataInfo.IsValid)
                            {
                                //OutputWarningMessage(chemContainedDataInfo.ErrorMessages);
                                return;
                            }
                        }


                    }

                    using (var chemGhsCateogryDataInfo = new ChemRegulationDataInfo(ClassIdType.GhsCategory))
                    {

                        if (updateType == UpdateType.Update ||
                            updateType == UpdateType.TemporaryUpdate ||
                            updateType == UpdateType.TemporaryUpdateFix)
                        {
                            var ghsCategoryInfo = new Nikon.GhsCategoryMasterInfo();
                            var ghsCategoryIds = ghsCategoryInfo.GetIdsBelongInChem(chem.CHEM_CD);
                            var deleteDataArray = new List<M_CHEM_REGULATION>();
                            foreach (var ghsCategoryId in ghsCategoryIds) deleteDataArray.Add(new M_CHEM_REGULATION(chem.CHEM_CD, ghsCategoryId));
                            chemGhsCateogryDataInfo.Remove(deleteDataArray, true);
                        }
                        {
                            if (HttpContext.Current.Session[sessionGhsCategoryIds] != null)
                            {
                                var chemGhsCategoryIds = (IEnumerable<int>)HttpContext.Current.Session[sessionGhsCategoryIds];
                                if (chemGhsCategoryIds != null)
                                {
                                    var addDataArray = new List<M_CHEM_REGULATION>();
                                    foreach (var chemGhsCategoryId in chemGhsCategoryIds)
                                    {
                                        string inputflag = null;
                                        foreach (var containedsone in gridghs)
                                        {
                                            if (containedsone.GHSCAT_ID == chemGhsCategoryId)
                                            {
                                                inputflag = containedsone.INPUT_FLAG;
                                            }
                                        }
                                        addDataArray.Add(new M_CHEM_REGULATION(chem.CHEM_CD, chemGhsCategoryId, inputflag));
                                    }
                                    chemGhsCateogryDataInfo.Add(addDataArray);
                                }
                            }
                        }
                    }

                    using (var chemRegulationDataInfo = new ChemRegulationDataInfo(ClassIdType.Regulation))
                    {
                        if (updateType == UpdateType.Update ||
                            updateType == UpdateType.TemporaryUpdate ||
                            updateType == UpdateType.TemporaryUpdateFix)
                        {
                            var regulationInfo = new Nikon.RegulationMasterInfo();
                            var regulationIds = regulationInfo.GetIdsBelongInChem(chem.CHEM_CD);
                            var deleteDataArray = new List<M_CHEM_REGULATION>();
                            foreach (var regulationId in regulationIds) deleteDataArray.Add(new M_CHEM_REGULATION(chem.CHEM_CD, regulationId));
                            chemRegulationDataInfo.Remove(deleteDataArray, true);
                        }
                        {
                            if (HttpContext.Current.Session[sessionRegulationIds] != null)
                            {
                                var chemRegulationIds = (IEnumerable<int>)HttpContext.Current.Session[sessionRegulationIds];
                                if (chemRegulationIds != null)
                                {
                                    var addDataArray = new List<M_CHEM_REGULATION>();

                                    foreach (var chemRegulationId in chemRegulationIds)
                                    {
                                        string inputFlag = null;
                                        foreach (var containedsone in gridreg)
                                        {
                                            if (containedsone.REG_TYPE_ID == chemRegulationId)
                                            {
                                                inputFlag = containedsone.INPUT_FLAG;
                                            }
                                        }
                                        addDataArray.Add(new M_CHEM_REGULATION(chem.CHEM_CD, chemRegulationId, inputFlag));
                                    }
                                    chemRegulationDataInfo.Add(addDataArray);
                                }
                            }

                        }
                    }


                    using (var chemFireDataInfo = new ChemRegulationDataInfo(ClassIdType.Fire))
                    {
                        if (updateType == UpdateType.Update ||
                            updateType == UpdateType.TemporaryUpdate ||
                            updateType == UpdateType.TemporaryUpdateFix)
                        {
                            var fireInfo = new FireMasterInfo();
                            var fireIds = fireInfo.GetIdsBelongInChem(chem.CHEM_CD);
                            var deleteDataArray = new List<M_CHEM_REGULATION>();
                            foreach (var fireId in fireIds) deleteDataArray.Add(new M_CHEM_REGULATION(chem.CHEM_CD, fireId));
                            chemFireDataInfo.Remove(deleteDataArray, true);
                        }
                        {
                            if (HttpContext.Current.Session[sessionFireIds] != null)
                            {
                                var fireIds = (IEnumerable<int>)HttpContext.Current.Session[sessionFireIds];
                                if (fireIds != null)
                                {
                                    var addDataArray = new List<M_CHEM_REGULATION>();
                                    foreach (var fireId in fireIds)
                                    {
                                        string inputflag = null;
                                        foreach (var containedsone in gridfire)
                                        {
                                            if (containedsone.FIRE_ID == fireId)
                                            {
                                                inputflag = containedsone.INPUT_FLAG;
                                            }
                                        }

                                        addDataArray.Add(new M_CHEM_REGULATION(chem.CHEM_CD, fireId, inputflag));
                                    }
                                    chemFireDataInfo.Add(addDataArray);
                                }
                            }
                        }
                    }
                    using (var chemOfficeDataInfo = new ChemRegulationDataInfo(ClassIdType.Office))
                    {
                        if (updateType == UpdateType.Update ||
                            updateType == UpdateType.TemporaryUpdate ||
                            updateType == UpdateType.TemporaryUpdateFix)
                        {
                            var officeInfo = new OfficeMasterInfo();
                            var officeIds = officeInfo.GetIdsBelongInChem(chem.CHEM_CD);
                            var deleteDataArray = new List<M_CHEM_REGULATION>();
                            foreach (var officeId in officeIds) deleteDataArray.Add(new M_CHEM_REGULATION(chem.CHEM_CD, officeId));
                            chemOfficeDataInfo.Remove(deleteDataArray, true);
                        }
                        {
                            if (HttpContext.Current.Session[sessionOfficeIds] != null)
                            {
                                var officeIds = (IEnumerable<int>)HttpContext.Current.Session[sessionOfficeIds];
                                if (officeIds != null)
                                {
                                    var addDataArray = new List<M_CHEM_REGULATION>();
                                    foreach (var officeId in officeIds)
                                    {
                                        string inputflag = null;
                                        foreach (var containedsone in gridoffice)
                                        {
                                            if (containedsone.OFFICE_ID == officeId)
                                            {
                                                inputflag = containedsone.INPUT_FLAG;
                                            }
                                        }
                                        addDataArray.Add(new M_CHEM_REGULATION(chem.CHEM_CD, officeId, inputflag));

                                    }
                                    chemOfficeDataInfo.Add(addDataArray);
                                }
                            }
                        }
                    }
                    if (HttpContext.Current.Session[sessionProducts] != null)
                    {
                        using (var chemProductDataInfo = new ChemProductDataInfo())
                        {

                            if (updateType == UpdateType.Update || updateType == UpdateType.TemporaryUpdate || updateType == UpdateType.TemporaryUpdateFix)
                            {

                                var condition = new ChemSearchModel();
                                condition.CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching;
                                condition.CHEM_CD = chem.CHEM_CD;
                                condition.CHEM_SEARCH_TARGET = (int)ChemSearchTarget.Product;
                                condition.USAGE_STATUS = (int)Classes.UsageStatusCondtionType.All;
                                condition.IsOnlyProduct = true;
                                var d = new ChemDataInfo();
                                var dataArray = d.GetSearchResult(condition);
                                d.Dispose();
                                var deleteDataArray = new List<ChemProductModel>();
                                foreach (var data in dataArray) deleteDataArray.Add(new ChemProductModel(data));
                                chemProductDataInfo.Remove(deleteDataArray, true);
                            }
                            var chemProducts = (IEnumerable<ChemProductModel>)HttpContext.Current.Session[sessionProducts];
                            var i = 1;
                            foreach (var chemProduct in chemProducts)
                            {
                                chemProduct.CHEM_CD = chem.CHEM_CD;
                                chemProduct.PRODUCT_SEQ = i;
                                i++;
                            }
                            if (updateType == UpdateType.TemporaryRegister || updateType == UpdateType.TemporaryUpdate)
                            {
                                chemProductDataInfo.Add(chemProducts, true);
                            }
                            else
                            {
                                chemProductDataInfo.Add(chemProducts);
                            }

                            if (!chemProductDataInfo.IsValid)
                            {
                                //OutputWarningMessage(chemProductDataInfo.ErrorMessages);
                                return;
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                _logoututil.OutErrLog(PROGRAM_ID, e.StackTrace.ToString());
            }
            finally
            {

            }
        }



        /// <summary>
        /// セッション変数をクリアします。
        /// </summary>
        /// 
        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019  
        /// Description :Clearing Session
        /// <summary>
        public void ClearSession()
        {


            HttpContext.Current.Session[sessionAliases] = null;
            HttpContext.Current.Session[sessionContaineds] = null;
            HttpContext.Current.Session[sessionGhsCategoryIds] = null;
            HttpContext.Current.Session[sessionRegulationIds] = null;
            HttpContext.Current.Session[sessionProducts] = null;
            HttpContext.Current.Session[sessionFireIds] = null;
            HttpContext.Current.Session[sessionOfficeIds] = null;
            HttpContext.Current.Session["RegulationModel"] = null;
            HttpContext.Current.Session["GHSModel"] = null;
            HttpContext.Current.Session["OfficeRulesmodel"] = null;
            HttpContext.Current.Session["FireServicesModel"] = null;
            HttpContext.Current.Session["Contained"] = null;
            HttpContext.Current.Session["sessionAliases"] = null;
            HttpContext.Current.Session["ProductDetails"] = null;

            HttpContext.Current.Session.Remove(nameof(ChemModel));
            HttpContext.Current.Session.Remove(sessionAliases);
            HttpContext.Current.Session.Remove(sessionContaineds);
            HttpContext.Current.Session.Remove(sessionGhsCategoryIds);
            HttpContext.Current.Session.Remove(sessionRegulationIds);
            HttpContext.Current.Session.Remove(sessionProducts);
            HttpContext.Current.Session.Remove(sessionFireIds);
            HttpContext.Current.Session.Remove(sessionOfficeIds);
            HttpContext.Current.Session.Remove("RegulationModel");
            HttpContext.Current.Session.Remove("GHSModel");
            HttpContext.Current.Session.Remove("OfficeRulesmodel");
            HttpContext.Current.Session.Remove("FireServicesModel");
            HttpContext.Current.Session.Remove("Contained");
            HttpContext.Current.Session.Remove("sessionAliases");
            HttpContext.Current.Session.Remove("ProductDetails");
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019  
        /// Description :This Method is for Legal And Regulatory Reflection
        /// <summary>
        public void LegalReflection(CsmsCommonInfo ChemControlValues)
        {
            List<CsmsCommonInfo> Refelection = new List<CsmsCommonInfo>();
            try
            {
                regulationFlg = true;
                ghsFlg = true;
                fireFlg = true;
                officeFlg = true;
                string ChemCat = ChemControlValues.CHEM_CAT;
                string Figure;
                if (ChemControlValues.FIGURE.ToString() == null)
                {
                    Figure = string.Empty;
                }
                else
                {
                    Figure = ChemControlValues.FIGURE.ToString(); ;

                }

                DataSet dataSet = new DataSet();
                //プロシージャの引数がTable値型なので、ここでDataTableを作成する
                DataTable ProArg = new DataTable();

                //プロシージャが必要なのはCASと下限値
                ProArg.Columns.Add("INPUT_CAS", Type.GetType("System.String"));
                ProArg.Columns.Add("INPUT_MIN", Type.GetType("System.Decimal"));
                ProArg.Columns.Add("INPUT_MAX", Type.GetType("System.Decimal"));

                //単体と混合物でそれぞれ引数のデータを作成する
                if (ChemCat == "単体")
                {
                    string CasNo = ChemControlValues.CAS_NO;

                    DataRow Dr = ProArg.NewRow();

                    Dr["INPUT_CAS"] = CasNo;
                    Dr["INPUT_MIN"] = DBNull.Value;
                    Dr["INPUT_MAX"] = DBNull.Value;
                    ProArg.Rows.Add(Dr);
                }
                else
                {
                    //含有物情報のデータを取得する
                    //GetChemContained();

                    if (HttpContext.Current.Session["Contained"] != null)
                    {
                        var containeds = (List<ChemContainedModel>)HttpContext.Current.Session["Contained"];
                        foreach (var contained in containeds)
                        {
                            DataRow Dr = ProArg.NewRow();
                            Dr["INPUT_CAS"] = contained.CAS_NO;
                            if (!string.IsNullOrEmpty(contained.MinContainedRate))
                            {
                                Dr["INPUT_MIN"] = Convert.ToDecimal(contained.MinContainedRate);
                            }
                            else
                            {
                                Dr["INPUT_MIN"] = DBNull.Value;
                            }
                            if (!string.IsNullOrEmpty(contained.MaxContainedRate))
                            {
                                Dr["INPUT_MAX"] = Convert.ToDecimal(contained.MaxContainedRate);
                            }
                            else
                            {
                                Dr["INPUT_MAX"] = DBNull.Value;
                            }
                            ProArg.Rows.Add(Dr);
                        }
                    }
                }

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnectionString"].ConnectionString))
                {
                    conn.Open();
                    SqlCommand cmd = conn.CreateCommand();
                    string sql = "select * from dbo.POISONABLES_HANTEI(@INPUT_CAT,@INPUT_FIGURE,@INPUT_CAS)";
                    sql = sql + " union";
                    sql = sql + " select * from dbo.YUUKISOKU_HANTEI(@INPUT_CAT,@INPUT_FIGURE,@INPUT_CAS)";
                    sql = sql + "union";
                    sql = sql + " select * from dbo.TOKKASOKU_HANTEI(@INPUT_CAT,@INPUT_FIGURE,@INPUT_CAS)";
                    sql = sql + "union";
                    sql = sql + " select * from dbo.SONOTA_HANTEI(@INPUT_CAT,@INPUT_FIGURE,@INPUT_CAS)";

                    cmd.CommandText = sql;
                    //cmd.CommandText = "select * from dbo.POISONABLES(@INPUT_CAT,@INPUT_FIGURE,@POISONTABLE)";

                    SqlParameter param1 = new SqlParameter("@INPUT_FIGURE", SqlDbType.VarChar);
                    param1.Value = Figure;
                    cmd.Parameters.Add(param1);

                    SqlParameter param2 = new SqlParameter("@INPUT_CAT", SqlDbType.VarChar);
                    param2.Value = ChemCat;
                    cmd.Parameters.Add(param2);

                    SqlParameter param3 = new SqlParameter("@INPUT_CAS", SqlDbType.Structured);
                    param3.Value = ProArg;
                    param3.TypeName = "ARGUMENTLIST";
                    cmd.Parameters.Add(param3);

                    using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
                    {
                        adapter.Fill(dataSet, "REG_TYPE_ID");
                    }

                }

                DataTable dtTable;

                dtTable = dataSet.Tables["REG_TYPE_ID"];

                for (int i = 0; i < dtTable.Rows.Count; i++)
                {
                    switch (dtTable.Rows[i].ItemArray[1].ToString())
                    {
                        case "1":
                            regtypeid.Add(dtTable.Rows[i].ItemArray[0].ToString());
                            break;
                        case "2":
                            ghstypeid.Add(dtTable.Rows[i].ItemArray[0].ToString());
                            break;
                        case "3":
                            firetypeid.Add(dtTable.Rows[i].ItemArray[0].ToString());
                            break;
                        case "4":
                            officetypeid.Add(dtTable.Rows[i].ItemArray[0].ToString());
                            break;
                        default:
                            break;

                    }
                }

                List<int> regtypeids = regtypeid.ConvertAll(s => Int32.Parse(s));
                List<int> ghstypeids = ghstypeid.ConvertAll(s => Int32.Parse(s));
                List<int> firetypeids = firetypeid.ConvertAll(s => Int32.Parse(s));
                List<int> officetypeids = officetypeid.ConvertAll(s => Int32.Parse(s));

                //HttpContext.Current.Session.Add(sessionRegulationIds, regtypeids);
                //HttpContext.Current.Session.Add(sessionGhsCategoryIds, ghstypeids);
                //HttpContext.Current.Session.Add(sessionFireIds, firetypeids);
                //HttpContext.Current.Session.Add(sessionOfficeIds, officetypeids);

                if (HttpContext.Current.Session[sessionRegulationIds] != null)
                {
                    List<int> SessionValues = new List<int>();
                    List<int> SessionValues2 = new List<int>();
                    List<int> OldSession = (List<int>)HttpContext.Current.Session[sessionRegulationIds];
                    if (regtypeid != null && regtypeid.Count > 0)
                    {
                        if (OldSession != null && OldSession.Count > 0)
                        {
                            foreach (var items in OldSession)
                            {
                                SessionValues.Add(items);
                            }
                        }
                        if (regtypeids != null && regtypeids.Count > 0)
                        {
                            foreach (var items in regtypeids)
                            {
                                SessionValues2.Add(items);
                            }
                        }
                        var NewValues = SessionValues.Union(SessionValues2);
                        var NewTypeIds = NewValues.ToList().ConvertAll(x => Convert.ToInt32(x)).AsEnumerable();
                        HttpContext.Current.Session[sessionRegulationIds] = NewTypeIds;
                    }

                }
                else
                {
                    HttpContext.Current.Session[sessionRegulationIds] = regtypeid.ConvertAll(s => Int32.Parse(s));
                }

                if (HttpContext.Current.Session[sessionGhsCategoryIds] != null)
                {
                    List<int> SessionValues = new List<int>();
                    List<int> SessionValues2 = new List<int>();
                    List<int> GhsCategoryOldSessionval = (List<int>)HttpContext.Current.Session[sessionGhsCategoryIds];
                    if (ghstypeids != null && ghstypeids.Count > 0)
                    {
                        if (GhsCategoryOldSessionval != null && GhsCategoryOldSessionval.Count > 0)
                        {
                            foreach (var items in GhsCategoryOldSessionval)
                            {
                                SessionValues.Add(items);
                            }
                        }
                        if (ghstypeids != null && ghstypeids.Count > 0)
                        {
                            foreach (var items in ghstypeids)
                            {
                                SessionValues2.Add(items);
                            }
                        }
                        var NewValues = SessionValues.Union(SessionValues2);
                        var Newghstypeids = NewValues.ToList().ConvertAll(x => Convert.ToInt32(x)).AsEnumerable();
                        HttpContext.Current.Session[sessionGhsCategoryIds] = Newghstypeids;
                    }
                }
                else
                {
                    HttpContext.Current.Session[sessionGhsCategoryIds] = ghstypeids;
                }

                if (HttpContext.Current.Session[sessionFireIds] != null)
                {
                    List<int> SessionValues = new List<int>();
                    List<int> SessionValues2 = new List<int>();
                    List<int> OldsessionFireIds = (List<int>)HttpContext.Current.Session[sessionFireIds];
                    if (firetypeids != null && firetypeids.Count > 0)
                    {
                        if (OldsessionFireIds != null && OldsessionFireIds.Count > 0)
                        {
                            foreach (var items in OldsessionFireIds)
                            {
                                SessionValues.Add(items);
                            }
                        }
                        if (firetypeids != null && firetypeids.Count > 0)
                        {
                            foreach (var items in firetypeids)
                            {
                                SessionValues2.Add(items);
                            }
                        }
                        var NewValues = SessionValues.Union(SessionValues2);
                        var NewsessionFireIds = NewValues.ToList().ConvertAll(x => Convert.ToInt32(x)).AsEnumerable();
                        HttpContext.Current.Session.Add(sessionFireIds, NewsessionFireIds);
                    }
                }
                else
                {
                    HttpContext.Current.Session.Add(sessionFireIds, firetypeids);
                }

                if (HttpContext.Current.Session[sessionOfficeIds] != null)
                {
                    List<int> SessionValues = new List<int>();
                    List<int> SessionValues2 = new List<int>();
                    List<int> OldsessionOfficeIds = (List<int>)HttpContext.Current.Session[sessionOfficeIds];
                    if (officetypeids != null && officetypeids.Count > 0)
                    {
                        if (OldsessionOfficeIds != null && OldsessionOfficeIds.Count > 0)
                        {
                            foreach (var items in OldsessionOfficeIds)
                            {
                                SessionValues.Add(items);
                            }
                        }
                        if (officetypeids != null && officetypeids.Count > 0)
                        {
                            foreach (var items in officetypeids)
                            {
                                SessionValues2.Add(items);
                            }
                        }
                        var NewValues = SessionValues.Union(SessionValues2);
                        var NewsessionOfficeIds = NewValues.ToList().ConvertAll(x => Convert.ToInt32(x)).AsEnumerable();
                        HttpContext.Current.Session.Add(sessionOfficeIds, NewsessionOfficeIds);
                    }
                }
                else
                {
                    HttpContext.Current.Session.Add(sessionOfficeIds, officetypeids);
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(PROGRAM_ID, ex.ToString());
                throw ex;
            }
        }




        /// <summary>
        /// 化学物質、製品別名、含有物一覧、商品一覧から現在入力されている値を取得し、セッションに格納します。
        /// 2019/04/03 TPE.Rin Add
        /// 法規制、消防法、社内ルール、GHS分類から現在入力されている値を取得し、セッションに格納します。
        /// </summary>
        /// 

        /// <summary>
        /// Author: MSCGI
        /// Created date: 25-9-2019  
        /// Description :Get Inputed Data
        /// <summary>
        public void getInputData(CsmsCommonInfo ChemControlValues)
        {
            var chem = new ChemModel();
            List<ChemAliasModel> chemAliases = new List<ChemAliasModel>();
            List<ChemContainedModel> chemContaineds = new List<ChemContainedModel>();
            List<ChemProductModel> chemProdcuts = new List<ChemProductModel>();
            CsmsCommonInfo csmsCommonInfo = new CsmsCommonInfo();

            var regulation = new List<int>();
            var fire = new List<int>();
            var office = new List<int>();
            var ghs = new List<int>();
            {
                chem = ChemControlValues;
                setUploadedFiles(chem); //2018/8/17 Add FJ
            }
            HttpContext.Current.Session.Add(nameof(ChemModel), chem);


            if (HttpContext.Current.Session["sessionAliases"] != null)
            {
                List<ChemAliasModel> Chemalias = new List<ChemAliasModel>();
                Chemalias = (List<ChemAliasModel>)HttpContext.Current.Session["sessionAliases"];
                foreach (var ChemAlias in Chemalias)
                {
                    var data = new ChemAliasModel();
                    data.CHEM_NAME = ChemAlias.CHEM_NAME;
                    chemAliases.Add(data);
                }
            }

            HttpContext.Current.Session.Add(sessionAliases, chemAliases);
            HttpContext.Current.Session.Add("OldAliases", chemAliases);


            //for (int i = 1; i <= HttpContext.Current.Request.Form.Count; i++)
            //{
            //    var MAkerId = HttpContext.Current.Request.Form["ManufactureName" + i + ""];
            //    var productName = HttpContext.Current.Request.Form["ProductName" + i + ""];
            //    var Capacity = HttpContext.Current.Request.Form["Capacity" + i + ""];
            //    var SiglebyteCapacity = HttpContext.Current.Request.Form["SiglebyteCapacity" + i + ""];
            //    var Productbarcode = HttpContext.Current.Request.Form["Productbarcode" + i + ""];
            //    var sdslinkurl = HttpContext.Current.Request.Form["sdslinkurl" + i + ""];
            //    var sdsupdatedate = HttpContext.Current.Request.Form["sdsupdatedate" + i + ""];

            //    if (MAkerId != null)
            //    {
            //        var chemicalProduct = new ChemProductModel();
            //        chemicalProduct.MAKER_ID = Convert.ToInt32(MAkerId);
            //        chemicalProduct.PRODUCT_NAME = productName;
            //        chemicalProduct.UNITSIZE = Capacity;
            //        chemicalProduct.UNITSIZE_ID = Convert.ToInt32(SiglebyteCapacity);
            //        chemicalProduct.PRODUCT_BARCODE = Productbarcode;
            //        chemicalProduct.SDS_URL = sdslinkurl;
            //        if (!string.IsNullOrWhiteSpace(sdsupdatedate))
            //        {
            //            chemicalProduct.SDS_UPD_DATE = Convert.ToDateTime(sdsupdatedate);
            //        }
            //        else
            //        {
            //            chemicalProduct.SDS_UPD_DATE = null;
            //        }
            //        chemProdcuts.Add(chemicalProduct);
            //    }
            //}
            //HttpContext.Current.Session.Add(sessionProducts, chemProdcuts);


            if (HttpContext.Current.Session["ProductDetails"] != null)
            {

                List<ChemProductModel> chemProduct = new List<ChemProductModel>();
                chemProduct = (List<ChemProductModel>)HttpContext.Current.Session["ProductDetails"];
                if (chemProduct != null && chemProduct.Count > 0)
                {
                    foreach (ChemProductModel items in chemProduct)
                    {
                        var chemicalProduct = new ChemProductModel();
                        chemicalProduct.MAKER_ID = items.MAKER_ID;
                        chemicalProduct.PRODUCT_NAME = items.PRODUCT_NAME;
                        chemicalProduct.UNITSIZE = items.UNITSIZE;
                        chemicalProduct.UNITSIZE_ID = items.UNITSIZE_ID;
                        chemicalProduct.PRODUCT_BARCODE = items.PRODUCT_BARCODE;
                        chemicalProduct.SDS_URL = items.SDS_URL;
                        if (items.SDS_UPD_DATE != null)
                        {
                            chemicalProduct.SDS_UPD_DATE = Convert.ToDateTime(items.SDS_UPD_DATE);
                        }
                        else
                        {
                            chemicalProduct.SDS_UPD_DATE = null;
                        }
                        chemProdcuts.Add(chemicalProduct);
                    }
                }
                HttpContext.Current.Session.Add(sessionProducts, chemProdcuts);
            }

            if (HttpContext.Current.Session["Contained"] != null)
            {
                List<ChemContainedModel> ContainedModel = (List<ChemContainedModel>)HttpContext.Current.Session["Contained"];
            }


            if (HttpContext.Current.Session["RegulationModel"] != null)
            {
                List<V_REGULATION> vregulation = new List<V_REGULATION>();
                vregulation = (List<V_REGULATION>)HttpContext.Current.Session["RegulationModel"];
                if (vregulation.Count > 0)
                {
                    foreach (var row in vregulation)
                    {
                        var data = new V_REGULATION();

                        data.REG_TYPE_ID = int.Parse(row.REG_TYPE_ID.ToString());
                        if (row.INPUT_FLAG == "手入力")
                        {
                            data.INPUT_FLAG = "1";
                        }
                        else
                        {
                            data.INPUT_FLAG = "0";
                        }
                        gridreg.Add(data);
                        regulation.Add(data.REG_TYPE_ID);
                    }
                    HttpContext.Current.Session.Add(sessionRegulationIds, regulation);
                }
                else
                {
                    if (HttpContext.Current.Session[sessionRegulationIds] == null)
                    {
                        HttpContext.Current.Session.Add(sessionRegulationIds, null);
                    }
                }
            }
            else
            {
                if (HttpContext.Current.Session[sessionRegulationIds] == null)
                {
                    HttpContext.Current.Session.Add(sessionRegulationIds, null);
                }
            }

            if (HttpContext.Current.Session["FireServicesModel"] != null)
            {
                List<V_FIRE> vfire = new List<V_FIRE>();
                vfire = (List<V_FIRE>)HttpContext.Current.Session["FireServicesModel"];
                if (vfire.Count > 0)
                {
                    foreach (var row in vfire)
                    {
                        var data = new V_FIRE();

                        data.FIRE_ID = int.Parse(row.FIRE_ID.ToString());
                        if (row.INPUT_FLAG == "手入力")
                        {
                            data.INPUT_FLAG = "1";
                        }
                        else
                        {
                            data.INPUT_FLAG = "0";
                        }

                        gridfire.Add(data);
                        fire.Add(data.FIRE_ID);
                    }
                    HttpContext.Current.Session.Add(sessionFireIds, fire);
                }
                else
                {
                    if (HttpContext.Current.Session[sessionFireIds] == null)
                    {
                        HttpContext.Current.Session.Add(sessionFireIds, null);
                    }
                }
            }
            else
            {
                if (HttpContext.Current.Session[sessionFireIds] == null)
                {
                    HttpContext.Current.Session.Add(sessionFireIds, null);
                }

            }

            if (HttpContext.Current.Session["OfficeRulesmodel"] != null)
            {
                List<V_OFFICE> voffice = new List<V_OFFICE>();
                voffice = (List<V_OFFICE>)HttpContext.Current.Session["OfficeRulesmodel"];
                if (voffice.Count > 0)
                {
                    foreach (var row in voffice)
                    {
                        var data = new V_OFFICE();

                        data.OFFICE_ID = int.Parse(row.OFFICE_ID.ToString());
                        if (row.INPUT_FLAG == "手入力")
                        {
                            data.INPUT_FLAG = "1";
                        }
                        else
                        {
                            data.INPUT_FLAG = "0";
                        }

                        gridoffice.Add(data);
                        office.Add(data.OFFICE_ID);
                    }

                    HttpContext.Current.Session.Add(sessionOfficeIds, office);

                }
                else
                {
                    if (HttpContext.Current.Session[sessionOfficeIds] == null)
                    {
                        HttpContext.Current.Session.Add(sessionOfficeIds, null);
                    }

                }
            }

            else
            {
                if (HttpContext.Current.Session[sessionOfficeIds] == null)
                {
                    HttpContext.Current.Session.Add(sessionOfficeIds, null);
                }
            }

            if (HttpContext.Current.Session["GHSModel"] != null)
            {
                List<V_GHSCAT> vghs = new List<V_GHSCAT>();
                vghs = (List<V_GHSCAT>)HttpContext.Current.Session["GHSModel"];
                if (vghs.Count > 0)
                {
                    foreach (var row in vghs)
                    {
                        var data = new V_GHSCAT();

                        data.GHSCAT_ID = int.Parse(row.GHSCAT_ID.ToString());
                        if (row.INPUT_FLAG == "手入力")
                        {
                            data.INPUT_FLAG = "1";
                        }
                        else
                        {
                            data.INPUT_FLAG = "0";
                        }

                        gridghs.Add(data);
                        ghs.Add(data.GHSCAT_ID);
                    }
                    HttpContext.Current.Session.Add(sessionGhsCategoryIds, ghs);
                }
                else
                {
                    if (HttpContext.Current.Session[sessionGhsCategoryIds] == null)
                    {
                        HttpContext.Current.Session.Add(sessionGhsCategoryIds, null);
                    }
                }
            }
            else
            {
                if (HttpContext.Current.Session[sessionGhsCategoryIds] == null)
                {
                    HttpContext.Current.Session.Add(sessionGhsCategoryIds, null);
                }

            }
        }

        /// <summary>
        /// SDSに入力されている値を取得し、セッションに格納します。 //2018/8/17 Add FJ
        /// </summary>
        /// <param name="ledger"></param>



        /// <summary>
        /// Author: MSCGI
        /// Created date: 25-9-2019  
        /// Description :its used to upload the file
        /// <summary>
        protected void setUploadedFiles(ChemModel chem)
        {
            //var SDS_ID = (HiddenField)MaintenanceFormView.FindControl("SDS_ID");
            //if (SDS_ID.Value != "")
            //{
            //    //var SDS_FILENAME = (TextBox)MaintenanceFormView.FindControl("SDS_FILE");
            //    SDS_FILENAME.ReadOnly = false;
            //    var fileInfo = Global.UploadFiles[SDS_ID.Value];
            //    var stream = fileInfo.File.OpenRead();
            //    chem.SDS = new byte[stream.Length];
            //    stream.Read(chem.SDS, 0, System.Convert.ToInt32(stream.Length));
            //    stream.Close();
            //    chem.SDS_MIMETYPE = MimeMapping.GetMimeMapping(fileInfo.OriginalFileName);
            //    chem.SDS_FILENAME = fileInfo.OriginalFileName;
            //    SDS_FILENAME.Text = chem.SDS_FILENAME;
            //    SDS_FILENAME.ReadOnly = true;
            //}
            //else
            //{
            //    var condition = new ChemSearchModel();
            //    condition.CHEM_CD = id;
            //    condition.CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching;
            //    condition.USAGE_STATUS = (int)UsageStatusCondtionType.All;
            //    if (condition.CHEM_CD != null)
            //    {
            //        var d = new ChemDataInfo();
            //        var chemOld = d.GetSearchResult(condition).Single();
            //        d.Dispose();

            //        if (chemOld.SDS != null)
            //        {
            //            chem.SDS = chemOld.SDS;
            //            chem.SDS_MIMETYPE = chemOld.SDS_MIMETYPE;
            //            chem.SDS_FILENAME = chemOld.SDS_FILENAME;
            //        }
            //    }
            //}
        }

    }
}