﻿using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Ledger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ZyCoaG;

namespace ChemMgmt.Repository
{
    public interface IMangementLedgerRepo : IDisposable
    {
        List<LedgerCommonInfo> searchEntity(LedgerSearchModel objLedgerCommonInfo);

        void ListOutput(LedgerSearchModel condition);

        IQueryable<LedgerCommonInfo> SearchExecute(LedgerSearchModel objLedgerCommonInfo);

        LedgerCommonInfo GetLedgerInfo(LedgerSearchModel model, TableType? type, Mode mode);

        List<string> Regain(LedgerCommonInfo objLedgerCommonInfo);

        List<string> Register(LedgerCommonInfo objLedgerCommonInfo, Mode? mode);

        List<string> Update(LedgerCommonInfo objLedgerCommonInfo, Mode? mode);

        List<string> Withdraw(LedgerCommonInfo objLedgerCommonInfo);

        List<string> Abolish(LedgerCommonInfo objLedgerCommonInfo);

        List<string> Remand(LedgerCommonInfo objLedgerCommonInfo);

        List<string> TemporaryRegister(LedgerCommonInfo objLedgerCommonInfo, Mode? mode);

        LedgerCommonInfo ClientsideValidation(LedgerCommonInfo objLedgerCommonInfo);

        void getInputData(LedgerCommonInfo objLedgerCommonInfo);

        LedgerCommonInfo EnablingDisblingButtons(Mode mode, int? LEDGER_FLOW_ID, string CHEM_CD, int? HIERARCHY, string APPROVAL_USER_CD, string SUBSITUTE_USER_CD, string User_CD, string ADMIN_FLAG);

        CommonDataModel<string> GroupList();

        IQueryable<LedgerProtectorModel> GetInputedProtectors(IEnumerable<LedgerProtectorModel> protectors);

        IQueryable<LedgerRiskReduceModel> GetInputedRiskReduces(IEnumerable<LedgerRiskReduceModel> riskReduces);

        void RiskBeforeEvaluate(LedgerModel objLedgerModel, bool flg);

        void ClearSession();

        string getchemoerationchiefname(string CHEM_OPERATION_CHIEF);
    }
}
