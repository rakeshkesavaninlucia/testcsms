﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.Extensions;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Flow;
using ChemMgmt.Nikon.History;
using ChemMgmt.Nikon.Ledger;
using IdentityManagement.Entities;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using ZyCoaG;
using ZyCoaG.BaseCommon;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.History;

namespace ChemMgmt.Repository
{
    public class MangementLedegerDao : NikonSearchPageBase, IMangementLedgerRepo
    {

        private int? id { get; set; }
        private string regno { get; set; }

        public string MEMO { get; set; }

        protected Mode mode { get; set; }

        /// <summary>
        /// メンテナンス対象の識別情報を取得または設定します。
        /// </summary>
        private string rowId { get; set; }


        private const string sessionOldRegisterNumbers = "OldRegisterNumbers";
        /// <summary>
        /// セッション変数に格納する使用場所一覧の名前を定義します。
        /// </summary>
        public const string sessionUsageLocations = "UsageLocations";

        /// <summary>
        /// 更新後に表示する名称を格納するセッション名を取得します。
        /// </summary>
        private string sessionAfterUpdateType { get; } = "AfterUpdateType";

        /// <summary>
        /// セッション変数に格納する化学物質の名前を定義します。
        /// </summary>
        private const string sessionContaineds = "Containeds";

        /// <summary>
        /// セッション変数に格納する法規制(画面表示用)の名前を定義します。
        /// </summary>
        private const string sessionRegulations = "Regulations";
        /// <summary>
        /// セッション変数に格納する法規制ID's(画面表示用)の名前を定義します。
        /// </summary>
        private const string sessionRegTypeIds = "RegTypeIds";

        /// <summary>
        /// セッション変数に格納する化学物質に紐付く法規制ID's(画面表示用でない)の名前を定義します。
        /// </summary>
        private const string sessionChemRegTypeIds = "ChemRegTypeIds";

        /// <summary>
        /// セッション変数に格納する化学物質に紐付く法規制(画面表示用でない)の名前を定義します。
        /// </summary>
        private const string sessionChemRegulations = "ChemRegulations";

        /// <summary>
        /// リスク低減策のセッション名を定義します。
        /// </summary>
        private const string sessionRiskReduces = "RiskReduces";

        /// <summary>
        /// セッション変数に格納する保管場所一覧の名前を定義します。
        /// </summary>
        private const string sessionStoringLocations = "StoringLocations";

        /// <summary>
        /// 保護具'sのセッション名を定義します。
        /// </summary>
        private const string sessionProtectors = "Protectors";

        /// <summary>
        /// 更新の種類
        /// </summary>
        public enum UpdateType
        {
            /// <summary>
            /// 登録
            /// </summary>
            Register,

            /// <summary>
            /// 更新
            /// </summary>
            Update,

            ///// <summary>
            ///// 抹消
            ///// </summary>
            //Remove,

            ///// <summary>
            ///// 抹消取消
            ///// </summary>
            //UndoRemove,

            /// <summary>
            /// 一時保存 (登録)
            /// </summary>
            TemporaryRegister,

            /// <summary>
            /// 一時保存 (更新)
            /// </summary>
            TemporaryUpdate,

            /// <summary>
            /// 廃止申請
            /// </summary>
            Abolition,

            /// <summary>
            /// 取り下げ 
            /// </summary>
            Withdraw,

            /// <summary>
            /// 取戻
            /// </summary>
            Regain,

            /// <summary>
            /// 差し戻し
            /// </summary>
            Remand,

            /// <summary>
            /// 承認
            /// </summary>
            Approval,

        }

        /// <summary>
        /// リスト出力時のZIPファイル名を取得します。
        /// </summary>
        protected string ZipvFileName { get; } = "管理台帳検索結果_{0:yyyyMMddHHmmss}.zip";

        protected override int GetMaxSearchResultCount() => new SystemParameters().MaxChemResultCount;

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public List<LedgerCommonInfo> searchEntity(LedgerSearchModel objLedgerCommonInfo)
        {
            //IsSearchButton = true;
            //ResultGrid.Sort(null, SortDirection.Ascending);

            getInputData(objLedgerCommonInfo);

            //SearchFormView.DataBind();
            return GetSearchResult(objLedgerCommonInfo);
        }
        public void getInputData(LedgerSearchModel objLedgerCommonInfo)
        {
            //var condition = new LedgerSearchModel();
            objLedgerCommonInfo.SearchinputList = objLedgerCommonInfo;
            if (objLedgerCommonInfo.ApplicationNo != null)
            {

                if (objLedgerCommonInfo.ApplicationNo != null)
                {
                    objLedgerCommonInfo.LEDGER_FLOW_ID = Convert.ToInt32(objLedgerCommonInfo.ApplicationNo.TrimStart(new Char[] { '0' }));

                }
                else
                {
                    objLedgerCommonInfo.LEDGER_FLOW_ID = -1;
                }
            }
            HttpContext.Current.Session.Add(nameof(LedgerCommonInfo), objLedgerCommonInfo);
        }

        /// <summary>
        /// 検索結果のSelectMethodで検索結果を取得します。
        /// </summary>
        /// <param name="condition">セッションに格納されている検索条件を抽出します。</param>
        /// <returns>検索結果を取得します。</returns>
        public List<LedgerCommonInfo> GetSearchResult(LedgerSearchModel objLedgerSearchModel)
        {
            List<int> count;
            if (objLedgerSearchModel != null)
            {
                var user = (UserInformation)(HttpContext.Current.Session["LoginUserSession"]);
                if (user != null && user.GROUP_CD != null)
                {
                    objLedgerSearchModel.USER_ADMINFLG = user.ADMIN_FLAG;
                    objLedgerSearchModel.REFERENCE_AUTHORITY = user.REFERENCE_AUTHORITY;
                    objLedgerSearchModel.LOGIN_USER_CD = user.User_CD;
                }
                var result = new List<LedgerCommonInfo>();

                var workCount = 0;
                using (var dataInfo = new LedgerWorkDataInfo(2))
                {
                    objLedgerSearchModel.CHEM_NAME = objLedgerSearchModel.CHEM_NAME.UnityWidth();
                    count = (List<int>)dataInfo.lstGetSearchResultCount(objLedgerSearchModel);
                    HttpContext.Current.Session["totalMaxSearchResultCount"] = count[0];
                    foreach (var intcount in count)
                    {
                        workCount = intcount;
                    }

                    if (!IsListOutputProcess)
                    {
                        MaxSearchResultCount = GetMaxSearchResultCount();
                        objLedgerSearchModel.MaxSearchResult = MaxSearchResultCount;
                        HttpContext.Current.Session["MaxSearchCount"] = MaxSearchResultCount;
                        if (objLedgerSearchModel.LEDGER_STATUS == (int)LedgerStatusConditionType.Applying)
                        {
                            ViewSearchResultCount(workCount);
                        }
                        if (IsMaxSerchResultCountOver)
                        {
                            SearchConditionCsv = dataInfo.Csv;
                            return new List<LedgerCommonInfo>().AsQueryable().ToList();
                        }
                    }
                    else
                    {
                        objLedgerSearchModel.MaxSearchResult = null;
                    }
                    if (workCount > 0)
                    {
                        result.AddRange(dataInfo.GetSearchResult(objLedgerSearchModel, false));
                    }
                    SearchConditionCsv = dataInfo.Csv;
                }
                if (objLedgerSearchModel.LEDGER_STATUS != (int)LedgerStatusConditionType.Applying)
                {
                    //using (var dataInfo = new LedgerDataInfo())
                    List<int> intcount;
                    var searchResultCount = 0;
                    using (var dataInfo = new LedgerDataInfo(2))
                    {
                        objLedgerSearchModel.CHEM_NAME = objLedgerSearchModel.CHEM_NAME.UnityWidth();
                        objLedgerSearchModel.HasExcludeOverlap = true;

                        intcount = (List<int>)dataInfo.lstGetSearchResultCount(objLedgerSearchModel);
                        var totalcount = intcount[0] + count[0];
                        HttpContext.Current.Session["totalMaxSearchResultCount"] = totalcount;
                        foreach (var entry in intcount)
                        {
                            searchResultCount = entry;
                        }

                        if (!IsListOutputProcess)
                        {
                            objLedgerSearchModel.MaxSearchResult = MaxSearchResultCount;
                            ViewSearchResultCount(workCount + searchResultCount);
                            if (IsMaxSerchResultCountOver)
                            {
                                SearchConditionCsv = dataInfo.Csv;
                                return new List<LedgerCommonInfo>().AsQueryable().ToList();
                            }
                            objLedgerSearchModel.MaxSearchResult = MaxSearchResultCount - workCount;
                        }
                        else
                        {
                            objLedgerSearchModel.MaxSearchResult = null;
                        }
                        if (searchResultCount > 0)
                        {
                            result.AddRange(dataInfo.GetSearchResult(objLedgerSearchModel, false));
                        }
                        if (workCount == 0)
                        {
                            SearchConditionCsv = dataInfo.Csv;
                        }
                    }
                }
                ViewSearchResultCount(result.Count);
                return result.OrderBy(x => x.REG_NO).AsQueryable().ToList();
            }
            ViewSearchResultCount(0);
            return new List<LedgerCommonInfo>().AsQueryable().ToList();
        }

        /// <summary>
        /// <para>リスト出力ボタンクリックイベントです。</para>
        /// <para>検索結果をCSVファイルに出力します。</para>
        /// </summary>
        public void ListOutput(LedgerSearchModel condition)
        {
            List<LedgerCommonInfo> LedgerCommonInfo = new List<LedgerCommonInfo>();
            LedgerCommonInfo LedgerCommonInfo1 = new LedgerCommonInfo();
            IsListOutputProcess = true;
            var enc = Encoding.GetEncoding("shift_jis");
            var folder = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
            Directory.CreateDirectory(folder);
            DateTime dt = DateTime.Now;
            var kensan = Path.Combine(folder, string.Format("管理台帳検索結果(検索結果一覧)_{0:yyyyMMddHHmmss}.csv", dt));
            var hokan = Path.Combine(folder, string.Format("管理台帳検索結果(保管場所一覧)_{0:yyyyMMddHHmmss}.csv", dt));
            var siyo = Path.Combine(folder, string.Format("管理台帳検索結果(使用場所一覧)_{0:yyyyMMddHHmmss}.csv", dt));
            var csvs = new string[] { kensan, hokan, siyo };

            if (condition.ApplicationNo != null)
            {
                condition.LEDGER_FLOW_ID = Convert.ToInt32(condition.ApplicationNo);
            }
            LedgerCommonInfo = GetSearchResult(condition);

            if (!string.IsNullOrEmpty(SearchConditionCsv))
            {
                using (var csv = new StreamWriter(kensan, false, enc))
                {
                    csv.Write(SearchConditionCsv);
                    csv.Close();
                }
            }
            Classes.Extensions.GridViewExtension.OutputCSV(LedgerCommonInfo, kensan, 1);

            {
                condition.LOC_BASE = true;
                LedgerCommonInfo = GetSearchResult(condition);

                if (!string.IsNullOrEmpty(SearchConditionCsv))
                {
                    using (var csv = new StreamWriter(hokan, false, enc))
                    {
                        csv.Write(SearchConditionCsv);
                        csv.Close();
                    }
                }
                Classes.Extensions.GridViewExtension.OutputCSV(LedgerCommonInfo, hokan, 2);
                condition.LOC_BASE = false;
            }

            {
                condition.USAGE_LOC_BASE = true;
                LedgerCommonInfo = GetSearchResult(condition);

                if (!string.IsNullOrEmpty(SearchConditionCsv))
                {
                    using (var csv = new StreamWriter(siyo, false, enc))
                    {
                        csv.Write(SearchConditionCsv);
                        csv.Close();
                    }
                }
                Classes.Extensions.GridViewExtension.OutputCSV(LedgerCommonInfo, siyo, 3);
                condition.USAGE_LOC_BASE = false;
            }
            string zipFileFullPath = folder + @"\" + string.Format(ZipvFileName, DateTime.Now.ToString("yyyyMMddHHmmss"));

            using (ZipArchive archive = ZipFile.Open(zipFileFullPath, ZipArchiveMode.Create, enc))
            {
                foreach (var c in csvs)
                {
                    archive.CreateEntryFromFile(c, Path.GetFileName(c));
                }
            }
            Control.FileDownload(zipFileFullPath, string.Format(ZipvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
            //2018/10/05 FJ Upd Wait処理対応
        }

        public IQueryable<LedgerCommonInfo> SearchExecute(LedgerSearchModel objLedgerCommonInfo)
        {
            IsNoCheckSearch = true;
            return searchEntity1(objLedgerCommonInfo);
        }


        /// <summary>
        /// 検索処理の実体です。
        /// </summary>
        private IQueryable<LedgerCommonInfo> searchEntity1(LedgerSearchModel objLedgerCommonInfo)
        {
            IsSearchButton = true;
            return GetSearchResult1(objLedgerCommonInfo);
        }

        /// <summary>
        /// 検索結果のSelectMethodで検索結果を取得します。
        /// </summary>
        /// <param name="condition">セッションに格納されている検索条件を抽出します。</param>
        /// <returns>検索結果を取得します。</returns>
        public IQueryable<LedgerCommonInfo> GetSearchResult1(LedgerSearchModel condition)
        {
            try
            {
                SearchConditionCsv = string.Empty;
                using (var dataInfo = new LedgerWorkDataInfo(1))
                {
                    UserInformation user = (UserInformation)HttpContext.Current.Session["LoginUserSession"];
                    var machiCount = dataInfo.WaitingCount(user.User_CD);
                    var flowCount = dataInfo.OnWorkflowCount(user.User_CD);
                    HttpContext.Current.Session["machiCount"] = machiCount;
                    HttpContext.Current.Session["flowCount"] = flowCount;

                    var searchResultCount = (condition.FLOW_SEARCH_TARGET == 0 ? machiCount : flowCount);

                    if (!IsListOutputProcess)
                    {
                        ViewSearchResultCount(searchResultCount);
                        condition.MaxSearchResult = MaxSearchResultCount;

                        if (IsMaxSerchResultCountOver)
                        {
                            return new List<LedgerCommonInfo>().AsQueryable();
                        }
                    }
                    else
                    {
                        condition.MaxSearchResult = null;
                    }
                    if (searchResultCount > 0)
                    {
                        SearchConditionCsv = dataInfo.Csv;
                        if (condition.FLOW_SEARCH_TARGET == 0)
                        {
                            //return dataInfo.GetWaitingModel(userInfo.USER_CD);
                            return dataInfo.GetWaitingList(user.User_CD); //レスポンス改善用
                        }
                        else
                        {
                            //return dataInfo.GetOnWorkflowModel(userInfo.USER_CD);
                            return dataInfo.GetOnWorkflowList(user.User_CD); //レスポンス改善用
                        }

                    }
                }

                ViewSearchResultCount(0);
                return new List<LedgerCommonInfo>().AsQueryable();
            }
            catch (Exception ex)
            {

                throw ex;
            }

        }

        /// <summary>
        /// 編集項目のSelectMethodで編集対象を取得します。
        /// </summary>
        /// <param name="model">セッションに格納されている化学物質情報を抽出します。</param>
        /// <returns>編集対象を取得します。</returns>
        public LedgerCommonInfo GetLedgerInfo(LedgerSearchModel model, TableType? type, Mode mode)
        {
            UserInformation loginModel = (UserInformation)HttpContext.Current.Session["LoginUserSession"];
            if (mode == Mode.Copy && model.REG_NO == null)
            {
                if (HttpContext.Current.Session[nameof(LedgerCommonInfo)] != null)
                {
                    return (LedgerCommonInfo)HttpContext.Current.Session[nameof(LedgerCommonInfo)];
                }
            }

            regno = model.REG_NO;
            id = model.LEDGER_FLOW_ID;
            if (regno != null)
            {
                var condition = new LedgerSearchModel();
                condition.REG_NO = regno;
                condition.REG_NO_CONDITION = (int)SearchConditionType.PerfectMatching;
                condition.LEDGER_STATUS = (int)LedgerStatusConditionType.All;
                var d = new LedgerDataInfo();
                var ledger = d.GetSearchResult(condition).FirstOrDefault();
                d.Dispose();
                ledger.Mode = mode;
                if (mode == Mode.Edit)
                {
                    ledger.APPLI_CLASS = (int)AppliClass.Edit;
                }
                else if (mode == Mode.Abolition)
                {
                    ledger.APPLI_CLASS = (int)AppliClass.Stopped;
                }
                HttpContext.Current.Session.Add(nameof(LedgerCommonInfo), ledger);
                return ledger;
            }
            else if (id != null)
            {
                if (type == TableType.History)
                {
                    var condition = new LedgerSearchModel();
                    condition.LEDGER_FLOW_ID = id;
                    var d = new LedgerHistoryDataInfo();
                    var ledgers = d.GetSearchResult(condition);
                    d.Dispose();
                    LedgerCommonInfo ledger;
                    if (ledgers.Count() == 0)
                    {
                        var work = new LedgerWorkDataInfo();
                        ledger = work.GetSearchResult(condition).FirstOrDefault();
                        type = TableType.Work;
                    }
                    else
                    {
                        ledger = ledgers.Single();
                    }
                    ledger.Mode = mode;
                    HttpContext.Current.Session.Add(nameof(LedgerCommonInfo), ledger);
                    return ledger;
                }
                else
                {
                    var condition = new LedgerSearchModel();
                    condition.LEDGER_FLOW_ID = id;
                    var d = new LedgerWorkDataInfo();
                    var ledger = d.GetSearchResult(condition).FirstOrDefault();
                    d.Dispose();
                    ledger.Mode = mode;
                    if (mode == Mode.Copy)
                    {
                        ledger.LEDGER_FLOW_ID = null;
                    }
                    HttpContext.Current.Session.Add(nameof(LedgerCommonInfo), ledger);
                    return ledger;
                }
            }

            var groupId = loginModel.GROUP_CD;
            var newledger = new LedgerCommonInfo()
            {
                Mode = mode,
                APPLI_CLASS = (int)AppliClass.New,
                APPLICANT = loginModel.UserName,
                GROUP_CD = groupId,
                GROUP_NAME = GetGroupList().Where(x => x.Id == groupId).FirstOrDefault()?.Name,
                NID = loginModel.User_CD
            };
            HttpContext.Current.Session.Add(nameof(LedgerCommonInfo), newledger);
            return newledger;
        }


        /// <summary>
        /// <para>登録ボタンの処理です。</para>
        /// <para>画面に表示されている化学物質管理台帳情報を登録します。</para>
        /// </summary>
        public List<string> Register(LedgerCommonInfo objLedgerCommonInfo, Mode? mode)
        {
            List<string> alert = DoUpdateForProcess(UpdateType.Register, objLedgerCommonInfo, mode);
            return alert;
        }

        public List<string> Update(LedgerCommonInfo objLedgerCommonInfo, Mode? mode)
        {
            List<string> alert = DoUpdateForProcess(UpdateType.Update, objLedgerCommonInfo, mode);
            return alert;
        }

        public List<string> Withdraw(LedgerCommonInfo objLedgerCommonInfo)
        {
            mode = (Mode)5;
            List<string> alert = DoUpdateForProcess(UpdateType.Withdraw, objLedgerCommonInfo, mode);
            return alert;
        }
        public List<string> Abolish(LedgerCommonInfo objLedgerCommonInfo)
        {
            mode = (Mode)4;
            List<string> alert = DoUpdateForProcess(UpdateType.Abolition, objLedgerCommonInfo, mode);
            return alert;
        }
        public List<string> Remand(LedgerCommonInfo objLedgerCommonInfo)
        {
            mode = (Mode)7;
            List<string> alert = DoUpdateForProcess(UpdateType.Remand, objLedgerCommonInfo, mode);
            return alert;
        }

        public List<string> Regain(LedgerCommonInfo objLedgerCommonInfo)
        {
            mode = (Mode)6;
            List<string> errormessage = DoUpdateForProcess(UpdateType.Regain, objLedgerCommonInfo, mode);
            return errormessage;
        }

        /// <summary>
        /// <para>一時保存(登録)ボタンの処理です。</para>
        /// <para>画面に表示されている化学物質管理台帳情報を登録します。</para>
        /// </summary>
        public List<string> TemporaryRegister(LedgerCommonInfo objLedgerCommonInfo, Mode? mode)
        {
            List<string> alert = DoUpdateForProcess(UpdateType.TemporaryRegister, objLedgerCommonInfo, mode);
            return alert;
        }





        /// <summary>
        /// <paramref name="updateType"/>に応じた更新処理を行います。
        /// </summary>
        /// <param name="updateType">更新の種類。</param>
        public List<string> DoUpdateForProcess(UpdateType updateType, LedgerCommonInfo objLedgerCommonInfo, Mode? mode)
        {
            //start
            //CutomeStopWatch StopWatch = new CutomeStopWatch();
            //StopWatch.Start();
            List<string> OutputWarningMessage = new List<string>();
            var ledger = (LedgerCommonInfo)HttpContext.Current.Session[nameof(LedgerCommonInfo)];
            var context = DbUtil.Open(true);
            SqlConnection sqlcontext = DbUtil.Open();
            try
            {
                using (var ledgerWorkDataInfo = new LedgerWorkDataInfo() { Context = context })
                {
                    if (ledger.BEFORE_WORKFLOW_RESULT != (int)WorkflowResult.Remand &&
                        ledger.BEFORE_WORKFLOW_RESULT != (int)WorkflowResult.Regained &&
                        updateType == UpdateType.Update ||
                        updateType == UpdateType.Abolition)
                    {
                        if (!ledgerWorkDataInfo.IsInfoSyncFromDB(ledger, true))
                        {
                            OutputWarningMessage = new List<string> { WarningMessages.AllreadyOperated };
                            return OutputWarningMessage;
                        }
                    }
                    else if (ledger.BEFORE_WORKFLOW_RESULT == (int)WorkflowResult.Remand ||
                        updateType == UpdateType.Regain ||
                        updateType == UpdateType.Remand)
                    {

                        if (!ledgerWorkDataInfo.IsInfoSyncFromDB(ledger))
                        {
                            OutputWarningMessage = new List<string> { WarningMessages.AllreadyOperated };
                            return OutputWarningMessage;
                        }
                    }
                    UserInformation userInfo = (UserInformation)HttpContext.Current.Session["LoginUserSession"];
                    ledger.REG_USER_CD = userInfo.User_CD;
                    var oldRegisterNumbers = (List<LedgerOldRegisterNumberModel>)HttpContext.Current.Session["sessionOldRegisterNumbers"];
                    if (oldRegisterNumbers == null)
                    {
                        oldRegisterNumbers = new List<LedgerOldRegisterNumberModel>();
                    }
                    var regulations = (List<LedgerRegulationModel>)HttpContext.Current.Session[sessionRegulations];
                    if (regulations == null)
                    {
                        regulations = new List<LedgerRegulationModel>();
                    }
                    var storingLocations = (List<LedgerLocationModel>)HttpContext.Current.Session["StoringLocationDetails"];
                    if (storingLocations == null)
                    {
                        storingLocations = new List<LedgerLocationModel>();
                    }
                    var usageLocations = (IEnumerable<LedgerUsageLocationModel>)HttpContext.Current.Session[sessionUsageLocations];
                    if (usageLocations == null)
                    {
                        usageLocations = new List<LedgerUsageLocationModel>();
                    }
                    ledger.CHEM_NAME = ledger.CHEM_NAME.UnityWidth();
                    ledger.TEMP_SAVED_FLAG = 0;
                    //StopWatch.Stop();

                    //DateTime StartTime1 = Convert.ToDateTime(StopWatch.StartAt);
                    //DateTime EndDtime1 = Convert.ToDateTime(StopWatch.EndAt);
                    //decimal TotalTimeTaken1 = Convert.ToDecimal(StopWatch.ElapsedMilliseconds / 1000.0);

                    //string todatDate1 = DateTime.Now.ToShortDateString();
                    //string path1 = System.Web.HttpContext.Current.Server.MapPath("~/Txt/" + todatDate1 + ".txt");
                    //FileInfo LogFile1 = new FileInfo(path1);

                    //if (!LogFile1.Exists)
                    //{
                    //    using (StreamWriter streamWriter = LogFile1.CreateText())
                    //    {
                    //        streamWriter.WriteLine("Register1==> StartTime:" + StartTime1 + ",EndTime:" + EndDtime1 + ",Total Time(Sec):" + TotalTimeTaken1 + "");
                    //    }
                    //}
                    //else
                    //{
                    //    using (StreamWriter streamWriter = new StreamWriter(path1, true))
                    //    {
                    //        streamWriter.WriteLine("Register1==> StartTime:" + StartTime1 + ",EndTime:" + EndDtime1 + ",Total Time(Sec):" + TotalTimeTaken1 + "");
                    //    }
                    //}


                    //end
                   
                    switch (updateType)
                    {
                        case UpdateType.Register:
                            //start
                            //StopWatch.Start();
                            ledger.FLOW_CD = userInfo.FLOW_CD;
                            ledger.TEMP_SAVED_FLAG = 0;
                            ledger.HIERARCHY = -1;

                            if (!ledger.LEDGER_FLOW_ID.HasValue || mode == Mode.Copy)
                            {
                                ledgerWorkDataInfo.AddWithChildren(ledger,
                                    oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                    storingLocations.Where(x => x.LOC_ID != null).ToList(), usageLocations);
                                objLedgerCommonInfo.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                            }
                            else
                            {
                                ledgerWorkDataInfo.SaveWithChildren(ledger,
                                    oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                    storingLocations.Where(x => x.LOC_ID != null).ToList(), usageLocations);
                                objLedgerCommonInfo.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                            }
                            if (!ledgerWorkDataInfo.IsValid)
                            {
                                var data = ledgerWorkDataInfo.ErrorMessages;
                                OutputWarningMessage = new List<string>(data);
                                return OutputWarningMessage;
                            }
                            //end
                            //StopWatch.Stop();

                            //DateTime StartTime2 = Convert.ToDateTime(StopWatch.StartAt);
                            //DateTime EndDtime2 = Convert.ToDateTime(StopWatch.EndAt);
                            //decimal TotalTimeTaken2 = Convert.ToDecimal(StopWatch.ElapsedMilliseconds / 1000.0);

                            //string todatDate2 = DateTime.Now.ToShortDateString();
                            //string path2 = System.Web.HttpContext.Current.Server.MapPath("~/Txt/" + todatDate2 + ".txt");
                            //FileInfo LogFile2 = new FileInfo(path2);

                            //if (!LogFile2.Exists)
                            //{
                            //    using (StreamWriter streamWriter = LogFile2.CreateText())
                            //    {
                            //        streamWriter.WriteLine("Register2==> StartTime:" + StartTime2 + ",EndTime:" + EndDtime2 + ",Total Time(Sec):" + TotalTimeTaken2 + "");
                            //    }
                            //}
                            //else
                            //{
                            //    using (StreamWriter streamWriter = new StreamWriter(path2, true))
                            //    {
                            //        streamWriter.WriteLine("Register2==> StartTime:" + StartTime2 + ",EndTime:" + EndDtime2 + ",Total Time(Sec):" + TotalTimeTaken2 + "");
                            //    }
                            //}
                            //start
                            //////StopWatch.Start();
                            using (var historyDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
                            {
                                bool flgMod = false;
                                if (mode == Mode.Edit)
                                {
                                    flgMod = true;
                                }
                                historyDataInfo.Application(ledger.LEDGER_FLOW_ID, userInfo.User_CD, flgMod);
                                if (!historyDataInfo.IsValid)
                                {
                                    var data = historyDataInfo.ErrorMessages;
                                    OutputWarningMessage = new List<string>(data);
                                    return OutputWarningMessage;
                                }
                            }
                            context.Commit();
                            {
                                var mail = new WorkflowMail();
                                LedgerModel obj = new LedgerModel();
                                obj.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                                obj.REG_NO = ledger.REG_NO;
                                obj.CHEM_NAME = ledger.CHEM_NAME;
                                //obj.APPLI_NO = ledger.APPLI_NO;
                                mail.SendMail(ledger);
                                
                            }
                            //end
                            //StopWatch.Stop();

                            //DateTime StartTime3 = Convert.ToDateTime(StopWatch.StartAt);
                            //DateTime EndDtime3 = Convert.ToDateTime(StopWatch.EndAt);
                            //decimal TotalTimeTaken3 = Convert.ToDecimal(StopWatch.ElapsedMilliseconds / 1000.0);

                            //string todatDate3 = DateTime.Now.ToShortDateString();
                            //string path3 = System.Web.HttpContext.Current.Server.MapPath("~/Txt/" + todatDate1 + ".txt");
                            //FileInfo LogFile3 = new FileInfo(path1);

                            //if (!LogFile3.Exists)
                            //{
                            //    using (StreamWriter streamWriter = LogFile3.CreateText())
                            //    {
                            //        streamWriter.WriteLine("Register3==> StartTime:" + StartTime3 + ",EndTime:" + EndDtime3 + ",Total Time(Sec):" + TotalTimeTaken3 + "");
                            //    }
                            //}
                            //else
                            //{
                            //    using (StreamWriter streamWriter = new StreamWriter(path1, true))
                            //    {
                            //        streamWriter.WriteLine("Register3==> StartTime:" + StartTime3 + ",EndTime:" + EndDtime3 + ",Total Time(Sec):" + TotalTimeTaken3 + "");
                            //    }
                            //}
                            break;

                        case UpdateType.TemporaryRegister:
                            ledger.FLOW_CD = userInfo.FLOW_CD;
                            ledger.TEMP_SAVED_FLAG = 1;

                            if (!ledger.LEDGER_FLOW_ID.HasValue || mode == Mode.Copy || mode == Mode.Edit || mode == Mode.Abolition)
                            {
                                ledgerWorkDataInfo.IsValidateUseAdd = false;
                                ledgerWorkDataInfo.AddWithChildren(ledger,
                                    oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                    storingLocations.Where(x => x.LOC_ID != null).ToList(), usageLocations);
                                objLedgerCommonInfo.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                            }
                            else
                            {
                                ledgerWorkDataInfo.IsValidateUseAdd = false;
                                ledgerWorkDataInfo.IsValidateUseSave = false;
                                ledgerWorkDataInfo.SaveWithChildren(ledger,
                                    oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                    storingLocations.Where(x => x.LOC_ID != null).ToList(), usageLocations);
                                objLedgerCommonInfo.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                            }
                            if (!ledgerWorkDataInfo.IsValid)
                            {
                                var data = ledgerWorkDataInfo.ErrorMessages;
                                OutputWarningMessage = new List<string>(data);
                                return OutputWarningMessage;
                            }
                            using (var historyDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
                            {
                                historyDataInfo.Temporary(ledger.LEDGER_FLOW_ID, userInfo.User_CD);
                                if (!historyDataInfo.IsValid)
                                {
                                    var data = historyDataInfo.ErrorMessages;
                                    OutputWarningMessage = new List<string>(data);
                                    return OutputWarningMessage;
                                }
                            }
                            context.Commit();
                            break;

                        case UpdateType.Update:
                            ledger.FLOW_CD = userInfo.FLOW_CD;
                            ledger.HIERARCHY = -1;
                            if (ledger.TableType == TableType.Ledger)
                            {
                                ledgerWorkDataInfo.AddWithChildren(ledger,
                                    oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                    storingLocations.Where(x => x.LOC_ID != null).ToList(), usageLocations);
                                objLedgerCommonInfo.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                            }
                            else
                            {
                                ledgerWorkDataInfo.SaveWithChildren(ledger,
                                    oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                    storingLocations.Where(x => x.LOC_ID != null).ToList(), usageLocations);
                                objLedgerCommonInfo.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                            }
                            if (!ledgerWorkDataInfo.IsValid)
                            {
                                var data = ledgerWorkDataInfo.ErrorMessages;
                                OutputWarningMessage = new List<string>(data);
                                return OutputWarningMessage;
                            }
                            using (var historyDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
                            {
                                historyDataInfo.HenkoApplication(ledger.LEDGER_FLOW_ID, userInfo.User_CD);
                                if (!historyDataInfo.IsValid)
                                {
                                    var data = historyDataInfo.ErrorMessages;
                                    OutputWarningMessage = new List<string>(data);
                                    return OutputWarningMessage;
                                }
                            }
                            context.Commit();
                            {
                                var mail = new WorkflowMail();
                                mail.SendMail(ledger);
                            }
                            break;

                        case UpdateType.TemporaryUpdate:
                            ledgerWorkDataInfo.IsValidateUseSave = false;
                            ledgerWorkDataInfo.SaveWithChildren(ledger,
                            oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                            storingLocations.Where(x => x.LOC_ID != null).ToList(), usageLocations);

                            if (!ledgerWorkDataInfo.IsValid)
                            {
                                var data = ledgerWorkDataInfo.ErrorMessages;
                                OutputWarningMessage = new List<string>(data);
                                return OutputWarningMessage;
                            }
                            context.Commit();
                            break;

                        case UpdateType.Abolition:
                            ledger.FLOW_CD = userInfo.FLOW_CD;
                            ledger.HIERARCHY = -1;

                            if (ledger.APPLI_CLASS == (int)AppliClass.Stopped)
                            {
                                foreach (var usageLocation in usageLocations)
                                {
                                    // Phase3追加項目には値が入っていないため、ダミーで-1を入れる
                                    if (usageLocation.REG_TRANSACTION_VOLUME == null)
                                    {
                                        usageLocation.REG_TRANSACTION_VOLUME = -1;
                                    }
                                    if (usageLocation.REG_WORK_FREQUENCY == null)
                                    {
                                        usageLocation.REG_WORK_FREQUENCY = -1;
                                    }
                                    if (usageLocation.REG_DISASTER_POSSIBILITY == null)
                                    {
                                        usageLocation.REG_DISASTER_POSSIBILITY = -1;
                                    }
                                }
                            }

                            ledgerWorkDataInfo.AddWithChildren(ledger,
                                    oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                    storingLocations.Where(x => x.LOC_ID != null).ToList(), usageLocations);
                            objLedgerCommonInfo.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;

                            if (!ledgerWorkDataInfo.IsValid)
                            {
                                var data = ledgerWorkDataInfo.ErrorMessages;
                                OutputWarningMessage = new List<string>(data);
                                return OutputWarningMessage;
                            }
                            using (var historyDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
                            {
                                historyDataInfo.Abolition(ledger.LEDGER_FLOW_ID, userInfo.User_CD);
                                if (!historyDataInfo.IsValid)
                                {
                                    var data = historyDataInfo.ErrorMessages;
                                    OutputWarningMessage = new List<string>(data);
                                    return OutputWarningMessage;
                                }
                            }
                            context.Commit();
                            {
                                var mail = new WorkflowMail();
                                mail.SendMail(ledger);
                            }
                            break;

                        case UpdateType.Withdraw:
                            if (ledger.flowid == null)
                            {
                                ledger.LEDGER_FLOW_ID = null;
                            }
                            ledgerWorkDataInfo.RemoveWithChildren((T_LEDGER_WORK)ledger, true);
                            if (!ledgerWorkDataInfo.IsValid)
                            {
                                var data = ledgerWorkDataInfo.ErrorMessages;
                                OutputWarningMessage = new List<string>(data);
                                return OutputWarningMessage;
                            }
                            using (var historyDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
                            {
                                historyDataInfo.Withdraw(ledger.LEDGER_FLOW_ID);
                                if (!historyDataInfo.IsValid)
                                {
                                    var data = historyDataInfo.ErrorMessages;
                                    OutputWarningMessage = new List<string>(data);
                                    return OutputWarningMessage;
                                }
                            }
                            context.Commit();
                            break;

                        case UpdateType.Regain:
                            ledger = ClientsideValidation(objLedgerCommonInfo);
                            ledger.HIERARCHY -= 1;
                            if (ledger.HIERARCHY == -2)
                            {
                                ledger.TEMP_SAVED_FLAG = 1;
                            }
                            ledgerWorkDataInfo.save((T_LEDGER_WORK)ledger);
                            if (!ledgerWorkDataInfo.IsValid)
                            {
                                var data = ledgerWorkDataInfo.ErrorMessages;
                                OutputWarningMessage = new List<string>(data);
                                return OutputWarningMessage;
                            }
                            using (var historyDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
                            {
                                historyDataInfo.Regained(ledger.LEDGER_FLOW_ID, userInfo.User_CD);
                                if (!historyDataInfo.IsValid)
                                {
                                    var data = historyDataInfo.ErrorMessages;
                                    OutputWarningMessage = new List<string>(data);
                                    return OutputWarningMessage;
                                }
                            }
                            context.Commit();
                            break;
                        case UpdateType.Remand:
                            ledger = ClientsideValidation(objLedgerCommonInfo);
                            ledger.HIERARCHY = -2;
                            ledgerWorkDataInfo.save((T_LEDGER_WORK)ledger);
                            if (!ledgerWorkDataInfo.IsValid)
                            {
                                var data = ledgerWorkDataInfo.ErrorMessages;
                                OutputWarningMessage = new List<string>(data);
                                return OutputWarningMessage;
                            }
                            using (var historyDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
                            {
                                historyDataInfo.Remand(ledger.LEDGER_FLOW_ID, userInfo.User_CD, MEMO);
                                if (!historyDataInfo.IsValid)
                                {
                                    var data = historyDataInfo.ErrorMessages;
                                    OutputWarningMessage = new List<string>(data);
                                    return OutputWarningMessage;
                                }
                            }
                            context.Commit();
                            {
                                var mail = new WorkflowMail();
                                mail.SendMail(ledger);
                            }
                            break;

                        case UpdateType.Approval:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                context.Rollback();
                context.Close();
                throw ex;
            }

            ledger.Mode = Mode.ReferenceAfterRegistration;
            ClearSession();
            //afterUpdateType = updateType;
            HttpContext.Current.Session.Add(SessionConst.IsMaintenaceFormBack, true);
            context.Close();
            return OutputWarningMessage;
        }

        public LedgerCommonInfo ClientsideValidation(LedgerCommonInfo objLedgerCommonInfo)
        {
            var ledger = (LedgerCommonInfo)HttpContext.Current.Session[nameof(LedgerCommonInfo)];
            if (objLedgerCommonInfo.LEDGER_FLOW_ID != null && objLedgerCommonInfo.LEDGER_FLOW_ID != ledger.LEDGER_FLOW_ID)
            {
                ledger.LEDGER_FLOW_ID = objLedgerCommonInfo.LEDGER_FLOW_ID;
            }
            if (objLedgerCommonInfo.FLG_RISK_ASSESSMENT != ledger.FLG_RISK_ASSESSMENT)
            {
                ledger.FLG_RISK_ASSESSMENT = objLedgerCommonInfo.FLG_RISK_ASSESSMENT;
            }
            if (objLedgerCommonInfo.REG_NO != null && objLedgerCommonInfo.REG_NO != ledger.REG_NO)
            {
                ledger.REG_NO = objLedgerCommonInfo.REG_NO;
            }
            if (objLedgerCommonInfo.GROUP_CD != null && objLedgerCommonInfo.GROUP_CD != ledger.GROUP_CD)
            {
                ledger.GROUP_CD = objLedgerCommonInfo.GROUP_CD;
            }
            if (objLedgerCommonInfo.GROUP_NAME != null && objLedgerCommonInfo.GROUP_NAME != ledger.GROUP_NAME)
            {
                ledger.GROUP_NAME = objLedgerCommonInfo.GROUP_NAME;
            }
            if (objLedgerCommonInfo.REASON_ID != null && objLedgerCommonInfo.REASON_ID != ledger.REASON_ID)
            {
                ledger.REASON_ID = objLedgerCommonInfo.REASON_ID;
            }

            if (objLedgerCommonInfo.OTHER_REASON != null && objLedgerCommonInfo.OTHER_REASON != ledger.OTHER_REASON)
            {
                ledger.OTHER_REASON = objLedgerCommonInfo.OTHER_REASON;
            }
            if (objLedgerCommonInfo.CHEM_CD != null && objLedgerCommonInfo.CHEM_CD != ledger.CHEM_CD)
            {
                ledger.CHEM_CD = objLedgerCommonInfo.CHEM_CD;
            }
            if (objLedgerCommonInfo.CHEM_NAME != null && objLedgerCommonInfo.CHEM_NAME != ledger.CHEM_NAME)
            {
                ledger.CHEM_NAME = objLedgerCommonInfo.CHEM_NAME;
            }
            if (objLedgerCommonInfo.CAS_NO != null && objLedgerCommonInfo.CAS_NO != ledger.CAS_NO)
            {
                ledger.CAS_NO = objLedgerCommonInfo.CAS_NO;
            }
            if (objLedgerCommonInfo.MAKER_NAME != null && objLedgerCommonInfo.MAKER_NAME != ledger.MAKER_NAME)
            {
                ledger.MAKER_NAME = objLedgerCommonInfo.MAKER_NAME;
            }
            if (objLedgerCommonInfo.FIGURE_NAME != null && objLedgerCommonInfo.FIGURE_NAME != ledger.FIGURE_NAME)
            {
                ledger.FIGURE_NAME = objLedgerCommonInfo.FIGURE_NAME;
            }
            if (objLedgerCommonInfo.CHEM_CAT != null && objLedgerCommonInfo.CHEM_CAT != ledger.CHEM_CAT)
            {
                ledger.CHEM_CAT = objLedgerCommonInfo.CHEM_CAT;
            }
            if (objLedgerCommonInfo.OTHER_REGULATION != null && objLedgerCommonInfo.OTHER_REGULATION != ledger.OTHER_REGULATION)
            {
                ledger.OTHER_REGULATION = objLedgerCommonInfo.OTHER_REGULATION;
            }
            if (objLedgerCommonInfo.REQUIRED_CHEM != null && objLedgerCommonInfo.REQUIRED_CHEM != ledger.REQUIRED_CHEM)
            {
                ledger.REQUIRED_CHEM = objLedgerCommonInfo.REQUIRED_CHEM;
            }
            if (objLedgerCommonInfo.HAZMAT_TYPE != null && objLedgerCommonInfo.HAZMAT_TYPE != ledger.HAZMAT_TYPE)
            {
                ledger.HAZMAT_TYPE = objLedgerCommonInfo.HAZMAT_TYPE;
            }
            if (objLedgerCommonInfo.SDS_URL != null && objLedgerCommonInfo.SDS_URL != ledger.SDS_URL)
            {
                ledger.SDS_URL = objLedgerCommonInfo.SDS_URL;
            }
            if (objLedgerCommonInfo.MEMO != null && objLedgerCommonInfo.MEMO != ledger.MEMO)
            {
                ledger.MEMO = objLedgerCommonInfo.MEMO;
            }
            if (objLedgerCommonInfo.hdnUNREGISTERED_CHEM != ledger.hdnUNREGISTERED_CHEM)
            {
                ledger.hdnUNREGISTERED_CHEM = objLedgerCommonInfo.hdnUNREGISTERED_CHEM;
            }
            HttpContext.Current.Session.Add(nameof(LedgerCommonInfo), ledger);

            return ledger;
        }

        /// <summary>
        /// セッション変数をクリアします。
        /// </summary>
        public void ClearSession()
        {
            HttpContext.Current.Session.Remove(nameof(LedgerWorkHistoryModel));
            HttpContext.Current.Session.Remove(sessionOldRegisterNumbers);
            HttpContext.Current.Session.Remove(sessionContaineds);
            HttpContext.Current.Session.Remove(sessionRegTypeIds);
            HttpContext.Current.Session.Remove(sessionRegulations);
            HttpContext.Current.Session.Remove(sessionChemRegTypeIds);
            HttpContext.Current.Session.Remove(sessionStoringLocations);
            HttpContext.Current.Session.Remove(sessionUsageLocations);
            HttpContext.Current.Session.Remove(sessionChemRegulations);
            HttpContext.Current.Session["sessionOldRegisterNumbers"] = null;
            HttpContext.Current.Session["Contained"] = null;
            HttpContext.Current.Session["StoringLocationDetails"] = null;
            HttpContext.Current.Session[sessionOldRegisterNumbers] = null;
        }

        public void getInputData(LedgerCommonInfo objLedgerCommonInfo)
        {

            var regulations = new List<LedgerRegulationModel>();
            string strRegTypeId = null;
            UserInformation userInfo = (UserInformation)HttpContext.Current.Session["LoginUserSession"];
            List<LedgerRegulationModel> objRegulation = new List<LedgerRegulationModel>();
            if (HttpContext.Current.Session[sessionRegulations] != null)
            {
                objRegulation = (List<LedgerRegulationModel>)HttpContext.Current.Session[sessionRegulations];
                if (objRegulation != null && objRegulation.Count > 0)
                {
                    foreach (LedgerRegulationModel items in objRegulation)
                    {
                        items.REG_USER_CD = userInfo.User_CD;
                        items.REG_NO = objLedgerCommonInfo.REG_NO;
                        items.LEDGER_FLOW_ID = objLedgerCommonInfo.LEDGER_FLOW_ID;
                        if (strRegTypeId != null)
                        {
                            strRegTypeId += ",";
                        }
                        strRegTypeId += Convert.ToInt32(items.REG_TYPE_ID);
                        //regulations.Add(data);
                    }
                }
            }
            HttpContext.Current.Session.Remove(sessionRegTypeIds);
            HttpContext.Current.Session.Remove(sessionRegulations);
            HttpContext.Current.Session.Add(sessionRegTypeIds, strRegTypeId);
            HttpContext.Current.Session.Add(sessionRegulations, objRegulation.AsEnumerable());
        }

        /// <summary>
        /// フッターボタンを設定します。
        /// </summary>
        public LedgerCommonInfo EnablingDisblingButtons(Mode mode, int? LEDGER_FLOW_ID, string CHEM_CD, int? HIERARCHY, string APPROVAL_USER_CD, string SUBSITUTE_USER_CD, string User_CD, string ADMIN_FLAG)
        {
            LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
            switch (mode)
            {
                case Mode.New:
                    objLedgerCommonInfo.btndeletematter = true;
                    objLedgerCommonInfo.btnRegain = true;
                    objLedgerCommonInfo.btntemporarysave = true;
                    break;
                case Mode.View:
                    objLedgerCommonInfo.btnReferenceRegister = true;
                    break;
                case Mode.ReferenceAfterRegistration:
                    break;
                case Mode.Approval:
                    objLedgerCommonInfo.btnRemand = true;
                    objLedgerCommonInfo.btnRegain = true;
                    objLedgerCommonInfo.btnChemRegister = true;
                    objLedgerCommonInfo.SendBackVisibility = true;
                    break;
                case Mode.Abolition:
                    objLedgerCommonInfo.btndeletematter = true;
                    objLedgerCommonInfo.btnRegain = true;
                    objLedgerCommonInfo.btntemporarysave = true;
                    break;
                case Mode.Deleted:
                    objLedgerCommonInfo.btndeletematter = false;
                    objLedgerCommonInfo.btnRegain = false;
                    objLedgerCommonInfo.btntemporarysave = false;
                    break;
                default:
                    objLedgerCommonInfo.btndeletematter = true;
                    objLedgerCommonInfo.btnRegain = true;
                    objLedgerCommonInfo.btntemporarysave = true;
                    break;
            }

            switch (mode)
            {
                case Mode.Approval:
                    objLedgerCommonInfo.btnApproval = true;
                    break;
                case Mode.Abolition:
                    objLedgerCommonInfo.btnRevocation = true;
                    break;
                case Mode.Edit:
                    objLedgerCommonInfo.btnApplicationchange = true;
                    break;
                case Mode.New:
                case Mode.Copy:
                    objLedgerCommonInfo.btntemporarysave = true;
                    objLedgerCommonInfo.btnRegister = true;
                    break;
            }


            switch (mode)
            {
                case Mode.New:
                    using (var workflow = new LedgerWorkHistoryDataInfo())
                    {
                        if (LEDGER_FLOW_ID != null)
                        {
                            var list = workflow.GetSearchResult(new T_LED_WORK_HISTORY { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
                            if (list.Count() > 0)
                            {
                                var first = list.FirstOrDefault(x => x.HIERARCHY == -1);
                                if (first.APPROVAL_USER_CD != User_CD && first.SUBSITUTE_USER_CD != User_CD)
                                {
                                    objLedgerCommonInfo.btndeletematter = false;
                                    objLedgerCommonInfo.btntemporarysave = false;
                                    objLedgerCommonInfo.btnRegister = false;
                                }
                            }
                            else
                            {
                                objLedgerCommonInfo.btndeletematter = false;
                                objLedgerCommonInfo.btntemporarysave = false;
                                objLedgerCommonInfo.btnRegister = false;
                            }
                        }
                    }
                    objLedgerCommonInfo.btnRegain = false;
                    break;
                case Mode.Copy:
                    objLedgerCommonInfo.btndeletematter = false;
                    objLedgerCommonInfo.btnRegain = false;
                    objLedgerCommonInfo.APPLI_CLASS = (int)AppliClass.New;
                    objLedgerCommonInfo.REASON_ID = null;
                    objLedgerCommonInfo.OTHER_REASON = null;
                    break;
                case Mode.Edit:
                    if (LEDGER_FLOW_ID != null)
                    {

                        using (var workflow = new LedgerWorkHistoryDataInfo())
                        {
                            var list = workflow.GetSearchResult(new T_LED_WORK_HISTORY { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
                            if (list.Count() > 0)
                            {
                                var first = list.FirstOrDefault(x => x.HIERARCHY == -1);
                                if (first.ACTUAL_USER_CD != User_CD)
                                {
                                    objLedgerCommonInfo.btndeletematter = false;
                                }
                            }
                            else
                            {
                                objLedgerCommonInfo.btndeletematter = false;
                            }
                        }
                    }
                    objLedgerCommonInfo.btnRegain = false;
                    break;
                case Mode.View:
                    if (objLedgerCommonInfo.TableType == TableType.Work)
                    {
                        objLedgerCommonInfo.btnReferenceRegister = false;
                    }
                    break;
                case Mode.Deleted:
                case Mode.ReferenceAfterRegistration:
                    //InactivateFooterButtonM3();
                    //InactivateFooterButtonM2();
                    //InactivateFooterButtonM1();
                    //InactivateFooterButton0();
                    break;
                case Mode.Abolition:
                    objLedgerCommonInfo.btnRevocation = true;
                    using (var workflow = new LedgerWorkHistoryDataInfo())
                    {
                        var list = workflow.GetSearchResult(new T_LED_WORK_HISTORY { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
                        if (list.Count() > 0)
                        {
                            var first = list.FirstOrDefault(x => x.HIERARCHY == -1);
                            if (first.ACTUAL_USER_CD != User_CD)
                            {
                                objLedgerCommonInfo.btndeletematter = false;
                            }
                        }
                        else
                        {
                            objLedgerCommonInfo.btndeletematter = false;
                        }
                    }
                    objLedgerCommonInfo.btnRegain = false;
                    break;
                case Mode.Approval:
                    objLedgerCommonInfo.btnApproval = InactivateApprovalButton(LEDGER_FLOW_ID, User_CD);
                    if (objLedgerCommonInfo.btnApproval == true)
                    {
                        objLedgerCommonInfo.btnRegister = false;
                    }
                    objLedgerCommonInfo.btnRemand = InactivateRemandButton(LEDGER_FLOW_ID, User_CD);
                    objLedgerCommonInfo.btnChemRegister = InactivateChemRegisterButton(CHEM_CD, HIERARCHY, APPROVAL_USER_CD, SUBSITUTE_USER_CD, User_CD);
                    if (objLedgerCommonInfo.btnChemRegister == false)
                    {
                        objLedgerCommonInfo.btntemporarysave = false;
                    }
                    objLedgerCommonInfo.btnRegain = InactivateRegainButton(LEDGER_FLOW_ID, User_CD);

                    break;
                default:
                    throw new ArgumentNullException();
            }
            return objLedgerCommonInfo;
        }

        /// <summary>
        /// 承認ボタンを無効化する
        /// </summary>
        protected bool InactivateApprovalButton(int? LEDGER_FLOW_ID, string usercd)
        {
            bool result = true;
            using (var hist = new LedgerWorkHistoryDataInfo())
            {
                if (!hist.CanApproval(LEDGER_FLOW_ID, usercd))
                {
                    result = false;
                }
            }
            return result;
        }
        /// <summary>
        /// 差戻しボタンを無効化する
        /// </summary>
        protected bool InactivateRemandButton(int? LEDGER_FLOW_ID, string usercd)
        {
            bool result = true;
            using (var hist = new LedgerWorkHistoryDataInfo())
            {
                if (!hist.CanRemand(LEDGER_FLOW_ID, usercd))
                {
                    //InactivateFooterButtonM3();
                    result = false;
                }
            }
            return result;
        }

        /// <summary>
        /// 化学物質登録ボタンを無効化する
        /// </summary>
        protected bool InactivateChemRegisterButton(string CHEM_CD, int? HIERARCHY, string APPROVAL_USER_CD, string SUBSITUTE_USER_CD, string USER_CD)
        {
            bool result = true;
            var ledger = (LedgerModel)HttpContext.Current.Session[nameof(LedgerModel)];
            if (CHEM_CD == NikonConst.UnregisteredChemCd && HIERARCHY != -1 &&
                (APPROVAL_USER_CD == USER_CD || SUBSITUTE_USER_CD == USER_CD))
            {
                return result;
            }
            else
            {
                result = false;
            }

            return result;
        }

        /// <summary>
        /// 取戻ボタンを無効化する
        /// </summary>
        protected bool InactivateRegainButton(int? LEDGER_FLOW_ID, string usercd)
        {
            bool result = true;
            if (!CanRegained(LEDGER_FLOW_ID, usercd))
            {
                //InactivateFooterButtonM2();
                result = false;
            }
            return result;
        }

        /// <summary>
        /// 取り戻せるか
        /// </summary>
        /// <param name="LEDGER_FLOW_ID"></param>
        /// <param name="USER_CD"></param>
        /// <returns></returns>
        public bool CanRegained(int? LEDGER_FLOW_ID, string USER_CD)
        {
            LedgerWorkHistoryDataInfo objLedgerWorkHistoryDataInfo = new LedgerWorkHistoryDataInfo();
            var list = objLedgerWorkHistoryDataInfo.GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
            var approve = list.Where(x => x.DEL_FLAG == 0 && x.RESULT != null).LastOrDefault();
            return approve != null &&
                (approve.APPROVAL_USER_CD == USER_CD || approve.SUBSITUTE_USER_CD == USER_CD) &&
                approve.RESULT != (int)WorkflowResult.Regained;
        }

        public CommonDataModel<string> GroupList()
        {
            var returnValue = HttpContext.Current.Session[SessionConst.CategorySelectReturnValue];
            var selectValue = (Dictionary<string, string>)returnValue;
            CommonDataModel<string> group = GetGroupList().Single(x => x.Id == selectValue.Single().Value);
            return group;
        }

        /// <summary>
        /// 保護具のSelectMethodで入力済みの保護具を取得します。
        /// </summary>
        /// <returns>選択済みの化学物質名を取得します。</returns>
        public IQueryable<LedgerProtectorModel> GetInputedProtectors(IEnumerable<LedgerProtectorModel> protectors)
        {
            if (HttpContext.Current.Session[sessionProtectors] != null)
            {
                return protectors.ToList().AsQueryable();
            }
            var usageLocation = GetLedgerUsageLocationInfo();
            var newDataArray = GetAllProtectors().ToList();
            if (usageLocation != null)
            {
                if (newDataArray.Count() == usageLocation.PROTECTOR_COLLECTION.Count())
                {
                    return usageLocation.PROTECTOR_COLLECTION.AsQueryable();
                }
                else
                {
                    foreach (var p in usageLocation.PROTECTOR_COLLECTION)
                    {
                        newDataArray.Single(x => x.PROTECTOR_ID == p.PROTECTOR_ID).CHECK = true;
                    }
                    return newDataArray.AsQueryable();
                }
            }
            else
            {
                HttpContext.Current.Session.Add(sessionProtectors, newDataArray.AsEnumerable());
                return newDataArray.AsQueryable();
            }
        }

        /// <summary>
        /// 編集項目のSelectMethodで編集対象を取得します。
        /// </summary>
        /// <param name="model">セッションに格納されている使用場所情報を抽出します。</param>
        /// <returns>編集対象を取得します。</returns>
        public LedgerUsageLocationModel GetLedgerUsageLocationInfo()
        {
            if (!string.IsNullOrEmpty(rowId))
            {
                var usageLocations = (IEnumerable<LedgerUsageLocationModel>)HttpContext.Current.Session[sessionUsageLocations];
                var usageLocation = usageLocations.Where(x => x.RowId == rowId).SingleOrDefault();
                usageLocation.Mode = mode;
                return usageLocation;
            }
            return new LedgerUsageLocationModel();
        }

        /// <summary>
        /// 保護具の一覧を取得します。
        /// </summary>
        /// <param name="protectors"></param>
        /// <returns></returns>
        protected IEnumerable<LedgerProtectorModel> GetAllProtectors(IEnumerable<LedgerProtectorModel> protectors = null)
        {
            var seq = 0;
            foreach (var protector in GetProtectorList().Where(x => x.Id != null))
            {
                var model = new LedgerProtectorModel()
                {
                    PROTECTOR_SEQ = seq++,
                    PROTECTOR_ID = protector.Id,
                    PROTECTOR_NAME = protector.Name,
                };
                yield return model;
            }
        }

        /// <summary>
        /// リスク低減策データを取得します。
        /// </summary>
        /// <returns>選択済みの化学物質名を取得します。</returns>
        public IQueryable<LedgerRiskReduceModel> GetInputedRiskReduces(IEnumerable<LedgerRiskReduceModel> riskReduces)
        {
            if (HttpContext.Current.Session[sessionRiskReduces] != null && riskReduces != null)
            {
                return riskReduces.ToList().AsQueryable();
            }
            var usageLocation = GetLedgerUsageLocationInfo();
            var newDataArray = GetAllRiskReduces().ToList();
            if (usageLocation != null)
            {
                if (usageLocation.RISK_REDUCE_COLLECTION.Count() != 0)
                {
                    foreach (var p in newDataArray)
                    {
                        usageLocation.RISK_REDUCE_COLLECTION.Single(x => x.LED_RISK_REDUCE_TYPE == p.LED_RISK_REDUCE_TYPE).RISK_REDUCE_POLICY = p.RISK_REDUCE_POLICY;
                    }
                    return usageLocation.RISK_REDUCE_COLLECTION.AsQueryable();
                }
                else
                {
                    return newDataArray.AsQueryable();
                }
            }
            else
            {
                HttpContext.Current.Session.Add(sessionRiskReduces, newDataArray.AsEnumerable());
                return newDataArray.AsQueryable();
            }
        }

        /// <summary>
        /// リスク低減策の一覧を取得します。
        /// </summary>
        /// <param name="riskReduces"></param>
        /// <returns></returns>
        protected IEnumerable<LedgerRiskReduceModel> GetAllRiskReduces(IEnumerable<LedgerRiskReduceModel> riskReduces = null)
        {
            foreach (var riskReduce in GetRiskReduceList().Where(x => x.Id != null))
            {
                var model = new LedgerRiskReduceModel()
                {
                    LED_RISK_REDUCE_ID = riskReduce.Id,
                    RISK_REDUCE_POLICY = riskReduce.Name,
                    LED_RISK_REDUCE_TYPE = riskReduce.Order,
                };
                yield return model;
            }
        }

        /// <summary>
        /// アセスメント評価値、対策前のリスクレベルを計算します。
        /// </summary>
        /// <param name="flg">true:エラーメッセージを出力する/ false:エラーメッセージを出力しない</param>
        public void RiskBeforeEvaluate(LedgerModel objLedgerModel, bool flg)
        {
            //var errorMessages = new List<string>();
            //var model = (LedgerUsageLocationModel)HttpContext.Current.Session[nameof(LedgerUsageLocationModel)];
            //if (model == null)
            //{
            //    model = new LedgerUsageLocationModel();
            //}
            //var sessionledger = (LedgerModel)HttpContext.Current.Session[nameof(LedgerModel)];
            //Boolean flgUnreisteredChem = Convert.ToBoolean(((HiddenField)MaintenanceFormView.FindControl("FLG_UNREGISTERED_CHEM")).Value);

            //var ctrlIU = (DropDownList)MaintenanceFormView.FindControl("ISHA_USAGE");
            //if (!string.IsNullOrWhiteSpace(ctrlIU.Text))
            //{
            //    //applicableLawGridInitialize();

            //    //// リスクアセスメントリスト更新
            //    //updateRAList(true, sessionledger.LEDGER_FLOW_ID);
            //}

            //if (string.IsNullOrWhiteSpace(ctrlIU.Text))
            //{
            //    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName((T_MGMT_LED_USAGE_LOC)model, "ISHA_USAGE")));
            //}

            //var ctrlRTV = (DropDownList)MaintenanceFormView.FindControl("REG_TRANSACTION_VOLUME");
            //var ctrlRWF = (DropDownList)MaintenanceFormView.FindControl("REG_WORK_FREQUENCY");
            //var ctrlRDP = (DropDownList)MaintenanceFormView.FindControl("REG_DISASTER_POSSIBILITY");

            //if (string.IsNullOrWhiteSpace(ctrlRTV.Text))
            //{
            //    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName((T_MGMT_LED_USAGE_LOC)model, "REG_TRANSACTION_VOLUME")));
            //}
            //if (string.IsNullOrWhiteSpace(ctrlRWF.Text))
            //{
            //    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName((T_MGMT_LED_USAGE_LOC)model, "REG_WORK_FREQUENCY")));
            //}
            //if (string.IsNullOrWhiteSpace(ctrlRDP.Text))
            //{
            //    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName((T_MGMT_LED_USAGE_LOC)model, "REG_DISASTER_POSSIBILITY")));
            //}

            //var measuresbeforescore = (Label)MaintenanceFormView.FindControl("MEASURES_BEFORE_SCORE");
            //var measuresbeforerankdisp = (Label)MaintenanceFormView.FindControl("MEASURES_BEFORE_RANK_DISP");
            //var measuresbeforerank = (HiddenField)MaintenanceFormView.FindControl("MEASURES_BEFORE_RANK");
            //var measuresbeforescoreunit = (Label)MaintenanceFormView.FindControl("MEASURES_BEFORE_SCORE_UNIT");
            //if (errorMessages.Count > 0)
            //{
            //    measuresbeforescore.Text = "";
            //    measuresbeforerankdisp.Text = "";
            //    measuresbeforerank.Value = "";
            //    measuresbeforescoreunit.Text = "";
            //    if (flg)
            //    {
            //        OutputWarningMessage(errorMessages);
            //    }
            //}
            //else
            //{
            //    if (!flgUnreisteredChem)
            //    {
            //        // ②職場の取扱量
            //        int intRTV = System.Convert.ToInt32(ctrlRTV.Text);
            //        decimal valueRTV = decimal.Parse(new CommonMasterInfo(NikonCommonMasterName.REG_TRANSACTION_VOLUME_VALUE).GetDictionary().GetValueToString(intRTV));

            //        // ③作業頻度
            //        int intRWF = System.Convert.ToInt32(ctrlRWF.Text);
            //        decimal valueRWF = decimal.Parse(new CommonMasterInfo(NikonCommonMasterName.REG_WORK_FREQUENCY_VALUE).GetDictionary().GetValueToString(intRWF));

            //        // ④災害発生の可能性
            //        int intRDP = System.Convert.ToInt32(ctrlRDP.Text);
            //        decimal valueRDP = decimal.Parse(new CommonMasterInfo(NikonCommonMasterName.REG_DISASTER_POSSIBILITY_VALUE).GetDictionary().GetValueToString(intRDP));

            //        // アセスメント評価値:(①適用法令×②職場の取扱量×③作業頻度)+④災害発生の可能性
            //        decimal dMBS = Math.Round(((TOTAL_MARKS * valueRTV * valueRWF) + valueRDP), 2, MidpointRounding.AwayFromZero);

            //        measuresbeforescore.Text = Convert.ToString(dMBS);
            //        measuresbeforescoreunit.Text = "点";

            //        int intRank;
            //        if (dMBS < 80)
            //        {
            //            intRank = 1;
            //        }
            //        else if (dMBS < 250)
            //        {
            //            intRank = 2;
            //        }
            //        else
            //        {
            //            intRank = 3;
            //            measuresbeforerankdisp.ForeColor = System.Drawing.Color.Red;
            //        }

            //        measuresbeforerankdisp.Text = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary().GetValueToString(intRank);
            //        measuresbeforerank.Value = intRank.ToString();
            //    }
            //    else
            //    {
            //        measuresbeforescore.Text = null;
            //        measuresbeforerankdisp.Text = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary().GetValueToString(4);
            //        measuresbeforerank.Value = "4";
            //    }
            //}
        }

        public string getchemoerationchiefname(string CHEM_OPERATION_CHIEF)
        {

            var userInfo = GetUserList().SingleOrDefault(x => x.Id == CHEM_OPERATION_CHIEF);
            return userInfo.Name.ToString();
        }

    }
}