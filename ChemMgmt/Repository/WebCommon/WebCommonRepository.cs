﻿using Dapper;
using System;
using System.Data.SqlClient;
using System.Text;
using ZyCoaG.Classes.util;
using ZyCoaG;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using ZyCoaG.BaseCommon;
using Microsoft.VisualBasic.FileIO;
using System.Web;
using ChemMgmt.Classes.util;
using ZyCoaG.util;
using ChemMgmt.Classes;
using ChemMgmt.Models;
using ChemMgmt.Classes.Extensions;
using System.IO;
using DatabaseType = ZyCoaG.DatabaseType;
using IdentityManagement.Entities;

namespace ChemMgmt.Repository
{
    public class WebCommonRepo
    {
        private Dictionary<string, string> inputParam;
        private LogUtil _logoututil = new LogUtil();
        private const string ProgramId = "ZC010";
        public List<UserInfo.ClsUser> getUserList = new List<UserInfo.ClsUser>();

        /// <summary>
        /// 選択したIDを格納します。
        /// </summary>
        public List<string> IDs(Dictionary<string, string> SelectedValue, string tableName)
        {
            string setValue = "";
            List<string> selectedList = new List<string>();
            foreach (var Values in SelectedValue)
            {
                if (!string.IsNullOrWhiteSpace(Values.Value))
                {
                    //return Values.Value.Split(SystemConst.SelectionsDelimiter.ToCharArray());
                }
                if (Values.Value != null && Values.Value.Count() > 0)
                {
                    setValue = Values.Value;
                    string SelectTextValue = string.Join(SystemConst.SelectionsDelimiter, setValue);
                    string SelectStatusView = SystemConst.SelectionViewSetText;
                }
                selectedList.Add(setValue);
            }
            return selectedList;
        }


        public void OutputCSV(List<InventoryUsageHistory> g, string fileFullPath, bool isHeaderDataColumnName, bool isDoubleQuotation)
        {
            var lines = new List<string>();
            var line = new List<string>();
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {
                for (int I = 0; I < g.Count; I++)
                {
                    int Seqno = I + 1;
                    if (I == 0)
                    {
                        csv.WriteLine("操作順" + "," + "操作" + "," + "化学物質名" + "," + "瓶番号" + "," + "ユーザー名" + "," + "ユーザーコード" + "," + "保管場所" + "," + "使用場所" + "," + "使用目的ID" + "," + "使用目的名" + "," + "使用目的(その他)" + "," + "作業時間（分）" + "," + "重量履歴（風袋込）" + "," + "重量履歴単位" + "," + "備考" + "," + "発生日時");
                        csv.WriteLine("\"" + Seqno + "\"" + "," + "\"" + g[I].ACTION_TEXT + "\"" + "," + "\"" + g[I].CHEM_NAME + "\"" + "," + "\"" + g[I].BARCODE + "\"" + "," + "\"" + g[I].USER_NAME + "\"" + "," + "\"" + g[I].USER_CD + "\"" + "," + "\"" + g[I].LOC_NAME + "\"" + "," + "\"" + g[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g[I].INTENDED_USE_ID + "\"" + "," + "\"" + g[I].INTENDED_NM + "\"" + "," + "\"" + g[I].INTENDED + "\"" + "," + "\"" + g[I].WORKING_TIMES + "\"" + "," + "\"" + g[I].CUR_GROSS_WEIGHT_VOLUME + "\"" + "," + "\"" + g[I].CUR_GROSS_WEIGHT_UNITSIZE + "\"" + "," + "\"" + g[I].MEMO + "\"" + "," + "\"" + g[I].REG_DATE + "\"");

                    }
                    else
                    {
                        csv.WriteLine("\"" + Seqno + "\"" + "," + "\"" + g[I].ACTION_TEXT + "\"" + "," + "\"" + g[I].CHEM_NAME + "\"" + "," + "\"" + g[I].BARCODE + "\"" + "," + "\"" + g[I].USER_NAME + "\"" + "," + "\"" + g[I].USER_CD + "\"" + "," + "\"" + g[I].LOC_NAME + "\"" + "," + "\"" + g[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g[I].INTENDED_USE_ID + "\"" + "," + "\"" + g[I].INTENDED_NM + "\"" + "," + "\"" + g[I].INTENDED + "\"" + "," + "\"" + g[I].WORKING_TIMES + "\"" + "," + "\"" + g[I].CUR_GROSS_WEIGHT_VOLUME + "\"" + "," + "\"" + g[I].CUR_GROSS_WEIGHT_UNITSIZE + "\"" + "," + "\"" + g[I].MEMO + "\"" + "," + "\"" + g[I].REG_DATE + "\"");
                    }
                }
            }
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 29-7-2019
        /// Description :Get the Message string in the login page.
        /// <summary>
        public string GetSqlPatternName()
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("SELECT  DISTINCT PATTERN_NAME FROM M_USER_MAKER WHERE USER_CD = @USER_CD AND DEL_FLAG <> 1 ORDER BY PATTERN_NAME");

            return sbSql.ToString();
        }

        /// <summary>
        /// ユーザー情報を取得する
        /// </summary>
        /// <paramMap name="AttestationFLG">0:Normal 1:LDAP</paramMap>        
        /// <returns>true：ユーザー情報取得 / false：取得失敗</returns>
        public bool GetLoginUserInfo(int AttestationFLG, string user)
        {
            bool ret = false;
            UserInfo.ClsUser _clsUser = new UserInfo.ClsUser();
            try
            {
                WebCommonInfo dtUser = GetUserTable(user);
                if (dtUser != null)
                {
                    if (AttestationFLG == 0) 
                    {
                        //   message = Common.GetMessage("C002", "ユーザーIDとパスワード");
                    }
                    else
                    {
                        //ユーザ情報を格納               
                        _clsUser.USER_CD = dtUser.USER_CD;
                        _clsUser.USER_NAME = dtUser.USER_NAME;
                        _clsUser.USER_NAME_KANA = dtUser.USER_NAME_KANA;
                        _clsUser.APPROVER1 = dtUser.APPROVER1;
                        _clsUser.APPROVER2 = dtUser.APPROVER2;
                        _clsUser.TEL = dtUser.TEL;
                        _clsUser.EMAIL = dtUser.EMAIL;
                        _clsUser.ROOM_ID = dtUser.ROOM_ID;
                        _clsUser.ADMINFLG = dtUser.ADMIN_FLAG;
                        _clsUser.MEMO = dtUser.MEMO;
                        _clsUser.BILINGUAL_MODE = dtUser.BILINGUAL_MODE;
                        _clsUser.GROUP_CD = dtUser.GROUP_CD;
                        _clsUser.AREA_ID = dtUser.AREA_ID;
                        _clsUser.IP_ADDRESS = HttpContext.Current.Request.UserHostAddress;//Request.UserHostAddress; 
                        _clsUser.FLOW_CD = dtUser.FLOW_CD;

                        // 発注・使用権限を設定
                        _clsUser.SetAuthority();

                        //セッションに格納
                        HttpContext.Current.Session[Constant.CsessionNameUserINFO] = _clsUser;
                        //dtUser.Clear();
                        dtUser = null;
                        ret = true;
                    }
                }
                else
                {
                    //    message = Common.GetMessage("C002", "ユーザーIDとパスワード");
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            return ret;
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 24-7-2019
        /// Description : To Get the User Master List in Dashboard.
        /// <summary>
        public void GetUserMasterList()
        {
            MasterService masterService = new MasterService();
            List<M_USER> dt = masterService.SelectUser();
            foreach (M_USER dr in dt)
            {
                UserInfo.ClsUser user = new UserInfo.ClsUser();
                user.USER_CD = Convert.ToString(dr.USER_CD);
                user.USER_NAME = Convert.ToString(dr.USER_NAME);
                getUserList.Add(user);
            }
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-7-2019
        /// Description : To display the right side message in the login screen
        /// <summary>
        public string DisplayMessage()
        {
            string strSQL = "";
            string displayValue = "";
            try
            {
                strSQL = "select MSG_ID,MSG_TEXT from M_MESSAGE where DEL_FLAG <> 1 order by MSG_ID desc";

                List<M_MESSAGE> msgList = DbUtil.Select<M_MESSAGE>(strSQL, null);
                if (msgList[0].MSG_TEXT != null)
                {
                    //MSG_IDが一番大きいデータを1件だけ表示。
                    displayValue = msgList[0].MSG_TEXT;
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString() + " \r\n SQL= " + strSQL);
            }
            return displayValue;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 17-7-2019
        /// Description : To Get all the User Information using user code in the User settings screen with dapper call.
        /// <summary>
        public WebCommonInfo GetUserInfo(string userCd, string identityFlag)
        {
            // ユーザーIDからユーザーマスター情報取得
            MasterService masterService = new MasterService();
            WebCommonInfo dt = masterService.SelectAllUserInformation(userCd);
            try
            {
                MasterService ms = new MasterService();
                // 管理者フラグ
                ArrayList arrayList = new ArrayList();
                List<SelectListItem> ddlAdminFlag = new List<SelectListItem> { };
                new CommonMasterInfo(SystemCommonMasterName.GrantName).GetSelectList().ToList().
                    ForEach(x => ddlAdminFlag.Add(new SelectListItem
                    { Value = x.Id.ToString(), Text = x.Name }));
                if (identityFlag != "M_USER")
                {
                    // called from web common user setting 
                    dt.ddlAdminFlag = ddlAdminFlag.Where(x => x.Value.Equals(dt.ADMIN_FLAG));
                }
                else
                {
                    // called from User Master
                    dt.ddlAdminFlag = ddlAdminFlag;
                }

                // フロー名
                List<M_WORKFLOW> dtFlow = ms.SelectWorkFlow();
                dt.ddlFlowCd = BindLookupValues(dtFlow);

                DynamicParameters param = new DynamicParameters();
                param.Add("USER_CD", userCd);
                if (dt.BATCH_FLAG == "1")
                {
                    dt.BATCH_FLG = true;
                }
                else
                {
                    dt.BATCH_FLG = false;
                }
                if (dt.DEL_FLAG == 1)
                {
                    dt.DEL_FLG = true;
                }
                else
                {
                    dt.DEL_FLG = false;
                }
                /*
                // need to check where it is used and coming with data instead of null. because below sql always provides no data output.
                // NOTE: M_USER_MAKER table has no records

                // DropDownの設定
                string sql = repository.GetSqlPatternName();
                List<M_USER_MAKER> dt1 = DbUtil.Select<M_USER_MAKER>(sql, param);
                dt.drpPattern = BindLookupValues(dt1);
                */
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            return dt;
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 17-7-2019
        /// Description : For Lookup in M_WORKFLOW model for the key and values in the user setting screen.
        /// <summary>
        public IEnumerable<SelectListItem> BindLookupValues(List<M_WORKFLOW> result)
        {
            //List<DataRow> list =res;
            IEnumerable<SelectListItem> selectList = from s in result
                                                     select new SelectListItem
                                                     {
                                                         Text = s.FLOW_NAME,
                                                         Value = s.FLOW_CD
                                                     };
            return selectList;
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 17-7-2019
        /// Description : For Lookup in M_USER_MAKER model for the key and values.
        /// <summary>
        public IEnumerable<SelectListItem> BindLookupValues(List<M_USER_MAKER> result)
        {
            //List<DataRow> list =res;
            IEnumerable<SelectListItem> selectList = from s in result
                                                     select new SelectListItem
                                                     {
                                                         Text = s.MAKER_NAME,
                                                         Value = s.MAKER_ID.ToString()
                                                     };
            return selectList;
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 17-7-2019
        /// Description : Get the FlowName by the flow code in the User Setting Screen.
        /// <summary>
        public string GetMessageById(IEnumerable<SelectListItem> items, string key)
        {
            IEnumerable<SelectListItem> item = (IEnumerable<SelectListItem>)items.Where(x => x.Value.Equals(key));
            return item.First(x => x.Value.Equals(key)).Text;
        }

        /// <summary>
        /// 取込処理を行います。
        /// </summary>
        public void DoImport(string fileName, out int importedCount, out string errMessage)
        {
            importedCount = 0;
            errMessage = "";

            if (HttpContext.Current.Session[SessionConst.TableColumnInformations] == null) return;

            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)HttpContext.Current.Session[SessionConst.TableColumnInformations];
            TextFieldParser csvParser = new TextFieldParser(fileName, Encoding.GetEncoding("Shift_JIS"));
            csvParser.TextFieldType = FieldType.Delimited;
            csvParser.SetDelimiters(SystemConst.SelectionsDelimiter);

            //check           UserInfo.ClsUser clsUser = (UserInfo.ClsUser)Session[Constant.CsessionNameUserINFO];

            // csvヘッダー行（1行目）を読み込んで テンプレートのヘッダーと比較する
            List<string> csvHeaders = csvParser.ReadFields().ToList().ConvertAll(x => x.RemoveRequiredMark());
            IEnumerable<string> templateHeaders = tableColumnInformations.Select(x => x.COLUMN_DISPLAY_NAME);

            if (string.Join(string.Empty, csvHeaders) != "状態NID社員名社員名（ローマ字）パスワード部署名上長代理者内線番号メールアドレス前回部署フロー名登録者登録日時更新者更新日時管理者権限削除者削除日時バッチ更新対象外")
            {
                errMessage = "ヘッダー行の形式が不正です。<br/>リスト出力もしくはテンプレート出力したファイルを使用してください。";
                return;
            }

            int rowIndex = 1;
            Dictionary<string, string> updateContainer = new Dictionary<string, string>();

            updateContainer.Add("DEL_FLAG", string.Empty);
            updateContainer.Add("USER_CD", string.Empty);
            updateContainer.Add("USER_NAME", string.Empty);
            updateContainer.Add("USER_NAME_KANA", string.Empty);
            updateContainer.Add("PASSWORD", string.Empty);
            updateContainer.Add("GROUP_CD", string.Empty);
            updateContainer.Add("APPROVER1", string.Empty);
            updateContainer.Add("APPROVER2", string.Empty);
            updateContainer.Add("TEL", string.Empty);
            updateContainer.Add("EMAIL", string.Empty);
            updateContainer.Add("PREVIOUS_GROUP_CD", string.Empty);
            updateContainer.Add("FLOW_CD", string.Empty);
            updateContainer.Add("REG_USER_CD", string.Empty);
            updateContainer.Add("REG_DATE", string.Empty);
            updateContainer.Add("UPD_USER_CD", string.Empty);
            updateContainer.Add("UPD_DATE", string.Empty);
            updateContainer.Add("ADMIN_FLAG", string.Empty);
            updateContainer.Add("DEL_USER_CD", string.Empty);
            updateContainer.Add("DEL_DATE", string.Empty);
            updateContainer.Add("BATCH_FLAG", string.Empty);

            while (!csvParser.EndOfData)
            {
                // 1行ずつ読み込んで配列にセット
                List<string> row = csvParser.ReadFields().ToList();
                int i = 0;
                Dictionary<string, string> updateData = new Dictionary<string, string>();
                foreach (string key in updateContainer.Keys)
                {
                    updateData.Add(key, row[i] != null ? row[i] : string.Empty);
                    i++;
                }

                string rowErrorMessage = "";
                inputParam = updateData;
                importedCount += Import(out rowErrorMessage);

                // 呼び出し先でcatchしたエラーを溜めておく
                if (rowErrorMessage != "")
                {
                    errMessage += rowIndex.ToString() + "行目：" + SystemConst.HtmlLinefeed + rowErrorMessage + "<br/>";
                }
                rowIndex++;
            }
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 17-7-2019
        /// Description : To Filter the Categories.
        /// <summary>
        public void FilterAndCreateCategory(List<Category> originalCategories, List<Category> newCategories, Category targetCategory)
        {
            if (!newCategories.Any(x => x.Key == targetCategory.Key)) newCategories.Add(targetCategory);
            if (string.IsNullOrWhiteSpace(targetCategory.UpperLevel)) return;
            FilterAndCreateCategory(originalCategories, newCategories, originalCategories.Single(x => x.Key == targetCategory.UpperLevel));
        }

        /// <summary>
        /// 選択ボタンの追加対象か、また上位カテゴリーのキーと値の項目情報を返します。
        /// <param name="tableName">判定するテーブル名を指定します。</param>
        /// <param name="columnName">判定する項目名を指定します。</param>
        /// <param name="keyColumnName">上位カテゴリーのキー項目を返します。</param>
        /// <param name="valueColumnName">上位カテゴリーの値項目を返します。</param>
        /// <param name="iconPathColumnName">アイコンの値項目を返します。</param>
        /// <param name="sortOrderColumnName">ソート順項目を返します。</param>
        /// <param name="classColumnName">区分項目を返します。</param>
        /// <returns>追加対象の場合は<see cref="true"/>、そうでない場合は<see cref="false"/>を返します。</returns>
        /// <summary>
        public static bool IsSelectButtonTarget(string tableName, string columnName, out string keyColumnName, out string valueColumnName, out string iconPathColumnName,
                                                out string sortOrderColumnName, out string classColumnName, out string selFlagName)//2018/08/19 TPE.Sugimoto Upd
        {
            bool isTarget = false;
            keyColumnName = null;
            valueColumnName = null;
            iconPathColumnName = null;
            sortOrderColumnName = null;
            classColumnName = null;
            selFlagName = null;

            // ユーザーマスター(上位無し)
            if (tableName == "V_GROUP_USER" && columnName == "PID")
            {
                isTarget = true;
                keyColumnName = "ID";
                valueColumnName = "NAME";
            }
            // 部署マスターの上位部署CD
            if (tableName == nameof(M_GROUP) && columnName == nameof(M_GROUP.P_GROUP_CD))
            {
                isTarget = true;
                keyColumnName = nameof(M_GROUP.GROUP_CD);
                valueColumnName = nameof(M_GROUP.GROUP_NAME);
                sortOrderColumnName = nameof(M_GROUP.GROUP_CD);
            }
            // 保管場所マスターの上位保管場所ID
            if ((tableName == nameof(M_LOCATION) || tableName == nameof(M_STORING_LOCATION) || tableName == nameof(M_USAGE_LOCATION)) &&
                 columnName == nameof(M_LOCATION.P_LOC_ID))
            {
                isTarget = true;
                keyColumnName = nameof(M_LOCATION.LOC_ID);
                valueColumnName = nameof(M_LOCATION.LOC_NAME);
                sortOrderColumnName = nameof(M_LOCATION.SORT_LEVEL);
                classColumnName = nameof(M_LOCATION.CLASS_ID);
                selFlagName = nameof(M_LOCATION.SEL_FLAG); //2018/08/19 TPE.Sugimoto Upd
            }
            // 法規制マスター(法規制のみ)の上位法規制種類ID
            if (tableName == nameof(M_REG) && columnName == nameof(M_REG.P_REG_TYPE_ID))
            {
                isTarget = true;
                keyColumnName = nameof(M_REG.REG_TYPE_ID);
                valueColumnName = nameof(M_REG.REG_TEXT);
                iconPathColumnName = nameof(M_REG.REG_ICON_PATH);
                sortOrderColumnName = nameof(M_REG.SORT_ORDER);
            }
            // 法規制マスター(GHS分類・法規制)の上位法規制種類ID
            if (tableName == nameof(M_REGULATION) && columnName == nameof(M_REGULATION.P_REG_TYPE_ID))
            {
                isTarget = true;
                keyColumnName = nameof(M_REGULATION.REG_TYPE_ID);
                valueColumnName = nameof(M_REGULATION.REG_TEXT);
                iconPathColumnName = nameof(M_REGULATION.REG_ICON_PATH);
                sortOrderColumnName = nameof(M_REGULATION.SORT_ORDER);
                classColumnName = nameof(M_REGULATION.CLASS_ID);
            }
            // GHS分類マスターの上位GHS分類ID
            if (tableName == "M_GHSCAT" && columnName == "P_GHSCAT_ID")
            {
                isTarget = true;
                keyColumnName = "GHSCAT_ID";
                valueColumnName = "GHSCAT_NAME";
                iconPathColumnName = "GHSCAT_ICON_PATH";
                sortOrderColumnName = "SORT_ORDER";
            }
            // M_COMMON
            if (tableName == "M_COMMON")
            {
                isTarget = true;
                keyColumnName = nameof(M_COMMON.KEY_VALUE);
                valueColumnName = nameof(M_COMMON.DISPLAY_VALUE);
                sortOrderColumnName = nameof(M_COMMON.DISPLAY_ORDER);
                classColumnName = nameof(M_COMMON.TABLE_NAME);
            }
            // 消防法マスターの上位GHS分類ID
            if (tableName == "M_FIRE" && columnName == "P_FIRE_ID")
            {
                isTarget = true;
                keyColumnName = "FIRE_ID";
                valueColumnName = "FIRE_NAME";
                iconPathColumnName = "FIRE_ICON_PATH";
                sortOrderColumnName = "SORT_ORDER";
            }
            // 社内ルールマスターの上位GHS分類ID
            if (tableName == "M_OFFICE" && columnName == "P_OFFICE_ID")
            {
                isTarget = true;
                keyColumnName = "OFFICE_ID";
                valueColumnName = "OFFICE_NAME";
                iconPathColumnName = "OFFICE_ICON_PATH";
                sortOrderColumnName = "SORT_ORDER";
            }
            return isTarget;
        }


        /// <summary>
        /// ツリーのタグを作成します。ツリーを作成するために再帰呼び出しを行います。
        /// </summary>
        /// <param name="script">スクリプトを出力する<see cref="StringBuilder"/>を指定します。</param>
        /// <param name="CategoryItems">ツリーに出力するカテゴリーおよびデータを指定します。</param>
        /// <param name="UpperLevel">上位カテゴリーのコードを指定します。</param>
        /// <param name="SelectType">単一選択か複数選択か<see cref="SystemConst.SelectType"/>を指定します。</param>
        /// <param name="isClear">クリア時の作成処理か。</param>
        /// <summary>

        public void CreateTreeTag(ref StringBuilder script, List<Category> CategoryItems, string upperLevel, SelectType selectType, IEnumerable<string> selectedKeys)
        {
            List<Category> categoryItems = CategoryItems.Where(x => x.UpperLevel?.Trim() == upperLevel?.Trim()).ToList();

            if (categoryItems.Count == 0)
            {
                script.Append("} \r\n");
            }
            else
            {
                bool isFirst = true;
                for (int i = 0; i < categoryItems.Count; i++)
                {
                    if (isFirst && !string.IsNullOrEmpty(upperLevel))
                    {
                        script.Append(" , children: [ \r\n");
                    }
                    isFirst = false;

                    script.Append(" {");
                    string tableName = (string)HttpContext.Current.Session[SessionConst.CategoryTableName];
                    if (selectType == SelectType.One && (tableName == "M_STORING_LOCATION" || tableName == "M_USAGE_LOCATION"))
                    {
                        if (categoryItems[i].SelFlg == "0")
                        {
                            bool isHideCheckBox = CategoryItems.Where(x => x.UpperLevel?.Trim() == categoryItems[i].Key.Trim()).Count() != 0;
                            script.Append(CreateSelectBoxTag(categoryItems[i], selectedKeys, true));
                        }
                        else
                        {
                            script.Append(CreateSelectBoxTag(categoryItems[i], selectedKeys, false));
                        }
                    }
                    else
                    {
                        // 末端複数選択可能時かつ末端の項目の場合にのみチェックボックスを表示する。
                        if (selectType == SelectType.MultipleTerminal || selectType == SelectType.OneTerminal)
                        {
                            bool isHideCheckBox = CategoryItems.Where(x => x.UpperLevel?.Trim() == categoryItems[i].Key.Trim()).Count() != 0;
                            script.Append(CreateSelectBoxTag(categoryItems[i], selectedKeys, isHideCheckBox));
                        }
                        else
                        {
                            script.Append(CreateSelectBoxTag(categoryItems[i], selectedKeys, false));
                        }
                    }
                    CreateTreeTag(ref script, CategoryItems, categoryItems[i].Key, selectType, selectedKeys);

                    if (i == categoryItems.Count - 1)
                    {
                        if (!string.IsNullOrEmpty(upperLevel))
                        {
                            script.Append("] \r\n");
                            script.Append("} \r\n");
                        }
                    }
                    else
                    {
                        script.Append(", \r\n");
                    }
                }
            }
        }

        /// <summary>
        /// 選択用ボックスの作成を行います。
        /// </summary>
        /// <param name="categoryItem">カテゴリーまたはデータを指定します。</param>
        /// <param name="selectedKeys">選択済みのキー。</param>
        /// <param name="isHideCheckBox">チェックボックスを非表示するか。</param>
        /// <param name="isunselectable">チェックボックスをクリック不可にするか。</param>
        /// <returns>選択用ボックスのタグを返します。</returns>
        /// <summary>
        public string CreateSelectBoxTag(Category categoryItem, IEnumerable<string> selectedKeys, bool isHideCheckBox)
        {
            //categoryItem.Valueにシングルクォーテーションがあった場合、エスケープする必要がある
            //categoryItem.Value = categoryItem.Value.Replace("'", "\\'");
            categoryItem.Value = categoryItem.Value.Replace("'", "\"");
            return " title: '" + categoryItem.Value + "', icon: false, select: " + selectedKeys.Contains(categoryItem.Key).ToString().ToLower() + ", " +
                   "hideCheckbox: " + isHideCheckBox.ToString().ToLower() + ", key: '" + categoryItem.Key + "', icon: " + categoryItem.IconPath + " \r\n";
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 17-7-2019
        /// Description :Get the Admin Flag.
        /// <summary>
        public static string GetAdminFlagName(string adminFlag)
        {
            IDictionary<int?, string> dic = new CommonMasterInfo(SystemCommonMasterName.GrantName).GetDictionary();

            if (string.IsNullOrWhiteSpace(adminFlag))
            {
                return string.Empty;
            }
            else
            {
                return dic[Convert.ToInt32(adminFlag)];
            }
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 29-7-2019
        /// Description : Select statement with Dapper parameters and return list.
        /// <summary>
        public string SelectGroupName(string userCd)
        {
            // SELECT SQL生成
            StringBuilder stbSql = new StringBuilder();
            stbSql.Append(" select");

            stbSql.Append("  G.GROUP_NAME AS GROUP_NAME");
            stbSql.Append(" from");
            stbSql.Append("   M_USER U0");
            stbSql.Append("   left join V_GROUP G  on U0.GROUP_CD  = G.GROUP_CD and G.DEL_FLAG = 0");
            stbSql.Append("   left join M_USER  U1 on U0.APPROVER1 = U1.USER_CD and U1.DEL_FLAG = 0");
            stbSql.Append("   left join M_USER  U2 on U0.APPROVER2 = U2.USER_CD and U2.DEL_FLAG = 0");
            stbSql.Append(" where");
            stbSql.Append("  U0.USER_CD = @USER_CD ");

            DynamicParameters param = new DynamicParameters();
            param.Add("@USER_CD", userCd);
            UserInformation info = null;
            info = DbUtil.SelectSingle<UserInformation>(stbSql.ToString(), param);
            return info.GROUP_NAME;
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 29-7-2019
        /// Description :Get the UserName from UserID.
        /// <summary>
        public string GetUserName(string userCd)
        {

            var sqlSb = new StringBuilder();
            sqlSb.AppendLine("select USER_NAME from M_USER where USER_CD = @USER_CD");

            var parameters = new DynamicParameters();
            parameters.Add("@USER_CD", userCd);

            SqlConnection connection = DbUtil.Open();
            M_USER records;
            try
            {
                records = DbUtil.SelectSingle<M_USER>(sqlSb.ToString(), parameters);
                connection.Close();
            }
            catch (Exception ex)
            {
                records = null;
                connection.Close();
                throw ex;
            }

            return records.USER_NAME;
        }

        /// <summary>
        /// リスト出力用の検索SQLを取得します。
        /// </summary>
        /// <param name="sql">作成SQL文</param>
        /// <param name="_params">バインド変数格納Hashtable</param>
        public void GetSqlForCsv(WebCommonInfo user, ref string sql, ref DynamicParameters _params)
        {
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbSql = new StringBuilder();

            // 検索条件生成
            SetSqlWhere(ref sbWhere, ref _params, user);

            // SELECT SQL生成
            sbSql.Append(" select  U0.USER_CD ,U0.USER_NAME ,U0.USER_NAME_KANA,'' AS PASSWORD ,G1.GROUP_NAME AS GROUP_NAME ,U1.USER_NAME AS APPROVER1 ,U2.USER_NAME AS APPROVER2 ,U0.TEL ,U0.EMAIL ,G2.GROUP_NAME AS PREVIOUS_GROUP_NM");
            sbSql.Append("  ,W1.USAGE AS USAGE ,UR.USER_NAME AS REG_USER ,U0.REG_DATE ,UU.USER_NAME AS UPD_USER ,U0.UPD_DATE ,IIF(U0.DEL_FLAG = 0,'使用中','削除済') AS DEL_FLAGNAME ,UD.USER_NAME AS DEL_USER ,U0.DEL_DATE");
            sbSql.Append("  ,IIF(IsNull(U0.ADMIN_FLAG,0) = 1,'管理者','一般ユーザー') AS ADMIN_FLAG,IIF(IsNull(U0.BATCH_FLAG,0) = 0,'対象','対象外') AS BATCH_FLAG  from  M_USER U0");
            sbSql.Append("   left join V_GROUP G1  on U0.GROUP_CD  = G1.GROUP_CD left join V_GROUP G2  on U0.PREVIOUS_GROUP_CD  = G2.GROUP_CD left join M_WORKFLOW W1 on U0.FLOW_CD = W1.FLOW_CD left join M_USER  U1 on U0.APPROVER1 = U1.USER_CD");
            sbSql.Append("   left join M_USER  U2 on U0.APPROVER2 = U2.USER_CD left join M_USER  UR on U0.REG_USER_CD = UR.USER_CD left join M_USER  UU on U0.UPD_USER_CD = UU.USER_CD left join M_USER  UD on U0.DEL_USER_CD = UD.USER_CD");
            sbSql.Append(sbWhere.ToString());
            sbSql.Append(" order by  U0.USER_CD");
            sql = sbSql.ToString();
        }

        /// <summary>
        /// マスター項目名が削除用項目かどうかを判断します。
        /// </summary>
        /// <param name="TableColumnInformation">削除用項目か判断するテーブル管理情報のレコードを指定します。</param>
        /// <returns>削除項目の場合は<see cref="true"/>、そうでない場合は<see cref="false"/>を返します。</returns>
        public static bool IsDeleteColumn(S_TAB_COL_CONTROL TableColumnInformation)
        {
            switch (TableColumnInformation.COLUMN_NAME.ToUpper())
            {
                case (nameof(SystemColumn.DEL_USER_CD)):
                case (nameof(SystemColumn.DEL_DATE)):
                case (nameof(SystemColumn.DEL_FLAG)):
                    return true;
                default:
                    return false;
            }
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 17-7-2019
        /// Description : Update the records with Commit on success and rollback in case of failure.
        /// <summary>
        public bool DoSql(string sql, DynamicParameters param)
        {
            bool ret = false;
            SqlConnection connection = null;
            try
            {
                connection = DbUtil.Open(); // with Transaction
                int cnt = DbUtil.ExecuteUpdate(connection, sql, param);
                connection.BeginTransaction().Commit();
                connection.Close();
                ret = true;
            }
            catch (Exception ex)
            {
                connection.BeginTransaction().Rollback();
                connection.Close();
                throw ex;
            }

            return ret;
        }

        /// <summary>
        /// 検索用SQLを取得します。
        /// </summary>
        /// <returns>sql</returns>
        public string GetSql()
        {
            StringBuilder ret = new StringBuilder();

            ret.Append(" select");
            ret.Append("   AH.ACTION_ID");
            ret.Append("  ,AH.ACTION_TYPE_ID");
            ret.Append("  ,AT.ACTION_TEXT");
            ret.Append("  ,C.CHEM_NAME");
            ret.Append("  ,AH.BARCODE");
            ret.Append("  ,AH.USER_CD");
            // 2018/12/11 Upd Start TPE Kometani
            //ret.Append("  , U.USER_NAME");
            ret.Append("  ,AH.ACTION_USER_NM USER_NAME");
            // 2018/12/11 Upd End TPE Kometani
            ret.Append("  ,AH.LOC_ID");
            ret.Append("  ,AH.LOC_NAME");
            ret.Append("  ,AH.USAGE_LOC_ID");
            ret.Append("  ,AH.USAGE_LOC_NAME");
            ret.Append("  ,AH.INTENDED_USE_ID");
            ret.Append("  ,AH.INTENDED_NM");
            ret.Append("  ,AH.INTENDED");
            ret.Append("  ,AH.WORKING_TIMES");
            if (Common.DatabaseType == DatabaseType.Oracle)
            {
                ret.Append("  ,( CASE WHEN");
                ret.Append("           AH.CUR_GROSS_WEIGHT_G is null THEN ''");
                ret.Append("          ELSE ");
                ret.Append("             CONCAT(RegExp_Replace(to_char(AH.CUR_GROSS_WEIGHT_G, 'FM9990D9999'),'\\.0*$' ), 'g' )");
                ret.Append("          END ");
                ret.Append("   ) CUR_GROSS_WEIGHT");
                ret.Append("  ,( CASE WHEN");
                ret.Append("           AH.CUR_GROSS_WEIGHT_G is null THEN ''");
                ret.Append("          ELSE ");
                ret.Append("             RegExp_Replace(to_char(AH.CUR_GROSS_WEIGHT_G, 'FM9990D9999'),'\\.0*$' )");
                ret.Append("          END ");
                ret.Append("   ) CUR_GROSS_WEIGHT_VOLUME");
                ret.Append("  ,( CASE WHEN");
                ret.Append("           AH.CUR_GROSS_WEIGHT_G is null THEN ''");
                ret.Append("          ELSE ");
                ret.Append("             'g'");
                ret.Append("          END ");
                ret.Append("   ) CUR_GROSS_WEIGHT_UNITSIZE");
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                ret.Append("  ,(case when AH.CUR_GROSS_WEIGHT_G is NULL then '' else convert(nvarchar,AH.CUR_GROSS_WEIGHT_G) + 'g' end) CUR_GROSS_WEIGHT");
                ret.Append("  ,(case when AH.CUR_GROSS_WEIGHT_G is NULL then NULL else AH.CUR_GROSS_WEIGHT_G end) CUR_GROSS_WEIGHT_VOLUME");
                ret.Append("  ,(case when AH.CUR_GROSS_WEIGHT_G is NULL then '' else 'g' end) CUR_GROSS_WEIGHT_UNITSIZE");
            }
            ret.Append("  ,AH.MEMO");
            if (Common.DatabaseType == ZyCoaG.DatabaseType.Oracle)
            {
                ret.Append("  ,TO_CHAR(AH.REG_DATE, 'yyyy/mm/dd hh24:mi:ss') REG_DATE");
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                ret.Append("  ,format(AH.REG_DATE, 'yyyy/MM/dd HH:mm:ss') REG_DATE");
            }
            ret.Append(" from ");
            ret.Append("  T_ACTION_HISTORY AH");
            ret.Append("    left join M_ACTION_TYPE AT on AH.ACTION_TYPE_ID = AT.ACTION_TYPE_ID");
            ret.Append("    left join V_LOCATION     L on AH.LOC_ID         =  L.LOC_ID");
            //ret.Append("    left join M_USER         U on AH.USER_CD        =  U.USER_CD");   // 2018/12/11 Del TPE Kometani
            ret.Append("    left join T_STOCK        S on AH.STOCK_ID       =  S.STOCK_ID");
            ret.Append("    left join M_CHEM         C on S.CAT_CD          =  C.CHEM_CD");
            ret.Append(" where ");
            ret.Append("    AH.BARCODE = @BARCODE OR AH.STOCK_ID = @STOCK_ID ");
            ret.Append(" and AH.DEL_FLAG = 0");
            ret.Append(" order by ");
            ret.Append("    AH.ACTION_ID");

            return ret.ToString();
        }

        public string GetSqlForLedger()
        {

            StringBuilder ret = new StringBuilder();
            ret.Append(" select ML.REG_NO from T_MGMT_LEDGER ML ");
            ret.Append("  left outer join T_STOCK S on ML.REG_NO = S.REG_NO ");
            ret.Append("  WHERE S.BARCODE = @BARCODE OR S.STOCK_ID = @STOCK_ID;");
            return ret.ToString();
        }

        /// <summary>
        /// バーコードよりT_LEDGERとM_GROUPの検索
        /// </summary>
        public string GetSqlForLedgerGroup()
        {
            StringBuilder ret = new StringBuilder();
            ret.Append("select ML.GROUP_CD from T_MGMT_LEDGER ML ");
            ret.Append(" left outer join T_STOCK S on ML.REG_NO = S.REG_NO ");
            ret.Append(" where ML.GROUP_CD in ( ");
            ret.Append("     (select G1.GROUP_CD from M_USER U1 left outer join M_GROUP G1 on G1.GROUP_CD = U1.GROUP_CD where U1.USER_CD = :USER_CD), ");
            ret.Append("     (select G2.GROUP_CD from M_GROUP G2 where G2.GROUP_CD = (select G5.P_GROUP_CD from M_GROUP G5 left outer join M_USER U5 on G5.GROUP_CD = U5.GROUP_CD where U5.USER_CD = :USER_CD)), ");
            ret.Append("     (select G3.GROUP_CD from M_USER U3 left outer join M_GROUP G3 on U3.PREVIOUS_GROUP_CD = G3.GROUP_CD where U3.USER_CD = :USER_CD and G3.DEL_FLAG = 1), ");
            ret.Append("     (select G4.GROUP_CD from M_GROUP G4 where G4.GROUP_CD = (select G6.P_GROUP_CD from M_USER U6 left outer join M_GROUP G6 on U6.PREVIOUS_GROUP_CD = G6.GROUP_CD where U6.USER_CD = :USER_CD and G6.DEL_FLAG = 1 )) ");
            ret.Append(" ) ");
            ret.Append(" and (S.BARCODE = @BARCODE OR S.STOCK_ID = @STOCK_ID);");
            return ret.ToString();
        }


        /// <summary>
        /// Author: MSCGI 
        /// Created date: 16-7-2019
        /// Description : Frame the User Table update sql string with dapper along with parameters.
        /// <summary>
        public string GetSqlUpdate(WebCommonInfo webCommon, DynamicParameters param)
        {
            // パラメタ設定
            param.Add("@USER_CD", webCommon.USER_CD);
            param.Add("@USER_NAME", webCommon.USER_NAME);
            param.Add("@USER_NAME_KANA", webCommon.USER_NAME_KANA);
            param.Add("@GROUP_CD", webCommon.GROUP_CD);
            param.Add("@EMAIL", webCommon.EMAIL);
            param.Add("@TEL", webCommon.TEL);
            param.Add("@APPROVER1", webCommon.APPROVER1);
            param.Add("@APPROVER2", webCommon.APPROVER2);
            param.Add("@ADMIN_FLAG", webCommon.ADMIN_FLAG);
            param.Add("@FLOW_CD", webCommon.FLOW_CD);
            param.Add("@UPD_DATE", DateTime.Now);
            if (webCommon.DEL_FLAG == 0)
            {
                param.Add("@DEL_USER_CD", null);//DBNull.Value
                param.Add("@DEL_DATE", null);//DBNull.Value
                param.Add("@DEL_FLAG", 0);
            }
            else
            {
                param.Add("@DEL_USER_CD", webCommon.USER_CD);
                param.Add("@DEL_DATE", DateTime.Now);
                param.Add("@DEL_FLAG", 1);
            }
            if (webCommon.BATCH_FLG == true)
            {
                param.Add("@BATCH_FLAG", 1);
            }
            else
            {
                param.Add("@BATCH_FLAG", 0);
            }
            if (Common.IsValidStr(webCommon.TxtPassword))
            {
                param.Add("@PASSWORD", Common.GetSHA1Hash(webCommon.TxtPassword));
            }

            // SQL生成
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("UPDATE M_USER SET USER_NAME = @USER_NAME, USER_NAME_KANA = @USER_NAME_KANA, GROUP_CD = @GROUP_CD, EMAIL = @EMAIL, TEL = @TEL, APPROVER1 = @APPROVER1");
            sbSql.Append("  ,APPROVER2 = @APPROVER2, ADMIN_FLAG = @ADMIN_FLAG, FLOW_CD = @FLOW_CD, UPD_DATE = @UPD_DATE, DEL_USER_CD = @DEL_USER_CD, DEL_DATE = @DEL_DATE, DEL_FLAG = @DEL_FLAG, BATCH_FLAG = @BATCH_FLAG");
            if (Common.IsValidStr(webCommon.TxtPassword))
            {
                sbSql.Append("    , PASSWORD = @PASSWORD");
            }
            sbSql.Append("  ,UPD_USER_CD = @LOGIN_USER_CD WHERE USER_CD = @USER_CD");

            return sbSql.ToString();
        }

        /// <summary>
        /// M_USER新規登録用SQLを取得します。
        /// </summary>
        /// <returns>作成SQL文</returns>
        public string GetSqlInsert(WebCommonInfo webCommon, DynamicParameters param)
        {
            // パラメター設定
            param.Add("@USER_CD", webCommon.USER_CD);
            param.Add("@USER_NAME", webCommon.USER_NAME);
            param.Add("@USER_NAME_KANA", webCommon.USER_NAME_KANA);
            param.Add("@GROUP_CD", webCommon.GROUP_CD);
            param.Add("@EMAIL", webCommon.EMAIL);
            param.Add("@TEL", webCommon.TEL);
            param.Add("@APPROVER1", webCommon.APPROVER1);
            param.Add("@APPROVER2", webCommon.APPROVER2);
            param.Add("@ADMIN_FLAG", webCommon.ADMIN_FLAG);
            param.Add("@FLOW_CD", webCommon.FLOW_CD);
            param.Add("@LOGIN_USER_CD", Common.GetLogonForm().Id);

            if (webCommon.DEL_FLG == false)
            {
                param.Add("DEL_FLAG", 0);
            }
            else
            {
                param.Add("DEL_DATE", DateTime.Now);
                param.Add("DEL_FLAG", 1);
            }
            if (webCommon.BATCH_FLG == false)
            {
                param.Add("BATCH_FLAG", 0);
            }
            else
            {
                param.Add("BATCH_FLAG", 1);
            }
            if (string.IsNullOrWhiteSpace(webCommon.TxtPassword))
            {
                param.Add("PASSWORD", Common.GetSHA1Hash(Guid.NewGuid().ToString()));
            }
            else
            {
                param.Add("PASSWORD", Common.GetSHA1Hash(webCommon.TxtPassword));
            }

            // SQL生成
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("INSERT INTO M_USER(USER_CD, USER_NAME, USER_NAME_KANA, GROUP_CD, EMAIL, TEL, APPROVER1, APPROVER2, ADMIN_FLAG, PASSWORD, REG_USER_CD, FLOW_CD");
            if (webCommon.DEL_FLG == true)
            {
                sbSql.Append(",DEL_DATE ,DEL_USER_CD");
            }
            sbSql.Append(",DEL_FLAG,BATCH_FLAG) VALUES (@USER_CD, @USER_NAME, @USER_NAME_KANA, @GROUP_CD, @EMAIL, @TEL, @APPROVER1, @APPROVER2, @ADMIN_FLAG, @PASSWORD, @LOGIN_USER_CD, @FLOW_CD");
            if (webCommon.DEL_FLG == true)
            {
                sbSql.Append(",@DEL_DATE,@LOGIN_USER_CD");
            }
            sbSql.Append(",@DEL_FLAG,@BATCH_FLAG)");

            return sbSql.ToString();
        }

        /// <summary>
        /// M_USER削除用SQLを取得します。
        /// </summary>
        /// <param name="sql">作成SQL文</param>
        public string GetSqlDelete(WebCommonInfo webCommon, DynamicParameters param)
        {
            // パラメター設定
            param.Add("@USER_CD", webCommon.USER_CD);

            // SQL生成
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append(" UPDATE M_USER SET DEL_USER_CD = @LOGIN_USER_CD, DEL_FLAG = " + ((int)webCommon.DEL_FLAG).ToString());
            sbSql.Append(" WHERE USER_CD = @USER_CD");

            return sbSql.ToString();
        }


        /// <summary>
        /// M_USERを検索します。
        /// </summary>
        /// <paramMap name="AttestationFLG">0:Normal 1:LDAP</paramMap>        
        /// <returns>検索結果格納DataTable</returns>
        public WebCommonInfo GetUserTable(string user)
        {
            StringBuilder sbSql = new StringBuilder();
            DynamicParameters _param = new DynamicParameters();

            // M_USER検索SQL
            sbSql.Append("select * from M_USER where DEL_FLAG <> 1 and UPPER(USER_CD) = @USER_CD");

            _param.Add("@USER_CD", user);
            return DbUtil.SelectSingle<WebCommonInfo>(sbSql.ToString(), _param);
        }

        public void GetSql(ref string sql, ref DynamicParameters _params, WebCommonInfo mUser)
        {
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbSql = new StringBuilder();

            // 検索条件生成
            SetSqlWhere(ref sbWhere, ref _params, mUser);

            // SELECT SQL生成
            sbSql.Append(" select U0.USER_CD,U0.USER_NAME,U0.USER_NAME_KANA ,'' AS PASSWORD ,U0.GROUP_CD AS GROUP_CD ,G1.GROUP_NAME AS GROUP_NAME ,U1.USER_NAME AS APPROVER1 ,U2.USER_NAME AS APPROVER2");
            sbSql.Append("  ,U0.TEL, U0.EMAIL, U0.PREVIOUS_GROUP_CD, G2.GROUP_NAME AS PREVIOUS_GROUP_NM, U0.FLOW_CD, W1.USAGE, UR.USER_NAME AS REG_USER, U0.REG_DATE, UU.USER_NAME AS UPD_USER");
            sbSql.Append("  ,U0.UPD_DATE,IIF(U0.DEL_FLAG = 0,N'使用中',N'削除済') AS DEL_USERNAME,UD.USER_NAME AS DEL_USER,U0.DEL_DATE,IIF(IsNull(U0.ADMIN_FLAG,0) = 1,N'管理者',N'一般ユーザー') AS ADMIN_FLAGNAME,IIF(IsNull(U0.BATCH_FLAG,0) = 0,N'対象',N'対象外') AS BATCH_FLAGNAME");
            sbSql.Append(" from M_USER U0 left join V_GROUP G1  on U0.GROUP_CD  = G1.GROUP_CD left join V_GROUP G2  on U0.PREVIOUS_GROUP_CD  = G2.GROUP_CD left join M_WORKFLOW W1 on U0.FLOW_CD = W1.FLOW_CD");
            sbSql.Append("   left join M_USER  U1 on U0.APPROVER1 = U1.USER_CD left join M_USER  U2 on U0.APPROVER2 = U2.USER_CD left join M_USER  UR on U0.REG_USER_CD = UR.USER_CD left join M_USER  UU on U0.UPD_USER_CD = UU.USER_CD left join M_USER  UD on U0.DEL_USER_CD = UD.USER_CD");
            sbSql.Append(sbWhere.ToString());
            sbSql.Append(" order by  U0.USER_CD");
            sql = sbSql.ToString();
        }

        public void SetSqlWhere(ref StringBuilder sbWhere, ref DynamicParameters _params, WebCommonInfo mUser)
        {
            if (mUser.DEL_FLAG == 1)
            {
                sbWhere.Append(" where");
                sbWhere.Append(" U0.DEL_FLAG=@DEL_FLAG");
                _params.Add("@DEL_FLAG", 0);
            }
            else if (mUser.DEL_FLAG == 2)
            {
                sbWhere.Append(" where");
                sbWhere.Append("  U0.DEL_FLAG=@DEL_FLAG");
                _params.Add("@DEL_FLAG", 1);
            }
            else
            {
                sbWhere.Append(" where");
                sbWhere.Append("  U0.DEL_FLAG in (0,1)");
            }
            if (mUser.BATCH_FLAG == "2")
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  IsNull(U0.BATCH_FLAG,0)=@BATCH_FLAG");
                _params.Add("@BATCH_FLAG", 0);
            }
            else if (mUser.BATCH_FLAG == "3")
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  IsNull(U0.BATCH_FLAG,0)=@BATCH_FLAG");
                _params.Add("@BATCH_FLAG", 1);
            }

            if (mUser.USER_CD != null)
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  UPPER(U0.USER_CD)=@USER_CD");
                _params.Add("@USER_CD", mUser.USER_CD.ToUpper());
            }

            if (mUser.USER_NAME != null)
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  U0.USER_NAME like CONCAT('%',@USER_NAME,'%')");
                _params.Add("@USER_NAME", mUser.USER_NAME);
            }

            if (mUser.USER_NAME_KANA != null)
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  U0.USER_NAME_KANA like CONCAT('%',@USER_NAME_KANA,'%')");
                _params.Add("@USER_NAME_KANA", mUser.USER_NAME_KANA);
            }

            if (mUser.GROUP_CD != null)
            {
                sbWhere.Append(" AND");
                sbWhere.Append("  U0.GROUP_CD=@GROUP_CD");
                _params.Add("@GROUP_CD", mUser.GROUP_CD);
            }
        }

        public bool GetUpdateSql(ref string sql, ref Hashtable _params)
        {

            //新規か更新か判断
            Hashtable _para = new Hashtable();
            StringBuilder createSql = new StringBuilder();
            createSql.AppendLine("select * FROM M_USER WHERE USER_CD = :USER_CD");
            _para.Add("USER_CD", inputParam["USER_CD"]);
            DataTable dt = DbUtil.Select(createSql.ToString(), _para);

            createSql.Clear();

            if (dt.Rows.Count == 0)
            {
                //新規
                createSql.AppendLine("INSERT INTO M_USER(USER_CD, USER_NAME, USER_NAME_KANA, PASSWORD, GROUP_CD, APPROVER1, APPROVER2, TEL, EMAIL, FLOW_CD, ADMIN_FLAG, REG_USER_CD, REG_DATE, BATCH_FLAG");

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",DEL_USER_CD, DEL_DATE, DEL_FLAG");
                }
                createSql.AppendLine(")VALUES (:USER_CD");
                _params.Add("USER_CD", inputParam["USER_CD"]);
                createSql.AppendLine(",:USER_NAME");
                _params.Add("USER_NAME", inputParam["USER_NAME"]);
                createSql.AppendLine(",:USER_NAME_KANA");
                _params.Add("USER_NAME_KANA", inputParam["USER_NAME_KANA"]);
                createSql.AppendLine(",:PASSWORD");
                if (inputParam["PASSWORD"].Trim() == "")
                {
                    _params.Add("PASSWORD", Common.GetSHA1Hash(Guid.NewGuid().ToString()));
                }
                else
                {
                    _params.Add("PASSWORD", Common.GetSHA1Hash(inputParam["PASSWORD"]));
                }
                createSql.AppendLine(",:GROUP_CD");
                _para.Clear();
                _para.Add("GROUP_NAME", inputParam["GROUP_CD"]);
                dt = DbUtil.Select("SELECT GROUP_CD FROM V_GROUP WHERE GROUP_NAME = :GROUP_NAME", _para);
                _params.Add("GROUP_CD", dt.Rows[0]["GROUP_CD"]);

                createSql.AppendLine(",:APPROVER1");
                _para.Clear();
                _para.Add("USER_NAME", inputParam["APPROVER1"]);
                dt = DbUtil.Select("SELECT USER_CD FROM M_USER WHERE USER_NAME = :USER_NAME", _para);
                _params.Add("APPROVER1", dt.Rows[0]["USER_CD"]);

                createSql.AppendLine(",:APPROVER2");

                _para.Clear();
                _para.Add("USER_NAME", inputParam["APPROVER2"]);
                dt = DbUtil.Select("SELECT USER_CD FROM M_USER WHERE USER_NAME = :USER_NAME", _para);
                _params.Add("APPROVER2", dt.Rows.Count == 0 ? null : dt.Rows[0]["USER_CD"]);

                createSql.AppendLine(",:TEL");
                _params.Add("TEL", inputParam["TEL"]);

                createSql.AppendLine(",:EMAIL");
                _params.Add("EMAIL", inputParam["EMAIL"]);

                createSql.AppendLine(",:FLOW_CD");
                _para.Clear();
                _para.Add("USAGE", inputParam["FLOW_CD"]);
                dt = DbUtil.Select("SELECT FLOW_CD FROM M_WORKFLOW WHERE USAGE = :USAGE", _para);
                _params.Add("FLOW_CD", dt.Rows[0]["FLOW_CD"]);

                createSql.AppendLine(",:ADMIN_FLAG");
                _params.Add("ADMIN_FLAG", inputParam["FLOW_CD"] == "管理者" ? 1 : 0);

                createSql.AppendLine(",:REG_USER_CD");
                _params.Add("REG_USER_CD", Common.GetLogonForm().Id);
                createSql.AppendLine(",:REG_DATE");
                _params.Add("REG_DATE", DateTime.Now);
                createSql.AppendLine(",:BATCH_FLAG");
                _params.Add("BATCH_FLAG", inputParam["BATCH_FLAG"] == "対象外" ? 1 : 0);

                if (inputParam["DEL_FLAG"] == "削除済")
                {
                    createSql.AppendLine(",:DEL_USER_CD");
                    _params.Add("DEL_USER_CD", Common.GetLogonForm().Id);
                    createSql.AppendLine(",:DEL_DATE");
                    _params.Add("DEL_DATE", DateTime.Now);
                    createSql.AppendLine(",:DEL_FLAG");
                    _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);

                }

                createSql.AppendLine(") ");
            }
            else
            {
                //更新
                createSql.AppendLine("UPDATE M_USER SET USER_CD = :USER_CD");
                _params.Add("USER_CD", inputParam["USER_CD"]);
                createSql.AppendLine(",USER_NAME = :USER_NAME");
                _params.Add("USER_NAME", inputParam["USER_NAME"]);
                createSql.AppendLine(",USER_NAME_KANA = :USER_NAME_KANA");
                _params.Add("USER_NAME_KANA", inputParam["USER_NAME_KANA"]);
                if (inputParam["PASSWORD"].Trim() != "")
                {
                    createSql.AppendLine(",PASSWORD = :PASSWORD");
                    _params.Add("PASSWORD", Common.GetSHA1Hash(inputParam["PASSWORD"]));
                }
                createSql.AppendLine(",GROUP_CD = :GROUP_CD");
                _para.Clear();
                _para.Add("GROUP_NAME", inputParam["GROUP_CD"]);
                dt = DbUtil.Select("SELECT GROUP_CD FROM V_GROUP WHERE GROUP_NAME = :GROUP_NAME", _para);
                _params.Add("GROUP_CD", dt.Rows[0]["GROUP_CD"]);

                createSql.AppendLine(",APPROVER1 = :APPROVER1");
                _para.Clear();
                _para.Add("USER_NAME", inputParam["APPROVER1"]);
                dt = DbUtil.Select("SELECT USER_CD FROM M_USER WHERE USER_NAME = :USER_NAME", _para);
                _params.Add("APPROVER1", dt.Rows[0]["USER_CD"]);

                createSql.AppendLine(",APPROVER2 = :APPROVER2");
                _para.Clear();
                _para.Add("USER_NAME", inputParam["APPROVER2"]);
                dt = DbUtil.Select("SELECT USER_CD FROM M_USER WHERE USER_NAME = :USER_NAME", _para);
                _params.Add("APPROVER2", dt.Rows.Count == 0 ? null : dt.Rows[0]["USER_CD"]);

                createSql.AppendLine(",TEL = :TEL");
                _params.Add("TEL", inputParam["TEL"]);

                createSql.AppendLine(",EMAIL = :EMAIL");
                _params.Add("EMAIL", inputParam["EMAIL"]);

                createSql.AppendLine(",FLOW_CD = :FLOW_CD");
                _para.Clear();
                _para.Add("USAGE", inputParam["FLOW_CD"]);
                dt = DbUtil.Select("SELECT FLOW_CD FROM M_WORKFLOW WHERE USAGE = :USAGE", _para);
                _params.Add("FLOW_CD", dt.Rows[0]["FLOW_CD"]);

                createSql.AppendLine(",ADMIN_FLAG = :ADMIN_FLAG");
                _params.Add("ADMIN_FLAG", inputParam["FLOW_CD"] == "管理者" ? 1 : 0);

                createSql.AppendLine(",UPD_USER_CD = :UPD_USER_CD");
                _params.Add("UPD_USER_CD", Common.GetLogonForm().Id);

                createSql.AppendLine(",UPD_DATE = :UPD_DATE");
                _params.Add("UPD_DATE", DateTime.Now);

                createSql.AppendLine(",BATCH_FLAG = :BATCH_FLAG");
                _params.Add("BATCH_FLAG", inputParam["BATCH_FLAG"] == "対象外" ? 1 : 0);

                createSql.AppendLine(",DEL_USER_CD = :DEL_USER_CD");
                _params.Add("DEL_USER_CD", inputParam["DEL_FLAG"] == "削除済" ? Common.GetLogonForm().Id : null);
                createSql.AppendLine(",DEL_DATE = :DEL_DATE");
                _params.Add("DEL_DATE", inputParam["DEL_FLAG"] == "削除済" ? DateTime.Now.ToString() : null);

                createSql.AppendLine(",DEL_FLAG = :DEL_FLAG");
                _params.Add("DEL_FLAG", inputParam["DEL_FLAG"] == "削除済" ? 1 : 0);

                createSql.AppendLine(" WHERE ");
                createSql.AppendLine(" USER_CD = :USER_CD ");
            }

            sql = createSql.ToString();

            return true;
        }

        /// <summary>
        /// SQLを実行します。
        /// </summary>
        /// <param name="sql">SQL文</param>
        /// <param name="_params">バインド変数格納HashTable</param>
        public int DoSql(string sql, Hashtable _params)
        {
            int cnt = 0;
            DbContext ctx = null;
            try
            {
                ctx = DbUtil.Open(true);
                cnt = DbUtil.ExecuteUpdate(ctx, sql, _params);
                ctx.Commit();
                ctx.Close();
                ctx = null;
            }
            catch (Exception ex)
            {
                if (ctx != null)
                {
                    ctx.Rollback();
                    ctx.Close();
                }
                throw ex;
            }
            return cnt;
        }


        /// <summary>
        /// 入力チェックを行います。
        /// </summary>
        /// <returns>true：正常 / false：異常</returns>
        private bool DataCheck(out string errMessage)
        {
            errMessage = "";
            List<string> errorMessages = new List<string>();

            DataCheckBase _DataCheck = new DataCheckBase();

            //状態
            errMessage = _DataCheck.CheckDelFlag(inputParam["DEL_FLAG"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //社員コード
            errMessage = _DataCheck.CheckUserCD(inputParam["USER_CD"], "NID", true, false, true);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //社員名
            errMessage = _DataCheck.CheckUserName(inputParam["USER_NAME"], "社員名", true, false);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //社員名（ローマ字）
            errMessage = _DataCheck.CheckUserNameKana(inputParam["USER_NAME_KANA"], "社員名（ローマ字）", true);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";


            //部署名(出力形式が階層になっているので上位部署チェックとしてチェックはする)
            errMessage = _DataCheck.CheckPGroupName(inputParam["GROUP_CD"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //上長(Phase1：第1承認者)
            errMessage = _DataCheck.CheckUserName(inputParam["APPROVER1"], "上長", true, true);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //上長(Phase1：第2承認者)
            errMessage = _DataCheck.CheckUserName(inputParam["APPROVER2"], "代理者", false, true);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //TEL
            errMessage = _DataCheck.CheckTel(inputParam["TEL"], "内線番号", false);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //メールアドレス
            errMessage = _DataCheck.CheckEMail(inputParam["EMAIL"], "メールアドレス", true);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //フロー名
            errMessage = _DataCheck.CheckFlowName(inputParam["FLOW_CD"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //管理者権限
            errMessage = _DataCheck.CheckADMIN(inputParam["ADMIN_FLAG"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            //バッチ更新対象外
            errMessage = _DataCheck.CheckBatchFlag(inputParam["BATCH_FLAG"]);
            if (errMessage != "")
            {
                errorMessages.Add(errMessage);
            }
            errMessage = "";

            if (errorMessages.Count() > 0)
            {
                errMessage = string.Join(SystemConst.HtmlLinefeed, errorMessages);
                return false;
            }

            return true;
        }



        /// <summary>
        /// Author: MSCGI 
        /// Created date: 24-10-2019
        /// Description : Get the sql to update the records
        /// <summary>
        public int Import(out string errMessage)
        {
            errMessage = "";
            int cnt = 0;

            try
            {
                // データのエラーチェック
                if (!DataCheck(out errMessage)) return cnt;

                string sql = "";
                Hashtable _params = new Hashtable();

                if (GetUpdateSql(ref sql, ref _params))
                {
                    DoSql(sql, _params);
                    cnt++;
                }
            }
            catch (Exception ex)
            {
                errMessage = ex.Message;
            }

            return cnt;
        }
    }
}