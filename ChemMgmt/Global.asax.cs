﻿using ChemMgmt.Classes.util;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using ZyCoaG.Util;
using ZyCoaG.Classes.util;
using System;
using System.Web;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Diagnostics;
using ZyCoaG.BaseCommon;
using ChemMgmt.Models;
using ChemMgmt.Repository;
using ChemMgmt.Classes;

namespace ChemMgmt
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static ZyCoaG.Classes.UploadFiles UploadFiles { get; set; }
        //void Application_BeginRequest(object sender, EventArgs e)
        //{
        //    HttpContext.Current.Response.AddHeader("x-frame-options", "SAMEORIGIN");
        //}

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            
            // アプリケーションのスタートアップで実行するコードです            
            Common.webSiteRoot = Server.MapPath("");
            Common.DatabaseType = WebConfigUtil.DatabaseType;
            if (Common.DatabaseType == DatabaseType.Oracle)
            {
                DbUtil.strConnectionString = WebConfigUtil.OracleConnectionString;
            }
            if (Common.DatabaseType == DatabaseType.SqlServer)
            {
                DbUtil.strConnectionString = WebConfigUtil.SqlServerConnectionString;
            }
            Common.LimitResultCountCatalog = WebConfigUtil.LimitResultCountCatalog;
            Common.LimitResultCountStock = WebConfigUtil.LimitResultCountStock;
            Common.LimitResultCountHistory = WebConfigUtil.LimitResultCountHistory;
            Common.AlertResultCountCatalog = WebConfigUtil.AlertResultCountCatalog;
            Common.AlertResultCountStock = WebConfigUtil.AlertResultCountStock;
            Common.AlertResultCountHistory = WebConfigUtil.AlertResultCountHistory;
            Common.AlertResultDialog = WebConfigUtil.AlertResultDialog;
            Common.LimitResultDialog = WebConfigUtil.LimitResultDialog;
            Common.keyField = WebConfigUtil.CraisKeyField;
            Common.checkLevel = WebConfigUtil.CraisCheckLevel;
            Common.appName = WebConfigUtil.CraisAppName;
            Common.CRAIS_URL = WebConfigUtil.CraisUrl;
            GetLookupValues();
            //UploadFiles = new UploadFiles();
            setDebugMode();
        }

        void Application_End(object sender, EventArgs e)
        {
            //UploadFiles.Clear();
            //  アプリケーションのシャットダウンで実行するコードです
            if (Session != null)
            {
                Session.Abandon();
            }
        }

        //void Application_Error(object sender, EventArgs e)
        //{
        //    // ハンドルされていないエラーが発生したときに実行するコードです
        //}

        //void Session_Start(object sender, EventArgs e)
        //{
        //    // 新規セッションを開始したときに実行するコードです
        //    setDebugMode();

        //}

        [Conditional("DEBUG_MODE")]
        private void setDebugMode()
        {
            if (Session[Constant.CsessionNameUserINFO] == null)
            {
                //var user = new UserInfo.clsUser();
                //user.USER_CD = "dev";
                //user.USER_NAME = "開発ユーザー";
                //user.USER_NAME_KANA = "Developer";
                //user.ADMINFLG = Constant.CadminFlagAdmin;
                //Session.Add(SessionConst.IsLogin, true);
                //Session.Add(Constant.CsessionNameUserINFO, user);
            }
        }

        void Session_End(object sender, EventArgs e)
        {
            // セッションが終了したときに実行するコードです 
            // メモ: Web.config ファイル内で sessionstate モードが InProc に設定されているときのみ、
            // Session_End イベントが発生します。 session モードが StateServer か、または 
            // SQLServer に設定されている場合、イベントは発生しません。
            if (Session != null)
            {
                Session.Abandon();
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            var exception = Server.GetLastError();
            Server.ClearError();
            // send the error message to the ErrorController with its status code
            var routeData = new RouteData();
            routeData.Values.Add("controller", "ZC010");
            routeData.Values.Add("action", "ZC01090");

            if (exception.GetType() == typeof(HttpException))
            {
                var httpException = (HttpException)exception;
                var code = httpException.GetHttpCode();
                routeData.Values.Add("status", code);
            }
            else
            {
                routeData.Values.Add("status", 500);
            }

            routeData.Values.Add("error", exception);
            IController errorController = new ChemMgmt.Controllers.ErrorController();
            errorController.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }

        protected void GetLookupValues()
        {
            InventoryManagement model = new InventoryManagement();
            InventoryDao _GlobalDao = new InventoryDao();
            model = _GlobalDao.getLookupValues();
            Application[Common.LookupConstants.CSNCONDITION] = model.LookUpCSNCondition;
            Application[Common.LookupConstants.MAKERNAMECONDITION] = model.LookUpMakerNameCondition;
            Application[Common.LookupConstants.CSCODECONDITION] = model.LookUpCSCodeCondition;
            Application[Common.LookupConstants.BOTTLENUMCONDITION] = model.LookUpBottleNumCondition;
            Application[Common.LookupConstants.ACCEPTCONDITION] = model.LookUpAcceptCondition;
            Application[Common.LookupConstants.MAKERNAME] = model.LookUpMakerName;
            Application[Common.LookupConstants.STATUS] = model.LookUpStatus;
            Application[Common.LookupConstants.AMOUNTUNITSIZE] = model.LookUpAmountUnitSize;
            Application[Common.LookupConstants.INCLUDE] = model.LookUpInclude;
            Application[Common.LookupConstants.DDLLOCATION] = model.LookUpDDLLocation;

            LedgerCommonInfo ledgermodel = new LedgerCommonInfo();
            MgmtLedgerDropDowns dropdownlistDTO = new MgmtLedgerDropDowns();
            ledgermodel = dropdownlistDTO.ddlLedgerList(ledgermodel);

            Application[Common.LookupConstants.CHEMNAMECONDITION] = ledgermodel.LookupChemNameCondition;
            Application[Common.LookupConstants.PRODUCTNAME] = ledgermodel.LookupProductName;
            Application[Common.LookupConstants.CAS] = ledgermodel.LookupCas;
            Application[Common.LookupConstants.REGISTRATIONNUMBER] = ledgermodel.LookupRegistrationNo;
            Application[Common.LookupConstants.STATE] = ledgermodel.LookupState;
            Application[Common.LookupConstants.REASON] = ledgermodel.LookupReason;
            Application[Common.LookupConstants.TRANSACTIONVOLUME] = ledgermodel.LookupREG_TRANSACTION_VOLUME;
            Application[Common.LookupConstants.WORKFREQUENCY] = ledgermodel.LookupREG_WORK_FREQUENCY;
            Application[Common.LookupConstants.DISASTERPOSSIBILITY] = ledgermodel.LookupREG_DISASTER_POSSIBILITY;
            Application[Common.LookupConstants.CONTAINER] = ledgermodel.LookupSUB_CONTAINER;
            Application[Common.LookupConstants.MANUFACTURENAME] = ledgermodel.LookupManufacturerName;
            Application[Common.LookupConstants.UNITSIZEMASTERINFO] = ledgermodel.LookupUnitSizeMasterInfo;
            Application[Common.LookupConstants.USAGEMASTERINFO] = ledgermodel.LookupUsageMasterInfo;




        }
    }
}