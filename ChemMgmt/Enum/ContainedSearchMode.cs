﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Enum
{
    public enum ContainedSearchMode
    {
        /// <summary>
        /// 含有物検索
        /// </summary>
        Contained,
        /// <summary>
        /// 化学物質検索
        /// </summary>
        Chem,
    }
}