﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Models
{
    /// <summary>
    /// 消防法マスター(循環参照対応)
    /// </summary>
    public class V_FIRE : M_FIRE
    {
        /// <summary>
        /// 消防法(カテゴリ)
        /// </summary>
        public string FIRE_NAME_BASE { get; set; }

        /// <summary>
        /// 消防法(名称)
        /// </summary>
        public string FIRE_NAME_LAST { get; set; }

        /// <summary>
        /// アイコンURL
        /// </summary>
        public string IconUrl
        {
            get
            {
                return !string.IsNullOrWhiteSpace(FIRE_ICON_PATH) ? "../" + SystemConst.RegulationImageFolderName + "/" + FIRE_ICON_PATH : null;
            }
        }

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

        public string INPUT_FLAG { get; set; }


    }
}