﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG
{
    /// <summary>
    /// 商品詳細マスター
    /// </summary>
    public class M_QUANTITY : SystemColumn
    {
        public long QP_SEQNO { get; set; }

        public long? PRD_SEQNO { get; set; }

        public int? COMPOUND_LIST_ID { get; set; }

        public string CAT_CD { get; set; }

        public string UNITSIZE { get; set; }

        public int? UNITSIZE_ID { get; set; }

        public string CURR { get; set; }

        public int? SORT_ORDER { get; set; } = 9999;

        public string PRESERVATION_CONDITION { get; set; }

        public string TRANSPORT_CONDITION { get; set; }

        public int? PRICE { get; set; }
    }
}