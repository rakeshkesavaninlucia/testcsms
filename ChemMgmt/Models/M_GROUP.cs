﻿using System.ComponentModel.DataAnnotations;

namespace ZyCoaG
{
    /// <summary>
    /// 部署マスター
    /// </summary>
    public class M_GROUP : SystemColumn
    {
        /// <summary>
        /// 部署ID
        /// </summary>
        public int GROUP_ID { get; set; }

        /// <summary>
        /// 部署コード
        /// </summary>
        public string GROUP_CD { get; set; }
        public string UPDATE_GROUP_CD { get; set; }
        /// <summary>
        /// 部署名
        /// </summary>
        public string GROUP_NAME { get; set; }
        public string UPDATE_GROUP_NAME { get; set; }
        /// <summary>
        /// 上位部署コード
        /// </summary>
        public string P_GROUP_ID { get; set; }

        /// <summary>
        /// 上位部署コード
        /// </summary>
        public string P_GROUP_CD { get; set; }
        public string P_GROUP_NAME { get; set; }
        public string UPDATE_P_GROUP_NAME { get; set; }
        public string BATCH_FLAG { get; set; }
        public string UPDATE_BATCH_FLAGNAME  { get; set; }
        public bool BATCH_FLG { get; set; }
     //   public bool DEL_FLG { get; set; }
        public string Status { get; set; }
        public string UPDATE_DEL_FLAGNAME { get; set; }
      //  public string DEL_FLAGNAME { get; set; }
        public string BATCH_FLAGNAME { get; set; }
        public string DEL_USER_NM { get; set; }
        public string UPD_USER_NM { get; set; }
        public string REG_USER_NM { get; set; }
        
        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string UPLOAD_FILE { get; set; }
        
        ///// <summary>
        ///// テーブル名
        ///// </summary>
        //   public string TableName { get; set; }

        ///// <summary>
        ///// キー項目名
        ///// </summary>
        //   public string KeyColumnName { get; set; }

        ///// <summary>
        ///// 値項目名
        ///// </summary>
        //   public string ValueColumnName { get; set; }
        //    public string STR_DEL_FLAG { get; set; }

        ///// <summary>
        ///// 上位キー項目名
        ///// </summary>
        //  public string UpperLevelColumnName { get; set; }

        ///// <summary>
        ///// アイコンパス項目名
        ///// </summary>
        //  public string IconPathColumnName { get; set; }

        ///// <summary>
        ///// ★
        ///// </summary>
        // public string SelFlgName { get; set; }
    }
}