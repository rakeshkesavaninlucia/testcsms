﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG
{
    public class M_PRODUCT : SystemColumn
    {
        public long PRD_SEQNO { get; set; }

        public string PRODUCT_NAME_JP { get; set; }

        public string PRODUCT_NAME_EN { get; set; }

        public string PRODUCT_NAME_SEARCH { get; set; }

        public string GRADE { get; set; }

        public string PURITY { get; set; }

        public int? MAKER_ID { get; set; }

        public int? VENDOR_ID { get; set; }
    }
}