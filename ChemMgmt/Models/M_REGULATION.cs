﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace ZyCoaG
{
    /// <summary>
    /// 法規制マスター
    /// </summary>
    public class M_REGULATION : SystemColumn
    {
        /// <summary>
        /// 法規制種類ID
        /// </summary>
        public int REG_TYPE_ID { get; set; }

        /// <summary>
        /// クラスID
        /// </summary>
        public int CLASS_ID { get; set; }
        public string CLASS_NAME { get; set; }
        /// <summary>
        /// 法規制名
        /// </summary>
        public string REG_TEXT { get; set; }

        /// <summary>
        /// 法規制アイコンパス
        /// </summary>
        public string REG_ICON_PATH { get; set; }
        public string REG_ICON_NAME { get; set; }

        /// <summary>
        /// 優先順
        /// </summary>
        public int? SORT_ORDER { get; set; }

        /// <summary>
        /// 上位法規制種類ID
        /// </summary>
        public int? P_REG_TYPE_ID { get; set; }

        /// <summary>
        /// 作業記録管理
        /// </summary>
        public int? WORKTIME_MGMT { get; set; }
        public string WORKTIME_MGMT_NAME { get; set; }

        /// <summary>
        /// 作業記録必要物質
        /// </summary>
        public int? WORK_RECORD { get; set; }

        /// <summary>
        /// ばく露作業報告該当物質
        /// </summary>
        public int? EXPOSURE_REPORT { get; set; }
        public string EXPOSURE_REPORT_VALUE { get; set; }
        public bool SEL_FLAG { get; set; }
        public string STATUS { get; set; }
        public string REG_ACRONYM_TEXT { get; set; }
        public string FIRE_SIZE { get; set; }
        public string UNITSIZE_ID { get; set; }
        public string UNITSIZE_NAME { get; set; }
        public string WEIGHT_MGMT { get; set; }
        public string WEIGHT_MGMT_NAME { get; set; }
        public string KEY_MGMT { get; set; }
        public string KEY_MGMT_NAME { get; set; }
        public string EXAMINATION_FLAG { get; set; }
        public bool EXAMINATION_FLAG_VALUE { get; set; }
        public string MEASUREMENT_FLAG { get; set; }

        public string MARKS { get; set; }
        public string CAS_NO { get; set; }
        public string REG_TEXT_CONDITION { get; set; }
        public string DEL_FLAG2 { get; set; }
        public string RA_FLAG { get; set; }
        public bool RA_FLAG2 { get; set; }
        public string DEL_USER_NM { get; set; }
        public string P_REG_TYPE_VALUE { get; set; }
        public string CLASSIFICATION { get; set; }
        public bool MEASUREMENT_FLAG_VALUE { get; set; }
        public string DEL_FLAG_VALUE { get; set; }
        public bool DEL_FLG { get; set; }
        public string REG_USER_NAME { get; set; }
        public string UPD_USER_NAME { get; set; }
        public string DEL_USER_NAME { get; set; }
        public List<SelectListItem> P_REG_NAMES { get; set; }



    }
}