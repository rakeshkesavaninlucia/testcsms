﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace ZyCoaG
{
    /// <summary>
    /// 動作管理テーブル
    /// </summary>
    public class T_ACTION_HISTORY : SystemColumn
    {
        [Key]
        public int ACTION_ID { get; set; }

        public int? ACTION_TYPE_ID { get; set; }

        public string BARCODE { get; set; }

        public string USER_CD { get; set; }

        public int? LOC_ID { get; set; }

        public string LOC_NAME { get; set; }

        public int? USAGE_LOC_ID { get; set; }

        public string USAGE_LOC_NAME { get; set; }

        public string INTENDED_USE_ID { get; set; }

        public string INTENDED_NM { get; set; }

        public string INTENDED { get; set; }

        public long? WORKING_TIMES { get; set; }

        public string CUR_GROSS_WEIGHT_G { get; set; }

        public string MEMO { get; set; }

        public int? STOCK_ID { get; set; }

        public string ACTION_USER_NM { get; set; }

        public string ACTION_GROUP_NAME { get; set; }

        public string ACTION_GROUP_CD { get; set; }
    }
}