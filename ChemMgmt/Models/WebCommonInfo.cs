﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace ZyCoaG
{
    public class WebCommonInfo : SystemColumn
    {
        [AllowHtml]
        public string select_keys { get; set; }
        public string MaintainanceMasterSubElement { get; set; }
        public IEnumerable<SelectListItem> LookupMasterName { get; set; }
        public IEnumerable<SelectListItem> ddlAdminFlag { get; set; }
        public IEnumerable<SelectListItem> ddlFlowCd { get; set; }
        public IEnumerable<SelectListItem> drpPattern { get; set; }
        public string ddlpattern { get; set; }

        public string TxtPassword { get; set; }
        public string TxtPasswordCheck { get; set; }

        /// <summary>クライアント端末のIPアドレス</summary>
        public string IP_ADDRESS { get; set; }
        /// <summary>ユーザーCD</summary>
        public string USER_CD { get; set; }
        public string USER { get; set; }
        public string TYPE { get; set; }
        public string IDENTITY { get; set; }
        public string CALLER_IDENTITY { get; set; }
        /// <summary>ユーザー名</summary>
        public string USER_NAME { get; set; }
        /// <summary>
        /// ユーザー名+権限名
        /// </summary>
        public string USER_NAME_KANA { get; set; }
        public string PASSWORD { get; set; }
        public string APPROVER1 { get; set; }

        public string APPROVER2 { get; set; }

        /// <summary>第一承認者</summary>
        public string APPROVER1_NAME { get; set; }
        /// <summary>第二承認者</summary>
        public string APPROVER2_NAME { get; set; }
        /// <summary>内線番号</summary>
        public string TEL { get; set; }
        /// <summary>メールアドレス</summary>
        public string EMAIL { get; set; }
        /// <summary>納品場所ID</summary>
        public string ROOM_ID { get; set; }
        /// <summary>管理者権限（0:一般 1:管理者）</summary>
        public string ADMIN_FLAG { get; set; }
        /// <summary>
        /// 権限名
        /// </summary>

        /// <summary>メモ？</summary>
        public string MEMO { get; set; }
        /// <summary>言語設定（0:日本語 1:英語）</summary>
        public string BILINGUAL_MODE { get; set; }
        /// <summary>部署コード</summary>
        public string GROUP_CD { get; set; }
        /// <summary>部署名</summary>
        public string GROUP_NAME { get; set; }
        /// <summary>地域ID</summary>
        public string AREA_ID { get; set; }
        /// <summary>所属部署１</summary>
      //  public string PREVIOUS_GROUP_CD { get; set; }
        public string PREVIOUS_GROUP_NM { get; set; }
        /// <summary>フローコード</summary>
        public string FLOW_CD { get; set; }
        /// <summary>削除フラグ</summary>
        public bool DEL_FLG { get; set; }
        /// <summary>バッチ更新対象フラグ</summary>
        public string BATCH_FLAG { get; set; }
        public bool BATCH_FLG { get; set; }
        /// <summary>発注権限</summary>
        //public string[] ORDER_REG_TYPE_ID { get; set; }
        ///// <summary>使用権限</summary>
        //public string[] USE_REG_TYPE_ID { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string UPLOAD_FILE { get; set; }
        /// <summary>参照権限（0:一般ユーザー 1:管理者）</summary>
        /// Author: MSCGI 
        /// Created date: 18-7-2019
        /// Description : Below portion is for ZC01024 screen UserSetting confirmation in webcommon subsystem.
        public string REG_USER { get; set; }
        public string UPD_USER { get; set; }
        public string DEL_USER { get; set; }
        public string FLOW_NAME { get; set; }
        public string USAGE { get; set; }
        public string DEL_FLAGNAME { get; set; }
        public string DEL_USERNAME { get; set; }
        public string ADMIN_FLAGNAME { get; set; }
        public string BATCH_FLAGNAME { get; set; }
        public string REFERENCE_AUTHORITY { get; set; }
        public string BottleFlow { get; set; }

        //Inventory Search

        public IEnumerable<SelectListItem> LookUpMakerName { get; set; }
        public IEnumerable<SelectListItem> LookUpCSNCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpMakerNameCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpBottleNumCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpAcceptCondition { get; set; }

        public List<InventoryUsageHistory> listInventoryUsageHistory { get; set; }
        public int cSNCondition { get; set; }
        public int makerNameCondition { get; set; }
        public int bottleNumberCondition { get; set; }
        public int acceptCondition { get; set; }
        public string makerName { get; set; }
        public string StockId { get; set; }
        public string BottleNumber { get; set; }
        public string Barocde { get; set; }

    }

    public class InventoryUsageHistory
    {
        public string SEQ_NO { get; set; }
        public string ACTION_ID { get; set; }
        public string ACTION_TYPE_ID { get; set; }
        public string ACTION_TEXT { get; set; }
        public string CHEM_NAME { get; set; }
        public string BARCODE { get; set; }
        public string USER_NAME { get; set; }
        public string USER_CD { get; set; }
        public string LOC_ID { get; set; }
        public string LOC_NAME { get; set; }
        public string USAGE_LOC_ID { get; set; }
        public string USAGE_LOC_NAME { get; set; }
        public string INTENDED_USE_ID { get; set; }
        public string INTENDED_NM { get; set; }
        public string INTENDED { get; set; }
        public string WORKING_TIMES { get; set; }
        public string CUR_GROSS_WEIGHT_VOLUME { get; set; }
        public string CUR_GROSS_WEIGHT { get; set; }
        public string CUR_GROSS_WEIGHT_UNITSIZE { get; set; }
        public string MEMO { get; set; }
        public string REG_DATE { get; set; }
    }
}
