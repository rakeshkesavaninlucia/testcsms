﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Models
{
    /// <summary>
    /// テーブル管理システムデータ
    /// </summary>
    public class S_TAB_CONTROL
    {
        /// <summary>
        /// テーブル名
        /// </summary>
        public string TABLE_NAME { get; set; }

        /// <summary>
        /// テーブル表示名
        /// </summary>
        public string TABLE_DISPLAY_NAME { get; set; }

        /// <summary>
        /// メンテナンスURL
        /// </summary>
        public string MAINTENANCE_URL { get; set; }

        /// <summary>
        /// 表示順
        /// </summary>
        public int DISPLAY_ORDER { get; set; }

        /// <summary>
        /// 新規登録可能か
        /// </summary>
        public bool IS_INSERT { get; set; }

        /// <summary>
        /// 更新可能か
        /// </summary>
        public bool IS_UPDATE { get; set; }

        /// <summary>
        /// 削除可能か
        /// </summary>
        public bool IS_DELETE { get; set; }

        /// <summary>
        /// 表示するか
        /// </summary>
        public bool IS_VIEW { get; set; }
    }
}