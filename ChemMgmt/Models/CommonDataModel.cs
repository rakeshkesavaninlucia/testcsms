﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Models
{
    /// <summary>
    /// IDと名称、順番で構成される汎用データモデルです。
    /// </summary>
    public class CommonDataModel<T>
    {
        /// <summary>
        /// ID
        /// </summary>
        public T Id { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 順番
        /// </summary>
        public int Order { get; set; }

        /// <summary>
        /// 削除済みか
        /// </summary>
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 空で<see cref="CommonDataModel"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        public CommonDataModel()
        {
            this.Id = default(T);
        }

        /// <summary>
        /// IDと名称を指定し、<see cref="CommonDataModel"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        /// <param name="Id">IDを<see cref="T"/>で指定します。</param>
        /// <param name="Name">名称を<see cref="string"/>で指定します。</param>
        public CommonDataModel(T Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }

        /// <summary>
        /// IDと名称と削除済みかを指定し、<see cref="CommonDataModel"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        /// <param name="Id">IDを<see cref="T"/>で指定します。</param>
        /// <param name="Name">名称を<see cref="string"/>で指定します。</param>
        public CommonDataModel(T Id, string Name, bool IsDeleted)
        {
            this.Id = Id;
            this.Name = Name;
            this.IsDeleted = IsDeleted;
        }

        /// <summary>
        /// IDと名称と順番を指定し、<see cref="CommonDataModel"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        /// <param name="Id">IDを<see cref="T"/>で指定します。</param>
        /// <param name="Name">名称を<see cref="string"/>で指定します。</param>
        /// <param name="Order">順番を<see cref="int"/>で指定します。</param>
        public CommonDataModel(T Id, string Name, int Order)
        {
            this.Id = Id;
            this.Name = Name;
            this.Order = Order;
        }

        /// <summary>
        /// IDと名称と順番と削除済みを指定し、<see cref="CommonDataModel"/>クラスの新しいインスタンスを初期化します。
        /// </summary>
        /// <param name="Id">IDを<see cref="T"/>で指定します。</param>
        /// <param name="Name">名称を<see cref="string"/>で指定します。</param>
        /// <param name="Order">順番を<see cref="int"/>で指定します。</param>
        public CommonDataModel(T Id, string Name, int Order, bool IsDeleted)
        {
            this.Id = Id;
            this.Name = Name;
            this.Order = Order;
            this.IsDeleted = IsDeleted;
        }

        /// <summary>
        /// IDと名称を指定して格納します。
        /// </summary>
        /// <param name="Id">IDを<see cref="T"/>で指定します。</param>
        /// <param name="Name">名称を<see cref="string"/>で指定します。</param>
        public void SetValue(T Id, string Name)
        {
            this.Id = Id;
            this.Name = Name;
        }

        /// <summary>
        /// IDと名称と削除済みかを指定して格納します。
        /// </summary>
        /// <param name="Id">IDを<see cref="T"/>で指定します。</param>
        /// <param name="Name">名称を<see cref="string"/>で指定します。</param>
        /// <param name="IsDeleted">削除済みかを<see cref="bool"/>で指定します。</param>
        public void SetValue(T Id, string Name, bool IsDeleted)
        {
            this.Id = Id;
            this.Name = Name;
            this.IsDeleted = IsDeleted;
        }

        /// <summary>
        /// IDと名称と順番を指定して格納します。
        /// </summary>
        /// <param name="Id">IDを<see cref="T"/>で指定します。</param>
        /// <param name="Name">名称を<see cref="string"/>で指定します。</param>
        /// <param name="Order">順番を<see cref="int"/>で指定します。</param>
        public void SetValue(T Id, string Name, int Order)
        {
            this.Id = Id;
            this.Name = Name;
            this.Order = Order;
        }

        /// <summary>
        /// IDと名称と順番と削除済みかを指定して格納します。
        /// </summary>
        /// <param name="Id">IDを<see cref="T"/>で指定します。</param>
        /// <param name="Name">名称を<see cref="string"/>で指定します。</param>
        /// <param name="Order">順番を<see cref="int"/>で指定します。</param>
        /// <param name="IsDeleted">削除済みかを<see cref="bool"/>で指定します。</param>
        public void SetValue(T Id, string Name, int Order, bool IsDeleted)
        {
            this.Id = Id;
            this.Name = Name;
            this.Order = Order;
            this.IsDeleted = IsDeleted;
        }
    }
}