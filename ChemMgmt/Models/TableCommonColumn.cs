﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZyCoaG
{
    /// <summary>
    /// テーブル共通項目です。
    /// </summary>
    public class TableCommonColumn
    {
        /// <summary>
        /// 登録日時
        /// </summary>
        public DateTime? REG_DATE { get; set; }

        /// <summary>
        /// 更新日時
        /// </summary>
        public DateTime? UPD_DATE { get; set; }

        /// <summary>
        /// 削除日時
        /// </summary>
        public DateTime? DEL_DATE { get; set; }

        /// <summary>
        /// 削除フラグ
        /// </summary>
        public int? DEL_FLAG { get; set; } = 0;
    }
}
