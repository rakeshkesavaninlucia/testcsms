﻿namespace ZyCoaG
{
    /// <summary>
    /// 法規制マスター
    /// </summary>
    public class M_REG : SystemColumn
    {
        /// <summary>
        /// 法規制種類ID
        /// </summary>
        public int REG_TYPE_ID { get; set; }

        /// <summary>
        /// 法規制名
        /// </summary>
        public string REG_TEXT { get; set; }

        /// <summary>
        /// 法規制アイコンパス
        /// </summary>
        public string REG_ICON_PATH { get; set; }

        /// <summary>
        /// 優先順
        /// </summary>
        public int? SORT_ORDER { get; set; }

        /// <summary>
        /// 上位法規制種類ID
        /// </summary>
        public int? P_REG_TYPE_ID { get; set; }
    }
}