﻿using ChemMgmt.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Models
{
    public enum UsageStatusCondtionType
    {
        /// <summary>
        /// 使用中
        /// </summary>
        Using,

        /// <summary>
        /// 削除済み
        /// </summary>
        Deleted,

        /// <summary>
        /// 保存中
        /// </summary>
        Temporary, // 20180723 FJ)Shinagawa Add

        /// <summary>
        /// 全て
        /// </summary>
        All
    }
    public class UsageStatusListMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        /// <summary>
        /// 選択項目に空白を追加するか
        /// </summary>
        protected override bool IsAddSpace { get; set; } = false;

        /// <summary>
        /// 全使用状態を取得します。
        /// </summary>
        /// <returns>全使用状態を<see cref="IEnumerable{CommonDataModel{int?}}"/>で返します。</returns>
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var searchCondition = new List<CommonDataModel<int?>>();
            searchCondition.Add(new CommonDataModel<int?>((int)UsageStatusCondtionType.Using, "使用中", 0));
            searchCondition.Add(new CommonDataModel<int?>((int)UsageStatusCondtionType.Deleted, "削除済み", 1));
            // 20180723 FJ)Shinagawa Mod Start ->
            //searchCondition.Add(new CommonDataModel<int?>((int)UsageStatusCondtionType.All, "全て", 2));
            searchCondition.Add(new CommonDataModel<int?>((int)UsageStatusCondtionType.Temporary, "保存中", 2));
            searchCondition.Add(new CommonDataModel<int?>((int)UsageStatusCondtionType.All, "全て", 3));
            // 20180723 FJ)Shinagawa Mod End <-
            return searchCondition;
        }
    }
}