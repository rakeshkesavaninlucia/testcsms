﻿using System.ComponentModel.DataAnnotations;

namespace ZyCoaG
{
    /// <summary>
    /// 汎用マスター
    /// </summary>
    public class M_COMMON : SystemColumn
    {
        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string UPLOAD_FILE { get; set; }
        /// <summary>
        /// 汎用ID
        /// </summary>
        public int COMMON_ID { get; set; }

        /// <summary>
        /// テーブル名
        /// </summary>
        public string TABLE_NAME { get; set; }

        /// <summary>
        /// キー値
        /// </summary>
        public int? KEY_VALUE { get; set; }

        /// <summary>
        /// 表示値
        /// </summary>
        public string DISPLAY_VALUE { get; set; }

        /// <summary>
        /// 表示順
        /// </summary>
        public int? DISPLAY_ORDER { get; set; }
        // REG_USER_CD
        //UPD_USER_CD]
        //DEL_USER_CD]
        //REG_DATE]
        //UPD_DATE]
        //DEL_DATE]
        //public string DEL_FLAG { get; set; }
        public string SEL_FLAG { get; set; }

        public string DEL_FLAG_VALUE { get; set; }
        public bool DEL_FLG { get; set; }
        public string REG_USER_NAME { get; set; }
        public string UPD_USER_NAME { get; set; }
        public string DEL_USER_NAME { get; set; }

        public int? TABLE_NAME_ID { get; set; }
        public string TABLENAME { get; set; }
        public int? KEY_VALUE_ID { get; set; }
        public int? DISPLAY_VALUE_ID { get; set; }
        public int? DISPLAY_ORDER_ID { get; set; }

    }
}