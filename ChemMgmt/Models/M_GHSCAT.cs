﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Models
{
    public class M_GHSCAT : SystemColumn
    {
        /// <summary>
        /// GHS分類ID
        /// </summary>
        public int GHSCAT_ID { get; set; }

        /// <summary>
        /// GHS分類名
        /// </summary>
        public string GHSCAT_NAME { get; set; }

        /// <summary>
        /// GHS分類アイコン
        /// </summary>
        public string GHSCAT_ICON_PATH { get; set; }

        /// <summary>
        /// 優先順
        /// </summary>
        public int? SORT_ORDER { get; set; }

        /// <summary>
        /// 上位GHS分類ID
        /// </summary>
        public int? P_GHSCAT_ID { get; set; }
    }
}