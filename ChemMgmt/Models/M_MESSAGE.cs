﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

/// Author: MSCGI 
/// Created date: 18-7-2019
/// Description : Message Model.
namespace ZyCoaG
{
    public class M_MESSAGE
    {
        public int MSG_ID { get; set; }
        [AllowHtml]
        public int P_MSG_TEXT { get; set; }
        [Display(Name = "Message Details:")]
        [Required(ErrorMessage = "Message description is required.")]
        [UIHint("tinymce_custom")]
        [AllowHtml]
        public string MSG_TEXT { get; set; }
        public string LBL_MSG { get; set; }

    }
}