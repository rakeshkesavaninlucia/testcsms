﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using ZyCoaG;

namespace ChemMgmt.Models
{
    public class M_FIRE : SystemColumn
    {
        /// <summary>
        /// 消防法ID
        /// </summary>
        public int FIRE_ID { get; set; }      


        /// <summary>
        /// 消防法分類名
        /// </summary>
        public string FIRE_NAME { get; set; }

        /// <summary>
        /// 消防法分類アイコン
        /// </summary>
        public string FIRE_ICON_PATH { get; set; }

        /// <summary>
        /// 優先順
        /// </summary>
        public int? SORT_ORDER { get; set; }

        /// <summary>
        /// 上位消防法分類ID
        /// </summary>
        public int? P_FIRE_ID { get; set; }


        public string labelMode { get; set; }

        public List<M_FIRE> listMFire { get; set; }

        public IEnumerable<SelectListItem> ddlType { get; set; }

        public IEnumerable<SelectListItem> ddlReportPlace { get; set; }
        public IEnumerable<SelectListItem> ddlLocation { get; set; }
        public IEnumerable<SelectListItem> ddlStatus { get; set; }
        public ViewFireDetails viewFire { get; set; }

        public List<ViewFireDetails> listViewFireDetails { get; set; }

        public string txtReportName { get; set; }

        public bool IsSetSession { get; set; }
        public bool IsPostBack { get; set; }
        public string hdnLocationId { get; set; }

        public string STATUS { get; set; }
        //public string DEL_FLAG { get; set; }
        public string REPORT_ID { get; set; }
        public string REPORT_NAME { get; set; }
        public string REPORT_YMD { get; set; }
        public string REPORT_SUBMIT { get; set; }
        public string REPORT_PLACE { get; set; }
        public string REPORT_LOCATION { get; set; }
        public string FIRE_SIZE { get; set; }
        public string REG_USER_NAME { get; set; }
        //public string REG_DATE { get; set; }
        public string UPD_USER_NAME { get; set; }
        //public string UPD_DATE { get; set; }
        public string DEL_USER_NAME { get; set; }
        //public string DEL_DATE { get; set; }

        public string REPORT_TYPE { get; set; }

        public string REPORT_PLACE_KEY { get; set; }

        public string REPORT_LOCATIONID { get; set; }

        public string hdnLocationId2 { get; set; }

        public string hdnLocationId1 { get; set; }
        public string tblFireData { get; set; }
        public string TYPE { get; set; }

        public string mode { get; set; }
        public string locname1 { get; set; }

        public string locname2 { get; set; }
        public string REPORT_LOCATION1 { get; set; }
        public string REPORT_LOCATION2 { get; set; }
        public string REPORT_LOCATION_NAME1 { get; set; }
        public string REPORT_LOCATION_NAME2 { get; set; }
        public List<string> lstLocation1 { get; set; }
        public List<string> lstLocation2 { get; set; }

        public Table tblFireView { get; set; }
        public string searchFlag { get; set; }
    }

    public class ViewFireDetails
    {
        public string fireName { get; set; }
        public string fireSize { get; set; }
        public string unitSize { get; set; }
        public string fireId { get; set; }
        public string txtFireSize { get; set; }
        public string lblMultiple { get; set; }
    }

    public class tblFireClass
    {
        public string id { get; set; }
        public string lblFireSize { get; set; }
        public string txtFireSize { get; set; }
        public string mulFirSize { get; set; }
        public string fireTotal { get; set; }
        public string mulFireTotal { get; set; }
        public string hdnFireSize { get; set; }
        public string hdnFactor { get; set; }
        public string hdnRegTypeId { get; set; }

    }
}