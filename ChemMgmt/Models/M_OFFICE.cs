﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Models
{
    /// <summary>
    /// 社内ルールマスター
    /// </summary>
    public class M_OFFICE : SystemColumn
    {
        /// <summary>
        /// 社内ルールID
        /// </summary>
        public int OFFICE_ID { get; set; }

        /// <summary>
        /// 社内ルール名
        /// </summary>
        public string OFFICE_NAME { get; set; }

        /// <summary>
        /// 社内ルールアイコン
        /// </summary>
        public string OFFICE_ICON_PATH { get; set; }

        /// <summary>
        /// 優先順
        /// </summary>
        public int? SORT_ORDER { get; set; }

        /// <summary>
        /// 上位社内ルールID
        /// </summary>
        public int? P_OFFICE_ID { get; set; }
    }
}