﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Ledger;
using ChemMgmt.Nikon.Models;
using IdentityManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.Nikon;

namespace ChemMgmt.Models
{
    public class LedgerCommonInfo : LedgerModel
    {
        public List<LedgerCommonInfo> ChemCommonList { get; set; }
        public List<LedgerCommonInfo> LedgerCommonList { get; set; }
        public List<LedgerRegulationModel> RegulationList { get; set; }
        public string ApplicationNo { get; set; }
        public string ViewModeValue { get; set; }
        public string RemandMemo { get; set; }
        public int hdnmode { set; get; }

        public int hdnApplino { set; get; }

        public string hdnupdatetype { set; get; }


        public bool FLG_LOC_HAZARD { get; set; }
        public int CHEM_NAME_CONDITION { get; set; }

        public Boolean FLG_RISK_ASSESSMENT { get; set; } = true;

        public int PRODUCT_NAME_CONDITION { get; set; }

        public int CAS_NO_CONDITION { get; set; }

        public int REG_NO_CONDITION { get; set; }

        public int FLOW_SEARCH_TARGET { get; set; }

        public int LEDGER_STATUS { get; set; } = (int)UsageStatusCondtionType.Using;
        public IEnumerable<SelectListItem> LookupChemNameCondition { get; set; }
        public IEnumerable<SelectListItem> LookupProductName { get; set; }
        public IEnumerable<SelectListItem> LookupCas { get; set; }
        public IEnumerable<SelectListItem> LookupRegistrationNo { get; set; }
        public IEnumerable<SelectListItem> LookupState { get; set; }
        public IEnumerable<SelectListItem> LookupReason { get; set; }
        public IEnumerable<SelectListItem> LookupREG_DISASTER_POSSIBILITY { get; set; }
        public IEnumerable<SelectListItem> LookupREG_WORK_FREQUENCY { get; set; }
        public IEnumerable<SelectListItem> LookupREG_TRANSACTION_VOLUME { get; set; }
        public IEnumerable<SelectListItem> LookupSUB_CONTAINER { get; set; }
        public IEnumerable<SelectListItem> LookupUnitSizeMasterInfo { get; set; }
        public IEnumerable<SelectListItem> LookupUsageMasterInfo { get; set; }
        public IEnumerable<SelectListItem> LookupManufacturerName { get; set; }
        public List<LedgerCommonInfo> ContainedModel { get; set; }
        public string hiddenRegulationIds { get; set; }
        public string managementgroupIds { get; set; }
        public string hiddenLocationIds { get; set; }
        public string hiddenusageIds { get; set; }
        public string hiddenstoragelocationIds { get; set; }
        public string hiddengroupIds { get; set; }

        public string hdnUSAGEAMOUNT { get; set; }
        public int? hdnUSAGEUNITSIZEID { get; set; }

        public int? hdnLOC_ID { get; set; }

        public string hdnAMOUNT { get; set; }
        public string hdnOperation { get; set; }

        public int? hdnUNITSIZE_ID { get; set; }

        public string hdnCHEM_CD { get; set; }
        public List<LedgerOldRegisterNumberModel> objLedgerOldRegisterNumberModel { get; set; }

        public List<LedgerLocationModel> StorageLocation { get; set; }
        public LedgerCommonInfo objLedgerRegulationIds { get; set; }
        public List<UserInformation> objUserInformation { get; set; }
        public List<LedgerRegulationModel> objLedgerRegulationModel { get; set; }
        public List<LedgerUsageLocationModel> objLedgerUsageLocationModel { get; set; }
        public IEnumerable<T_LED_WORK_HISTORY> objT_LED_WORK_HISTORY { get; set; }
        public List<LedgerProtectorModel> objLedgerProtectorModel { get; set; }
        public List<LedgerRiskReduceModel> objLedgerRisk { get; set; }
        public List<LedgerCommonInfo> _listLedgerModel { get; set; }
        public int machiCount { get; set; }
        public string StorageRowid { get; set; }
        public int flowCount { get; set; }
        public string lblalreadySetValue { get; set; }
        public IEnumerable<int> REG_COLLECTION { get; set; }
        public IEnumerable<string> GROUP_CD_COLLECTION { get; set; }
        public IEnumerable<int> ACTION_LOC_COLLECTION { get; set; }
        public IEnumerable<int> USAGE_LOC_COLLECTION { get; set; }
        public int UniqueRowId { get; set; }

        public string MEASURES_BEFORE_SCORE_UNIT { get; set; }

        public string ADMIN_FLAG { get; set; }
        public string MEASURES_BEFORE_RANK_DISP { get; set; }
        public string MEASURES_AFTER_RANK_DISP { get; set; }
        public string MEASURES_AFTER_SCORE_UNIT { get; set; }
        public bool FLG_UNREGISTERED_CHEM { get; set; }
        public string ErrorMessage { get; set; }

        public int? FLOW_Mode { get; set; }

        public class ApplicableLawGridModel
        {
            public string REG_TEXT_BASE { get; set; }
            public string MARKS { get; set; }
            public string REG_TEXT_LAST { get; set; }
            public string RowCountHeader { get; set; }
        }

        public bool btnApproval { get; set; }
        public bool btnRemand { get; set; }

        public bool SendBackVisibility { get; set; }
        public bool btnChemRegister { get; set; }
        public bool btnRegain { get; set; }
        public bool btnsendback { get; set; }

        public bool btndeletematter { get; set; }
        public bool btnReferenceRegister { get; set; }

        public bool ReferenceRegistration { get; set; } = false;

        public bool btntemporarysave { get; set; }


        public bool btnRegister { get; set; }
        public bool btnRevocation { get; set; }

        public bool btnApplicationchange { get; set; }
        public int? flowid { get; set; }

    }
}