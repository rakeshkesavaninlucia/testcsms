﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Models
{
    /// <summary>
    /// 法規制マスター(循環参照対応)
    /// </summary>
    public class V_REGULATION : M_REGULATION
    {
        /// <summary>
        /// 法規制名(カテゴリ)
        /// </summary>
        public string REG_TEXT_BASE { get; set; }

        /// <summary>
        /// 法規制名(名称)
        /// </summary>
        public string REG_TEXT_LAST { get; set; }

        /// <summary>
        /// 指定数量
        /// </summary>
        public decimal FIRE_SIZE { get; set; }


        /// <summary>
        /// アイコンURL
        /// </summary>
        public string IconUrl
        {
            get
            {
                return !string.IsNullOrWhiteSpace(REG_ICON_PATH) ? "../" + SystemConst.RegulationImageFolderName + "/" + REG_ICON_PATH : null;
            }
        }

        //2019/03/12 TPE.Rin Add
        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

        public string INPUT_FLAG { get; set; }
    }
}