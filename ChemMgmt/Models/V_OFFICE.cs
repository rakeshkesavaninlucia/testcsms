﻿using ChemMgmt.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Models
{
    /// <summary>
    /// 社内ルールマスター(循環参照対応)
    /// </summary>
    public class V_OFFICE : M_OFFICE
    {
        /// <summary>
        /// 社内ルール(カテゴリ)
        /// </summary>
        public string OFFICE_NAME_BASE { get; set; }

        /// <summary>
        /// 社内ルール(名称)
        /// </summary>
        public string OFFICE_NAME_LAST { get; set; }

        /// <summary>
        /// アイコンURL
        /// </summary>
        public string IconUrl
        {
            get
            {
                return !string.IsNullOrWhiteSpace(OFFICE_ICON_PATH) ? "../" + SystemConst.RegulationImageFolderName + "/" + OFFICE_ICON_PATH : null;
            }
        }

        /// <summary>
        /// 選択
        /// </summary>
        public bool CHECK { get; set; }

        public string INPUT_FLAG { get; set; }

    }
}