﻿using ChemMgmt.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ZyCoaG
{
    public class T_MGMT_LED_USAGE_LOC
    {
        [Key]
        [Display(Name = "登録番号")]
        public string REG_NO { get; set; }

        [Key]
        [Display(Name = "使用場所シーケンス（連番）")]
        public int? USAGE_LOC_SEQ { get; set; }

        [Display(Name = "場所ID")]
        public int? LOC_ID { get; set; }

        [Display(Name = "最大使用量")]
        [NumberLength(8, 3)]
        [RegularNumber]
        [Required]
        public string AMOUNT { get; set; }

        [Display(Name = "単位")]
        [Required]
        public int? UNITSIZE_ID { get; set; }

        [Display(Name = "労働安全衛生法の用途")]
        [Required]
        public int? ISHA_USAGE { get; set; }

        [Display(Name = "労働安全衛生法の用途(その他)")]
        public string ISHA_OTHER_USAGE { get; set; }

        [Display(Name = "化学物質取扱責任者")]
        //[StringLength(30)]    2018/08/26 TPE Delete
        //[Required]            2018/08/26 TPE Delete
        [ExcludingProhibition]
        public string CHEM_OPERATION_CHIEF { get; set; }

        [Display(Name = "小分け容器の有無")]
        [Required]
        public int? SUB_CONTAINER { get; set; }

        [Display(Name = "装置への投入")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string ENTRY_DEVICE { get; set; }

        [Display(Name = "保護具(その他)")]
        [StringLength(100)]
        [ExcludingProhibition]
        public string OTHER_PROTECTOR { get; set; }

        [Display(Name = "措置前点数")]
        [NumberLength(5, 2)]
        [RegularNumber]
        public decimal? MEASURES_BEFORE_SCORE { get; set; }

        [Display(Name = "措置前ランク")]
        [Required]
        public int? MEASURES_BEFORE_RANK { get; set; }

        [Display(Name = "措置後点数")]
        [NumberLength(5, 2)]
        [RegularNumber]
        public decimal? MEASURES_AFTER_SCORE { get; set; }

        [Display(Name = "措置後ランク")]
        [Required]
        public int? MEASURES_AFTER_RANK { get; set; }

        [Display(Name = "CAS")]
        public string CAS_NO { get; set; }
        public string RA { get; set; }
        public DateTime? REG_DATE { get; set; }
        public string UPD_DATE { get; set; }
        public string DEL_DATE { get; set; }
        public string DEL_FLAG { get; set; }
        public string RA_FILENAME { get; set; }
        [Display(Name = "職場の取扱量")]
        [Required]
        public int? REG_TRANSACTION_VOLUME { get; set; }
        [Display(Name = "作業頻度")]
        [Required]
        public int? REG_WORK_FREQUENCY { get; set; }
        [Display(Name = "災害発生の可能性")]
        [Required]
        public int? REG_DISASTER_POSSIBILITY { get; set; }
    }
}