﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


/// Author: MSCGI
/// Created date: 18-7-2019
/// Description : For Dapper M_USER_MAKER Model class
/// <summary>
namespace ZyCoaG
{
        public class M_USER_MAKER //: SystemColumn
        {
        public int MAKER_ID { get; set; }
        public string MAKER_NAME { get; set; }
        public string MSG_TEXT { get; set; }
        public string PATTERN_NAME { get; set; }
        public string USER_CD { get; set; }
        public int DEL_FLAG { get; set; }
        
    }
}