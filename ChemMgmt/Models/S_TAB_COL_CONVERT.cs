﻿namespace ChemMgmt.Models
{
    /// <summary>
    /// テーブル項目変換システムデータ
    /// </summary>
    public class S_TAB_COL_CONVERT
    {
        /// <summary>
        /// テーブル名
        /// </summary>
        public string TABLE_NAME { get; set; }

        /// <summary>
        /// 項目名
        /// </summary>
        public string COLUMN_NAME { get; set; }

        /// <summary>
        /// キー
        /// </summary>
        public string CONVERT_KEY { get; set; }
        
        /// <summary>
        /// 値
        /// </summary>
        public int CONVERT_VALUE { get; set; }
    }
}