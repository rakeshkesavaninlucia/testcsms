﻿using ChemMgmt.Classes.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Models
{ /// <summary>
  /// テーブル項目管理システムデータ
  /// </summary>
    public class S_TAB_COL_CONTROL
    {
        /// <summary>
        /// データ型の定数です。
        /// </summary>
        public enum DataType
        {
            /// <summary>
            /// 文字
            /// </summary>
            Character = 0,

            /// <summary>
            /// 数字
            /// </summary>
            Number = 1,

            /// <summary>
            /// 日付
            /// </summary>
            Date = 2,

            /// <summary>
            /// チェック
            /// </summary>
            Check = 3,

            /// <summary>
            /// 選択
            /// </summary>
            Select = 4,

            /// <summary>
            /// NID入力
            /// </summary>
            NIDInput = 5,
        }

        /// <summary>
        /// テーブル名
        /// </summary>
        public string TABLE_NAME { get; set; }

        /// <summary>
        /// テーブル項目名
        /// </summary>
        public string COLUMN_NAME { get; set; }

        /// <summary>
        /// テーブル項目表示名
        /// </summary>
        public string COLUMN_DISPLAY_NAME { get; set; }

        /// <summary>
        /// テーブル項目表示名に必須マークをつけた値を返します。
        /// </summary>
        public string DisplayNameAndRequiredMark
        {
            get
            {
                if (IS_REQUIRED) return COLUMN_DISPLAY_NAME.AddRequiredMark();
                return COLUMN_DISPLAY_NAME;
            }
        }

        /// <summary>
        /// 表示順
        /// </summary>
        public int DISPLAY_ORDER { get; set; }

        /// <summary>
        /// データ型
        /// </summary>
        public DataType DATA_TYPE { get; set; }

        /// <summary>
        /// データ桁数
        /// </summary>
        public int DATA_LENGTH { get; set; }

        /// <summary>
        /// データ桁数(小数)
        /// </summary>
        public int DEC_DATA_LENGTH { get; set; }

        /// <summary>
        /// 検索条件か
        /// </summary>
        public bool IS_SEARCH { get; set; }

        /// <summary>
        /// キーか
        /// </summary>
        public bool IS_KEY { get; set; }

        /// <summary>
        /// 識別項目か
        /// </summary>
        public bool IS_IDENTITY { get; set; }

        /// <summary>
        /// 一意か
        /// </summary>
        public bool IS_UNIQUE { get; set; }

        /// <summary>
        /// 文字列中のスペースを除いて一意か
        /// </summary>
        public bool IS_UNIQUE_SPACE { get; set; }

        /// <summary>
        /// 必須か
        /// </summary>
        public bool IS_REQUIRED { get; set; }

        /// <summary>
        /// 半角か
        /// </summary>
        public bool IS_HALFWIDTH { get; set; }

        /// <summary>
        /// パスワードか
        /// </summary>
        public bool IS_PASSWORD { get; set; }

        /// <summary>
        /// 変換テーブル
        /// </summary>
        public string CONVERT_TABLE { get; set; }

        /// <summary>
        /// 変換キー項目
        /// </summary>
        public string CONVERT_KEY { get; set; }

        /// <summary>
        /// 変換値項目
        /// </summary>
        public string CONVERT_VALUE { get; set; }

        /// <summary>
        /// 予備1(半角英字か)
        /// </summary>
        public bool SUB_1 { get; set; }

        /// <summary>
        /// 予備2(メールアドレスか)
        /// </summary>
        public bool SUB_2 { get; set; }

        /// <summary>
        /// 予備3
        /// </summary>
        public bool SUB_3 { get; set; }

        /// <summary>
        /// 予備4
        /// </summary>
        public bool SUB_4 { get; set; }

        /// <summary>
        /// 予備5
        /// </summary>
        public bool SUB_5 { get; set; }

        /// <summary>
        /// 予備6
        /// </summary>
        public bool SUB_6 { get; set; }

        /// <summary>
        /// 予備7
        /// </summary>
        public bool SUB_7 { get; set; }

        /// <summary>
        /// 予備8
        /// </summary>
        public bool SUB_8 { get; set; }

        /// <summary>
        /// 予備9
        /// </summary>
        public bool SUB_9 { get; set; }

        /// <summary>
        /// 予備10
        /// </summary>
        public bool SUB_10 { get; set; }

        /// <summary>
        /// 選択データ
        /// </summary>
        public Dictionary<string, string> SelectData { get; set; }

        /// <summary>
        /// 変換データ
        /// </summary>
        public Dictionary<string, string> ConvertData { get; set; }

        /// <summary>
        /// 検索条件入力値
        /// </summary>
        public string InputValue { get; set; }
    }
}