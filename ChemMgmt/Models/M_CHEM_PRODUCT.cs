﻿using ChemMgmt.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Models
{
    /// <summary>
    /// 化学物質商品マスター
    /// </summary>
    public class M_CHEM_PRODUCT : SystemColumn
    {
        /// <summary>
        /// 化学物質コード
        /// </summary>
        [Key]
        public string CHEM_CD { get; set; }

        /// <summary>
        /// 商品シーケンス
        /// </summary>
        [Key]
        public int PRODUCT_SEQ { get; set; }

        /// <summary>
        /// メーカーID
        /// </summary>
        [Display(Name = "メーカー名")]
        [Required]
        public int? MAKER_ID { get; set; }

        Dictionary<string,int> MAKER_IDS;
        /// <summary>
        /// 商品名
        /// </summary>
        [Display(Name = "商品名")]
        [StringLength(255)]
        [Required]
        [ExcludingProhibition]
        public string PRODUCT_NAME { get; set; }

        /// <summary>
        /// 容量
        /// </summary>
        [Display(Name = "容量")]
        [NumberLength(8, 3)]
        [Halfwidth]
        [RegularNumber]
        [Required]
        public string UNITSIZE { get; set; }

        /// <summary>
        /// 単位ID
        /// </summary>
        [Display(Name = "単位")]
        [Required]
        public int? UNITSIZE_ID { get; set; }

        private string _PRODUCT_BARCODE { get; set; }
        /// <summary>
        /// 商品バーコード
        /// </summary>
        [Display(Name = "商品バーコード")]
        [Unique]
        [StringLength(30)]
        [Halfwidth]
        [ExcludingProhibition]
        public string PRODUCT_BARCODE
        {
            get
            {
                return _PRODUCT_BARCODE;
            }
            set
            {
                _PRODUCT_BARCODE = value?.ToUpper();
            }
        }

        /// <summary>
        /// SDSリンクURL
        /// </summary>
        [Display(Name = "SDSリンクURL")]
        [StringLength(4000)]
        public string SDS_URL { get; set; }

        /// <summary>
        /// SDS更新日
        /// </summary>
        public DateTime? SDS_UPD_DATE { get; set; }

        /// <summary>
        /// SDS更新日
        /// </summary>
        [Display(Name = "SDS更新日")]
        [Halfwidth]
        [Date]
        public string SdsUpdateDate { get; set; }

        public int TempPRODUCT_SEQ { get; set; }
    }
}