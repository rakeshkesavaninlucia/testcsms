﻿using COE.Common;
using System;
using System.Data;

namespace ZyCoaG
{
    /// <summary>
    /// マスターのシステムで使用する項目です。
    /// </summary>
    public class SystemColumn
    {
        /// <summary>
        /// 登録ユーザーコード
        /// </summary>
        public string REG_USER_CD { get; set; }

        /// <summary>
        /// 更新ユーザーコード
        /// </summary>
        public string UPD_USER_CD { get; set; }

        /// <summary>
        /// 削除ユーザーコード
        /// </summary>
        public string DEL_USER_CD { get; set; }

        /// <summary>
        /// 登録日時
        /// </summary>
        public DateTime? REG_DATE { get; set; }

        /// <summary>
        /// 更新日時
        /// </summary>
        public DateTime? UPD_DATE { get; set; }

        /// <summary>
        /// 削除日時
        /// </summary>
        public DateTime? DEL_DATE { get; set; }

        /// <summary>
        /// 削除フラグ
        /// </summary>
        public int? DEL_FLAG { get; set; } = 0;
        public bool DEL_FLG { get; set; }
        public string DEL_FLAGNAME { get; set; }

        /// <summary>
        /// DataRowからモデルへ代入する
        /// </summary>
        /// <param name="datarow">DataRow</param>
        public void SetDataRow(DataRow datarow)
        {
            var properties = GetType().GetProperties();
            foreach (var prop in properties)
            {
                if (!prop.CanWrite)
                {
                    continue;
                }
                else if (!datarow.Table.Columns.Contains(prop.Name))
                {
                    continue;
                }
                var col = datarow[prop.Name];
                if (Convert.IsDBNull(col) && prop.PropertyType != typeof(string))
                {
                    prop.SetValue(this, null);
                }
                else if (Convert.IsDBNull(col) && prop.PropertyType == typeof(string))
                {
                    prop.SetValue(this, string.Empty);
                }
                else if (prop.PropertyType == typeof(int?))
                {
                    prop.SetValue(this, OrgConvert.ToNullableInt32(col));
                }
                else if (prop.PropertyType == typeof(long?))
                {
                    prop.SetValue(this, OrgConvert.ToNullableInt64(col));
                }
                else if (prop.PropertyType == typeof(decimal?))
                {
                    prop.SetValue(this, OrgConvert.ToNullableDecimal(col));
                }
                else if (prop.PropertyType == typeof(DateTime?))
                {
                    prop.SetValue(this, OrgConvert.ToNullableDateTime(col));
                }
                else
                {
                    prop.SetValue(this, Convert.ChangeType(col, prop.PropertyType));
                }
            }
        }
    }
}