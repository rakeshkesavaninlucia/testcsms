﻿

using System.ComponentModel.DataAnnotations;

namespace ZyCoaG
{
    /// <summary>
    /// Author: MSCGI 
    /// Created date: 18-7-2019
    /// Description : Workflow Model.
    public class M_WORKFLOW : SystemColumn
    {
        public int FLOW_ID { get; set; }
        public string FLOW_CD { get; set; }

        public string FLOW_NAME { get; set; }
        public string FLOW_TEXT { get; set; }
        public string FLOW_TYPE_ID { get; set; }
        public string USAGE { get; set; }
        public string FLOW_HIERARCHY { get; set; }
        public string APPROVAL_TARGET1 { get; set; }
        public string APPROVAL_USER_CD1 { get; set; }
        public string SUBSITUTE_USER_CD1 { get; set; }
        //public string APPROVAL_TARGET1_NM { get; set; }
        public string APPROVAL_USER_CD1_NM { get; set; }
        public string SUBSITUTE_USER_CD1_NM { get; set; }
        public string APPROVAL_TARGET2 { get; set; }
        public string APPROVAL_USER_CD2 { get; set; }
        public string SUBSITUTE_USER_CD2 { get; set; }
        //public string APPROVAL_TARGET2_NM { get; set; }
        public string APPROVAL_USER_CD2_NM { get; set; }
        public string SUBSITUTE_USER_CD2_NM { get; set; }
        public string APPROVAL_TARGET3 { get; set; }
        public string APPROVAL_USER_CD3 { get; set; }
        public string SUBSITUTE_USER_CD3 { get; set; }
        //public string APPROVAL_TARGET3_NM { get; set; }
        public string APPROVAL_USER_CD3_NM { get; set; }
        public string SUBSITUTE_USER_CD3_NM { get; set; }
        public string APPROVAL_TARGET4 { get; set; }
        public string APPROVAL_USER_CD4 { get; set; }
        public string SUBSITUTE_USER_CD4 { get; set; }
        //public string APPROVAL_TARGET4_NM { get; set; }
        public string APPROVAL_USER_CD4_NM { get; set; }
        public string SUBSITUTE_USER_CD4_NM { get; set; }
        public string APPROVAL_TARGET5 { get; set; }
        public string APPROVAL_USER_CD5 { get; set; }
        public string SUBSITUTE_USER_CD5 { get; set; }
        //public string APPROVAL_TARGET5_NM { get; set; }
        public string APPROVAL_USER_CD5_NM { get; set; }
        public string SUBSITUTE_USER_CD5_NM { get; set; }
        public string APPROVAL_TARGET6 { get; set; }
        public string APPROVAL_USER_CD6 { get; set; }
        public string SUBSITUTE_USER_CD6 { get; set; }
        //public string APPROVAL_TARGET6_NM { get; set; }
        public string APPROVAL_USER_CD6_NM { get; set; }
        public string SUBSITUTE_USER_CD6_NM { get; set; }

        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string UPLOAD_FILE { get; set; }
        public string REG_USER_NM { get; set; }
        public string UPD_USER_NM { get; set; }
        public string DEL_USER_NM { get; set; }

        public string UPDATE_FLOW_TYPE_ID { get; set; }
        public string UPDATE_FLOW_CD { get; set; }
        public int UPDATE_FLOW_ID { get; set; }
        public string UPDATE_FLOW_NAME { get; set; }
        public string UPDATE_DEL_FLAGNAME { get; set; }
        
        public string UPDATE_USAGE { get; set; }
        public string UPDATE_FLOW_HIERARCHY { get; set; }
        public string UPDATE_APPROVAL_TARGET1 { get; set; }
        public string UPDATE_APPROVAL_TARGET2 { get; set; }
        public string UPDATE_APPROVAL_TARGET3 { get; set; }
        public string UPDATE_APPROVAL_TARGET4 { get; set; }
        public string UPDATE_APPROVAL_TARGET5 { get; set; }
        public string UPDATE_APPROVAL_TARGET6 { get; set; }
        public string UPDATE_APPROVAL_USER_CD1 { get; set; }
        public string UPDATE_APPROVAL_USER_CD2 { get; set; }
        public string UPDATE_APPROVAL_USER_CD3 { get; set; }
        public string UPDATE_APPROVAL_USER_CD4 { get; set; }
        public string UPDATE_APPROVAL_USER_CD5 { get; set; }
        public string UPDATE_APPROVAL_USER_CD6 { get; set; }
        public string UPDATE_SUBSITUTE_USER_CD1 { get; set; }
        public string UPDATE_SUBSITUTE_USER_CD2 { get; set; }
        public string UPDATE_SUBSITUTE_USER_CD3 { get; set; }
        public string UPDATE_SUBSITUTE_USER_CD4 { get; set; }
        public string UPDATE_SUBSITUTE_USER_CD5 { get; set; }
        public string UPDATE_SUBSITUTE_USER_CD6 { get; set; }

        /// <summary>
        /// 承認目的
        /// </summary>
        public string[] APPROVAL_TARGET
        {
            get
            {
                return new string[]
                {
                    string.Empty,
                    APPROVAL_TARGET1, APPROVAL_TARGET2, APPROVAL_TARGET3,
                    APPROVAL_TARGET4, APPROVAL_TARGET5, APPROVAL_TARGET6,
                };
            }
        }

        /// <summary>
        /// 承認者
        /// </summary>
        public string[] APPROVAL_USER_CD
        {
            get
            {
                return new string[]
                {
                    string.Empty,
                    APPROVAL_USER_CD1, APPROVAL_USER_CD2, APPROVAL_USER_CD3,
                    APPROVAL_USER_CD4, APPROVAL_USER_CD5, APPROVAL_USER_CD6,
                };
            }
        }

        /// <summary>
        /// 代行者
        /// </summary>
        public string[] SUBSITUTE_USER_CD
        {
            get
            {
                return new string[]
                {
                    string.Empty,
                    SUBSITUTE_USER_CD1, SUBSITUTE_USER_CD2, SUBSITUTE_USER_CD3,
                    SUBSITUTE_USER_CD4, SUBSITUTE_USER_CD5, SUBSITUTE_USER_CD6,
                };
            }
        }

    }
}