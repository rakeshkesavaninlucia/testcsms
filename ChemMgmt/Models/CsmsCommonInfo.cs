﻿using ChemMgmt.Classes;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Chem;
using ChemMgmt.Nikon.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace ChemMgmt.Models
{
    public class CsmsCommonInfo : ChemModel
    {
        public List<SelectListItem> LookupFigure { get; set; }
        public IEnumerable<SelectListItem> LookupClassification { get; set; }
        public IEnumerable<SelectListItem> LookupFigureName { get; set; }
        public IEnumerable<SelectListItem> LookupChemSubCode { get; set; }
        public IEnumerable<SelectListItem> LookupChemNameCondition { get; set; }
        public IEnumerable<SelectListItem> LookupProductName { get; set; }
        public IEnumerable<SelectListItem> LookupCas { get; set; }
        public IEnumerable<SelectListItem> LookupManufacturerName { get; set; }
        public IEnumerable<SelectListItem> LookupKeyMgmt { get; set; }
        public IEnumerable<SelectListItem> LookupWeightMgmt { get; set; }
        public IEnumerable<SelectListItem> LookupSpeMedChekup { get; set; }
        public IEnumerable<SelectListItem> LookupWorkEnvMeasurement { get; set; }
        public IEnumerable<SelectListItem> LookupContent { get; set; }
        public IEnumerable<SelectListItem> LookupState { get; set; }
        public IEnumerable<SelectListItem> LookupOfManufacturer { get; set; }
        public IEnumerable<SelectListItem> LookupUnitSizeMasterInfo { get; set; }
        public List<CsmsCommonInfo> ChemCommonList { get; set; }
        public List<ChemAliasModel> ChemAliasModel { get; set; }
        public List<ChemAliasModel> OldChemAliasModel { get; set; }
        public List<ChemContainedModel> ContainedModel { get; set; }
        public List<V_OFFICE> OfficeGridModel { get; set; }
        public List<V_GHSCAT> GHSModel { get; set; }
        public List<V_REGULATION> RegulationModel { get; set; }
        public List<V_FIRE> FireModel { get; set; }

        public string HiddenMode { get; set; }
        public string Hiddenoperation { get; set; }
        public string HiddenTempflag { get; set; }

        public string HiddenChemCD { get; set; }

        public string HiddenRefChemCD { get; set; }
        public string hiddenRegulationIds { get; set; }

        public string hiddenfire { get; set; }
        public string hiddenGHS { get; set; }
        public string hiddenoffice { get; set; }

        public string hiddenbusyCheckid { get; set; }

        public int? hdnmode { get; set; }       

        public ChemSearchModel ChemCommonformList { get; set; }

        public decimal? MAX_CONTAINED_RATE { get; set; }
        public decimal? MIN_CONTAINED_RATE { get; set; }
        public List<string> Errormessages { get; set; }
        /// <summary>
        /// GHSアイコン
        /// </summary>
        public string GHS_ICON_PATH { get; set; }
     
        public List<ChemProductModel> ChemProductListModel { get; set; }
        public string SDS_ID { get; set; }
        public int CHEM_SEARCH_TARGET { get; set; }
        public int CHEM_CD_CONDITION { get; set; }
        public int CHEM_NAME_CONDITION { get; set; }
        public int PRODUCT_NAME_CONDITION { get; set; }
        public int CAS_NO_CONDITION { get; set; }
        public int IncludesContained { get; set; }
        public int USAGE_STATUS { get; set; }
        public int? EXAMINATION_FLAG { get; set; }
        public int? MEASUREMENT_FLAG { get; set; }
        public string _PRODUCT_BARCODE { get; set; }

        public string REGULATION_Division_Text { get; set; }
        public string REGULATION_Text { get; set; }
        public string Fire_Text { get; set; }
        public string Fire_FLAG_Text { get; set; }
        public string Office_Text { get; set; }
        public string Office_Division_Text { get; set; }
        public string GHS_TEXT { get; set; }
        public string GHS_Division_Text { get; set; }


        public string REG_SELECTION { get; set; }
        public string ViewModeValue { get; set; }
        public string lblalreadySetValue { get; set; }

        public IEnumerable<int> REG_COLLECTION { get; set; }

        public IEnumerable<int> FIRE_COLLECTION { get; set; }
        public IEnumerable<int> OFFICE_COLLECTION { get; set; }
        public IEnumerable<int> GAS_COLLECTION { get; set; }

        /// <summary>
        /// 鍵管理辞書
        /// </summary>
        private IDictionary<int?, string> keyManagementDic { get; set; }

        /// <summary>
        /// 重量管理辞書
        /// </summary>
        private IDictionary<int?, string> weightManagementDic { get; set; }

        /// <summary>
        /// 形状辞書
        /// </summary>
        private IDictionary<int?, string> figureDic { get; set; }

        /// <summary>
        /// メーカー辞書
        /// </summary>
        private IDictionary<int?, string> makerDic { get; set; }

        /// <summary>
        /// メーカー選択リスト
        /// </summary>
        private IEnumerable<CommonDataModel<int?>> makerSelectList { get; set; }

        /// <summary>
        /// 単位辞書
        /// </summary>
        private IDictionary<int?, string> unitsizeDic { get; set; }

        /// <summary>
        /// 単位選択リスト
        /// </summary>
        private IEnumerable<CommonDataModel<int?>> unitsizeSelectList { get; set; }

        /// <summary>
        /// ユーザー辞書
        /// </summary>
        private IDictionary<string, string> userDic { get; set; }

        /// <summary>
        /// 法規制辞書
        /// </summary>
        private IDictionary<int?, string> regulationDic { get; set; }

        /// <summary>
        /// 状態辞書
        /// </summary>
        private IDictionary<int?, string> usageStatusDic { get; set; }

    }
}