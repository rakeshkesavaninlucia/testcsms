﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Models
{
    public class T_ORDER : SystemColumn
    {
        public int ORDER_NO { get; set; }

        public int? STATUS_ID { get; set; }

        public int? ORDER_NUM { get; set; }

        public string ORDER_USER_CD { get; set; }

        public int? ROOM_ID { get; set; }

        public int? COMPOUND_LIST_ID { get; set; }

        public long? PRD_SEQNO { get; set; }

        public long? QP_SEQNO { get; set; }

        public string CAT_CD { get; set; }

        public string GRADE { get; set; }

        public string UNITSIZE { get; set; }

        public int? UNITSIZE_ID { get; set; }

        public int? MAKER_ID { get; set; }

        public string PRODUCT_NAME_JP { get; set; }

        public string PRODUCT_NAME_EN { get; set; }

        public string PRODUCT_NAME_SEARCH { get; set; }

        public string CURR { get; set; }

        public int? PRICE { get; set; }

        public string MEMO { get; set; }

        public string PROJECT_CD { get; set; }

        public int? ORDER_TYPE { get; set; }

        public int? BASE_ORDER_NO { get; set; }

        public string BASE_BARCODE { get; set; }

        public int? VENDOR_ID { get; set; }

        public int? AREA_ID { get; set; }

        public int? OLD_ORDER_NUM { get; set; }

        public string APPROVER1 { get; set; }

        public string APPROVER2 { get; set; }

        public string TEL { get; set; }

        public string EMAIL { get; set; }

        public string PRESERVATION_CONDITION { get; set; }

        public string TRANSPORT_CONDITION { get; set; }

        public int? KANJYO_ID { get; set; }

        public int? ORDER_CONF_FLAG { get; set; } = 0;

        public int? DELI_FLAG { get; set; }

        public DateTime? APPROVE_DATE { get; set; }

        public DateTime? ORDER_DATE { get; set; }

        public string OLD_UNITSIZE { get; set; }

        public int? OLD_UNITSIZE_ID { get; set; }

        public string VENDOR_ORDER_NO { get; set; }

        public string UPD_USER { get; set; }
    }
}