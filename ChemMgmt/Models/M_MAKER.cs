﻿using System.ComponentModel.DataAnnotations;

namespace ZyCoaG
{
    public class M_MAKER : SystemColumn
    {
        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string UPLOAD_FILE { get; set; }
        public string STATUS { get; set; }
        public int MAKER_ID { get; set; }
        public string MAKER_NAME { get; set; }
        public string MAKER_URL { get; set; }
        public string MAKER_TEL { get; set; }
        public string MAKER_EMAIL1 { get; set; }
        public string MAKER_EMAIL2 { get; set; }
        public string MAKER_EMAIL3 { get; set; }
        public string MAKER_ADDRESS { get; set; }
        public bool MAKER_DEL_FLAG { get; set; }
        public string DEL_FLAG2 { get; set; }
    }
}