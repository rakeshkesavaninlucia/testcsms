﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.util;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Web;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Util;

namespace ChemMgmt.Models
{
    public class T_STOCK : SystemColumn
    {
        [Key]
        public int STOCK_ID { get; set; }

        private string _BARCODE { get; set; }
        [Display(Name = "瓶番号")]
        [Unique]
        [StringLength(20)]
        [Halfwidth]
        [ExcludingProhibition]
        public string BARCODE
        {
            get
            {
                return _BARCODE;
            }
            set
            {
                _BARCODE = value?.ToUpper();
            }
        }

        public int? STATUS_ID { get; set; }

        public string OWNER { get; set; }

        public int? COMPOUND_LIST_ID { get; set; }

        public string CAT_CD { get; set; }

        [Display(Name = "容量")]
        [NumberLength(8, 3)]
        [Halfwidth]
        [RegularNumber]
        [Required]
        public string UNITSIZE { get; set; }

        [Display(Name = "単位")]
        [Required]
        public int? UNITSIZE_ID { get; set; }

        [Display(Name = "化学物質名")]
        [ExcludingProhibition]
        public string PRODUCT_NAME_JP { get; set; }

        public string PRODUCT_NAME_EN { get; set; }

        public string PRODUCT_NAME_SEARCH { get; set; }

        public string GRADE { get; set; }

        public int? AREA_ID { get; set; }

        [Display(Name = "メーカー名")]
        [Required]
        public int? MAKER_ID { get; set; }

        public string BORROWER { get; set; }

        public DateTime? BRW_DATE { get; set; }

        public int? ORDER_NO { get; set; }

        public int? LOC_ID { get; set; } = 0;

        public DateTime? LIMIT_DATE { get; set; }

        public int? MANAGE_WEIGHT { get; set; }

        public int? VENDOR_ID { get; set; }

        public decimal? INI_GROSS_WEIGHT_G { get; set; }

        public decimal? CUR_GROSS_WEIGHT_G { get; set; }

        public string INTENDED_USE_ID { get; set; }

        public string INTENDED_NM { get; set; }

        public string INTENDED { get; set; }

        public decimal? BOTTLE_WEIGHT_G { get; set; }

        [Display(Name = "備考")]
        [StringLength(1000)]
        [ExcludingProhibition]
        public string MEMO { get; set; } = string.Empty;

        public string PRESERVATION_CONDITION { get; set; }

        public string TRANSPORT_CONDITION { get; set; }

        public DateTime? DELI_DATE { get; set; }

        public int? FINE_POISON_FLAG { get; set; } = 0;

        [Display(Name = "保管場所")]
        [Required]
        public int? FIRST_LOC_ID { get; set; }

        public DateTime? DEL_MAIL_DATE { get; set; }

        public int? ACCOUNT_ID { get; set; }

        public DateTime? LAST_UPD_DATE { get; set; }

        public DateTime? INV_DATE { get; set; }

        public void IssueNewBarcodeAndStore()
        {
            BARCODE = IssueNewBarcode();
        }

        public string TotalWeigth { get; set; }
        public string GrossWeight { get; set; }

        [NotVerifyValue]
        public DbContext context { get; set; } = null;

        public string IssueNewBarcode()
        {
            return IssueNewBarcode(context);
        }

        public string IssueNewBarcode(DbContext context)
        {
            while (true)
            {
                var sql = string.Empty;
                if (ZyCoaG.Classes.util.Common.DatabaseType == ZyCoaG.DatabaseType.Oracle)
                {
                    sql = "select BARCODE_SEQ.nextval as BARCODE_SEQ from dual";
                }
                if (ZyCoaG.Classes.util.Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
                {
                    sql = "select next value for BARCODE_SEQ";
                }
                DataRow row;
                DataTable barcodeTable;
                if (context == null)
                {
                    barcodeTable = DbUtil.Select(sql);
                }
                else
                {
                    barcodeTable = DbUtil.Select(context, sql);
                }
                var barcode = new ClsBarcode();
                row = barcodeTable.Rows[0];
                var barcodeString = ClsBarcode.NikonBarcodeConvert(Convert.ToInt64(row[0]));

                // バーコードが使用されていないかチェックし、使用されていればもう一度発行します。
                sql = "select BARCODE from T_STOCK where BARCODE = :BARCODE";
                var parameters = new Hashtable();
                parameters.Add("BARCODE", barcodeString);
                DataTable barcodeExists;
                if (context == null)
                {
                    barcodeExists = DbUtil.Select(sql, parameters);
                }
                else
                {
                    barcodeExists = DbUtil.Select(context, sql, parameters);
                }
                if (barcodeExists.Rows.Count == 0)
                {
                    return barcodeString;
                }
            }
        }

        private string _REG_BARCODE { get; set; }

        [NotVerifyValue]
        public string REG_BARCODE
        {
            get
            {
                if (!string.IsNullOrWhiteSpace(BARCODE)) return BARCODE;
                if (string.IsNullOrWhiteSpace(_REG_BARCODE))
                {
                    _REG_BARCODE = IssueNewBarcode();
                }
                return _REG_BARCODE;
            }
        }
    }
}