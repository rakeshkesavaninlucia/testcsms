﻿using System.ComponentModel.DataAnnotations;

namespace ZyCoaG
{

    public class M_ACTION : SystemColumn
    {
        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string UPLOAD_FILE { get; set; }
        public int ACTION_TYPE_ID { get; set; }

        public string ACTION_TEXT { get; set; }

        public int UPDATE_ACTION_TYPE_ID { get; set; }

        public string UPDATE_ACTION_TEXT { get; set; }
        public bool ACTION_DEL_FLAG { get; set; }

    }
}