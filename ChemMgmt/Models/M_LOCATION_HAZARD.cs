﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG
{
    public class M_LOCATION_HAZARD
    {

        /// <summary>
        /// 場所ID
        /// </summary>
        public int LOC_ID { get; set; }


        public int REG_TYPE_ID { get; set; }


        public int HAZARD_CHECK { get; set; }

        public string REG_USER_CD { get; set; }
        /// <summary>
        /// 登録日時
        /// </summary>
        public DateTime? REG_DATE { get; set; }

    }
}