﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ZyCoaG
{
    public class M_UNITSIZE : SystemColumn
    {
        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string UPLOAD_FILE { get; set; }
        //[Key]
        //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal UNITSIZE_ID { get; set; }

        [Display(Name = "単位名称")]
        [Required(ErrorMessage = "Unit name is required.")]
        public string UNIT_NAME { get; set; }

        [Display(Name = "変換単位(半角)")]
        [Required(ErrorMessage = "Conversion Unit is required.")]
        public string FACTOR { get; set; }

        public string UNITSIZE { get; set; }

       // public bool UNITSIZE_DEL_FLAG { get; set; }

        public string DEL_FLAG2 { get; set; }

        //[Display(Name = "状態")]
        //[Required]
        //public bool DEL_FLAG { get; set; }

        //[Display(Name = "登録者")]
        //public string REG_USER_CD { get; set; }

        //[Display(Name = "更新者")]
        //public string UPD_USER_CD { get; set; }

        //[Display(Name = "削除者")]
        //public string DEL_USER_CD { get; set; }

        //[Display(Name = "登録日時")]
        //public DateTime REG_DATE { get; set; }

        //[Display(Name = "更新日時")]
        //public DateTime UPD_DATE { get; set; }

        //[Display(Name = "削除日時")]
        //public DateTime DEL_DATE { get; set; }

        public enum STATUS
        {
            使用中,
            削除済,
            全て
        }
    }
}