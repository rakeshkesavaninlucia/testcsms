﻿using System.Collections.Generic;
using System.Web.Mvc;
using ZyCoaG.Nikon;

namespace ZyCoaG
{
    public class M_MASTER
    {
        public M_GROUP mGroup {get; set; }  // Department Master
        public M_LOCATION mLocation { get; set; }// Location Master
        public M_REGULATION mRegulation { get; set; }   // Legal Master
        public M_MAKER mMaker { get; set; } // Maker Master
        public M_UNITSIZE mUnitSize { get; set; }   // Capacity Master
        public M_STATUS mStatus { get; set; }   // Status Master
        public M_MESSAGE mMessage { get; set; } // Message Master
        public M_ACTION mActionType { get; set; } // Operation Master
        public M_COMMON mCommon { get; set; }   // General Purpose Master
        public M_WORKFLOW mWorkFlow { get; set; }   // WorkFlow Master
      //  public M_FIRE mFire { get; set; }// Fire Service Master
        public string masterMaintenanceScreen { get; set; }
        //public string TableName { get; set; }

        public string PRODUCT_NAME { get; set; }

        public string M_StatusPreview { get; set; }

       // public List<M_MAKER> listMakerDetails { get; set; }
   //     public List<M_ACTION> listActionDetails { get; set; }
   //     public List<M_STATUS> listStatusDetails { get; set; }
    //    public List<M_UNITSIZE> listUnitDetails { get; set; }
        public IEnumerable<SelectListItem> LookUpStatus { get; set; }
        public IEnumerable<SelectListItem> LookUpCMICondition { get; set; }
        public IEnumerable<SelectListItem> LookUpCMDCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpCMACondition { get; set; }
        public string TYPE { get; set; }
        public string CALLER { get; set; }
        //M_Regulation Master

        public CasRegModel casRegModel { get; set; }
        public List<M_REGULATION> listRegulationDetails { get; set; }
        public List<M_COMMON> listCommonDetails { get; set; }
        public List<CasRegModel> listCasRegModelDetails { get; set; }
        public IEnumerable<CasRegModel> vCasRegModel { get; set; }
        public IEnumerable<SelectListItem> LookUpTableName { get; set; }

        public IEnumerable<SelectListItem> LookUpClassification { get; set; }
        public IEnumerable<SelectListItem> LookUpRegNameCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpRegName { get; set; }
        public IEnumerable<SelectListItem> LookUpDelStatus { get; set; }
        public IEnumerable<SelectListItem> LookUpIconType { get; set; }
        public IEnumerable<SelectListItem> LookUpLegalName { get; set; }
        public IEnumerable<SelectListItem> LookUpKeyMgmt { get; set; }
        public IEnumerable<SelectListItem> LookUpWeightMgmt { get; set; }
        public IEnumerable<SelectListItem> LookUpUnitSize { get; set; }
        public IEnumerable<SelectListItem> LookUpWorkTimeMgmt { get; set; }
        public IEnumerable<SelectListItem> LookUpRegIconPath { get; set; }
        public IEnumerable<SelectListItem> LookUpExposureReport { get; set; }
        public IEnumerable<SelectListItem> LookUpReportNameCont { get; set; }
        public IEnumerable<SelectListItem> LookUpLocation { get; set; }
        public IEnumerable<SelectListItem> LookUpReportPlace { get; set; }
        //public string TableName { get; set; }
        public int id { get; set; }

        public string Message { get; set; }
    }
}