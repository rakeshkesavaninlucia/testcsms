﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ChemMgmt.Models
{
    public class Chem
    {
        [Display(Name = "Classification")]
        [StringLength(100)]
        [Required]
        public type CHEM_CAT { get; set; }

        public enum type
        {
            Male,
            Female
        }


        [Key]
        [Display(Name = "Chemical substance code")]
        public string CHEM_CD { get; set; }

        [Display(Name = "Chemical substance name")]
        [StringLength(255)]
        [Required]
        public string CHEM_NAME { get; set; }


        [Display(Name = "Product name")]
        [StringLength(255)]
        [Required]

        public string PRODUCT_NAME { get; set; }


        [Display(Name = "CAS#")]
        [StringLength(60)]
        public string CAS_NO { get; set; }

        [Display(Name = "Weight management`")]
        [Required]
        public int? WEIGHT_MGMT { get; set; }



        [Display(Name = "shape")]
        [Required]
        public int? FIGURE { get; set; }



        [Display(Name = "Key management")]
        [Required]
        public int? KEY_MGMT { get; set; }

        //[Display(Name ="Laws and Regulation")] 
        //[Required]

     }
}