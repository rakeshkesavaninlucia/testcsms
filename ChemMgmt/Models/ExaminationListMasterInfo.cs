﻿using ChemMgmt.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChemMgmt.Models
{
    public enum ExaminationList
    {
        /// <summary>
        /// 非該当
        /// </summary>
        Higaitou,
        /// <summary>
        /// 該当
        /// </summary>
        Gaitou,
        /// <summary>
        /// 全て
        /// </summary>
        All
    }
    public class ExaminationListMasterInfo : CommonDataModelMasterInfoBase<int?>
    {
        // <summary>
        /// 選択項目に空白を追加するか
        /// </summary>
        protected override bool IsAddSpace { get; set; } = false;
        protected override IEnumerable<CommonDataModel<int?>> GetAllData()
        {
            var searchCondition = new List<CommonDataModel<int?>>();
            searchCondition.Add(new CommonDataModel<int?>((int)ExaminationList.Higaitou, "非該当", 0));
            searchCondition.Add(new CommonDataModel<int?>((int)ExaminationList.Gaitou, "該当", 1));
            searchCondition.Add(new CommonDataModel<int?>((int)ExaminationList.All, "全て", 2));
            return searchCondition;
        }
    }
}