﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ZyCoaG
{
    public class M_USER : SystemColumn
    {
        /// <summary>
        /// ユーザーコード
        /// </summary>
        [Display(Name = "ユーザーコード")]
        public string USER_CD { get; set; }

        /// <summary>
        /// ユーザー名称
        /// </summary>
        [Display(Name = "ユーザー名称")]
        public string USER_NAME { get; set; }

        /// <summary>
        /// 第一承認者
        /// </summary>
        [Display(Name = "第一承認者")]
        public string APPROVER1 { get; set; }

        /// <summary>
        /// 第二承認者
        /// </summary>
        [Display(Name = "第二承認者")]
        public string APPROVER2 { get; set; }

        /// <summary>
        /// 内線番号
        /// </summary>
        [Display(Name = "内線番号")]
        public string TEL { get; set; }

        /// <summary>
        /// メールアドレス
        /// </summary>
        [Display(Name = "メールアドレス")]
        public string EMAIL { get; set; }

        /// <summary>
        /// 納品場所ID
        /// </summary>
        [Display(Name = "納品場所ID")]
        public int? ROOM_ID { get; set; }

        /// <summary>
        /// 最終ログイン日時
        /// </summary>
        [Display(Name = "最終ログイン日時")]
        public DateTime? LAST_LOGIN_DATE { get; set; }

        /// <summary>
        /// ログイン回数
        /// </summary>
        [Display(Name = "ログイン回数")]
        public int? LOGIN_COUNT { get; set; }

        /// <summary>
        /// 管理者権限
        /// </summary>
        [Display(Name = "管理者権限")]
        public int? ADMIN_FLAG { get; set; }

        /// <summary>
        /// パスワード
        /// </summary>
        [Display(Name = "パスワード")]
        public string PASSWORD { get; set; }

        /// <summary>
        /// メモ
        /// </summary>
        [Display(Name = "メモ")]
        public string MEMO { get; set; }

        /// <summary>
        /// 使用言語
        /// </summary>
        [Display(Name = "使用言語")]
        public int? BILINGUAL_MODE { get; set; }

        /// <summary>
        /// 部署ID
        /// </summary>
        [Display(Name = "部署ID")]
        public int? GROUP_ID { get; set; }

        /// <summary>
        /// 地域情報
        /// </summary>
        [Display(Name = "地域情報")]
        public int? AREA_ID { get; set; }

        /// <summary>
        /// フローコード
        /// </summary>
        [Display(Name = "フローコード")]
        public string FLOW_CD { get; set; }

        /// <summary>
        /// ユーザー名称ローマ字
        /// </summary>
        [Display(Name = "ユーザー名称ローマ字")]
        public string USER_NAME_KANA { get; set; }

        /// <summary>
        /// 部署コード
        /// </summary>
        [Display(Name = "部署コード")]
        public string GROUP_CD { get; set; }

        /// <summary>
        /// 所属部署１
        /// </summary>
        [Display(Name = "所属部署１")]
        public string PREVIOUS_GROUP_CD { get; set; }

        public string GROUP_NAME { get; set; }

        public string DEL_USERNAME { get; set; }
        public string ADMIN_FLAGNAME { get; set; }
        public string BATCH_FLAGNAME { get; set; }
        public string USER_NM { get; set; }
        public string USER_KANA { get; set; }
        public string BATCH_FLAG { get; set; }
        public string Status { get; set; }
        public string PREVIOUS_GROUP_NM { get; set; }
        public string UPD_USER { get; set; }   
        public string USAGE { get; set; }
        public string HIDDEN_GROUP_CD { get; set; }

    }
}