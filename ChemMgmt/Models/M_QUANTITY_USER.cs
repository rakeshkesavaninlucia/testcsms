﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// Author: MSCGI 
/// Created date: 18-7-2019
/// Description : Quantity User Model.
/// // model may not be useful since M_QUANTITY_USER table is missing in DB. check ClsCatalogData.cs class. need to investigate inventory list output confirmation screen
namespace ChemMgmt.Models
{
    public class M_QUANTITY_USER
    {
        public string COMPOUND_LIST_ID { get; set; }
        public string CAT_CD { get; set; }
        public string UNITSIZE { get; set; }
        public string UNITSIZE_ID { get; set; }
        public string CURR { get; set; }
        public string PRICE { get; set; }
        public string PRESERVATION_CONDITION { get; set; }
        public string TRANSPORT_CONDITION { get; set; }
        public string DEL_FLAG { get; set; }
        public string QP_SEQNO { get; set; }
    }
}