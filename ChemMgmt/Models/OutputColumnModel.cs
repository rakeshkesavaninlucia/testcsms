﻿namespace ZyCoaG
{
    public enum OutputColumnType
    {
        Label,
        Button,
        LinkButton,
        TextBox,
        DropDownList,
        Number,
        Date,
        DateTime,
        CheckBox,
        Image,
        HyperLink,
    }

    public class OutputColumnModel
    {
        public string Key { get; set; }

        public string SortKey { get; set; }

        public string Value { get; set; }

        public int Order { get; set; } = 0;

        public int? Width { get; set; }

        public OutputColumnType OutputColumnType { get; set; } = OutputColumnType.Label;

        public bool Visible { get; set; } = true;

        public string SelectMethod { get; set; }

        public string DataValueField { get; set; }

        public string DataTextField { get; set; }

        public string CommandName { get; set; }

        public OutputColumnModel() { }

        public OutputColumnModel(string Name)
        {
            this.Key = Name;
        }

        public OutputColumnModel(string Name, int Order)
        {
            this.Key = Name;
            this.Order = Order;
        }

        public OutputColumnModel(string Name, int Order, int Width)
        {
            this.Key = Name;
            this.Order = Order;
        }

        public OutputColumnModel(string Name, string Value)
        {
            this.Key = Name;
            this.Value = Value;
            this.Width = Width;
        }

        public OutputColumnModel(string Name, string Value, int Order)
        {
            this.Key = Name;
            this.Value = Value;
            this.Order = Order;
        }

        public OutputColumnModel(string Name, string Value, int Order, int Width)
        {
            this.Key = Name;
            this.Value = Value;
            this.Order = Order;
            this.Width = Width;
        }

        public OutputColumnModel(string Name, OutputColumnType OutputColumnType)
        {
            this.Key = Name;
            this.OutputColumnType = OutputColumnType;
        }

        public OutputColumnModel(string Name, OutputColumnType OutputColumnType, int Width)
        {
            this.Key = Name;
            this.OutputColumnType = OutputColumnType;
            this.Width = Width;
        }

        public OutputColumnModel(string Name, string Value, OutputColumnType OutputColumnType)
        {
            this.Key = Name;
            this.Value = Value;
            this.OutputColumnType = OutputColumnType;
        }

        public OutputColumnModel(string Name, string Value, OutputColumnType OutputColumnType, int Width)
        {
            this.Key = Name;
            this.Value = Value;
            this.OutputColumnType = OutputColumnType;
            this.Width = Width;
        }

        public OutputColumnModel(string Name, string Value, int Order, OutputColumnType OutputColumnType)
        {
            this.Key = Name;
            this.Value = Value;
            this.Order = Order;
            this.OutputColumnType = OutputColumnType;
        }

        public OutputColumnModel(string Name, string Value, int Order, OutputColumnType OutputColumnType, int Width)
        {
            this.Key = Name;
            this.Value = Value;
            this.Order = Order;
            this.OutputColumnType = OutputColumnType;
            this.Width = Width;
        }
    }
}
