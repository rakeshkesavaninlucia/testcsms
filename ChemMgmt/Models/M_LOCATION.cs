﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace ZyCoaG
{
    /// <summary>
    /// 場所マスター
    /// </summary>
    public class M_LOCATION : SystemColumn
    {
        /// <summary>
        /// 場所ID
        /// </summary>
        public int LOC_ID { get; set; }
        /// <summary>
        /// 場所名称
        /// </summary>
        public string LOC_NAME { get; set; }

        /// <summary>
        /// クラスID
        /// </summary>
        public int? CLASS_ID { get; set; }
        public string CLASS_NAME { get; set; }

        /// <summary>
        /// 場所バーコード
        /// </summary>
        public string LOC_BARCODE { get; set; }
        
        /// <summary>
        /// 並び順
        /// </summary>
        public int? SORT_LEVEL { get; set; }
        
             public List<SelectListItem> P_LOC_NAMES { get; set; }
        /// <summary>
        /// 上位場所ID
        /// </summary>
        public int? P_LOC_ID { get; set; }
        public string P_LOC_BASEID { get; set; }
        public string P_LOC_NAME { get; set; }
        public string REG_TEXT { get; set; }

        public bool HAZARD_CHECK1 { get; set; }
        public bool HAZARD_CHECK2 { get; set; }
        public bool HAZARD_CHECK3 { get; set; }
        public bool HAZARD_CHECK4 { get; set; }
        public bool HAZARD_CHECK5 { get; set; }
        public bool HAZARD_CHECK6 { get; set; }
        public bool HAZARD_CHECK7 { get; set; }

        public int ChiefCount { get; set; }
        /// <summary>
        /// 選択可否フラグ
        /// </summary>
        public bool SEL_FLAG { get; set; }
        public string SELECTION_FLAG { get; set; }
        public string SEL_FLAGNAME { get; set; }

        public string OVER_CHECK_FLAG { get; set; }
        public string OVER_CHECK_FLAGNAME { get; set; }
        //ChemChief NID
        public string USER_CD { get; set; }
        public string CHEM_CHIEF { get; set; }
        public string USER_NAME { get; set; }
        
        public string REG_USER_NM { get; set; }
        public string UPD_USER_NM { get; set; }
        public string DEL_USER_NM { get; set; }


        public string UPDATE_CLASS_NAME { get; set; }
        public string UPDATE_LOC_NAME { get; set; }
        public string UPDATE_P_LOC_NAME { get; set; }
        public string UPDATE_SEL_FLAGNAME { get; set; }
        public string UPDATE_USER_CD { get; set; }
        public string UPDATE_DEL_FLAGNAME { get; set; }
        public string UPDATE_LOC_BARCODE { get; set; }
        public int UPDATE_SORT_LEVEL { get; set; }
        public string UPDATE_OVER_CHECK_FLAGNAME { get; set; }
        public string UPDATE_REG_TEXT { get; set; }
        public string UPDATE_USER_NAME { get; set; }
        public string UPDATE_LOC_ID { get; set; }
        public string UPDATE_P_LOC_ID { get; set; }
    }
}