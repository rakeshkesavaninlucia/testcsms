﻿using System.ComponentModel.DataAnnotations;

namespace ZyCoaG
{

    public class M_STATUS : SystemColumn
    {
        [DataType(DataType.Upload)]
        [Display(Name = "Upload File")]
        [Required(ErrorMessage = "Please choose file to upload.")]
        public string UPLOAD_FILE { get; set; }
        public int STATUS_ID { get; set; }

        public int CLASS_ID { get; set; }

        public string STATUS_TEXT { get; set; }
        public int UPDATE_STATUS_ID { get; set; }

        public int UPDATE_CLASS_ID { get; set; }

        public string UPDATE_STATUS_TEXT { get; set; }
        public bool STATUS_DEL_FLAG { get; set; }

    }
}