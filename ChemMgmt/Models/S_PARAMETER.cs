﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ZyCoaG
{
    /// <summary>
    /// 管理システムデータ
    /// </summary>
    public class S_PARAMETER
    {
        /// <summary>
        /// 名前
        /// </summary>
        public string PARAMETER_NAME { get; set; }

        /// <summary>
        /// 表示名
        /// </summary>
        public string PARAMETER_DISPLAY_NAME { get; set; }

        /// <summary>
        /// 値
        /// </summary>
        public string PARAMETER_VALUE { get; set; }

        /// <summary>
        /// コメント
        /// </summary>
        public string PARAMETER_COMMENT { get; set; }
    }
}