﻿using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Chem;
using ChemMgmt.Nikon.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using ZyCoaG.Nikon;

namespace ChemMgmt.Models
{
    public class InventoryManagement : ChemModel
    {
      
        //public string MAKER_ID { get; set; }
        //[Display(Name = "メーカー名")]
        //public string MAKER_NAME { get; set; }
        public string VENDOR_ID { get; set; }
        public string VENDOR_NAME { get; set; }
        [Display(Name = "化学物質コード")]
        public string CAT_CD { get; set; }
        //[Display(Name = "CAS#(半角)")]//2019/02/18 TPE.Sugimoto Add
        //public string CAS_NO { get; set; }//2019/02/18 TPE.Sugimoto Add
        [Display(Name = "瓶番号")]
        public string BARCODE { get; set; }
        public string ORDER_NO { get; set; }
        [Display(Name = "在庫ステータス")]
        public string STATUS { get; set; }
        [Display(Name = "受入者")]
        public string ORDER_USER { get; set; }
        [Display(Name = "受入開始日")]
        public string DELI_DATE_FROM { get; set; }
        [Display(Name = "受入終了日")]
        public string DELI_DATE_TO { get; set; }
        public string STORING_LOC_NAME { get; set; }
        public string[] REGULATION_INFO { get; set; }
        [Display(Name = "法規制")]
        public string[] REGULATION_IDs { get; set; }
        public string[] LOCATION_INFO { get; set; }
        [Display(Name = "保管場所")]
        public string[] LOCATION_IDs { get; set; }
        public string STRUCTURE_BASE64 { get; set; }
        public int PRODUCT_NAME_TYPE { get; set; }
        public int MAKER_NAME_TYPE { get; set; }
        public int CAT_CD_TYPE { get; set; }
        public int BARCODE_TYPE { get; set; }
        public int ORDER_USER_TYPE { get; set; }
        public int STRUCTURE_TYPE { get; set; }
        public string FAVORITES_MAKER { get; set; }
        public string STOCK_ID { get; set; }
        public string ddlIntended { get; set; }
        public string txtIntended { get; set; }
        public string UsageLocation { get; set; }
        public string WorkerIds { get; set; }
        public string lblWorkers { get; set; }
        public string hdnRegulationIds { get; set; }              
        public string STATUS_TEXT { get; set; }      
        public string LocationID { get; set; }
        public string LocationName { get; set; }
        public string PRODUCT_NAME_JP { get; set; }
        public string PRODUCT_NAME_EN { get; set; }
        public string LOC_NAME { get; set; }

        public string FIRE_TOTAL { get; set; }
        public string REGULATION_UNITNAME { get; set; }
        public string GRADE { get; set; }     
    
        public string DELI_DATE { get; set; }

        public string DISPLAY_SIZE { get; set; }
        public string CONTENT_WEIGHT { get; set; }
        public string GHSCAT { get; set; }
        public string REGULATORY { get; set; }

        public string OFFICE_NAME { get; set; }

        public string FIRE_NAME { get; set; }

        public string STATUS_NAME { get; set; }
        public string UNIT_NAME { get; set; }
        public string ORDER_NAME { get; set; }
        public string CUR_GROSS_WEIGHT_G { get; set; }
        public string BRW_DATE { get; set; }
        public string BORROWER_NAME { get; set; }
        public string PRESERVATION_CONDITION { get; set; }
        public string LIMIT_DATE { get; set; }   
        public string INI_GROSS_WEIGHT_G { get; set; }
        public string BOTTLE_WEIGHT_G { get; set; }
        public string FIRST_LOC_NAME { get; set; }
        public string DEL_MAIL_DATE { get; set; }
        public string TRANSPORT_CONDITION { get; set; }
        public string labUnitSize { get; set; }
        public string labMemo { get; set; }
        public bool imgBottle_weight { get; set; }
        public bool imgIni_weight { get; set; }
        public bool imgWeight { get; set; }
        public string ErrorMessage { get; set; }

        public string txtAllWeight { get; set; }
        public string hdnExecuteType { get; set; }
        public string txtUnitSize { get; set; }
        public string txtMemo { get; set; }
        public string txtWorkingTimes { get; set; }

        public string labLocName { get; set; }

        public string storingFilterIds { get; set; }
        public string usageFilterIds { get; set; }

        [DataType(DataType.MultilineText)]
        public string locName { get; set; }

        [DataType(DataType.MultilineText)]
        public string usageLocName { get; set; }


        public string LOC_BARCODE { get; set; }
        public string AMOUNT_BASE { get; set; }
        public string REG_TEXT2 { get; set; }
        public string RECENT_LEDGER_FLOW_ID { get; set; }
        public string FIRE_SIZE { get; set; }
        public string GROUP_NAME { get; set; }
        public string AMOUNT { get; set; }
        public string TEL { get; set; }
        public string CONTENT_WEIGHT_VOLUME { get; set; }
        public string CONTENT_WEIGHT_UNITSIZE { get; set; }
        /// <summary>
        /// 選択表示の選択時の文字を定義します。
        /// </summary>
        public string SelectionViewSetText { get; set; } = "設定済み";

        public string labelMode { get; set; }

        //Inventory Search
        public IEnumerable<SelectListItem> LookUpMakerName { get; set; }
        public IEnumerable<SelectListItem> LookUpStatus { get; set; }
        public IEnumerable<SelectListItem> LookUpCSNCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpMakerNameCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpCSCodeCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpBottleNumCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpAcceptCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpExposureWorkUse { get; set; }
        public IEnumerable<SelectListItem> LookUpExposureWorkType { get; set; }
        public IEnumerable<SelectListItem> LookUpUseTemperature { get; set; }
        public IEnumerable<SelectListItem> LookUpAmountUnitSize { get; set; }
        public IEnumerable<SelectListItem> LookUpIntended { get; set; }



        public string lblCasNo { get; set; }
        public string lblProduct { get; set; }
        public string lblBarcode { get; set; }
        public string lblProduct2 { get; set; }
        public string lblCatCd { get; set; }
        public string lblMaker { get; set; }
        public string lblIniWeight { get; set; }
        public string lblGWeight { get; set; }
        public string lblLimit { get; set; }
        public string lblUnitSize { get; set; }
        public string OriginalLocation { get; set; }
        public string StoringLocation { get; set; }
        public string lblWeight { get; set; }
        public string hdnOriginalWeight { get; set; }
        public string hdnAllWeight { get; set; }
        public string hdnUnitSize { get; set; }
        public string hdnStatusId { get; set; }
        public string hdnStatus { get; set; }
        public string hdnManageWeight { get; set; }
        public string LastWeight { get; set; }
        public string StockId { get; set; }
        public string WorkRecordId { get; set; }
        public string RegNo { get; set; }
        public string LoginUserId { get; set; }
        public string LoginUserCode { get; set; }
        public string locbarcode { get; set; }
        public string LastUsageLocId { get; set; }
        public string IsExposureWork { get; set; }
        public string IsworkerFlag { get; set; }
        public string IsAmountCheck { get; set; }


        public string lblStatus { get; set; }
        public string IntendedValue { get; set; }
        public string AmountUnitSize { get; set; }
        public string UseTemperature { get; set; }
        public string ExposureWorkType { get; set; }
        public string ExposureWorkUse { get; set; }
        public string txtAmount { get; set; }
        public string WorkComment { get; set; }
        public string WorkCommentTargetWorkerIds { get; set; }
        public string IsWorkCommentInput { get; set; }

        //Model for ZC130 Block
        public IEnumerable<SelectListItem> LookUpBarcodeCondition { get; set; }
        public IEnumerable<SelectListItem> LookUpInclude { get; set; }
        public IEnumerable<SelectListItem> LookUpDDLLocation { get; set; }
        public string USER_NAME { get; set; }
        public string ISSUE_START_DATE { get; set; }
        public string ISSUE_END_DATE { get; set; }
        public string RECEIPT_START_DATE { get; set; }
        public string RECEIPT_END_DATE { get; set; }
        public string PRODUCT_NAME_CONDITION { get; set; }
        public string BARCODE_CONDITION { get; set; }
        public string REG_NO_CONDITION { get; set; }
        public string USER_NAME_CONDITION { get; set; }
        public string IncludesContained { get; set; }
        public string txtReportName { get; set; }
        public string ddlLocation { get; set; }
        public string ddlsession { get; set; }
        public string hdnLocationId { get; set; }
        public string txtBarcode { get; set; }
        public string hdnGroupId { get; set; }
        public string ddlType { get; set; }
       
        public string hdnGroupIds { get; set; }       
        public string hdnLocationIds { get; set; }

        public string FlowCode { get; set; }


        //End

        public ViewInventoryDetails viewInventory { get; set; }

        public WorkRecordSearchModel viewWorkSearchModel { get; set; }

        public List<ViewInventoryDetails> listViewInventoryDetails { get; set; }
        public List<ChemStockBulkChangeModel> listBulkChangeInventoryDetails { get; set; }
        public List<WorkRecordModel> listWorkRecordModel { get; set; }
        public List<ChemUsageHistoryModel> listchemusageModel { get; set; }
        public List<ViewInventoryDetails> listLedgerDetails { get; set; }
        public List<ViewInventoryDetails> listLedgerManagementDetails { get; set; }
        public List<InventoryManagement> listInventoryManagement { get; set; }
        public List<ViewInventoryDetails> listInventoryStockDetails { get; set; }
        public List<ChemStockModel> listChemStockDetails { get; set; }
        public IEnumerable<V_GHSCAT> vGhscat { get; set; }
        public IEnumerable<V_REGULATION> vRegulation { get; set; }
        public IEnumerable<V_FIRE> vFire { get; set; }
        public IEnumerable<V_OFFICE> vOffice { get; set; }
    }

    public class ViewInventoryDetails
    {
        public string STATUS_TEXT { get; set; }
        public string REG_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string BARCODE { get; set; }
        public string PRODUCT_NAME_JP { get; set; }
        public string PRODUCT_NAME_EN { get; set; }
        public string LOC_NAME { get; set; }
        public string CAT_CD { get; set; }
        public string CHEM_NAME { get; set; }
        public string CAS_NO { get; set; }
        public string MAKER_NAME { get; set; }
        public string MAKER_ID { get; set; }
        public string LAST_LOC_ID { get; set; }
        public string LOC_ID { get; set; }
        public string GRADE { get; set; }
        public string UNITSIZE_ID { get; set; }
        public string UNITSIZE { get; set; }
        public string DELI_DATE { get; set; }
        public string UNIT_NAME { get; set; }
        public string ORDER_NAME { get; set; }
        public string CUR_GROSS_WEIGHT_G { get; set; }
        public string BRW_DATE { get; set; }
        public string BORROWER_NAME { get; set; }
        public string PRESERVATION_CONDITION { get; set; }
        public string LIMIT_DATE { get; set; }
        public string MEMO { get; set; }
        public string INI_GROSS_WEIGHT_G { get; set; }
        public string BOTTLE_WEIGHT_G { get; set; }
        public string FIRST_LOC_NAME { get; set; }
        public string DEL_MAIL_DATE { get; set; }
        public string TRANSPORT_CONDITION { get; set; }
        public string labUnitSize { get; set; }
        public string labUnitName { get; set; }
        public string labMemo { get; set; }
        //public string imgBottle_weight { get; set; }
        public bool imgIni_weight { get; set; }
        public bool imgWeight { get; set; }
        public bool imgbottle_weight { get; set; }
        public string STOCK_ID { get; set; }
        public string DELI_DATE_S { get; set; }
        public string USER_NAME { get; set; }
        public string CUR_GROSS_WEIGHT { get; set; }
        public string STATUS_NAME { get; set; }
        public string GHSCAT_NAME { get; set; }
        public string FIRECAT_NAME { get; set; }
        public string OFFICECAT_NAME { get; set; }
        public string MANAGE_WEIGHT { get; set; }
        public string STATUS_ID { get; set; }
        public string LAST_WEIGHT { get; set; }
        public string LOC_BARCODE { get; set; }
        public string INTENDED_USE_ID { get; set; }
        public string INTENDED { get; set; }

    }

    public class FireDataModel
    {
        public int FIRE_ID { get; set; }
        public string FIRE_SIZE { get; set; }
        public string REG_TEXT { get; set; }
        public string REG_TEXT2 { get; set; }
        public string AMOUNT { get; set; }

    }

}
