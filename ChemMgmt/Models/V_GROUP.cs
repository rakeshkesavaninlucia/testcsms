﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ZyCoaG;

namespace ChemMgmt.Models
{
    public class V_GROUP : M_GROUP
    {
        /// <summary>
        /// 部署名(カテゴリ)
        /// </summary>
        public string GROUP_NAME_BASE { get; set; }

        /// <summary>
        /// 部署名(名称)
        /// </summary>
        public string GROUP_NAME_LAST { get; set; }
    }
}