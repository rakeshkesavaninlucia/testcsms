﻿
namespace ZyCoaG
{
    public class V_LOCATION : M_LOCATION
    {
        /// <summary>
        /// 場所名称(カテゴリ)
        /// </summary>
        public string LOC_NAME_BASE { get; set; }

        /// <summary>
        /// 場所名称(名称)
        /// </summary>
        public string LOC_NAME_LAST { get; set; }
    }
}