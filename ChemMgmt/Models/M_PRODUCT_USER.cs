﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// Author: MSCGI 
/// Created date: 18-7-2019
/// Description : Product Model.

// model may not be useful since M_Product_user table is missing in DB. check ClsCatalogData.cs class; need to investigate inventory list output confirmation screen
namespace ChemMgmt.Models
{
    public class M_PRODUCT_USER
    {
       public string PRODUCT_NAME_JP { get; set; }  
            public string PRODUCT_NAME_EN { get; set; }
        public string MAKER_ID { get; set; }
        public string VENDOR_ID { get; set; }
        public string GRADE { get; set; }
        public string PURITY { get; set; }
        public decimal PRD_SEQNO { get; set; }
        public string DEL_FLAG { get; set; }
        
    }
}