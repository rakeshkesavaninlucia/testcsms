﻿using System;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.ZC010;


namespace ChemMgmt.Controllers
{
    public class ErrorController : Controller
    {
        // GET: /Error/
        ZC010Controller zc = new ZC010Controller();
        public ActionResult ZC01090(int status, Exception error)
        {
            Response.StatusCode = status;
            //if (status == 404)
            //{
            //    return RedirectToAction("Error404");
            //}
            //if (err != null)
            //{
            //    ViewBag.Exception = err;
            //}
            //else
            //{
                ViewBag.Exception = error.Message;
            //}
           // zc.SessionRemove();
            return View();
        }
        //public ActionResult Exception(string exeption)
        //{
        //    return RedirectToAction("ZC01090", "ZC010", new {Message=exeption });

        //}
        public ActionResult Error404()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
        }

    }
}