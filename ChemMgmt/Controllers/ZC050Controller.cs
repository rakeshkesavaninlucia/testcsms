﻿using ChemMgmt.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;
using ZyCoaG.BaseCommon;
using System.Linq;
using ZyCoaG.Nikon;
using System.IO;
using System.Web;
using static ZyCoaG.MasterMaintenance.Common;
using System.Text;
using System.Web.UI.WebControls;
using ZyCoaG.PrintLabel;
using ZyCoaG.Classes.util;
using System.Collections;
using ChemMgmt.Models;
using ChemMgmt.Classes.util;
using ChemMgmt.Classes;
using ZyCoaG.util;
using ChemMgmt.Classes.Extensions;
using COE.Common.BaseExtension;
using Dapper;
using System.Web.UI;
using System.Drawing;

//using Common = ZyCoaG.MasterMaintenance.Common;
using Control = ZyCoaG.BaseCommon.Control;
using ChemMgmt.Nikon;
using System.Text.RegularExpressions;
using ChemMgmt.Nikon.Models;
using System.Configuration;

namespace ZyCoaG.ZC050
{
    [HandleError]
    public class ZC050Controller : Controller
    {
        // GET: ZC050
        MasterMaintenanceRepository repository = new MasterMaintenanceRepository();

        /// <summary>
        /// 検索されたか
        /// </summary>
        private bool isSearch
        {
            get
            {
                if (Session[SessionConst.IsCommonMasterSearch] != null && (bool)Session[SessionConst.IsCommonMasterSearch])
                {
                    return (bool)Session[SessionConst.IsCommonMasterSearch];
                }
                return false;
            }
            set
            {
                Session.Add(SessionConst.IsCommonMasterSearch, value);
            }
        }

        S_TAB_CONTROL tableInformation;
        List<S_TAB_COL_CONTROL> tableColumnInformations;


        /// <summary>
        /// ログ出力を定義します。
        /// </summary>
        private LogUtil _logoututil = new LogUtil();

        /// <summary>
        /// プログラムIDを定義します。
        /// </summary>
        private const string ProgramId = "ZC050";

        /// <summary>
        /// 検索結果を格納するセッション変数名を定義します。
        /// </summary>
        public const string cSESSION_NAME_InputSearch = "ZC05901_InputSearch";
        private const string cSESSION_NAME_SEARCH_RESULT = "ZC05901_SEARCH_RESULT";
        private const string cSESSION_NAME_SEARCH_RESULT_COUNT = "ZC05901_SEARCH_RESULT_COUNT";

        private const string CsvFileNameSuffix = ".csv";
        private const string TemplateFileNameSuffix = "テンプレート.csv";


        private const string sessionPLocId = "P_LOC_ID";

        private const string statusInsert = "INSERT";
        private const string statusUpdate = "UPDATE";

        private const string selflagOk = "選択可";
        private const string selflagNg = "選択不可";

        private static int searchEnabled = 0;
        // Fire Law
        private const string OUTLIST_FILENAME = "消防法届出マスター.csv";

        List<tblFireClass> tblFireList = new List<tblFireClass>();

        List<tblFireClass> tblFireClsList = new List<tblFireClass>();
        public Table tblFire { get; set; }

        M_FIRE mFire = new M_FIRE();

        private UserInfo.ClsUser _clsUser = new UserInfo.ClsUser();

        // private const string ProgramId = "ZC050";
        [Authorize(Roles = "0,1")]
        public ActionResult ZC05201(M_MASTER master, string TableName, bool IsPostBack = false)
        {
            master = new M_MASTER();
            master.mMessage = new M_MESSAGE();
            master.TYPE = "New";
            KeyValuePair<string, string> msg = repository.InitDisp();
            master.mMessage.MSG_ID = Convert.ToInt32(msg.Key);
            master.mMessage.MSG_TEXT = msg.Value;
            return View(master);
        }

        [HttpPost]
        public ActionResult ZC05201(M_MASTER master)
        {
            master.TYPE = "Edit";
            if (Request.Form["btnBack_Click"] != null)
            {
                // remove the maintainence screen session and load the search screen
                return RedirectToAction("ZC01011", "ZC010");
            }
            /*  else if (Request.Form["btnClear_Click"] != null)
              {
              master.mMessage.MSG_TEXT = "";
              }
              else if (Request.Form["btnInitialize_Click"] != null)
              {
              KeyValuePair<string, string> msg = repository.InitDisp();
              master.mMessage.MSG_ID = Convert.ToInt32(msg.Key);
              master.mMessage.MSG_TEXT = msg.Value;
              }*/
            else if (Request.Form["btnRegistration_Click"] != null)
            {
                TempData["Message"] = repository.DoRegist(master.mMessage.MSG_ID.ToString(), master.mMessage.MSG_TEXT);
            }

            return View(master);
        }

        [Authorize(Roles = "0,1")]
        public ActionResult ZC05901(M_MASTER master, string TableName, bool IsPostBack = false)
        {
            // メンテナンス対象のテーブルを取得します。
            if ((TableName != null && Session["tableInformation"] == null) && !IsPostBack)
            {
                master.masterMaintenanceScreen = TableName;
                if (master.masterMaintenanceScreen == "M_LOCATION" || master.masterMaintenanceScreen == "M_WORKFLOW" || master.masterMaintenanceScreen == "M_REGULATION" || master.masterMaintenanceScreen == "M_COMMON")
                {
                    List<SelectListItem> items = new List<SelectListItem>();
                    if ((S_TAB_CONTROL)Session[SessionConst.TableInformation] == null)
                    {
                        // テーブル情報を取得します。
                        List<S_TAB_CONTROL> tableControls = Control.GetTableControl();
                        if (master.masterMaintenanceScreen == "M_LOCATION")
                        {
                            tableInformation = tableControls.Single(x => x.TABLE_NAME == "M_LOCATION");
                            tableColumnInformations = Control.GetTableColumnControl("M_LOCATION");

                            foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                            {
                                if (tableColumnInformation.IS_SEARCH)
                                {
                                    foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                                    {
                                        SelectListItem item = new SelectListItem
                                        {
                                            Text = convertData.Value,
                                            Value = convertData.Key
                                        };
                                        items.Add(item);
                                    }
                                }
                            }
                            Session["P_LOC_NAMES"] = items;
                        }
                        else if (master.masterMaintenanceScreen == "M_WORKFLOW")
                        {
                            tableInformation = tableControls.Single(x => x.TABLE_NAME == "M_WORKFLOW");
                            tableColumnInformations = Control.GetTableColumnControl("M_WORKFLOW");
                            foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                            {
                                if (tableColumnInformation.IS_SEARCH)
                                {
                                    foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                                    {
                                        SelectListItem item = new SelectListItem
                                        {
                                            Text = convertData.Value,
                                            Value = convertData.Key
                                        };
                                        items.Add(item);
                                    }
                                }
                            }
                            Session["FLOW_TYPE_ID"] = items;
                        }
                        else if (master.masterMaintenanceScreen == "M_REGULATION")
                        {


                            if ((S_TAB_CONTROL)Session[SessionConst.TableInformation] == null)
                            {

                                Session[SessionConst.TableInformation] = tableInformation;
                                Session[SessionConst.TableColumnInformations] = tableColumnInformations;
                            }
                            else
                            {
                                // セッション変数から取得します。
                                tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                                tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                            }
                        }

                        else if (master.masterMaintenanceScreen == "M_COMMON")
                        {


                            if ((S_TAB_CONTROL)Session[SessionConst.TableInformation] == null)
                            {
                                // テーブル情報を取得します。 

                                // セッション変数に格納します。
                                Session[SessionConst.TableInformation] = tableInformation;
                                Session[SessionConst.TableColumnInformations] = tableColumnInformations;
                            }
                            else
                            {
                                // セッション変数から取得します。
                                tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                                tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                            }
                        }
                        // セッション変数に格納します。
                        Session[SessionConst.TableInformation] = tableInformation;
                        Session[SessionConst.TableColumnInformations] = tableColumnInformations;
                    }

                    else
                    {
                        // セッション変数から取得します。
                        tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                    }
                }
            }

            if (IsPostBack)
            {
                if (Session[SessionConst.MasterModelZC05901] != null)
                {
                    master = (M_MASTER)Session[SessionConst.MasterModelZC05901];
                }
                if (searchEnabled == 1 && Session[SessionConst.WebGridCount] == null)
                {
                    // セッション変数から取得します。
                    tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                    tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                    if (TableName == "M_GROUP")
                    {
                        repository.GroupMasterSearch(master, tableInformation, tableColumnInformations);
                    }
                    else if (TableName == "M_LOCATION")
                    {
                        repository.LocationMasterSearch(master, tableInformation, tableColumnInformations);
                    }
                    else if (TableName == "M_WORKFLOW")
                    {
                        repository.WorkFlowMasterSearch(master, tableInformation, tableColumnInformations);
                    }
                    else if (TableName == "M_MAKER")
                    {
                        repository.MakerMasterSearch(master, tableInformation, tableColumnInformations);
                    }
                    else if (TableName == "M_UNITSIZE")
                    {
                        repository.UnitMasterSearch(master, tableInformation, tableColumnInformations);
                    }
                    else if (TableName == "M_ACTION_TYPE")
                    {
                        repository.ActionMasterSearch(master, tableInformation, tableColumnInformations);
                    }
                    else if (TableName == "M_STATUS")
                    {
                        repository.StatusMasterSearch(master, tableInformation, tableColumnInformations);
                    }
                    else if (master.masterMaintenanceScreen == "M_REGULATION")
                    {
                        master.listRegulationDetails = repository.RegulationMasterSearch(master, tableInformation, tableColumnInformations);
                    }
                    else if (master.masterMaintenanceScreen == "M_COMMON" || TableName == "M_COMMON")
                    {
                        master.listCommonDetails = repository.CommonMasterSearch(master, tableInformation, tableColumnInformations);
                    }
                }
            }
            if (TableName == "M_MAKER" || TableName == "M_UNITSIZE" || TableName == "M_ACTION_TYPE" || TableName == "M_STATUS")
            {
                if (master == null) { master = new M_MASTER(); }
                DataTable Status = new DataTable();
                Status.Columns.Add("Text");
                Status.Columns.Add("Val");
                Status.Rows.Add("使用中", "0");
                Status.Rows.Add("削除済", "1");
                Status.Rows.Add("全て", "2");

                master.LookUpCMICondition = BindLookupValuesStatus(Status);
            }

            master = DropDownFillRegulation(master);
            return View(master);
        }


        /// <summary>
        /// Author: MSCGI 
        /// Created date: 24-7-2019
        /// Description : Get the Approver Name in the Right side of the Superior and Agent textbox in user Setting
        /// <summary>
        [HttpPost]
        public string GetNIDUserName(string UserCode)
        {

            UserInfo.ClsUser userInfo = null;
            string returnName = "";
            WebCommonRepo repository = new WebCommonRepo();
            repository.GetUserMasterList();
            userInfo = repository.getUserList.SingleOrDefault(x => x.USER_CD == UserCode.Trim());
            // if name is available send it or send null
            if (userInfo != null)
            {
                returnName = userInfo.USER_NAME;
            }
            return returnName;
        }

        [HttpPost]
        public bool DeleteNIDUsers(List<string> userCodes)
        {
            if (userCodes.Count > 0)
            {
                Dictionary<string, string> chief = new Dictionary<string, string>();
                if (Session[SessionConst.sessionChemChief] != null)
                {
                    chief = (Dictionary<string, string>)Session[SessionConst.sessionChemChief];
                }

                foreach (string userCode in userCodes)
                {
                    if (chief.ContainsKey(userCode))
                    {
                        chief.Remove(userCode);
                    }
                }

                Session[SessionConst.sessionChemChief] = chief;
            }
            return true;
        }



        [HttpPost]
        public string AddNIDUserName(string userCode)
        {
            Dictionary<string, string> chief = new Dictionary<string, string>();
            if (Session[SessionConst.sessionChemChief] != null)
            {
                chief = (Dictionary<string, string>)Session[SessionConst.sessionChemChief];
            }
            MasterService masterService = new MasterService();
            string userName = masterService.SelectUserName(userCode);
            if (userName != null)
            {
                if (!chief.ContainsKey(userCode))
                {
                    chief.Add(userCode, userName);
                    Session[SessionConst.sessionChemChief] = chief;
                }
            }
            return userName;
        }




        [HttpPost]
        public string LocationBarCodeLabelPrintZC05901()
        {
            List<M_LOCATION> data = (List<M_LOCATION>)Session[SessionConst.WebGrid];
            string retval = "";
            if (data.Count == 0)
            {
                retval = Common.GetMessage("C034");
                return retval;
            }

            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
            List<ILabelData> dataCollection = new List<ILabelData>();

            // 検索結果からバーコードを取得します。
            for (int i = 0; i < data.Count; i++)
            {
                if (data != null)
                {
                    string code = Convert.ToString(data[i].LOC_BARCODE);
                    string name = Convert.ToString(data[i].LOC_NAME);
                    // 親カテゴリの最下層と結合して名前を表示します。
                    string parentCode = Convert.ToString(data[i].P_LOC_ID);

                    if (!string.IsNullOrEmpty(parentCode))
                    {
                        string parentName = tableColumnInformations.Where(x => x.COLUMN_NAME == "P_LOC_ID").Single().ConvertData[parentCode];
                        if (parentName.LastIndexOf(SystemConst.ValuesDelimiter) == -1)
                        {
                            name = parentName + SystemConst.ValuesDelimiter + name;
                        }
                        else
                        {
                            name = parentName.Substring(parentName.LastIndexOf(SystemConst.ValuesDelimiter) + 1) + SystemConst.ValuesDelimiter + name;
                        }
                    }
                    if (!string.IsNullOrEmpty(code))
                    {
                        dataCollection.Add(new LabelData(code, name));
                    }
                }
                else
                {
                    retval = string.Format(WarningMessages.NotInput, "社員コード");
                    return retval;
                }
            }
            PTouchLabelPrint labelPrint = new PTouchLabelPrint(LabelTypes.Location);
            retval = labelPrint.CreatePrintJavaScript(dataCollection);
            return retval;
        }

        [HttpPost]
        public string LocationBarCodeLabelPrintZC05902(string barcode, string locName, string upperLocID)
        {
            string retval = "";

            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
            List<ILabelData> dataCollection = new List<ILabelData>();

            // 親カテゴリの最下層と結合して名前を表示します。

            if (!string.IsNullOrEmpty(upperLocID))
            {
                string parentName = tableColumnInformations.Where(x => x.COLUMN_NAME == "P_LOC_ID").Single().ConvertData[upperLocID];
                if (parentName.LastIndexOf(SystemConst.ValuesDelimiter) == -1)
                {
                    locName = parentName + SystemConst.ValuesDelimiter + locName;
                }
                else
                {
                    locName = parentName.Substring(parentName.LastIndexOf(SystemConst.ValuesDelimiter) + 1) + SystemConst.ValuesDelimiter + locName;
                }
            }
            barcode = barcode.Trim();
            if (!string.IsNullOrEmpty(barcode.Trim()))
            {
                dataCollection.Add(new LabelData(barcode, locName));
            }

            PTouchLabelPrint labelPrint = new PTouchLabelPrint(LabelTypes.Location);
            retval = labelPrint.CreatePrintJavaScript(dataCollection);
            return retval;
        }


        [HttpPost]
        public ActionResult ZC05901(M_MASTER master)
        {
            string tableName = master.masterMaintenanceScreen;

            if ((S_TAB_CONTROL)Session[SessionConst.TableInformation] == null)
            {
                // テーブル情報を取得します。
                List<S_TAB_CONTROL> tableControls = Control.GetTableControl();
                tableInformation = tableControls.Single(x => x.TABLE_NAME == tableName);
                tableColumnInformations = Control.GetTableColumnControl(tableName);

                // セッション変数に格納します。
                Session[SessionConst.TableInformation] = tableInformation;
                Session[SessionConst.TableColumnInformations] = tableColumnInformations;
            }
            else
            {
                // セッション変数から取得します。
                tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
            }


            if (master.masterMaintenanceScreen == "M_GROUP")
            {
                if (Request.Form["btnRegistration_Click"] != null)
                {
                    return RedirectToAction("ZC05902", "ZC050", new { TYPE = "New", TableName = "M_GROUP" });
                }
                else if (Request.Form["btnSearch_Click"] != null)
                {
                    try
                    {
                        Session[SessionConst.MasterModelZC05901] = master;
                        repository.GroupMasterSearch(master, tableInformation, tableColumnInformations);
                        searchEnabled = 1;
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    return View(master);
                }
                else if (Request.Form["btnOutputList_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];

                        List<M_GROUP> searchResult = (List<M_GROUP>)Session[SessionConst.WebGrid];

                        string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        GridViewExtension.GroupOutputCSV(searchResult, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + CsvFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    return View(master);
                }
                else if (Request.Form["btnOutTemplate_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        DataTable templateDataTable = repository.CreateListFormat(tableColumnInformations);

                        string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        MasterMaintenance.Common.TemplateCSV(templateDataTable, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + TemplateFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    return View(master);
                }
                else if (Request.Form["btnInclude_Click"] != null)
                {
                    string serverFileName = "";
                    string fileStatus = "";
                    try
                    {
                        HttpPostedFileBase posted = Request.Files[0];// uploadするファイル
                        string serverPath = Server.MapPath("../upload");    // uploadするフォルダ
                        // ファイルをuploadして uploadしたファイルのパスを取得
                        serverFileName = Common.UploadFile(posted, serverPath);
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }

                    if (serverFileName != "")
                    {
                        int importedCount = 0;
                        string errMessage = "";
                        repository.DoImport(serverFileName, out importedCount, out errMessage);

                        if (importedCount > 0)
                        {
                            fileStatus += importedCount.ToString() + "件が正常に取り込まれました。<br/><br/>";
                        }

                        if (errMessage != "")
                        {
                            fileStatus += "取り込みエラーが発生しました。<br/><br/>[詳細]<br/>" + errMessage;
                        }
                    }
                    ViewBag.FileStatus = fileStatus;
                    return View(master);
                }
                else if (Request.Form["btnBack_Click"] != null)
                {
                    searchEnabled = 0;

                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    Session.Remove(SessionConst.TableColumnInformations);
                    Session.Remove(SessionConst.TableInformation);
                    Session.Remove(SessionConst.MasterModelZC05901);
                    TempData["Error"] = null;
                    TempData["Message"] = null;
                    return RedirectToAction("ZC01011", "ZC010");
                }
                else if (Request.Form["btnClear_Click"] != null)
                {
                    searchEnabled = 0;
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    return RedirectToAction("ZC05901", "ZC050", new { TableName = "M_GROUP" });
                }
                else
                {
                    // Edit Individual Records
                    master.TYPE = "Update";
                    master.mGroup.GROUP_CD = master.mGroup.UPDATE_GROUP_CD;
                    master.mGroup.GROUP_NAME = master.mGroup.UPDATE_GROUP_NAME;
                    master.mGroup.P_GROUP_NAME = master.mGroup.UPDATE_P_GROUP_NAME;
                    if (master.mGroup.UPDATE_DEL_FLAGNAME == "使用中")
                    {
                        master.mGroup.DEL_FLG = false;
                    }
                    else if (master.mGroup.UPDATE_DEL_FLAGNAME == "削除済")
                    {
                        master.mGroup.DEL_FLG = true;
                    }

                    if (master.mGroup.UPDATE_BATCH_FLAGNAME == "対象")
                    {
                        master.mGroup.BATCH_FLG = false;
                    }
                    else if (master.mGroup.UPDATE_BATCH_FLAGNAME == "対象外")
                    {
                        master.mGroup.BATCH_FLG = true;
                    }

                    Session[SessionConst.MasterModelZC05902] = master;
                    return RedirectToAction("ZC05902", "ZC050", new { Type = "Update", TableName = "M_GROUP" });
                }
            }
            else if (master.masterMaintenanceScreen == "M_REGULATION")
            {
                if (Request.Form["btnNewRegulation_Click"] != null)
                {
                    return RedirectToAction("ZC05902", "ZC050", new { TYPE = "New", TableName = "M_REGULATION" });
                }
                else if (Request.Form["btnSearch_Click"] != null)
                {
                    try
                    {
                        Session[SessionConst.MasterModelZC05901] = master;
                        master.listRegulationDetails = repository.RegulationMasterSearch(master, tableInformation, tableColumnInformations);
                        searchEnabled = 1;
                        master.TYPE = "Search";
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    master = DropDownFillRegulation(master);
                    return View(master);
                }
                else if (Request.Form["btnOutList_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];

                        List<M_REGULATION> searchResult = (List<M_REGULATION>)Session[SessionConst.WebGrid];

                        var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        ChemMgmt.Classes.Extensions.GridViewExtension.RegulationOutputCSV(searchResult, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + CsvFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    master = DropDownFillRegulation(master);
                    return View(master);
                }
                else if (Request.Form["btnOutTemplate_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        DataTable templateDataTable = repository.CreateListFormat(tableColumnInformations);

                        //    "*状態","部署ID","*部署コード","*部署名","上位部署名","登録者","登録日時","更新者","更新日時","削除者","削除日時","バッチ更新対象外"
                        string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        MasterMaintenance.Common.TemplateCSV(templateDataTable, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + TemplateFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    master = DropDownFillRegulation(master);
                    return View(master);
                }
                else if (Request.Form["btnClear_Click"] != null)
                {
                    searchEnabled = 0;
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    return RedirectToAction("ZC05901", "ZC050", new { TableName = "M_REGULATION" });
                }
                else if (Request.Form["btnBack_Click"] != null)
                {
                    searchEnabled = 0;
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    Session.Remove(SessionConst.TableColumnInformations);
                    Session.Remove(SessionConst.TableInformation);
                    Session.Remove(SessionConst.MasterModelZC05901);
                    TempData["Error"] = null;
                    TempData["Message"] = null;
                    master = DropDownFillRegulation(master);
                    return RedirectToAction("ZC01011", "ZC010");
                }
            }
            else if (master.masterMaintenanceScreen == "M_MAKER")
            {
                try
                {
                    if (Request.Form["btnNewSignupMaker"] != null)
                    {
                        return RedirectToAction("ZC05902", "ZC050", new { Type = "New", TableName = "M_MAKER" });
                    }
                    else if (Request.Form["btnBack_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        Session.Remove(SessionConst.TableColumnInformations);
                        Session.Remove(SessionConst.TableInformation);
                        TempData["Error"] = null;
                        TempData["Message"] = null;
                        return RedirectToAction("ZC01011", "ZC010");
                    }
                    else if (Request.Form["btnClear_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session[SessionConst.MasterModelZC05901] = null;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        return RedirectToAction("ZC05901", "ZC050", new { TableName = "M_MAKER" });
                    }
                    else if (Request.Form["btnSearch_Click"] != null)
                    {
                        Session[SessionConst.MasterModelZC05901] = master;
                        // string del_Flag = (master.mMaker.DEL_FLAG).ToString();
                        repository.MakerMasterSearch(master, tableInformation, tableColumnInformations);
                        searchEnabled = 1;

                        //   ConvertMakerMaster(tableColumnInformations, m_master);
                        DataTable mStatus1 = new DataTable();
                        mStatus1.Columns.Add("Text");
                        mStatus1.Columns.Add("Val");
                        mStatus1.Rows.Add("使用中", "0");
                        mStatus1.Rows.Add("削除済", "1");
                        mStatus1.Rows.Add("全て", "2");
                        master.LookUpCMICondition = BindLookupValuesStatus(mStatus1);
                        Session[SessionConst.MasterModelZC05901] = master;
                        return View(master);
                    }
                    else if (Request.Form["btnOutputList_Click"] != null)
                    {
                        try
                        {
                            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];


                            List<M_MAKER> searchResult = new List<M_MAKER>();
                            if (Session[SessionConst.MasterModelZC05901] != null)
                            {
                                //M_MASTER mstr = new M_MASTER();
                                //mstr = (M_MASTER)Session[SessionConst.MasterModelZC05901];
                                //searchResult = mstr.listMakerDetails;
                                searchResult = (List<M_MAKER>)Session[SessionConst.WebGrid];
                            }

                            var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            GridViewExtension.MakerOutputCSV(searchResult, csvFileFullPath);
                            string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + CsvFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                            DataTable mStatus2 = new DataTable();
                            mStatus2.Columns.Add("Text");
                            mStatus2.Columns.Add("Val");
                            mStatus2.Rows.Add("使用中", "0");
                            mStatus2.Rows.Add("削除済", "1");
                            mStatus2.Rows.Add("全て", "2");
                            master.LookUpCMICondition = BindLookupValuesStatus(mStatus2);
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }
                        return View(master);
                    }
                    else if (Request.Form["btnOutTemplate_Click"] != null)
                    {
                        try
                        {
                            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                            DataTable templateDataTable = repository.CreateListFormat(tableColumnInformations);

                            // "*状態","部署ID","*部署コード","*部署名","上位部署名","登録者","登録日時","更新者","更新日時","削除者","削除日時","バッチ更新対象外"
                            string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            MasterMaintenance.Common.TemplateCSV(templateDataTable, csvFileFullPath);
                            string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + TemplateFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                            DataTable mStatus3 = new DataTable();
                            mStatus3.Columns.Add("Text");
                            mStatus3.Columns.Add("Val");
                            mStatus3.Rows.Add("使用中", "0");
                            mStatus3.Rows.Add("削除済", "1");
                            mStatus3.Rows.Add("全て", "2");
                            master.LookUpCMICondition = BindLookupValuesStatus(mStatus3);
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }

                        return View(master);
                    }
                    else if (Request.Form["btnInclude_Click"] != null)
                    {
                        string serverFileName = "";
                        string fileStatus = "";
                        try
                        {
                            HttpPostedFileBase posted = Request.Files[0];// uploadするファイル
                            string serverPath = Server.MapPath("../upload");    // uploadするフォルダ
                                                                                // ファイルをuploadして uploadしたファイルのパスを取得
                            serverFileName = Common.UploadFile(posted, serverPath);
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }

                        if (serverFileName != "")
                        {
                            int importedCount = 0;
                            string errMessage = "";
                            repository.DoImport(serverFileName, out importedCount, out errMessage);

                            if (importedCount > 0)
                            {
                                fileStatus += importedCount.ToString() + "件が正常に取り込まれました。<br/><br/>";
                            }

                            if (errMessage != "")
                            {
                                fileStatus += "取り込みエラーが発生しました。<br/><br/>[詳細]<br/>" + errMessage;
                            }
                        }
                        if (Session[SessionConst.MasterModelZC05901] != null)
                        {
                            master = (M_MASTER)Session[SessionConst.MasterModelZC05901];
                        }
                        //Session[SessionConst.MasterModelZC05901] = master;
                        ViewBag.FileStatus = fileStatus;
                        DataTable mStatus4 = new DataTable();
                        mStatus4.Columns.Add("Text");
                        mStatus4.Columns.Add("Val");
                        mStatus4.Rows.Add("使用中", "0");
                        mStatus4.Rows.Add("削除済", "1");
                        mStatus4.Rows.Add("全て", "2");
                        master.LookUpCMICondition = BindLookupValuesStatus(mStatus4);
                        return View(master);
                    }
                    else if (Request.Form["btnBack_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        Session.Remove(SessionConst.TableColumnInformations);
                        Session.Remove(SessionConst.TableInformation);

                        TempData["Error"] = null;
                        TempData["Message"] = null;
                        return RedirectToAction("ZC01011", "ZC010");
                    }
                    else
                    {
                        // Edit Individual Records
                        master.TYPE = "Update";
                        master.mMaker.MAKER_ID = master.mMaker.MAKER_ID;
                        master.mMaker.MAKER_NAME = master.mMaker.MAKER_NAME;

                        Session[SessionConst.MasterModelZC05902] = master;
                        return RedirectToAction("ZC05902", "ZC050", new { Type = "Update", TableName = "M_MAKER" });
                    }
                }
                catch (Exception ex)
                {
                    _logoututil.OutErrLog(ProgramId, ex.ToString());
                }
            }
            else if (master.masterMaintenanceScreen == "M_UNITSIZE")
            {
                try
                {
                    if (Request.Form["btnNewSignupMaker"] != null)
                    {
                        return RedirectToAction("ZC05902", "ZC050", new { Type = "New", TableName = "M_UNITSIZE" });
                    }
                    else if (Request.Form["btnBack_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        Session.Remove(SessionConst.TableColumnInformations);
                        Session.Remove(SessionConst.TableInformation);
                        TempData["Error"] = null;
                        TempData["Message"] = null;
                        return RedirectToAction("ZC01011", "ZC010");
                    }
                    else if (Request.Form["btnClear_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session[SessionConst.MasterModelZC05901] = null;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        return RedirectToAction("ZC05901", "ZC050", new { TableName = "M_UNITSIZE" });
                    }
                    else if (Request.Form["btnSearch_Click"] != null)
                    {
                        //  string del_Flag = (master.mUnitSize.DEL_FLAG).ToString();
                        repository.UnitMasterSearch(master, tableInformation, tableColumnInformations);
                        searchEnabled = 1;

                        DataTable mStatus1 = new DataTable();
                        mStatus1.Columns.Add("Text");
                        mStatus1.Columns.Add("Val");
                        mStatus1.Rows.Add("使用中", "0");
                        mStatus1.Rows.Add("削除済", "1");
                        mStatus1.Rows.Add("全て", "2");

                        master.LookUpCMICondition = BindLookupValuesStatus(mStatus1);
                        Session[SessionConst.MasterModelZC05901] = master;
                        return View(master);
                    }
                    else if (Request.Form["btnOutputList_Click"] != null)
                    {
                        try
                        {
                            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];


                            List<M_UNITSIZE> searchResult = new List<M_UNITSIZE>();
                            if (Session[SessionConst.MasterModelZC05901] != null)
                            {
                                searchResult = (List<M_UNITSIZE>)Session[SessionConst.WebGrid];
                            }

                            var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            GridViewExtension.UnitOutputCSV(searchResult, csvFileFullPath, false, false);
                            string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + CsvFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                            DataTable mStatus1 = new DataTable();
                            mStatus1.Columns.Add("Text");
                            mStatus1.Columns.Add("Val");
                            mStatus1.Rows.Add("使用中", "0");
                            mStatus1.Rows.Add("削除済", "1");
                            mStatus1.Rows.Add("全て", "2");

                            master.LookUpCMICondition = BindLookupValuesStatus(mStatus1);
                            Session[SessionConst.MasterModelZC05901] = master;
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }
                        return View(master);
                    }
                    else if (Request.Form["btnOutTemplate_Click"] != null)
                    {
                        try
                        {
                            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                            DataTable templateDataTable = repository.CreateListFormat(tableColumnInformations);

                            // "*状態","部署ID","*部署コード","*部署名","上位部署名","登録者","登録日時","更新者","更新日時","削除者","削除日時","バッチ更新対象外"
                            string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            MasterMaintenance.Common.TemplateCSV(templateDataTable, csvFileFullPath);
                            string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + TemplateFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                            DataTable mStatus1 = new DataTable();
                            mStatus1.Columns.Add("Text");
                            mStatus1.Columns.Add("Val");
                            mStatus1.Rows.Add("使用中", "0");
                            mStatus1.Rows.Add("削除済", "1");
                            mStatus1.Rows.Add("全て", "2");

                            master.LookUpCMICondition = BindLookupValuesStatus(mStatus1);
                            Session[SessionConst.MasterModelZC05901] = master;
                            return View(master);
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }
                        return View(master);
                    }
                    else if (Request.Form["btnInclude_Click"] != null)
                    {
                        string serverFileName = "";
                        string fileStatus = "";
                        try
                        {
                            HttpPostedFileBase posted = Request.Files[0];// uploadするファイル
                            string serverPath = Server.MapPath("../upload");    // uploadするフォルダ
                                                                                // ファイルをuploadして uploadしたファイルのパスを取得
                            serverFileName = Common.UploadFile(posted, serverPath);
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }

                        if (serverFileName != "")
                        {
                            int importedCount = 0;
                            string errMessage = "";
                            repository.DoImport(serverFileName, out importedCount, out errMessage);

                            if (importedCount > 0)
                            {
                                fileStatus += importedCount.ToString() + "件が正常に取り込まれました。<br/><br/>";
                            }

                            if (errMessage != "")
                            {
                                fileStatus += "取り込みエラーが発生しました。<br/><br/>[詳細]<br/>" + errMessage;
                            }
                        }
                        if (Session[SessionConst.MasterModelZC05901] != null)
                        {
                            master = (M_MASTER)Session[SessionConst.MasterModelZC05901];
                        }
                        ViewBag.FileStatus = fileStatus;
                        DataTable mStatus1 = new DataTable();
                        mStatus1.Columns.Add("Text");
                        mStatus1.Columns.Add("Val");
                        mStatus1.Rows.Add("使用中", "0");
                        mStatus1.Rows.Add("削除済", "1");
                        mStatus1.Rows.Add("全て", "2");
                        master.LookUpCMICondition = BindLookupValuesStatus(mStatus1);
                        return View(master);
                    }
                    else if (Request.Form["btnBack_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        Session.Remove(SessionConst.TableColumnInformations);
                        Session.Remove(SessionConst.TableInformation);

                        TempData["Error"] = null;
                        TempData["Message"] = null;
                        return RedirectToAction("ZC01011", "ZC010");
                    }
                    else
                    {
                        // Edit Individual Records
                        master.TYPE = "Update";
                        master.mUnitSize.UNITSIZE_ID = master.mUnitSize.UNITSIZE_ID;
                        master.mUnitSize.UNIT_NAME = master.mUnitSize.UNIT_NAME;

                        Session[SessionConst.MasterModelZC05902] = master;
                        return RedirectToAction("ZC05902", "ZC050", new { Type = "Update", TableName = "M_UNITSIZE" });
                    }
                }
                catch (Exception ex)
                {
                    _logoututil.OutErrLog(ProgramId, ex.ToString());
                }
            }
            else if (master.masterMaintenanceScreen == "M_ACTION_TYPE")
            {
                try
                {
                    if (Request.Form["btnSignupOperation"] != null)
                    {
                        return RedirectToAction("ZC05902", "ZC050", new { Type = "New", TableName = "M_ACTION_TYPE" });
                    }
              /*      else if (Request.Form["btnBack_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        Session.Remove(SessionConst.TableColumnInformations);
                        Session.Remove(SessionConst.TableInformation);
                        TempData["Error"] = null;
                        TempData["Message"] = null;
                        return RedirectToAction("ZC01011", "ZC010");
                    }*/
                    else if (Request.Form["btnBack_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        Session.Remove(SessionConst.TableColumnInformations);
                        Session.Remove(SessionConst.TableInformation);

                        TempData["Error"] = null;
                        TempData["Message"] = null;
                        return RedirectToAction("ZC01011", "ZC010");
                    }
                    else if (Request.Form["btnClear_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        return RedirectToAction("ZC05901", "ZC050", new { TableName = "M_ACTION_TYPE" });
                    }
                    else if (Request.Form["btnSearch_Click"] != null)
                    {
                        repository.ActionMasterSearch(master, tableInformation, tableColumnInformations);
                        searchEnabled = 1;
                        Session[SessionConst.MasterModelZC05901] = master;
                        return View(master);
                    }
                    else if (Request.Form["btnOutputList_Click"] != null)
                    {
                        try
                        {
                            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];


                            List<M_ACTION> searchResult = new List<M_ACTION>();
                            if (Session[SessionConst.MasterModelZC05901] != null)
                            {
                                searchResult = (List<M_ACTION>)Session[SessionConst.WebGrid];
                            }

                            var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            GridViewExtension.ActionOutputCSV(searchResult, csvFileFullPath, false, false);
                            string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + CsvFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));

                            List<SelectListItem> listCSNCondition1 = new List<SelectListItem> { };
                            new ChemMgmt.Classes.UsageStatusListMasterInfo().GetSelectList().ToList().
                                ForEach(x => listCSNCondition1.Add(new SelectListItem
                                {
                                    Value = x.Id.ToString(),
                                    Text = x.Name
                                }));
                            master.LookUpCMICondition = listCSNCondition1;
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }
                        return View(master);
                    }
                    else if (Request.Form["btnOutTemplate_Click"] != null)
                    {
                        try
                        {
                            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                            DataTable templateDataTable = repository.CreateListFormat(tableColumnInformations);

                            // "*状態","部署ID","*部署コード","*部署名","上位部署名","登録者","登録日時","更新者","更新日時","削除者","削除日時","バッチ更新対象外"
                            string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            MasterMaintenance.Common.TemplateCSV(templateDataTable, csvFileFullPath);
                            string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + TemplateFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }
                        return View(master);
                    }
                    else if (Request.Form["btnInclude_Click"] != null)
                    {
                        string serverFileName = "";
                        string fileStatus = "";
                        try
                        {
                            HttpPostedFileBase posted = Request.Files[0];// uploadするファイル
                            string serverPath = Server.MapPath("../upload");    // uploadするフォルダ
                                                                                // ファイルをuploadして uploadしたファイルのパスを取得
                            serverFileName = Common.UploadFile(posted, serverPath);
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }

                        if (serverFileName != "")
                        {
                            int importedCount = 0;
                            string errMessage = "";
                            repository.DoImport(serverFileName, out importedCount, out errMessage);

                            if (importedCount > 0)
                            {
                                fileStatus += importedCount.ToString() + "件が正常に取り込まれました。<br/><br/>";
                            }

                            if (errMessage != "")
                            {
                                fileStatus += "取り込みエラーが発生しました。<br/><br/>[詳細]<br/>" + errMessage;
                            }
                        }
                        ViewBag.FileStatus = fileStatus;
                        return View(master);
                    }
                    else
                    {
                        // Edit Individual Records
                        master.TYPE = "Update";
                        master.mActionType.ACTION_TYPE_ID = master.mActionType.UPDATE_ACTION_TYPE_ID;
                        master.mActionType.ACTION_TEXT = master.mActionType.UPDATE_ACTION_TEXT;

                        Session[SessionConst.MasterModelZC05902] = master;
                        return RedirectToAction("ZC05902", "ZC050", new { Type = "Update", TableName = "M_ACTION_TYPE" });
                    }
                }
                catch (Exception ex)
                {
                    _logoututil.OutErrLog(ProgramId, ex.ToString());
                }
            }
            else if (master.masterMaintenanceScreen == "M_STATUS")
            {
                try
                {
                    if (Request.Form["btnNewSignupMaker"] != null)
                    {
                        return RedirectToAction("ZC05902", "ZC050", new { Type = "New", TableName = "M_STATUS" });
                    }
                    else if (Request.Form["btnBack_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        Session.Remove(SessionConst.TableColumnInformations);
                        Session.Remove(SessionConst.TableInformation);
                        TempData["Error"] = null;
                        TempData["Message"] = null;
                        return RedirectToAction("ZC01011", "ZC010");
                    }
                    else if (Request.Form["btnClear_Click"] != null)
                    {
                        searchEnabled = 0;
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        return RedirectToAction("ZC05901", "ZC050", new { TableName = "M_STATUS" });
                    }
                    else if (Request.Form["btnSearch_Click"] != null)
                    {
                        repository.StatusMasterSearch(master, tableInformation, tableColumnInformations);
                        searchEnabled = 1;
                        Session[SessionConst.MasterModelZC05901] = master;
                        return View(master);
                    }
                    else if (Request.Form["btnOutputList_Click"] != null)
                    {
                        try
                        {
                            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];


                            List<M_STATUS> searchResult = new List<M_STATUS>();
                            if (Session[SessionConst.MasterModelZC05901] != null)
                            {
                                searchResult = (List<M_STATUS>)Session[SessionConst.WebGrid];
                            }

                            var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            GridViewExtension.StatusOutputCSV(searchResult, csvFileFullPath, false, false);
                            string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + CsvFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));

                            List<SelectListItem> listCSNCondition1 = new List<SelectListItem> { };
                            new ChemMgmt.Classes.UsageStatusListMasterInfo().GetSelectList().ToList().
                                ForEach(x => listCSNCondition1.Add(new SelectListItem
                                {
                                    Value = x.Id.ToString(),
                                    Text = x.Name
                                }));
                            master.LookUpCMICondition = listCSNCondition1;
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }
                        return View(master);
                    }
                    else if (Request.Form["btnOutTemplate_Click"] != null)
                    {
                        try
                        {
                            S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                            List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                            DataTable templateDataTable = repository.CreateListFormat(tableColumnInformations);

                            // "*状態","部署ID","*部署コード","*部署名","上位部署名","登録者","登録日時","更新者","更新日時","削除者","削除日時","バッチ更新対象外"
                            string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            MasterMaintenance.Common.TemplateCSV(templateDataTable, csvFileFullPath);
                            string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + TemplateFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }
                        return View(master);
                    }
                    else if (Request.Form["btnInclude_Click"] != null)
                    {
                        string serverFileName = "";
                        string fileStatus = "";
                        try
                        {
                            HttpPostedFileBase posted = Request.Files[0];// uploadするファイル
                            string serverPath = Server.MapPath("../upload");    // uploadするフォルダ
                                                                                // ファイルをuploadして uploadしたファイルのパスを取得
                            serverFileName = Common.UploadFile(posted, serverPath);
                        }
                        catch (Exception ex)
                        {
                            _logoututil.OutErrLog(ProgramId, ex.ToString());
                        }

                        if (serverFileName != "")
                        {
                            int importedCount = 0;
                            string errMessage = "";
                            repository.DoImport(serverFileName, out importedCount, out errMessage);

                            if (importedCount > 0)
                            {
                                fileStatus += importedCount.ToString() + "件が正常に取り込まれました。<br/><br/>";
                            }

                            if (errMessage != "")
                            {
                                fileStatus += "取り込みエラーが発生しました。<br/><br/>[詳細]<br/>" + errMessage;
                            }
                        }
                        ViewBag.FileStatus = fileStatus;
                        List<SelectListItem> listCSNCondition2 = new List<SelectListItem> { };
                        new ChemMgmt.Classes.UsageStatusListMasterInfo().GetSelectList().ToList().
                            ForEach(x => listCSNCondition2.Add(new SelectListItem
                            {
                                Value = x.Id.ToString(),
                                Text = x.Name
                            }));
                        master.LookUpCMICondition = listCSNCondition2;
                        return View(master);
                    }
                    else
                    {
                        // Edit Individual Records
                        master.TYPE = "Update";
                        master.mStatus.STATUS_ID = master.mStatus.UPDATE_STATUS_ID;
                        master.mStatus.STATUS_TEXT = master.mStatus.UPDATE_STATUS_TEXT;
                        master.mStatus.CLASS_ID = Convert.ToInt32(master.mStatus.UPDATE_CLASS_ID);
                        //     var tableColumnInformations = Control.GetTableColumnControl(newMaster.masterMaintenanceScreen);
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Text");
                        dt.Columns.Add("Val");
                        foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                        {
                            if (tableColumnInformation.IS_SEARCH)
                            {
                                foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                                {
                                    dt.Rows.Add(convertData.Value, convertData.Key);
                                    master.LookUpStatus = BindLookupValuesStatus(dt);
                                }
                            }
                        }
                        Session[SessionConst.MasterModelZC05902] = master;
                        return RedirectToAction("ZC05902", "ZC050", new { Type = "Update", TableName = "M_STATUS" });
                    }
                }
                catch (Exception ex)
                {
                    _logoututil.OutErrLog(ProgramId, ex.ToString());
                }
            }
            else if (master.masterMaintenanceScreen == "M_WORKFLOW")
            {
                if (Request.Form["btnRegistration_Click"] != null)
                {
                    return RedirectToAction("ZC05902", "ZC050", new { TYPE = "New", TableName = "M_WORKFLOW" });
                }
                else if (Request.Form["btnSearch_Click"] != null)
                {
                    try
                    {
                        Session[SessionConst.MasterModelZC05901] = master;
                        repository.WorkFlowMasterSearch(master, tableInformation, tableColumnInformations);
                        searchEnabled = 1;
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    return View(master);
                }
                else if (Request.Form["btnOutputList_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];

                        List<M_WORKFLOW> searchResult = (List<M_WORKFLOW>)Session[SessionConst.WebGrid];

                        string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        ChemMgmt.Classes.Extensions.GridViewExtension.WorkflowOutputCSV(searchResult, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + CsvFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    return View(master);
                }
                else if (Request.Form["btnOutTemplate_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        DataTable templateDataTable = repository.CreateListFormat(tableColumnInformations);

                        string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        MasterMaintenance.Common.TemplateCSV(templateDataTable, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + TemplateFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    return View(master);
                }
                else if (Request.Form["btnInclude_Click"] != null)
                {
                    string serverFileName = "";
                    string fileStatus = "";
                    try
                    {
                        HttpPostedFileBase posted = Request.Files[0];// uploadするファイル
                        string serverPath = Server.MapPath("../upload");    // uploadするフォルダ
                        // ファイルをuploadして uploadしたファイルのパスを取得
                        serverFileName = Common.UploadFile(posted, serverPath);
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }

                    if (serverFileName != "")
                    {
                        int importedCount = 0;
                        string errMessage = "";
                        repository.DoImport(serverFileName, out importedCount, out errMessage);

                        if (importedCount > 0)
                        {
                            fileStatus += importedCount.ToString() + "件が正常に取り込まれました。<br/><br/>";
                        }

                        if (errMessage != "")
                        {
                            fileStatus += "取り込みエラーが発生しました。<br/><br/>[詳細]<br/>" + errMessage;
                        }
                    }
                    ViewBag.FileStatus = fileStatus;
                    return View(master);
                }
                else if (Request.Form["btnBack_Click"] != null)
                {
                    searchEnabled = 0;
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    Session.Remove(SessionConst.TableColumnInformations);
                    Session.Remove(SessionConst.TableInformation);
                    Session.Remove(SessionConst.MasterModelZC05901);
                    TempData["Error"] = null;
                    TempData["Message"] = null;
                    return RedirectToAction("ZC01011", "ZC010");
                }
                else if (Request.Form["btnClear_Click"] != null)
                {
                    searchEnabled = 0;
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    return RedirectToAction("ZC05901", "ZC050", new { TableName = "M_WORKFLOW" });
                }
                else
                {
                    //Edit Individual Records
                    master.TYPE = "Update";
                    master.mWorkFlow.FLOW_CD = master.mWorkFlow.UPDATE_FLOW_CD;
                    master.mWorkFlow.FLOW_ID = master.mWorkFlow.UPDATE_FLOW_ID;
                    //master.mWorkFlow.FLOW_TYPE_ID = master.mWorkFlow.UPDATE_FLOW_TYPE_ID;
                    master.mWorkFlow.FLOW_NAME = master.mWorkFlow.UPDATE_FLOW_NAME;
                    if (master.mWorkFlow.UPDATE_FLOW_NAME == "化学物質管理台帳申請")
                    {
                        master.mWorkFlow.FLOW_TYPE_ID = "1";
                    }
                    master.mWorkFlow.USAGE = master.mWorkFlow.UPDATE_USAGE;
                    master.mWorkFlow.FLOW_HIERARCHY = master.mWorkFlow.UPDATE_FLOW_HIERARCHY;
                    if (master.mWorkFlow.UPDATE_DEL_FLAGNAME == "使用中")
                    {
                        master.mWorkFlow.DEL_FLG = false;
                    }
                    else if (master.mWorkFlow.UPDATE_DEL_FLAGNAME == "削除済")
                    {
                        master.mWorkFlow.DEL_FLG = true;
                    }
                    master.mWorkFlow.APPROVAL_TARGET1 = master.mWorkFlow.UPDATE_APPROVAL_TARGET1;
                    master.mWorkFlow.APPROVAL_TARGET2 = master.mWorkFlow.UPDATE_APPROVAL_TARGET2;
                    master.mWorkFlow.APPROVAL_TARGET3 = master.mWorkFlow.UPDATE_APPROVAL_TARGET3;
                    master.mWorkFlow.APPROVAL_TARGET4 = master.mWorkFlow.UPDATE_APPROVAL_TARGET4;
                    master.mWorkFlow.APPROVAL_TARGET5 = master.mWorkFlow.UPDATE_APPROVAL_TARGET5;
                    master.mWorkFlow.APPROVAL_TARGET6 = master.mWorkFlow.UPDATE_APPROVAL_TARGET6;

                    if (master.mWorkFlow.UPDATE_APPROVAL_USER_CD1 != null)
                    {
                        master.mWorkFlow.APPROVAL_USER_CD1 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD1.Trim();
                        master.mWorkFlow.APPROVAL_USER_CD1_NM = GetNIDUserName(master.mWorkFlow.APPROVAL_USER_CD1);
                    }
                    if (master.mWorkFlow.UPDATE_APPROVAL_USER_CD2 != null)
                    {
                        master.mWorkFlow.APPROVAL_USER_CD2 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD2.Trim();
                        master.mWorkFlow.APPROVAL_USER_CD2_NM = GetNIDUserName(master.mWorkFlow.APPROVAL_USER_CD2);
                    }
                    if (master.mWorkFlow.UPDATE_APPROVAL_USER_CD3 != null)
                    {
                        master.mWorkFlow.APPROVAL_USER_CD3 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD3.Trim();
                        master.mWorkFlow.APPROVAL_USER_CD3_NM = GetNIDUserName(master.mWorkFlow.APPROVAL_USER_CD3);
                    }
                    if (master.mWorkFlow.UPDATE_APPROVAL_USER_CD4 != null)
                    {
                        master.mWorkFlow.APPROVAL_USER_CD4 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD4.Trim();
                        master.mWorkFlow.APPROVAL_USER_CD4_NM = GetNIDUserName(master.mWorkFlow.APPROVAL_USER_CD4);
                    }
                    if (master.mWorkFlow.UPDATE_APPROVAL_USER_CD5 != null)
                    {
                        master.mWorkFlow.APPROVAL_USER_CD5 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD5.Trim();
                        master.mWorkFlow.APPROVAL_USER_CD5_NM = GetNIDUserName(master.mWorkFlow.APPROVAL_USER_CD5);
                    }
                    if (master.mWorkFlow.UPDATE_APPROVAL_USER_CD6 != null)
                    {
                        master.mWorkFlow.APPROVAL_USER_CD6 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD6.Trim();
                        master.mWorkFlow.APPROVAL_USER_CD6_NM = GetNIDUserName(master.mWorkFlow.APPROVAL_USER_CD6);
                    }

                    //master.mWorkFlow.APPROVAL_USER_CD2 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD2;
                    //master.mWorkFlow.APPROVAL_USER_CD3 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD3;
                    //master.mWorkFlow.APPROVAL_USER_CD4 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD4;
                    //master.mWorkFlow.APPROVAL_USER_CD5 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD5;
                    //master.mWorkFlow.APPROVAL_USER_CD6 = master.mWorkFlow.UPDATE_APPROVAL_USER_CD6;
                    if (master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD1 != null)
                    {
                        master.mWorkFlow.SUBSITUTE_USER_CD1 = master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD1.Trim();
                        master.mWorkFlow.SUBSITUTE_USER_CD1_NM = GetNIDUserName(master.mWorkFlow.SUBSITUTE_USER_CD1);
                    }
                    if (master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD2 != null)
                    {
                        master.mWorkFlow.SUBSITUTE_USER_CD2 = master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD2.Trim();
                        master.mWorkFlow.SUBSITUTE_USER_CD2_NM = GetNIDUserName(master.mWorkFlow.SUBSITUTE_USER_CD2);
                    }
                    if (master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD3 != null)
                    {
                        master.mWorkFlow.SUBSITUTE_USER_CD3 = master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD3.Trim();
                        master.mWorkFlow.SUBSITUTE_USER_CD3_NM = GetNIDUserName(master.mWorkFlow.SUBSITUTE_USER_CD3);
                    }
                    if (master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD4 != null)
                    {
                        master.mWorkFlow.SUBSITUTE_USER_CD4 = master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD4.Trim();
                        master.mWorkFlow.SUBSITUTE_USER_CD4_NM = GetNIDUserName(master.mWorkFlow.SUBSITUTE_USER_CD4);
                    }
                    if (master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD5 != null)
                    {
                        master.mWorkFlow.SUBSITUTE_USER_CD5 = master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD5.Trim();
                        master.mWorkFlow.SUBSITUTE_USER_CD5_NM = GetNIDUserName(master.mWorkFlow.SUBSITUTE_USER_CD5);
                    }
                    if (master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD6 != null)
                    {
                        master.mWorkFlow.SUBSITUTE_USER_CD6 = master.mWorkFlow.UPDATE_SUBSITUTE_USER_CD6.Trim();
                        master.mWorkFlow.SUBSITUTE_USER_CD6_NM = GetNIDUserName(master.mWorkFlow.SUBSITUTE_USER_CD6);
                    }

                    Session[SessionConst.MasterModelZC05902] = master;
                    return RedirectToAction("ZC05902", "ZC050", new { Type = "Update", TableName = "M_WORKFLOW" });
                }
            }
            else if (master.masterMaintenanceScreen == "M_LOCATION")
            {
                if (Request.Form["btnRegistration_Click"] != null)
                {
                    return RedirectToAction("ZC05902", "ZC050", new { TYPE = "New", TableName = "M_LOCATION" });
                }
                else if (Request.Form["btnSearch_Click"] != null)
                {
                    try
                    {
                        Session[SessionConst.MasterModelZC05901] = master;
                        repository.LocationMasterSearch(master, tableInformation, tableColumnInformations);
                        searchEnabled = 1;
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    return View(master);
                }
                else if (Request.Form["btnOutputList_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];

                        List<M_LOCATION> searchResult = (List<M_LOCATION>)Session[SessionConst.WebGrid];

                        string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        ChemMgmt.Classes.Extensions.GridViewExtension.LocationOutputCSV(searchResult, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + CsvFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    return View(master);
                }
                else if (Request.Form["btnOutTemplate_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        DataTable templateDataTable = repository.CreateListFormat(tableColumnInformations);

                        string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        MasterMaintenance.Common.TemplateCSV(templateDataTable, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + TemplateFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    return View(master);
                }
                else if (Request.Form["btnInclude_Click"] != null)
                {
                    string serverFileName = "";
                    string fileStatus = "";
                    try
                    {
                        HttpPostedFileBase posted = Request.Files[0];// uploadするファイル
                        string serverPath = Server.MapPath("../upload");    // uploadするフォルダ
                        // ファイルをuploadして uploadしたファイルのパスを取得
                        serverFileName = Common.UploadFile(posted, serverPath);
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }

                    if (serverFileName != "")
                    {
                        int importedCount = 0;
                        string errMessage = "";
                        repository.DoImport(serverFileName, out importedCount, out errMessage);

                        if (importedCount > 0)
                        {
                            fileStatus += importedCount.ToString() + "件が正常に取り込まれました。<br/><br/>";
                        }

                        if (errMessage != "")
                        {
                            fileStatus += "取り込みエラーが発生しました。<br/><br/>[詳細]<br/>" + errMessage;
                        }
                    }
                    ViewBag.FileStatus = fileStatus;
                    return View(master);
                }
                else if (Request.Form["btnBack_Click"] != null)
                {
                    searchEnabled = 0;
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    Session.Remove(SessionConst.sessionChemChief);
                    Session.Remove(SessionConst.TableColumnInformations);
                    Session.Remove(SessionConst.TableInformation);
                    Session.Remove(SessionConst.MasterModelZC05901);
                    TempData["Error"] = null;
                    TempData["Message"] = null;
                    return RedirectToAction("ZC01011", "ZC010");
                }
                else if (Request.Form["btnClear_Click"] != null)
                {
                    searchEnabled = 0;
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    return RedirectToAction("ZC05901", "ZC050", new { TableName = "M_LOCATION" });
                }


                else
                {
                    // Edit Individual Records
                    master.TYPE = "Update";
                    master.mLocation.LOC_ID = Convert.ToInt32(master.mLocation.UPDATE_LOC_ID);
                    master.mLocation.P_LOC_ID = Convert.ToInt32(master.mLocation.UPDATE_P_LOC_ID);
                    master.mLocation.CLASS_NAME = master.mLocation.UPDATE_CLASS_NAME;
                    master.mLocation.LOC_BARCODE = master.mLocation.UPDATE_LOC_BARCODE;
                    master.mLocation.SORT_LEVEL = master.mLocation.UPDATE_SORT_LEVEL;
                    if (master.mLocation.UPDATE_CLASS_NAME == "保管場所")
                    {
                        master.mLocation.CLASS_ID = 1;
                    }
                    else if (master.mLocation.UPDATE_CLASS_NAME == "使用場所")
                    {
                        master.mLocation.CLASS_ID = 2;
                    }
                    else
                    {
                        master.mLocation.CLASS_ID = 0;
                    }
                    master.mLocation.LOC_NAME = master.mLocation.UPDATE_LOC_NAME;
                    master.mLocation.P_LOC_NAME = master.mLocation.UPDATE_P_LOC_NAME;
                    if (master.mLocation.UPDATE_SEL_FLAGNAME == "選択不可")
                    {
                        master.mLocation.SELECTION_FLAG = "0";
                    }
                    else if (master.mLocation.UPDATE_SEL_FLAGNAME == "選択可")
                    {
                        master.mLocation.SELECTION_FLAG = "1";
                    }
                    else
                    {
                        master.mLocation.SELECTION_FLAG = "";
                    }

                    if (master.mLocation.UPDATE_DEL_FLAGNAME == "使用中")
                    {
                        master.mLocation.DEL_FLAG = 0;
                        master.mLocation.DEL_FLG = false;
                    }
                    else if (master.mLocation.UPDATE_DEL_FLAGNAME == "削除済")
                    {
                        master.mLocation.DEL_FLAG = 1;
                        master.mLocation.DEL_FLG = true;
                    }
                    else
                    {
                        master.mLocation.DEL_FLAG = 2;
                    }

                    if (master.mLocation.UPDATE_OVER_CHECK_FLAGNAME == "無")
                    {
                        master.mLocation.OVER_CHECK_FLAG = "0";
                    }
                    else if (master.mLocation.UPDATE_OVER_CHECK_FLAGNAME == "有")
                    {
                        master.mLocation.OVER_CHECK_FLAG = "1";
                    }

                    if (master.mLocation.UPDATE_REG_TEXT != null)
                    {
                        string[] harazardousElements = master.mLocation.UPDATE_REG_TEXT.Split(';');
                        foreach (string harazardousElement in harazardousElements)
                        {
                            if (harazardousElement == "第一類")
                            {
                                master.mLocation.HAZARD_CHECK1 = true;
                            }
                            else if (harazardousElement == "第二類")
                            {
                                master.mLocation.HAZARD_CHECK2 = true;
                            }
                            else if (harazardousElement == "第三類")
                            {
                                master.mLocation.HAZARD_CHECK3 = true;
                            }
                            else if (harazardousElement == "指定可燃物")
                            {
                                // combustible 
                                master.mLocation.HAZARD_CHECK7 = true;
                            }
                            else if (harazardousElement == "第四類")
                            {
                                master.mLocation.HAZARD_CHECK4 = true;
                            }
                            else if (harazardousElement == "第五類")
                            {
                                master.mLocation.HAZARD_CHECK5 = true;
                            }
                            else if (harazardousElement == "第六類")
                            {
                                master.mLocation.HAZARD_CHECK6 = true;
                            }
                        }
                    }
                    // split the user codes and user names of chemchiefs and keep in dict format to store them in session
                    if (master.mLocation.UPDATE_USER_CD != null)
                    {
                        string[] userCodes = master.mLocation.UPDATE_USER_CD.Split(';');
                        string[] userNames = master.mLocation.UPDATE_USER_NAME.Split(';');
                        Session[SessionConst.sessionChemChief] = Enumerable.Range(0, userCodes.Length).ToDictionary(i => userCodes[i], i => userNames[i]);
                    }
                    else
                    {
                        Session[SessionConst.sessionChemChief] = null;
                    }
                    Session[SessionConst.MasterModelZC05902] = master;
                    return RedirectToAction("ZC05902", "ZC050", new { Type = "Update", TableName = "M_LOCATION" });
                }
            }
            else if (master.masterMaintenanceScreen == "M_COMMON")
            {
                if (Request.Form["btnNewCommon_Click"] != null)
                {
                    return RedirectToAction("ZC05902", "ZC050", new { TYPE = "New", TableName = "M_COMMON" });
                }
                else if (Request.Form["btnSearch_Click"] != null)
                {
                    try
                    {
                        Session[SessionConst.MasterModelZC05901] = master;
                        master.listCommonDetails = repository.CommonMasterSearch(master, tableInformation, tableColumnInformations);
                        searchEnabled = 1;
                        master.TYPE = "Search";
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    master = DropDownFillRegulation(master);
                    return View(master);
                }
                else if (Request.Form["btnOutList_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];

                        List<M_COMMON> searchResult = (List<M_COMMON>)Session[SessionConst.WebGrid];

                        var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        ChemMgmt.Classes.Extensions.GridViewExtension.CommonOutputCSV(searchResult, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + CsvFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    master = DropDownFillRegulation(master);
                    return View(master);
                }
                else if (Request.Form["btnOutTemplate_Click"] != null)
                {
                    try
                    {
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        DataTable templateDataTable = repository.CreateListFormat(tableColumnInformations);

                        //    "*状態","部署ID","*部署コード","*部署名","上位部署名","登録者","登録日時","更新者","更新日時","削除者","削除日時","バッチ更新対象外"
                        string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        MasterMaintenance.Common.TemplateCSV(templateDataTable, csvFileFullPath);
                        string replaceCsvFileName = tableInformation.TABLE_DISPLAY_NAME + TemplateFileNameSuffix.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }
                    master = DropDownFillRegulation(master);
                    return View(master);
                }
                else if (Request.Form["btnClear_Click"] != null)
                {
                    searchEnabled = 0;
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    return RedirectToAction("ZC05901", "ZC050", new { TableName = "M_COMMON" });
                }
                else if (Request.Form["btnBack_Click"] != null)
                {
                    searchEnabled = 0;
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    Session.Remove(SessionConst.TableColumnInformations);
                    Session.Remove(SessionConst.TableInformation);
                    Session.Remove(SessionConst.MasterModelZC05901);
                    TempData["Error"] = null;
                    TempData["Message"] = null;
                    master = DropDownFillRegulation(master);
                    return RedirectToAction("ZC01011", "ZC010");
                }
                else if (Request.Form["btnInclude_Click"] != null)
                {
                    string serverFileName = "";
                    string fileStatus = "";
                    try
                    {
                        HttpPostedFileBase posted = Request.Files[0];// uploadするファイル
                        string serverPath = Server.MapPath("../upload");    // uploadするフォルダ
                        // ファイルをuploadして uploadしたファイルのパスを取得
                        serverFileName = Common.UploadFile(posted, serverPath);
                    }
                    catch (Exception ex)
                    {
                        _logoututil.OutErrLog(ProgramId, ex.ToString());
                    }

                    if (serverFileName != "")
                    {
                        int importedCount = 0;
                        string errMessage = "";
                        repository.DoImport(serverFileName, out importedCount, out errMessage);

                        if (importedCount > 0)
                        {
                            fileStatus += importedCount.ToString() + "件が正常に取り込まれました。<br/><br/>";
                        }

                        if (errMessage != "")
                        {
                            fileStatus += "取り込みエラーが発生しました。<br/><br/>[詳細]<br/>" + errMessage;
                        }
                    }
                    ViewBag.FileStatus = fileStatus;
                    master = DropDownFillRegulation(master);
                    return View(master);
                }
            }
            return View(master);
        }


        [Authorize(Roles = "0,1")]
        public ActionResult ZC05902(string delFlag, string Type, string TableName, string id, bool IsPostBack = false)
        {
            // read the model data through session
            M_MASTER master = (M_MASTER)Session[SessionConst.MasterModelZC05902];
            M_MASTER newMasterReg = new M_MASTER();
            if ((S_TAB_CONTROL)Session[SessionConst.TableInformation] == null)
            {
                // テーブル情報を取得します。
                var tableControls = Control.GetTableControl();
                tableInformation = tableControls.Single(x => x.TABLE_NAME == TableName);
                tableColumnInformations = Control.GetTableColumnControl(TableName);

                // セッション変数に格納します。
                Session[SessionConst.TableInformation] = tableInformation;
                Session[SessionConst.TableColumnInformations] = tableColumnInformations;
            }
            else
            {
                // セッション変数から取得します。
                tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
            }
            if (IsPostBack)
            {
                // If it is a postback from submit operation, return the same page with existing data
                return View(master);
            }

            if (TableName == "M_REGULATION" && Type == "Update")
            {

                List<M_REGULATION> listRegulationDetails;

                newMasterReg.id = Convert.ToInt32(id);
                if (delFlag == "削除済")
                {
                    listRegulationDetails = SearchCommonMasterNew(tableInformation, tableColumnInformations, DEL_FLAG.Deleted, Convert.ToInt32(id));
                }
                else
                {
                    listRegulationDetails = SearchCommonMasterNew(tableInformation, tableColumnInformations, DEL_FLAG.Undelete, Convert.ToInt32(id));
                }
                listRegulationDetails = ConvertRegulationMaster(tableColumnInformations, listRegulationDetails);
                newMasterReg.listCasRegModelDetails = GetMCasReg(Convert.ToInt32(id)).ToList();
                Session.Add(SessionConst.SessionCas, newMasterReg.listCasRegModelDetails.AsEnumerable());
                foreach (var items in listRegulationDetails)
                {
                    M_REGULATION mRegulation = new M_REGULATION();
                    mRegulation.REG_TYPE_ID = items.REG_TYPE_ID;
                    mRegulation.CLASS_ID = items.CLASS_ID;
                    mRegulation.CLASSIFICATION = items.CLASSIFICATION;
                    mRegulation.REG_TEXT = items.REG_TEXT.Trim();
                    mRegulation.REG_ACRONYM_TEXT = items.REG_ACRONYM_TEXT;
                    mRegulation.REG_ICON_PATH = items.REG_ICON_PATH;
                    mRegulation.REG_ICON_NAME = items.REG_ICON_NAME;
                    mRegulation.REG_TEXT_CONDITION = items.REG_TEXT_CONDITION;
                    mRegulation.P_REG_TYPE_ID = items.P_REG_TYPE_ID;
                    mRegulation.P_REG_TYPE_VALUE = items.P_REG_TYPE_VALUE;
                    mRegulation.MARKS = items.MARKS;
                    mRegulation.KEY_MGMT = items.KEY_MGMT;
                    mRegulation.KEY_MGMT_NAME = items.KEY_MGMT_NAME;
                    mRegulation.MEASUREMENT_FLAG = items.MEASUREMENT_FLAG;
                    mRegulation.MEASUREMENT_FLAG_VALUE = items.MEASUREMENT_FLAG_VALUE;
                    mRegulation.EXAMINATION_FLAG = items.EXAMINATION_FLAG;
                    mRegulation.EXAMINATION_FLAG_VALUE = items.EXAMINATION_FLAG_VALUE;
                    mRegulation.RA_FLAG = items.RA_FLAG;
                    mRegulation.RA_FLAG2 = items.RA_FLAG2;
                    mRegulation.REG_DATE = items.REG_DATE;
                    mRegulation.EXPOSURE_REPORT = items.EXPOSURE_REPORT;
                    mRegulation.EXPOSURE_REPORT_VALUE = items.EXPOSURE_REPORT_VALUE;
                    mRegulation.FIRE_SIZE = items.FIRE_SIZE;
                    mRegulation.UNITSIZE_ID = items.UNITSIZE_ID;
                    mRegulation.UNITSIZE_NAME = items.UNITSIZE_NAME;
                    mRegulation.UPD_DATE = items.UPD_DATE;
                    mRegulation.UPD_USER_CD = items.UPD_USER_CD;
                    mRegulation.UPD_USER_NAME = items.UPD_USER_NAME;
                    mRegulation.DEL_DATE = items.DEL_DATE;
                    mRegulation.DEL_USER_CD = items.DEL_USER_CD;
                    mRegulation.DEL_USER_NAME = items.DEL_USER_NAME;
                    mRegulation.DEL_USER_NM = items.DEL_USER_NM;
                    mRegulation.DEL_FLAG_VALUE = items.DEL_FLAG_VALUE;
                    mRegulation.SORT_ORDER = items.SORT_ORDER;
                    mRegulation.STATUS = items.STATUS;
                    mRegulation.SEL_FLAG = items.SEL_FLAG;
                    mRegulation.WEIGHT_MGMT = items.WEIGHT_MGMT;
                    mRegulation.WEIGHT_MGMT_NAME = items.WEIGHT_MGMT_NAME;
                    mRegulation.WORKTIME_MGMT = items.WORKTIME_MGMT;
                    mRegulation.WORKTIME_MGMT_NAME = items.WORKTIME_MGMT_NAME;
                    mRegulation.WORK_RECORD = items.WORK_RECORD;
                    mRegulation.CAS_NO = items.CAS_NO;
                    mRegulation.CLASS_NAME = items.CLASS_NAME;
                    mRegulation.DEL_FLG = items.DEL_FLG;
                    mRegulation.REG_TEXT_CONDITION = items.REG_TEXT_CONDITION;

                    newMasterReg.mRegulation = mRegulation;

                    newMasterReg.masterMaintenanceScreen = TableName;
                    newMasterReg.TYPE = "Update";

                    Session["REGULATION"] = newMasterReg;
                    Session[SessionConst.MasterData] = newMasterReg.mRegulation;
                }
                newMasterReg = DropDownFillRegulation(newMasterReg);
                return View(newMasterReg);
            }
            else if (TableName == "M_MAKER" && Type == "Update")
            {
                master = new M_MASTER();
                master.masterMaintenanceScreen = TableName;
                DynamicParameters dynp = new DynamicParameters();
                string sql = "SELECT * FROM [M_MAKER] WHERE MAKER_ID=@MAKER_ID";
                dynp.Add("@MAKER_ID", id);
                List<M_MAKER> m_maker = DbUtil.Select<M_MAKER>(sql, dynp);
                master.mMaker = new M_MAKER();

                if (m_maker.Count > 0)
                {
                    master.mMaker.MAKER_ID = m_maker[0].MAKER_ID;
                    master.mMaker.MAKER_NAME = m_maker[0].MAKER_NAME;
                    master.mMaker.MAKER_URL = m_maker[0].MAKER_URL;
                    master.mMaker.MAKER_TEL = m_maker[0].MAKER_TEL;
                    master.mMaker.MAKER_EMAIL1 = m_maker[0].MAKER_EMAIL1;
                    master.mMaker.MAKER_EMAIL2 = m_maker[0].MAKER_EMAIL2;
                    master.mMaker.MAKER_EMAIL3 = m_maker[0].MAKER_EMAIL3;
                    master.mMaker.MAKER_ADDRESS = m_maker[0].MAKER_ADDRESS;
                    master.mMaker.MAKER_DEL_FLAG = m_maker[0].DEL_FLAG == 0 ? false : true;
                }
                return View(master);
            }
            else if (TableName == "M_UNITSIZE" && Type == "Update")
            {
                master = new M_MASTER();
                master.masterMaintenanceScreen = TableName;
                DynamicParameters dynp = new DynamicParameters();
                string sql = "SELECT * FROM [M_UNITSIZE] WHERE UNITSIZE_ID=@UNITSIZE_ID";
                dynp.Add("@UNITSIZE_ID", id);
                List<M_UNITSIZE> m_unitsize = DbUtil.Select<M_UNITSIZE>(sql, dynp);
                master.mUnitSize = new M_UNITSIZE();

                if (m_unitsize.Count > 0)
                {
                    master.mUnitSize.UNITSIZE_ID = m_unitsize[0].UNITSIZE_ID;
                    master.mUnitSize.UNIT_NAME = m_unitsize[0].UNIT_NAME;
                    master.mUnitSize.FACTOR = m_unitsize[0].FACTOR;
                    master.mUnitSize.DEL_FLAG = (m_unitsize[0].DEL_FLAG).ToString() == "false" ? 0 : 1;
                }
                return View(master);
            }
            else if (TableName == "M_COMMON" && Type == "Update")
            {
                List<M_COMMON> listCommonDetails;

                newMasterReg.id = Convert.ToInt32(id);
                if (delFlag == "0")
                {
                    listCommonDetails = SearchCommonMasterRecordUpdate(tableInformation, tableColumnInformations, DEL_FLAG.Undelete, Convert.ToInt32(id));
                }
                else
                {
                    listCommonDetails = SearchCommonMasterRecordUpdate(tableInformation, tableColumnInformations, DEL_FLAG.Deleted, Convert.ToInt32(id));
                }
                listCommonDetails = ConvertCommonMaster(tableColumnInformations, listCommonDetails);


                foreach (var item in listCommonDetails)
                {
                    M_COMMON mCommon = new M_COMMON();
                    mCommon.COMMON_ID = item.COMMON_ID;
                    mCommon.TABLE_NAME = item.TABLE_NAME;
               //     mCommon.TABLENAME = item.TABLENAME;
                    mCommon.TABLE_NAME_ID = item.TABLE_NAME_ID;
                    mCommon.KEY_VALUE = item.KEY_VALUE;

                    mCommon.KEY_VALUE_ID = item.KEY_VALUE;
                    mCommon.DISPLAY_ORDER = item.DISPLAY_ORDER;
                    mCommon.DISPLAY_ORDER_ID = item.DISPLAY_ORDER;
                    mCommon.DISPLAY_VALUE = item.DISPLAY_VALUE;
                    mCommon.REG_USER_CD = item.REG_USER_CD;
                    mCommon.REG_USER_NAME = item.REG_USER_NAME;
                    mCommon.REG_DATE = item.REG_DATE;
                    mCommon.UPD_DATE = item.UPD_DATE;
                    mCommon.UPD_USER_CD = item.UPD_USER_CD;
                    mCommon.UPD_USER_NAME = item.UPD_USER_NAME;

                    newMasterReg.mCommon = mCommon;
                    newMasterReg.masterMaintenanceScreen = "M_COMMON";
                    newMasterReg.TYPE = "Update";
                    Session["M_COMMON"] = newMasterReg;
                    Session[SessionConst.MasterData] = newMasterReg.mCommon;
                }


                newMasterReg = DropDownFillRegulation(newMasterReg);
                return View(newMasterReg);
            }
            else
            {
                if (Type == "Update")
                {
                    master.TYPE = "Update";
                    return View(master);
                }
                else if (Type == "New")
                {
                    // return fresh model for Registration screen
                    M_MASTER newMaster = new M_MASTER();
                    newMaster.TYPE = "New";
                    if (Request.QueryString.Get(QueryString.TableNameKey) == "M_GROUP")
                    {
                        newMaster.masterMaintenanceScreen = "M_GROUP";
                    }
                    else if (Request.QueryString.Get(QueryString.TableNameKey) == "M_LOCATION")
                    {
                        newMaster.masterMaintenanceScreen = "M_LOCATION";
                    }
                    else if (Request.QueryString.Get(QueryString.TableNameKey) == "M_ACTION_TYPE")
                    {
                        newMaster.masterMaintenanceScreen = "M_ACTION_TYPE";
                    }
                    else if (Request.QueryString.Get(QueryString.TableNameKey) == "M_STATUS")
                    {
                        newMaster.masterMaintenanceScreen = "M_STATUS";
                        var tableColumnInformations = Control.GetTableColumnControl(newMaster.masterMaintenanceScreen);
                        DataTable dt = new DataTable();
                        dt.Columns.Add("Text");
                        dt.Columns.Add("Val");
                        foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                        {
                            if (tableColumnInformation.IS_SEARCH)
                            {
                                foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                                {
                                    dt.Rows.Add(convertData.Value, convertData.Key);
                                    newMaster.LookUpStatus = BindLookupValuesStatus(dt);
                                }
                            }
                        }
                    }
                    else if (Request.QueryString.Get(QueryString.TableNameKey) == "M_WORKFLOW")
                    {
                        newMaster.masterMaintenanceScreen = "M_WORKFLOW";
                    }
                    else if (Request.QueryString.Get(QueryString.TableNameKey) == "M_REGULATION")
                    {
                        newMaster.TYPE = "New";
                        Session[SessionConst.SessionCas] = null;
                        newMaster = DropDownFillRegulation(newMaster);
                        newMaster.masterMaintenanceScreen = "M_REGULATION";
                    }
                    else if (Request.QueryString.Get(QueryString.TableNameKey) == "M_COMMON")
                    {
                        newMaster = DropDownFillRegulation(newMaster);
                        newMaster.masterMaintenanceScreen = "M_COMMON";
                    }
                    return View(newMaster);
                }
                else
                {
                    // For Delete operations
                    return View(master);
                }
            }
        }


        [HttpPost]
        public void SessionRemoveZC05902()
        {
            Session.Remove(SessionConst.MasterModelZC05902);
            Session.Remove(SessionConst.MasterData);
            Session.Remove(SessionConst.ChemChief);
            Session.Remove(SessionConst.sessionChemChief);
        }

        [HttpPost]
        public void SessionRemoveZC05901()
        {
            Session.Remove(SessionConst.MasterModelZC05901);
            Session.Remove(SessionConst.MasterData);
        }

        [HttpPost]
        public void SessionRemoveZC05903()
        {
            Session.Remove(SessionConst.MasterData);
            Session.Remove(SessionConst.sessionChemChief);
        }

        [HttpPost]
        public ActionResult ZC05902(M_MASTER master)
        {
            M_MASTER m_Master = new M_MASTER();
            try
            {

                if (master.masterMaintenanceScreen == "M_GROUP")
                {
                    if (Request.Form["btnBack_Click"] != null)
                    {
                        // remove the search maintainence session data and redirect to search screen
                        Session.Remove(SessionConst.MasterModelZC05902);
                        Session.Remove(SessionConst.MasterData);
                        Session.Remove(SessionConst.sessionChemChief);
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = "M_GROUP" });
                    }
                    if (Request.Form["btnRegistration_Click"] != null)
                    {
                        master.TYPE = "New";
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();
                        masterData.Add("GROUP_CD", master.mGroup.GROUP_CD);
                        masterData.Add("GROUP_NAME", master.mGroup.GROUP_NAME);

                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                        masterData["P_GROUP_CD"] = master.mGroup.P_GROUP_CD;

                        IEnumerable<string> errorMessages = ValidateInputValue(tableColumnInformations, masterData, null);

                        List<string> errorlist = new List<string>();
                        errorlist.AddRange(errorMessages);
                        if (master.mGroup.DEL_FLG == true)
                        {
                            List<M_GROUP> records = repository.IsHighLevelDepartment(masterData["GROUP_CD"]);

                            // 親として1件でも設定されていればエラーとする。
                            if (records.Count > 0)
                            {
                                //Cannot be deleted because it is set as a high-level department name.
                                errorlist.Add("上位部署名として設定されているため、削除できません。");
                                // errorlist.Add(string.Format(WarningMessages.ParentAsConfigured, "上位部署名", "削除"));
                            }
                        }

                        if (errorlist.Count() > 0)
                        {
                            TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                            return View(master);
                        }

                        masterData.Add("DEL_USER_CD", master.mGroup.DEL_FLG == false ? null : Common.GetLogonForm().Id);
                        masterData.Add("DEL_DATE", master.mGroup.DEL_FLG == false ? null : DateTime.Now.ToString());
                        masterData.Add("DEL_FLAG", master.mGroup.DEL_FLG == false ? "0" : "1");
                        masterData.Add("BATCH_FLAG", master.mGroup.BATCH_FLG == false ? "0" : "1");

                        master.masterMaintenanceScreen = "M_GROUP";


                        Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.MasterModelZC05902] = master;
                        return RedirectToAction("ZC05903", "ZC050", new { TableName = "M_GROUP" });
                    }
                    else if (Request.Form["btnUpdate_Click"] != null)
                    {
                        master.TYPE = "Update";
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();
                        masterData.Add("GROUP_CD", master.mGroup.GROUP_CD);
                        masterData.Add("GROUP_NAME", master.mGroup.GROUP_NAME);
                        List<string> errorlist = new List<string>();

                        // キー値と入力値を結合します。
                        Dictionary<string, string> datas = new Dictionary<string, string>();
                        datas.Add("GROUP_ID", master.mGroup.GROUP_ID.ToString());
                        foreach (KeyValuePair<string, string> data in masterData)
                        {
                            if (datas.ContainsKey(data.Key)) datas.Remove(data.Key);
                            datas.Add(data.Key, data.Value);
                        }

                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];

                        master.masterMaintenanceScreen = "M_GROUP";
                        masterData["P_GROUP_CD"] = master.mGroup.P_GROUP_CD;

                        IEnumerable<string> errorMessages = ValidateInputValue(tableColumnInformations, datas, masterData);
                        errorlist.AddRange(errorMessages);

                        if (master.mGroup.DEL_FLG == true)
                        {
                            List<M_GROUP> records = repository.IsHighLevelDepartment(masterData["GROUP_CD"]);
                            // 親として1件でも設定されていればエラーとする。
                            if (records.Count > 0)
                            {
                                //Cannot be deleted because it is set as a high-level department name.
                                errorlist.Add("上位部署名として設定されているため、削除できません。");
                                // errorlist.Add(string.Format(WarningMessages.ParentAsConfigured, "上位部署名", "削除"));
                            }
                        }

                        if (errorlist.Count() > 0)
                        {
                            TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                            return View(master);
                        }

                        masterData.Add("DEL_USER_CD", master.mGroup.DEL_FLG == false ? null : Common.GetLogonForm().Id); // userInfo.USER_CD
                        masterData.Add("DEL_DATE", master.mGroup.DEL_FLG == false ? null : DateTime.Now.ToString());
                        masterData.Add("DEL_FLAG", master.mGroup.DEL_FLG == false ? "0" : "1");
                        masterData.Add("BATCH_FLAG", master.mGroup.BATCH_FLG == false ? "0" : "1");
                        master.masterMaintenanceScreen = "M_GROUP";

                        // store model data in session to access in confirmation screen
                        Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.MasterModelZC05902] = master;
                        // redirect to confirmation screen
                        return RedirectToAction("ZC05903", "ZC050", new { TableName = "M_GROUP" });
                    }
                }
                else if (master.masterMaintenanceScreen == "M_LOCATION")
                {
                    if (Request.Form["btnBack_Click"] != null)
                    {
                        // remove the maintainence screen session and load the search screen
                        Session.Remove(SessionConst.MasterModelZC05902);
                        Session.Remove(SessionConst.MasterData);
                        Session.Remove(SessionConst.sessionChemChief);
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = "M_LOCATION" });
                    }
                    if (Request.Form["btnRegistration_Click"] != null)
                    {
                        master.TYPE = "New";
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();
                        masterData.Add("CLASS_ID", master.mLocation.CLASS_ID.ToString());
                        masterData.Add("LOC_BARCODE", master.mLocation.LOC_BARCODE);
                        masterData.Add("LOC_NAME", master.mLocation.LOC_NAME);
                        masterData.Add("SORT_LEVEL", master.mLocation.SORT_LEVEL.ToString());
                        masterData.Add("P_LOC_BASEID", master.mLocation.P_LOC_BASEID);
                        masterData.Add("P_LOC_ID", master.mLocation.P_LOC_ID.ToString());
                        masterData.Add("SEL_FLAG", master.mLocation.SELECTION_FLAG);
                        masterData.Add("OVER_CHECK_FLAG", master.mLocation.OVER_CHECK_FLAG);
                        masterData.Add("USER_CD", master.mLocation.USER_CD);
                        masterData.Add("USER_NAME", master.mLocation.USER_NAME);

                        if (master.mLocation.SELECTION_FLAG == "0")
                        {
                            master.mLocation.SEL_FLAGNAME = "選択不可";
                        }
                        else if (master.mLocation.SELECTION_FLAG == "1")
                        {
                            master.mLocation.SEL_FLAGNAME = "選択可";
                        }

                        if (master.mLocation.OVER_CHECK_FLAG == "0")
                        {
                            master.mLocation.OVER_CHECK_FLAGNAME = "無";
                        }
                        else if (master.mLocation.OVER_CHECK_FLAG == "1")
                        {
                            master.mLocation.OVER_CHECK_FLAGNAME = "有";
                        }
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];

                        IEnumerable<string> errorMessages = ValidateInputValue(tableColumnInformations, masterData, null);

                        List<string> errorlist = new List<string>();

                        errorlist.AddRange(errorMessages);

                        if (errorlist.Count() > 0)
                        {
                            TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                            return View(master);
                        }
                        masterData.Add("HAZARD_CHECK1", master.mLocation.HAZARD_CHECK1 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK2", master.mLocation.HAZARD_CHECK2 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK3", master.mLocation.HAZARD_CHECK3 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK4", master.mLocation.HAZARD_CHECK4 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK5", master.mLocation.HAZARD_CHECK5 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK6", master.mLocation.HAZARD_CHECK6 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK7", master.mLocation.HAZARD_CHECK7 == false ? "0" : "1");

                        masterData.Add("DEL_USER_CD", master.mLocation.DEL_FLG == false ? null : Common.GetLogonForm().Id);
                        masterData.Add("DEL_DATE", master.mLocation.DEL_FLG == false ? null : DateTime.Now.ToString());
                        masterData.Add("DEL_FLAG", master.mLocation.DEL_FLG == false ? "0" : "1");

                        Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.ChemChief] = "";
                        Session[SessionConst.MasterModelZC05902] = master;

                        return RedirectToAction("ZC05903", "ZC050", new { TableName = "M_LOCATION" });
                    }
                    else if (Request.Form["btnUpdate_Click"] != null)
                    {
                        master.TYPE = "Update";
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();
                        masterData.Add("CLASS_ID", master.mLocation.CLASS_ID.ToString());
                        if (master.mLocation.LOC_BARCODE != null)
                        {
                            master.mLocation.LOC_BARCODE = master.mLocation.LOC_BARCODE.Trim();
                        }
                        masterData.Add("LOC_BARCODE", master.mLocation.LOC_BARCODE);
                        masterData.Add("LOC_NAME", master.mLocation.LOC_NAME);
                        masterData.Add("SORT_LEVEL", master.mLocation.SORT_LEVEL.ToString());
                        masterData.Add("P_LOC_BASEID", master.mLocation.P_LOC_BASEID);
                        masterData.Add("P_LOC_ID", master.mLocation.P_LOC_ID.ToString());
                        masterData.Add("SEL_FLAG", master.mLocation.SELECTION_FLAG);
                        masterData.Add("OVER_CHECK_FLAG", master.mLocation.OVER_CHECK_FLAG);
                        masterData.Add("USER_CD", master.mLocation.USER_CD);
                        masterData.Add("USER_NAME", master.mLocation.USER_NAME);

                        // キー値と入力値を結合します。
                        Dictionary<string, string> datas = new Dictionary<string, string>();
                        datas.Add("LOC_ID", master.mLocation.LOC_ID.ToString());
                        foreach (KeyValuePair<string, string> data in masterData)
                        {
                            if (datas.ContainsKey(data.Key)) datas.Remove(data.Key);
                            datas.Add(data.Key, data.Value);
                        }

                        if (master.mLocation.SELECTION_FLAG == "0")
                        {
                            master.mLocation.SEL_FLAGNAME = "選択不可";
                        }
                        else if (master.mLocation.SELECTION_FLAG == "1")
                        {
                            master.mLocation.SEL_FLAGNAME = "選択可";
                        }

                        if (master.mLocation.OVER_CHECK_FLAG == "0")
                        {
                            master.mLocation.OVER_CHECK_FLAGNAME = "無";
                        }
                        else if (master.mLocation.OVER_CHECK_FLAG == "1")
                        {
                            master.mLocation.OVER_CHECK_FLAGNAME = "有";
                        }

                        masterData.Add("HAZARD_CHECK1", master.mLocation.HAZARD_CHECK1 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK2", master.mLocation.HAZARD_CHECK2 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK3", master.mLocation.HAZARD_CHECK3 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK4", master.mLocation.HAZARD_CHECK4 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK5", master.mLocation.HAZARD_CHECK5 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK6", master.mLocation.HAZARD_CHECK6 == false ? "0" : "1");
                        masterData.Add("HAZARD_CHECK7", master.mLocation.HAZARD_CHECK7 == false ? "0" : "1");

                        List<string> errorlist = new List<string>();

                        Dictionary<string, string> masterKeys = new Dictionary<string, string>();
                        masterKeys.Add("LOC_ID", master.mLocation.LOC_ID.ToString());

                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];

                        IEnumerable<string> errorMessages = ValidateInputValue(tableColumnInformations, datas, masterData);
                        errorlist.AddRange(errorMessages);

                        if (master.mLocation.DEL_FLG)
                        {
                            List<M_LOCATION> records = repository.IsUpperStorageLocation(masterKeys["LOC_ID"]);
                            // 上位保管場所として設定されていればエラーとする。
                            if (records.Count > 0)
                            {
                                // errorlist.Add(string.Format(WarningMessages.ParentAsConfigured, "上位保管場所名", "削除"));
                                errorlist.Add("上位保管場所名として設定されているため、削除できません。");
                            }

                            if (errorlist.Count() > 0)
                            {
                                TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                                return View(master);
                            }
                        }
                        masterData.Add("DEL_USER_CD", master.mLocation.DEL_FLG == false ? null : Common.GetLogonForm().Id);
                        string currentdatetime = DateTime.Now.Year + "." + DateTime.Now.Month + "." + DateTime.Now.Day +
                            " " + DateTime.Now.Hour + (":") + DateTime.Now.Minute + (":") + DateTime.Now.Second;
                        masterData.Add("DEL_DATE", master.mLocation.DEL_FLG == false ? null : currentdatetime);//DateTime.Now.ToString()
                        masterData.Add("DEL_FLAG", master.mLocation.DEL_FLG == false ? "0" : "1");
                        master.masterMaintenanceScreen = "M_LOCATION";

                        // store model data in session to access in confirmation screen
                        Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.MasterModelZC05902] = master;
                        // redirect to confirmation screen
                        return RedirectToAction("ZC05903", "ZC050", new { TableName = "M_LOCATION" });
                    }
                }
                else if (master.masterMaintenanceScreen == "M_REGULATION")
                {
                    if (Request.Form["btnBack_Click"] != null)
                    {
                        Session.Remove(SessionConst.MasterModelZC05902);
                        Session.Remove(SessionConst.MasterData);
                        master = DropDownFillRegulation(master);
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = "M_REGULATION" });
                    }
                    if (Request.Form["btnRegistration_Click"] != null)
                    {
                        if (Session[SessionConst.SessionCas] != null)
                        {
                            master.listCasRegModelDetails = (List<CasRegModel>)Session[SessionConst.SessionCas];
                        }
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();// GetInputData(pnlIO, tableColumnInformations);

                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];

                        List<string> errorlist = new List<string>();
                        if (tableInformation.TABLE_NAME == "M_REGULATION")
                        {
                            if (master.mRegulation.MARKS != null)
                            {
                                //点数の入力値チェック 3桁の数字　2019/04/13
                                var tb = master.mRegulation.MARKS;
                                var r = new Regex(@"^[0-9]+$");
                                var match = r.Match(tb);
                                if (match.Value == tb)
                                {
                                    if (match.Length > 3)
                                    {
                                        errorlist.Add("点数は半角数字3桁です。");
                                    }
                                }
                                else
                                {
                                    errorlist.Add("点数は半角数字3桁です。");
                                }
                            }
                        }

                        List<string> ChiefName = new List<string>();
                        int y = 1;

                        if (tableInformation.TABLE_NAME == "M_REGULATION")
                        {
                            masterData.Add("REG_TYPE_ID", master.mRegulation.REG_TYPE_ID.ToString());
                            masterData.Add("CLASS_ID", master.mRegulation.CLASS_ID.ToString());
                            masterData.Add("CLASSIFICATION", master.mRegulation.CLASSIFICATION);
                            masterData.Add("REG_TEXT", master.mRegulation.REG_TEXT);
                            masterData.Add("REG_ACRONYM_TEXT", master.mRegulation.REG_ACRONYM_TEXT);
                            masterData.Add("REG_ICON_PATH", master.mRegulation.REG_ICON_PATH);
                            masterData.Add("REG_ICON_NAME", master.mRegulation.REG_ICON_NAME);
                            masterData.Add("REG_TEXT_CONDITION", master.mRegulation.REG_TEXT_CONDITION);
                            masterData.Add("P_REG_TYPE_ID", master.mRegulation.P_REG_TYPE_ID.ToString());
                            masterData.Add("P_REG_TYPE_VALUE", master.mRegulation.P_REG_TYPE_VALUE);
                            masterData.Add("KEY_MGMT", master.mRegulation.KEY_MGMT);
                            masterData.Add("KEY_MGMT_NAME", master.mRegulation.KEY_MGMT_NAME);
                            masterData.Add("MEASUREMENT_FLAG_VALUE", master.mRegulation.MEASUREMENT_FLAG_VALUE.ToString());
                            masterData.Add("EXAMINATION_FLAG_VALUE", master.mRegulation.EXAMINATION_FLAG_VALUE.ToString());
                            masterData.Add("RA_FLAG2", master.mRegulation.RA_FLAG2.ToString());
                            masterData.Add("REG_DATE", DateTime.Now.ToString());
                            masterData.Add("EXPOSURE_REPORT", master.mRegulation.EXPOSURE_REPORT.ToString());
                            masterData.Add("EXPOSURE_REPORT_VALUE", master.mRegulation.EXPOSURE_REPORT_VALUE);
                            masterData.Add("FIRE_SIZE", master.mRegulation.FIRE_SIZE);
                            masterData.Add("UNITSIZE_ID", master.mRegulation.UNITSIZE_ID);
                            masterData.Add("UNITSIZE_NAME", master.mRegulation.UNITSIZE_NAME);
                            masterData.Add("UPD_DATE", DateTime.Now.ToString());
                            masterData.Add("UPD_USER_CD", master.mRegulation.UPD_USER_CD);
                            masterData.Add("UPD_USER_NAME", master.mRegulation.UPD_USER_NAME);
                            masterData.Add("DEL_USER_NAME", master.mRegulation.DEL_USER_NAME);
                            masterData.Add("DEL_USER_NM", master.mRegulation.DEL_USER_NM);
                            masterData.Add("DEL_FLAG_VALUE", master.mRegulation.DEL_FLG.ToString());
                            masterData.Add("SORT_ORDER", master.mRegulation.SORT_ORDER.ToString());
                            masterData.Add("STATUS", master.mRegulation.STATUS);
                            masterData.Add("SEL_FLAG", master.mRegulation.SEL_FLAG.ToString());
                            masterData.Add("WEIGHT_MGMT", master.mRegulation.WEIGHT_MGMT);
                            masterData.Add("WEIGHT_MGMT_NAME", master.mRegulation.WEIGHT_MGMT_NAME);
                            masterData.Add("WORKTIME_MGMT", master.mRegulation.WORKTIME_MGMT.ToString());
                            masterData.Add("WORKTIME_MGMT_NAME", master.mRegulation.WORKTIME_MGMT_NAME);
                            masterData.Add("WORK_RECORD", master.mRegulation.WORK_RECORD.ToString());
                            masterData.Add("CAS_NO", master.mRegulation.CAS_NO);
                            masterData.Add("CLASS_NAME", master.mRegulation.CLASS_NAME);
                            masterData.Add("REG_TEXT_CONDITION ", master.mRegulation.REG_TEXT_CONDITION);


                            masterData.Add("DEL_USER_CD", master.mRegulation.DEL_FLG == false ? null : Classes.util.Common.GetLogonForm().Id); // userInfo.USER_CD
                            masterData.Add("DEL_DATE", master.mRegulation.DEL_FLG == false ? null : DateTime.Now.ToString());
                            masterData.Add("DEL_FLAG", master.mRegulation.DEL_FLG == false ? "0" : "1");

                            masterData.Add("RA_FLAG", master.mRegulation.RA_FLAG2 == false ? "0" : "1");
                            masterData.Add("MARKS", master.mRegulation.MARKS);

                        }
                        foreach (var data in masterData)
                        {
                            if (data.Key == "P_REG_TYPE_ID")
                            {
                                var value = data.Value != null ? data.Value : string.Empty;

                                var tableColumnInfo = tableColumnInformations.Where(x => x.COLUMN_NAME == data.Key).Single();
                                string keyColumnName;
                                string valueColumnName;
                                string iconPathColumnName;
                                string sortOrderColumnName;
                                string classColumnName;
                                string selFlgName;//2018/08/19 TPE.Sugimoto Add
                                string classId;

                                if (repository.IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                                                                                                              out sortOrderColumnName, out classColumnName, out selFlgName))
                                //2018/08/19 TPE Add End
                                //if (IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                                //                                                                                  out sortOrderColumnName, out classColumnName, out selFlgName,out classId))    //2019/02/09 TPE.Rin Add
                                {
                                    var table = repository.GetParentChildTable(tableColumnInfo.TABLE_NAME, keyColumnName, classColumnName, tableColumnInfo.COLUMN_NAME);

                                    // 上位カテゴリーに自身が存在しないか確認します。
                                    var keyValue = string.Empty;
                                    if (masterData.ContainsKey(keyColumnName)) keyValue = masterData[keyColumnName];
                                    if (!string.IsNullOrEmpty(keyValue) && !string.IsNullOrEmpty(value))
                                    {
                                        if (repository.IsSelfUsed(table, value, keyValue))
                                        {

                                            errorlist.Add(string.Format(WarningMessages.NotHalfwidth, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                            //errorMessages += string.Format(WarningMessages.ParentOwnExists, tableColumnInfo.COLUMN_DISPLAY_NAME);
                                        }
                                    }

                                    // 最上位カテゴリーとの分類に矛盾がないか確認します。
                                    if (!string.IsNullOrEmpty(classColumnName) && !string.IsNullOrEmpty(value))
                                    {
                                        var parent = repository.GetParentData(table, value);
                                        //var classId = datas.GetValue(classColumnName);
                                        var classid = masterData.GetValue(classColumnName);  //2019/02/09 TPE.Rin Mod
                                                                                             //if (parent != null && parent.Class != classId)
                                        if (parent != null && parent.Class != classid)
                                        {
                                            var classDisplayName = tableColumnInformations.Single(x => x.COLUMN_NAME == classColumnName).COLUMN_DISPLAY_NAME;
                                            errorlist.Add(string.Format(WarningMessages.Contradiction, classDisplayName, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                            //return string.Format(WarningMessages.Contradiction, classDisplayName, tableColumnInfo.COLUMN_DISPLAY_NAME);
                                        }
                                    }
                                }
                            }
                        }
                        if (errorlist.Count() > 0)
                        {
                            TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                            master = DropDownFillRegulation(master);
                            return View(master);
                        }

                        master = DropDownFillRegulation(master);
                        Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.MasterModelZC05902] = master;
                        return RedirectToAction("ZC05903", "ZC050", new { TableName = "M_REGULATION" });
                    }
                    else if (Request.Form["btnUpdate_Click"] != null)
                    {

                        string useCode = Classes.util.Common.GetLogonForm().Id;
                        List<CasRegModel> cas = new List<CasRegModel>();
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();// GetInputData(pnlIO, tableColumnInformations);


                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];

                        List<string> errorlist = new List<string>();
                        if (tableInformation.TABLE_NAME == "M_REGULATION")
                        {
                            if (master.mRegulation.MARKS != null)
                            {
                                //点数の入力値チェック 3桁の数字　2019/04/13
                                var tb = master.mRegulation.MARKS;
                                var r = new Regex(@"^[0-9]+$");
                                var match = r.Match(tb);
                                if (match.Value == tb)
                                {
                                    if (match.Length > 3)
                                    {
                                        errorlist.Add("点数は半角数字3桁です。");
                                    }
                                }
                                else
                                {
                                    errorlist.Add("点数は半角数字3桁です。");
                                }
                            }
                        }
                        var errorMessages = ValidateInputValue(tableColumnInformations, masterData, null);
                        List<string> ChiefName = new List<string>();
                        int y = 1;

                        errorlist.AddRange(errorMessages);
                        if (Session[SessionConst.SessionCas] != null)
                        {
                            cas = (List<CasRegModel>)Session[SessionConst.SessionCas];
                        }
                        M_REGULATION mRegulation = new M_REGULATION();
                        m_Master.listCasRegModelDetails = cas;
                        //m_Master.mRegulation.REG_TYPE_ID = master.mRegulation.REG_TYPE_ID;
                        mRegulation.CLASS_ID = master.mRegulation.CLASS_ID;
                        mRegulation.CLASSIFICATION = master.mRegulation.CLASSIFICATION;
                        mRegulation.REG_TEXT = master.mRegulation.REG_TEXT;
                        mRegulation.REG_ACRONYM_TEXT = master.mRegulation.REG_ACRONYM_TEXT;
                        mRegulation.REG_ICON_PATH = master.mRegulation.REG_ICON_PATH;
                        mRegulation.REG_TYPE_ID = master.mRegulation.REG_TYPE_ID;
                        mRegulation.P_REG_TYPE_ID = master.mRegulation.P_REG_TYPE_ID;
                        mRegulation.KEY_MGMT = master.mRegulation.KEY_MGMT;
                        mRegulation.MEASUREMENT_FLAG_VALUE = master.mRegulation.MEASUREMENT_FLAG_VALUE;
                        mRegulation.EXAMINATION_FLAG_VALUE = master.mRegulation.EXAMINATION_FLAG_VALUE;
                        mRegulation.RA_FLAG2 = master.mRegulation.RA_FLAG2;
                        mRegulation.RA_FLAG = master.mRegulation.RA_FLAG;
                        mRegulation.SEL_FLAG = master.mRegulation.SEL_FLAG;
                        mRegulation.DEL_FLG = master.mRegulation.DEL_FLG;
                        mRegulation.DEL_FLAG = master.mRegulation.DEL_FLAG;
                        mRegulation.EXPOSURE_REPORT = master.mRegulation.EXPOSURE_REPORT;
                        mRegulation.FIRE_SIZE = master.mRegulation.FIRE_SIZE;
                        mRegulation.UNITSIZE_ID = master.mRegulation.UNITSIZE_ID;
                        mRegulation.SORT_ORDER = master.mRegulation.SORT_ORDER;
                        mRegulation.WEIGHT_MGMT = master.mRegulation.WEIGHT_MGMT;
                        mRegulation.WORKTIME_MGMT = master.mRegulation.WORKTIME_MGMT;


                        if (master.mRegulation.DEL_FLG == true)
                        {
                            mRegulation.UPD_USER_CD = null;
                            mRegulation.UPD_DATE = null;
                        }
                        else
                        {
                            mRegulation.UPD_USER_CD = useCode;
                            mRegulation.UPD_DATE = DateTime.Now;
                        }
                        mRegulation.MARKS = master.mRegulation.MARKS;
                        if (master.mRegulation.RA_FLAG2 == false)
                        {
                            mRegulation.RA_FLAG = "0";
                        }
                        else
                        {
                            mRegulation.RA_FLAG = "1";
                        }
                        if (master.mRegulation.EXAMINATION_FLAG_VALUE == false)
                        {
                            mRegulation.EXAMINATION_FLAG = "0";
                        }
                        else
                        {
                            mRegulation.EXAMINATION_FLAG = "1";
                        }

                        if (master.mRegulation.MEASUREMENT_FLAG_VALUE == false)
                        {
                            mRegulation.MEASUREMENT_FLAG = "0";
                        }
                        else
                        {
                            mRegulation.MEASUREMENT_FLAG = "1";
                        }

                        mRegulation.DEL_FLAG = master.mRegulation.DEL_FLAG;

                        if (master.mRegulation.DEL_FLG == false)
                        {
                            mRegulation.DEL_USER_CD = null;
                            mRegulation.DEL_DATE = null;
                            mRegulation.DEL_FLAG = 0;
                        }
                        else
                        {
                            mRegulation.DEL_USER_CD = useCode;
                            mRegulation.DEL_DATE = DateTime.Now;
                            mRegulation.DEL_FLAG = 1;

                        }
                        Session.Add(SessionConst.MasterData, master.mRegulation);



                        if (tableInformation.TABLE_NAME == "M_REGULATION")
                        {
                            masterData.Add("P_REG_TYPE_ID", master.mRegulation.P_REG_TYPE_ID.ToString());

                        }
                        foreach (var data in masterData)
                        {
                            if (data.Key == "P_REG_TYPE_ID")
                            {
                                var value = data.Value != null ? data.Value : string.Empty;

                                var tableColumnInfo = tableColumnInformations.Where(x => x.COLUMN_NAME == data.Key).Single();
                                string keyColumnName;
                                string valueColumnName;
                                string iconPathColumnName;
                                string sortOrderColumnName;
                                string classColumnName;
                                string selFlgName;//2018/08/19 TPE.Sugimoto Add
                                string classId;

                                if (repository.IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                                                                                                              out sortOrderColumnName, out classColumnName, out selFlgName))
                                //2018/08/19 TPE Add End
                                //if (IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                                //                                                                                  out sortOrderColumnName, out classColumnName, out selFlgName,out classId))    //2019/02/09 TPE.Rin Add
                                {
                                    var table = repository.GetParentChildTable(tableColumnInfo.TABLE_NAME, keyColumnName, classColumnName, tableColumnInfo.COLUMN_NAME);

                                    // 上位カテゴリーに自身が存在しないか確認します。
                                    var keyValue = string.Empty;
                                    if (masterData.ContainsKey(keyColumnName)) keyValue = masterData[keyColumnName];
                                    if (!string.IsNullOrEmpty(keyValue) && !string.IsNullOrEmpty(value))
                                    {
                                        if (repository.IsSelfUsed(table, value, keyValue))
                                        {

                                            errorlist.Add(string.Format(WarningMessages.NotHalfwidth, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                            //errorMessages += string.Format(WarningMessages.ParentOwnExists, tableColumnInfo.COLUMN_DISPLAY_NAME);
                                        }
                                    }

                                    // 最上位カテゴリーとの分類に矛盾がないか確認します。
                                    if (!string.IsNullOrEmpty(classColumnName) && !string.IsNullOrEmpty(value))
                                    {
                                        var parent = repository.GetParentData(table, value);
                                        //var classId = datas.GetValue(classColumnName);
                                        var classid = masterData.GetValue(classColumnName);  //2019/02/09 TPE.Rin Mod
                                                                                             //if (parent != null && parent.Class != classId)
                                        if (parent != null && parent.Class != classid)
                                        {
                                            var classDisplayName = tableColumnInformations.Single(x => x.COLUMN_NAME == classColumnName).COLUMN_DISPLAY_NAME;
                                            errorlist.Add(string.Format(WarningMessages.Contradiction, classDisplayName, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                            //return string.Format(WarningMessages.Contradiction, classDisplayName, tableColumnInfo.COLUMN_DISPLAY_NAME);
                                        }
                                    }
                                }
                            }
                        }

                        if (tableInformation.TABLE_NAME == "M_REGULATION" && master.mRegulation.DEL_FLG == true)
                        {

                            var sql = new System.Text.StringBuilder();
                            sql.AppendLine("select");
                            sql.AppendLine(" REG_TYPE_ID ");
                            sql.AppendLine("from");
                            sql.AppendLine(" " + tableInformation.TABLE_NAME);
                            sql.AppendLine("where");
                            //sql.AppendLine(" P_REG_TYPE_ID = '" + Convert.ToString(masterData["P_REG_TYPE_ID"]) + "'");   2019/05/11 TPE.Rin Del
                            sql.AppendLine(" P_REG_TYPE_ID = '" + Convert.ToString(master.mRegulation.REG_TYPE_ID) + "'");
                            sql.AppendLine(" and");
                            sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

                            var records = DbUtil.Select(sql.ToString());

                            // 親として1件でも設定されていればエラーとする。
                            if (records.Rows.Count > 0)
                            {
                                //errorlist.Add(string.Format(WarningMessages.ParentAsConfigured, "親法規制", "削除"));
                                errorlist.Add("親として1件でも設定されていればエラーとする");
                            }

                        }

                        if (errorlist.Count() > 0)
                        {
                            TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                            master = DropDownFillRegulation(master);
                            return View(master);
                        }

                        m_Master.masterMaintenanceScreen = master.masterMaintenanceScreen;
                        m_Master.mRegulation = mRegulation;
                        m_Master.TYPE = "Update";
                        m_Master = DropDownFillRegulation(m_Master);
                        //Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.MasterModelZC05902] = m_Master;
                        return RedirectToAction("ZC05903", "ZC050", new { TableName = "M_REGULATION" });
                    }
                }
                else if (master.masterMaintenanceScreen == "M_WORKFLOW")
                {
                    if (Request.Form["btnBack_Click"] != null)
                    {
                        // remove the search maintainence session data and redirect to search screen
                        Session.Remove(SessionConst.MasterModelZC05902);
                        Session.Remove(SessionConst.MasterData);
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = "M_WORKFLOW" });
                    }
                    if (Request.Form["btnRegistration_Click"] != null)
                    {
                        master.TYPE = "New";
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();
                        masterData.Add("FLOW_CD", master.mWorkFlow.FLOW_CD);
                        masterData.Add("FLOW_TYPE_ID", master.mWorkFlow.FLOW_TYPE_ID);
                        masterData["USAGE"] = master.mWorkFlow.USAGE;

                        masterData.Add("APPROVAL_TARGET1", master.mWorkFlow.APPROVAL_TARGET1);
                        masterData["APPROVAL_USER_CD1"] = master.mWorkFlow.APPROVAL_USER_CD1;
                        masterData.Add("SUBSITUTE_USER_CD1", master.mWorkFlow.SUBSITUTE_USER_CD1);
                        masterData.Add("APPROVAL_TARGET2", master.mWorkFlow.APPROVAL_TARGET2);
                        masterData["APPROVAL_USER_CD2"] = master.mWorkFlow.APPROVAL_USER_CD2;
                        masterData.Add("SUBSITUTE_USER_CD2", master.mWorkFlow.SUBSITUTE_USER_CD2);
                        masterData.Add("APPROVAL_TARGET3", master.mWorkFlow.APPROVAL_TARGET3);
                        masterData["APPROVAL_USER_CD3"] = master.mWorkFlow.APPROVAL_USER_CD3;
                        masterData.Add("SUBSITUTE_USER_CD3", master.mWorkFlow.SUBSITUTE_USER_CD3);
                        masterData.Add("APPROVAL_TARGET4", master.mWorkFlow.APPROVAL_TARGET4);
                        masterData["APPROVAL_USER_CD4"] = master.mWorkFlow.APPROVAL_USER_CD4;
                        masterData.Add("SUBSITUTE_USER_CD4", master.mWorkFlow.SUBSITUTE_USER_CD4);
                        masterData.Add("APPROVAL_TARGET5", master.mWorkFlow.APPROVAL_TARGET5);
                        masterData["APPROVAL_USER_CD5"] = master.mWorkFlow.APPROVAL_USER_CD5;
                        masterData.Add("SUBSITUTE_USER_CD5", master.mWorkFlow.SUBSITUTE_USER_CD5);
                        masterData.Add("APPROVAL_TARGET6", master.mWorkFlow.APPROVAL_TARGET6);
                        masterData["APPROVAL_USER_CD6"] = master.mWorkFlow.APPROVAL_USER_CD6;
                        masterData["SUBSITUTE_USER_CD6"] = master.mWorkFlow.SUBSITUTE_USER_CD6;
                        int count = 0;
                        if (master.mWorkFlow.APPROVAL_USER_CD1 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD2 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD3 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD4 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD5 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD6 != null) count++;

                        master.mWorkFlow.FLOW_HIERARCHY = count.ToString();
                        masterData.Add("FLOW_HIERARCHY", master.mWorkFlow.FLOW_HIERARCHY);
                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];


                        IEnumerable<string> errorMessages = ValidateInputValue(tableColumnInformations, masterData, null);

                        List<string> errorlist = new List<string>();
                        errorlist.AddRange(errorMessages);

                        if (errorlist.Count() > 0)
                        {
                            TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                            return View(master);
                        }



                        masterData.Add("DEL_USER_CD", master.mWorkFlow.DEL_FLG == false ? null : Common.GetLogonForm().Id);
                        masterData.Add("DEL_DATE", master.mWorkFlow.DEL_FLG == false ? null : DateTime.Now.ToString());
                        masterData.Add("DEL_FLAG", master.mWorkFlow.DEL_FLG == false ? "0" : "1");
                        master.masterMaintenanceScreen = "M_WORKFLOW";


                        Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.MasterModelZC05902] = master;
                        return RedirectToAction("ZC05903", "ZC050", new { TableName = "M_WORKFLOW" });
                    }
                    else if (Request.Form["btnUpdate_Click"] != null)
                    {
                        master.TYPE = "Update";
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();
                        masterData.Add("FLOW_CD", master.mWorkFlow.FLOW_CD);

                        masterData.Add("FLOW_TYPE_ID", master.mWorkFlow.FLOW_TYPE_ID);

                        masterData["USAGE"] = master.mWorkFlow.USAGE;

                        int count = 0;
                        if (master.mWorkFlow.APPROVAL_USER_CD1 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD2 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD3 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD4 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD5 != null) count++;
                        if (master.mWorkFlow.APPROVAL_USER_CD6 != null) count++;

                        master.mWorkFlow.FLOW_HIERARCHY = count.ToString();
                        masterData.Add("FLOW_HIERARCHY", master.mWorkFlow.FLOW_HIERARCHY);

                        masterData.Add("APPROVAL_TARGET1", master.mWorkFlow.APPROVAL_TARGET1 != null ? master.mWorkFlow.APPROVAL_TARGET1.Trim() : null);
                        masterData["APPROVAL_USER_CD1"] = master.mWorkFlow.APPROVAL_USER_CD1 != null ? master.mWorkFlow.APPROVAL_USER_CD1.Trim() : null;
                        masterData.Add("SUBSITUTE_USER_CD1", master.mWorkFlow.SUBSITUTE_USER_CD1 != null ? master.mWorkFlow.SUBSITUTE_USER_CD1.Trim() : null);
                        masterData.Add("APPROVAL_TARGET2", master.mWorkFlow.APPROVAL_TARGET2 != null ? master.mWorkFlow.APPROVAL_TARGET2.Trim() : null);
                        masterData["APPROVAL_USER_CD2"] = master.mWorkFlow.APPROVAL_USER_CD2 != null ? master.mWorkFlow.APPROVAL_USER_CD2.Trim() : null;
                        masterData.Add("SUBSITUTE_USER_CD2", master.mWorkFlow.SUBSITUTE_USER_CD2 != null ? master.mWorkFlow.SUBSITUTE_USER_CD2.Trim() : null);
                        masterData.Add("APPROVAL_TARGET3", master.mWorkFlow.APPROVAL_TARGET3 != null ? master.mWorkFlow.APPROVAL_TARGET3.Trim() : null);
                        masterData["APPROVAL_USER_CD3"] = master.mWorkFlow.APPROVAL_USER_CD3 != null ? master.mWorkFlow.APPROVAL_USER_CD3.Trim() : null;
                        masterData.Add("SUBSITUTE_USER_CD3", master.mWorkFlow.SUBSITUTE_USER_CD3 != null ? master.mWorkFlow.SUBSITUTE_USER_CD3.Trim() : null);
                        masterData.Add("APPROVAL_TARGET4", master.mWorkFlow.APPROVAL_TARGET4 != null ? master.mWorkFlow.APPROVAL_TARGET4.Trim() : null);
                        masterData["APPROVAL_USER_CD4"] = master.mWorkFlow.APPROVAL_USER_CD4 != null ? master.mWorkFlow.APPROVAL_USER_CD4.Trim() : null;
                        masterData.Add("SUBSITUTE_USER_CD4", master.mWorkFlow.SUBSITUTE_USER_CD4 != null ? master.mWorkFlow.SUBSITUTE_USER_CD4.Trim() : null);
                        masterData.Add("APPROVAL_TARGET5", master.mWorkFlow.APPROVAL_TARGET5 != null ? master.mWorkFlow.APPROVAL_TARGET5.Trim() : null);
                        masterData["APPROVAL_USER_CD5"] = master.mWorkFlow.APPROVAL_USER_CD5 != null ? master.mWorkFlow.APPROVAL_USER_CD5.Trim() : null;
                        masterData.Add("SUBSITUTE_USER_CD5", master.mWorkFlow.SUBSITUTE_USER_CD5 != null ? master.mWorkFlow.SUBSITUTE_USER_CD5.Trim() : null);
                        masterData.Add("APPROVAL_TARGET6", master.mWorkFlow.APPROVAL_TARGET6 != null ? master.mWorkFlow.APPROVAL_TARGET6.Trim() : null);
                        masterData["APPROVAL_USER_CD6"] = master.mWorkFlow.APPROVAL_USER_CD6 != null ? master.mWorkFlow.APPROVAL_USER_CD6.Trim() : null;
                        masterData["SUBSITUTE_USER_CD6"] = master.mWorkFlow.SUBSITUTE_USER_CD6 != null ? master.mWorkFlow.SUBSITUTE_USER_CD6.Trim() : null;
                        List<string> errorlist = new List<string>();

                        // キー値と入力値を結合します。
                        Dictionary<string, string> datas = new Dictionary<string, string>();
                        //datas.Add("GROUP_ID", master.mGroup.GROUP_ID.ToString());
                        foreach (KeyValuePair<string, string> data in masterData)
                        {
                            if (datas.ContainsKey(data.Key)) datas.Remove(data.Key);
                            datas.Add(data.Key, data.Value);
                        }
                        datas.Add("FLOW_ID", master.mWorkFlow.FLOW_ID.ToString());

                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];

                        master.masterMaintenanceScreen = "M_WORKFLOW";

                        IEnumerable<string> errorMessages = ValidateInputValue(tableColumnInformations, datas, masterData);
                        errorlist.AddRange(errorMessages);


                        if (errorlist.Count() > 0)
                        {
                            TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                            return View(master);
                        }

                        masterData.Add("DEL_USER_CD", master.mWorkFlow.DEL_FLG == false ? null : Common.GetLogonForm().Id);
                        string currentdatetime = DateTime.Now.Year + "." + DateTime.Now.Month + "." + DateTime.Now.Day +
                            " " + DateTime.Now.Hour + (":") + DateTime.Now.Minute + (":") + DateTime.Now.Second;
                        masterData.Add("DEL_DATE", master.mWorkFlow.DEL_FLG == false ? null : currentdatetime); //DateTime.Now.ToString()
                        masterData.Add("DEL_FLAG", master.mWorkFlow.DEL_FLG == false ? "0" : "1");
                        master.masterMaintenanceScreen = "M_WORKFLOW";

                        // store model data in session to access in confirmation screen
                        Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.MasterModelZC05902] = master;
                        // redirect to confirmation screen
                        return RedirectToAction("ZC05903", "ZC050", new { TableName = master.masterMaintenanceScreen });
                    }
                }
                else if (master.masterMaintenanceScreen == "M_MAKER")
                {
                    if (Request.Form["btnBack_Click"] != null)
                    {
                        Session.Remove(SessionConst.MasterModelZC05902);
                        Session.Remove(SessionConst.MasterData);
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = master.masterMaintenanceScreen });
                    }
                    var tableColumnInformations = Control.GetTableColumnControl(master.masterMaintenanceScreen);
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Text");
                    dt.Columns.Add("Value");

                    Dictionary<string, string> masterData = new Dictionary<string, string>();
                    var datas = new Dictionary<string, string>();
                    masterData.Add("MAKER_NAME", master.mMaker.MAKER_NAME);
                    masterData.Add("MAKER_URL", master.mMaker.MAKER_URL);
                    masterData.Add("MAKER_TEL", master.mMaker.MAKER_TEL);
                    masterData.Add("MAKER_EMAIL1", master.mMaker.MAKER_EMAIL1);
                    masterData.Add("MAKER_EMAIL2", master.mMaker.MAKER_EMAIL2);
                    masterData.Add("MAKER_EMAIL3", master.mMaker.MAKER_EMAIL3);
                    masterData.Add("MAKER_ADDRESS", master.mMaker.MAKER_ADDRESS);
                    if (master.TYPE == "Update")
                    {
                        datas.Add("MAKER_NAME", master.mMaker.MAKER_NAME);
                        datas.Add("MAKER_ID", master.mMaker.MAKER_ID.ToString());
                        datas.Add("MAKER_URL", master.mMaker.MAKER_URL);
                        datas.Add("MAKER_EMAIL1", master.mMaker.MAKER_EMAIL1);
                        datas.Add("MAKER_EMAIL2", master.mMaker.MAKER_EMAIL2);
                        datas.Add("MAKER_EMAIL3", master.mMaker.MAKER_EMAIL3);
                        datas.Add("MAKER_TEL", master.mMaker.MAKER_TEL);
                        datas.Add("MAKER_ADDRESS", master.mMaker.MAKER_ADDRESS);
                    }
                    List<string> errorlist = new List<string>();
                    foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                    {
                        if (tableColumnInformation.IS_SEARCH)
                        {
                            foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                            {
                                dt.Rows.Add(convertData.Value, convertData.Key);
                                master.LookUpStatus = BindLookupValuesStatus(dt);
                            }
                        }
                    }
                    IEnumerable<string> errorMessages;
                    if (master.TYPE == "Update")
                    {
                        errorMessages = ValidateInputValue(tableColumnInformations, datas, masterData);
                    }
                    else
                    {
                        errorMessages = ValidateInputValue(tableColumnInformations, masterData, null);
                    }
                    errorlist.AddRange(errorMessages);
                    if (errorlist.Count() > 0)
                    {
                        string emsg = null;
                        foreach (var elist in errorlist)
                        {
                            emsg += elist + "\\n";
                        }
                        TempData["Error"] = emsg;
                        return View(master);
                    }

                    masterData.Add("DEL_USER_CD", master.mMaker.MAKER_DEL_FLAG == false ? null : Common.GetLogonForm().Id);
                    string currentdatetime = DateTime.Now.Year + "." + DateTime.Now.Month + "." + DateTime.Now.Day +
                        " " + DateTime.Now.Hour + (":") + DateTime.Now.Minute + (":") + DateTime.Now.Second;
                    masterData.Add("DEL_DATE", master.mMaker.MAKER_DEL_FLAG == false ? null : currentdatetime); //DateTime.Now.ToString()
                    masterData.Add("DEL_FLAG", master.mMaker.MAKER_DEL_FLAG == false ? "0" : "1");
                    Session[SessionConst.MasterData] = masterData;
                    Session[SessionConst.MasterModelZC05902] = master;
                    return RedirectToAction("ZC05903", "ZC050", new { TableName = master.masterMaintenanceScreen });
                }
                else if (master.masterMaintenanceScreen == "M_UNITSIZE")
                {
                    if (Request.Form["btnBack_Click"] != null)
                    {
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = master.masterMaintenanceScreen });
                    }
                    var tableColumnInformations = Control.GetTableColumnControl(master.masterMaintenanceScreen);
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Text");
                    dt.Columns.Add("Value");

                    Dictionary<string, string> masterData = new Dictionary<string, string>();
                    // masterData.Add("UNITSIZE_ID", (master.mUnitSize.UNITSIZE_ID).ToString());
                    masterData.Add("UNIT_NAME", master.mUnitSize.UNIT_NAME);
                    if (master.mUnitSize.FACTOR == null)
                    {
                        masterData.Add("FACTOR", "0");
                    }
                    else
                    {
                        masterData.Add("FACTOR", (master.mUnitSize.FACTOR).ToString());
                    }

                    var datas = new Dictionary<string, string>();



                    if (master.TYPE == "Update")
                    {
                        datas.Add("FACTOR", master.mUnitSize.FACTOR);
                        datas.Add("UNIT_NAME", master.mUnitSize.UNIT_NAME);
                        datas.Add("UNITSIZE_ID", master.mUnitSize.UNITSIZE_ID.ToString());
                    }

                    List<string> errorlist = new List<string>();
                    foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                    {
                        if (tableColumnInformation.IS_SEARCH)
                        {
                            foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                            {
                                dt.Rows.Add(convertData.Value, convertData.Key);
                                master.LookUpStatus = BindLookupValuesStatus(dt);
                            }
                        }
                    }
                    IEnumerable<string> errorMessages;
                    if (master.TYPE == "Update")
                    {
                        errorMessages = ValidateInputValue(tableColumnInformations, datas, masterData);
                    }
                    else
                    {
                        errorMessages = ValidateInputValue(tableColumnInformations, masterData, null);
                    }

                    errorlist.AddRange(errorMessages);
                    if (errorlist.Count() > 0)
                    {
                        string emsg = null;
                        foreach (var elist in errorlist)
                        {
                            emsg += elist + "\\n";
                        }
                        TempData["Error"] = emsg;
                        return View(master);
                    }

                    masterData.Add("DEL_USER_CD", master.mUnitSize.DEL_FLG == false ? null : Common.GetLogonForm().Id);
                    // masterData.Add("DEL_DATE",  == false ? null : DateTime.Now);
                    string currentdatetime = DateTime.Now.Year + "." + DateTime.Now.Month + "." + DateTime.Now.Day +
    " " + DateTime.Now.Hour + (":") + DateTime.Now.Minute + (":") + DateTime.Now.Second;
                    masterData.Add("DEL_DATE", master.mUnitSize.DEL_FLG == false ? null : currentdatetime);
                    masterData.Add("DEL_FLAG", master.mUnitSize.DEL_FLG == false ? "0" : "1");

                    Session[SessionConst.MasterData] = masterData;
                    Session[SessionConst.MasterModelZC05902] = master;
                    return RedirectToAction("ZC05903", "ZC050", new { TableName = master.masterMaintenanceScreen });
                }
                else if (master.masterMaintenanceScreen == "M_ACTION_TYPE")
                {
                    Dictionary<string, string> masterData = new Dictionary<string, string>();
                    if (Request.Form["btnBack_Click"] != null)
                    {
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = master.masterMaintenanceScreen });
                    }
                    if (Request.Form["btnRegistration_Click"] != null)
                    {
                        master.TYPE = "New";
                        masterData.Add("ACTION_TYPE_ID", (master.mActionType.ACTION_TYPE_ID).ToString());
                    }
                    else
                    {
                        master.TYPE = "Update";
                    }
                    masterData.Add("ACTION_TEXT", master.mActionType.ACTION_TEXT);
                    var tableColumnInformations = Control.GetTableColumnControl(master.masterMaintenanceScreen);
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Text");
                    dt.Columns.Add("Value");



                    var datas = new Dictionary<string, string>();
                    datas.Add("ACTION_TYPE_ID", master.mActionType.ACTION_TYPE_ID.ToString());

                    List<string> errorlist = new List<string>();
                    foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                    {
                        if (tableColumnInformation.IS_SEARCH)
                        {
                            foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                            {
                                dt.Rows.Add(convertData.Value, convertData.Key);
                                master.LookUpStatus = BindLookupValuesStatus(dt);
                            }
                        }
                    }
                    var errorMessages = ValidateInputValue(tableColumnInformations, datas, masterData);
                    errorlist.AddRange(errorMessages);
                    if (errorlist.Count() > 0)
                    {
                        TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                        return View(master);
                    }

                    Session[SessionConst.MasterData] = masterData;
                    Session[SessionConst.MasterModelZC05902] = master;
                    return RedirectToAction("ZC05903", "ZC050", new { TableName = master.masterMaintenanceScreen });
                }
                else if (master.masterMaintenanceScreen == "M_STATUS")
                {
                    Dictionary<string, string> masterData = new Dictionary<string, string>();
                    if (Request.Form["btnBack_Click"] != null)
                    {
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = master.masterMaintenanceScreen });
                    }
                    if (Request.Form["btnRegistration_Click"] != null)
                    {
                        master.TYPE = "New";
                        masterData.Add("STATUS_ID", (master.mStatus.STATUS_ID).ToString());
                    }
                    else
                    {
                        master.TYPE = "Update";
                    }
                    var tableColumnInformations = Control.GetTableColumnControl(master.masterMaintenanceScreen);
                    DataTable dt = new DataTable();
                    dt.Columns.Add("Text");
                    dt.Columns.Add("Value");


                    masterData.Add("CLASS_ID", (master.mStatus.CLASS_ID).ToString());
                    masterData.Add("STATUS_TEXT", master.mStatus.STATUS_TEXT);
                    masterData.Add("DEL_FLAG", (master.mStatus.DEL_FLAG).ToString());

                    var datas = new Dictionary<string, string>();
                    datas.Add("STATUS_ID", master.mStatus.STATUS_ID.ToString());
                    datas.Add("CLASS_ID", master.mStatus.CLASS_ID.ToString());
                    datas.Add("STATUS_TEXT", master.mStatus.STATUS_TEXT);
                    List<string> errorlist = new List<string>();
                    foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                    {
                        if (tableColumnInformation.IS_SEARCH)
                        {
                            foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                            {
                                dt.Rows.Add(convertData.Value, convertData.Key);
                                master.LookUpStatus = BindLookupValuesStatus(dt);
                            }
                        }
                    }
                    var errorMessages = ValidateInputValue(tableColumnInformations, datas, masterData);
                    errorlist.AddRange(errorMessages);
                    if (errorlist.Count() > 0)
                    {
                        TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                        return View(master);
                    }
                    Session[SessionConst.MasterData] = masterData;
                    Session[SessionConst.MasterModelZC05902] = master;
                    return RedirectToAction("ZC05903", "ZC050", new { TableName = master.masterMaintenanceScreen });
                }
                else if (master.masterMaintenanceScreen == "M_COMMON")
                {
                    if (Request.Form["btnBack_Click"] != null)
                    {
                        Session.Remove(SessionConst.MasterModelZC05902);
                        Session.Remove(SessionConst.MasterData);
                        master = DropDownFillRegulation(master);
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = "M_COMMON" });
                    }
                    if (Request.Form["btnRegistration_Click"] != null)
                    {
                        master.TYPE = "New";

                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();// GetInputData(pnlIO, tableColumnInformations);

                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];



                        List<string> errorlist = new List<string>();

                        int y = 1;

                        if (tableInformation.TABLE_NAME == "M_COMMON")
                        {
                            //masterData.Add("COMMON_ID", master.mCommon.COMMON_ID.ToString());
                            masterData.Add("TABLE_NAME", master.mCommon.TABLE_NAME.ToString());
                            masterData.Add("KEY_VALUE", master.mCommon.KEY_VALUE.ToString());
                            masterData.Add("DISPLAY_VALUE", master.mCommon.DISPLAY_VALUE);
                            masterData.Add("DISPLAY_ORDER", master.mCommon.DISPLAY_ORDER.ToString());

                            //masterData.Add("REG_USER_CD", master.mCommon.REG_USER_CD);
                            //masterData.Add("REG_USER_NAME", master.mCommon.REG_USER_NAME);
                            //masterData.Add("REG_DATE", DateTime.Now.ToString());

                            //masterData.Add("UPD_DATE", DateTime.Now.ToString());
                            //masterData.Add("UPD_USER_CD", master.mRegulation.UPD_USER_CD);
                            //masterData.Add("UPD_USER_NAME", master.mRegulation.UPD_USER_NAME);
                        }
                        //IEnumerable<string> errorMessages;
                        //errorMessages = ValidateInputValueCommon(tableColumnInformations, masterData);

                        var errorMessages = new List<string>();
                        bool isToothless = false;
                        bool isToothlessAdded = false;

                        foreach (var data in masterData)
                        {
                            var value = data.Value != null ? data.Value : string.Empty;

                            var tableColumnInfo = tableColumnInformations.Where(x => x.COLUMN_NAME == data.Key).Single();

                            //2018/09/25 TPE Add

                            if (tableColumnInfo.IS_REQUIRED && string.IsNullOrWhiteSpace(value))
                            {
                                errorlist.Add(string.Format(WarningMessages.Required, tableColumnInfo.COLUMN_DISPLAY_NAME));
                            }

                            if (tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character || tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Date)
                            {
                                if (tableColumnInfo.DATA_LENGTH > 0 && tableColumnInfo.DATA_LENGTH < value.ToString().Length)
                                {
                                    errorlist.Add(string.Format(WarningMessages.MaximumLength, tableColumnInfo.COLUMN_DISPLAY_NAME, tableColumnInfo.DATA_LENGTH.ToString()));
                                }
                            }

                            var matchCollection = System.Text.RegularExpressions.Regex.Matches(value, @"[a-zA-Z0-9!-/:-@\[-`{-~]+$");
                            if (tableColumnInfo.TABLE_NAME == "M_USER" && tableColumnInfo.COLUMN_NAME == "TEL")
                            {
                                matchCollection = System.Text.RegularExpressions.Regex.Matches(value, @"[a-zA-Z0-9 !-/:-@\[-`{-~]+$");
                            }

                            if (tableColumnInfo.IS_HALFWIDTH && !string.IsNullOrEmpty(value) && (matchCollection.Count != 1 || !(value.Length == matchCollection[0].Length)))
                            {
                                errorlist.Add(string.Format(WarningMessages.NotHalfwidth, tableColumnInfo.COLUMN_DISPLAY_NAME));
                            }

                            // 半角英字のみかチェックします。
                            if (tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character && tableColumnInfo.SUB_1 && !string.IsNullOrEmpty(value))
                            {
                                if (!value.IsAlphabetOrNumber())
                                {
                                    errorlist.Add(string.Format(WarningMessages.NotAlphabetOrNumber, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                }
                            }

                            var isValidNumber = true;
                            if (!string.IsNullOrWhiteSpace(value) && tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
                            {
                                var dec1 = default(decimal);
                                var dec2 = default(decimal);
                                var str1 = string.Empty;
                                var str2 = string.Empty;
                                if (value.Length > 28)
                                {
                                    str1 = value.Substring(0, 28);
                                    str2 = value.Substring(28);
                                }
                                else
                                {
                                    str1 = value;
                                    str2 = "0";

                                }
                                if (!decimal.TryParse(str1, out dec1) || !decimal.TryParse(str2, out dec2))
                                {
                                    isValidNumber = false;
                                    errorlist.Add(string.Format(WarningMessages.NotNumber, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                }

                                //2019/7/23 TPE Add 不正データ登録チェックを追加
                                if (value.IndexOf(',') >= 0 && isValidNumber == true)
                                {
                                    isValidNumber = false;
                                    errorlist.Add(string.Format(WarningMessages.NotNumber, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                }

                            }

                            if (tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Number)
                            {
                                if (isValidNumber)
                                {
                                    decimal dec = default(decimal);
                                    if (decimal.TryParse(value, out dec))
                                    {
                                        var numberArray = value.Replace("-", string.Empty).Replace(",", string.Empty).Split(".".ToCharArray());
                                        if (tableColumnInfo.DATA_LENGTH > 0 && numberArray[0].Length > tableColumnInfo.DATA_LENGTH)
                                        {
                                            errorlist.Add(string.Format(WarningMessages.PositiveNumberLength, tableColumnInfo.COLUMN_DISPLAY_NAME, tableColumnInfo.DATA_LENGTH.ToString()));
                                        }
                                        if (tableColumnInfo.DEC_DATA_LENGTH == 0 && numberArray.Length == 2)
                                        {
                                            errorlist.Add(string.Format(WarningMessages.PositiveNumber, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                        }
                                        else if (numberArray.Length == 2 && numberArray[1].Length > tableColumnInfo.DEC_DATA_LENGTH)
                                        {
                                            errorlist.Add(string.Format(WarningMessages.DecimalNumberLength, tableColumnInfo.COLUMN_DISPLAY_NAME, tableColumnInfo.DEC_DATA_LENGTH.ToString()));
                                        }
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(value) && tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character)
                            {
                                var p = value.ExtractProhibited();
                                if (p.Count() > 0)
                                {
                                    errorlist.Add(string.Format(WarningMessages.ContainProhibitionCharacter, tableColumnInfo.COLUMN_DISPLAY_NAME, string.Join(" ", p).Replace(@"\", @"\\")));
                                }
                            }

                            // メールアドレスの形式であるかチェックします。
                            if (tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Character && tableColumnInfo.SUB_2 && !string.IsNullOrEmpty(value))
                            {
                                if (!value.IsMailAddressFormat())
                                {
                                    errorlist.Add(string.Format(WarningMessages.NotMailAddressFormat, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(value) && tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.Date)
                            {
                                DateTime date = default(DateTime);
                                if (!DateTime.TryParse(value, out date))
                                {
                                    errorlist.Add(string.Format(WarningMessages.NotDate, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                }
                            }

                            if (!string.IsNullOrWhiteSpace(value) && tableColumnInfo.DATA_TYPE == S_TAB_COL_CONTROL.DataType.NIDInput)
                            {
                                var userDic = new UserMasterInfo().GetDictionary();

                                if (!userDic.ContainsKey(value))
                                {
                                    errorlist.Add(string.Format(WarningMessages.NotFound, tableColumnInfo.COLUMN_DISPLAY_NAME + "(NID)"));
                                }

                            }

                            if (!string.IsNullOrWhiteSpace(value) && tableColumnInfo.IS_UNIQUE)
                            {
                                DbContext context = null;
                                context = DbUtil.Open(false);

                                var parameters = new Hashtable();

                                var keyWhere = new StringBuilder();
                                var keyColumns = tableColumnInformations.Where(x => x.IS_KEY);
                                foreach (var keyColumn in keyColumns)
                                {
                                    if (!masterData.ContainsKey(keyColumn.COLUMN_NAME) || string.IsNullOrEmpty(masterData[keyColumn.COLUMN_NAME].ToString())) continue;
                                    keyWhere.AppendLine(" " + keyColumn.COLUMN_NAME + "= :" + keyColumn.COLUMN_NAME);
                                    if (keyColumn != keyColumns.Last()) keyWhere.AppendLine(" and");
                                    parameters.Add(keyColumn.COLUMN_NAME, masterData[keyColumn.COLUMN_NAME]);
                                }
                                var sql = new StringBuilder();
                                sql.AppendLine("select");
                                sql.AppendLine(" " + tableColumnInfo.COLUMN_NAME);
                                sql.AppendLine(" from");
                                sql.AppendLine(" " + tableColumnInfo.TABLE_NAME);
                                sql.AppendLine("where");
                                if (keyWhere.Length > 0)
                                {
                                    sql.AppendLine(" not(" + keyWhere.ToString() + ")");
                                    sql.AppendLine(" and");
                                }

                                //2018/08/10 TPE Delete start
                                //sql.AppendLine(" " + tableColumnInfo.COLUMN_NAME + " = :" + tableColumnInfo.COLUMN_NAME);

                                //if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value);
                                //2018/08/10 TPE Delete End

                                //2018/08/10 TPE.Sugimoto Add Sta
                                if (tableColumnInfo.COLUMN_NAME == "UNIT_NAME" ||
                                    tableColumnInfo.COLUMN_NAME == "USAGE")
                                {
                                    sql.AppendLine(" REPLACE(" + tableColumnInfo.COLUMN_NAME + ",' ','') = :" + tableColumnInfo.COLUMN_NAME);
                                    if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value.Replace(" ", "").Replace("　", ""));
                                }
                                //2018/09/20 TPE.Kometani Add Sta
                                else if (tableColumnInfo.COLUMN_NAME == "MAKER_NAME")
                                {
                                    sql.AppendLine(" REPLACE(" + tableColumnInfo.COLUMN_NAME + ",' ','') = :" + tableColumnInfo.COLUMN_NAME);
                                    sql.AppendLine(" COLLATE Japanese_CI_AS_KS ");
                                    if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value.Replace(" ", "").Replace("　", ""));
                                }
                                //2018/09/20 TPE.Kometani Add End
                                //2018/10/31 TPE.Sugimoto Add Sta
                                else if (tableColumnInfo.COLUMN_NAME == "LOC_BARCODE")
                                {
                                    sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");
                                    sql.AppendLine(" and");
                                    sql.AppendLine(" " + tableColumnInfo.COLUMN_NAME + " = :" + tableColumnInfo.COLUMN_NAME);
                                    if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value);
                                }
                                //2018/10/31 TPE.Sugimoto Add End
                                else
                                {
                                    sql.AppendLine(" " + tableColumnInfo.COLUMN_NAME + " = :" + tableColumnInfo.COLUMN_NAME);
                                    if (!parameters.ContainsKey(tableColumnInfo.COLUMN_NAME)) parameters.Add(tableColumnInfo.COLUMN_NAME, value);
                                }
                                //2018/08/10 TPE.Sugimoto Add End


                                var records = DbUtil.Select(context, sql.ToString(), parameters);

                                if (records.Rows.Count > 0)
                                {
                                    errorlist.Add(string.Format(WarningMessages.NotUnique, tableColumnInfo.COLUMN_DISPLAY_NAME, value));
                                }

                                context.Close();
                            }


                            //2019/02/18 TPE.Sugimoto Add End

                            string keyColumnName;
                            string valueColumnName;
                            string iconPathColumnName;
                            string sortOrderColumnName;
                            string classColumnName;
                            string selFlgName;//2018/08/19 TPE.Sugimoto Add
                            string classId;

                            // 親子関係があるテーブル且つ分類がある場合
                            //2018/08/19 TPE Delete Start
                            //if (IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                            //                                                                                  out sortOrderColumnName, out classColumnName))
                            //2018/08/19 TPE Delete End

                            //2018/08/19 TPE Add Start
                            if (repository.IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                                                                                                              out sortOrderColumnName, out classColumnName, out selFlgName))
                            //2018/08/19 TPE Add End
                            //if (IsSelectButtonTarget(tableColumnInfo.TABLE_NAME, tableColumnInfo.COLUMN_NAME, out keyColumnName, out valueColumnName, out iconPathColumnName,
                            //                                                                                  out sortOrderColumnName, out classColumnName, out selFlgName,out classId))    //2019/02/09 TPE.Rin Add
                            {
                                var table = repository.GetParentChildTable(tableColumnInfo.TABLE_NAME, keyColumnName, classColumnName, tableColumnInfo.COLUMN_NAME);

                                // 上位カテゴリーに自身が存在しないか確認します。
                                var keyValue = string.Empty;
                                if (masterData.ContainsKey(keyColumnName)) keyValue = masterData[keyColumnName];
                                if (!string.IsNullOrEmpty(keyValue) && !string.IsNullOrEmpty(value))
                                {
                                    if (repository.IsSelfUsed(table, value, keyValue))
                                    {
                                        errorlist.Add(string.Format(WarningMessages.ParentOwnExists, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                    }

                                }

                                // 最上位カテゴリーとの分類に矛盾がないか確認します。
                                if (!string.IsNullOrEmpty(classColumnName) && !string.IsNullOrEmpty(value))
                                {
                                    var parent = repository.GetParentData(table, value);
                                    //var classId = datas.GetValue(classColumnName);
                                    var classid = masterData.GetValue(classColumnName);  //2019/02/09 TPE.Rin Mod
                                                                                         //if (parent != null && parent.Class != classId)
                                    if (parent != null && parent.Class != classid)
                                    {
                                        var classDisplayName = tableColumnInformations.Single(x => x.COLUMN_NAME == classColumnName).COLUMN_DISPLAY_NAME;
                                        errorlist.Add(string.Format(WarningMessages.Contradiction, classDisplayName, tableColumnInfo.COLUMN_DISPLAY_NAME));
                                    }
                                }
                            }

                            // フロー階層に歯抜けがある場合
                            if (tableColumnInfo.TABLE_NAME == "M_WORKFLOW" && !isToothlessAdded)
                            {
                                if (tableColumnInfo.COLUMN_NAME.Contains("APPROVAL_USER_CD"))
                                {
                                    if (string.IsNullOrWhiteSpace(value) && isToothless == false)
                                    {
                                        isToothless = true;

                                    }
                                    else if (!string.IsNullOrWhiteSpace(value) && isToothless == true)
                                    {
                                        errorlist.Add(string.Format(WarningMessages.Toothless));
                                        isToothlessAdded = true;
                                    }
                                }
                            }

                            //if (errorlist.Count != 0)
                            //{
                            //    return RedirectToAction("ZC01090", "ZC010");
                            //}

                        }

                        if (errorlist.Count() > 0)
                        {
                            TempData["Error"] = string.Join(SystemConst.ScriptLinefeed, errorlist);
                            master = DropDownFillRegulation(master);
                            return View(master);
                        }

                        master = DropDownFillRegulation(master);
                        Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.MasterModelZC05902] = master;
                        return RedirectToAction("ZC05903", "ZC050", new { TableName = "M_COMMON" });
                    }
                    else if (Request.Form["btnUpdate_Click"] != null)
                    {

                        string useCode = Classes.util.Common.GetLogonForm().Id;
                        List<CasRegModel> cas = new List<CasRegModel>();
                        List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                        Dictionary<string, string> masterData = new Dictionary<string, string>();// GetInputData(pnlIO, tableColumnInformations);


                        S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];

                        List<string> errorlist = new List<string>();
                        if (tableInformation.TABLE_NAME == "M_REGULATION")
                        {
                            if (master.mRegulation.MARKS != null)
                            {
                                //点数の入力値チェック 3桁の数字　2019/04/13
                                var tb = master.mRegulation.MARKS;
                                var r = new Regex(@"^[0-9]+$");
                                var match = r.Match(tb);
                                if (match.Value == tb)
                                {
                                    if (match.Length > 3)
                                    {
                                        errorlist.Add("点数は半角数字3桁です。");
                                    }
                                }
                                else
                                {
                                    errorlist.Add("点数は半角数字3桁です。");
                                }
                            }
                        }
                        var errorMessages = ValidateInputValue(tableColumnInformations, masterData, null);
                        List<string> ChiefName = new List<string>();
                        int y = 1;

                        errorlist.AddRange(errorMessages);
                        if (Session[SessionConst.SessionCas] != null)
                        {
                            cas = (List<CasRegModel>)Session[SessionConst.SessionCas];
                        }
                        M_REGULATION mRegulation = new M_REGULATION();
                        m_Master.listCasRegModelDetails = cas;
                        //m_Master.mRegulation.REG_TYPE_ID = master.mRegulation.REG_TYPE_ID;
                        mRegulation.CLASS_ID = master.mRegulation.CLASS_ID;
                        mRegulation.CLASSIFICATION = master.mRegulation.CLASSIFICATION;
                        mRegulation.REG_TEXT = master.mRegulation.REG_TEXT;
                        mRegulation.REG_ACRONYM_TEXT = master.mRegulation.REG_ACRONYM_TEXT;
                        mRegulation.REG_ICON_PATH = master.mRegulation.REG_ICON_PATH;
                        mRegulation.REG_TYPE_ID = master.mRegulation.REG_TYPE_ID;
                        //masterData.Add("REG_ICON_NAME", master.mRegulation.REG_ICON_NAME);
                        //masterData.Add("REG_TEXT_CONDITION", master.mRegulation.REG_TEXT_CONDITION);
                        mRegulation.P_REG_TYPE_ID = master.mRegulation.P_REG_TYPE_ID;
                        //masterData.Add("P_REG_TYPE_VALUE", master.mRegulation.P_REG_TYPE_VALUE);
                        mRegulation.KEY_MGMT = master.mRegulation.KEY_MGMT;
                        //masterData.Add("KEY_MGMT_NAME", master.mRegulation.KEY_MGMT_NAME);
                        mRegulation.MEASUREMENT_FLAG_VALUE = master.mRegulation.MEASUREMENT_FLAG_VALUE;
                        mRegulation.EXAMINATION_FLAG_VALUE = master.mRegulation.EXAMINATION_FLAG_VALUE;
                        mRegulation.RA_FLAG2 = master.mRegulation.RA_FLAG2;
                        mRegulation.RA_FLAG = master.mRegulation.RA_FLAG;
                        mRegulation.SEL_FLAG = master.mRegulation.SEL_FLAG;
                        mRegulation.DEL_FLG = master.mRegulation.DEL_FLG;
                        mRegulation.DEL_FLAG = master.mRegulation.DEL_FLAG;
                        //masterData.Add("MEASUREMENT_FLAG_VALUE", master.mRegulation.MEASUREMENT_FLAG_VALUE);
                        //masterData.Add("RA_FLAG2", master.mRegulation.RA_FLAG2.ToString());
                        //masterData.Add("REG_DATE", master.mRegulation.REG_DATE.ToString());
                        mRegulation.EXPOSURE_REPORT = master.mRegulation.EXPOSURE_REPORT;
                        //masterData.Add("EXPOSURE_REPORT_VALUE", master.mRegulation.EXPOSURE_REPORT_VALUE);
                        mRegulation.FIRE_SIZE = master.mRegulation.FIRE_SIZE;
                        mRegulation.UNITSIZE_ID = master.mRegulation.UNITSIZE_ID;
                        //masterData.Add("UNITSIZE_NAME", master.mRegulation.UNITSIZE_NAME);
                        //masterData.Add("UPD_DATE", master.mRegulation.UPD_DATE.ToString());
                        //masterData.Add("UPD_USER_CD", master.mRegulation.UPD_USER_CD);
                        //masterData.Add("UPD_USER_NAME", master.mRegulation.UPD_USER_NAME);
                        //masterData.Add("DEL_USER_NAME", master.mRegulation.DEL_USER_NAME);
                        //masterData.Add("DEL_USER_NM", master.mRegulation.DEL_USER_NM);
                        //masterData.Add("DEL_FLAG_VALUE", master.mRegulation.DEL_FLAG_VALUE);
                        mRegulation.SORT_ORDER = master.mRegulation.SORT_ORDER;
                        //masterData.Add("STATUS", master.mRegulation.STATUS);
                        //masterData.Add("SEL_FLAG", master.mRegulation.SEL_FLAG.ToString());
                        mRegulation.WEIGHT_MGMT = master.mRegulation.WEIGHT_MGMT;
                        //masterData.Add("WEIGHT_MGMT_NAME", master.mRegulation.WEIGHT_MGMT_NAME);
                        mRegulation.WORKTIME_MGMT = master.mRegulation.WORKTIME_MGMT;
                        //masterData.Add("WORKTIME_MGMT_NAME", master.mRegulation.WORKTIME_MGMT_NAME);
                        //masterData.Add("WORK_RECORD", master.mRegulation.WORK_RECORD.ToString());
                        //masterData.Add("CAS_NO", master.mRegulation.CAS_NO);
                        //masterData.Add("CLASS_NAME", master.mRegulation.CLASS_NAME);
                        //masterData.Add("REG_TEXT_CONDITION ", master.mRegulation.REG_TEXT_CONDITION);


                        if (master.mRegulation.DEL_FLG == true)
                        {
                            mRegulation.UPD_USER_CD = null;
                            mRegulation.UPD_DATE = null;
                        }
                        else
                        {
                            mRegulation.UPD_USER_CD = useCode;
                            mRegulation.UPD_DATE = DateTime.Now;
                        }
                        mRegulation.MARKS = master.mRegulation.MARKS;
                        if (master.mRegulation.RA_FLAG2 == false)
                        {
                            mRegulation.RA_FLAG = "0";
                        }
                        else
                        {
                            mRegulation.RA_FLAG = "1";
                        }
                        if (master.mRegulation.EXAMINATION_FLAG_VALUE == false)
                        {
                            mRegulation.EXAMINATION_FLAG = "0";
                        }
                        else
                        {
                            mRegulation.EXAMINATION_FLAG = "1";
                        }

                        if (master.mRegulation.MEASUREMENT_FLAG_VALUE == false)
                        {
                            mRegulation.MEASUREMENT_FLAG = "0";
                        }
                        else
                        {
                            mRegulation.MEASUREMENT_FLAG = "1";
                        }
                        //m_Master.mRegulation.RA_FLAG = master.mRegulation.RA_FLAG;
                        mRegulation.DEL_FLAG = master.mRegulation.DEL_FLAG;

                        if (master.mRegulation.DEL_FLG == false)
                        {
                            mRegulation.DEL_USER_CD = null;
                            mRegulation.DEL_DATE = null;
                            mRegulation.DEL_FLAG = 0;
                        }
                        else
                        {
                            mRegulation.DEL_USER_CD = useCode;
                            mRegulation.DEL_DATE = DateTime.Now;
                            mRegulation.DEL_FLAG = 1;

                        }
                        Session.Add(SessionConst.MasterData, master.mRegulation);
                        if (tableInformation.TABLE_NAME == "M_REGULATION" && master.mRegulation.DEL_FLG == true)
                        {

                            var sql = new System.Text.StringBuilder();
                            sql.AppendLine("select");
                            sql.AppendLine(" REG_TYPE_ID ");
                            sql.AppendLine("from");
                            sql.AppendLine(" " + tableInformation.TABLE_NAME);
                            sql.AppendLine("where");
                            //sql.AppendLine(" P_REG_TYPE_ID = '" + Convert.ToString(masterData["P_REG_TYPE_ID"]) + "'");   2019/05/11 TPE.Rin Del
                            sql.AppendLine(" P_REG_TYPE_ID = '" + Convert.ToString(master.mRegulation.REG_TYPE_ID) + "'");
                            sql.AppendLine(" and");
                            sql.AppendLine(" " + nameof(SystemColumn.DEL_FLAG) + " = 0");

                            var records = DbUtil.Select(sql.ToString());

                            // 親として1件でも設定されていればエラーとする。
                            if (records.Rows.Count > 0)
                            {
                                //errorlist.Add(string.Format(WarningMessages.ParentAsConfigured, "親法規制", "削除"));
                                errorlist.Add("親として1件でも設定されていればエラーとする");
                            }

                        }

                        //if (tableInformation.TABLE_NAME == "M_REGULATION")
                        //{

                        //    masterData.Add("DEL_USER_CD", master.mRegulation.DEL_FLG == false ? null : Classes.Util.Common.GetLogonForm().Id); // userInfo.USER_CD
                        //                                                                                                                       // masterData.Add("DEL_DATE", master.mRegulation.DEL_FLG == false ? null : DateTime.Now.ToString());
                        //    masterData.Add("DEL_FLAG", master.mRegulation.DEL_FLG == false ? "0" : "1");

                        //    masterData.Add("RA_FLAG", master.mRegulation.RA_FLAG2 == false ? "0" : "1");
                        //    masterData.Add("MARKS", master.mRegulation.MARKS);

                        //    masterData.Add("UPD_USER_CD", master.mRegulation.DEL_FLG == true ? null : Classes.Util.Common.GetLogonForm().Id);
                        //    //masterData.Add("UPD_DATE", master.mRegulation.DEL_FLG == true ? null : DateTime.Now.ToString());

                        //    //masterData.Add("REG_TYPE_ID", master.mRegulation.REG_TYPE_ID.ToString());
                        //    masterData.Add("CLASS_ID", master.mRegulation.CLASS_ID.ToString());
                        //    //masterData.Add("CLASSIFICATION", master.mRegulation.CLASSIFICATION);
                        //    masterData.Add("REG_TEXT", master.mRegulation.REG_TEXT);
                        //    masterData.Add("REG_ACRONYM_TEXT", master.mRegulation.REG_ACRONYM_TEXT);
                        //    masterData.Add("REG_ICON_PATH", master.mRegulation.REG_ICON_PATH);
                        //    //masterData.Add("REG_ICON_NAME", master.mRegulation.REG_ICON_NAME);
                        //    //masterData.Add("REG_TEXT_CONDITION", master.mRegulation.REG_TEXT_CONDITION);
                        //    masterData.Add("P_REG_TYPE_ID", master.mRegulation.P_REG_TYPE_ID.ToString());
                        //    //masterData.Add("P_REG_TYPE_VALUE", master.mRegulation.P_REG_TYPE_VALUE);
                        //    masterData.Add("KEY_MGMT", master.mRegulation.KEY_MGMT);
                        //    //masterData.Add("KEY_MGMT_NAME", master.mRegulation.KEY_MGMT_NAME);
                        //    masterData.Add("MEASUREMENT_FLAG", master.mRegulation.MEASUREMENT_FLAG_VALUE.ToString());
                        //    //masterData.Add("MEASUREMENT_FLAG_VALUE", master.mRegulation.MEASUREMENT_FLAG_VALUE);
                        //    masterData.Add("RA_FLAG2", master.mRegulation.RA_FLAG2.ToString());
                        //    //masterData.Add("REG_DATE", master.mRegulation.REG_DATE.ToString());
                        //    masterData.Add("EXPOSURE_REPORT", master.mRegulation.EXPOSURE_REPORT.ToString());
                        //    //masterData.Add("EXPOSURE_REPORT_VALUE", master.mRegulation.EXPOSURE_REPORT_VALUE);
                        //    masterData.Add("FIRE_SIZE", master.mRegulation.FIRE_SIZE);
                        //    masterData.Add("UNITSIZE_ID", master.mRegulation.UNITSIZE_ID);
                        //    //masterData.Add("UNITSIZE_NAME", master.mRegulation.UNITSIZE_NAME);
                        //    //masterData.Add("UPD_DATE", master.mRegulation.UPD_DATE.ToString());
                        //    //masterData.Add("UPD_USER_CD", master.mRegulation.UPD_USER_CD);
                        //    //masterData.Add("UPD_USER_NAME", master.mRegulation.UPD_USER_NAME);
                        //    //masterData.Add("DEL_USER_NAME", master.mRegulation.DEL_USER_NAME);
                        //    //masterData.Add("DEL_USER_NM", master.mRegulation.DEL_USER_NM);
                        //    //masterData.Add("DEL_FLAG_VALUE", master.mRegulation.DEL_FLAG_VALUE);
                        //    masterData.Add("SORT_ORDER", master.mRegulation.SORT_ORDER.ToString());
                        //    //masterData.Add("STATUS", master.mRegulation.STATUS);
                        //    masterData.Add("SEL_FLAG", master.mRegulation.SEL_FLAG.ToString());
                        //    masterData.Add("EXAMINATION_FLAG", master.mRegulation.EXAMINATION_FLAG_VALUE.ToString());
                        //    masterData.Add("WEIGHT_MGMT", master.mRegulation.WEIGHT_MGMT);
                        //    masterData.Add("DEL_FLAG", master.mRegulation.DEL_FLAG.ToString());
                        //    //masterData.Add("WEIGHT_MGMT_NAME", master.mRegulation.WEIGHT_MGMT_NAME);
                        //    masterData.Add("WORKTIME_MGMT", master.mRegulation.WORKTIME_MGMT.ToString());
                        //    //masterData.Add("WORKTIME_MGMT_NAME", master.mRegulation.WORKTIME_MGMT_NAME);
                        //    //masterData.Add("WORK_RECORD", master.mRegulation.WORK_RECORD.ToString());
                        //    //masterData.Add("CAS_NO", master.mRegulation.CAS_NO);
                        //    //masterData.Add("CLASS_NAME", master.mRegulation.CLASS_NAME);
                        //    //masterData.Add("REG_TEXT_CONDITION ", master.mRegulation.REG_TEXT_CONDITION);



                        //}
                        m_Master.masterMaintenanceScreen = "M_REGULATION";
                        //m_Master.TableName = "M_COMMON";
                        m_Master.mRegulation = mRegulation;
                        m_Master.TYPE = "Update";
                        m_Master = DropDownFillRegulation(m_Master);
                        //Session[SessionConst.MasterData] = masterData;
                        Session[SessionConst.MasterModelZC05902] = m_Master;
                        return RedirectToAction("ZC05903", "ZC050", new { TableName = "M_REGULATION" });
                    }
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
                if (ex.ToString() != null)
                {
                    return RedirectToAction("ZC01090", "ZC010", new { Message = ex.ToString() });
                }
            }
            return View(master);
        }

        [Authorize(Roles = "0,1")]
        public ActionResult ZC05903(string TableName, string Type, string id)
        {
            // get the maintainence screen session model and display confirmation screen
            M_MASTER master = (M_MASTER)Session[SessionConst.MasterModelZC05902];

            if (TableName == "M_ACTION_TYPE" && Type == "Delete")
            {
                DynamicParameters dynp = new DynamicParameters();
                string sql = "SELECT * FROM [M_ACTION_TYPE] WHERE ACTION_TYPE_ID=@ACTION_TYPE_ID";
                dynp.Add("@ACTION_TYPE_ID", id);
                List<M_ACTION> m_action = DbUtil.Select<M_ACTION>(sql, dynp);
                //   masterKeys.Add("ACTION_TYPE_ID", id);
                master = new M_MASTER();
                master.masterMaintenanceScreen = TableName;
                master.mActionType = new M_ACTION();
                if (m_action.Count > 0)
                {
                    master.TYPE = "Delete";
                    master.mActionType.ACTION_TYPE_ID = m_action[0].ACTION_TYPE_ID;
                    master.mActionType.ACTION_TEXT = m_action[0].ACTION_TEXT;
                    master.mActionType.DEL_FLAG = m_action[0].DEL_FLAG;
                }
            }
            else if (TableName == "M_STATUS")
            {
                if (Type == "Delete")
                {
                    DynamicParameters dynp = new DynamicParameters();
                    string sql = "SELECT * FROM [M_STATUS] WHERE STATUS_ID=@STATUS_ID";
                    dynp.Add("@STATUS_ID", id);
                    List<M_STATUS> m_status = DbUtil.Select<M_STATUS>(sql, dynp);
                    //   masterKeys.Add("ACTION_TYPE_ID", id);
                    master = new M_MASTER();
                    master.masterMaintenanceScreen = TableName;
                    master.mStatus = new M_STATUS();
                    if (m_status.Count > 0)
                    {
                        master.TYPE = "Delete";
                        master.mStatus.STATUS_ID = m_status[0].STATUS_ID;
                        master.mStatus.CLASS_ID = m_status[0].CLASS_ID;
                        master.mStatus.STATUS_TEXT = m_status[0].STATUS_TEXT;
                        master.mStatus.DEL_FLAG = m_status[0].DEL_FLAG;
                    }
                }
                var tableColumnInformations = Control.GetTableColumnControl(master.masterMaintenanceScreen);
                DataTable dt = new DataTable();
                dt.Columns.Add("Text");
                dt.Columns.Add("Val");
                foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                {
                    if (tableColumnInformation.IS_SEARCH)
                    {
                        foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                        {
                            dt.Rows.Add(convertData.Value, convertData.Key);
                            master.LookUpStatus = BindLookupValuesStatus(dt);
                        }
                    }
                }
            }
            else if (TableName == "M_REGULATION")
            {
                return View(master);
            }
            Session[SessionConst.MasterModelZC05902] = master;

            return View(master);

        }

        [HttpPost]
        public ActionResult ZC05903(M_MASTER master)
        {
            try
            {
                if (Request.Form["btnBack_Click"] != null)
                {
                    if (master.TYPE == "Delete")
                    {
                        return RedirectToAction("ZC05901", "ZC050", new { IsPostBack = true, TableName = master.masterMaintenanceScreen });
                    }
                    return RedirectToAction("ZC05902", "ZC050", new { IsPostBack = true, TableName = master.masterMaintenanceScreen });
                }
                if (Request.Form["btnConfirm_Click"] != null)
                {
                    S_TAB_CONTROL tableInformation = (S_TAB_CONTROL)Session[SessionConst.TableInformation];
                    List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                    Dictionary<string, string> masterData = new Dictionary<string, string>();
                    Dictionary<string, string> chemchief = (Dictionary<string, string>)Session[SessionConst.sessionChemChief];
                    master.masterMaintenanceScreen = tableInformation.TABLE_NAME;
                    List<CasRegModel> listCaseModel = new List<CasRegModel>();
                    if (tableInformation.TABLE_NAME == "M_REGULATION")
                    {
                        master = (M_MASTER)Session[SessionConst.MasterModelZC05902];
                    }
                    if (tableInformation.TABLE_NAME != "M_REGULATION")
                    {
                        masterData = (Dictionary<string, string>)Session[SessionConst.MasterData];
                    }
                    if (Session[SessionConst.SessionCas] != null)
                    {
                        listCaseModel = (List<CasRegModel>)Session[SessionConst.SessionCas];
                    }
                    Dictionary<string, string> masterKeys = new Dictionary<string, string>();
                    // check if it is creating new record or updating existing record
                    if (master.TYPE == "New")
                    {
                        if (tableInformation.TABLE_NAME == "M_GROUP")
                        {
                            InsertCommonMaster(tableInformation, tableColumnInformations, masterData, Common.GetLogonForm().Id);
                        }
                        else if (tableInformation.TABLE_NAME == "M_LOCATION")
                        {
                            InsertLocation(tableInformation, tableColumnInformations, masterData, Common.GetLogonForm().Id, chemchief);
                        }
                        else if (tableInformation.TABLE_NAME == "M_WORKFLOW")
                        {
                            InsertCommonMaster(tableInformation, tableColumnInformations, masterData, Common.GetLogonForm().Id);
                        }
                        else if (tableInformation.TABLE_NAME == "M_UNITSIZE")
                        {
                            InsertCommonMaster(tableInformation, tableColumnInformations, masterData, Common.GetLogonForm().Id);
                        }
                        else if (tableInformation.TABLE_NAME == "M_REGULATION")
                        {
                            master.listCasRegModelDetails = listCaseModel;
                            //InsertCommonMaster(tableInformation, tableColumnInformations, master, Classes.Util.Common.GetLogonForm().Id);
                            InsertCAS(tableInformation, tableColumnInformations, master, Common.GetLogonForm().Id, listCaseModel);
                        }
                        else /*if (tableInformation.TABLE_NAME == "M_MAKER")*/
                        {
                            InsertCommonMaster(tableInformation, tableColumnInformations, masterData, Common.GetLogonForm().Id);
                        }
                        TempData["Message"] = Common.GetMessage("I001");
                    }
                    else if (master.TYPE == "Update")
                    {
                        if (tableInformation.TABLE_NAME == "M_LOCATION")
                        {
                            // update the group location record
                            masterKeys.Add("LOC_ID", master.mLocation.LOC_ID.ToString());
                            UpdateLocation(tableInformation, masterKeys, masterData, Common.GetLogonForm().Id, chemchief);
                        }
                        else if (tableInformation.TABLE_NAME == "M_WORKFLOW")
                        {
                            masterKeys.Add("FLOW_ID", master.mWorkFlow.FLOW_ID.ToString());
                            UpdateCommonMaster(tableInformation, masterKeys, masterData, Common.GetLogonForm().Id);
                        }
                        else if (tableInformation.TABLE_NAME == "M_GROUP")
                        {
                            // update the group master record
                            masterKeys.Add("GROUP_ID", master.mGroup.GROUP_ID.ToString());
                            UpdateCommonMaster(tableInformation, masterKeys, masterData, Common.GetLogonForm().Id);
                        }
                        else if (tableInformation.TABLE_NAME == "M_MAKER")
                        {
                            masterKeys.Add("MAKER_ID", master.mMaker.MAKER_ID.ToString());
                            UpdateCommonMaster(tableInformation, masterKeys, masterData, Common.GetLogonForm().Id);
                        }
                        else if (tableInformation.TABLE_NAME == "M_STATUS")
                        {
                            masterKeys.Add("STATUS_ID", master.mStatus.STATUS_ID.ToString());
                            UpdateCommonMaster(tableInformation, masterKeys, masterData, Common.GetLogonForm().Id);
                        }
                        else if (tableInformation.TABLE_NAME == "M_ACTION_TYPE")
                        {
                            masterKeys.Add("ACTION_TYPE_ID", master.mActionType.ACTION_TYPE_ID.ToString());
                            UpdateCommonMaster(tableInformation, masterKeys, masterData, Common.GetLogonForm().Id);
                        }
                        else if (tableInformation.TABLE_NAME == "M_UNITSIZE")
                        {
                            masterKeys.Add("UNITSIZE_ID", master.mUnitSize.UNITSIZE_ID.ToString());
                            UpdateCommonMaster(tableInformation, masterKeys, masterData, Common.GetLogonForm().Id);
                        }
                        else if (tableInformation.TABLE_NAME == "M_REGULATION")
                        {
                            masterKeys.Add("REG_TYPE_ID", master.mRegulation.REG_TYPE_ID.ToString());
                            master.listCasRegModelDetails = listCaseModel;
                            UpdateCAS(tableInformation, masterKeys, master, Classes.util.Common.GetLogonForm().Id, listCaseModel);
                        }
                        TempData["Message"] = Common.GetMessage("I002");
                    }
                    else if (master.TYPE == "Delete")
                    {
                        masterData = new Dictionary<string, string>();
                        if (tableInformation.TABLE_NAME == "M_WORKFLOW")
                        {
                            if (isUsedByUser(masterData) || isRegistByFlow(masterData))
                            {
                                // Massage.xmlからメッセージ取得でエラーになるため、メッセージ直書き("C058")
                                TempData["Error"] = "フロー中、またはユーザーのフローコードに設定されているワークフローの為、削除できません。";
                                return View(master);
                            }
                        }
                        else if (tableInformation.TABLE_NAME == "M_ACTION_TYPE")
                        {
                            masterKeys.Add("ACTION_TYPE_ID", master.mActionType.ACTION_TYPE_ID.ToString());
                        }
                        else if (tableInformation.TABLE_NAME == "M_STATUS")
                        {
                            masterKeys.Add("STATUS_ID", master.mStatus.STATUS_ID.ToString());
                        }
                        masterData.Add(nameof(SystemColumn.DEL_FLAG), Convert.ToString((int)DEL_FLAG.Deleted));
                        UpdateCommonMaster(tableInformation, masterKeys, masterData, Common.GetLogonForm().Id);
                        TempData["Message"] = Common.GetMessage("I003");
                    }
                    if (master.TYPE == "Update" && (tableInformation.TABLE_NAME == "M_USER" || tableInformation.TABLE_NAME == "M_WORKFLOW"))
                    {
                        // uncomment below 2 lines once management ledger will be integrated
                        //using (var workflow = new LedgerWorkHistoryDataInfo())
                        //{
                        //    workflow.UpdateFlowChange(masterData["FLOW_CD"]);
                        //}
                    }
                    // Remove the session to refresh the search results in search master webgrid
                    Session.Remove(SessionConst.WebGridCount);
                    Session.Remove(SessionConst.WebGrid);
                    Session.Remove(SessionConst.sessionChemChief);
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            //redirect to search master screen
            return RedirectToAction("ZC05901", "ZC050", new { TableName = master.masterMaintenanceScreen, IsPostBack = true });
        }

        public IEnumerable<SelectListItem> BindLookupValuesStatus(DataTable res)
        {
            //List<DataRow> list =res;
            List<DataRow> list = res.AsEnumerable().ToList();
            IEnumerable<SelectListItem> selectlist = from s in list
                                                     select new SelectListItem
                                                     {
                                                         Text = s["Text"].ToString(),
                                                         Value = s["Val"].ToString()
                                                     };
            return selectlist;
        }

        /// Fire Service Law Master

        public ActionResult ZC05031(string TableName, string IsPostBack)
        {
            mFire = new M_FIRE();
            if (Session["SearchResult"] != null)
            {
                M_FIRE m_Fire = (M_FIRE)Session["SearchResult"];
                mFire.searchFlag = m_Fire.searchFlag;

            }
            if (TableName == "M_FIRE" && (IsPostBack == "True" && mFire.searchFlag == "1"))
            {

                Session["SearchResult"] = null;
                mFire.hdnLocationId = Request.Form["Locationidval"];
                mFire.IsSetSession = true;
                mFire.listMFire = repository.DoSearch(mFire);

                Session["SearchResult"] = mFire;
            }
            //mFire.labelMode = mode;
            mFire = DropDownFillFire(mFire);
            return View(mFire);

        }

        [HttpPost]
        public ActionResult ZC05031(M_FIRE mFire)
        {
            try
            {
                if (Request.Form["btnSearch"] != null)
                {
                    Session["SearchResult"] = null;
                    mFire.hdnLocationId = Request.Form["Locationidval"];
                    mFire.IsSetSession = true;
                    mFire.listMFire = repository.DoSearch(mFire);

                    mFire.searchFlag = "1";
                    Session["SearchResult"] = mFire;
                }
                else if (Request.Form["btnClear"] != null)
                {
                    Session["SearchResult"] = null;
                    Session["TBLFIRECLASS"] = string.Empty;
                    Session["TblFireListUpd"] = string.Empty;

                    return RedirectToAction("ZC05031", "ZC050");
                }
                else if (Request.Form["btnRegister"] != null)
                {
                    mFire.mode = "0";
                    return RedirectToAction("ZC05032", "ZC050", new { Type = "New", TableName = "M_FIRE_REPORT", IsPostBack = "False" });
                }
                else if (Request.Form["btnUpdate"] != null)
                {
                    if (Session[cSESSION_NAME_InputSearch] != null)
                    {
                        mFire.IsSetSession = true;
                        mFire.IsPostBack = false;
                        mFire.listMFire = repository.DoSearch(mFire);
                        Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Edit;
                        return RedirectToAction("ZC05032", "ZC050", new { Type = "Update", TableName = "M_FIRE_REPORT" });
                    }
                }
                else if (Request.Form["btnInsert"] != null)
                {
                    if (Session[cSESSION_NAME_InputSearch] != null)
                    {
                        mFire.IsSetSession = true;
                        mFire.IsPostBack = false;
                        mFire.listMFire = repository.DoSearch(mFire);
                        Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Edit;
                        return RedirectToAction("ZC05032", "ZC050", new { Type = "Update", TableName = "M_FIRE_REPORT" });
                    }
                }
                else if (Request.Form["btnDelete"] != null)
                {
                    if (Session[cSESSION_NAME_InputSearch] != null)
                    {
                        mFire.IsSetSession = true;
                        mFire.IsPostBack = false;
                        mFire.listMFire = repository.DoSearch(mFire);
                        Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Delete;
                        return RedirectToAction("ZC05032", "ZC050", new { Type = "Delete", TableName = "M_FIRE_REPORT" });
                    }
                }
                else if (Request.Form["btnBack_Click"] != null)
                {
                    Session["TBLFIRECLASS"] = string.Empty;
                    Session["TblFireListUpd"] = string.Empty;

                    TempData["Error"] = null;
                    TempData["Message"] = null;
                    mFire = DropDownFillFire(mFire);
                    return RedirectToAction("ZC01011", "ZC010");
                }

                mFire = DropDownFillFire(mFire);
            }

            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            mFire = DropDownFillFire(mFire);
            return View(mFire);
        }

        // DropDown List Loading for M_Fire Law Notification Master
        public M_FIRE DropDownFillFire(M_FIRE mFire)
        {
            try
            {
                // REPORT_NAME Condition
                List<SelectListItem> listReportNameCondition = new List<SelectListItem> { };
                new SearchConditionListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listReportNameCondition.Add(new SelectListItem
                   {
                       Value = x.Id.ToString(),
                       Text = x.Name
                   }));
                mFire.ddlType = listReportNameCondition;

                // REPORT_LOCATION Condition
                DataTable dtLocation = new DataTable();
                dtLocation.Columns.Add("Text");
                dtLocation.Columns.Add("Val");
                dtLocation.Rows.Add("保管場所", "0");
                dtLocation.Rows.Add("使用場所", "1");
                dtLocation.Rows.Add("全て", "2");
                mFire.ddlLocation = BindLookupValues(dtLocation);

                // STATUS List
                DataTable dtStatus = new DataTable();
                dtStatus.Columns.Add("Text");
                dtStatus.Columns.Add("Val");
                dtStatus.Rows.Add("使用中", "0");
                dtStatus.Rows.Add("削除済", "1");
                dtStatus.Rows.Add("全て", "2");
                mFire.ddlStatus = BindLookupValues(dtStatus);

                // Handling Office List
                List<M_COMMON> dtHandlingOffice = repository.InitHandlingOfficeComboBox();
                mFire.ddlReportPlace = BindLookupValues(dtHandlingOffice);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            return mFire;
        }

        public IEnumerable<SelectListItem> BindLookupValues(List<M_COMMON> mCommon)
        {
            IEnumerable<SelectListItem> selectlist = from s in mCommon
                                                     select new SelectListItem
                                                     {
                                                         Text = s.DISPLAY_VALUE,
                                                         Value = s.KEY_VALUE.ToString()
                                                     };
            return selectlist;
        }


        public IEnumerable<SelectListItem> BindLookupValues(DataTable res)
        {
            List<DataRow> list = res.AsEnumerable().ToList();
            IEnumerable<SelectListItem> selectlist = from s in list
                                                     select new SelectListItem
                                                     {
                                                         Text = s["Text"].ToString(),
                                                         Value = s["Val"].ToString()
                                                     };
            return selectlist;
        }

        [HttpGet]
        public ActionResult ZC05032(string Type, string TableName, string FireId, string IsPostBack, string mode)
        {
            M_FIRE mFire = new M_FIRE();
            tblFireList = new List<tblFireClass>();
            tblFire = new Table();
            mFire.labelMode = mode;
            try
            {
                if (Request.QueryString["IsPostBack"] == "False" || IsPostBack == "True")
                {
                    if (Type == "New")
                    {
                        if (Session["RedirectData"] != null)
                        {
                            mFire = (M_FIRE)Session["RedirectData"];
                            Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.New;
                            string type = "New";
                            tblFire = CreateFireDataUpd(type);
                            mFire.tblFireView = tblFire;
                            mFire.TYPE = "New";
                            mFire.mode = "0";
                            StringBuilder stringBuilder = new StringBuilder();
                            StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
                            HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
                            tblFire.RenderControl(ourHtmlWriter2);
                            mFire.tblFireData = ourHtmlWriter2.InnerWriter.ToString();
                            //Session["TBLFIRECLASS"] = tblFireList;
                        }
                        else
                        {
                            Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.New;
                            tblFire = CreateFireData();
                            mFire.tblFireView = tblFire;
                            mFire.TYPE = "New";
                            mFire.mode = "0";
                            StringBuilder stringBuilder = new StringBuilder();
                            StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
                            HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
                            tblFire.RenderControl(ourHtmlWriter2);
                            mFire.tblFireData = ourHtmlWriter2.InnerWriter.ToString();
                            Session["TBLFIRECLASS"] = tblFireList;
                        }
                    }
                    else if (Type == "Update")
                    {
                        if (Session["RedirectData"] != null)
                        {
                            mFire = (M_FIRE)Session["RedirectData"];
                            Session[SessionConst.MasterKeys] = FireId;
                            string type = "Update";
                            tblFire = CreateFireDataUpd(type);
                            mFire.tblFireView = tblFire;
                            mFire.TYPE = "Update";
                            Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Edit;

                            Session["Mode"] = "0";
                            if ((MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Edit ||
                                (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Reference ||
                                (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Delete)
                            {
                                //初期データセット
                                mFire.tblFireView = tblFire;
                                //mFire = SetFireData(mFire);

                                StringBuilder stringBuilder = new StringBuilder();
                                StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
                                HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
                                tblFire.RenderControl(ourHtmlWriter2);
                                mFire.mode = "0";
                                mFire.tblFireData = ourHtmlWriter2.InnerWriter.ToString();
                            }
                        }
                        else
                        {
                            Session[SessionConst.MasterKeys] = FireId;
                            string type = "Update";
                            tblFire = CreateFireDataUpd(type);
                            mFire.tblFireView = tblFire;
                            mFire.TYPE = "Update";
                            Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Edit;

                            Session["Mode"] = "0";
                            if ((MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Edit ||
                                (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Reference ||
                                (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Delete)
                            {
                                //初期データセット
                                mFire.tblFireView = tblFire;
                                mFire = repository.SetFireData(mFire);

                                StringBuilder stringBuilder = new StringBuilder();
                                StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
                                HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
                                tblFire.RenderControl(ourHtmlWriter2);
                                mFire.mode = "0";
                                mFire.tblFireData = ourHtmlWriter2.InnerWriter.ToString();
                            }
                        }
                    }
                    else if (Type == "RefRegister")
                    {
                        if (Session["RedirectData"] != null)
                        {
                            mFire = (M_FIRE)Session["RedirectData"];

                            Session[SessionConst.MasterKeys] = FireId;
                            string type = "Update";
                            tblFire = CreateFireDataUpd(type);
                            mFire.tblFireView = tblFire;
                            mFire.TYPE = "RefRegister";
                            Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Reference;

                            Session["Mode"] = "0";
                            if ((MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Edit ||
                                (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Reference ||
                                (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Delete)
                            {
                                //初期データセット
                                mFire.tblFireView = tblFire;
                                //mFire = SetFireData(mFire);

                                StringBuilder stringBuilder = new StringBuilder();
                                StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
                                HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
                                tblFire.RenderControl(ourHtmlWriter2);
                                mFire.mode = "0";
                                mFire.tblFireData = ourHtmlWriter2.InnerWriter.ToString();
                            }
                        }
                        else
                        {
                            Session[SessionConst.MasterKeys] = FireId;
                            string type = "Update";
                            tblFire = CreateFireDataUpd(type);
                            mFire.tblFireView = tblFire;
                            mFire.TYPE = "RefRegister";
                            Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Reference;

                            Session["Mode"] = "0";
                            if ((MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Edit ||
                                (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Reference ||
                                (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Delete)
                            {
                                //初期データセット
                                mFire.tblFireView = tblFire;
                                mFire = repository.SetFireData(mFire);

                                StringBuilder stringBuilder = new StringBuilder();
                                StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
                                HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
                                tblFire.RenderControl(ourHtmlWriter2);
                                mFire.mode = "0";
                                mFire.tblFireData = ourHtmlWriter2.InnerWriter.ToString();
                            }
                        }
                    }

                    else if (Type == "Delete")
                    {
                        Session[SessionConst.MasterKeys] = FireId;
                        string type = "Update";
                        tblFire = CreateFireDataUpd(type);
                        mFire.TYPE = "Delete";
                        Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Delete;

                        Session["Mode"] = "0";
                        if ((MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Edit ||
                            (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Reference ||
                            (MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Delete)
                        {
                            //初期データセット
                            mFire.tblFireView = tblFire;
                            mFire = repository.SetFireData(mFire);

                            StringBuilder stringBuilder = new StringBuilder();
                            StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
                            HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
                            tblFire.RenderControl(ourHtmlWriter2);
                            mFire.mode = "0";
                            mFire.tblFireData = ourHtmlWriter2.InnerWriter.ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }

            //mFire.mode = "0";
            mFire = DropDownFillFire(mFire);
            Session["CSVExport"] = mFire;
            return View(mFire);
        }

        [HttpPost]
        public ActionResult ZC05032(M_FIRE mFire)
        {
            M_FIRE m_FIRE = new M_FIRE();
            List<M_FIRE> listFireDetails;
            bool result = false;
            try
            {
                if (Request.Form["regFlag"] == "3" && Request.Form["btnBack_Click"] == null)
                {
                    Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.New;
                    Session[SessionConst.MasterKeys] = "";
                    if (Session["StorageLocId"] != null)
                    {
                        mFire.hdnLocationId1 = (string)Session["StorageLocId"];
                    }
                    else
                    {
                        mFire.hdnLocationId1 = Request.Form["Locationidval"];
                    }
                    if (Session["UsageLocId"] != null)
                    {
                        mFire.hdnLocationId2 = (string)Session["UsageLocId"];
                    }
                    else
                    {
                        mFire.hdnLocationId2 = Request.Form["UsageLocationidval"];
                    }
                    //mFire.mode = "0";
                    mFire.IsPostBack = true;
                    Session["MFire"] = mFire;
                    result = repository.DoSqlInsert(Common.GetLogonForm().Id, mFire);
                    TempData["Message"] = Common.GetMessage("I001");

                }


                else if (Request.Form["regFlag"] == "1" && Request.Form["btnBack_Click"] == null)
                {
                    if (Session["StorageLocId"] != null)
                    {
                        mFire.hdnLocationId1 = (string)Session["StorageLocId"];
                    }
                    else
                    {
                        mFire.hdnLocationId1 = Request.Form["Locationidval"];
                    }
                    if (Session["UsageLocId"] != null)
                    {
                        mFire.hdnLocationId2 = (string)Session["UsageLocId"];
                    }
                    else
                    {
                        mFire.hdnLocationId2 = Request.Form["UsageLocationidval"];
                    }
                    //mFire.mode = "0";
                    mFire.IsPostBack = true;
                    Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Edit;
                    Session["ZC05032UpdateData"] = mFire;
                    //Session[SessionConst.MasterKeys] = mFire.REPORT_ID;
                    //mFire = DropDownFill(m_FIRE);

                    result = repository.DoSqlUpdate(Common.GetLogonForm().Id);
                    TempData["Message"] = Common.GetMessage("I002");
                    //return RedirectToAction("ZC05031", "ZC050", new { TableName = "M_FIRE_REPORT", IsPostBack = "True" });

                }

                else if (Request.Form["regFlag"] == "2" && Request.Form["btnBack_Click"] == null)
                {
                    if (Session["StorageLocId"] != null)
                    {
                        mFire.hdnLocationId1 = (string)Session["StorageLocId"];
                    }
                    else
                    {
                        mFire.hdnLocationId1 = Request.Form["Locationidval"];
                    }
                    if (Session["UsageLocId"] != null)
                    {
                        mFire.hdnLocationId2 = (string)Session["UsageLocId"];
                    }
                    else
                    {
                        mFire.hdnLocationId2 = Request.Form["UsageLocationidval"];
                    }
                    Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Edit;
                    //mFire.mode = "0";
                    mFire.IsPostBack = true;
                    Session["ZC05032RefData"] = mFire;
                    //Session[SessionConst.MasterKeys] = mFire.REPORT_ID;

                    result = repository.DoSqlInsert(Common.GetLogonForm().Id, mFire);
                    //mFire = DropDownFill(m_FIRE);
                    TempData["Message"] = Common.GetMessage("I001");
                    //return RedirectToAction("ZC05031", "ZC050", new { TableName = "M_FIRE_REPORT", IsPostBack = "True" });
                }

                else if (mFire.TYPE == "Delete")
                {
                    Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Delete;
                    Session["ZC05032DelData"] = mFire;

                    result = repository.DoSqlDelete(Common.GetLogonForm().Id);
                    //mFire = DropDownFill(m_FIRE);
                    TempData["Message"] = Common.GetMessage("I003");
                    //return RedirectToAction("ZC05031", "ZC050", new { TableName = "M_FIRE_REPORT", IsPostBack = "True" });
                }
                else if (Request.Form["btnBack_Click"] != null)
                {
                    if (mFire.IsPostBack || mFire.labelMode == null)
                    {
                        Session["RedirectData"] = null;
                        Session["TBLFIRECLASS"] = null;
                        Session["TblFireListUpd"] = null;
                        return RedirectToAction("ZC05031", "ZC050", new { IsPostBack = true, TableName = "M_FIRE" });
                    }
                    else if (mFire.IsPostBack || mFire.labelMode == "1")
                    {
                        Session["RedirectData"] = null;
                        Session["TBLFIRECLASS"] = null;
                        Session["TblFireListUpd"] = null;
                        return RedirectToAction("ZC05031", "ZC050", new { IsPostBack = true, TableName = "M_FIRE" });
                    }
                    else
                    {
                        //mFire.mode = "0";

                        mFire.REPORT_ID = (string)Session[SessionConst.MasterKeys];
                        //mFire.REPORT_NAME = mFire.REPORT_NAME;
                        if (Session["StorageLocId"] != null)
                        {
                            mFire.hdnLocationId1 = (string)Session["StorageLocId"];
                        }
                        else
                        {
                            mFire.hdnLocationId1 = Request.Form["Locationidval"];
                        }
                        if (Session["UsageLocId"] != null)
                        {
                            mFire.hdnLocationId2 = (string)Session["UsageLocId"];
                        }
                        else
                        {
                            mFire.hdnLocationId2 = Request.Form["UsageLocationidval"];
                        }
                        List<string> lstLocation1 = new List<string>();
                        List<string> lstLocation2 = new List<string>();
                        string locname1 = Request.Form["lstLocation1"];
                        string[] lines = Regex.Split(locname1, "\r\n");
                        foreach (string a in lines)
                        {
                            lstLocation1.Add(a);
                            mFire.locname1 += a.ToString() + "\n";
                        }
                        mFire.lstLocation1 = lstLocation1;

                        string locname2 = Request.Form["lstLocation2"];
                        string[] lines1 = Regex.Split(locname2, "\r\n");
                        foreach (string a in lines1)
                        {
                            lstLocation1.Add(a);
                            mFire.locname2 += a.ToString() + "\n";
                        }
                        mFire.lstLocation2 = lstLocation2;
                        mFire.labelMode = "1";
                        Session["RedirectData"] = mFire;
                        return RedirectToAction("ZC05032", "ZC050", new { IsPostBack = false, TableName = "M_FIRE", Type = mFire.TYPE, fireId = mFire.REPORT_ID, mode = mFire.labelMode });
                    }
                }
                if ((Request.Form["regFlag"] == "3" && Request.Form["btnBack_Click"] == null) || (mFire.TYPE == "Delete") || (Request.Form["regFlag"] == "2" && Request.Form["btnBack_Click"] == null) || (Request.Form["regFlag"] == "1" && Request.Form["btnBack_Click"] == null))
                {
                    return RedirectToAction("ZC05031", "ZC050", new { TableName = "M_FIRE", IsPostBack = "True" });
                }

                if (Request.Form["btnCSVExport"] != null)
                {
                    if (Session["CSVExport"] != null)
                    {
                        List<ViewFireDetails> listViewFireDetails = new List<ViewFireDetails>();
                        Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.New;
                        DataTable _dt = GetFireDataType1();  //消防法
                        DataTable _dt2 = GetFireDataType2(); //指定可燃物

                        foreach (DataRow item in _dt.Rows)
                        {
                            ViewFireDetails viewFireDetails = new ViewFireDetails();

                            viewFireDetails.fireName = item["FIRE_NAME"].ToString();
                            viewFireDetails.fireSize = item["FIRE_SIZE"].ToString();
                            viewFireDetails.fireId = item["FIRE_ID"].ToString();
                            viewFireDetails.unitSize = item["UNIT_NAME"].ToString();

                            listViewFireDetails.Add(viewFireDetails);
                        }

                        foreach (DataRow item in _dt2.Rows)
                        {
                            ViewFireDetails viewFireDetails = new ViewFireDetails();

                            viewFireDetails.fireName = item["FIRE_NAME"].ToString();
                            viewFireDetails.fireSize = item["FIRE_SIZE"].ToString();
                            viewFireDetails.fireId = item["FIRE_ID"].ToString();
                            viewFireDetails.unitSize = item["UNIT_NAME"].ToString();

                            listViewFireDetails.Add(viewFireDetails);
                        }

                        mFire.listViewFireDetails = listViewFireDetails;
                        btnCsvOutput_Click(mFire);
                    }
                }

            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }

            tblFireList = new List<tblFireClass>();
            tblFire = new Table();
            tblFire = CreateFireData();
            StringBuilder stringBuilder = new StringBuilder();
            StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
            HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
            tblFire.RenderControl(ourHtmlWriter2);

            mFire.tblFireData = ourHtmlWriter2.InnerWriter.ToString();

            //Session["TBLFIRECLASS"] = tblFireList;
            mFire = DropDownFillFire(mFire);
            return View(mFire);
        }


        [HttpPost]
        public string GetStorageLocDetails()
        {
            var reg = "";
            try
            {
                Session["sessionStorageLocIds"] = null;
                var selectValue = (Dictionary<string, string>)null;
                if (Session[SessionConst.CategorySelectReturnValue] != null)
                {
                    selectValue = (Dictionary<string, string>)Session[SessionConst.CategorySelectReturnValue];

                    var regulationIds = selectValue.Values.ToList().ConvertAll(x => Convert.ToInt32(x)).AsEnumerable();

                    Session.Add("sessionStorageLocIds", regulationIds);
                }

            }
            catch (Exception ex)
            {
                reg = "";
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            return reg;

        }
        public JsonResult GetTblFireValue(string id, string txtvalue, string txtFireSizeTotal, string lblMultipleTotal, string regTypeId, string lblMultiple, string hdnFireSize)
        {
            JsonResult result = txt_TextChanged(txtvalue, id, txtFireSizeTotal, lblMultipleTotal, regTypeId, lblMultiple, hdnFireSize);
            return result;
        }

        /// <summary>
        /// 消防法側届出数量入力時
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private JsonResult txt_TextChanged(string txtFireSize, string id, string txtFireSizeTotal, string lblMultiTotal, string regTypeId, string lblMultipleValue, string hdnFireSizeValue)
        {

            //tblFireClass tblFireCls = new tblFireClass();
            List<string> result = new List<string>();
            decimal oldTxtValue = 0;
            decimal oldLblValue = 0;
            var txtId = Convert.ToUInt32(id);
            var tblId = 31;
            if ((MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.New)
            {
                tblFireClass tblFireCls = new tblFireClass();
                List<tblFireClass> tblFireClass = new List<tblFireClass>();
                if (Session["TBLFIRECLASS"] != null)
                {
                    tblFireClass = (List<tblFireClass>)Session["TBLFIRECLASS"];

                    foreach (var item in tblFireClass)
                    {
                        if (item.id == id.Trim())
                        {
                            string lblMultiple = null;
                            if (Common.IsNumber(txtFireSize) == false)
                            {
                                result.Add("Number");
                                result.Add(null);
                                result.Add(null);
                                result.Add(null);
                                return Json(result, JsonRequestBehavior.AllowGet);
                            }
                            oldTxtValue = Convert.ToDecimal(item.txtFireSize);
                            //oldLblValue = Convert.ToDecimal(item.);
                            if (txtFireSize.Trim() == "")
                            {

                            }
                            else
                            {
                                txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
                                item.txtFireSize = txtFireSize;
                                if (item.hdnFireSize == "")
                                {

                                }
                                else
                                {
                                    lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(item.hdnFireSize), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                }
                            }

                            decimal dblTotal = 0;
                            decimal dblTotal2 = 0;
                            decimal dblSubTxt = 0;
                            decimal dblSubLbl = 0;

                            if (txtFireSize != null && lblMultiple != null)
                            {
                                if (Common.IsNumber(txtFireSize) == true)
                                {
                                    //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
                                    if (txtFireSizeTotal != "")
                                    {
                                        if (regTypeId == item.hdnRegTypeId)
                                        {
                                            if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                            {
                                                dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                            }
                                            else
                                            {
                                                dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                            }
                                        }
                                    }
                                }

                                if (Common.IsNumber(lblMultiple) == true)
                                {
                                    if (lblMultipleValue == "")
                                    {
                                        oldLblValue = 0;
                                    }
                                    else
                                    {
                                        oldLblValue = Convert.ToDecimal(lblMultipleValue);
                                    }

                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
                                    if (lblMultiTotal != "")
                                    {
                                        if (regTypeId == item.hdnRegTypeId)
                                        {
                                            if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                            {
                                                dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                            }
                                            else
                                            {
                                                dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                            }
                                        }

                                        //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                    }
                                }
                            }
                            else
                            {
                                if (txtFireSizeTotal == "" && lblMultiTotal == "")
                                {
                                    txtFireSizeTotal = "0";
                                    lblMultiTotal = "0";
                                }
                                dblTotal = Convert.ToDecimal(txtFireSizeTotal);
                                dblTotal2 = Convert.ToDecimal(lblMultiTotal);
                            }

                            //}

                            string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
                            string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

                            //if (txtId >= tblId)
                            //{
                            //    //result.Add(lblMultiple);
                            //    result.Add(txtFireSize);
                            //    //result.Add(fireSizeTotal);
                            //    //result.Add(lblMultipleTotal);
                            //}
                            //else
                            //{
                            result.Add(lblMultiple);
                            result.Add(txtFireSize);
                            result.Add(fireSizeTotal);
                            result.Add(lblMultipleTotal);
                            // }

                            item.hdnRegTypeId = regTypeId;
                            item.hdnFireSize = hdnFireSizeValue;
                            item.mulFirSize = lblMultiple;
                            //tblFireClass.Add(item);
                            break;
                        }

                    }

                }
                Session["TBLFIRECLASS"] = tblFireClass;
            }
            else
            {
                tblFireClass tblFires = new tblFireClass();
                List<tblFireClass> tblFireClass = new List<tblFireClass>();
                int i;
                int val = Convert.ToInt32(id);
                if (txtFireSize == "")
                {
                    if (Session["TblFireListUpd"] != null)
                    {
                        tblFireClass = (List<tblFireClass>)Session["TblFireListUpd"];
                        for (i = 0; i < tblFireClass.Count; i++)
                        {
                            if (val < tblFireClass.Count)
                            {
                                if (tblFireClass[val].hdnRegTypeId == regTypeId.Trim())
                                {
                                    string lblMultiple = null;
                                    //if (Common.IsNumber(txtFireSize) == false)
                                    //{
                                    //    result.Add("Number");
                                    //    result.Add(null);
                                    //    result.Add(null);
                                    //    result.Add(null);
                                    //    return Json(result, JsonRequestBehavior.AllowGet);
                                    //}
                                    oldTxtValue = Convert.ToDecimal(tblFireClass[val].txtFireSize);

                                    if (txtFireSize.Trim() == "")
                                    {
                                        txtFireSize = "0";
                                        lblMultiple = "0";
                                    }
                                    else
                                    {
                                        txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
                                        tblFireClass[val].txtFireSize = txtFireSize;
                                        lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(tblFireClass[val].hdnFireSize), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                    }

                                    decimal dblTotal = 0;
                                    decimal dblTotal2 = 0;
                                    decimal dblSubTxt = 0;
                                    decimal dblSubLbl = 0;


                                    if (txtFireSize != null && lblMultiple != null)
                                    {
                                        if (Common.IsNumber(txtFireSize) == true)
                                        {
                                            //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                                            dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
                                            if (txtFireSizeTotal != "")
                                            {
                                                if (regTypeId == tblFireClass[val].hdnRegTypeId)
                                                {
                                                    if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                                    {
                                                        dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                        dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                    }
                                                    else
                                                    {
                                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                    }
                                                }
                                            }

                                            //if (txtFireSizeTotal != "")
                                            //{
                                            //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                            //}
                                        }

                                        if (Common.IsNumber(lblMultiple) == true)
                                        {
                                            if (lblMultipleValue == "")
                                            {
                                                oldLblValue = 0;
                                            }
                                            else
                                            {
                                                oldLblValue = Convert.ToDecimal(lblMultipleValue);
                                            }

                                            dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
                                            if (lblMultiTotal != "")
                                            {
                                                if (regTypeId == tblFireClass[val].hdnRegTypeId)
                                                {
                                                    if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                                    {
                                                        dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                        dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                    }
                                                    else
                                                    {
                                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                                    }
                                                }
                                                //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                            }
                                        }
                                    }
                                    //}

                                    string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
                                    string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));
                                    lblMultiple = string.Empty;
                                    txtFireSize = string.Empty;

                                    result.Add(lblMultiple);
                                    result.Add(txtFireSize);
                                    result.Add(fireSizeTotal);
                                    result.Add(lblMultipleTotal);


                                    tblFireClass[val].hdnRegTypeId = regTypeId;
                                    tblFireClass[val].hdnFireSize = hdnFireSizeValue;
                                    tblFireClass[val].mulFirSize = lblMultiple;
                                    //tblFires.hdnRegTypeId = regTypeId;
                                    //tblFires.hdnFireSize = hdnFireSizeValue;
                                    //tblFires.mulFirSize = lblMultiple;

                                    //tblFireClass.Add(tblFires);
                                    break;
                                }

                                else
                                {
                                    string lblMultiple = null;
                                    if (Common.IsNumber(txtFireSize) == false)
                                    {
                                        result.Add("Number");
                                        result.Add(null);
                                        result.Add(null);
                                        result.Add(null);
                                        return Json(result, JsonRequestBehavior.AllowGet);
                                    }
                                    foreach (var item in tblFireClass)
                                    {
                                        if (item.hdnRegTypeId == regTypeId)
                                        {
                                            oldTxtValue = Convert.ToDecimal(item.txtFireSize);
                                        }
                                    }


                                    if (txtFireSize.Trim() == "")
                                    {

                                    }
                                    else
                                    {
                                        txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
                                        tblFires.txtFireSize = txtFireSize;
                                        lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                    }

                                    decimal dblTotal = 0;
                                    decimal dblTotal2 = 0;
                                    decimal dblSubTxt = 0;
                                    decimal dblSubLbl = 0;


                                    if (txtFireSize != null && lblMultiple != null)
                                    {
                                        if (Common.IsNumber(txtFireSize) == true)
                                        {
                                            //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                                            dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
                                            if (txtFireSizeTotal != "")
                                            {
                                                foreach (var item in tblFireClass)
                                                {
                                                    if (regTypeId == item.hdnRegTypeId)
                                                    {
                                                        if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                                        {
                                                            dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                            dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                        }
                                                        //else
                                                        //{
                                                        //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                        //}
                                                    }
                                                    //else
                                                    //{
                                                    //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                    //}



                                                }
                                                dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);

                                            }

                                            //if (txtFireSizeTotal != "")
                                            //{
                                            //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                            //}
                                        }

                                        if (Common.IsNumber(lblMultiple) == true)
                                        {
                                            if (lblMultipleValue == "")
                                            {
                                                oldLblValue = 0;
                                            }
                                            else
                                            {
                                                oldLblValue = Convert.ToDecimal(lblMultipleValue);
                                            }

                                            dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
                                            if (lblMultiTotal != "")
                                            {
                                                foreach (var item in tblFireClass)
                                                {
                                                    if (regTypeId == item.hdnRegTypeId)
                                                    {
                                                        if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                                        {
                                                            dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                            dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                        }
                                                        //else
                                                        //{
                                                        //    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                                        //}
                                                    }
                                                }

                                                dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);

                                                //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                            }
                                        }
                                    }
                                    //}

                                    string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
                                    string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

                                    result.Add(lblMultiple);
                                    result.Add(txtFireSize);
                                    result.Add(fireSizeTotal);
                                    result.Add(lblMultipleTotal);

                                    tblFires.hdnRegTypeId = regTypeId;
                                    tblFires.hdnFireSize = hdnFireSizeValue;
                                    tblFires.mulFirSize = lblMultiple;

                                    tblFireClass.Add(tblFires);
                                    break;
                                }
                            }


                            else
                            {
                                string lblMultiple = null;
                                if (Common.IsNumber(txtFireSize) == false)
                                {
                                    result.Add("Number");
                                    result.Add(null);
                                    result.Add(null);
                                    result.Add(null);
                                    return Json(result, JsonRequestBehavior.AllowGet);
                                }
                                oldTxtValue = Convert.ToDecimal(tblFireClass[i].txtFireSize);

                                if (txtFireSize.Trim() == "")
                                {

                                }
                                else
                                {
                                    txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
                                    tblFires.txtFireSize = txtFireSize;
                                    lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                }

                                decimal dblTotal = 0;
                                decimal dblTotal2 = 0;
                                decimal dblSubTxt = 0;
                                decimal dblSubLbl = 0;


                                if (txtFireSize != null && lblMultiple != null)
                                {
                                    if (Common.IsNumber(txtFireSize) == true)
                                    {
                                        //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
                                        if (txtFireSizeTotal != "")
                                        {
                                            if (regTypeId == tblFireClass[i].hdnRegTypeId)
                                            {
                                                if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                                {
                                                    dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                    dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                }
                                                else
                                                {
                                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                }
                                            }
                                            else
                                            {
                                                dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                            }
                                        }

                                        //if (txtFireSizeTotal != "")
                                        //{
                                        //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                        //}
                                    }

                                    if (Common.IsNumber(lblMultiple) == true)
                                    {
                                        if (lblMultipleValue == "")
                                        {
                                            oldLblValue = 0;
                                        }
                                        else
                                        {
                                            oldLblValue = Convert.ToDecimal(lblMultipleValue);
                                        }

                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
                                        if (lblMultiTotal != "")
                                        {
                                            if (regTypeId == tblFireClass[i].hdnRegTypeId)
                                            {
                                                if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                                {
                                                    dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                    dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                }
                                                else
                                                {
                                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                                }
                                            }
                                            else
                                            {
                                                dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                            }
                                            //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                        }
                                    }
                                }
                                //}

                                string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
                                string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

                                result.Add(lblMultiple);
                                result.Add(txtFireSize);
                                result.Add(fireSizeTotal);
                                result.Add(lblMultipleTotal);

                                tblFires.hdnRegTypeId = regTypeId;
                                tblFires.hdnFireSize = hdnFireSizeValue;
                                tblFires.mulFirSize = lblMultiple;

                                tblFireClass.Add(tblFires);
                                break;
                            }

                        }

                    }
                    return Json(result, JsonRequestBehavior.AllowGet);

                }

                if (Session["TblFireListUpd"] != null)
                {
                    tblFireClass = (List<tblFireClass>)Session["TblFireListUpd"];
                    for (i = 0; i < tblFireClass.Count; i++)
                    {
                        if (val < tblFireClass.Count)
                        {
                            if (tblFireClass[val].hdnRegTypeId == regTypeId.Trim())
                            {
                                string lblMultiple = null;
                                if (Common.IsNumber(txtFireSize) == false)
                                {
                                    result.Add("Number");
                                    result.Add(null);
                                    result.Add(null);
                                    result.Add(null);
                                    return Json(result, JsonRequestBehavior.AllowGet);
                                }
                                oldTxtValue = Convert.ToDecimal(tblFireClass[val].txtFireSize);

                                if (txtFireSize.Trim() == "")
                                {

                                }
                                else
                                {
                                    txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
                                    tblFireClass[val].txtFireSize = txtFireSize;
                                    lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(tblFireClass[val].hdnFireSize), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                }

                                decimal dblTotal = 0;
                                decimal dblTotal2 = 0;
                                decimal dblSubTxt = 0;
                                decimal dblSubLbl = 0;


                                if (txtFireSize != null && lblMultiple != null)
                                {
                                    if (Common.IsNumber(txtFireSize) == true)
                                    {
                                        //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
                                        if (txtFireSizeTotal != "")
                                        {
                                            if (regTypeId == tblFireClass[val].hdnRegTypeId)
                                            {
                                                if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                                {
                                                    if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                                    {
                                                        dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                        dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                    }
                                                    else
                                                    {
                                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                    }
                                                    //dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                    //dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                }
                                                else
                                                {
                                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                }
                                            }
                                        }

                                        //if (txtFireSizeTotal != "")
                                        //{
                                        //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                        //}
                                    }

                                    if (Common.IsNumber(lblMultiple) == true)
                                    {
                                        if (lblMultipleValue == "")
                                        {
                                            oldLblValue = 0;
                                        }
                                        else
                                        {
                                            oldLblValue = Convert.ToDecimal(lblMultipleValue);
                                        }

                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
                                        if (lblMultiTotal != "")
                                        {
                                            if (regTypeId == tblFireClass[val].hdnRegTypeId)
                                            {
                                                if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                                {
                                                    if (Convert.ToDecimal(oldLblValue) < Convert.ToDecimal(lblMultiple))
                                                    {
                                                        dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                        dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                    }
                                                    else
                                                    {
                                                        dblSubLbl = Convert.ToDecimal(lblMultiple) - Convert.ToDecimal(oldLblValue);
                                                        dblTotal2 = Convert.ToDecimal(lblMultiTotal) + dblSubLbl;
                                                    }
                                                    //dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                    //dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                }
                                                else
                                                {
                                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                                }
                                            }
                                            //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                        }
                                    }
                                }
                                //}

                                string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
                                string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

                                result.Add(lblMultiple);
                                result.Add(txtFireSize);
                                result.Add(fireSizeTotal);
                                result.Add(lblMultipleTotal);


                                tblFireClass[val].hdnRegTypeId = regTypeId;
                                tblFireClass[val].hdnFireSize = hdnFireSizeValue;
                                tblFireClass[val].mulFirSize = lblMultiple;
                                //tblFires.hdnRegTypeId = regTypeId;
                                //tblFires.hdnFireSize = hdnFireSizeValue;
                                //tblFires.mulFirSize = lblMultiple;

                                //tblFireClass.Add(tblFires);
                                break;
                            }

                            else
                            {
                                string lblMultiple = null;
                                if (Common.IsNumber(txtFireSize) == false)
                                {
                                    result.Add("Number");
                                    result.Add(null);
                                    result.Add(null);
                                    result.Add(null);
                                    return Json(result, JsonRequestBehavior.AllowGet);
                                }
                                foreach (var item in tblFireClass)
                                {
                                    if (item.hdnRegTypeId == regTypeId)
                                    {
                                        oldTxtValue = Convert.ToDecimal(item.txtFireSize);
                                    }
                                }


                                if (txtFireSize.Trim() == "")
                                {

                                }
                                else
                                {
                                    txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
                                    tblFires.txtFireSize = txtFireSize;
                                    lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                }

                                decimal dblTotal = 0;
                                decimal dblTotal2 = 0;
                                decimal dblSubTxt = 0;
                                decimal dblSubLbl = 0;


                                if (txtFireSize != null && lblMultiple != null)
                                {
                                    if (Common.IsNumber(txtFireSize) == true)
                                    {
                                        //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
                                        if (txtFireSizeTotal != "")
                                        {
                                            foreach (var item in tblFireClass)
                                            {
                                                if (regTypeId == item.hdnRegTypeId)
                                                {
                                                    //if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                                    //{
                                                    //    dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                    //    dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                    //}

                                                    if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                                    {
                                                        dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                        dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                    }
                                                    else
                                                    {
                                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                    }
                                                    //else
                                                    //{
                                                    //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                    //}
                                                }
                                                //else
                                                //{
                                                //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                //}



                                            }
                                            //dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);

                                        }

                                        //if (txtFireSizeTotal != "")
                                        //{
                                        //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                        //}
                                    }

                                    if (Common.IsNumber(lblMultiple) == true)
                                    {
                                        if (lblMultipleValue == "")
                                        {
                                            oldLblValue = 0;
                                        }
                                        else
                                        {
                                            oldLblValue = Convert.ToDecimal(lblMultipleValue);
                                        }

                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
                                        //if (lblMultiTotal != "")
                                        //{
                                        //    //foreach (var item in tblFireClass)
                                        //    //{
                                        //        if (regTypeId == item.hdnRegTypeId)
                                        //        {
                                        //            if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                        //            {
                                        //                if (Convert.ToDecimal(oldLblValue) < Convert.ToDecimal(lblMultiple))
                                        //                {
                                        //                    dblSubLbl = Convert.ToDecimal(lblMultiple) - Convert.ToDecimal(oldLblValue);

                                        //                    dblTotal2 = Convert.ToDecimal(lblMultiTotal) + dblSubLbl;
                                        //                }
                                        //                else
                                        //                {
                                        //                    dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                        //                    dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                        //                }
                                        //                //dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                        //                //dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                        //            }
                                        //            //else
                                        //            //{
                                        //            //    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                        //            //}
                                        //        }
                                        //    //}

                                        //    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);

                                        //    //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                        //}

                                        if (lblMultiTotal != "")
                                        {
                                            if (regTypeId == tblFireClass[i].hdnRegTypeId)
                                            {
                                                if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                                {
                                                    if (Convert.ToDecimal(oldLblValue) < Convert.ToDecimal(lblMultiple))
                                                    {
                                                        dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                        dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                    }
                                                    else
                                                    {
                                                        dblSubLbl = Convert.ToDecimal(lblMultiple) - Convert.ToDecimal(oldLblValue);
                                                        dblTotal2 = Convert.ToDecimal(lblMultiTotal) + dblSubLbl;
                                                    }

                                                    //dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                    //dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                }
                                                else
                                                {
                                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                                }
                                            }
                                            else
                                            {
                                                for (int a = 0; a < tblFireClass.Count; a++)
                                                {
                                                    if (regTypeId == tblFireClass[a].hdnRegTypeId)
                                                    {
                                                        if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                                        {
                                                            dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                            dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                        }
                                                        else
                                                        {
                                                            dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                                        }
                                                    }
                                                }
                                            }
                                            //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                        }
                                    }
                                }
                                //}

                                string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
                                string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

                                result.Add(lblMultiple);
                                result.Add(txtFireSize);
                                result.Add(fireSizeTotal);
                                result.Add(lblMultipleTotal);

                                tblFires.hdnRegTypeId = regTypeId;
                                tblFires.hdnFireSize = hdnFireSizeValue;
                                tblFires.mulFirSize = lblMultiple;

                                tblFireClass.Add(tblFires);
                                break;
                            }
                        }




                        else
                        {
                            string lblMultiple = null;
                            if (Common.IsNumber(txtFireSize) == false)
                            {
                                result.Add("Number");
                                result.Add(null);
                                result.Add(null);
                                result.Add(null);
                                return Json(result, JsonRequestBehavior.AllowGet);
                            }
                            for (int a = 0; a < tblFireClass.Count; a++)
                            {
                                if (regTypeId == tblFireClass[a].hdnRegTypeId)
                                {
                                    oldTxtValue = Convert.ToDecimal(tblFireClass[a].txtFireSize);
                                }
                            }

                            if (txtFireSize.Trim() == "")
                            {

                            }
                            else
                            {
                                txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
                                tblFires.txtFireSize = txtFireSize;
                                if (hdnFireSizeValue == "")
                                {

                                }
                                else
                                {
                                    lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                }
                            }

                            decimal dblTotal = 0;
                            decimal dblTotal2 = 0;
                            decimal dblSubTxt = 0;
                            decimal dblSubLbl = 0;


                            if (txtFireSize != null && lblMultiple != null)
                            {
                                if (Common.IsNumber(txtFireSize) == true)
                                {
                                    //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
                                    if (txtFireSizeTotal != "")
                                    {
                                        if (regTypeId == tblFireClass[i].hdnRegTypeId)
                                        {
                                            if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                            {
                                                if (Convert.ToDecimal(oldTxtValue) < Convert.ToDecimal(txtFireSize))
                                                {
                                                    dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                    dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                }
                                                else
                                                {
                                                    dblSubTxt = Convert.ToDecimal(txtFireSize) - Convert.ToDecimal(oldTxtValue);
                                                    dblTotal = Convert.ToDecimal(txtFireSizeTotal) + dblSubTxt;
                                                }

                                                //dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                //dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                            }
                                            else
                                            {
                                                dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                            }
                                        }
                                        else
                                        {
                                            for (int a = 0; a < tblFireClass.Count; a++)
                                            {
                                                if (regTypeId == tblFireClass[a].hdnRegTypeId)
                                                {
                                                    if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                                    {
                                                        if (Convert.ToDecimal(oldTxtValue) < Convert.ToDecimal(txtFireSize))
                                                        {
                                                            dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                            dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                        }
                                                        else
                                                        {
                                                            dblSubTxt = Convert.ToDecimal(txtFireSize) - Convert.ToDecimal(oldTxtValue);
                                                            dblTotal = Convert.ToDecimal(txtFireSizeTotal) + dblSubTxt;
                                                        }

                                                    }
                                                    else
                                                    {
                                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                    }
                                                }
                                            }

                                        }
                                    }

                                    //if (txtFireSizeTotal != "")
                                    //{
                                    //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                    //}
                                }

                                if (Common.IsNumber(lblMultiple) == true)
                                {
                                    if (lblMultipleValue == "")
                                    {
                                        oldLblValue = 0;
                                    }
                                    else
                                    {
                                        oldLblValue = Convert.ToDecimal(lblMultipleValue);
                                    }

                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
                                    if (lblMultiTotal != "")
                                    {
                                        if (regTypeId == tblFireClass[i].hdnRegTypeId)
                                        {
                                            if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                            {
                                                if (Convert.ToDecimal(oldLblValue) < Convert.ToDecimal(lblMultiple))
                                                {
                                                    dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                    dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                }
                                                else
                                                {
                                                    dblSubLbl = Convert.ToDecimal(lblMultiple) - Convert.ToDecimal(oldLblValue);
                                                    dblTotal2 = Convert.ToDecimal(lblMultiTotal) + dblSubLbl;
                                                }

                                                //dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                //dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                            }
                                            else
                                            {
                                                dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                            }
                                        }
                                        else
                                        {
                                            for (int a = 0; a < tblFireClass.Count; a++)
                                            {
                                                if (regTypeId == tblFireClass[a].hdnRegTypeId)
                                                {
                                                    if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                                    {
                                                        dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                        dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                    }
                                                    else
                                                    {
                                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                                    }
                                                }
                                            }
                                        }
                                        //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                    }
                                }
                            }
                            else
                            {
                                if (txtFireSizeTotal == "" && lblMultiTotal == "")
                                {
                                    txtFireSizeTotal = "0";
                                    lblMultiTotal = "0";
                                }
                                else
                                {
                                    if (Common.IsNumber(txtFireSize) == true)
                                    {
                                        //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                                        dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
                                        if (txtFireSizeTotal != "")
                                        {
                                            if (regTypeId == tblFireClass[i].hdnRegTypeId)
                                            {
                                                if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                                {
                                                    dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                                    dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                                }
                                                else
                                                {
                                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                                }
                                            }
                                            else
                                            {
                                                dblTotal = Convert.ToDecimal(txtFireSizeTotal);
                                                dblTotal2 = Convert.ToDecimal(lblMultiTotal);
                                            }
                                        }

                                        //if (txtFireSizeTotal != "")
                                        //{
                                        //    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                        //}
                                    }

                                    if (Common.IsNumber(lblMultiple) == true)
                                    {
                                        if (lblMultipleValue == "")
                                        {
                                            oldLblValue = 0;
                                        }
                                        else
                                        {
                                            oldLblValue = Convert.ToDecimal(lblMultipleValue);
                                        }

                                        dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
                                        if (lblMultiTotal != "")
                                        {
                                            if (regTypeId == tblFireClass[i].hdnRegTypeId)
                                            {
                                                if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                                {
                                                    dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                                    dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                                }
                                                else
                                                {
                                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                                }
                                            }
                                            else
                                            {
                                                dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                            }
                                            //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                        }
                                    }

                                }
                                //dblTotal = Convert.ToDecimal(txtFireSizeTotal);
                                //dblTotal2 = Convert.ToDecimal(lblMultiTotal);
                            }
                            //}

                            string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
                            string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

                            result.Add(lblMultiple);
                            result.Add(txtFireSize);
                            result.Add(fireSizeTotal);
                            result.Add(lblMultipleTotal);

                            if (regTypeId == tblFireClass[i].hdnRegTypeId)
                            {
                                tblFireClass.RemoveAt(i);
                            }
                            else
                            {
                                for (int a = 0; a < tblFireClass.Count; a++)
                                {
                                    if (regTypeId == tblFireClass[a].hdnRegTypeId)
                                    {
                                        tblFireClass.RemoveAt(a);
                                    }
                                }
                            }



                            //    tblFireClass[val].hdnRegTypeId = regTypeId;
                            //tblFireClass[val].hdnFireSize = hdnFireSizeValue;
                            //tblFireClass[val].mulFirSize = lblMultiple;



                            tblFires.hdnRegTypeId = regTypeId;
                            tblFires.hdnFireSize = hdnFireSizeValue;
                            tblFires.mulFirSize = lblMultiple;

                            tblFireClass.Add(tblFires);
                            break;
                        }

                    }
                }
                else
                {
                    string lblMultiple = null;
                    if (Common.IsNumber(txtFireSize) == false)
                    {
                        result.Add("Number");
                        result.Add(null);
                        result.Add(null);
                        result.Add(null);
                        return Json(result, JsonRequestBehavior.AllowGet);
                    }
                    oldTxtValue = Convert.ToDecimal(txtFireSize);
                    //oldLblValue = Convert.ToDecimal(item.);
                    if (txtFireSize.Trim() == "")
                    {

                    }
                    else
                    {
                        txtFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize), 4).ToString("F4"));
                        tblFires.txtFireSize = txtFireSize;
                        lblMultiple = Convert.ToString(Math.Round(Convert.ToDecimal(txtFireSize) / Convert.ToDecimal(hdnFireSizeValue), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                    }

                    decimal dblTotal = 0;
                    decimal dblTotal2 = 0;
                    decimal dblSubTxt = 0;
                    decimal dblSubLbl = 0;

                    if (txtFireSize != null && lblMultiple != null)
                    {
                        if (Common.IsNumber(txtFireSize) == true)
                        {
                            //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                            dblTotal = dblTotal + Convert.ToDecimal(txtFireSize);
                            if (txtFireSizeTotal != "")
                            {

                                if (txtFireSize != oldTxtValue.ToString() && oldTxtValue != 0)
                                {
                                    dblSubTxt = Convert.ToDecimal(oldTxtValue) - Convert.ToDecimal(txtFireSize);
                                    dblTotal = Convert.ToDecimal(txtFireSizeTotal) - dblSubTxt;
                                }
                                else
                                {
                                    dblTotal = dblTotal + Convert.ToDecimal(txtFireSizeTotal);
                                }
                                //}
                            }
                        }

                        if (Common.IsNumber(lblMultiple) == true)
                        {
                            if (lblMultipleValue == "")
                            {
                                oldLblValue = 0;
                            }
                            else
                            {
                                oldLblValue = Convert.ToDecimal(lblMultipleValue);
                            }

                            dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiple);
                            if (lblMultiTotal != "")
                            {

                                if (lblMultiple != oldLblValue.ToString() && oldLblValue != 0)
                                {
                                    dblSubLbl = Convert.ToDecimal(oldLblValue) - Convert.ToDecimal(lblMultiple);
                                    dblTotal2 = Convert.ToDecimal(lblMultiTotal) - dblSubLbl;
                                }
                                else
                                {
                                    dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                                }

                                //dblTotal2 = dblTotal2 + Convert.ToDecimal(lblMultiTotal);
                            }
                        }
                    }
                    //}

                    string fireSizeTotal = Convert.ToString(dblTotal.ToString("F4"));
                    string lblMultipleTotal = Convert.ToString(dblTotal2.ToString("F4"));

                    result.Add(lblMultiple);
                    result.Add(txtFireSize);
                    result.Add(fireSizeTotal);
                    result.Add(lblMultipleTotal);

                    tblFires.hdnRegTypeId = regTypeId;
                    tblFires.hdnFireSize = hdnFireSizeValue;
                    tblFires.mulFirSize = lblMultiple;


                    tblFireClass.Add(tblFires);

                }
                Session["TblFireListUpd"] = tblFireClass;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019
        /// Description : This  Method is for get loction name
        /// <summary>
        public string GetStorageLocName(string locId)
        {
            Session["StorageLocId"] = locId;
            StringBuilder stbSql = new StringBuilder();
            string locName = null;
            try
            {
                if (locId != null)
                {
                    locId = locId.Replace("\"", string.Empty);
                }

                // SELECT SQL生成
                stbSql.Append(" SELECT V.LOC_NAME ");
                stbSql.Append(" FROM ");
                stbSql.Append(" V_LOCATION V");
                stbSql.Append(" WHERE LOC_ID IN (" + locId + ")");
                DynamicParameters _params = new DynamicParameters();
                _params.Add("LOC_ID", locId);

                List<string> locname = DbUtil.Select<string>(stbSql.ToString(), _params);
                if (locname.Count > 0)
                {
                    foreach (var loc in locname)
                    {
                        locName += loc.ToString() + "\n";
                    }
                    return locName;
                }

            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            return null;
        }

        public string GetUsageLocName(string locId)
        {
            Session["UsageLocId"] = locId;
            StringBuilder stbSql = new StringBuilder();
            string locName = null;
            try
            {
                if (locId != null)
                {
                    locId = locId.Replace("\"", string.Empty);
                }

                // SELECT SQL生成
                stbSql.Append(" SELECT V.LOC_NAME ");
                stbSql.Append(" FROM ");
                stbSql.Append(" V_LOCATION V");
                stbSql.Append(" WHERE LOC_ID IN (" + locId + ")");
                DynamicParameters _params = new DynamicParameters();
                _params.Add("LOC_ID", locId);

                List<string> locname = DbUtil.Select<string>(stbSql.ToString(), _params);
                if (locname.Count > 0)
                {
                    foreach (var loc in locname)
                    {
                        locName += loc.ToString() + "\n";
                    }
                    return locName;
                }

            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                //LogUtil.OutErrLog(ProgramId, ex.StackTrace.ToString());
                _logoututil.OutErrLog(ProgramId, ex.StackTrace.ToString());
            }
            return null;
        }

        /// <summary>
        /// 消防法危険物合計処理
        /// </summary>
        private M_FIRE FireTotal(M_FIRE mFire)
        {
            Label _lblMultipleTotal = (Label)tblFire.FindControl("lblMultipleTotal");
            Label _txtFireSizeTotal = (Label)tblFire.FindControl("txtFireSizeTotal");


            decimal dblTotal = 0;
            decimal dblTotal2 = 0;

            for (int i = 0; i < mFire.tblFireView.Rows.Count - 1; i++)
            {
                TextBox _txt = (TextBox)tblFire.FindControl("txtFireSize" + i);
                Label _lbl = (Label)tblFire.FindControl("lblMultiple" + i);
                HiddenField _hdn = (HiddenField)tblFire.FindControl("hdnFactor" + i);

                if (_txt != null && _lbl != null)
                {
                    if (Common.IsNumber(_txt.Text) == true)
                    {
                        //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                        dblTotal = dblTotal + Convert.ToDecimal(_txt.Text);
                    }

                    if (Common.IsNumber(_lbl.Text) == true)
                    {
                        dblTotal2 = dblTotal2 + Convert.ToDecimal(_lbl.Text);
                    }
                }
            }

            _txtFireSizeTotal.Text = Convert.ToString(dblTotal.ToString("F4"));
            _lblMultipleTotal.Text = Convert.ToString(dblTotal2.ToString("F4"));
            return mFire;
        }

        /// <summary>
        /// 入力チェックを行います。
        /// </summary>
        /// <returns>true：正常 / false：異常</returns>
        [HttpPost]
        public string InputCheck(M_FIRE mFire)
        {
            string errorMessages = null;
            string errorMsg = null;
            DataCheckBase _DataCheck = new DataCheckBase();

            //届出名称
            if (Request.Form["regFlag"] != "1")
            {
                //if ((MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.New)
                //{
                errorMessages = _DataCheck.CheckReportName(mFire.REPORT_NAME, mFire.REPORT_YMD, "届出名称", true, "", new string[] { "txtReportName", "0" });

                if (errorMessages != null)
                {
                    return errorMsg += errorMessages;
                }

                //}
            }
            else if (Request.Form["regFlag"] == "1")
            //if ((MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Edit)
            {
                errorMessages = _DataCheck.CheckReportName(mFire.REPORT_NAME, mFire.REPORT_YMD, "届出名称", true, Convert.ToString(Session[SessionConst.MasterKeys]), new string[] { "txtReportName", "0" });

                if (errorMessages != null)
                {
                    return errorMsg += errorMessages;
                }

            }
            else if (Request.Form["regFlag"] == "2")
            //if ((MasterMaintenance.MaintenanceType)Session[SessionConst.MaintenanceType] == MasterMaintenance.MaintenanceType.Reference)
            {
                errorMessages = _DataCheck.CheckReportName(mFire.REPORT_NAME, mFire.REPORT_YMD, "届出名称", true, "", new string[] { "txtReportName", "0" });
                if (errorMessages != null)
                {
                    return errorMsg += errorMessages;
                }

            }

            //届出年月日
            errorMessages = _DataCheck.CheckReportYMD(mFire.REPORT_YMD, "届出年月日", true, new string[] { "txtReportYMD", "0" });

            if (errorMessages != null)
            {
                return errorMsg += errorMessages;
            }


            //届出先
            errorMessages = _DataCheck.CheckReportSubmit(mFire.REPORT_SUBMIT, "届出先", true, new string[] { "txtReportSubmit", "0" });

            if (errorMessages != null)
            {
                return errorMsg += errorMessages;
            }


            if (mFire.lstLocation1 != null || mFire.lstLocation1.All(x => string.IsNullOrWhiteSpace(x)))
            {
                mFire.lstLocation1 = mFire.lstLocation1.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();
                mFire.lstLocation2 = mFire.lstLocation2.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();
                //保管場所/使用場所
                errorMessages = _DataCheck.CheckReportLocation(mFire.lstLocation1.Count, mFire.lstLocation2.Count, "保管場所/使用場所", new string[] { "lstLocation1", "0" });

                if (errorMessages != null)
                {
                    return errorMsg += errorMessages;
                }
            }

            //届出数量チェック
            bool checkInput = false;


            List<tblFireClass> tblFire = new List<tblFireClass>();

            if (Session["TblFireListUpd"] != null)
            {

                tblFire = (List<tblFireClass>)Session["TblFireListUpd"];
                for (int i = 0; i < tblFire.Count; i++)
                {
                    if (tblFire[i].txtFireSize != null)
                    {
                        if (Common.IsNumber(tblFire[i].txtFireSize) == false && tblFire[i].txtFireSize != "")
                        {
                            //Common.MsgBox(this.Page, Common.GetMessage("C010", "届出数量"), new string[] { _txt.ID, "0" });

                            errorMessages = Common.GetMessage("C010", "届出数量");
                            if (errorMessages != null)
                            {
                                return errorMsg += errorMessages;
                            }
                        }

                        if (tblFire[i].txtFireSize != "")
                        {
                            if (Convert.ToDouble(tblFire[i].txtFireSize) > Convert.ToDouble(999999.9999))
                            {
                                //Common.MsgBox(this.Page, Common.GetMessage("C056", new string[] { "届出数量", "999999.9999以内" }), new string[] { _txt.ID, "0" });
                                errorMessages = Common.GetMessage("C056", new string[] { "届出数量", "999999.9999以内" });
                                if (errorMessages != null)
                                {
                                    return errorMsg += errorMessages;
                                }
                            }
                        }
                        if (tblFire[i].txtFireSize != "")
                        {
                            checkInput = true;
                        }
                    }
                }
            }
            else if (Session["TBLFIRECLASS"] != null)
            {

                tblFire = (List<tblFireClass>)Session["TBLFIRECLASS"];
                for (int i = 0; i < tblFire.Count; i++)
                {
                    if (tblFire[i].txtFireSize != null)
                    {
                        if (Common.IsNumber(tblFire[i].txtFireSize) == false && tblFire[i].txtFireSize != "")
                        {
                            //Common.MsgBox(this.Page, Common.GetMessage("C010", "届出数量"), new string[] { _txt.ID, "0" });

                            errorMessages = Common.GetMessage("C010", "届出数量");
                            if (errorMessages != null)
                            {
                                return errorMsg += errorMessages;
                            }
                        }

                        if (tblFire[i].txtFireSize != "")
                        {
                            if (Convert.ToDouble(tblFire[i].txtFireSize) > Convert.ToDouble(999999.9999))
                            {
                                //Common.MsgBox(this.Page, Common.GetMessage("C056", new string[] { "届出数量", "999999.9999以内" }), new string[] { _txt.ID, "0" });
                                errorMessages = Common.GetMessage("C056", new string[] { "届出数量", "999999.9999以内" });
                                if (errorMessages != null)
                                {
                                    return errorMsg += errorMessages;
                                }
                            }
                        }
                        if (tblFire[i].txtFireSize != "")
                        {
                            checkInput = true;
                        }
                    }
                }
            }
            if (mFire.REPORT_PLACE_KEY != "少量危険物未満")
            {
                if (checkInput == false)
                {
                    errorMessages = Common.GetMessage("C001", "届出数量");
                    if (errorMessages != null)
                    {
                        return errorMsg += errorMessages;
                    }
                }
            }

            return errorMsg;
        }


        /// <summary>
        /// 更新処理を行います。
        /// </summary>
        private void DoUpdate()
        {
            try
            {
                // 登録情報設定
                //var objClsTranfser = new clsTransfer();
                //SetDataDisplayToInfoClass(ref objClsTranfser);

                // セッション情報設定
                //Session[ZC05032.cSESSION_NAME_Trans] = objClsTranfser;
                Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.Edit;
                Session[SessionConst.MasterKeys] = Session[SessionConst.MasterKeys];

                Session["Mode"] = "2";
                //SetMode(Convert.ToString(Session["Mode"]));

            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
                throw ex;
            }
        }

        /// 新規登録処理を行います。
        /// </summary>
        private void DoInsert()
        {
            try
            {
                // 登録情報設定
                //var objClsTranfser = new clsTransfer();
                //SetDataDisplayToInfoClass(ref objClsTranfser);

                // セッション情報設定
                //Session[ZC05032.cSESSION_NAME_Trans] = objClsTranfser;
                //Session[SessionConst.MaintenanceType] = MasterMaintenance.MaintenanceType.New;
                Session[SessionConst.MasterKeys] = Session[SessionConst.MasterKeys];

                Session["Mode"] = "2";
                //SetMode(Convert.ToString(Session["Mode"]));
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
                throw ex;
            }
        }





        /// <summary>
        /// CSV出力
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //[HttpPost]
        public bool btnCsvOutput_Click(M_FIRE mFire)
        {
            string replaceCsvFileName;

            if (mFire.listViewFireDetails != null && mFire.listViewFireDetails.Count > 0)
            {
                var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                //var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                if (mFire.listViewFireDetails != null & mFire.listViewFireDetails.Count > 0)
                {
                    using (var csv = new StreamWriter(csvFileFullPath, false, Encoding.GetEncoding("shift_jis")))
                    {
                        csv.Write(string.Empty);
                        csv.Close();
                    }
                }
                ChemMgmt.Classes.Extensions.GridViewExtension.FireOutputCSV(mFire.listViewFireDetails, csvFileFullPath, mFire);


                if (mFire.REPORT_NAME == string.Empty)
                {
                    replaceCsvFileName = OUTLIST_FILENAME.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                }
                else
                {
                    replaceCsvFileName = mFire.REPORT_NAME + ".csv".Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                }

                BaseCommon.Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
            }
            return true;
        }






        /// DROP-DOWN LIST FILL METHOD
        public M_MASTER DropDownFillRegulation(M_MASTER master)
        {
            try
            {
                // In Use Condition
                List<SelectListItem> listCSNCondition = new List<SelectListItem> { };
                new ChemMgmt.Classes.UsageStatusListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listCSNCondition.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                master.LookUpCMICondition = listCSNCondition;

                List<SelectListItem> listRegNameCondition = new List<SelectListItem> { };
                new SearchConditionListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listRegNameCondition.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                master.LookUpRegNameCondition = listRegNameCondition;

                List<SelectListItem> listDelStatus = new List<SelectListItem> { };
                new ChemMgmt.Classes.UsageStatusListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listDelStatus.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                master.LookUpDelStatus = listDelStatus;

                List<SelectListItem> listKeyMgmt = new List<SelectListItem> { };
                new CommonMasterInfo(NikonCommonMasterName.KEY_MGMT).GetSelectList().ToList().
                   ForEach(x => listKeyMgmt.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                master.LookUpKeyMgmt = listKeyMgmt;

                List<SelectListItem> listWeightMgmt = new List<SelectListItem> { };
                new CommonMasterInfo(NikonCommonMasterName.WEIGHT_MGMT).GetSelectList().ToList().
                   ForEach(x => listWeightMgmt.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                master.LookUpWeightMgmt = listWeightMgmt;

                List<SelectListItem> listUnitSize = new List<SelectListItem> { };
                new UnitSizeMasterInfo().GetSelectList().ToList().
                       ForEach(x => listUnitSize.Add(new SelectListItem
                       { Value = x.Id.ToString(), Text = x.Name }));
                master.LookUpUnitSize = listUnitSize;

                List<SelectListItem> listWorktimeMgmt = new List<SelectListItem> { };
                new CommonMasterInfo(NikonCommonMasterName.WORKTIME_MGMT).GetSelectList().ToList().
                   ForEach(x => listWorktimeMgmt.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                master.LookUpWorkTimeMgmt = listWorktimeMgmt;

                // REPORT_NAME Condition
                List<SelectListItem> listReportNameCondition = new List<SelectListItem> { };
                new SearchConditionListMasterInfo().GetSelectList().ToList().
                   ForEach(x => listReportNameCondition.Add(new SelectListItem
                   {
                       Value = x.Id.ToString(),
                       Text = x.Name
                   }));
                master.LookUpReportNameCont = listReportNameCondition;


                List<SelectListItem> items = new List<SelectListItem> { };
                var tableCtls = Control.GetTableControl();
                tableInformation = tableCtls.Single(x => x.TABLE_NAME == "M_REGULATION");
                tableColumnInformations = Control.GetTableColumnControl("M_REGULATION");
                foreach (S_TAB_COL_CONTROL tableColumnInformation in tableColumnInformations)
                {
                    if (tableColumnInformation.IS_SEARCH)
                    {
                        if (tableColumnInformation.COLUMN_NAME == "P_REG_TYPE_ID")
                        {
                            foreach (KeyValuePair<string, string> convertData in tableColumnInformation.SelectData)
                            {
                                SelectListItem item = new SelectListItem
                                {
                                    Text = convertData.Value,
                                    Value = convertData.Key
                                };
                                items.Add(item);
                            }
                        }
                    }
                }

                master.LookUpRegName = items;

                // REPORT_LOCATION Condition
                DataTable dtLocation = new DataTable();
                dtLocation.Columns.Add("Text");
                dtLocation.Columns.Add("Val");
                dtLocation.Rows.Add("保管場所", "0");
                dtLocation.Rows.Add("使用場所", "1");
                dtLocation.Rows.Add("全て", "2");
                master.LookUpLocation = BindLookupValuesReg(dtLocation);

                // STATUS List
                DataTable dtStatus = new DataTable();
                dtStatus.Columns.Add("Text");
                dtStatus.Columns.Add("Val");
                dtStatus.Rows.Add("使用中", "0");
                dtStatus.Rows.Add("削除済", "1");
                dtStatus.Rows.Add("全て", "2");
                master.LookUpStatus = BindLookupValuesReg(dtStatus);

                DataTable dtClassfication = new DataTable();
                dtClassfication.Columns.Add("Text");
                dtClassfication.Columns.Add("Val");
                dtClassfication.Rows.Add("", "0");
                dtClassfication.Rows.Add("法規制", "1");
                dtClassfication.Rows.Add("GHS分類", "2");
                dtClassfication.Rows.Add("消防法", "3");
                dtClassfication.Rows.Add("社内ルール", "4");
                master.LookUpClassification = BindLookupValuesReg(dtClassfication);

                DataTable dtExposureReport = new DataTable();
                dtExposureReport.Columns.Add("Text");
                dtExposureReport.Columns.Add("Val");
                dtExposureReport.Rows.Add("", "0");
                dtExposureReport.Rows.Add("非該当", "1");
                dtExposureReport.Rows.Add("該当", "2");
                master.LookUpExposureReport = BindLookupValuesReg(dtExposureReport);

                DataTable dtRegIconPath = new DataTable();
                dtRegIconPath.Columns.Add("Text");
                dtRegIconPath.Columns.Add("Val");
                dtRegIconPath.Rows.Add("", "0");
                dtRegIconPath.Rows.Add("？", "0000.gif");
                dtRegIconPath.Rows.Add("炎", "0001.gif");
                dtRegIconPath.Rows.Add("爆弾の爆発", "0002.gif");
                dtRegIconPath.Rows.Add("ガスボンベ", "0003.gif");
                dtRegIconPath.Rows.Add("どくろ", "0004.gif");
                dtRegIconPath.Rows.Add("健康有害性", "0005.gif");
                dtRegIconPath.Rows.Add("感嘆符", "0006.gif");
                dtRegIconPath.Rows.Add("環境", "0007.gif");
                dtRegIconPath.Rows.Add("腐食性", "0008.gif");
                dtRegIconPath.Rows.Add("円状の炎", "0009.gif");
                dtRegIconPath.Rows.Add("オレンジ色1.5", "0010.gif");
                dtRegIconPath.Rows.Add("オレンジ色1.6", "0011.gif");
                dtRegIconPath.Rows.Add("爆弾の爆発＆炎", "0012.gif");
                dtRegIconPath.Rows.Add("審 - 1", "10001.gif");
                dtRegIconPath.Rows.Add("審 - 2", "10002.gif");
                dtRegIconPath.Rows.Add("危", "1001.gif");
                dtRegIconPath.Rows.Add("カルタヘナ", "11001.gif");
                dtRegIconPath.Rows.Add("危1", "1101.gif");
                dtRegIconPath.Rows.Add("鉛", "12001.gif");
                dtRegIconPath.Rows.Add("危2", "1201.gif");
                dtRegIconPath.Rows.Add("危3", "1301.gif");
                dtRegIconPath.Rows.Add("危4", "1401.gif");
                dtRegIconPath.Rows.Add("危5", "1501.gif");
                dtRegIconPath.Rows.Add("危6", "1601.gif");
                dtRegIconPath.Rows.Add("指定可燃物", "1701.gif");
                dtRegIconPath.Rows.Add("消防活動阻害物資", "1801.gif");
                dtRegIconPath.Rows.Add("劇", "2001.gif");
                dtRegIconPath.Rows.Add("覚", "3001.gif");
                dtRegIconPath.Rows.Add("麻", "3501.gif");
                dtRegIconPath.Rows.Add("毒", "4001.gif");
                dtRegIconPath.Rows.Add("特化則", "5001.gif");
                dtRegIconPath.Rows.Add("向", "6001.gif");
                dtRegIconPath.Rows.Add("化兵", "7001.gif");
                dtRegIconPath.Rows.Add("有機則", "8001.gif");
                dtRegIconPath.Rows.Add("戦略物資", "9001.gif");
                dtRegIconPath.Rows.Add("×", "99999.gif");
                master.LookUpRegIconPath = BindLookupValuesReg(dtRegIconPath);

                DataTable dtTableName = new DataTable();
                dtTableName.Columns.Add("Text");
                dtTableName.Columns.Add("Val");
                dtTableName.Rows.Add("", "0");
                dtTableName.Rows.Add("形状マスター", "M_FIGURE");
                dtTableName.Rows.Add("権限名マスター", "M_GRANT_NAME");
                dtTableName.Rows.Add("労働安全衛生法上の用途マスター", "M_ISHA_USAGE");
                dtTableName.Rows.Add("鍵管理マスター", "M_KEY_MGMT");
                dtTableName.Rows.Add("保護具マスター", "M_PROTECTOR");
                dtTableName.Rows.Add("変更・廃止の事由マスター", "M_REASON");
                dtTableName.Rows.Add("リスクアセスメントマスター", "M_RISK_ASSESSMENT");
                dtTableName.Rows.Add("小分け容器の有無マスター", "M_SUB_CONTAINER");
                dtTableName.Rows.Add("重量管理マスター", "M_WEIGHT_MGMT");
                dtTableName.Rows.Add("作業時間管理マスター", "M_WORKTIME_MGMT");
                master.LookUpTableName = BindLookupValuesReg(dtTableName); ;

            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }

            return master;
        }

        public IEnumerable<SelectListItem> BindLookupValuesReg(DataTable res)
        {
            //List<DataRow> list =res;
            List<DataRow> list = res.AsEnumerable().ToList();
            IEnumerable<SelectListItem> selectlist = from s in list
                                                     select new SelectListItem
                                                     {
                                                         Text = s["Text"].ToString(),
                                                         Value = s["Val"].ToString()
                                                     };
            return selectlist;
        }

        //2019/02/19 TPE.Rin Add
        /// <summary>
        /// <para>casの追加ボタンの処理です。</para>
        /// <para></para>
        /// </summary>
        public string ValidateCASDataView(string CAS, string MAX, string MIN, string SHAPESELECTION, string TERMSSELECTION)
        {
            string ErrorStatus = null;
            List<CasRegModel> listregmodel = new List<CasRegModel>();
            if (Session[SessionConst.SessionCas] != null)
            {
                listregmodel = (List<CasRegModel>)Session[SessionConst.SessionCas];
            }
            List<string> errorlist = new List<string>();
            CasRegModel cas2 = new CasRegModel();

            repository.CheckCasData(ref errorlist, CAS, MAX, MIN, SHAPESELECTION, TERMSSELECTION);

            if (errorlist.Count > 0)
            {
                foreach (var items in errorlist)
                {
                    ErrorStatus += string.Join(SystemConst.ScriptLinefeed, items);
                }
                return ErrorStatus;
            }
            else
            {
                ErrorStatus = repository.searchEntity(CAS);
            }
            return ErrorStatus;
        }

        public ActionResult GetCasnoDetails(string CAS, string MAX, string MIN, string SHAPESELECTION, string TERMSSELECTION, string SELECTINDEX)
        {
            M_MASTER m_MASTER = new M_MASTER();
            List<CasRegModel> listregmodel = new List<CasRegModel>();
            var cas = (List<CasRegModel>)Session[SessionConst.SessionCas];
            CasRegModel cas2 = new CasRegModel();
            try
            {
                if (Session[SessionConst.SessionCas] != null)
                {
                    listregmodel = (List<CasRegModel>)Session[SessionConst.SessionCas];
                }
                if (SHAPESELECTION != "")
                {

                    SHAPESELECTION = string.Join(SystemConst.SelectionsSemicolon, SHAPESELECTION);
                    var masterService = new MasterService();
                    DataTable dt = masterService.SelectMcommon("M_FIGURE", SHAPESELECTION);
                    if (dt.Rows.Count > 0)
                    {
                        string shape = "";
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            shape += Convert.ToString(dt.Rows[i]["DISPLAY_VALUE"]) + ";";

                        }
                        shape = shape.Trim(';');
                        cas2.shape = shape;
                        cas2.shapekey = SHAPESELECTION;
                    }
                    else
                    {
                        cas2.shape = "";
                    }
                }

                if (TERMSSELECTION != "")
                {

                    TERMSSELECTION = string.Join(SystemConst.SelectionsSemicolon, TERMSSELECTION);

                    var masterService = new MasterService();
                    DataTable dt = masterService.SelectMcommon("M_ISHA_USAGE", TERMSSELECTION);
                    if (dt.Rows.Count > 0)
                    {
                        string terms = "";
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            terms += Convert.ToString(dt.Rows[i]["DISPLAY_VALUE"]) + ";";

                        }
                        terms = terms.Trim(';');
                        cas2.terms = terms;
                        cas2.termskey = TERMSSELECTION;
                    }
                    else
                    {
                        cas2.terms = "";
                    }
                }

                cas2.casno = CAS;
                if (!string.IsNullOrWhiteSpace(MAX))
                {
                    cas2.maximum = Convert.ToDecimal(MAX);
                }
                else
                {
                    cas2.maximum = null;
                }
                if (!string.IsNullOrWhiteSpace(MIN))
                {
                    cas2.minimum = Convert.ToDecimal(MIN);
                }
                else
                {
                    cas2.minimum = null;
                }

                if (listregmodel.Count() > 0)
                {
                    for (int i = 0; i < listregmodel.Count; i++)
                    {
                        if (SELECTINDEX != "")
                        {
                            if (listregmodel[i].ROW_ID == Convert.ToInt32(SELECTINDEX))
                            {
                                cas2.ROW_ID = Convert.ToInt32(SELECTINDEX);

                            }
                        }
                        else
                        {
                            cas2.ROW_ID = listregmodel.Count() + 1;
                        }
                    }

                }
                else
                {
                    cas2.ROW_ID = 0;

                }
                //行選択されていた場合
                if (!string.IsNullOrWhiteSpace(SELECTINDEX))
                {

                    int selectindex;
                    if (int.TryParse(SELECTINDEX, out selectindex))
                    {
                        //selectindex = Convert.ToInt32(SELECTINDEX);
                        for (int i = 0; i < listregmodel.Count; i++)
                        {
                            if (listregmodel[i].ROW_ID == Convert.ToInt32(SELECTINDEX))
                            {
                                listregmodel.RemoveAt(i);
                                listregmodel.Add(cas2);
                                break;
                            }
                        }
                    }
                    Session[SessionConst.SessionCas] = listregmodel;
                    m_MASTER.listCasRegModelDetails = listregmodel;
                }
                else
                {
                    listregmodel.Add(cas2);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            Session[SessionConst.SessionCas] = listregmodel;
            m_MASTER.listCasRegModelDetails = listregmodel;
            return PartialView("_PartialCASGrid", m_MASTER);
        }

        //2019/03/09 TPE.Rin Add
        /// <summary>
        /// <para>casの削除ボタンの処理です。</para>
        /// <para></para>
        /// </summary>
        public ActionResult CasDel(string SELECTINDEX)
        {

            M_MASTER m_MASTER = new M_MASTER();
            List<CasRegModel> listregmodel = new List<CasRegModel>();

            if (Session[SessionConst.SessionCas] != null)
            {
                listregmodel = (List<CasRegModel>)Session[SessionConst.SessionCas];

                int selectindex;
                if (int.TryParse(SELECTINDEX, out selectindex))
                {
                    for (int i = 0; i < listregmodel.Count; i++)
                    {
                        if (listregmodel[i].ROW_ID == Convert.ToInt32(SELECTINDEX))
                        {
                            listregmodel.RemoveAt(i);
                            break;
                        }
                    }
                }
                Session[SessionConst.SessionCas] = listregmodel;
                m_MASTER.listCasRegModelDetails = listregmodel;
            }
            return PartialView("_PartialCASGrid", m_MASTER);
        }


        public JsonResult btnControlCASValues(string SELECTINDEX)
        {
            M_MASTER m_MASTER = new M_MASTER();
            List<CasRegModel> listregmodel = new List<CasRegModel>();
            List<string> result = new List<string>();
            CasRegModel casRegModel = new CasRegModel();

            if (Session[SessionConst.SessionCas] != null)
            {
                listregmodel = (List<CasRegModel>)Session[SessionConst.SessionCas];

                int selectindex;
                if (int.TryParse(SELECTINDEX, out selectindex))
                {
                    for (int i = 0; i < listregmodel.Count; i++)
                    {
                        if (listregmodel[i].ROW_ID == Convert.ToInt32(SELECTINDEX))
                        {
                            result.Add(listregmodel[i].casno);
                            result.Add(listregmodel[i].minimum.ToString());
                            result.Add(listregmodel[i].maximum.ToString());
                            result.Add(listregmodel[i].shape);
                            result.Add(listregmodel[i].shapekey);
                            result.Add(listregmodel[i].terms);
                            result.Add(listregmodel[i].termskey);
                            break;
                        }
                    }
                }
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// 消防法テーブル作成
        /// </summary>
        private Table CreateFireData()
        {

            DataTable _dt = GetFireDataType1();  //消防法
            DataTable _dt2 = GetFireDataType2(); //指定可燃物

            //tblFire.Dispose();

            //行部分作成
            //最大階層数の取得
            int intMax = 0;
            foreach (DataRow dt in _dt.Rows)
            {
                if (intMax < dt["FIRE_NAME"].ToString().Split('/').Length)
                {
                    intMax = dt["FIRE_NAME"].ToString().Split('/').Length;
                }
            }

            int intMax2 = 0;
            foreach (DataRow dt in _dt2.Rows)
            {
                if (intMax2 < dt["FIRE_NAME"].ToString().Split('/').Length)
                {
                    intMax2 = dt["FIRE_NAME"].ToString().Split('/').Length;
                }
            }

            if (intMax < intMax2)
            {
                intMax = intMax2;
            }



            CreateFireTable1(intMax, _dt);
            CreateFireTable2(intMax, _dt2, tblFire.Rows.Count - 2); //0から始まるのと1行途中に合計欄があるので-2を引いている

            //行の結合処理(右側から結合しないとレイアウトが崩れる)
            for (int c = (intMax - 1); c >= 0; c--)
            {
                CellsJoin(tblFire, c);
            }
            return tblFire;

        }

        /// <summary>
        /// 消防法テーブル作成
        /// </summary>
        /// <param name="intMax"></param>
        /// <param name="_dt"></param>
        private void CreateFireTable1(int intMax, DataTable _dt)
        {
            tblFire.BorderStyle = BorderStyle.Solid;
            tblFire.GridLines = GridLines.Both;
            tblFire.BackColor = Color.White;

            TableRow tableRow;
            TableCell tableCell;

            //ヘッダ部作成
            tableRow = new TableRow();
            tableRow.BackColor = Color.Maroon;
            tableRow.ForeColor = Color.White;
            tableRow.HorizontalAlign = HorizontalAlign.Center;

            tableCell = new TableCell();
            tableCell.Text = "危険物の種類";
            tableCell.ColumnSpan = intMax;
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.Text = "指定数量";
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.Text = "届出数量";
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.Text = "倍数";
            tableCell.Width = 80;
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.Text = "\"危険物の種類\",\"指定数量\",\"指定数量単位\",\"届出数量\",\"届出数量単位\",\"倍数\"";
            tableCell.Visible = false;
            tableRow.Cells.Add(tableCell);

            tblFire.Rows.Add(tableRow);

            int i = 0;

            string strName = string.Empty; //消防法の類を退避し、類が変われば区切り線を変更する。CELL結合で類をまたいで結合されるため
            string strBlank = string.Empty;

            foreach (DataRow dt in _dt.Rows)
            {
                tblFireClass tblFireClass = new tblFireClass();
                string strCSV = string.Empty;

                //CSV出力データ作成
                strCSV = "\"" + Convert.ToString(dt["FIRE_NAME"]) + "\"";
                strCSV += ",\"" + Convert.ToString(dt["FIRE_SIZE"]) + "\"";
                strCSV += ",\"" + Convert.ToString(dt["UNIT_NAME"]) + "\"";
                strCSV += ",\"" + "{0}" + "\"";
                strCSV += ",\"" + Convert.ToString(dt["UNIT_NAME"]) + "\"";
                strCSV += ",\"" + "{1}" + "\"";

                tableRow = new TableRow();
                tblFire.Rows.Add(tableRow);

                //危険物の種類
                string[] strText = dt["FIRE_NAME"].ToString().Split('/');

                if (strName != strText[0] && i > 0)
                {
                    if (strBlank == string.Empty)
                    {
                        strBlank = " ";
                    }
                    else
                    {
                        strBlank = string.Empty;
                    }
                }

                strName = strText[0];

                for (int x = 0; x < strText.Length; x++)
                {

                    tableCell = new TableCell();

                    tableCell.Text = strText[x];
                    tableCell.HorizontalAlign = HorizontalAlign.Center;
                    //if (x == (strText.Length - 1)) //危険物の最終列
                    //{
                    //    tableCell.ColumnSpan = (intMax - strText.Length) + 1;
                    //}
                    tableRow.Cells.Add(tableCell);
                }

                for (int x = 0; x < (intMax - strText.Length); x++)
                {
                    tableCell = new TableCell();

                    tableCell.Text = "-" + strBlank;
                    tableCell.HorizontalAlign = HorizontalAlign.Center;
                    tableRow.Cells.Add(tableCell);
                }

                //指定数量
                tableCell = new TableCell();
                tableCell.HorizontalAlign = HorizontalAlign.Center;
                Label _lbl1 = new Label();
                _lbl1.Text = Convert.ToDecimal(dt["FIRE_SIZE"]).ToString("0.###") + " " + dt["UNIT_NAME"].ToString();
                tableCell.Controls.Add(_lbl1);

                HiddenField _hdnFireSize = new HiddenField();
                _hdnFireSize.ID = "hdnFireSize" + i;
                _hdnFireSize.Value = dt["FIRE_SIZE"].ToString();
                tableCell.Controls.Add(_hdnFireSize);

                HiddenField _hdnRegTypeId = new HiddenField();
                _hdnRegTypeId.ID = "hdnRegTypeId" + i;
                _hdnRegTypeId.Value = dt["FIRE_ID"].ToString();
                tableCell.Controls.Add(_hdnRegTypeId);

                HiddenField _hdnFactor = new HiddenField();
                _hdnFactor.ID = "hdnFactor" + i;
                _hdnFactor.Value = dt["FACTOR"].ToString();
                tableCell.Controls.Add(_hdnFactor);

                tblFireClass.lblFireSize = Convert.ToDecimal(dt["FIRE_SIZE"]).ToString("0.###") + " " + dt["UNIT_NAME"].ToString();
                tblFireClass.hdnFireSize = dt["FIRE_SIZE"].ToString();
                tblFireClass.hdnRegTypeId = dt["FIRE_ID"].ToString();
                tblFireClass.hdnFactor = dt["FACTOR"].ToString();
                tblFireClass.id = i.ToString();

                tblFireList.Add(tblFireClass);
                tableRow.Cells.Add(tableCell);



                //届出数量
                tableCell = new TableCell();
                //tableCell.HorizontalAlign = HorizontalAlign.Center;
                TextBox _txt = new TextBox();
                _txt.MaxLength = 11;
                _txt.Width = 60;
                _txt.ID = "txtFireSize" + i;
                _txt.Style.Add("ime-mode", "disabled");
                _txt.Style.Add("text-align", "right");

                //_txt.AutoPostBack = true;
                //_txt.TextChanged += _txt_TextChanged;
                _txt.Attributes["onchange"] = "return txtOnChange(" + i + ",this.value);"; //IDを退避しておいてカーソルをそこに移動する
                tableCell.Controls.Add(_txt);

                Label _lbl2 = new Label();
                _lbl2.Text = dt["UNIT_NAME"].ToString();
                tableCell.Controls.Add(_lbl2);
                tableRow.Cells.Add(tableCell);

                //倍数
                tableCell = new TableCell();
                tableCell.HorizontalAlign = HorizontalAlign.Right;
                Label _lbl3 = new Label();
                _lbl3.ID = "lblMultiple" + i;
                _lbl3.Style.Add("text-align", "right");
                tableCell.Controls.Add(_lbl3);
                tableRow.Cells.Add(tableCell);

                //CSV出力用
                tableCell = new TableCell();
                tableCell.Text = strCSV;
                tableCell.Visible = false;
                tableRow.Cells.Add(tableCell);

                tblFire.Rows.Add(tableRow);

                i++;
            }

            //合計欄の作成
            tableRow = new TableRow();
            tblFire.Rows.Add(tableRow);
            tableCell = new TableCell();
            tableCell.HorizontalAlign = HorizontalAlign.Right;
            tableCell.Style.Add("background-color", "#F5F5DC");
            tableCell.ColumnSpan = intMax + 1;
            tableCell.Text = "<b>危険物合計：</b>";
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.HorizontalAlign = HorizontalAlign.Right;

            Label _lblTotal = new Label();
            _lblTotal.ID = "txtFireSizeTotal";
            _lblTotal.Style.Add("text-align", "right");
            tableCell.Controls.Add(_lblTotal);
            tableRow.Cells.Add(tableCell);


            tableCell = new TableCell();
            tableCell.HorizontalAlign = HorizontalAlign.Right;

            Label _lblMultipleTotal = new Label();
            _lblMultipleTotal.ID = "lblMultipleTotal";
            _lblMultipleTotal.Style.Add("text-align", "right");
            tableCell.Controls.Add(_lblMultipleTotal);
            tableRow.Cells.Add(tableCell);


            tblFire.Rows.Add(tableRow);
        }

        /// <summary>
        /// 指定可燃物テーブル作成
        /// </summary>
        /// <param name="intMax"></param>
        /// <param name="_dt"></param>
        /// <param name="intRowStart"></param>
        private void CreateFireTable2(int intMax, DataTable _dt, int intRowStart)
        {
            tblFire.BorderStyle = BorderStyle.Solid;
            tblFire.GridLines = GridLines.Both;
            tblFire.BackColor = Color.White;

            TableRow tableRow;
            TableCell tableCell;

            int i = intRowStart;

            string strName = string.Empty; //消防法の類を退避し、類が変われば区切り線を変更する。CELL結合で類をまたいで結合されるため
            string strBlank = string.Empty;

            foreach (DataRow dt in _dt.Rows)
            {
                tblFireClass tblFireClass = new tblFireClass();
                string strCSV = string.Empty;

                //CSV出力データ作成
                strCSV = "\"" + Convert.ToString(dt["FIRE_NAME"]) + "\"";
                strCSV += ",\"" + Convert.ToString(dt["FIRE_SIZE"]) + "\"";
                strCSV += ",\"" + Convert.ToString(dt["UNIT_NAME"]) + "\"";
                strCSV += ",\"" + "{0}" + "\"";
                strCSV += ",\"" + Convert.ToString(dt["UNIT_NAME"]) + "\"";
                strCSV += ",\"" + "{1}" + "\"";

                tableRow = new TableRow();
                tblFire.Rows.Add(tableRow);

                //危険物の種類
                string[] strText = dt["FIRE_NAME"].ToString().Split('/');

                if (strName != strText[0] && i > intRowStart)
                {
                    if (strBlank == string.Empty)
                    {
                        strBlank = " ";
                    }
                    else
                    {
                        strBlank = string.Empty;
                    }
                }

                strName = strText[0];

                for (int x = 0; x < strText.Length; x++)
                {

                    tableCell = new TableCell();

                    tableCell.Text = strText[x];
                    tableCell.HorizontalAlign = HorizontalAlign.Center;
                    tableRow.Cells.Add(tableCell);
                }

                for (int x = 0; x < (intMax - strText.Length); x++)
                {
                    tableCell = new TableCell();

                    tableCell.Text = "-";
                    tableCell.HorizontalAlign = HorizontalAlign.Center;
                    tableRow.Cells.Add(tableCell);
                }

                //指定数量
                tableCell = new TableCell();
                tableCell.HorizontalAlign = HorizontalAlign.Center;
                Label _lbl1 = new Label();
                if (dt["FIRE_SIZE"] == DBNull.Value)
                {
                    _lbl1.Text = string.Empty;
                }
                else
                {
                    _lbl1.Text = Convert.ToDecimal(dt["FIRE_SIZE"]).ToString("0.###") + " " + dt["UNIT_NAME"].ToString();
                }
                tableCell.Controls.Add(_lbl1);

                HiddenField _hdnFireSize = new HiddenField();
                _hdnFireSize.ID = "hdnFireSize" + i;
                _hdnFireSize.Value = dt["FIRE_SIZE"].ToString();
                tableCell.Controls.Add(_hdnFireSize);

                HiddenField _hdnRegTypeId = new HiddenField();
                _hdnRegTypeId.ID = "hdnRegTypeId" + i;
                _hdnRegTypeId.Value = dt["FIRE_ID"].ToString();
                tableCell.Controls.Add(_hdnRegTypeId);

                HiddenField _hdnFactor = new HiddenField();
                _hdnFactor.ID = "hdnFactor" + i;
                _hdnFactor.Value = dt["FACTOR"].ToString();
                tableCell.Controls.Add(_hdnFactor);


                tblFireClass.lblFireSize = _lbl1.Text;
                tblFireClass.hdnFireSize = dt["FIRE_SIZE"].ToString();
                tblFireClass.hdnRegTypeId = dt["FIRE_ID"].ToString();
                tblFireClass.hdnFactor = dt["FACTOR"].ToString();
                tblFireClass.id = i.ToString();

                tblFireList.Add(tblFireClass);
                tableRow.Cells.Add(tableCell);

                //届出数量
                tableCell = new TableCell();
                TextBox _txt = new TextBox();
                _txt.MaxLength = 11;
                _txt.Width = 60;
                _txt.ID = "txtFireSize" + i;
                _txt.Style.Add("ime-mode", "disabled");
                _txt.Style.Add("text-align", "right");
                //_txt.AutoPostBack = true;
                // _txt.TextChanged += _txt_TextChanged2;
                _txt.Attributes["onchange"] = "return txtOnChange(" + i + ",this.value);"; //IDを退避しておいてカーソルをそこに移動する
                tableCell.Controls.Add(_txt);

                Label _lbl2 = new Label();
                _lbl2.Text = dt["UNIT_NAME"].ToString();
                tableCell.Controls.Add(_lbl2);
                tableRow.Cells.Add(tableCell);

                //倍数
                tableCell = new TableCell();
                tableCell.HorizontalAlign = HorizontalAlign.Right;
                tableCell.Text = "-";
                tableRow.Cells.Add(tableCell);

                //CSV出力用
                tableCell = new TableCell();
                tableCell.Text = strCSV;
                tableCell.Visible = false;
                tableRow.Cells.Add(tableCell);

                tblFire.Rows.Add(tableRow);

                i++;
            }
        }
        /// <summary>
        /// Cellの結合(行)
        /// </summary>
        /// <param name="Col">対象列</param>
        private void CellsJoin(Table tblTarget, int col)
        {
            TableCell tableCell = new TableCell();
            string strTempData = string.Empty;
            int intRowCnt = 0;
            int intIndex = 1;
            int intSpanCnt = 1;
            foreach (TableRow row in tblTarget.Rows)
            {
                if (intRowCnt > 0) //ヘッダ行の次行から処理対象
                {
                    if (row.Cells[col].Text == strTempData)
                    {
                        tableCell = tblTarget.Rows[intRowCnt].Cells[col];

                        intSpanCnt++;
                        if (row.Cells[col].Text != "")
                        {
                            tblTarget.Rows[intRowCnt].Cells.Remove(tableCell);
                            tblTarget.Rows[intIndex].Cells[col].RowSpan = intSpanCnt;
                        }
                    }
                    else
                    {
                        strTempData = row.Cells[col].Text;
                        intSpanCnt = 1;
                        intIndex = intRowCnt;

                    }
                }
                intRowCnt++;
            }
        }


        /// <summary>
        /// 消防法テーブル作成
        /// </summary>
        private Table CreateFireDataUpd(string type)
        {

            DataTable _dt = GetFireDataType1();  //消防法
            DataTable _dt2 = GetFireDataType2(); //指定可燃物

            tblFire.Dispose();

            //行部分作成
            //最大階層数の取得
            int intMax = 0;
            foreach (DataRow dt in _dt.Rows)
            {
                if (intMax < dt["FIRE_NAME"].ToString().Split('/').Length)
                {
                    intMax = dt["FIRE_NAME"].ToString().Split('/').Length;
                }
            }

            int intMax2 = 0;
            foreach (DataRow dt in _dt2.Rows)
            {
                if (intMax2 < dt["FIRE_NAME"].ToString().Split('/').Length)
                {
                    intMax2 = dt["FIRE_NAME"].ToString().Split('/').Length;
                }
            }

            if (intMax < intMax2)
            {
                intMax = intMax2;
            }


            string mode = type;

            if (Session["TBLFIRECLASS"] != null)
            {
                List<tblFireClass> tblFireClassup = new List<tblFireClass>();
                tblFireClassup = (List<tblFireClass>)Session["TBLFIRECLASS"];
                Session["Tbl2FireClassData"] = tblFireClassup;
            }
            else
            {
                List<tblFireClass> tblFireClassup = new List<tblFireClass>();
                tblFireClassup = (List<tblFireClass>)Session["TblFireListUpd"];
                Session["Tbl2FireClassData"] = tblFireClassup;
            }

            CreateFireTable1Upd(intMax, _dt, mode);
            CreateFireTable2Upd(intMax, _dt2, tblFire.Rows.Count - 2, mode); //0から始まるのと1行途中に合計欄があるので-2を引いている

            //行の結合処理(右側から結合しないとレイアウトが崩れる)
            for (int c = (intMax - 1); c >= 0; c--)
            {
                CellsJoinUpd(tblFire, c);
            }
            //Session["tblFire"] = tblFire;
            return tblFire;

        }

        /// <summary>
        /// 消防法テーブル作成
        /// </summary>
        /// <param name="intMax"></param>
        /// <param name="_dt"></param>
        private void CreateFireTable1Upd(int intMax, DataTable _dt, string type)
        {
            tblFire.BorderStyle = BorderStyle.Solid;
            tblFire.GridLines = GridLines.Both;
            tblFire.BackColor = Color.White;

            TableRow tableRow;
            TableCell tableCell;

            //ヘッダ部作成
            tableRow = new TableRow();
            tableRow.BackColor = Color.Maroon;
            tableRow.ForeColor = Color.White;
            tableRow.HorizontalAlign = HorizontalAlign.Center;

            tableCell = new TableCell();
            tableCell.Text = "危険物の種類";
            tableCell.ColumnSpan = intMax;
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.Text = "指定数量";
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.Text = "届出数量";
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.Text = "倍数";
            tableCell.Width = 80;
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.Text = "\"危険物の種類\",\"指定数量\",\"指定数量単位\",\"届出数量\",\"届出数量単位\",\"倍数\"";
            tableCell.Visible = false;
            tableRow.Cells.Add(tableCell);

            tblFire.Rows.Add(tableRow);

            string strName = string.Empty; //消防法の類を退避し、類が変われば区切り線を変更する。CELL結合で類をまたいで結合されるため
            string strBlank = string.Empty;

            string sql = null;
            Hashtable _params = new Hashtable();
            DataTable dtsql = new DataTable();
            tblFireClass tblFireClass = new tblFireClass();
            string strCSV = string.Empty;

            List<tblFireClass> lstTblFireClass = new List<tblFireClass>();

            if (type == "Update")
            {
                GetFireSql(ref sql, ref _params);

                dtsql = DbUtil.Select(sql, _params);
            }


            List<string> txtVal = new List<string>();
            List<string> lblVal = new List<string>();

            TextBox _txtValTotal = new TextBox();
            Label _lblValTotal = new Label();

            //count = dtsql.Rows.Count();
            int i = 0;
            int j = 0;

            //List<tblFireClass> tblFireClsList = new List<tblFireClass>();
            foreach (DataRow dt in _dt.Rows)
            {

                //CSV出力データ作成
                strCSV = "\"" + Convert.ToString(dt["FIRE_NAME"]) + "\"";
                strCSV += ",\"" + Convert.ToString(dt["FIRE_SIZE"]) + "\"";
                strCSV += ",\"" + Convert.ToString(dt["UNIT_NAME"]) + "\"";
                strCSV += ",\"" + "{0}" + "\"";
                strCSV += ",\"" + Convert.ToString(dt["UNIT_NAME"]) + "\"";
                strCSV += ",\"" + "{1}" + "\"";

                tableRow = new TableRow();
                tblFire.Rows.Add(tableRow);

                //危険物の種類
                string[] strText = dt["FIRE_NAME"].ToString().Split('/');

                if (strName != strText[0] && i > 0)
                {
                    if (strBlank == string.Empty)
                    {
                        strBlank = " ";
                    }
                    else
                    {
                        strBlank = string.Empty;
                    }
                }

                strName = strText[0];

                for (int x = 0; x < strText.Length; x++)
                {

                    tableCell = new TableCell();

                    tableCell.Text = strText[x];
                    tableCell.HorizontalAlign = HorizontalAlign.Center;
                    //if (x == (strText.Length - 1)) //危険物の最終列
                    //{
                    //    tableCell.ColumnSpan = (intMax - strText.Length) + 1;
                    //}
                    tableRow.Cells.Add(tableCell);
                }

                for (int x = 0; x < (intMax - strText.Length); x++)
                {
                    tableCell = new TableCell();

                    tableCell.Text = "-" + strBlank;
                    tableCell.HorizontalAlign = HorizontalAlign.Center;
                    tableRow.Cells.Add(tableCell);
                }

                //指定数量
                tableCell = new TableCell();
                tableCell.HorizontalAlign = HorizontalAlign.Center;
                Label _lbl1 = new Label();
                _lbl1.Text = Convert.ToDecimal(dt["FIRE_SIZE"]).ToString("0.###") + " " + dt["UNIT_NAME"].ToString();
                tableCell.Controls.Add(_lbl1);

                HiddenField _hdnFireSize = new HiddenField();
                _hdnFireSize.ID = "hdnFireSize" + i;
                _hdnFireSize.Value = dt["FIRE_SIZE"].ToString();
                tableCell.Controls.Add(_hdnFireSize);

                HiddenField _hdnRegTypeId = new HiddenField();
                _hdnRegTypeId.ID = "hdnRegTypeId" + i;
                _hdnRegTypeId.Value = dt["FIRE_ID"].ToString();
                tableCell.Controls.Add(_hdnRegTypeId);

                HiddenField _hdnFactor = new HiddenField();
                _hdnFactor.ID = "hdnFactor" + i;
                _hdnFactor.Value = dt["FACTOR"].ToString();
                tableCell.Controls.Add(_hdnFactor);

                tblFireClass.lblFireSize = Convert.ToDecimal(dt["FIRE_SIZE"]).ToString("0.###") + " " + dt["UNIT_NAME"].ToString();
                tblFireClass.hdnFireSize = dt["FIRE_SIZE"].ToString();
                tblFireClass.hdnRegTypeId = dt["FIRE_ID"].ToString();
                tblFireClass.hdnFactor = dt["FACTOR"].ToString();
                tblFireClass.id = i.ToString();

                tblFireList.Add(tblFireClass);
                tableRow.Cells.Add(tableCell);

                //届出数量
                tableCell = new TableCell();
                //tableCell.HorizontalAlign = HorizontalAlign.Center;
                TextBox _txt = new TextBox();
                _txt.MaxLength = 11;
                _txt.Width = 60;
                _txt.ID = "txtFireSize" + i;
                tblFireClass tblFirecls = new tblFireClass();
                foreach (DataRow _row in dtsql.Rows)
                {
                    List<tblFireClass> tblFireClass1 = new List<tblFireClass>();
                    if (Session["TblFireListUpd"] != null)
                    {
                        tblFireClass1 = (List<tblFireClass>)Session["TblFireListUpd"];
                        for (j = 0; j < tblFireClass1.Count; j++)
                        {
                            if (tblFireClass1[j].hdnRegTypeId == _hdnRegTypeId.Value)
                            {
                                if (Common.IsNumber(tblFireClass1[j].txtFireSize) == false)
                                {
                                    _txt.Text = string.Empty;
                                    _txtValTotal.Text = tblFireClass1[j].txtFireSize;
                                    txtVal.Add(_txtValTotal.Text);
                                }
                                if (tblFireClass1[j].txtFireSize == null)
                                {
                                    _txt.Text = string.Empty;

                                }
                                else
                                {
                                    _txt.Text = Convert.ToString(Convert.ToDecimal(tblFireClass1[j].txtFireSize).ToString("F4"));
                                }
                                _txtValTotal.Text = tblFireClass1[j].txtFireSize;
                                txtVal.Add(_txtValTotal.Text);

                                tblFirecls.txtFireSize = tblFireClass1[j].txtFireSize;
                                tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                                tblFirecls.hdnFireSize = _hdnFireSize.Value;
                                tblFirecls.id = i.ToString();
                            }
                        }
                        break;
                    }

                    else if (_hdnRegTypeId.Value == _row["REG_TYPE_ID"].ToString())
                    {
                        if (Common.IsNumber(_txt.Text) == false)
                        {
                            _txt.Text = string.Empty;
                            _txtValTotal.Text = _txt.Text;
                            txtVal.Add(_txtValTotal.Text);
                        }

                        //if (tblFireClass1[j].txtFireSize == null)
                        //{
                        //    _txt.Text = string.Empty;

                        //}
                        //else
                        //{
                        _txt.Text = Convert.ToString(Convert.ToDecimal(_row["FIRE_SIZE"]).ToString("F4"));
                        //}
                        _txtValTotal.Text = _txt.Text;
                        txtVal.Add(_txtValTotal.Text);

                        tblFirecls.txtFireSize = _txt.Text;
                        tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                        tblFirecls.hdnFireSize = _hdnFireSize.Value;

                    }
                    //tblFireClsList.Add(tblFirecls);
                    //if (type == "Update")
                    //{
                    //    Session["TblFireListUpd"] = tblFireClsList;
                    //}
                    //else
                    //{
                    //    Session["TBLFIRECLASS"] = tblFireClsList;
                    //}


                }
                if (type != "Update")
                {
                    if (Session["TBLFIRECLASS"] != null)
                    {
                        List<tblFireClass> tblFireClass2 = new List<tblFireClass>();
                        tblFireClass2 = (List<tblFireClass>)Session["TBLFIRECLASS"];
                        for (j = 0; j < tblFireClass2.Count; j++)
                        {
                            if (tblFireClass2[j].hdnRegTypeId == _hdnRegTypeId.Value)
                            {
                                if (Common.IsNumber(tblFireClass2[j].txtFireSize) == false)
                                {
                                    _txt.Text = string.Empty;
                                    _txtValTotal.Text = tblFireClass2[j].txtFireSize;
                                    txtVal.Add(_txtValTotal.Text);
                                }
                                if (tblFireClass2[j].txtFireSize == null)
                                {
                                    _txt.Text = string.Empty;

                                }
                                else
                                {
                                    _txt.Text = Convert.ToString(Convert.ToDecimal(tblFireClass2[j].txtFireSize).ToString("F4"));
                                }
                                _txtValTotal.Text = tblFireClass2[j].txtFireSize;
                                txtVal.Add(_txtValTotal.Text);

                                tblFirecls.txtFireSize = tblFireClass2[j].txtFireSize;
                                tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                                tblFirecls.hdnFireSize = _hdnFireSize.Value;
                                tblFirecls.id = i.ToString();
                            }
                            else if (Convert.ToInt32(tblFireClass2[j].id) >= 32)
                            {
                                if (tblFireClass2[j].hdnRegTypeId == _hdnRegTypeId.Value)
                                {
                                    if (Common.IsNumber(tblFireClass2[j].txtFireSize) == false)
                                    {
                                        _txt.Text = string.Empty;
                                        _txtValTotal.Text = tblFireClass2[j].txtFireSize;
                                        txtVal.Add(_txtValTotal.Text);
                                    }
                                    if (tblFireClass2[j].txtFireSize == null)
                                    {
                                        _txt.Text = string.Empty;

                                    }
                                    else
                                    {
                                        _txt.Text = Convert.ToString(Convert.ToDecimal(tblFireClass2[j].txtFireSize).ToString("F4"));
                                    }
                                    _txtValTotal.Text = tblFireClass2[j].txtFireSize;
                                    txtVal.Add(_txtValTotal.Text);

                                    tblFirecls.txtFireSize = tblFireClass2[j].txtFireSize;
                                    tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                                    tblFirecls.hdnFireSize = _hdnFireSize.Value;
                                    tblFirecls.id = i.ToString();
                                }
                            }

                        }
                    }
                    //else if (_hdnRegTypeId.Value == _row["REG_TYPE_ID"].ToString())
                    //{
                    //    if (Common.IsNumber(_txt.Text) == false)
                    //    {
                    //        _txt.Text = string.Empty;
                    //        _txtValTotal.Text = _txt.Text;
                    //        txtVal.Add(_txtValTotal.Text);
                    //    }
                    //    _txt.Text = Convert.ToString(Convert.ToDecimal(_row["FIRE_SIZE"]).ToString("F4"));
                    //    _txtValTotal.Text = _txt.Text;
                    //    txtVal.Add(_txtValTotal.Text);

                    //    tblFirecls.txtFireSize = _txt.Text;
                    //    tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                    //    tblFirecls.hdnFireSize = _hdnFireSize.Value;

                    //}
                }

                _txt.Style.Add("ime-mode", "disabled");
                _txt.Style.Add("text-align", "right");

                //_txt.AutoPostBack = true;
                //_txt.TextChanged += _txt_TextChanged;
                _txt.Attributes["onchange"] = "return txtOnChange(" + i + ",this.value);"; //IDを退避しておいてカーソルをそこに移動する
                tableCell.Controls.Add(_txt);

                Label _lbl2 = new Label();
                _lbl2.Text = dt["UNIT_NAME"].ToString();
                tableCell.Controls.Add(_lbl2);
                tableRow.Cells.Add(tableCell);

                //倍数
                tableCell = new TableCell();
                tableCell.HorizontalAlign = HorizontalAlign.Right;
                Label _lbl3 = new Label();
                _lbl3.ID = "lblMultiple" + i;
                foreach (DataRow _row in dtsql.Rows)
                {
                    //if (_hdnRegTypeId.Value == _row["REG_TYPE_ID"].ToString())
                    //{
                    //    if (Common.IsNumber(_txt.Text) == false)
                    //    {
                    //        _lbl3.Text = string.Empty;

                    //    }
                    //    if (_lbl3 != null)
                    //    {
                    //        if (_txt.Text.Trim() == "")
                    //        {
                    //            _lbl3.Text = string.Empty;
                    //            _lblValTotal.Text = string.Empty;
                    //            lblVal.Add(_lblValTotal.Text);
                    //        }
                    //        else
                    //        {
                    //            _lbl3.Text = Convert.ToString(Math.Round(Convert.ToDecimal(_txt.Text) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                    //            _lblValTotal.Text = Convert.ToString(Math.Round(Convert.ToDecimal(_txt.Text) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                    //            lblVal.Add(_lblValTotal.Text);
                    //        }
                    //    }
                    //    tblFirecls.mulFirSize = _lblValTotal.Text;
                    //    tblFireClsList.Add(tblFirecls);

                    //}
                    //else 

                    if (Session["TblFireListUpd"] != null)
                    {
                        List<tblFireClass> tblFireClass1 = new List<tblFireClass>();
                        tblFireClass1 = (List<tblFireClass>)Session["TblFireListUpd"];
                        for (j = 0; j < tblFireClass1.Count; j++)
                        {
                            if (tblFireClass1[j].hdnRegTypeId == _hdnRegTypeId.Value)
                            {
                                if (Common.IsNumber(tblFireClass1[j].txtFireSize) == false)
                                {
                                    _lbl3.Text = string.Empty;

                                }
                                if (_lbl3 != null)
                                {
                                    if (_txt.Text.Trim() == "")
                                    {
                                        _lbl3.Text = string.Empty;
                                        _lblValTotal.Text = string.Empty;
                                        lblVal.Add(_lblValTotal.Text);
                                    }
                                    else
                                    {
                                        _lbl3.Text = Convert.ToString(Math.Round(Convert.ToDecimal(tblFireClass1[j].txtFireSize) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                        _lblValTotal.Text = Convert.ToString(Math.Round(Convert.ToDecimal(tblFireClass1[j].txtFireSize) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                        lblVal.Add(_lblValTotal.Text);
                                    }
                                }
                                tblFirecls.mulFirSize = _lblValTotal.Text;
                                tblFireClsList.Add(tblFirecls);
                            }
                        }
                        break;
                    }
                    else if (_hdnRegTypeId.Value == _row["REG_TYPE_ID"].ToString())
                    {
                        if (Common.IsNumber(_txt.Text) == false)
                        {
                            _lbl3.Text = string.Empty;

                        }
                        if (_lbl3 != null)
                        {
                            if (_txt.Text.Trim() == "")
                            {
                                _lbl3.Text = string.Empty;
                                _lblValTotal.Text = string.Empty;
                                lblVal.Add(_lblValTotal.Text);
                            }
                            else
                            {
                                _lbl3.Text = Convert.ToString(Math.Round(Convert.ToDecimal(_txt.Text) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                _lblValTotal.Text = Convert.ToString(Math.Round(Convert.ToDecimal(_txt.Text) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                lblVal.Add(_lblValTotal.Text);
                            }
                        }
                        tblFirecls.mulFirSize = _lblValTotal.Text;
                        tblFireClsList.Add(tblFirecls);

                    }


                }



                if (type != "Update")
                {
                    if (Session["TBLFIRECLASS"] != null)
                    {
                        List<tblFireClass> tblFireClass1 = new List<tblFireClass>();
                        tblFireClass1 = (List<tblFireClass>)Session["TBLFIRECLASS"];

                        for (j = 0; j < tblFireClass1.Count; j++)
                        {
                            if (tblFireClass1[j].hdnRegTypeId == _hdnRegTypeId.Value)
                            {
                                if (Common.IsNumber(tblFireClass1[j].txtFireSize) == false)
                                {
                                    _lbl3.Text = string.Empty;

                                }
                                if (_lbl3 != null)
                                {
                                    if (_txt.Text.Trim() == "")
                                    {
                                        _lbl3.Text = string.Empty;
                                        _lblValTotal.Text = string.Empty;
                                        lblVal.Add(_lblValTotal.Text);
                                    }
                                    else
                                    {
                                        if (tblFireClass1[j].txtFireSize == null)
                                        {
                                            _lbl3.Text = string.Empty;

                                        }
                                        else
                                        {
                                            _lbl3.Text = Convert.ToString(Math.Round(Convert.ToDecimal(tblFireClass1[j].txtFireSize) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                        }
                                        _lblValTotal.Text = Convert.ToString(Math.Round(Convert.ToDecimal(tblFireClass1[j].txtFireSize) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                        lblVal.Add(_lblValTotal.Text);
                                    }
                                }
                                tblFirecls.mulFirSize = _lblValTotal.Text;
                                tblFireClsList.Add(tblFirecls);
                            }

                        }
                    }
                    //else if (_hdnRegTypeId.Value == _row["REG_TYPE_ID"].ToString())
                    //{
                    //    if (Common.IsNumber(_txt.Text) == false)
                    //    {
                    //        _lbl3.Text = string.Empty;

                    //    }
                    //    if (_lbl3 != null)
                    //    {
                    //        if (_txt.Text.Trim() == "")
                    //        {
                    //            _lbl3.Text = string.Empty;
                    //            _lblValTotal.Text = string.Empty;
                    //            lblVal.Add(_lblValTotal.Text);
                    //        }
                    //        else
                    //        {
                    //            _lbl3.Text = Convert.ToString(Math.Round(Convert.ToDecimal(_txt.Text) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                    //            _lblValTotal.Text = Convert.ToString(Math.Round(Convert.ToDecimal(_txt.Text) / Convert.ToDecimal(_hdnFireSize.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                    //            lblVal.Add(_lblValTotal.Text);
                    //        }
                    //    }
                    //    tblFirecls.mulFirSize = _lblValTotal.Text;
                    //    tblFireClsList.Add(tblFirecls);
                    //    //if (type == "Update")
                    //    //{
                    //    //    Session["TblFireListUpd"] = tblFireClsList;
                    //    //}
                    //    //else
                    //    //{
                    //    //    Session["TBLFIRECLASS"] = tblFireClsList;
                    //    //}
                    //}
                }


                //if (Session["TblFireListUpd"] != null)
                //{
                //    tblFireClass = (List<tblFireClass>)Session["TblFireListUpd"];
                //}



                _lbl3.Style.Add("text-align", "right");
                tableCell.Controls.Add(_lbl3);
                tableRow.Cells.Add(tableCell);

                //CSV出力用
                tableCell = new TableCell();
                tableCell.Text = strCSV;
                tableCell.Visible = false;
                tableRow.Cells.Add(tableCell);

                tblFire.Rows.Add(tableRow);

                i++;
            }
            if (type == "Update")
            {
                Session["TblFireListUpd"] = tblFireClsList;
            }
            else
            {
                //List<tblFireClass> tblFireClass1 = new List<tblFireClass>();
                //tblFireClass1 = (List<tblFireClass>)Session["TBLFIRECLASS"];

                Session["TBLFIRECLASS"] = tblFireClsList;
            }

            //合計欄の作成
            tableRow = new TableRow();
            tblFire.Rows.Add(tableRow);
            tableCell = new TableCell();
            tableCell.HorizontalAlign = HorizontalAlign.Right;
            tableCell.Style.Add("background-color", "#F5F5DC");
            tableCell.ColumnSpan = intMax + 1;
            tableCell.Text = "<b>危険物合計：</b>";
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.HorizontalAlign = HorizontalAlign.Right;

            Label _lblTotal = new Label();
            _lblTotal.ID = "txtFireSizeTotal";
            _lblTotal.Style.Add("text-align", "right");
            tableCell.Controls.Add(_lblTotal);
            tableRow.Cells.Add(tableCell);

            tableCell = new TableCell();
            tableCell.HorizontalAlign = HorizontalAlign.Right;

            Label _lblMultipleTotal = new Label();
            _lblMultipleTotal.ID = "lblMultipleTotal";
            _lblMultipleTotal.Style.Add("text-align", "right");
            tableCell.Controls.Add(_lblMultipleTotal);
            tableRow.Cells.Add(tableCell);

            decimal dblTotal = 0;
            decimal dblTotal2 = 0;

            //for (int i = 0; i < mFire.tblFireView.Rows.Count - 1; i++)
            //{
            //    TextBox _txt = (TextBox)tblFire.FindControl("txtFireSize" + i);
            //    Label _lbl = (Label)tblFire.FindControl("lblMultiple" + i);
            //    HiddenField _hdn = (HiddenField)tblFire.FindControl("hdnFactor" + i);
            foreach (var item1 in txtVal)
            {

                if (Common.IsNumber(item1) == true)
                {
                    //dblTotal = dblTotal + Convert.ToDouble(_txt.Text) * Convert.ToDouble(_hdn.Value);
                    dblTotal = dblTotal + Convert.ToDecimal(item1);
                }
            }
            foreach (var item2 in lblVal)
            {

                if (Common.IsNumber(item2) == true)
                {
                    dblTotal2 = dblTotal2 + Convert.ToDecimal(item2);
                }
            }

            //}            //}
            if (dblTotal2 == 0 && dblTotal == 0)
            {
                _lblTotal.Text = string.Empty;
                _lblMultipleTotal.Text = string.Empty;
            }
            else
            {
                _lblTotal.Text = Convert.ToString(dblTotal.ToString("F4"));
                _lblMultipleTotal.Text = Convert.ToString(dblTotal2.ToString("F4"));
            }




            tblFire.Rows.Add(tableRow);
        }

        /// <summary>
        /// 指定可燃物テーブル作成
        /// </summary>
        /// <param name="intMax"></param>
        /// <param name="_dt"></param>
        /// <param name="intRowStart"></param>
        private void CreateFireTable2Upd(int intMax, DataTable _dt, int intRowStart, string type)
        {
            tblFire.BorderStyle = BorderStyle.Solid;
            tblFire.GridLines = GridLines.Both;
            tblFire.BackColor = Color.White;

            TableRow tableRow;
            TableCell tableCell;

            int i = intRowStart;

            string strName = string.Empty; //消防法の類を退避し、類が変われば区切り線を変更する。CELL結合で類をまたいで結合されるため
            string strBlank = string.Empty;

            string sql = null;
            Hashtable _params = new Hashtable();
            DataTable dtsql = new DataTable();
            tblFireClass tblFireClass = new tblFireClass();
            string strCSV = string.Empty;

            List<tblFireClass> lstTblFireClass = new List<tblFireClass>();

            if (type == "Update")
            {
                GetFireSql(ref sql, ref _params);

                dtsql = DbUtil.Select(sql, _params);
            }

            List<string> txtVal = new List<string>();
            List<string> lblVal = new List<string>();

            TextBox _txtValTotal = new TextBox();
            Label _lblValTotal = new Label();

            //int i = 0;
            int j = 0;

            foreach (DataRow dt in _dt.Rows)
            {

                //CSV出力データ作成
                strCSV = "\"" + Convert.ToString(dt["FIRE_NAME"]) + "\"";
                strCSV += ",\"" + Convert.ToString(dt["FIRE_SIZE"]) + "\"";
                strCSV += ",\"" + Convert.ToString(dt["UNIT_NAME"]) + "\"";
                strCSV += ",\"" + "{0}" + "\"";
                strCSV += ",\"" + Convert.ToString(dt["UNIT_NAME"]) + "\"";
                strCSV += ",\"" + "{1}" + "\"";

                tableRow = new TableRow();
                tblFire.Rows.Add(tableRow);

                //危険物の種類
                string[] strText = dt["FIRE_NAME"].ToString().Split('/');

                if (strName != strText[0] && i > intRowStart)
                {
                    if (strBlank == string.Empty)
                    {
                        strBlank = " ";
                    }
                    else
                    {
                        strBlank = string.Empty;
                    }
                }

                strName = strText[0];

                for (int x = 0; x < strText.Length; x++)
                {

                    tableCell = new TableCell();

                    tableCell.Text = strText[x];
                    tableCell.HorizontalAlign = HorizontalAlign.Center;
                    tableRow.Cells.Add(tableCell);
                }

                for (int x = 0; x < (intMax - strText.Length); x++)
                {
                    tableCell = new TableCell();

                    tableCell.Text = "-";
                    tableCell.HorizontalAlign = HorizontalAlign.Center;
                    tableRow.Cells.Add(tableCell);
                }

                //指定数量
                tableCell = new TableCell();
                tableCell.HorizontalAlign = HorizontalAlign.Center;
                Label _lbl1 = new Label();
                if (dt["FIRE_SIZE"] == DBNull.Value)
                {
                    _lbl1.Text = string.Empty;
                }
                else
                {
                    _lbl1.Text = Convert.ToDecimal(dt["FIRE_SIZE"]).ToString("0.###") + " " + dt["UNIT_NAME"].ToString();
                }
                tableCell.Controls.Add(_lbl1);

                HiddenField _hdnFireSize = new HiddenField();
                _hdnFireSize.ID = "hdnFireSize" + i;
                _hdnFireSize.Value = dt["FIRE_SIZE"].ToString();
                tableCell.Controls.Add(_hdnFireSize);

                HiddenField _hdnRegTypeId = new HiddenField();
                _hdnRegTypeId.ID = "hdnRegTypeId" + i;
                _hdnRegTypeId.Value = dt["FIRE_ID"].ToString();
                tableCell.Controls.Add(_hdnRegTypeId);

                HiddenField _hdnFactor = new HiddenField();
                _hdnFactor.ID = "hdnFactor" + i;
                _hdnFactor.Value = dt["FACTOR"].ToString();
                tableCell.Controls.Add(_hdnFactor);


                tblFireClass.lblFireSize = _lbl1.Text;
                tblFireClass.hdnFireSize = dt["FIRE_SIZE"].ToString();
                tblFireClass.hdnRegTypeId = dt["FIRE_ID"].ToString();
                tblFireClass.hdnFactor = dt["FACTOR"].ToString();
                tblFireClass.id = i.ToString();

                tblFireList.Add(tblFireClass);
                tableRow.Cells.Add(tableCell);

                //届出数量
                tableCell = new TableCell();
                TextBox _txt = new TextBox();
                _txt.MaxLength = 11;
                _txt.Width = 60;
                _txt.ID = "txtFireSize" + i;
                //int j = 0;
                tblFireClass tblFirecls = new tblFireClass();
                foreach (DataRow _row in dtsql.Rows)
                {
                    List<tblFireClass> tblFireClass1 = new List<tblFireClass>();
                    if (_row["REG_TYPE_ID"].ToString() == _hdnRegTypeId.Value)
                    {
                        _txt.Text = _row["FIRE_SIZE"].ToString();
                        tblFirecls.txtFireSize = _row["FIRE_SIZE"].ToString();
                        tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                        tblFirecls.hdnFireSize = _hdnFireSize.Value;
                        tblFirecls.id = i.ToString();
                        tblFireClsList.Add(tblFirecls);
                    }

                    //if (Session["TblFireListUpd"] != null)
                    //{
                    //    tblFireClass1 = (List<tblFireClass>)Session["TblFireListUpd"];
                    //    for (j = 0; j < tblFireClass1.Count; j++)
                    //    {
                    //        /* if (_hdnRegTypeId.Value == _row["REG_TYPE_ID"].ToString())
                    //        {
                    //            if (Common.IsNumber(_txt.Text) == false)
                    //            {
                    //                _txt.Text = string.Empty;
                    //                _txtValTotal.Text = _txt.Text;
                    //                txtVal.Add(_txtValTotal.Text);
                    //            }
                    //            _txt.Text = Convert.ToString(Convert.ToDecimal(_row["FIRE_SIZE"]).ToString("F4"));
                    //            _txtValTotal.Text = _txt.Text;
                    //            txtVal.Add(_txtValTotal.Text);

                    //            tblFirecls.txtFireSize = _txt.Text;
                    //            tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                    //            tblFirecls.hdnFireSize = _hdnFireSize.Value;
                    //            tblFirecls.id = i.ToString();
                    //            tblFireClsList.Add(tblFirecls);
                    //        } */

                    //        if (tblFireClass1[j].hdnRegTypeId == _hdnRegTypeId.Value)
                    //        {
                    //            if (Common.IsNumber(tblFireClass1[j].txtFireSize) == false)
                    //            {
                    //                _txt.Text = string.Empty;
                    //                _txtValTotal.Text = tblFireClass1[j].txtFireSize;
                    //                txtVal.Add(_txtValTotal.Text);
                    //            }
                    //            if (tblFireClass1[j].txtFireSize == null)
                    //            {
                    //                _txt.Text = string.Empty;

                    //            }
                    //            else
                    //            {
                    //                _txt.Text = Convert.ToString(Convert.ToDecimal(tblFireClass1[j].txtFireSize).ToString("F4"));
                    //            }
                    //            _txtValTotal.Text = tblFireClass1[j].txtFireSize;
                    //            txtVal.Add(_txtValTotal.Text);

                    //            tblFirecls.txtFireSize = tblFireClass1[j].txtFireSize;
                    //            tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                    //            tblFirecls.hdnFireSize = _hdnFireSize.Value;
                    //            tblFirecls.id = i.ToString();
                    //        }


                    //    }
                    //    break;
                    //}
                    //else if (_hdnRegTypeId.Value == _row["REG_TYPE_ID"].ToString())
                    //{
                    //    if (Common.IsNumber(_txt.Text) == false)
                    //    {
                    //        _txt.Text = string.Empty;
                    //        _txtValTotal.Text = _txt.Text;
                    //        txtVal.Add(_txtValTotal.Text);
                    //    }
                    //    _txt.Text = Convert.ToString(Convert.ToDecimal(_row["FIRE_SIZE"]).ToString("F4"));
                    //    _txtValTotal.Text = _txt.Text;
                    //    txtVal.Add(_txtValTotal.Text);

                    //    tblFirecls.txtFireSize = _txt.Text;
                    //    tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                    //    tblFirecls.hdnFireSize = _hdnFireSize.Value;
                    //    /*  tblFirecls.id = i.ToString();
                    //     tblFireClsList.Add(tblFirecls); */
                    //}
                    //Session["TblFireListUpd"] = tblFireClsList;
                }
                if (type != "Update")
                {

                    if (Session["Tbl2FireClassData"] != null)
                    {
                        List<tblFireClass> tblFireClass2 = new List<tblFireClass>();
                        tblFireClass2 = (List<tblFireClass>)Session["Tbl2FireClassData"];
                        for (j = 0; j < tblFireClass2.Count; j++)
                        {
                            if (tblFireClass2[j].hdnRegTypeId == _hdnRegTypeId.Value)
                            {
                                if (Common.IsNumber(tblFireClass2[j].txtFireSize) == false)
                                {
                                    _txt.Text = string.Empty;
                                    _txtValTotal.Text = tblFireClass2[j].txtFireSize;
                                    txtVal.Add(_txtValTotal.Text);
                                    tblFirecls.txtFireSize = tblFireClass2[j].txtFireSize;
                                    tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                                    tblFirecls.hdnFireSize = _hdnFireSize.Value;
                                    tblFirecls.id = i.ToString();
                                }
                                else
                                {
                                    if (tblFireClass2[j].txtFireSize == null)
                                    {
                                        _txt.Text = string.Empty;
                                    }
                                    else
                                    {
                                        _txt.Text = Convert.ToString(Convert.ToDecimal(tblFireClass2[j].txtFireSize).ToString("F4"));
                                    }
                                    _txtValTotal.Text = tblFireClass2[j].txtFireSize;
                                    txtVal.Add(_txtValTotal.Text);

                                    tblFirecls.txtFireSize = tblFireClass2[j].txtFireSize;
                                    tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                                    tblFirecls.hdnFireSize = _hdnFireSize.Value;
                                    tblFirecls.id = i.ToString();
                                }
                                tblFireClsList.Add(tblFirecls);
                            }
                            //else if (Convert.ToInt32(tblFireClass2[j].id) >= 31)
                            //{
                            //    if (tblFireClass2[j].hdnRegTypeId == _hdnRegTypeId.Value)
                            //    {
                            //        if (Common.IsNumber(tblFireClass2[j].txtFireSize) == false)
                            //        {
                            //            _txt.Text = string.Empty;
                            //            _txtValTotal.Text = tblFireClass2[j].txtFireSize;
                            //            txtVal.Add(_txtValTotal.Text);
                            //        }
                            //        if (tblFireClass2[j].txtFireSize == null)
                            //        {
                            //            _txt.Text = string.Empty;

                            //        }
                            //        else
                            //        {
                            //            _txt.Text = Convert.ToString(Convert.ToDecimal(tblFireClass2[j].txtFireSize).ToString("F4"));
                            //        }
                            //        _txtValTotal.Text = tblFireClass2[j].txtFireSize;
                            //        txtVal.Add(_txtValTotal.Text);

                            //        tblFirecls.txtFireSize = tblFireClass2[j].txtFireSize;
                            //        tblFirecls.hdnRegTypeId = _hdnRegTypeId.Value;
                            //        tblFirecls.hdnFireSize = _hdnFireSize.Value;
                            //        tblFirecls.id = i.ToString();
                            //    }
                            //}
                        }
                    }
                }


                _txt.Style.Add("ime-mode", "disabled");
                _txt.Style.Add("text-align", "right");
                //_txt.AutoPostBack = true;
                // _txt.TextChanged += _txt_TextChanged2;
                _txt.Attributes["onchange"] = "return txtOnChange(" + i + ",this.value);"; //IDを退避しておいてカーソルをそこに移動する
                tableCell.Controls.Add(_txt);

                Label _lbl2 = new Label();
                _lbl2.Text = dt["UNIT_NAME"].ToString();
                tableCell.Controls.Add(_lbl2);
                tableRow.Cells.Add(tableCell);

                //倍数
                tableCell = new TableCell();
                tableCell.HorizontalAlign = HorizontalAlign.Right;
                tableCell.Text = "-";
                tableRow.Cells.Add(tableCell);

                //CSV出力用
                tableCell = new TableCell();
                tableCell.Text = strCSV;
                tableCell.Visible = false;
                tableRow.Cells.Add(tableCell);

                tblFire.Rows.Add(tableRow);

                i++;
            }

            if (type == "Update")
            {
                Session["TblFireListUpd"] = tblFireClsList;
            }
            else
            {
                //List<tblFireClass> tblFireClass1 = new List<tblFireClass>();
                //tblFireClass1 = (List<tblFireClass>)Session["TBLFIRECLASS"];

                Session["TBLFIRECLASS"] = tblFireClsList;
            }
        }
        /// <summary>
        /// Cellの結合(行)
        /// </summary>
        /// <param name="Col">対象列</param>
        private void CellsJoinUpd(Table tblTarget, int col)
        {
            TableCell tableCell = new TableCell();
            string strTempData = string.Empty;
            int intRowCnt = 0;
            int intIndex = 1;
            int intSpanCnt = 1;
            foreach (TableRow row in tblTarget.Rows)
            {
                if (intRowCnt > 0) //ヘッダ行の次行から処理対象
                {
                    if (row.Cells[col].Text == strTempData)
                    {
                        tableCell = tblTarget.Rows[intRowCnt].Cells[col];

                        intSpanCnt++;
                        if (row.Cells[col].Text != "")
                        {
                            tblTarget.Rows[intRowCnt].Cells.Remove(tableCell);
                            tblTarget.Rows[intIndex].Cells[col].RowSpan = intSpanCnt;
                        }
                    }
                    else
                    {
                        strTempData = row.Cells[col].Text;
                        intSpanCnt = 1;
                        intIndex = intRowCnt;

                    }
                }
                intRowCnt++;
            }
        }

        private DataTable GetFireDataType1()
        {
            string sql = null;
            Hashtable _params = new Hashtable();
            DataTable dt;

            StringBuilder sbSql = new StringBuilder();

            // SELECT SQL生成
            sbSql.Append(" SELECT M.FIRE_SIZE ");
            sbSql.Append(" ,U.UNIT_NAME");
            sbSql.Append(" ,ISNULL(U.FACTOR,1) AS FACTOR");
            sbSql.Append(" ,V.FIRE_ID");
            sbSql.Append(" ,V.CLASS_ID");
            sbSql.Append(" ,V.FIRE_NAME");
            sbSql.Append(" ,V.FIRE_NAME_BASE");
            sbSql.Append(" ,V.FIRE_NAME_LAST");
            sbSql.Append(" ,V.FIRE_ICON_PATH");
            sbSql.Append(" ,V.SORT_ORDER");
            sbSql.Append(" ,V.P_FIRE_ID");
            sbSql.Append(" ,V.DEL_FLAG");
            sbSql.Append(" FROM ");
            sbSql.Append(" V_FIRE V");
            sbSql.Append(" LEFT JOIN M_REGULATION M ON V.FIRE_ID = M.REG_TYPE_ID ");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U ON M.UNITSIZE_ID = U.UNITSIZE_ID");
            sbSql.Append(" WHERE V.FIRE_NAME LIKE '%第%類%'");
            sbSql.Append(" AND M.FIRE_SIZE IS NOT NULL");
            sbSql.Append(" AND V.DEL_FLAG = 0 ");
            sbSql.Append(" ORDER BY REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SUBSTRING(FIRE_NAME,0,CHARINDEX('類/',FIRE_NAME) + 1),'一',1),'二','2'),'三','3'),'四','4'),'五','5'),'六','6'),FIRE_SIZE,SORT_ORDER ");
            sql = sbSql.ToString();

            dt = DbUtil.Select(sql, _params);

            return dt;
        }

        private DataTable GetFireDataType2()
        {
            string sql = null;
            Hashtable _params = new Hashtable();
            DataTable dt;

            StringBuilder sbSql = new StringBuilder();

            // SELECT SQL生成
            sbSql.Append(" SELECT M.FIRE_SIZE ");
            sbSql.Append(" ,U.UNIT_NAME");
            sbSql.Append(" ,ISNULL(U.FACTOR,1) AS FACTOR");
            sbSql.Append(" ,V.FIRE_ID");
            sbSql.Append(" ,V.CLASS_ID");
            sbSql.Append(" ,V.FIRE_NAME");
            sbSql.Append(" ,V.FIRE_NAME_BASE");
            sbSql.Append(" ,V.FIRE_NAME_LAST");
            sbSql.Append(" ,V.FIRE_ICON_PATH");
            sbSql.Append(" ,V.SORT_ORDER");
            sbSql.Append(" ,V.P_FIRE_ID");
            sbSql.Append(" ,V.DEL_FLAG");
            sbSql.Append(" FROM ");
            sbSql.Append(" V_FIRE V");
            sbSql.Append(" LEFT JOIN M_REGULATION M ON V.FIRE_ID = M.REG_TYPE_ID ");
            sbSql.Append(" LEFT JOIN M_UNITSIZE U ON M.UNITSIZE_ID = U.UNITSIZE_ID");
            sbSql.Append(" WHERE V.FIRE_NAME LIKE '%指定可燃物/%'");
            //sbSql.Append(" AND M.FIRE_SIZE IS NOT NULL");
            sbSql.Append(" AND V.DEL_FLAG = 0 ");
            sbSql.Append(" ORDER BY FIRE_NAME ");
            sql = sbSql.ToString();

            dt = DbUtil.Select(sql, _params);

            return dt;
        }

        private M_FIRE GetFireData(M_FIRE mFire)
        {
            string sql = null;
            Hashtable _params = new Hashtable();
            DataTable dt;

            // Sql取得
            GetFireSql(ref sql, ref _params);

            dt = DbUtil.Select(sql, _params);

            foreach (DataRow _row in dt.Rows)
            {
                for (int i = 0; i < tblFire.Rows.Count - 1; i++)
                {
                    HiddenField _hdnRegTypeId = (HiddenField)tblFire.FindControl("hdnRegTypeId" + i);
                    if (_hdnRegTypeId != null)
                    {
                        if (_hdnRegTypeId.Value == _row["REG_TYPE_ID"].ToString())
                        {
                            TextBox _txtFireSize = (TextBox)tblFire.FindControl("txtFireSize" + i);
                            _txtFireSize.Text = Convert.ToString(Convert.ToDecimal(_row["FIRE_SIZE"]).ToString("F4"));

                            HiddenField _hdn = (HiddenField)tblFire.FindControl("hdnFireSize" + i);
                            Label _lblMultiple = (Label)tblFire.FindControl("lblMultiple" + i);

                            if (Common.IsNumber(_txtFireSize.Text) == false)
                            {
                                _txtFireSize.Text = string.Empty;
                                _lblMultiple.Text = string.Empty;
                                return mFire;
                            }

                            if (_lblMultiple != null)
                            {
                                if (_txtFireSize.Text.Trim() == "")
                                {
                                    _lblMultiple.Text = string.Empty;
                                }
                                else
                                {
                                    _lblMultiple.Text = Convert.ToString(Math.Round(Convert.ToDecimal(_txtFireSize.Text) / Convert.ToDecimal(_hdn.Value), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                                }
                            }
                            break;
                        }
                    }
                }
            }

            //mFire = FireTotal(mFire); //消防法危険物合計処理
            return mFire;
        }

        /// 検索用SQLを取得します。
        /// </summary>
        /// <param name="sql">作成SQL文</param>
        /// <param name="_params">バインド変数格納Hashtable</param>
        private void GetFireSql(ref string sql, ref Hashtable _params)
        {
            StringBuilder sbWhere = new StringBuilder();
            StringBuilder sbSql = new StringBuilder();

            // SELECT SQL生成
            sbSql.Append(" select");
            sbSql.Append(" REPORT_ID");
            sbSql.Append(" ,REG_TYPE_ID");
            sbSql.Append(" ,FIRE_SIZE");
            sbSql.Append(" from");
            sbSql.Append("   M_FIRE_SIZE FS");
            sbSql.Append(" where");
            sbSql.Append("   FS.REPORT_ID = " + Convert.ToString(Session[SessionConst.MasterKeys]) + "");
            sql = sbSql.ToString();
        }

    }
}