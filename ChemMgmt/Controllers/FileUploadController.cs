﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using ZyCoaG.Classes;
namespace ChemMgmt.Controllers
{
    public class FileUploadController : ApiController
    {
        //[HttpGet]
        //public string Post()
        //{
        //    return "hi";
        //}
        public async Task<string> Post()
        {
            if (!Request.Content.IsMimeMultipartContent()) return string.Empty;

            var rootPath = Path.GetTempPath();
            var provider = new MultipartFormDataStreamProvider(rootPath);

            await Request.Content.ReadAsMultipartAsync(provider);

            var guid = Guid.NewGuid().ToString();

            var originalFileName = provider.FormData["fileName"].ToString();
            var hiddenFieldId = provider.FormData["HiddenFieldId"].ToString();

            string split = string.Empty;
            if (hiddenFieldId.Contains("_"))
            {
                split = hiddenFieldId.Split('_')[0];
            }



            foreach (var file in provider.FileData)
            {
                if (file.Headers.ContentDisposition.Name.Contains(split))
                {
                    MvcApplication.UploadFiles.TryAdd(guid, new UploadFileInfo(file.LocalFileName, originalFileName));
                    break;
                }
            }

            return guid;
        }

        //public async Task<string> Post()
        //{
        //    try
        //    {
        //        if (!Request.Content.IsMimeMultipartContent()) return string.Empty;
        //        //var rootPath = HttpContext.Current.Server.MapPath();
        //        var rootPath = Path.GetTempPath();

        //        var provider = new MultipartFormDataStreamProvider(rootPath);

        //        await Request.Content.ReadAsMultipartAsync(provider);


        //        //var guid = Guid.NewGuid().ToString();

        //        //var originalFileName = provider.FormData["SDS"].ToString();//

        //        //var hiddenFieldId = provider.FormData["HiddenFieldId"].ToString();

        //        //string split = string.Empty;
        //        //if (hiddenFieldId.Contains("_"))
        //        //{
        //        //    split = hiddenFieldId.Split('_')[0];
        //        //}

        //        foreach (var file in provider.FileData)
        //        {
        //            rootPath = Path.GetTempPath();

        //            var guid = Guid.NewGuid().ToString();

        //            string originalFileName = file.Headers.ContentDisposition.FileName.Replace("\"", "");
        //            string path = Path.Combine(rootPath, guid);

        //            savefilepath.Add(path.ToString());

        //            MvcApplication.UploadFiles.TryAdd(guid, new UploadFileInfo(path, originalFileName));

        //            break;

        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        var messeage = ex.Message;
        //        var st = "";

        //    }
        //    var guid1 = Guid.NewGuid().ToString();
        //    return guid1;
        //}
    }
}
