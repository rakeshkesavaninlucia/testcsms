﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.Extensions;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Nikon.History;
using Dapper;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.BaseCommon;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.WorkRecord;
using ZyCoaG.Util;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using NPOI.HSSF.UserModel;
using ChemMgmt.Repository;
using ZyCoaG.util;
using ChemMgmt.Classes.util;
using System.Reflection;

namespace ChemMgmt.Controllers
{
    [Authorize]
    [HandleError]
    public class ZC130Controller : Controller
    {
        InventoryManagement dropDownList = new InventoryManagement();
        private UserInfo.ClsUser _clsUser = new UserInfo.ClsUser();
        IInventoryRepo inventoryRepo = new InventoryDao();
        private const string PROGRAM_ID = "ZC13030";
        LogUtil logutil = new LogUtil();
        protected string SearchConditionCsv { get; set; } = string.Empty;
        public ActionResult ZC13010(string FlowCode)
        {
            dropDownList = new InventoryManagement();
            dropDownList = DropDownFill(dropDownList);
            dropDownList.FlowCode = FlowCode;
            return View(dropDownList);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 25-9-2019
        /// Description : This  Method is to search the chem search
        /// <summary>
        public ActionResult ZC13020(string sortdir, string sort, string mode)
        {
            InventoryManagement inventory = new InventoryManagement();
            if ((sortdir != null && sort != null) || mode != null)
            {
                if (Session[SessionConst.CategorySelectReturnValue] != null)
                {
                    inventory.labelMode = mode;
                }
                if (Session["JobRecordSearchResult"] != null)
                {
                    inventory = (InventoryManagement)Session["JobRecordSearchResult"];
                    //dropDownList.listInventoryManagement = getStock(dropDownList);
                }

            }
            //inventory.listWorkRecordModel = null;
            inventory = DropDownFill(inventory);
            //Session["JobRecordSearchResult"] = null;
            return View(inventory);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 25-9-2019
        /// Description : This  Method is to load the chem search
        /// <summary>
        public ActionResult ZC13030()
        {
            dropDownList = new InventoryManagement();
            dropDownList = DropDownFill(dropDownList);
            return View(dropDownList);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 25-9-2019
        /// Description : This  Method is to get the chem search values
        /// <summary>
        [HttpPost]
        public ActionResult ZC13010(InventoryManagement inventoryManagement)
        {
            ChemUsageHistorySearchModel condition = new ChemUsageHistorySearchModel();
            try
            {
                if (Request.Form["ListOutput"] != null)
                {
                    if (Request.Form["LocationSelection"] != null)
                    {
                        inventoryManagement.hdnLocationIds = Request.Form["LocationSelection"];
                    }
                    if (Request.Form["RegulationSelection"] != null)
                    {
                        inventoryManagement.hdnRegulationIds = Request.Form["RegulationSelection"];
                    }
                    if (Request.Form["USER_SELECTION"] != null)
                    {
                        inventoryManagement.hdnGroupIds = Request.Form["USER_SELECTION"];
                    }

                    condition.PRODUCT_NAME_JP = inventoryManagement.PRODUCT_NAME_JP.UnityWidth() == null ? null : inventoryManagement.PRODUCT_NAME_JP.UnityWidth().Trim();
                    condition.PRODUCT_NAME_CONDITION = Convert.ToInt32(inventoryManagement.PRODUCT_NAME_CONDITION);
                    condition.LOCATION_SELECTION = inventoryManagement.hdnLocationIds;
                    condition.REG_SELECTION = inventoryManagement.hdnRegulationIds;
                    condition.GROUP_SELECTION = inventoryManagement.hdnGroupIds;

                    if (condition.GROUP_SELECTION != "")
                    {
                        condition.GROUP_SELECTION = string.Join(SystemConst.SelectionsDelimiter, condition.GROUP_SELECTION);
                        var ret = GetGroupList().Where(x => condition.GROUP_SELECTION.Split(',').Contains(x.Id));
                        System.Collections.ArrayList _list = new System.Collections.ArrayList();
                        foreach (var result in ret)
                        {
                            _list.Add(result.Name);
                        }
                        condition.GROUP_SELECTION = string.Join(SystemConst.SelectionsDelimiter, _list.ToArray());
                    }

                    if (condition.REG_SELECTION != "")
                    {
                        condition.REG_SELECTION = string.Join(SystemConst.SelectionsDelimiter, condition.REG_SELECTION);
                    }

                    if (condition.LOCATION_SELECTION != "")
                    {
                        condition.LOCATION_SELECTION = string.Join(SystemConst.SelectionsDelimiter, condition.LOCATION_SELECTION);
                    }

                    condition.BARCODE = inventoryManagement.BARCODE == null ? null : inventoryManagement.BARCODE.Trim();
                    condition.CAS_NO = inventoryManagement.CAS_NO == null ? null : inventoryManagement.CAS_NO.Trim();
                    condition.REG_NO_CONDITION = Convert.ToInt32(inventoryManagement.REG_NO_CONDITION);
                    condition.REG_NO = inventoryManagement.REG_NO == null ? null : inventoryManagement.REG_NO.Trim();
                    condition.BARCODE_CONDITION = Convert.ToInt32(inventoryManagement.BARCODE_CONDITION);
                    condition.USER_NAME = inventoryManagement.USER_NAME == null ? null : inventoryManagement.USER_NAME.Trim();
                    condition.USER_NAME_CONDITION = Convert.ToInt32(inventoryManagement.USER_NAME_CONDITION);
                    condition.OCCURRENCE_START_DATE = inventoryManagement.RECEIPT_START_DATE == null ? null : inventoryManagement.RECEIPT_START_DATE.Trim();
                    condition.OCCURRENCE_END_DATE = inventoryManagement.RECEIPT_END_DATE == null ? null : inventoryManagement.RECEIPT_END_DATE.Trim();
                    condition.IncludesContained = Convert.ToInt32(inventoryManagement.IncludesContained);

                    string ValidateMessage = validateCondition(condition);

                    if (ValidateMessage != null)
                    {
                        TempData["ErrorMessages"] = ValidateMessage;
                        inventoryManagement = DropDownFill(inventoryManagement);
                        return View(inventoryManagement);
                    }
                    else
                    {
                        using (var chemUsageHistory = new ChemUsageHistoryDataInfo())
                        {
                            inventoryManagement.listchemusageModel = chemUsageHistory.GetSearchResult(condition);
                            SearchConditionCsv = chemUsageHistory.Csv;
                        }
                        if (inventoryManagement.listchemusageModel != null && inventoryManagement.listchemusageModel.Count() == 0)
                        {
                            TempData["ErrorMessages"] = string.Format(WarningMessages.NotFound, "使用履歴");
                            inventoryManagement = DropDownFill(inventoryManagement);
                            return View(inventoryManagement);
                        }
                    }

                    if (inventoryManagement.listchemusageModel != null && inventoryManagement.listchemusageModel.Count > 0)
                    {
                        var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        if (inventoryManagement.listchemusageModel != null & inventoryManagement.listchemusageModel.Count > 0)
                        {
                            using (var csv = new StreamWriter(csvFileFullPath, false, Encoding.GetEncoding("shift_jis")))
                            {
                                csv.Write(SearchConditionCsv);
                                csv.Close();
                            }
                        }
                        string csvFilename = "使用履歴検索結果.csv";
                        ChemCompositeOutputCSV(inventoryManagement.listchemusageModel, csvFileFullPath, false, false);
                        string replaceCsvFileName = csvFilename.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                        TempData["SuccessMessages"] = "SuccessMessages";
                    }
                }
                else if (Request.Form["Clear"] != null)
                {
                    if (!string.IsNullOrEmpty(inventoryManagement.FlowCode))
                    {
                        return RedirectToAction("ZC13010", "ZC130", new { FlowCode = inventoryManagement.FlowCode });
                    }
                    else
                    {
                        return RedirectToAction("ZC13010", "ZC130");
                    }
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            inventoryManagement = DropDownFill(inventoryManagement);
            return View(inventoryManagement);
        }


        /// <summary>
        /// 検索条件の入力内容を検証します。
        /// </summary>
        /// <param name="condition">検証する検索条件。</param>
        /// <returns>検証した結果を<see cref="IEnumerable{string}"/>でを返します。</returns>
        private string validateCondition(ChemUsageHistorySearchModel condition)
        {
            string ErrorStatus = null;
            DateTime validateStartDate = default(DateTime);
            DateTime validateEndDate = default(DateTime);
            if (!string.IsNullOrWhiteSpace(condition.OCCURRENCE_START_DATE))
            {
                if (!DateTime.TryParse(condition.OCCURRENCE_START_DATE, out validateStartDate))
                {
                    ErrorStatus = string.Format(WarningMessages.NotDate, "発生開始日") + "\n";
                }
            }
            if (!string.IsNullOrWhiteSpace(condition.OCCURRENCE_END_DATE))
            {
                if (!DateTime.TryParse(condition.OCCURRENCE_END_DATE, out validateEndDate))
                {
                    ErrorStatus = string.Format(WarningMessages.NotDate, "発生終了日") + "\n";
                }
            }
            if (!string.IsNullOrWhiteSpace(condition.OCCURRENCE_START_DATE) && !string.IsNullOrWhiteSpace(condition.OCCURRENCE_END_DATE))
            {
                if (validateStartDate > validateEndDate)
                {
                    ErrorStatus = string.Format(WarningMessages.ErrorToPeriod, "発生日") + "\n";
                }
            }
            return ErrorStatus;
        }

        /// <summary>
        /// 部署マスターからデータを取得します。
        /// </summary>
        /// <returns>部署マスターデータを取得します。</returns>
        public virtual IEnumerable<CommonDataModel<string>> GetGroupList() => new GroupMasterInfo().GetSelectList();

        public string ValidateCountComposite(InventoryManagement inventoryManagement)
        {
            ChemUsageHistorySearchModel condition = new ChemUsageHistorySearchModel();
            string CountConfirmationMessage = null;
            try
            {

                if (Request.Form["LocationSelection"] != null)
                {
                    inventoryManagement.hdnLocationIds = Request.Form["LocationSelection"];
                }
                if (Request.Form["RegulationSelection"] != null)
                {
                    inventoryManagement.hdnRegulationIds = Request.Form["RegulationSelection"];
                }
                if (Request.Form["USER_SELECTION"] != null)
                {
                    inventoryManagement.hdnGroupIds = Request.Form["USER_SELECTION"];
                }

                condition.PRODUCT_NAME_JP = inventoryManagement.PRODUCT_NAME_JP.UnityWidth() == null ? null : inventoryManagement.PRODUCT_NAME_JP.UnityWidth().Trim();
                condition.PRODUCT_NAME_CONDITION = Convert.ToInt32(inventoryManagement.PRODUCT_NAME_CONDITION);
                condition.LOCATION_SELECTION = inventoryManagement.hdnLocationIds;
                condition.REG_SELECTION = inventoryManagement.hdnRegulationIds;
                condition.GROUP_SELECTION = inventoryManagement.hdnGroupIds;

                if (condition.GROUP_SELECTION != "")
                {
                    condition.GROUP_SELECTION = string.Join(SystemConst.SelectionsDelimiter, condition.GROUP_SELECTION);
                    var ret = GetGroupList().Where(x => condition.GROUP_SELECTION.Split(',').Contains(x.Id));
                    System.Collections.ArrayList _list = new System.Collections.ArrayList();
                    foreach (var result in ret)
                    {
                        _list.Add(result.Name);
                    }
                    condition.GROUP_SELECTION = string.Join(SystemConst.SelectionsDelimiter, _list.ToArray());
                }

                if (condition.REG_SELECTION != "")
                {
                    condition.REG_SELECTION = string.Join(SystemConst.SelectionsDelimiter, condition.REG_SELECTION);
                }

                if (condition.LOCATION_SELECTION != "")
                {
                    condition.LOCATION_SELECTION = string.Join(SystemConst.SelectionsDelimiter, condition.LOCATION_SELECTION);
                }

                condition.BARCODE = inventoryManagement.BARCODE == null ? null : inventoryManagement.BARCODE.Trim();
                condition.CAS_NO = inventoryManagement.CAS_NO == null ? null : inventoryManagement.CAS_NO.Trim();
                condition.REG_NO_CONDITION = Convert.ToInt32(inventoryManagement.REG_NO_CONDITION);
                condition.REG_NO = inventoryManagement.REG_NO == null ? null : inventoryManagement.REG_NO.Trim();
                condition.BARCODE_CONDITION = Convert.ToInt32(inventoryManagement.BARCODE_CONDITION);
                condition.USER_NAME = inventoryManagement.USER_NAME == null ? null : inventoryManagement.USER_NAME.Trim();
                condition.USER_NAME_CONDITION = Convert.ToInt32(inventoryManagement.USER_NAME_CONDITION);
                condition.OCCURRENCE_START_DATE = inventoryManagement.RECEIPT_START_DATE == null ? null : inventoryManagement.RECEIPT_START_DATE.Trim();
                condition.OCCURRENCE_END_DATE = inventoryManagement.RECEIPT_END_DATE == null ? null : inventoryManagement.RECEIPT_END_DATE.Trim();
                condition.IncludesContained = Convert.ToInt32(inventoryManagement.IncludesContained);


                using (var chemUsageHistory = new ChemUsageHistoryDataInfo())
                {
                    var searchResultCount = chemUsageHistory.GetSearchResultValuesCount(condition);
                    int MaxSearchResultCount = new SystemParameters().MaxStockResultCount;
                    if (searchResultCount[0] > MaxSearchResultCount)
                    {
                        CountConfirmationMessage = string.Format("{0}件以上({1}件)ヒットしました。{2}しますか？", MaxSearchResultCount.ToString(), searchResultCount[0].ToString(), "リスト出力");
                    }
                }

            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return CountConfirmationMessage;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 25-9-2019
        /// Description : This  Method is to get the list output values of chem search values
        /// <summary>
        [HttpPost]
        public ActionResult ZC13020(InventoryManagement inventoryManagement)
        {
            try
            {
                if (Request.Form["Search"] != null)
                {
                    if (Request.Form["LocationSelection"] != null)
                    {
                        inventoryManagement.hdnLocationIds = Request.Form["LocationSelection"];
                    }
                    if (Request.Form["RegulationSelection"] != null)
                    {
                        inventoryManagement.hdnRegulationIds = Request.Form["RegulationSelection"];
                    }
                    if (Request.Form["USER_SELECTION"] != null)
                    {
                        inventoryManagement.hdnGroupIds = Request.Form["USER_SELECTION"];
                    }

                    inventoryManagement.listWorkRecordModel = GetSearchResult(inventoryManagement);
                    Session["JobRecordSearchResult"] = inventoryManagement;
                }
                else if (Request.Form["ListOutput"] != null)
                {
                    //if (Session["JobRecordSearchResult"] != null)
                    //{
                    //InventoryManagement inventorymanagement = new InventoryManagement();
                    //inventorymanagement = (InventoryManagement)Session["JobRecordSearchResult"];
                    inventoryManagement.listWorkRecordModel = GetSearchResult(inventoryManagement);
                    if (inventoryManagement.listWorkRecordModel != null && inventoryManagement.listWorkRecordModel.Count > 0)
                    {
                        var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                        if (inventoryManagement.listWorkRecordModel != null & inventoryManagement.listWorkRecordModel.Count > 0)
                        {
                            using (var csv = new StreamWriter(csvFileFullPath, false, Encoding.GetEncoding("shift_jis")))
                            {
                                csv.Write(SearchConditionCsv);
                                csv.Close();
                            }
                        }
                        string csvFilename = "作業記録検索結果.csv";
                        OutputCSV(inventoryManagement.listWorkRecordModel, csvFileFullPath, false, false);
                        string replaceCsvFileName = csvFilename.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                        Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                    }
                    //}
                }
                else if (Request.Form["Clear"] != null)
                {
                    Session["JobRecordSearchResult"] = null;
                    return RedirectToAction("ZC13020", "ZC130");
                }
                inventoryManagement = DropDownFill(inventoryManagement);
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return View(inventoryManagement);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 25-9-2019
        /// Description : This  Method is to load the excel sheet values
        /// <summary>
        [HttpPost]
        public ActionResult ZC13030(InventoryManagement inventoryManagement)
        {
            string excelFilePath = null;
            try
            {
                if (Request.Form["btnTotal"] != null)
                {

                    if (Request.Form["LocationSelection"] != null)
                    {
                        inventoryManagement.hdnLocationIds = Request.Form["LocationSelection"];
                    }
                    if (Request.Form["USER_SELECTION"] != null)
                    {
                        inventoryManagement.hdnGroupIds = Request.Form["USER_SELECTION"];
                    }
                    if (inventoryManagement.ddlLocation == null)
                    {
                        inventoryManagement.ddlLocation = Request.Form["hdnddllocation"];
                    }
                    int intDataCnt = 0;

                    List<InventoryManagement> dt = inventoryRepo.DoSearchSheet1(inventoryManagement);
                    List<InventoryManagement> dt2 = inventoryRepo.DoSearchSheet2(inventoryManagement);

                    intDataCnt = dt.Count + dt2.Count;

                    List<InventoryManagement> dt3 = null;
                    if (inventoryManagement.txtReportName != null)
                    {
                        dt3 = inventoryRepo.DoSearchSheet3(inventoryManagement);
                        intDataCnt = intDataCnt + dt3.Count;
                    }

                    if (intDataCnt == 0)
                    {
                        TempData["NotFoundMessage"] = string.Format(WarningMessages.NotFound, "危険物集計");
                        inventoryManagement = DropDownFill(inventoryManagement);
                        return View(inventoryManagement);
                    }

                    //Excelファイル作成
                    var workbook = new XSSFWorkbook();
                    CreateSheet1(dt, ref workbook);
                    CreateSheet2(dt2, ref workbook);

                    if (inventoryManagement.txtReportName != null)
                    {
                        CreateSheet3(dt2, dt3, ref workbook);
                    }

                    // ファイルを保存します。
                    excelFilePath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                    using (var fileStream = new FileStream(excelFilePath, FileMode.OpenOrCreate, FileAccess.Write))
                    {
                        workbook.Write(fileStream);
                    }

                    workbook = null;
                }
                else if (Request.Form["btnClear"] != null)
                {
                    return RedirectToAction("ZC13030", "ZC130");
                }

                inventoryManagement = DropDownFill(inventoryManagement);

                Control.FileDownload(excelFilePath, "危険物集計_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx");

            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
                //Common.transErrorAspx(this, PROGRAM_ID, ex.ToString(), true);

            }
            return View(inventoryManagement);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 25-9-2019
        /// Description : This  Method is to get the search result values
        /// <summary>
        public List<ChemUsageHistoryModel> GetSearchResult(ChemUsageHistorySearchModel condition)
        {
            try
            {
                condition.PRODUCT_NAME_JP = condition.PRODUCT_NAME_JP.UnityWidth();
                ChemUsageHistoryDataInfo chemUsageHistory = new ChemUsageHistoryDataInfo();
                var searchResultCount = chemUsageHistory.GetSearchResultCount(condition);
                var chemUsageHistoryViewInfo = chemUsageHistory.GetSearchResult(condition);
                var chemUsageHistoryList = chemUsageHistoryViewInfo.ToList();
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return new List<ChemUsageHistoryModel>();
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 22-9-2019
        /// Description : This  Method is to load the  dropdown values
        /// <summary>
        public InventoryManagement DropDownFill(InventoryManagement inventoryManagement)
        {
            try
            {
                inventoryManagement.LookUpCSNCondition = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.CSNCONDITION];
                inventoryManagement.LookUpBarcodeCondition = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.BOTTLENUMCONDITION];
                inventoryManagement.LookUpInclude = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.INCLUDE];
                inventoryManagement.LookUpDDLLocation = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.DDLLOCATION];
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return inventoryManagement;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 22-9-2019
        /// Description : This  Method is to get the search result values
        /// <summary>
        public List<WorkRecordModel> GetSearchResult(InventoryManagement inventoryManagement)
        {
            try
            {
                WorkRecordSearchModel condition = new WorkRecordSearchModel();
                using (var workRecord = new WorkRecordDataInfo())
                {
                    condition.PRODUCT_NAME_JP = inventoryManagement.PRODUCT_NAME_JP.UnityWidth() == null ? null : inventoryManagement.PRODUCT_NAME_JP.UnityWidth().Trim();
                    condition.PRODUCT_NAME_CONDITION = Convert.ToInt32(inventoryManagement.PRODUCT_NAME_CONDITION);
                    condition.BARCODE_CONDITION = Convert.ToInt32(inventoryManagement.BARCODE_CONDITION);
                    condition.BARCODE = inventoryManagement.BARCODE == null ? null : inventoryManagement.BARCODE.Trim();
                    condition.CAS_NO = inventoryManagement.CAS_NO == null ? null : inventoryManagement.CAS_NO.Trim();
                    condition.REG_NO_CONDITION = Convert.ToInt32(inventoryManagement.REG_NO_CONDITION);
                    condition.REG_NO = inventoryManagement.REG_NO == null ? null : inventoryManagement.REG_NO.Trim();
                    condition.USER_NAME_CONDITION = Convert.ToInt32(inventoryManagement.USER_NAME_CONDITION);
                    condition.USER_NAME = inventoryManagement.USER_NAME == null ? null : inventoryManagement.USER_NAME.Trim(); ;
                    condition.RECEIPT_START_DATE = inventoryManagement.RECEIPT_START_DATE == null ? null : inventoryManagement.RECEIPT_START_DATE.Trim();
                    condition.RECEIPT_END_DATE = inventoryManagement.RECEIPT_END_DATE == null ? null : inventoryManagement.RECEIPT_END_DATE.Trim();
                    condition.ISSUE_START_DATE = inventoryManagement.ISSUE_START_DATE == null ? null : inventoryManagement.ISSUE_START_DATE.Trim();
                    condition.ISSUE_END_DATE = inventoryManagement.ISSUE_END_DATE == null ? null : inventoryManagement.ISSUE_END_DATE.Trim();
                    condition.IncludesContained = Convert.ToInt32(inventoryManagement.IncludesContained);
                    condition.hdnRegulationIds = inventoryManagement.hdnRegulationIds;
                    condition.hdnLocationIds = inventoryManagement.hdnLocationIds;
                    condition.hdnGroupIds = inventoryManagement.hdnGroupIds;
                    if (condition.hdnGroupIds != null)
                    {
                        var ret = GetGroupList().Where(x => condition.hdnGroupIds.Split(',').Contains(x.Id));

                        System.Collections.ArrayList _list = new System.Collections.ArrayList();
                        foreach (var result in ret)
                        {
                            _list.Add(result.Name);
                        }
                        condition.hdnGroupIds = string.Join(SystemConst.SelectionsDelimiter, _list.ToArray());
                    }

                    var searchResultCount = workRecord.GetSearchResult(condition);
                    if (searchResultCount.Count() > 0)
                    {
                        SearchConditionCsv = workRecord.Csv;
                        return searchResultCount.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return new List<WorkRecordModel>().AsQueryable().ToList();
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 22-9-2019
        /// Description : This  Method is to get the login information
        /// <summary>
        private void getInputData()
        {
            UserInfo.ClsUser userInfo = null;
            userInfo.USER_CD = Common.GetLogonForm().Id;
            userInfo.USER_CD = Common.GetLogonForm().REFERENCE_AUTHORITY;
            var condition = new WorkRecordSearchModel();
            {
                var classTools = new ClassTools<WorkRecordSearchModel>(condition);
                //condition = classTools.GetControlValues(SearchFormView);
                condition.REFERENCE_AUTHORITY = userInfo.REFERENCE_AUTHORITY;
                condition.LOGIN_USER_CD = userInfo.USER_CD;
            }
            Session.Add(nameof(WorkRecordSearchModel), condition);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 26-9-2019
        /// Description : This  Method is to get the list output values
        /// <summary>
        public static void OutputCSV(List<WorkRecordModel> g, string fileFullPath, bool isHeaderDataColumnName, bool isDoubleQuotation)
        {
            var lines = new List<string>();
            var line = new List<string>();
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {

                for (int I = 0; I < g.Count; I++)
                {
                    string IssueDate = ""; string RECEIPT_DATE = ""; string upddate = "";
                    if (g[I].ISSUE_DATE != null)
                    {
                        DateTime IssueDates = Convert.ToDateTime(g[I].ISSUE_DATE);
                        IssueDate = IssueDates.ToString("yyyy-MM-dd");
                    }
                    if (g[I].RECEIPT_DATE != null)
                    {
                        DateTime ReceiptDates = Convert.ToDateTime(g[I].RECEIPT_DATE);
                        RECEIPT_DATE = ReceiptDates.ToString("yyyy-MM-dd");
                    }
                    if (g[I].UPD_DATE != null)
                    {
                        DateTime upddates = Convert.ToDateTime(g[I].UPD_DATE);
                        upddate = upddates.ToString("yyyy-MM-dd");

                    }
                    if (I == 0)
                    {
                        csv.WriteLine("修正区分" + "," + "登録番号" + "," + "旧安全衛生番号" + "," + "化学物質名" + "," + "CAS#" + "," + "瓶番号" + "," + "出庫日" + "," + "入庫日" + "," + "部署名" + "," + "作業者名" + "," + "NID" + "," + "使用場所" + "," + "使用目的" + "," + "使用目的(その他)" + "," + "ばく露作業の用途" + "," + "ばく露作業の種類" + "," + "使用時の温度" + "," + "作業時間（分)" + "," + "使用量" + "," + "使用量単位" + "," + "含有物質名" + "," + "含有率下限値" + "," + "含有率上限値" + "," + "特記事項" + "," + "法規制" + "," + "修正者" + "," + "修正日時" + "," + "修正理由" + "," + "備考");
                        csv.WriteLine("\"" + g[I].UPD_TYPE + "\"" + "," + "\"" + g[I].REG_NO + "\"" + "," + "\"" + g[I].REG_OLD_NO + "\"" + "," + "\"" + g[I].CHEM_NAME + "\"" + "," + "\"" + g[I].CAS_NO + "\"" + "," + "\"" + g[I].BARCODE + "\"" + "," + "\"" + IssueDate + "\"" + "," + "\"" + RECEIPT_DATE + "\"" + "," + "\"" + g[I].GROUP_NAME + "\"" + "," + "\"" + g[I].WORKER_USER_NM + "\"" + "," + "\"" + g[I].WORKER_USER_CD + "\"" + "," + "\"" + g[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g[I].INTENDED_NM + "\"" + "," + "\"" + g[I].INTENDED + "\"" + "," + "\"" + g[I].EXPOSURE_WORK_USE_NAME + "\"" + "," + "\"" + g[I].EXPOSURE_WORK_TYPE_NAME + "\"" + "," + "\"" + g[I].EXPOSURE_WORK_TEMPERATURE_NAME + "\"" + "," + "\"" + g[I].WORKING_TIMES + "\"" + "," + "\"" + g[I].UNITSIZE + "\"" + "," + "\"" + g[I].UNIT_NAME + "\"" + "," + "\"" + g[I].CONTAINED_NAME + "\"" + "," + "\"" + g[I].MIN_CONTAINED_RATE + "\"" + "," + "\"" + g[I].MAX_CONTAINED_RATE + "\"" + "," + "\"" + g[I].NOTICES + "\"" + "," + "\"" + g[I].REG_TEXT + "\"" + "," + "\"" + g[I].UPD_USER_NM + "\"" + "," + "\"" + upddate + "\"" + "," + "\"" + g[I].REASON_FOR_UPDATE + "\"" + "," + "\"" + g[I].MEMO + "\"");

                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].UPD_TYPE + "\"" + "," + "\"" + g[I].REG_NO + "\"" + "," + "\"" + g[I].REG_OLD_NO + "\"" + "," + "\"" + g[I].CHEM_NAME + "\"" + "," + "\"" + g[I].CAS_NO + "\"" + "," + "\"" + g[I].BARCODE + "\"" + "," + "\"" + IssueDate + "\"" + "," + "\"" + RECEIPT_DATE + "\"" + "," + "\"" + g[I].GROUP_NAME + "\"" + "," + "\"" + g[I].WORKER_USER_NM + "\"" + "," + "\"" + g[I].WORKER_USER_CD + "\"" + "," + "\"" + g[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g[I].INTENDED_NM + "\"" + "," + "\"" + g[I].INTENDED + "\"" + "," + "\"" + g[I].EXPOSURE_WORK_USE_NAME + "\"" + "," + "\"" + g[I].EXPOSURE_WORK_TYPE_NAME + "\"" + "," + "\"" + g[I].EXPOSURE_WORK_TEMPERATURE_NAME + "\"" + "," + "\"" + g[I].WORKING_TIMES + "\"" + "," + "\"" + g[I].UNITSIZE + "\"" + "," + "\"" + g[I].UNIT_NAME + "\"" + "," + "\"" + g[I].CONTAINED_NAME + "\"" + "," + "\"" + g[I].MIN_CONTAINED_RATE + "\"" + "," + "\"" + g[I].MAX_CONTAINED_RATE + "\"" + "," + "\"" + g[I].NOTICES + "\"" + "," + "\"" + g[I].REG_TEXT + "\"" + "," + "\"" + g[I].UPD_USER_NM + "\"" + "," + "\"" + upddate + "\"" + "," + "\"" + g[I].REASON_FOR_UPDATE + "\"" + "," + "\"" + g[I].MEMO + "\"");
                    }
                }
            }
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 26-9-2019
        /// Description : This  Method is to get the list output values
        /// <summary>
        public static void ChemCompositeOutputCSV(List<ChemUsageHistoryModel> g, string fileFullPath, bool isHeaderDataColumnName, bool isDoubleQuotation)
        {
            using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
            {

                for (int I = 0; I < g.Count; I++)
                {
                    string Regdate = "";
                    string volume = "";
                    string originalWeight = "";

                    if (g[I].REG_DATE != null)
                    {
                        DateTime regDate = Convert.ToDateTime(g[I].REG_DATE);
                        Regdate = regDate.ToString("yyyy-MM-dd HH:mm:ss");
                    }
                    if (g[I].CUR_GROSS_WEIGHT_G != null)
                    {
                        string[] Weight = g[I].CUR_GROSS_WEIGHT_G.ToString().Split('.');
                        originalWeight = Weight[0];
                        volume = "g";
                    }
                    if (I == 0)
                    {
                        csv.WriteLine("登録番号" + "," + "化学物質名" + "," + "CAS#" + "," + "瓶番号" + "," + "操作順" + "," + "発生日時" + "," + "操作" + "," + "部署名" + "," + "ユーザー名" + "," + "ユーザーコード" + "," + "保管場所" + "," + "使用場所" + "," + "使用目的ID" + "," + "使用目的名" + "," + "使用目的(その他)" + "," + "作業時間（分）" + "," + "重量履歴（風袋込）" + "," + "重量履歴単位" + "," + "法規制" + "," + "GHS分類" + "," + "備考");
                        csv.WriteLine("\"" + g[I].REG_NO + "\"" + "," + "\"" + g[I].PRODUCT_NAME_JP + "\"" + "," + "\"" + g[I].CAS_NO + "\"" + "," + "\"" + g[I].BARCODE + "\"" + "," + "\"" + g[I].ACTION_SEQ + "\"" + "," + "\"" + Regdate + "\"" + "," + "\"" + g[I].ACTION_TYPE_NAME + "\"" + "," + "\"" + g[I].GROUP_NAME + "\"" + "," + "\"" + g[I].USER_NAME + "\"" + "," + "\"" + g[I].USER_CD + "\"" + "," + "\"" + g[I].ACTION_LOC_NAME + "\"" + "," + "\"" + g[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g[I].INTENDED_USE_ID + "\"" + "," + "\"" + g[I].INTENDED_NM + "\"" + "," + "\"" + g[I].INTENDED + "\"" + "," + "\"" + g[I].WORKING_TIMES + "\"" + "," + "\"" + originalWeight + "\"" + "," + "\"" + volume + "\"" + "," + "\"" + g[I].REG_TEXT + "\"" + "," + "\"" + g[I].GHSCAT_NAME + "\"" + "," + "\"" + g[I].MEMO + "\"");

                    }
                    else
                    {
                        csv.WriteLine("\"" + g[I].REG_NO + "\"" + "," + "\"" + g[I].PRODUCT_NAME_JP + "\"" + "," + "\"" + g[I].CAS_NO + "\"" + "," + "\"" + g[I].BARCODE + "\"" + "," + "\"" + g[I].ACTION_SEQ + "\"" + "," + "\"" + Regdate + "\"" + "," + "\"" + g[I].ACTION_TYPE_NAME + "\"" + "," + "\"" + g[I].GROUP_NAME + "\"" + "," + "\"" + g[I].USER_NAME + "\"" + "," + "\"" + g[I].USER_CD + "\"" + "," + "\"" + g[I].ACTION_LOC_NAME + "\"" + "," + "\"" + g[I].USAGE_LOC_NAME + "\"" + "," + "\"" + g[I].INTENDED_USE_ID + "\"" + "," + "\"" + g[I].INTENDED_NM + "\"" + "," + "\"" + g[I].INTENDED + "\"" + "," + "\"" + g[I].WORKING_TIMES + "\"" + "," + "\"" + originalWeight + "\"" + "," + "\"" + volume + "\"" + "," + "\"" + g[I].REG_TEXT + "\"" + "," + "\"" + g[I].GHSCAT_NAME + "\"" + "," + "\"" + g[I].MEMO + "\"");
                    }
                }
            }
        }



        /// <summary>
        /// Author: MSCGI
        /// Created date: 26-9-2019
        /// Description : This  Method is to create the excel sheet1 values
        /// <summary>
        private void CreateSheet1(List<InventoryManagement> dt, ref XSSFWorkbook workbook)
        {
            try
            {
                var workSheet = workbook.CreateSheet("危険物一覧");
                {
                    var font = workbook.CreateFont();

                    font.FontHeightInPoints = ((short)10);

                    var style = workbook.CreateCellStyle();
                    style.Alignment = HorizontalAlignment.Center;
                    style.VerticalAlignment = VerticalAlignment.Center;
                    style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                    style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    style.SetFont(font);

                    var styleNum2 = workbook.CreateCellStyle();
                    styleNum2.Alignment = HorizontalAlignment.Center;
                    styleNum2.VerticalAlignment = VerticalAlignment.Center;
                    styleNum2.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                    styleNum2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    styleNum2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    styleNum2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    styleNum2.SetFont(font);
                    styleNum2.DataFormat = workbook.CreateDataFormat().GetFormat("0.00");

                    var styleNum4 = workbook.CreateCellStyle();
                    styleNum4.Alignment = HorizontalAlignment.Center;
                    styleNum4.VerticalAlignment = VerticalAlignment.Center;
                    styleNum4.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                    styleNum4.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    styleNum4.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    styleNum4.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    styleNum4.SetFont(font);
                    styleNum4.DataFormat = workbook.CreateDataFormat().GetFormat("0.0000");

                    var row = workSheet.CreateRow(0);
                    var cell = row.CreateCell(0);
                    var row1 = workSheet.CreateRow(1);
                    var cell1 = row1.CreateCell(0);
                    //cell1.CellStyle = style;

                    for (int i = 0; i < 17; i++)
                    {
                        cell = row.CreateCell(i);
                        cell.CellStyle = style;

                        cell1 = row1.CreateCell(i);
                        cell1.CellStyle = style;
                    }

                    //登録番号
                    var idReg = row.CreateCell(0);
                    idReg.SetCellValue("登録番号");
                    idReg.CellStyle = style;

                    workSheet.SetColumnWidth(0, 256 * 14);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 0, 0));

                    // 申請番号
                    var idCell = row.CreateCell(1);
                    idCell.SetCellValue("申請番号");
                    idCell.CellStyle = style;

                    workSheet.SetColumnWidth(1, 256 * 14);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 1, 1));

                    // 保管場所名
                    var idLoc = row.CreateCell(2);
                    idLoc.SetCellValue("保管場所名");
                    idLoc.CellStyle = style;

                    workSheet.SetColumnWidth(2, 256 * 60);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 2, 2));

                    // 保管場所バーコード
                    var nameCell = row.CreateCell(3);
                    nameCell.SetCellValue("保管場所バーコード");
                    nameCell.CellStyle = style;
                    cell1 = row1.CreateCell(3);
                    cell1.CellStyle = style;

                    workSheet.SetColumnWidth(3, 256 * 20);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 3, 3));

                    // 取扱商品
                    var Cell3 = row.CreateCell(4);
                    Cell3.SetCellValue("取扱商品");
                    Cell3.CellStyle = style;
                    cell1 = row1.CreateCell(4);
                    cell1.CellStyle = style;

                    workSheet.SetColumnWidth(4, 256 * 30);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 4, 4));

                    // 種別
                    var Cell4 = row.CreateCell(5);
                    Cell4.SetCellValue("危険物類");
                    Cell4.CellStyle = style;
                    cell1 = row1.CreateCell(5);
                    cell1.CellStyle = style;
                    cell1.SetCellValue("種別");

                    workSheet.SetColumnWidth(5, 256 * 6);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 5, 12));

                    // 類別
                    var Cell5 = row1.CreateCell(6);
                    Cell5.SetCellValue("類別");
                    Cell5.CellStyle = style;
                    workSheet.SetColumnWidth(6, 256 * 24);

                    // 指定数量
                    var Cell6 = row1.CreateCell(7);
                    Cell6.SetCellValue("指定数量");
                    Cell6.CellStyle = style;
                    workSheet.SetColumnWidth(7, 256 * 10);

                    // 最大保管量
                    var Cell7 = row1.CreateCell(8);
                    Cell7.SetCellValue("最大保管量(申請値)");
                    Cell7.CellStyle = style;
                    workSheet.SetColumnWidth(8, 256 * 12);

                    // 単位
                    var Cell8 = row1.CreateCell(9);
                    Cell8.SetCellValue("単位(申請値)");
                    Cell8.CellStyle = style;
                    workSheet.SetColumnWidth(9, 256 * 6);

                    // 最大保管量
                    var Cell9 = row1.CreateCell(10);
                    Cell9.SetCellValue("最大保管量(変換値)");
                    Cell9.CellStyle = style;
                    workSheet.SetColumnWidth(10, 256 * 12);

                    // 単位
                    var Cel10 = row1.CreateCell(11);
                    Cel10.SetCellValue("単位(変換値)");
                    Cel10.CellStyle = style;
                    workSheet.SetColumnWidth(11, 256 * 6);

                    // 指定数量
                    var Cell11 = row1.CreateCell(12);
                    Cell11.SetCellValue("最大倍数");
                    Cell11.CellStyle = style;
                    workSheet.SetColumnWidth(12, 256 * 10);

                    // 職場名
                    var Cell12 = row.CreateCell(13);
                    Cell12.SetCellValue("職場名");
                    Cell12.CellStyle = style;
                    cell1 = row1.CreateCell(13);
                    cell1.CellStyle = style;

                    workSheet.SetColumnWidth(13, 256 * 40);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 13, 13));

                    // 担当者
                    var Cell13 = row.CreateCell(14);
                    Cell13.SetCellValue("担当者");
                    Cell13.CellStyle = style;
                    cell1 = row1.CreateCell(14);
                    cell1.CellStyle = style;

                    workSheet.SetColumnWidth(14, 256 * 20);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 14, 14));

                    // 内線
                    var Cell14 = row.CreateCell(15);
                    Cell14.SetCellValue("内線");
                    Cell14.CellStyle = style;
                    cell1 = row1.CreateCell(15);
                    cell1.CellStyle = style;

                    workSheet.SetColumnWidth(15, 256 * 20);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 15, 15));

                    // 取扱商品
                    var Cell15 = row.CreateCell(16);
                    Cell15.SetCellValue("備考");
                    Cell15.CellStyle = style;
                    cell1 = row1.CreateCell(16);
                    cell1.CellStyle = style;

                    workSheet.SetColumnWidth(16, 256 * 30);
                    workSheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 16, 16));

                    int rowCnt = 2;
                    foreach (var dtRow in dt)
                    {
                        var rowData = workSheet.CreateRow(rowCnt);
                        var colData = rowData.CreateCell(0);
                        colData.SetCellValue(Convert.ToString(dtRow.REG_NO));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(1);
                        colData.SetCellValue(Convert.ToString(dtRow.RECENT_LEDGER_FLOW_ID));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(2);
                        colData.SetCellValue(Convert.ToString(dtRow.LOC_NAME));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(3);
                        colData.SetCellValue(Convert.ToString(dtRow.LOC_BARCODE));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(4);
                        colData.SetCellValue(Convert.ToString(dtRow.CHEM_NAME));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(5);
                        colData.SetCellValue(Convert.ToString(dtRow.REG_TEXT));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(6);
                        colData.SetCellValue(Convert.ToString(dtRow.REG_TEXT2));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(7);
                        if (dtRow.FIRE_SIZE != null)
                        {
                            colData.SetCellValue(Convert.ToDouble(Convert.ToDecimal(dtRow.FIRE_SIZE).ToString("0.###")));
                        }
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(8);
                        if (Convert.ToString(dtRow.AMOUNT_BASE) != "")
                        {
                            colData.SetCellValue(Convert.ToDouble(dtRow.AMOUNT_BASE));
                            colData.CellStyle = styleNum2;
                        }
                        else
                        {
                            colData.SetCellValue(Convert.ToString(dtRow.AMOUNT_BASE));
                            colData.CellStyle = style;
                        }
                        colData = rowData.CreateCell(9);
                        colData.SetCellValue(Convert.ToString(dtRow.UNIT_NAME));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(10);
                        if (Convert.ToString(dtRow.AMOUNT) != "")
                        {
                            colData.SetCellValue(Convert.ToDouble(dtRow.AMOUNT));
                            colData.CellStyle = styleNum2;
                        }
                        else
                        {
                            colData.SetCellValue(Convert.ToString(dtRow.AMOUNT));
                            colData.CellStyle = style;
                        }

                        colData = rowData.CreateCell(11);
                        colData.SetCellValue(Convert.ToString(dtRow.REGULATION_UNITNAME));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(12);
                        if (Common.IsNumber(Convert.ToString(dtRow.AMOUNT)) == true && Common.IsNumber(Convert.ToString(dtRow.FIRE_SIZE)) == true)
                        {
                            string strFireSize = Convert.ToString(Math.Round(Convert.ToDecimal(dtRow.AMOUNT) / Convert.ToDecimal(dtRow.FIRE_SIZE), 4, MidpointRounding.AwayFromZero).ToString("F4"));
                            if (Convert.ToString(strFireSize) != "")
                            {
                                colData.SetCellValue(Convert.ToDouble(strFireSize));
                                colData.CellStyle = styleNum4;
                            }
                            else
                            {
                                colData.SetCellValue(Convert.ToString(strFireSize));
                                colData.CellStyle = style;
                            }
                        }

                        colData = rowData.CreateCell(13);
                        colData.SetCellValue(Convert.ToString(dtRow.GROUP_NAME));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(14);
                        colData.SetCellValue(Convert.ToString(dtRow.USER_NAME));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(15);
                        colData.SetCellValue(Convert.ToString(dtRow.TEL));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(16);
                        colData.CellStyle = style;

                        rowCnt = rowCnt + 1;
                    }
                }

                workSheet = null;
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 26-9-2019
        /// Description : This  Method is to create the excel sheet2 values
        /// <summary>
        private void CreateSheet2(List<InventoryManagement> dt, ref XSSFWorkbook workbook)
        {
            decimal decFireTotal = 0;
            decimal decMultipleTotal = 0;
            try
            {
                var workSheet2 = workbook.CreateSheet("危険物類別集計");
                {
                    var font = workbook.CreateFont();

                    font.FontHeightInPoints = ((short)10);

                    var style = workbook.CreateCellStyle();
                    style.Alignment = HorizontalAlignment.Center;
                    style.VerticalAlignment = VerticalAlignment.Center;
                    style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                    style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    style.SetFont(font);

                    var style2 = workbook.CreateCellStyle();
                    style2.Alignment = HorizontalAlignment.Center;
                    style2.VerticalAlignment = VerticalAlignment.Center;
                    style2.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                    style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Double;
                    style2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    style2.SetFont(font);

                    var style3 = workbook.CreateCellStyle();
                    style3.Alignment = HorizontalAlignment.Left;
                    style3.VerticalAlignment = VerticalAlignment.Center;
                    style3.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                    style3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                    style3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                    style3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                    style3.SetFont(font);

                    var row = workSheet2.CreateRow(0);
                    var cell = row.CreateCell(0);

                    var row1 = workSheet2.CreateRow(1);
                    var cell1 = row1.CreateCell(0);
                    //cell1.CellStyle = style;

                    for (int i = 0; i < 5; i++)
                    {
                        cell = row.CreateCell(i);
                        cell.CellStyle = style;

                        cell1 = row1.CreateCell(i);
                        cell1.CellStyle = style;
                    }

                    // 0列目は種別の列
                    var Cell0 = row.CreateCell(0);
                    Cell0.SetCellValue("種別");
                    Cell0.CellStyle = style;
                    Cell0 = row1.CreateCell(0);
                    Cell0.CellStyle = style2;

                    workSheet2.SetColumnWidth(0, 256 * 10);
                    workSheet2.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 0, 0));

                    // 1列目は種別の列
                    var Cell1 = row.CreateCell(1);
                    Cell1.SetCellValue("種類");
                    Cell1.CellStyle = style;
                    Cell1 = row1.CreateCell(1);
                    Cell1.CellStyle = style2;

                    workSheet2.SetColumnWidth(1, 256 * 50);
                    workSheet2.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 1, 1));

                    // 2列目は種別の列
                    var Cell2 = row.CreateCell(2);
                    Cell2.SetCellValue("指定数量" + Environment.NewLine + "(L)(kg)");
                    Cell2.CellStyle = style;
                    Cell2.CellStyle.WrapText = true;
                    Cell2 = row1.CreateCell(2);
                    Cell2.CellStyle = style2;

                    workSheet2.SetColumnWidth(2, 256 * 16);
                    workSheet2.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 2, 2));

                    // 3列目は種別の列
                    var Cell4 = row.CreateCell(3);
                    Cell4.SetCellValue("集計値");
                    Cell4.CellStyle = style;
                    cell1 = row1.CreateCell(3);
                    cell1.CellStyle = style2;
                    cell1.SetCellValue("最大数量" + Environment.NewLine + "(L)(kg)");
                    cell1.CellStyle.WrapText = true;

                    workSheet2.SetColumnWidth(3, 256 * 16);
                    workSheet2.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 3, 4));

                    // 5列目は倍数の列
                    var Cell5 = row1.CreateCell(4);
                    Cell5.SetCellValue("倍数");
                    Cell5.CellStyle = style2;
                    workSheet2.SetColumnWidth(4, 256 * 10);

                    //消防法シート作成
                    int rowCnt = 2;
                    List<FireDataModel> _dtFire = inventoryRepo.DoSearchFireData();

                    foreach (var dtRow in _dtFire)
                    {
                        var rowData = workSheet2.CreateRow(rowCnt);
                        var colData = rowData.CreateCell(0);
                        colData.SetCellValue(Convert.ToString(dtRow.REG_TEXT));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(1);
                        colData.SetCellValue(Convert.ToString(dtRow.REG_TEXT2));
                        colData.CellStyle = style;

                        colData = rowData.CreateCell(2);
                        if (dtRow.FIRE_SIZE != null)
                        {
                            colData.SetCellValue(Convert.ToDouble(Convert.ToDecimal(dtRow.FIRE_SIZE).ToString("0.###")));
                        }

                        colData.CellStyle = style;

                        var filteredResult = from ss in dt
                                             where ss.REG_TEXT == dtRow.REG_TEXT && ss.REG_TEXT2.Contains(dtRow.REG_TEXT2)
                                             select ss;
                        decimal decTotal = 0;
                        foreach (var dtrow in filteredResult)
                        {
                            decTotal += Convert.ToDecimal(dtrow.AMOUNT);
                        }

                        decFireTotal += decTotal;

                        colData = rowData.CreateCell(3);
                        colData.SetCellValue(decTotal.ToString("0.00"));
                        colData.CellStyle = style;

                        decimal decMultiple = Convert.ToDecimal(Math.Round(Convert.ToDecimal(decTotal) / Convert.ToDecimal(dtRow.FIRE_SIZE), 4, MidpointRounding.AwayFromZero).ToString("F4"));

                        decMultipleTotal += decMultiple;

                        colData = rowData.CreateCell(4);
                        colData.SetCellValue(decMultiple.ToString("0.0000"));
                        colData.CellStyle = style;

                        rowCnt = rowCnt + 1;
                    }

                    //合計欄作成
                    var rowTotal = workSheet2.CreateRow(rowCnt);
                    var colTotal = rowTotal.CreateCell(0);
                    colTotal.SetCellValue("合計");
                    colTotal.CellStyle = style3;

                    colTotal = rowTotal.CreateCell(1);
                    colTotal.SetCellValue("");
                    colTotal.CellStyle = style;

                    colTotal = rowTotal.CreateCell(2);
                    colTotal.SetCellValue("");
                    colTotal.CellStyle = style;

                    workSheet2.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowCnt, rowCnt, 0, 2));

                    colTotal = rowTotal.CreateCell(3);
                    colTotal.SetCellValue(Convert.ToString(decFireTotal.ToString("0.00")));
                    colTotal.CellStyle = style;

                    colTotal = rowTotal.CreateCell(4);
                    colTotal.SetCellValue(Convert.ToString(decMultipleTotal.ToString("0.0000")));
                    colTotal.CellStyle = style;

                    //セルの結合
                    int intStartCell = 2;
                    for (int i = 2; i < rowCnt; i++)
                    {
                        if (workSheet2.GetRow(i).GetCell(0).StringCellValue != workSheet2.GetRow(i + 1).GetCell(0).StringCellValue)
                        {
                            workSheet2.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(intStartCell, i, 0, 0));
                            intStartCell = i + 1;
                        }
                    }
                }

                workSheet2 = null;
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 26-9-2019
        /// Description : This  Method is to create the excel sheet3 values
        /// <summary>
        private void CreateSheet3(List<InventoryManagement> dt2, List<InventoryManagement> dt3, ref XSSFWorkbook workbook)
        {
            decimal decFireTotal = 0; //届出値
            decimal decMultipleTotal = 0; //届出値

            decimal decFireTotal2 = 0; //集計値
            decimal decMultipleTotal2 = 0; //集計値

            var workSheet3 = workbook.CreateSheet("危険物類別集計_届出数量");
            {
                var font = workbook.CreateFont();

                font.FontHeightInPoints = ((short)10);


                var fontRed = workbook.CreateFont();

                fontRed.FontHeightInPoints = ((short)10);
                fontRed.Color = HSSFColor.Red.Index;

                var style = workbook.CreateCellStyle();
                style.Alignment = HorizontalAlignment.Center;
                style.VerticalAlignment = VerticalAlignment.Center;
                style.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                style.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                style.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                style.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                style.SetFont(font);

                var style2 = workbook.CreateCellStyle();
                style2.Alignment = HorizontalAlignment.Center;
                style2.VerticalAlignment = VerticalAlignment.Center;
                style2.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                style2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                style2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Double;
                style2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                style2.SetFont(font);

                var style3 = workbook.CreateCellStyle();
                style3.Alignment = HorizontalAlignment.Left;
                style3.VerticalAlignment = VerticalAlignment.Center;
                style3.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                style3.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                style3.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                style3.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                style3.SetFont(font);

                var style4 = workbook.CreateCellStyle();
                style4.Alignment = HorizontalAlignment.Center;
                style4.VerticalAlignment = VerticalAlignment.Center;
                style4.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                style4.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                style4.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                style4.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                style4.SetFont(fontRed);

                var styleNum2 = workbook.CreateCellStyle();
                styleNum2.Alignment = HorizontalAlignment.Center;
                styleNum2.VerticalAlignment = VerticalAlignment.Center;
                styleNum2.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum2.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum2.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum2.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum2.SetFont(font);
                styleNum2.DataFormat = workbook.CreateDataFormat().GetFormat("0.00");

                var styleNum4 = workbook.CreateCellStyle();
                styleNum4.Alignment = HorizontalAlignment.Center;
                styleNum4.VerticalAlignment = VerticalAlignment.Center;
                styleNum4.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum4.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum4.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum4.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum4.SetFont(font);
                styleNum4.DataFormat = workbook.CreateDataFormat().GetFormat("0.0000");

                var styleNum2_RED = workbook.CreateCellStyle();
                styleNum2_RED.Alignment = HorizontalAlignment.Center;
                styleNum2_RED.VerticalAlignment = VerticalAlignment.Center;
                styleNum2_RED.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum2_RED.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum2_RED.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum2_RED.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum2_RED.SetFont(fontRed);
                styleNum2_RED.DataFormat = workbook.CreateDataFormat().GetFormat("0.00");

                var styleNum4_RED = workbook.CreateCellStyle();
                styleNum4_RED.Alignment = HorizontalAlignment.Center;
                styleNum4_RED.VerticalAlignment = VerticalAlignment.Center;
                styleNum4_RED.BorderTop = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum4_RED.BorderLeft = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum4_RED.BorderBottom = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum4_RED.BorderRight = NPOI.SS.UserModel.BorderStyle.Thin;
                styleNum4_RED.SetFont(fontRed);
                styleNum4_RED.DataFormat = workbook.CreateDataFormat().GetFormat("0.0000");

                var row = workSheet3.CreateRow(0);
                var cell = row.CreateCell(0);

                var row1 = workSheet3.CreateRow(1);
                var cell1 = row1.CreateCell(0);
                //cell1.CellStyle = style;

                for (int i = 0; i < 9; i++)
                {
                    cell = row.CreateCell(i);
                    cell.CellStyle = style;

                    cell1 = row1.CreateCell(i);
                    cell1.CellStyle = style;
                }

                // 0列目は種別の列
                var Cell0 = row.CreateCell(0);
                Cell0.SetCellValue("種別");
                Cell0.CellStyle = style;
                Cell0 = row1.CreateCell(0);
                Cell0.CellStyle = style2;

                workSheet3.SetColumnWidth(0, 256 * 10);
                workSheet3.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 0, 0));

                // 1列目は種類の列
                var Cell1 = row.CreateCell(1);
                Cell1.SetCellValue("種類");
                Cell1.CellStyle = style;
                Cell1 = row1.CreateCell(1);
                Cell1.CellStyle = style2;

                workSheet3.SetColumnWidth(1, 256 * 50);
                workSheet3.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 1, 1));

                // 2列目は指定数量の列
                var Cell2 = row.CreateCell(2);
                Cell2.SetCellValue("指定数量" + Environment.NewLine + "(L)(kg)");
                Cell2.CellStyle = style;
                Cell2.CellStyle.WrapText = true;
                Cell2 = row1.CreateCell(2);
                Cell2.CellStyle = style2;

                workSheet3.SetColumnWidth(2, 256 * 16);
                workSheet3.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 2, 2));

                // 3列目は届出値の列
                var Cell4 = row.CreateCell(3);
                Cell4.SetCellValue("届出値①");
                Cell4.CellStyle = style;
                cell1 = row1.CreateCell(3);
                cell1.CellStyle = style2;
                cell1.SetCellValue("最大数量" + Environment.NewLine + "(L)(kg)");
                cell1.CellStyle.WrapText = true;

                workSheet3.SetColumnWidth(3, 256 * 16);
                workSheet3.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 3, 4));

                // 4列目は倍数の列
                var Cell5 = row1.CreateCell(4);
                Cell5.SetCellValue("倍数");
                Cell5.CellStyle = style2;
                workSheet3.SetColumnWidth(4, 256 * 10);

                // 5列目は集計値の列
                var Cell6 = row.CreateCell(5);
                Cell6.SetCellValue("集計値②");
                Cell6.CellStyle = style;
                cell1 = row1.CreateCell(5);
                cell1.CellStyle = style2;
                cell1.SetCellValue("最大数量" + Environment.NewLine + "(L)(kg)");
                cell1.CellStyle.WrapText = true;

                workSheet3.SetColumnWidth(5, 256 * 16);
                workSheet3.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 5, 6));

                // 6列目は倍数の列
                var Cell7 = row1.CreateCell(6);
                Cell7.SetCellValue("倍数");
                Cell7.CellStyle = style2;
                workSheet3.SetColumnWidth(6, 256 * 10);

                // 7列目は集計値の列
                var Cell8 = row.CreateCell(7);
                Cell8.SetCellValue("差異(①－②)");
                Cell8.CellStyle = style;
                cell1 = row1.CreateCell(7);
                cell1.CellStyle = style2;
                cell1.SetCellValue("最大数量" + Environment.NewLine + "(L)(kg)");
                cell1.CellStyle.WrapText = true;

                workSheet3.SetColumnWidth(7, 256 * 16);
                workSheet3.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 7, 8));

                // 8列目は倍数の列
                var Cell9 = row1.CreateCell(8);
                Cell9.SetCellValue("倍数");
                Cell9.CellStyle = style2;
                workSheet3.SetColumnWidth(8, 256 * 10);

                //シートのデータ作成
                int rowCnt = 2;
                List<FireDataModel> _dtFire = inventoryRepo.DoSearchFireData();

                foreach (var dtRow in _dtFire)
                {
                    var rowData = workSheet3.CreateRow(rowCnt);
                    var colData = rowData.CreateCell(0);
                    colData.SetCellValue(Convert.ToString(dtRow.REG_TEXT));
                    colData.CellStyle = style;

                    colData = rowData.CreateCell(1);
                    colData.SetCellValue(Convert.ToString(dtRow.REG_TEXT2));
                    colData.CellStyle = style;

                    colData = rowData.CreateCell(2);
                    if (dtRow.FIRE_SIZE != null)
                    {
                        colData.SetCellValue(Convert.ToDouble(Convert.ToDecimal(dtRow.FIRE_SIZE).ToString("0.###")));
                    }
                    colData.CellStyle = style;

                    StringBuilder strCondition = new StringBuilder();
                    strCondition.Append("REG_TEXT = ");
                    strCondition.Append("'" + Convert.ToString(dtRow.REG_TEXT) + "' ");
                    strCondition.Append("AND ");
                    strCondition.Append("REG_TEXT2 LIKE ");
                    strCondition.Append("'" + Convert.ToString(dtRow.REG_TEXT2 + "%' "));

                    DataTable dt3data = ToDataTable(dt3);


                    //var rowsvalue = (from list in dt3
                    //                  where list.REG_TEXT == Convert.ToString(list.REG_TEXT) &&
                    //                  list.REG_TEXT2.Contains(Convert.ToString(dtRow.REG_TEXT2))
                    //                  select list).ToList();
                    //List<InventoryManagement> selectedRows = new List<InventoryManagement>();
                    //selectedRows = rowsvalue;
                    DataRow[] selectedRows = dt3data.Select(strCondition.ToString());
                    decimal decTotal = 0;
                    foreach (DataRow dtrow in selectedRows)
                    {
                        decTotal += Convert.ToDecimal(dtrow["FIRE_TOTAL"]);
                    }

                    decFireTotal += decTotal;

                    colData = rowData.CreateCell(3);
                    colData.SetCellValue(Convert.ToDouble(decTotal.ToString("0.00")));
                    colData.CellStyle = styleNum2;

                    decimal decMultiple = Convert.ToDecimal(Math.Round(Convert.ToDecimal(decTotal) / Convert.ToDecimal(dtRow.FIRE_SIZE), 4, MidpointRounding.AwayFromZero).ToString("F4"));

                    decMultipleTotal += decMultiple;

                    colData = rowData.CreateCell(4);
                    colData.SetCellValue(Convert.ToDouble(decMultiple.ToString("0.0000")));
                    colData.CellStyle = styleNum4;

                    //var reowsvalue2 = (from list in dt2
                    //                  where list.REG_TEXT == Convert.ToString(list.REG_TEXT) &&
                    //                  list.REG_TEXT2.Contains(Convert.ToString(dtRow.REG_TEXT2))
                    //                  select list).ToList();

                    //List<InventoryManagement> selectedRows2 = reowsvalue2;
                    //decimal decTotal2 = 0;
                    //foreach (var dtrow in selectedRows2)
                    //{
                    //    decTotal2 += Convert.ToDecimal(dtrow.AMOUNT);
                    //}
                    DataTable dt2data = ToDataTable(dt2);
                    DataRow[] selectedRows2 = dt2data.Select(strCondition.ToString());
                    decimal decTotal2 = 0;
                    foreach (DataRow dtrow in selectedRows2)
                    {
                        decTotal2 += Convert.ToDecimal(dtrow["AMOUNT"]);
                    }
                    decFireTotal2 += decTotal2;

                    colData = rowData.CreateCell(5);
                    colData.SetCellValue(Convert.ToDouble(decTotal2.ToString("0.00")));
                    colData.CellStyle = styleNum2;

                    decimal decMultiple2 = Convert.ToDecimal(Math.Round(Convert.ToDecimal(decTotal2) / Convert.ToDecimal(dtRow.FIRE_SIZE), 4, MidpointRounding.AwayFromZero).ToString("F4"));

                    decMultipleTotal2 += decMultiple2;

                    colData = rowData.CreateCell(6);
                    colData.SetCellValue(Convert.ToDouble(decMultiple2.ToString("0.0000")));
                    colData.CellStyle = styleNum4;

                    colData = rowData.CreateCell(7);
                    colData.SetCellValue(Convert.ToDouble((decTotal - decTotal2).ToString("0.00")));
                    if (decTotal - decTotal2 < 0)
                    {
                        colData.CellStyle = styleNum2_RED;
                    }
                    else
                    {
                        colData.CellStyle = styleNum2;
                    }

                    colData = rowData.CreateCell(8);
                    colData.SetCellValue(Convert.ToDouble((decMultiple - decMultiple2).ToString("0.0000")));
                    if (decMultiple - decMultiple2 < 0)
                    {
                        colData.CellStyle = styleNum4_RED;
                    }
                    else
                    {
                        colData.CellStyle = styleNum4;
                    }

                    rowCnt = rowCnt + 1;
                }

                //合計欄作成
                var rowTotal = workSheet3.CreateRow(rowCnt);
                var colTotal = rowTotal.CreateCell(0);
                colTotal.SetCellValue("合計");
                colTotal.CellStyle = style3;

                colTotal = rowTotal.CreateCell(1);
                colTotal.SetCellValue("");
                colTotal.CellStyle = style;

                colTotal = rowTotal.CreateCell(2);
                colTotal.SetCellValue("");
                colTotal.CellStyle = style;

                workSheet3.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowCnt, rowCnt, 0, 2));

                colTotal = rowTotal.CreateCell(3);
                colTotal.SetCellValue(Convert.ToDouble(decFireTotal.ToString("0.00")));
                colTotal.CellStyle = styleNum2;

                colTotal = rowTotal.CreateCell(4);
                colTotal.SetCellValue(Convert.ToDouble(decMultipleTotal.ToString("0.0000")));
                colTotal.CellStyle = styleNum4;

                colTotal = rowTotal.CreateCell(5);
                colTotal.SetCellValue(Convert.ToDouble(decFireTotal2.ToString("0.00")));
                colTotal.CellStyle = styleNum2;

                colTotal = rowTotal.CreateCell(6);
                colTotal.SetCellValue(Convert.ToDouble(decMultipleTotal2.ToString("0.0000")));
                colTotal.CellStyle = styleNum4;

                colTotal = rowTotal.CreateCell(7);
                colTotal.SetCellValue(Convert.ToDouble((decFireTotal - decFireTotal2).ToString("0.00")));
                if (decFireTotal - decFireTotal2 < 0)
                {
                    colTotal.CellStyle = styleNum2_RED;
                }
                else
                {
                    colTotal.CellStyle = styleNum2;
                }

                colTotal = rowTotal.CreateCell(8);
                colTotal.SetCellValue(Convert.ToDouble((decMultipleTotal - decMultipleTotal2).ToString("0.0000")));
                if (decMultipleTotal - decMultipleTotal2 < 0)
                {
                    colTotal.CellStyle = styleNum4_RED;
                }
                else
                {
                    colTotal.CellStyle = styleNum4;
                }

                //セルの結合
                int intStartCell = 2;
                for (int i = 2; i < rowCnt; i++)
                {
                    if (workSheet3.GetRow(i).GetCell(0).StringCellValue != workSheet3.GetRow(i + 1).GetCell(0).StringCellValue)
                    {
                        workSheet3.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(intStartCell, i, 0, 0));
                        intStartCell = i + 1;
                    }
                }
            }
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                dataTable.Columns.Add(prop.Name, type);
            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    //inserting property values to datatable rows
                    values[i] = Props[i].GetValue(item, null);
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
    }
}