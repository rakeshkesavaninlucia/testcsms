﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.Extensions;
using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Chem;
using ChemMgmt.Nikon.Ledger;
using ChemMgmt.Nikon.Models;
using ChemMgmt.Repository;
using COE.Common;
using Dapper;
using IdentityManagement.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.BaseCommon;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.Chem;

namespace ChemMgmt.Controllers
{
    [Authorize]
    [HandleError]
    public class ZC110Controller : Controller
    {
        public enum ContainedSearchMode
        {
            /// <summary>
            /// 含有物検索
            /// </summary>
            Contained,
            /// <summary>
            /// 化学物質検索
            /// </summary>
            Chem,
        }

        DropDownLists dropdownlist = new DropDownLists();
        CsmsCommonInfo csmscommoninfo = new CsmsCommonInfo();
        CsmsCommonInfo dropdownlistDTO = new CsmsCommonInfo();
        SearchChemicalAndProducts objBAL = new SearchChemicalAndProducts();
        private LogUtil _logoututil = new LogUtil();
        private const string programId = "ZC110";

        protected ContainedSearchMode SearchMode { get; set; }

        //Grid削除後の内容をListに入れるため
        private List<V_REGULATION> delthengridreg;

        private List<V_FIRE> delthengridfire;

        private List<V_OFFICE> delthengridoffice;

        private List<V_GHSCAT> delthengridghs;

        /// <summary>
        /// メンテナンス対象の識別情報を取得または設定します。
        /// </summary>
        private string id { get; set; }
        /// <summary>
        /// セッション変数に格納する含有物一覧の名前を定義します。
        /// </summary>
        private string sessionContaineds = "Containeds";

        private List<V_REGULATION> gridreg = new List<V_REGULATION>();

        /// <summary>
        /// セッション変数に格納するGHS分類ID'sの名前を定義します。
        /// </summary>
        private const string sessionGhsCategoryIds = "GhsCategoryIds";

        /// <summary>
        /// セッション変数に格納する法規制ID'sの名前を定義します。
        /// </summary>
        private const string sessionRegulationIds = "RegulationIds";


        /// <summary>
        /// セッション変数に格納する消防法ID'sの名前を定義します。
        /// </summary>
        private const string sessionFireIds = "FireIds";

        /// <summary>
        /// セッション変数に格納する社内ルールID'sの名前を定義します。
        /// </summary>
        private const string sessionOfficeIds = "OfficeIds";

        /// <summary>
        /// セッション変数に格納する商品一覧の名前を定義します。
        /// </summary>
        private const string sessionProducts = "Products";

        /// <summary>
        /// 法規制反映で反映された法規制を定義します。
        /// </summary>
        private const string sessionReflection = "Reflection";

        /// <summary>
        /// セッション変数に格納する製品別名一覧の名前を定義します。
        /// </summary>
        private const string sessionAliases = "Aliases";

        private const string OldsessionAliases = "OldAliases";


        private bool regulationFlag = false;
        private bool ghsFlag = false;
        private bool fireFlag = false;
        private bool officeFlag = false;
        private bool IsSearchButton = true;
        private bool IsListOutputProcess = false;

        /// <summary>
        /// 法規制Gridを削除したかのフラグ。
        /// </summary>
        private bool regdel = false;

        /// <summary>
        /// 消防法Gridを削除したかのフラグ。
        /// </summary>
        private bool firedel = false;

        /// <summary>
        /// 社内ルールGridを削除したかのフラグ。
        /// </summary>
        private bool officedel = false;

        /// <summary>
        /// GHS分類Gridを削除したかのフラグ。
        /// </summary>
        private bool ghsdel = false;

        // GET: ZC110


        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-8-2019
        /// Description : This  Method is used to load the search page
        /// <summary>
        public ActionResult ZC11010(string mode)
        {
            CsmsCommonInfo csmsCommonInfo = new CsmsCommonInfo();
            try
            {
                string adminFlag = "";
                WebCommonInfo webCommon = new WebCommonInfo();
                if (Session["LoginUserSession"] != null)
                {
                    ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];
                    adminFlag = appUser.ADMIN_FLAG;
                }
                else
                {
                    return RedirectToAction("ZC01001", "ZC010");
                }


                if (mode == "6" || mode == "7")
                {
                    CsmsCommonInfo CSMSinfo = new CsmsCommonInfo();
                    CSMSinfo = null;
                    if (Session["SessionList"] != null)
                    {
                        Session["SessionList"] = null;
                    }
                }


                ViewBag.list = "";
                if (Session["SessionList"] != null)
                {
                    csmsCommonInfo = (CsmsCommonInfo)Session["SessionList"];
                }
                else
                {
                    csmsCommonInfo.ChemCommonList = new List<CsmsCommonInfo>();
                }
                ViewBag.MaxChemResultCount = 0;
                ViewBag.CHEM_SEARCH_TARGET = 0;
                ViewBag.searchtarget = 0;
                dropdownlistDTO = dropdownlist.ddlChemList();
                ViewBag.ddl = dropdownlistDTO;
                Session["FixedCount"] = null;
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.StackTrace.ToString());
            }
            return View(csmsCommonInfo);
        }



        [HttpPost]
        public string AddAliasValues()
        {
            List<ChemAliasModel> chemAliases = new List<ChemAliasModel>();
            string aliase = null;
            if (Session["sessionAliases"] != null)
            {
                List<ChemAliasModel> chemmodel = (List<ChemAliasModel>)Session["sessionAliases"];
                ChemAliasModel data = new ChemAliasModel();
                data.CHEM_NAME = null;
                data.CHEM_NAME_SEQ = chemmodel.Count() + 1;
                chemmodel.Add(data);
                Session["sessionAliases"] = chemmodel;
                aliase = "<tr><td align='Center'><input type='checkbox' name='Checkbox' id=" + data.CHEM_NAME_SEQ + " align='right' class=aliasChekbox></td><td align='Center'><input type='text' name='ChemAliasModel' style='Width: 100%'  class='required aliasName' onchange='return ChemAliasValuesOnchange(this.value," + data.CHEM_NAME_SEQ + ");' onkeypress='return validateSpecialCharecters()'></td></tr>";

            }
            else
            {
                var data = new ChemAliasModel();
                data.CHEM_NAME = null;
                data.CHEM_NAME_SEQ = 1;
                chemAliases.Add(data);
                Session["sessionAliases"] = chemAliases;
                aliase = "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + data.CHEM_NAME_SEQ + " class=aliasChekbox></td><td align='Center'><input type='text' name='ChemAliasModel' style='Width: 100%' class='required aliasName' onchange='return ChemAliasValuesOnchange(this.value," + data.CHEM_NAME_SEQ + ");' onkeypress='return validateSpecialCharecters()'></td></tr>";
            }
            return aliase;
        }


        public string LoadAliasValues()
        {
            List<ChemAliasModel> chemAliases = new List<ChemAliasModel>();
            string aliase = null;
            var data = new ChemAliasModel();
            if (Session["sessionAliases"] != null)
            {
                List<ChemAliasModel> chemmodel = (List<ChemAliasModel>)Session["sessionAliases"];
                foreach (var items in chemmodel)
                {
                    aliase += "<tr><td align='Center'><input type='checkbox' name='Checkbox'  id=" + data.CHEM_NAME_SEQ + " align='right' class=aliasChekbox></td><td align='Center'><input type='text' name='ChemAliasModel' id=" + items.CHEM_NAME_SEQ + " value=" + items.CHEM_NAME + " style='Width: 100%' class='required aliasName'></td></tr>";
                }
            }
            return aliase;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019
        /// Description : This Action Method is used to Clear the Sessions
        /// <summary>
        public void ClearSessionsandClosewindow()
        {
            NewRegistration ChemRegistration = new NewRegistration();
            try
            {
                ChemRegistration.ClearSession();
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }

        [HttpPost]
        public JsonResult MaxCountResult(ChemSearchModel chemSearchModel)
        {
            try
            {
                List<int> MyProperty = objBAL.GetSearchResultCount(chemSearchModel);
                int SearchCount = Convert.ToInt32(Session["SearchCount"]);
                int fixedCount = Convert.ToInt32(Session["FixedCount"]);
                List<int> result = new List<int>();
                result.Add(SearchCount);
                result.Add(fixedCount);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.StackTrace.ToString());
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019
        /// Description : This link is to Redirect 
        /// <summary>
        public ActionResult SDSUrlLink()
        {
            return RedirectToAction(csmscommoninfo.SDS_URL2, "ZC110");
        }



        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019
        /// Description :get Action Method to load the Registration Form
        /// <summary>

        [HttpGet]
        [Authorize(Roles = "0,1")]
        public ActionResult ZC11020(string ChemCd, string mode, string Operation, string TempFlag)
        {
            try
            {
                string userName = "";
                if (Session["LoginUserSession"] != null)
                {
                    WebCommonInfo webCommon = new WebCommonInfo();
                    ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];
                    userName = appUser.UserName;
                    TempData["username"] = userName;
                }
                else
                {
                    return RedirectToAction("ZC01001", "ZC010");
                }

                if (Operation != null)
                {
                    csmscommoninfo = new CsmsCommonInfo();
                    var condition = new ChemSearchModel();
                    condition.CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching;
                    condition.CHEM_CD = ChemCd;
                    condition.USAGE_STATUS = (int)Classes.UsageStatusCondtionType.All;
                    var d = new ChemDataInfo();
                    csmscommoninfo = d.GetSearchResult(condition).Single();
                    if (csmscommoninfo.REF_CHEM_CD != "")
                    {
                        TempData["ReferenceCd"] = csmscommoninfo.REF_CHEM_CD;
                    }
                    d.Dispose();

                    if (Session["RegulationModel"] != null)
                    {
                        //Laws and regulations Binding of the Data
                        List<V_REGULATION> RegulationModel = (List<V_REGULATION>)Session["RegulationModel"];
                        csmscommoninfo.RegulationModel = RegulationModel;
                    }
                    if (Session["FireServicesModel"] != null)
                    {
                        List<V_FIRE> FireGrid = (List<V_FIRE>)Session["FireServicesModel"];
                        csmscommoninfo.FireModel = FireGrid; ;
                    }
                    if (Session["GHSModel"] != null)
                    {
                        List<V_GHSCAT> GHSClassification = (List<V_GHSCAT>)Session["GHSModel"];
                        csmscommoninfo.GHSModel = GHSClassification;
                    }
                    if (Session["OfficeRulesmodel"] != null)
                    {
                        List<V_OFFICE> OfficeGridModel = (List<V_OFFICE>)Session["OfficeRulesmodel"];
                        csmscommoninfo.OfficeGridModel = OfficeGridModel;
                    }

                    if (Session["Contained"] != null)
                    {
                        List<ChemContainedModel> chemContainedModels = (List<ChemContainedModel>)Session["Contained"];
                        csmscommoninfo.ContainedModel = chemContainedModels;
                    }

                    if (Session["ProductDetails"] != null)
                    {
                        List<ChemProductModel> chemproductmodel = (List<ChemProductModel>)Session["ProductDetails"];
                        csmscommoninfo.ChemProductListModel = chemproductmodel;
                    }
                    if (Session["sessionAliases"] != null)
                    {
                        List<ChemAliasModel> chemaliasmodel = (List<ChemAliasModel>)Session["sessionAliases"];
                        csmscommoninfo.ChemAliasModel = chemaliasmodel;
                    }
                }

                if (Operation != null)
                {
                    TempData["Register"] = Operation;
                }
                if (!string.IsNullOrEmpty(ChemCd) && mode != null && Operation == null)
                {
                    ClearSessionValues();

                    var condition = new ChemSearchModel();
                    condition.CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching;
                    condition.CHEM_CD = ChemCd;
                    condition.USAGE_STATUS = (int)Classes.UsageStatusCondtionType.All;
                    ChemDataInfo chemDataInfo = new ChemDataInfo();
                    csmscommoninfo = chemDataInfo.GetSearchResult(condition).Single();
                    if (csmscommoninfo.REF_CHEM_CD != "")
                    {
                        TempData["ReferenceCd"] = csmscommoninfo.REF_CHEM_CD;
                    }
                    chemDataInfo.Dispose();

                    //This Method gives Alias grid records
                    var AliasArray = GetInputedAlias(ChemCd, mode).AsEnumerable();
                    csmscommoninfo.ChemAliasModel = AliasArray.ToList();
                    Session["sessionAliases"] = csmscommoninfo.ChemAliasModel;


                    //This Method gives Contained grid records
                    var containedArray = GetSelectedContained(ChemCd, mode, Session[sessionContaineds] as IEnumerable<ChemContainedModel>).AsEnumerable();
                    csmscommoninfo.ContainedModel = containedArray.ToList();
                    Session["Contained"] = csmscommoninfo.ContainedModel;

                    //Laws and regulations Binding of the Data
                    var regulatlationArray = GetSelectedRegulation(ChemCd, mode, Session[sessionRegulationIds] as List<int>).AsEnumerable();
                    csmscommoninfo.RegulationModel = regulatlationArray.ToList();
                    Session["RegulationModel"] = csmscommoninfo.RegulationModel;

                    //Fire Service act Binding of the Data
                    var fireGrid = GetSelectedFire(ChemCd, mode, Session[sessionFireIds] as List<int>).AsEnumerable();
                    csmscommoninfo.FireModel = fireGrid.ToList();
                    Session["FireServicesModel"] = csmscommoninfo.FireModel;

                    //This Method gives InternalRules/office grid records
                    var internalRules = GetSelectedOffice(ChemCd, mode, Session[sessionOfficeIds] as List<int>).AsEnumerable();
                    csmscommoninfo.OfficeGridModel = internalRules.ToList();
                    Session["OfficeRulesmodel"] = csmscommoninfo.OfficeGridModel;

                    //This Method gives GHS Clasification grid records
                    var ghsClassification = GetSelectedGhsCategory(ChemCd, mode, Session[sessionGhsCategoryIds] as List<int>).AsEnumerable();
                    csmscommoninfo.GHSModel = ghsClassification.ToList();
                    Session["GHSModel"] = csmscommoninfo.GHSModel;

                    //This Method gives Product Details grid records
                    var productDetails = GetProducts(ChemCd, mode).AsEnumerable();
                    csmscommoninfo = GetLookupValues(csmscommoninfo);
                    csmscommoninfo.ChemProductListModel = productDetails.ToList();
                    Session["ProductDetails"] = csmscommoninfo.ChemProductListModel;


                    
                    
                }

                if (mode == "1")
                {
                    csmscommoninfo.ViewModeValue = "1";
                }
                if (mode == "2")
                {
                    csmscommoninfo.ViewModeValue = "2";
                }
                if (mode == "0")
                {
                    ClearSessionValues();
                }


                Session.Remove(SessionConst.CategorySelectReturnValue);
                Session.Remove(SessionConst.CategoryColumnName);
                Session.Remove(SessionConst.CategoryTableName);
                Session.Remove(SessionConst.RowId);
                Session[SessionConst.CategorySelectReturnValue] = null;

                ViewBag.list = "";
                dropdownlistDTO = dropdownlist.ddlChemList();
                csmscommoninfo.LookupManufacturerName = dropdownlistDTO.LookupOfManufacturer;
                csmscommoninfo.LookupUnitSizeMasterInfo = dropdownlistDTO.LookupUnitSizeMasterInfo;
                csmscommoninfo.LookupClassification = dropdownlistDTO.LookupClassification;

                if (csmscommoninfo.CHEM_CAT != "単体" && csmscommoninfo.CHEM_CAT != "混合物" && csmscommoninfo.CHEM_CAT != "")
                {
                    List<SelectListItem> listClassification = new List<SelectListItem> { };
                    new CommonMasterInfo(NikonCommonMasterName.CHEM_CAT).GetSelectList().ToList().ForEach(x => listClassification.Add(new SelectListItem { Value = x.Name, Text = x.Name }));
                    listClassification.Add(new SelectListItem { Text = csmscommoninfo.CHEM_CAT, Value = csmscommoninfo.CHEM_CAT });
                    csmscommoninfo.LookupClassification = listClassification;

                }
                ViewBag.ddl = dropdownlistDTO;

                csmscommoninfo.HiddenMode = mode;
                csmscommoninfo.HiddenTempflag = TempFlag;
                csmscommoninfo.HiddenChemCD = ChemCd;
                if (csmscommoninfo.REF_CHEM_CD != null)
                {
                    csmscommoninfo.HiddenRefChemCD = csmscommoninfo.REF_CHEM_CD;
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return View(csmscommoninfo);
        }


        /// </summary>
        ///  /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019 
        /// <para>含有物一覧の削除ボタンの処理です。</para>
        /// <para>選択行を削除します。</para>
        /// <summary>
        public bool DeleteContained(string ChekedRowid)
        {
            bool containedResult = false;
            try
            {

                string cheCode = Session[sessionContaineds].ToString();


                List<string> containedChem = cheCode.Split(',').ToList();
                if (containedChem != null)
                {
                    var containeds = new List<string>();
                    string[] values = ChekedRowid.Split(',');
                    for (int J = 0; J < containedChem.Count; J++)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (containedChem[J].Trim() == values[i].Trim())
                            {
                                containedChem.Remove(containedChem[J]);

                                if (Session["Contained"] != null)
                                {
                                    List<ChemContainedModel> chemContainedmodel = (List<ChemContainedModel>)Session["Contained"];
                                    chemContainedmodel = chemContainedmodel.Where(x => x.C_CHEM_CD != values[i].Trim().ToString()).ToList();
                                    Session["Contained"] = chemContainedmodel;
                                }
                            }
                        }
                    }
                    for (int k = 0; k < containedChem.Count; k++)
                    {
                        containeds.Add(containedChem[k]);
                    }
                    Session[sessionContaineds] = null;
                    if (containeds != null && containeds.Count > 0)
                    {
                        Session.Add(sessionContaineds, string.Join(",", containeds));
                    }
                    containedResult = true;
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                containedResult = false;
            }
            return containedResult;
        }



        /// <para>法規制の削除ボタンの処理です。</para>
        /// <para>選択行を削除します。</para>
        /// Description :get Action Method to load the Registration Form
        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019         
        /// <summary>
        public string DeleteRegulation(string ChekedRowid)
        {
            string LeftoverVlaues = "";
            try
            {
                CsmsCommonInfo CsmsCommoninfo = new CsmsCommonInfo();
                var regulation = new List<int>();
                var ids = (List<int>)Session[sessionRegulationIds];
                if (ids != null)
                {
                    var Containeds = new List<int>();
                    string[] values = ChekedRowid.Split(',');
                    for (int J = 0; J < ids.Count; J++)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (ids[J] == Convert.ToInt32(values[i]))
                            {
                                ids.Remove(ids[J]);
                            }
                        }
                    }
                    for (int k = 0; k < ids.Count; k++)
                    {
                        Containeds.Add(ids[k]);
                    }

                    LeftoverVlaues = string.Join(",", Containeds);
                    csmscommoninfo.hiddenRegulationIds = LeftoverVlaues;
                    Session[sessionRegulationIds] = null;
                    Session.Add(sessionRegulationIds, Containeds.AsEnumerable());
                    Session["RegulationModel"] = null;

                    var regulatlationArray = GetSelectedRegulation(null, null, Session[sessionRegulationIds] as List<int>).AsEnumerable();
                    csmscommoninfo.RegulationModel = regulatlationArray.ToList();
                    Session["RegulationModel"] = csmscommoninfo.RegulationModel;
                }
            }
            catch (Exception ex)
            {
                LeftoverVlaues = "";
                //regulationResult = false;
            }
            return LeftoverVlaues;

        }


        public string DeleteProduct(string Rowid)
        {
            string Alias = "true";
            try
            {
                List<ChemProductModel> ProductExists = (List<ChemProductModel>)Session["ProductDetails"];
                if (ProductExists != null && ProductExists.Count > 0)
                {
                    string[] values = Rowid.Split(',');
                    for (int J = 0; J < ProductExists.Count; J++)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (ProductExists[J].TempPRODUCT_SEQ == Convert.ToInt32(values[i]))
                            {
                                ProductExists.Remove(ProductExists[J]);
                            }
                        }
                    }
                    Session["ProductDetails"] = ProductExists;
                    Alias = "true";
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return Alias;
        }



        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019  
        /// Description :this method gives the Deleteting of fireIds
        /// <summary>
        public string DeleteFireIds(string ChekedRowid)
        {
            string fire = "";
            try
            {
                var fireExists = (List<int>)Session[sessionFireIds];
                var fireIds = new List<int>();
                if (fireExists != null)
                {
                    string[] values = ChekedRowid.Split(',');
                    for (int J = 0; J < fireExists.Count; J++)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (fireExists[J] == Convert.ToInt32(values[i]))
                            {
                                fireExists.Remove(fireExists[J]);
                            }

                        }
                    }
                    for (int k = 0; k < fireExists.Count; k++)
                    {
                        fireIds.Add(fireExists[k]);
                    }
                    fire = string.Join(",", fireIds);

                    csmscommoninfo.hiddenfire = fire;
                    Session[sessionFireIds] = null;
                    Session.Add(sessionFireIds, fireIds.AsEnumerable());

                    Session["FireServicesModel"] = null;
                    var FireGrid = GetSelectedFire(null, null, Session[sessionFireIds] as List<int>).AsEnumerable();
                    csmscommoninfo.FireModel = FireGrid.ToList();
                    Session["FireServicesModel"] = csmscommoninfo.FireModel;

                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return fire;
        }


        /// <para>社内ルールの削除ボタンの処理です。</para>
        /// <para>選択行を削除します。</para>
        /// </summary>

        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019  
        /// Description :Deleting of office Ids
        /// <summary>
        public string DeleteofficeIds(string ChekedRowid)
        {
            string office = "";
            try
            {
                var officeExists = (List<int>)Session[sessionOfficeIds];
                var officeIds = new List<int>();
                if (officeExists != null)
                {
                    string[] values = ChekedRowid.Split(',');
                    for (int J = 0; J < officeExists.Count; J++)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (officeExists[J] == Convert.ToInt32(values[i]))
                            {
                                officeExists.Remove(officeExists[J]);
                            }
                        }
                    }
                    for (int k = 0; k < officeExists.Count; k++)
                    {
                        officeIds.Add(officeExists[k]);
                    }
                    office = string.Join(",", officeIds);
                    csmscommoninfo.hiddenoffice = office;
                    Session[sessionOfficeIds] = null;
                    Session.Add(sessionOfficeIds, officeIds.AsEnumerable());

                    Session["OfficeRulesmodel"] = null;
                    var internalRules = GetSelectedOffice(null, null, Session[sessionOfficeIds] as List<int>).AsEnumerable();
                    csmscommoninfo.OfficeGridModel = internalRules.ToList();
                    Session["OfficeRulesmodel"] = csmscommoninfo.OfficeGridModel;
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return office;
        }


        /// <para>GHS分類の削除ボタンの処理です。</para>
        /// <para>選択行を削除します。</para>
        /// </summary>

        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019  
        /// Description :Deleting of GHS Ids
        /// <summary>
        public string GhsCategoryDelete(string ChekedRowid)
        {
            string ghs = "";
            try
            {
                var ghsExists = (List<int>)Session[sessionGhsCategoryIds];
                if (ghsExists != null)
                {
                    var ghsIds = new List<int>();
                    string[] values = ChekedRowid.Split(',');
                    for (int J = 0; J < ghsExists.Count; J++)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (ghsExists[J] == Convert.ToInt32(values[i]))
                            {
                                ghsExists.Remove(ghsExists[J]);
                            }
                        }
                    }
                    for (int k = 0; k < ghsExists.Count; k++)
                    {
                        ghsIds.Add(ghsExists[k]);
                    }

                    ghs = string.Join(",", ghsIds);

                    csmscommoninfo.hiddenGHS = ghs;
                    Session[sessionGhsCategoryIds] = null;
                    Session["GHSModel"] = null;
                    Session.Add(sessionGhsCategoryIds, ghsIds.AsEnumerable());

                    var GHSClassification = GetSelectedGhsCategory(null, null, Session[sessionGhsCategoryIds] as List<int>).AsEnumerable();
                    csmscommoninfo.GHSModel = GHSClassification.ToList();
                    Session["GHSModel"] = csmscommoninfo.GHSModel;
                }
            }
            catch (Exception exe)
            {
                exe.StackTrace.ToString();
            }
            return ghs;
        }



        public string DeleteAlias(string Rowid)
        {
            string Alias = "true";
            try
            {
                List<ChemAliasModel> aliasExists = (List<ChemAliasModel>)Session["sessionAliases"];
                if (aliasExists != null && aliasExists.Count > 0)
                {

                    string[] values = Rowid.Split(',');
                    for (int J = 0; J < aliasExists.Count; J++)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (aliasExists[J].CHEM_NAME_SEQ == Convert.ToInt32(values[i]))
                            {
                                aliasExists.Remove(aliasExists[J]);
                            }

                        }
                    }
                    Session["sessionAliases"] = aliasExists;
                    Alias = "true";
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                Alias = "false";
            }
            return Alias;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019  
        /// Description :this method is used to for chemical Registration
        /// <summary>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ZC11020(CsmsCommonInfo chemControlValues, HttpPostedFileBase file)
        {
            try
            {
                if (Request.Form["Regulatory"] != null)
                {
                    NewRegistration ChemRegistration1 = new NewRegistration();
                    CsmsCommonInfo csmscommoninfo = new CsmsCommonInfo();
                    ChemRegistration1.LegalReflection(chemControlValues);

                    var RegulatlationArray = GetSelectedRegulation(null, null, Session[sessionRegulationIds] as List<int>).AsEnumerable();
                    csmscommoninfo.RegulationModel = RegulatlationArray.ToList();
                    Session["RegulationModel"] = csmscommoninfo.RegulationModel;

                    //Fire Service act Binding of the Data
                    var FireGrid = GetSelectedFire(null, null, Session[sessionFireIds] as List<int>).AsEnumerable();
                    csmscommoninfo.FireModel = FireGrid.ToList();
                    Session["FireServicesModel"] = csmscommoninfo.FireModel;

                    //This Method gives InternalRules/office grid records
                    var internalRules = GetSelectedOffice(null, null, Session[sessionOfficeIds] as List<int>).AsEnumerable();
                    csmscommoninfo.OfficeGridModel = internalRules.ToList();
                    Session["OfficeRulesmodel"] = csmscommoninfo.OfficeGridModel;
                    //This Method gives GHS Clasification grid records               

                    var GHSClassification = GetSelectedGhsCategory(null, null, Session[sessionGhsCategoryIds] as List<int>).AsEnumerable();
                    csmscommoninfo.GHSModel = GHSClassification.ToList();
                    Session["GHSModel"] = csmscommoninfo.GHSModel;

                    if (Session["Contained"] != null)
                    {
                        List<ChemContainedModel> ChemContained = (List<ChemContainedModel>)Session["Contained"];
                        csmscommoninfo.ContainedModel = ChemContained;
                    }
                    else
                    {
                        var ContainedResult = GetSelectedContained(null, null, Session[sessionContaineds] as IEnumerable<ChemContainedModel>).AsEnumerable();
                        csmscommoninfo.ContainedModel = ContainedResult.ToList();
                        Session["Contained"] = csmscommoninfo.ContainedModel;
                    }


                    if (chemControlValues.CHEM_CD != null)
                    {
                        if (Session["sessionAliases"] != null)
                        {
                            List<ChemAliasModel> chemAliases = (List<ChemAliasModel>)Session["sessionAliases"];
                            csmscommoninfo.ChemAliasModel = chemAliases;
                        }
                        else
                        {
                            var AliasArray = GetInputedAlias(chemControlValues.CHEM_CD, "2").AsEnumerable();
                            csmscommoninfo.ChemAliasModel = AliasArray.ToList();
                            Session["sessionAliases"] = csmscommoninfo.ChemAliasModel;
                        }
                    }


                    if (Session["ProductDetails"] != null)
                    {
                        List<ChemProductModel> chemproductmodel = (List<ChemProductModel>)Session["ProductDetails"];
                        csmscommoninfo.ChemProductListModel = chemproductmodel;
                    }
                    else
                    {
                        //This Method gives Product Details grid records
                        var productDetails = GetProducts(chemControlValues.CHEM_CD, null).AsEnumerable();
                        csmscommoninfo = GetLookupValues(csmscommoninfo);
                        csmscommoninfo.ChemProductListModel = productDetails.ToList();
                        Session["ProductDetails"] = csmscommoninfo.ChemProductListModel;
                    }

                    csmscommoninfo.CHEM_CD = chemControlValues.CHEM_CD;
                    csmscommoninfo.CHEM_CAT = chemControlValues.CHEM_CAT;
                    csmscommoninfo.CAS_NO = chemControlValues.CAS_NO;
                    csmscommoninfo.CHEM_NAME = chemControlValues.CHEM_NAME;
                    csmscommoninfo.FIGURE = chemControlValues.FIGURE;
                    csmscommoninfo.DENSITY = chemControlValues.DENSITY;
                    csmscommoninfo.FLASH_POINT = chemControlValues.FLASH_POINT;
                    csmscommoninfo.KEY_MGMT = chemControlValues.KEY_MGMT;
                    csmscommoninfo.WEIGHT_MGMT = chemControlValues.WEIGHT_MGMT;
                    csmscommoninfo.SDS_FILENAME = chemControlValues.SDS_FILENAME;
                    csmscommoninfo.SDS_URL = chemControlValues.SDS_URL;
                    csmscommoninfo.MEMO = chemControlValues.MEMO;
                    csmscommoninfo.OPTIONAL1 = chemControlValues.OPTIONAL1;
                    csmscommoninfo.OPTIONAL2 = chemControlValues.OPTIONAL2;
                    csmscommoninfo.OPTIONAL3 = chemControlValues.OPTIONAL3;
                    csmscommoninfo.OPTIONAL4 = chemControlValues.OPTIONAL4;
                    csmscommoninfo.OPTIONAL5 = chemControlValues.OPTIONAL5;
                    csmscommoninfo.SDS_UPD_DATE2 = chemControlValues.SDS_UPD_DATE2;
                    csmscommoninfo.UPD_DATE_DISPLAY = chemControlValues.UPD_DATE_DISPLAY;
                    csmscommoninfo.REG_USER_CD = chemControlValues.REG_USER_CD;
                    //csmscommoninfo.ViewModeValue = chemControlValues.ViewModeValue;

                    if (chemControlValues.SDS_UPD_DATE2 != null)
                    {
                        csmscommoninfo.SDS_UPD_DATE2 = chemControlValues.SDS_UPD_DATE2;
                    }

                    if (chemControlValues.REG_DATE != null)
                    {
                        csmscommoninfo.REG_DATE = OrgConvert.ToNullableDateTime(chemControlValues.REG_DATE);
                    }
                    else if (chemControlValues.UPD_DATE != null)
                    {
                        csmscommoninfo.UPD_DATE = OrgConvert.ToNullableDateTime(chemControlValues.UPD_DATE);
                    }
                    else
                    {
                        csmscommoninfo.DEL_DATE = OrgConvert.ToNullableDateTime(chemControlValues.DEL_DATE);
                    }

                    if (chemControlValues.REG_USER_CD != null)
                    {
                        csmscommoninfo.REG_USER_CD = chemControlValues.UPD_USER_NAME_DISPLAY;
                    }
                    else if (chemControlValues.UPD_USER_CD != null)
                    {
                        csmscommoninfo.UPD_USER_CD = chemControlValues.UPD_USER_NAME_DISPLAY;
                    }
                    else if (chemControlValues.DEL_USER_CD != null)
                    {
                        csmscommoninfo.DEL_USER_CD = chemControlValues.UPD_USER_NAME_DISPLAY;
                    }

                    if (Request.Form["GhsCateogry"] != null)
                    {
                        csmscommoninfo.hiddenRegulationIds = Request.Form["GhsCateogry"];
                    }
                    if (Request.Form["Regulation"] != null)
                    {
                        csmscommoninfo.hiddenRegulationIds = Request.Form["Regulation"];
                    }
                    if (Request.Form["Fire"] != null)
                    {
                        csmscommoninfo.hiddenfire = Request.Form["Fire"];
                    }
                    if (Request.Form["Office"] != null)
                    {
                        csmscommoninfo.hiddenoffice = Request.Form["Office"];
                    }

                    dropdownlistDTO = dropdownlist.ddlChemList();
                    csmscommoninfo.LookupManufacturerName = dropdownlistDTO.LookupOfManufacturer;
                    csmscommoninfo.LookupUnitSizeMasterInfo = dropdownlistDTO.LookupUnitSizeMasterInfo;
                    csmscommoninfo.LookupClassification = dropdownlistDTO.LookupClassification;
                    if (csmscommoninfo.CHEM_CAT != "単体" && csmscommoninfo.CHEM_CAT != "混合物" && csmscommoninfo.CHEM_CAT != "")
                    {
                        List<SelectListItem> listClassification = new List<SelectListItem> { };
                        new CommonMasterInfo(NikonCommonMasterName.CHEM_CAT).GetSelectList().ToList().ForEach(x => listClassification.Add(new SelectListItem { Value = x.Name, Text = x.Name }));
                        listClassification.Add(new SelectListItem { Text = csmscommoninfo.CHEM_CAT, Value = csmscommoninfo.CHEM_CAT });
                        csmscommoninfo.LookupClassification = listClassification;
                        listClassification.RemoveAll(item => item == null);
                    }
                    ViewBag.ddl = dropdownlistDTO;

                    TempData["Regulatory"] = "Regulatory";

                    csmscommoninfo.HiddenMode = chemControlValues.HiddenMode;
                    csmscommoninfo.HiddenTempflag = chemControlValues.HiddenTempflag;
                    csmscommoninfo.HiddenChemCD = chemControlValues.CHEM_CD;
                    if (csmscommoninfo.HiddenRefChemCD == null)
                    {
                        csmscommoninfo.REF_CHEM_CD = Request.Form["RefChemCD"];
                        csmscommoninfo.HiddenRefChemCD = Request.Form["RefChemCD"];
                    }
                    if (csmscommoninfo.HiddenRefChemCD != "")
                    {
                        TempData["ReferenceCd"] = csmscommoninfo.REF_CHEM_CD;
                    }
                    return View(csmscommoninfo);
                }
                else if (Request.Form["FlowCode"] == "1")
                {
                    NewRegistration ChemRegistration = new NewRegistration();

                    if (file != null && file.ContentLength > 0)
                    {
                        MemoryStream target = new MemoryStream();
                        file.InputStream.CopyTo(target);
                        chemControlValues.SDS = target.ToArray();
                        chemControlValues.SDS_MIMETYPE = MimeMapping.GetMimeMapping(file.ContentType);
                        chemControlValues.SDS_FILENAME = file.FileName;
                    }
                    else
                    {
                        if (chemControlValues.CHEM_CD != null)
                        {
                            var condition = new ChemSearchModel();
                            condition.CHEM_CD = chemControlValues.CHEM_CD;
                            condition.CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching;
                            condition.USAGE_STATUS = (int)Classes.UsageStatusCondtionType.All;
                            if (condition.CHEM_CD != null)
                            {
                                var d = new ChemDataInfo();
                                var chemOld = d.GetSearchResult(condition).Single();
                                d.Dispose();

                                if (chemOld.SDS != null)
                                {
                                    chemControlValues.SDS = chemOld.SDS;
                                    chemControlValues.SDS_MIMETYPE = chemOld.SDS_MIMETYPE;
                                    chemControlValues.SDS_FILENAME = chemOld.SDS_FILENAME;
                                }
                            }
                        }
                        else
                        {
                            chemControlValues.SDS = null;
                        }
                    }

                  
                    ChemRegistration.Register(chemControlValues);
                  
                    dropdownlistDTO = dropdownlist.ddlChemList();
                    ViewBag.ddl = dropdownlistDTO;
                    csmscommoninfo.LookupManufacturerName = dropdownlistDTO.LookupOfManufacturer;
                    csmscommoninfo.LookupUnitSizeMasterInfo = dropdownlistDTO.LookupUnitSizeMasterInfo;
                    csmscommoninfo.LookupClassification = dropdownlistDTO.LookupClassification;
                    if (csmscommoninfo.CHEM_CAT != "単体" && csmscommoninfo.CHEM_CAT != "混合物" && csmscommoninfo.CHEM_CAT != "")
                    {
                        List<SelectListItem> listClassification = new List<SelectListItem> { };
                        new CommonMasterInfo(NikonCommonMasterName.CHEM_CAT).GetSelectList().ToList().ForEach(x => listClassification.Add(new SelectListItem { Value = x.Name, Text = x.Name }));
                        listClassification.Add(new SelectListItem { Text = csmscommoninfo.CHEM_CAT, Value = csmscommoninfo.CHEM_CAT });
                        csmscommoninfo.LookupClassification = listClassification;
                        listClassification.RemoveAll(item => item == null);
                    }
                    return RedirectToAction("ZC11020", new { ChemCd = chemControlValues.CHEM_CD, mode = "3", Operation = "Register" });
                }
                else if (Request.Form["FlowCode"] == "2")
                {
                    NewRegistration ChemRegistration = new NewRegistration();
                    if (file != null)
                    {
                        if (file != null && file.ContentLength > 0)
                        {
                            MemoryStream target = new MemoryStream();
                            file.InputStream.CopyTo(target);
                            chemControlValues.SDS = target.ToArray();
                            chemControlValues.SDS_MIMETYPE = MimeMapping.GetMimeMapping(file.ContentType);
                            chemControlValues.SDS_FILENAME = file.FileName;
                        }
                    }
                    else
                    {
                        var condition = new ChemSearchModel();
                        condition.CHEM_CD = chemControlValues.CHEM_CD;
                        condition.CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching;
                        condition.USAGE_STATUS = (int)Classes.UsageStatusCondtionType.All;
                        if (condition.CHEM_CD != null)
                        {
                            var d = new ChemDataInfo();
                            var chemOld = d.GetSearchResult(condition).Single();
                            d.Dispose();

                            if (chemOld.SDS != null)
                            {
                                chemControlValues.SDS = chemOld.SDS;
                                chemControlValues.SDS_MIMETYPE = chemOld.SDS_MIMETYPE;
                                chemControlValues.SDS_FILENAME = chemOld.SDS_FILENAME;
                            }
                        }
                    }
                   

                    ChemRegistration.updateRecords(chemControlValues);

                   
                    dropdownlistDTO = dropdownlist.ddlChemList();
                    ViewBag.ddl = dropdownlistDTO;
                    csmscommoninfo.LookupManufacturerName = dropdownlistDTO.LookupOfManufacturer;
                    csmscommoninfo.LookupUnitSizeMasterInfo = dropdownlistDTO.LookupUnitSizeMasterInfo;
                    csmscommoninfo.LookupClassification = dropdownlistDTO.LookupClassification;
                    if (csmscommoninfo.CHEM_CAT != "単体" && csmscommoninfo.CHEM_CAT != "混合物" && csmscommoninfo.CHEM_CAT != "")
                    {
                        List<SelectListItem> listClassification = new List<SelectListItem> { };
                        new CommonMasterInfo(NikonCommonMasterName.CHEM_CAT).GetSelectList().ToList().ForEach(x => listClassification.Add(new SelectListItem { Value = x.Name, Text = x.Name }));
                        listClassification.Add(new SelectListItem { Text = csmscommoninfo.CHEM_CAT, Value = csmscommoninfo.CHEM_CAT });
                        csmscommoninfo.LookupClassification = listClassification;
                        listClassification.RemoveAll(item => item == null);
                    }
                    return RedirectToAction("ZC11020", new { ChemCd = chemControlValues.CHEM_CD, mode = "3", Operation = "Update" });
                }
                else if (Request.Form["FlowCode"] == "3")
                {
                    NewRegistration ChemRegistration = new NewRegistration();
                    if (chemControlValues.CHEM_CD.Substring(0, 2).Contains("WK"))
                    {
                        CsmsCommonInfo csmscommoninfo = new CsmsCommonInfo();
                        ChemRegistration.LegalReflection(chemControlValues);

                        var RegulatlationArray = GetSelectedRegulation(null, null, Session[sessionRegulationIds] as List<int>).AsEnumerable();
                        csmscommoninfo.RegulationModel = RegulatlationArray.ToList();

                        //Fire Service act Binding of the Data
                        var FireGrid = GetSelectedFire(null, null, Session[sessionFireIds] as List<int>).AsEnumerable();
                        csmscommoninfo.FireModel = FireGrid.ToList();

                        //This Method gives InternalRules/office grid records
                        var internalRules = GetSelectedOffice(null, null, Session[sessionOfficeIds] as List<int>).AsEnumerable();
                        csmscommoninfo.OfficeGridModel = internalRules.ToList();
                        //This Method gives GHS Clasification grid records               

                        var GHSClassification = GetSelectedGhsCategory(null, null, Session[sessionGhsCategoryIds] as List<int>).AsEnumerable();
                        csmscommoninfo.GHSModel = GHSClassification.ToList();

                        if (Session["Contained"] != null)
                        {
                            List<ChemContainedModel> ChemContained = (List<ChemContainedModel>)Session["Contained"];
                            csmscommoninfo.ContainedModel = ChemContained;
                        }
                        else
                        {
                            var ContainedResult = GetSelectedContained(null, null, Session[sessionContaineds] as IEnumerable<ChemContainedModel>).AsEnumerable();
                            csmscommoninfo.ContainedModel = ContainedResult.ToList();
                        }


                        if (chemControlValues.CHEM_CD != null)
                        {
                            if (Session["sessionAliases"] != null)
                            {
                                List<ChemAliasModel> chemAliases = (List<ChemAliasModel>)Session["sessionAliases"];
                                csmscommoninfo.ChemAliasModel = chemAliases;
                            }
                            else
                            {
                                var AliasArray = GetInputedAlias(chemControlValues.CHEM_CD, "2").AsEnumerable();
                                csmscommoninfo.ChemAliasModel = AliasArray.ToList();
                            }
                        }


                        if (Session["ProductDetails"] != null)
                        {
                            List<ChemProductModel> chemproductmodel = (List<ChemProductModel>)Session["ProductDetails"];
                            csmscommoninfo.ChemProductListModel = chemproductmodel;
                        }
                        else
                        {
                            //This Method gives Product Details grid records
                            var productDetails = GetProducts(chemControlValues.CHEM_CD, null).AsEnumerable();
                            csmscommoninfo = GetLookupValues(csmscommoninfo);
                            csmscommoninfo.ChemProductListModel = productDetails.ToList();
                        }

                        csmscommoninfo.CHEM_CD = chemControlValues.CHEM_CD;
                        csmscommoninfo.CHEM_CAT = chemControlValues.CHEM_CAT;
                        csmscommoninfo.CAS_NO = chemControlValues.CAS_NO;
                        csmscommoninfo.CHEM_NAME = chemControlValues.CHEM_NAME;
                        csmscommoninfo.FIGURE = chemControlValues.FIGURE;
                        csmscommoninfo.DENSITY = chemControlValues.DENSITY;
                        csmscommoninfo.FLASH_POINT = chemControlValues.FLASH_POINT;
                        csmscommoninfo.KEY_MGMT = chemControlValues.KEY_MGMT;
                        csmscommoninfo.WEIGHT_MGMT = chemControlValues.WEIGHT_MGMT;
                        csmscommoninfo.SDS_FILENAME = chemControlValues.SDS_FILENAME;
                        csmscommoninfo.SDS_URL = chemControlValues.SDS_URL;
                        csmscommoninfo.MEMO = chemControlValues.MEMO;
                        csmscommoninfo.OPTIONAL1 = chemControlValues.OPTIONAL1;
                        csmscommoninfo.OPTIONAL2 = chemControlValues.OPTIONAL2;
                        csmscommoninfo.OPTIONAL3 = chemControlValues.OPTIONAL3;
                        csmscommoninfo.OPTIONAL4 = chemControlValues.OPTIONAL4;
                        csmscommoninfo.OPTIONAL5 = chemControlValues.OPTIONAL5;
                        csmscommoninfo.SDS_UPD_DATE2 = chemControlValues.SDS_UPD_DATE2;
                        csmscommoninfo.REG_USER_CD = chemControlValues.REG_USER_CD;
                        csmscommoninfo.SDS_URL2 = chemControlValues.SDS_URL2;
                        csmscommoninfo.Hiddenoperation = "Delete";
                        csmscommoninfo.HiddenMode = "3";
                        csmscommoninfo.HiddenChemCD = chemControlValues.HiddenChemCD;

                        dropdownlistDTO = dropdownlist.ddlChemList();
                        csmscommoninfo.LookupManufacturerName = dropdownlistDTO.LookupOfManufacturer;
                        csmscommoninfo.LookupUnitSizeMasterInfo = dropdownlistDTO.LookupUnitSizeMasterInfo;
                        csmscommoninfo.LookupClassification = dropdownlistDTO.LookupClassification;
                        if (csmscommoninfo.CHEM_CAT != "単体" && csmscommoninfo.CHEM_CAT != "混合物" && csmscommoninfo.CHEM_CAT != "")
                        {
                            List<SelectListItem> listClassification = new List<SelectListItem> { };
                            new CommonMasterInfo(NikonCommonMasterName.CHEM_CAT).GetSelectList().ToList().ForEach(x => listClassification.Add(new SelectListItem { Value = x.Name, Text = x.Name }));
                            listClassification.Add(new SelectListItem { Text = csmscommoninfo.CHEM_CAT, Value = csmscommoninfo.CHEM_CAT });
                            csmscommoninfo.LookupClassification = listClassification;
                            listClassification.RemoveAll(item => item == null);
                        }
                        ViewBag.ddl = dropdownlistDTO;



                      

                        ChemRegistration.RemovePhysical(chemControlValues);

                      


                        if (Session["SessionList"] != null)
                        {
                            CsmsCommonInfo csmsCommon = new CsmsCommonInfo();
                            csmsCommon = (CsmsCommonInfo)Session["SessionList"];
                            csmsCommon.ChemCommonList = csmsCommon.ChemCommonList.Where(x => x.CHEM_CD != chemControlValues.CHEM_CD).ToList();
                            Session["SessionList"] = csmsCommon;
                        }
                        TempData["PhysicalDelete"] = "PhysicalDelete";
                        return View(csmscommoninfo);
                    }
                    else
                    {
                       
                        ChemRegistration.Remove(chemControlValues);
                        

                        dropdownlistDTO = dropdownlist.ddlChemList();
                        csmscommoninfo.LookupManufacturerName = dropdownlistDTO.LookupOfManufacturer;
                        csmscommoninfo.LookupUnitSizeMasterInfo = dropdownlistDTO.LookupUnitSizeMasterInfo;
                        csmscommoninfo.LookupClassification = dropdownlistDTO.LookupClassification;
                        if (csmscommoninfo.CHEM_CAT != "単体" && csmscommoninfo.CHEM_CAT != "混合物" && csmscommoninfo.CHEM_CAT != "")
                        {
                            List<SelectListItem> listClassification = new List<SelectListItem> { };
                            new CommonMasterInfo(NikonCommonMasterName.CHEM_CAT).GetSelectList().ToList().ForEach(x => listClassification.Add(new SelectListItem { Value = x.Name, Text = x.Name }));
                            listClassification.Add(new SelectListItem { Text = csmscommoninfo.CHEM_CAT, Value = csmscommoninfo.CHEM_CAT });
                            csmscommoninfo.LookupClassification = listClassification;
                            listClassification.RemoveAll(item => item == null);
                        }
                        ViewBag.ddl = dropdownlistDTO;
                        if (Session["SessionList"] != null)
                        {
                            CsmsCommonInfo csmsCommon = new CsmsCommonInfo();
                            csmsCommon = (CsmsCommonInfo)Session["SessionList"];
                            csmsCommon.ChemCommonList = csmsCommon.ChemCommonList.Where(x => x.CHEM_CD != chemControlValues.CHEM_CD).ToList();
                            Session["SessionList"] = csmsCommon;
                        }
                        return RedirectToAction("ZC11020", new { ChemCd = chemControlValues.CHEM_CD, mode = "3", Operation = "Delete" });
                    }
                }

            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.StackTrace.ToString());
            }
            return View(csmscommoninfo);

        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019  
        /// Description :this method is used to for Temporary Registration
        /// <summary>

        [HttpPost]
        public ActionResult TemproryRegistration(CsmsCommonInfo chemControlValues, HttpPostedFileBase file)
        {
            try
            {

                NewRegistration ChemRegistration = new NewRegistration();
                if (file != null && file.ContentLength > 0)
                {
                    MemoryStream target = new MemoryStream();
                    file.InputStream.CopyTo(target);
                    chemControlValues.SDS = target.ToArray();
                    chemControlValues.SDS_MIMETYPE = MimeMapping.GetMimeMapping(file.ContentType);
                    chemControlValues.SDS_FILENAME = file.FileName;
                }
                else
                {
                    var condition = new ChemSearchModel();
                    condition.CHEM_CD = id;
                    condition.CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching;
                    condition.USAGE_STATUS = (int)Classes.UsageStatusCondtionType.All;
                    if (condition.CHEM_CD != null)
                    {
                        var d = new ChemDataInfo();
                        var chemOld = d.GetSearchResult(condition).Single();
                        d.Dispose();

                        if (chemOld.SDS != null)
                        {
                            chemControlValues.SDS = chemOld.SDS;
                            chemControlValues.SDS_MIMETYPE = chemOld.SDS_MIMETYPE;
                            chemControlValues.SDS_FILENAME = chemOld.SDS_FILENAME;
                        }
                    }
                }
                ChemRegistration.TemproryRegister(chemControlValues);
                dropdownlistDTO = dropdownlist.ddlChemList();
                ViewBag.ddl = dropdownlistDTO;
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.StackTrace.ToString());
            }

            return RedirectToAction("ZC11020", new { ChemCd = chemControlValues.CHEM_CD, mode = "3", Operation = "TempSave", RefeCode = chemControlValues.REF_CHEM_CD });
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019  
        /// Description :this method is used to for Loading of the Chemical Substances Grid details
        /// <summary>

        [HttpPost]
        public ActionResult ZC11010(ChemSearchModel objChem)
        {
            CsmsCommonInfo csmsmodel = new CsmsCommonInfo();
           
            ChemDataInfo chemDataInfo = new ChemDataInfo();
            try
            {
                if (Request.Form["ListOutput"] != null)
                {
                    dropdownlistDTO = dropdownlist.ddlChemList();
                    csmsmodel.ChemCommonList = (List<CsmsCommonInfo>)Session["listoutputrecords"];

                   
                    objBAL.ListOutput(csmsmodel.ChemCommonList);
                    
                    return View(dropdownlistDTO);
                }

                string username = "";
                if (Session["LoginUserSession"] != null)
                {
                    WebCommonInfo webCommon = new WebCommonInfo();
                    ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];
                    username = appUser.UserName;
                }
                else
                {
                    if (username == null || username == "")
                    {
                        RedirectToAction("ZC01001", "ZC010");
                    }
                }

                int searchtarget = objChem.CHEM_SEARCH_TARGET;
                csmsmodel.CHEM_SEARCH_TARGET = objChem.CHEM_SEARCH_TARGET;
                csmsmodel.CHEM_CAT = objChem.CHEM_CAT;
                csmsmodel.CHEM_CD_CONDITION = objChem.CHEM_CD_CONDITION;
                csmsmodel.CHEM_CD = objChem.CHEM_CD;
                csmsmodel.CHEM_NAME_CONDITION = objChem.CHEM_NAME_CONDITION;
                csmsmodel.CHEM_NAME = objChem.CHEM_NAME;
                csmsmodel.PRODUCT_NAME_CONDITION = objChem.PRODUCT_NAME_CONDITION;
                csmsmodel.PRODUCT_NAME = objChem.PRODUCT_NAME;
                csmsmodel.CAS_NO_CONDITION = objChem.CAS_NO_CONDITION;
                csmsmodel.CAS_NO = objChem.CAS_NO;
                csmsmodel.FIGURE = objChem.FIGURE;
                csmsmodel.KEY_MGMT = objChem.KEY_MGMT;
                csmsmodel.WEIGHT_MGMT = objChem.WEIGHT_MGMT;
                csmsmodel.MAKER_NAME_CONDITION = objChem.MAKER_NAME_CONDITION;
                csmsmodel.MAKER_NAME = objChem.MAKER_NAME;
                csmsmodel.EXAMINATION_FLAG = objChem.Examination_FLAG;
                csmsmodel.MEASUREMENT_FLAG = objChem.Measurement_FLAG;
                csmsmodel.IncludesContained = objChem.IncludesContained;
                csmsmodel.USAGE_STATUS = objChem.USAGE_STATUS;
                csmsmodel.REG_COLLECTION = objChem.REG_COLLECTION;
                csmsmodel.FIRE_COLLECTION = objChem.FIRE_COLLECTION;
                csmsmodel.OFFICE_COLLECTION = objChem.OFFICE_COLLECTION;
                csmsmodel.GAS_COLLECTION = objChem.GAS_COLLECTION;

                csmsmodel.hiddenRegulationIds = objChem.REG_SELECTION;
                csmsmodel.hiddenfire = objChem.FIRE_SELECTION;
                csmsmodel.hiddenoffice = objChem.OFFICE_SELECTION;
                csmsmodel.hiddenGHS = objChem.GAS_SELECTION;

                if (objChem.REG_COLLECTION != null && objChem.REG_COLLECTION.Count() > 0)
                {
                    csmsmodel.lblalreadySetValue = "設定済み";
                }
                if (objChem.FIRE_COLLECTION != null && objChem.FIRE_COLLECTION.Count() > 0)
                {
                    csmsmodel.lblalreadySetValue = "設定済み";
                }
                if (objChem.OFFICE_COLLECTION != null && objChem.OFFICE_COLLECTION.Count() > 0)
                {
                    csmsmodel.lblalreadySetValue = "設定済み";
                }
                if (objChem.GAS_COLLECTION != null && objChem.GAS_COLLECTION.Count() > 0)
                {
                    csmsmodel.lblalreadySetValue = "設定済み";
                }

                ViewBag.searchtarget = searchtarget;
               

                List<CsmsCommonInfo> csmslistmodel = Session["SearchResultRows"] as List<CsmsCommonInfo>;
                dropdownlistDTO = dropdownlist.ddlChemList();
                ViewBag.ddl = dropdownlistDTO;
                if (csmslistmodel.Count() >= 500)
                {
                    csmsmodel.ChemCommonList = (List<CsmsCommonInfo>)csmslistmodel.GetRange(0, 500);
                }
                else
                {
                    csmsmodel.ChemCommonList = csmslistmodel;
                }
                int Count = csmslistmodel.Count;
                Session.Add("SessionList", csmsmodel);

                //This list supposed to be cleared once its use is done!
                Session.Add("listoutputrecords", csmslistmodel);

               



                TempData["DBCall"] = "DB";

            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.StackTrace.ToString());
            }
            return View(csmsmodel);
        }



        [HttpPost]
        public void GetAllRecords(ChemSearchModel Conditions)
        {
            try
            {
                ChemDataInfo Chemdata = new ChemDataInfo();
                List<CsmsCommonInfo> AllRecords = Chemdata.GetSearchResult(Conditions);
                Session.Add("listoutputrecords", AllRecords);
            }
            catch (Exception exe)
            {
                exe.StackTrace.ToString();

            }
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019  
        /// Description :this method is used to for Deleting of the Record
        /// <summary>
        [HttpPost]
        public ActionResult Delete(CsmsCommonInfo objChem)
        {
            

            NewRegistration ChemRegistration = new NewRegistration();
            try
            {
                
                ChemRegistration.Remove(objChem);


                dropdownlistDTO = dropdownlist.ddlChemList();
                ViewBag.ddl = dropdownlistDTO;

                if (Session["SessionList"] != null)
                {
                    CsmsCommonInfo csmsCommon = new CsmsCommonInfo();
                    csmsCommon = (CsmsCommonInfo)Session["SessionList"];
                    csmsCommon.ChemCommonList = csmsCommon.ChemCommonList.Where(x => x.CHEM_CD != objChem.CHEM_CD).ToList();
                    Session["SessionList"] = csmsCommon;
                }

            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }

            return RedirectToAction("ZC11020", new { ChemCd = objChem.CHEM_CD, mode = "3", Operation = "Delete" });
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-8-2019  
        /// Description :this method is used to for undoing the Deleted Record
        /// <summary>
        [HttpPost]
        public ActionResult DeleteCancel(CsmsCommonInfo objChem)
        {
           
            NewRegistration ChemRegistration = new NewRegistration();
            try
            {
               
                ChemRegistration.UndoRemove(objChem);
               

                dropdownlistDTO = dropdownlist.ddlChemList();
                ViewBag.ddl = dropdownlistDTO;
                if (Session["SessionList"] != null)
                {
                    CsmsCommonInfo csmsCommon = new CsmsCommonInfo();
                    csmsCommon = (CsmsCommonInfo)Session["SessionList"];
                    csmsCommon.ChemCommonList = csmsCommon.ChemCommonList.Where(x => x.CHEM_CD != objChem.CHEM_CD).ToList();
                    Session["SessionList"] = csmsCommon;
                }


            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return RedirectToAction("ZC11020", new { ChemCd = objChem.CHEM_CD, mode = "4", Operation = "DeleteCancel" });
        }



        /// <summary>
        /// Author: MSCGI
        /// Created date: 20-8-2019  
        /// Description :Clears the Entered Details in the search Page
        /// <summary>
        [HttpPost]
        public ActionResult ClearSearch(ChemSearchModel searchResult)
        {
            try
            {
                Session["SessionList"] = null;
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return RedirectToAction("ZC11010", "ZC110");
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 21-8-2019  
        /// Description :Comes Back To the previous page
        /// <summary>
        public ActionResult RegistrationBackbutton()
        {
            NewRegistration ChemRegistration = new NewRegistration();
            ChemRegistration.ClearSession();
            return RedirectToAction("ZC11010", "ZC110");
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 21-8-2019  
        /// Description :Comes Back To the Dashboard
        /// <summary>
        public ActionResult SearchBack()
        {
            try
            {
                Session["SearchResultRows"] = null;
                Session["SessionList"] = null;
                Session["listoutputrecords"] = null;
                Session["SearchCount"] = null;
                Session["FixedCount"] = null;
                Session["SearchResultRows"] = null;

                Session.Remove("SearchResultRows");
                Session.Remove("SearchCount");
                Session.Remove("FixedCount");
                Session.Remove("SearchResultRows");
                Session.Remove("SessionList");
                Session.Remove("listoutputrecords");
            }
            catch (Exception exe)
            {
                exe.StackTrace.ToString();
            }
            return RedirectToAction("ZC01011", "ZC010");
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 21-8-2019  
        /// Description :Navigate to Registration page
        /// <summary>
        public ActionResult RedirectRegistration()
        {
            NewRegistration ChemRegistration = new NewRegistration();
            ChemRegistration.ClearSession();
            return RedirectToAction("ZC11020", "ZC110", new { mode = 0 });
        }



        [HttpPost]
        /// <summary>
        /// Author: MSCGI
        /// Created date: 28-8-2019  
        /// Description :this Method loads the Maker Name and Unitsize_id
        /// <summary>
        public ActionResult Manufacturer()
        {
            List<SelectListItem> listOfManufacturer = new List<SelectListItem> { };
            List<SelectListItem> listofUnitSize = new List<SelectListItem> { };
            object[] ArrayList = new object[] { };
            try
            {
                new MakerMasterInfo().GetSelectList().ToList().
                    ForEach(x => listOfManufacturer.Add(new SelectListItem
                    { Value = x.Id.ToString(), Text = x.Name }));

                new UnitSizeMasterInfo().GetSelectList().ToList().
                    ForEach(x => listofUnitSize.Add(new SelectListItem
                    { Value = x.Id.ToString(), Text = x.Name }));
                ArrayList = new object[] { listOfManufacturer, listofUnitSize };
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return Json(ArrayList);
        }




        /// <summary>
        /// Author: MSCGI
        /// Created date: 30-8-2019  
        /// Description :this Method loads Regulation Details
        /// <summary>
        [HttpPost]
        public string GetRegulationDetails()
        {
            var reg = "";
            try
            {
                Session[sessionRegulationIds] = null;
                var selectValue = (Dictionary<string, string>)null;
                if (Session[SessionConst.CategorySelectReturnValue] != null)
                {
                    selectValue = (Dictionary<string, string>)Session[SessionConst.CategorySelectReturnValue];

                    var regulationIds = selectValue.Values.ToList().ConvertAll(x => Convert.ToInt32(x)).AsEnumerable();

                    Session.Add(sessionRegulationIds, regulationIds);

                    //Laws and regulations Binding of the Data
                    var regulatlationArray = GetSelectedRegulation(null, null, Session[sessionRegulationIds] as List<int>).AsEnumerable();
                    csmscommoninfo.RegulationModel = regulatlationArray.ToList();
                    Session["RegulationModel"] = csmscommoninfo.RegulationModel;
                }

            }
            catch (Exception exe)
            {
                reg = "";
            }

            if (csmscommoninfo.RegulationModel != null)
            {
                foreach (var item in csmscommoninfo.RegulationModel)
                {
                    if (item.IconUrl == null)
                    {
                        item.REG_ICON_PATH = null;
                    }

                    reg += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + item.REG_TYPE_ID + "></td><td align='Center'><img src='" + item.IconUrl + "' style='height:32px;'></td><td style='width: 364px;'><font style='vertical-align: inherit;'><font style='vertical-align:inherit;'>" + item.REG_TEXT + "</font></font></td><td style='width:50px;'><font style='vertical-align: inherit;'><font style='vertical-align:inherit;'></font>" + item.INPUT_FLAG + "</font></td></tr>";
                }
            }
            return reg;

        }



        /// <summary>
        /// Author: MSCGI
        /// Created date: 03-9-2019  
        /// Description :this Method loads Fire Details
        /// <summary>
        public string GetFireIdsDetails()
        {
            var Fire = "";
            try
            {
                Session[sessionFireIds] = null;
                var selectValue = (Dictionary<string, string>)null;
                if (Session[SessionConst.CategorySelectReturnValue] != null)
                {
                    selectValue = (Dictionary<string, string>)Session[SessionConst.CategorySelectReturnValue];
                }

                var fireIds = selectValue.Values.ToList().ConvertAll(x => Convert.ToInt32(x)).AsEnumerable();
                Session.Add(sessionFireIds, fireIds);

                //Fire Service act Binding of the Data
                var fireGrid = GetSelectedFire(null, null, Session[sessionFireIds] as List<int>).AsEnumerable();
                csmscommoninfo.FireModel = fireGrid.ToList();
                Session["FireServicesModel"] = csmscommoninfo.FireModel;
            }
            catch (Exception exe)
            {
                Fire = "";
            }

            if (csmscommoninfo.FireModel != null)
            {
                foreach (var item in csmscommoninfo.FireModel)
                {

                    Fire += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + item.FIRE_ID + "></td><td align='Center'><img src='" + item.IconUrl + "' style='height:32px;'/></td><td style='width:364px;'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'>" + item.FIRE_NAME + "</font></font></td><td style='width: 50px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'></font>" + item.INPUT_FLAG + "</font></td></tr>";
                }
            }
            return Fire;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :this Method loads office Details
        /// <summary>
        public string GetofficeIdsDetails()
        {
            var office = "";
            try
            {
                Session[sessionOfficeIds] = null;
                var selectValue = (Dictionary<string, string>)null;
                if (Session[SessionConst.CategorySelectReturnValue] != null)
                {
                    selectValue = (Dictionary<string, string>)Session[SessionConst.CategorySelectReturnValue];
                }

                var officeids = selectValue.Values.ToList().ConvertAll(x => Convert.ToInt32(x)).AsEnumerable();
                Session.Add(sessionOfficeIds, officeids);

                //This Method gives InternalRules/office grid records
                var internalRules = GetSelectedOffice(null, null, Session[sessionOfficeIds] as List<int>).AsEnumerable();
                csmscommoninfo.OfficeGridModel = internalRules.ToList();
                Session["OfficeRulesmodel"] = csmscommoninfo.OfficeGridModel;

                if (csmscommoninfo.OfficeGridModel != null)
                {
                    foreach (var item in csmscommoninfo.OfficeGridModel)
                    {
                        office += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + item.OFFICE_ID + "></td><td align='center' style='width: 70px;'><img src='" + item.IconUrl + "' style='height: 32px;'/></td><td style='width:364px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'>" + item.OFFICE_NAME + "</font></font></td><td style='width: 50px;'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'></font>" + item.INPUT_FLAG + "</font></td></tr>";
                    }
                }

            }
            catch (Exception exe)
            {
                exe.StackTrace.ToString();
                office = "";
            }
            return office;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :this Method loads ghs Details
        /// <summary>
        public string GetghsIdsDetails()
        {
            var ghs = "";
            try
            {
                Session[sessionGhsCategoryIds] = null;
                var selectValue = (Dictionary<string, string>)null;
                if (Session[SessionConst.CategorySelectReturnValue] != null)
                {
                    selectValue = (Dictionary<string, string>)Session[SessionConst.CategorySelectReturnValue];
                }

                var ghsids = selectValue.Values.ToList().ConvertAll(x => Convert.ToInt32(x)).AsEnumerable();
                Session.Add(sessionGhsCategoryIds, ghsids);

                //This Method gives GHS Clasification grid records
                var ghsClassification = GetSelectedGhsCategory(null, null, Session[sessionGhsCategoryIds] as List<int>).AsEnumerable();
                csmscommoninfo.GHSModel = ghsClassification.ToList();
                Session["GHSModel"] = csmscommoninfo.GHSModel;
            }
            catch (Exception exe)
            {
                exe.StackTrace.ToString();
                ghs = "";
            }

            if (csmscommoninfo.GHSModel != null)
            {
                foreach (var item in csmscommoninfo.GHSModel)
                {

                    ghs += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + item.GHSCAT_ID + "></td><td align='Center'><img src='" + item.IconUrl + "' style='height:32px;'/></td><td style='width:364px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'>" + item.GHSCAT_NAME + "</font></font></td><td style='width:50px;'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'></font>" + item.INPUT_FLAG + "</font></td></tr>";
                }
            }
            return ghs;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :stores max Result count
        /// <summary>
        [HttpPost]
        public ActionResult GetChemResultCount()
        {
            Session["FixedCount"] = null;
            return View("ZC11010");
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 09-9-2019  
        /// Description :updates alias Names
        /// <summary>
        public void UpdateAliaslist(string ChemAliasName, int Rowid)
        {
            try
            {
                if (Session["sessionAliases"] != null)
                {
                    List<ChemAliasModel> Chemalias = new List<ChemAliasModel>();
                    Chemalias = (List<ChemAliasModel>)Session["sessionAliases"];
                    for (int i = 0; i < Chemalias.Count; i++)
                    {
                        if (Chemalias[i].CHEM_NAME_SEQ == Rowid)
                        {
                            Chemalias[i].CHEM_NAME = ChemAliasName;
                        }
                    }

                    Session.Add("sessionAliases", Chemalias);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();

            }
        }



        public void UpdateMakerId(string MakerId, int Rowid)
        {
            try
            {
                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> Chemproduct = (List<ChemProductModel>)Session["ProductDetails"];
                    for (int i = 0; i < Chemproduct.Count; i++)
                    {
                        if (Chemproduct[i].TempPRODUCT_SEQ == Rowid || Chemproduct[i].PRODUCT_SEQ == Rowid)
                        {
                            Chemproduct[i].MAKER_ID = Convert.ToInt32(MakerId);
                        }
                    }
                    Session.Add("ProductDetails", Chemproduct);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }
        public void UpdateProductName(string ProductName, int Rowid)
        {
            try
            {
                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> Chemproduct = (List<ChemProductModel>)Session["ProductDetails"];
                    for (int i = 0; i < Chemproduct.Count; i++)
                    {
                        if (Chemproduct[i].TempPRODUCT_SEQ == Rowid || Chemproduct[i].PRODUCT_SEQ == Rowid)
                        {
                            Chemproduct[i].PRODUCT_NAME = ProductName;
                        }
                    }
                    Session.Add("ProductDetails", Chemproduct);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }
        public void UpdateCapacity(string Capacity, int Rowid)
        {
            try
            {
                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> Chemproduct = (List<ChemProductModel>)Session["ProductDetails"];
                    for (int i = 0; i < Chemproduct.Count; i++)
                    {
                        if (Chemproduct[i].TempPRODUCT_SEQ == Rowid || Chemproduct[i].PRODUCT_SEQ == Rowid)
                        {
                            Chemproduct[i].UNITSIZE = Capacity;
                        }
                    }
                    Session.Add("ProductDetails", Chemproduct);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }
        public void UpdateDensity(string Density, int Rowid)
        {
            try
            {
                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> Chemproduct = (List<ChemProductModel>)Session["ProductDetails"];
                    for (int i = 0; i < Chemproduct.Count; i++)
                    {
                        if (Chemproduct[i].TempPRODUCT_SEQ == Rowid || Chemproduct[i].PRODUCT_SEQ == Rowid)
                        {
                            Chemproduct[i].UNITSIZE_ID = Convert.ToInt32(Density);
                        }
                    }
                    Session.Add("ProductDetails", Chemproduct);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }

        public void UpdateProductBarcode(string ProductBarcode, int Rowid)
        {
            try
            {
                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> Chemproduct = (List<ChemProductModel>)Session["ProductDetails"];
                    for (int i = 0; i < Chemproduct.Count; i++)
                    {
                        if (Chemproduct[i].TempPRODUCT_SEQ == Rowid || Chemproduct[i].PRODUCT_SEQ == Rowid)
                        {
                            Chemproduct[i].PRODUCT_BARCODE = ProductBarcode;
                        }
                    }
                    Session.Add("ProductDetails", Chemproduct);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }
        public void UpdateSDSURL(string SDSURL, int Rowid)
        {
            try
            {
                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> Chemproduct = (List<ChemProductModel>)Session["ProductDetails"];
                    for (int i = 0; i < Chemproduct.Count; i++)
                    {
                        if (Chemproduct[i].TempPRODUCT_SEQ == Rowid || Chemproduct[i].PRODUCT_SEQ == Rowid)
                        {
                            Chemproduct[i].SDS_URL = SDSURL;
                        }
                    }
                    Session.Add("ProductDetails", Chemproduct);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }

        public void UpdateUPDdate(string UPDdate, int Rowid)
        {
            try
            {
                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> Chemproduct = (List<ChemProductModel>)Session["ProductDetails"];
                    for (int i = 0; i < Chemproduct.Count; i++)
                    {
                        if (Chemproduct[i].TempPRODUCT_SEQ == Rowid || Chemproduct[i].PRODUCT_SEQ == Rowid)
                        {
                            if (!string.IsNullOrWhiteSpace(UPDdate))
                            {
                                Chemproduct[i].SDS_UPD_DATE = Convert.ToDateTime(UPDdate);
                            }
                            else
                            {
                                Chemproduct[i].SDS_UPD_DATE = null;
                            }
                        }
                    }
                    Session.Add("ProductDetails", Chemproduct);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :gives the Alias Details
        /// <summary>
        public IQueryable<ChemAliasModel> GetInputedAlias(string ChemCd, string mode)
        {
            var newDataArray = new List<ChemAliasModel>();
            try
            {
                if (Session[sessionAliases] != null)
                {
                    //return aliases.ToList().AsQueryable();
                }
                if (!string.IsNullOrEmpty(ChemCd))
                {
                    var condition = new ChemAliasSearchModel();
                    condition.CHEM_CD = ChemCd;
                    var d = new ChemAliasDataInfo();
                    var dataArray = d.GetSearchResult(condition).ToList();
                    d.Dispose();

                    // 化学物質マスタ－に登録されている別名を削除して表示します。
                    var deleteData = dataArray.SingleOrDefault(x => x.CHEM_NAME_SEQ == 0);
                    if (deleteData != null) dataArray.Remove(dataArray.Single(x => x.CHEM_NAME_SEQ == 0));

                    Session.Add(sessionAliases, dataArray.AsEnumerable());
                    Session.Add(OldsessionAliases, dataArray.AsEnumerable());
                    return dataArray.AsQueryable();
                }
                if (mode != "3")
                {
                    Session.Add(sessionAliases, newDataArray.AsEnumerable());
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return newDataArray.AsQueryable();
        }


        /// /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :this Method gives the selected Details
        /// <summary>
        public IQueryable<ChemContainedModel> GetSelectedContained(string ChemCd, string mode, IEnumerable<ChemContainedModel> containeds)
        {
            if (!string.IsNullOrEmpty(ChemCd))
            {
                var condition = new ChemContainedSearchModel();
                condition.CHEM_CD = ChemCd;
                var d = new ChemContainedDataInfo();
                var dataArray = d.GetSearchResult(condition).AsEnumerable();
                d.Dispose();
                string containedvalue = null;
                foreach (var temp in dataArray)
                {
                    if (!string.IsNullOrEmpty(temp.C_CHEM_CD))
                    {
                        containedvalue += temp.C_CHEM_CD + ",";
                    }
                }
                if (containedvalue != null)
                {
                    containedvalue = containedvalue.TrimEnd(',');
                }

                Session.Add(sessionContaineds, containedvalue);
                return dataArray.AsQueryable();
            }
            var newDataArray = new List<ChemContainedModel>();
            return newDataArray.AsQueryable();
        }

        /// <summary>
        /// GHS分類のSelectMethodで選択済みのGHS分類を取得します。
        /// </summary>
        /// <param name="ghsCategoryIds">セッションに格納されている選択済みのGHS分類のIDを抽出します。</param>
        /// <returns>選択済みのGHS分類を取得します。</returns>


        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :this Method loads GetSelectedGhsCategory Details
        /// <summary>
        public IQueryable<V_GHSCAT> GetSelectedGhsCategory(string ChemCd, string mode, List<int> ghsCategoryIds)
        {
            if (ghsCategoryIds != null)
            {
                if (Session[sessionGhsCategoryIds] != null)
                {
                    if (ghsFlag == true)
                    {
                        List<V_GHSCAT> item = new List<V_GHSCAT>();
                        List<string> reg = new List<string>();

                        //セッションのを入れる                  
                        List<string> selfInputReg = new List<string>();
                        //string[] regarry = ghsCategory.Value.Split(',');

                        string[] regarray = null;
                        if (csmscommoninfo.hiddenfire != null)
                        {
                            string exitingGhsIds = csmscommoninfo.hiddenfire;
                            regarray = exitingGhsIds.Split(',');
                        }

                        //法規制反映ボタン押下した際
                        //すでにGridに手入力の法規制を取得する
                        if (Session["GHSModel"] != null)
                        {
                            List<V_GHSCAT> vghs = new List<V_GHSCAT>();
                            vghs = (List<V_GHSCAT>)Session["GHSModel"];
                            foreach (var row in vghs)
                            {
                                if (row.INPUT_FLAG == "手入力")
                                {
                                    selfInputReg.Add(row.GHSCAT_ID.ToString());
                                }
                            }
                        }

                        //手入力のものはGridに表示させるため、戻り値に入れる
                        foreach (var temp in selfInputReg)
                        {
                            if (!ghsCategoryIds.Contains<int>(Convert.ToInt16(temp)))
                            {
                                item.Add(new GhsCategoryMasterInfo().GetSelectedOneItems(Convert.ToInt16(temp)));
                                reg.Add(temp);

                            }
                        }
                        //法規制で引きあたったもの
                        foreach (var temp2 in ghsCategoryIds)
                        {
                            item.Add(new GhsCategoryMasterInfoAuto().GetSelectedOneItems(Convert.ToInt16(temp2)));
                            reg.Add(temp2.ToString());
                        }
                        ghsFlag = false;


                        List<V_GHSCAT> tempItem = new List<V_GHSCAT>();
                        tempItem = item.OrderBy(x => x.P_GHSCAT_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string ghsvalue = "";
                        foreach (V_GHSCAT temp in tempItem)
                        {
                            ghsvalue = ghsvalue + temp.GHSCAT_ID + ",";
                        }
                        ghsvalue = ghsvalue.TrimEnd(',');
                        csmscommoninfo.hiddenGHS = ghsvalue;
                        return item.OrderBy(x => x.P_GHSCAT_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();
                    }

                    else if (ghsdel == true)
                    {
                        List<string> reg = new List<string>();
                        List<V_GHSCAT> item = new List<V_GHSCAT>();
                        foreach (var temp in ghsCategoryIds)
                        {
                            reg.Add(temp.ToString());

                            foreach (var temp2 in delthengridghs)
                            {
                                if (temp2.GHSCAT_ID == temp)
                                {

                                    if (temp2.INPUT_FLAG == "1")
                                    {
                                        item.Add(new GhsCategoryMasterInfo().GetSelectedOneItems(temp));
                                    }
                                    else
                                    {
                                        item.Add(new GhsCategoryMasterInfoAuto().GetSelectedOneItems(temp));
                                    }
                                }
                            }

                        }

                        List<V_GHSCAT> tempItem = new List<V_GHSCAT>();
                        tempItem = item.OrderBy(x => x.P_GHSCAT_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string ghsValue = "";
                        foreach (V_GHSCAT temp in tempItem)
                        {
                            ghsValue = ghsValue + temp.GHSCAT_ID + ",";
                        }
                        ghsValue = ghsValue.TrimEnd(',');
                        csmscommoninfo.hiddenGHS = ghsValue;
                        return item.OrderBy(x => x.P_GHSCAT_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();

                    }
                    else
                    {
                        List<string> inputReg = new List<string>();
                        List<V_GHSCAT> item = new List<V_GHSCAT>();
                        List<string> reg = new List<string>();

                        //既存のGridの内容を取得する
                        //法規制引当で引き当てた法規制が手入力で上書きされないようにする
                        if (Session["GHSModel"] != null)
                        {
                            List<V_GHSCAT> vghs = new List<V_GHSCAT>();
                            vghs = (List<V_GHSCAT>)Session["GHSModel"];
                            foreach (var row in vghs)
                            {
                                if (row.INPUT_FLAG == "" || row.INPUT_FLAG == null && row.GHSCAT_ID != 0)
                                {
                                    inputReg.Add(row.GHSCAT_ID.ToString());
                                }
                            }
                        }

                        //選択画面で選択したものとinputRegを比較し、同じものがあれば、上書きされないようにする
                        foreach (string temp in inputReg)
                        {
                            foreach (var ids in ghsCategoryIds)
                            {
                                if (temp == ids.ToString())
                                {
                                    item.Add(new GhsCategoryMasterInfoAuto().GetSelectedOneItems(Convert.ToInt16(temp)));
                                    reg.Add(temp);
                                }
                            }
                        }

                        //選択画面で追加したものを処理する
                        foreach (var temp2 in ghsCategoryIds)
                        {
                            if (!inputReg.Contains(temp2.ToString()))
                            {
                                item.Add(new GhsCategoryMasterInfo().GetSelectedOneItems(Convert.ToInt16(temp2)));
                                reg.Add(temp2.ToString());
                            }
                        }

                        List<V_GHSCAT> tempItem = new List<V_GHSCAT>();
                        tempItem = item.OrderBy(x => x.P_GHSCAT_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string ghsValue = "";
                        foreach (V_GHSCAT temp in tempItem)
                        {
                            ghsValue = ghsValue + temp.GHSCAT_ID + ",";
                        }
                        ghsValue = ghsValue.TrimEnd(',');
                        csmscommoninfo.hiddenGHS = ghsValue;
                        return item.OrderBy(x => x.P_GHSCAT_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();

                    }
                }
            }
            else if (!string.IsNullOrEmpty(ChemCd))
            {
                using (var ghsCategoryInfo = new Nikon.GhsCategoryMasterInfo())
                {
                    List<V_GHSCAT> item = new List<V_GHSCAT>();
                    var ids = ghsCategoryInfo.GetIdsBelongInChem(ChemCd);
                    var idsFlag = ghsCategoryInfo.GetIdsInputFlg(ChemCd);    //2019/04/15 TPE.Rin Add
                    Session.Add(sessionGhsCategoryIds, ids);

                    //手入力と分ける必要があるため
                    foreach (var lineidsflag in idsFlag)
                    {
                        if (lineidsflag.Value.ToString() == "0")
                        {
                            item.Add(new GhsCategoryMasterInfoAuto().GetSelectedOneItems(lineidsflag.Key));
                        }
                        else
                        {
                            item.Add(new Nikon.GhsCategoryMasterInfo().GetSelectedOneItems(lineidsflag.Key));
                        }
                    }


                    List<V_GHSCAT> tempItem = new List<V_GHSCAT>();
                    tempItem = item.OrderBy(x => x.P_GHSCAT_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                    string ghsValue = "";
                    foreach (V_GHSCAT temp in tempItem)
                    {
                        ghsValue = ghsValue + temp.GHSCAT_ID + ",";
                    }
                    ghsValue = ghsValue.TrimEnd(',');
                    csmscommoninfo.hiddenGHS = ghsValue;
                    return item.OrderBy(x => x.P_GHSCAT_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();
                }
            }
            if (mode != "3")
            {
                Session.Add(sessionGhsCategoryIds, new List<int>().AsEnumerable());
            }
            return new List<V_GHSCAT>().AsQueryable();
        }

        /// <summary>
        /// 法規制のSelectMethodで選択済みの法規制を取得します。
        /// </summary>
        /// <param name="regulationIds">セッションに格納されている選択済みの法規制のIDを抽出します。</param>
        /// <returns>選択済みの法規制を取得します。</returns>

        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :this Method loads Selected Regulation Details
        /// <summary>
        public IQueryable<V_REGULATION> GetSelectedRegulation(string ChemCd, string mode, List<int> regulationIds)
        {
            if (regulationIds != null && regulationIds.Count > 0)
            {
                if (Session[sessionRegulationIds] != null)
                {
                    if (regulationFlag == true)
                    {
                        List<V_REGULATION> item = new List<V_REGULATION>();
                        List<string> reg = new List<string>();

                        //セッションのを入れる                  
                        List<string> selfInputReg = new List<string>();
                        string[] regArray = null;
                        if (csmscommoninfo.hiddenRegulationIds != null)
                        {
                            string exitingRegulationIds = csmscommoninfo.hiddenRegulationIds;
                            regArray = exitingRegulationIds.Split(',');
                        }

                        //法規制反映ボタン押下した際
                        //すでにGridに手入力の法規制を取得する
                        if (Session["RegulationModel"] != null)
                        {
                            List<V_REGULATION> vRegulation = new List<V_REGULATION>();
                            vRegulation = (List<V_REGULATION>)Session["RegulationModel"];
                            foreach (var row in vRegulation)
                            {
                                if (row.INPUT_FLAG == "手入力")
                                {
                                    selfInputReg.Add(row.REG_TYPE_ID.ToString());
                                }
                            }
                        }

                        //手入力のものはGridに表示させるため、戻り値に入れる
                        foreach (var temp in selfInputReg)
                        {

                            if (!regulationIds.Contains<int>(Convert.ToInt16(temp)))
                            {
                                item.Add(new Nikon.RegulationMasterInfo().GetSelectedOneItems(Convert.ToInt16(temp)));
                                reg.Add(temp.ToString());
                            }
                        }
                        //法規制で引きあたったもの
                        foreach (var temp2 in regulationIds)
                        {
                            item.Add(new RegulationMasterInfoAuto().GetSelectedOneItems(Convert.ToInt16(temp2)));
                            reg.Add(temp2.ToString());

                        }

                        regulationFlag = false;
                        List<V_REGULATION> tempItem = new List<V_REGULATION>();
                        tempItem = item.OrderBy(x => x.P_REG_TYPE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string regulationValue = "";
                        foreach (V_REGULATION temp in tempItem)
                        {
                            regulationValue = regulationValue + temp.REG_TYPE_ID + ",";
                        }
                        regulationValue = regulationValue.TrimEnd(',');
                        csmscommoninfo.hiddenRegulationIds = regulationValue;
                        return item.OrderBy(x => x.P_REG_TYPE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();

                    }
                    //Gridを削除した場合
                    else if (regdel == true)
                    {
                        List<string> reg = new List<string>();
                        List<V_REGULATION> item = new List<V_REGULATION>();
                        foreach (var temp in regulationIds)
                        {
                            reg.Add(temp.ToString());
                            //法規制Gridの値
                            foreach (var temp2 in delthengridreg)
                            {
                                //法規制GridのREG_TYPE_IDと引数のREG_TYPE_IDをチェック
                                if (temp2.REG_TYPE_ID == temp)
                                {
                                    //Gridの内容が手入力だった場合
                                    if (temp2.INPUT_FLAG == "1")
                                    {
                                        item.Add(new Nikon.RegulationMasterInfo().GetSelectedOneItems(temp));
                                    }
                                    else
                                    {
                                        item.Add(new RegulationMasterInfoAuto().GetSelectedOneItems(temp));
                                    }
                                }
                            }
                        }
                        regdel = false;
                        List<V_REGULATION> tempItem = new List<V_REGULATION>();
                        tempItem = item.OrderBy(x => x.P_REG_TYPE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string regulationValue = "";
                        foreach (V_REGULATION temp in tempItem)
                        {
                            regulationValue = regulationValue + temp.REG_TYPE_ID + ",";
                        }
                        regulationValue = regulationValue.TrimEnd(',');
                        csmscommoninfo.hiddenRegulationIds = regulationValue;
                        return item.OrderBy(x => x.P_REG_TYPE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();
                    }
                    else
                    {
                        //セッションのを入れる 
                        string[] regArray = null;
                        List<string> inputReg = new List<string>();
                        if (csmscommoninfo.hiddenRegulationIds != null)
                        {
                            string exitingRegulationIds = csmscommoninfo.hiddenRegulationIds;
                            regArray = exitingRegulationIds.Split(',');
                        }

                        //返すようのList
                        List<V_REGULATION> item = new List<V_REGULATION>();
                        List<string> reg = new List<string>();

                        //既存のGridの内容を取得する
                        //法規制引当で引き当てた法規制が手入力で上書きされないようにする
                        if (Session["RegulationModel"] != null)
                        {
                            List<V_REGULATION> vregulation = new List<V_REGULATION>();
                            vregulation = (List<V_REGULATION>)Session["RegulationModel"];
                            foreach (var row in vregulation)
                            {
                                if (row.INPUT_FLAG == "" || row.INPUT_FLAG == null && row.REG_TYPE_ID != 0)
                                {
                                    inputReg.Add(row.REG_TYPE_ID.ToString());
                                }
                            }
                        }


                        //選択画面で選択したものとinputRegを比較し、同じものがあれば、上書きされないようにする
                        foreach (string temp in inputReg)
                        {
                            foreach (var ids in regulationIds)
                            {
                                if (temp == ids.ToString())
                                {
                                    item.Add(new RegulationMasterInfoAuto().GetSelectedOneItems(Convert.ToInt16(temp)));
                                    reg.Add(temp);
                                }
                            }
                        }

                        //選択画面で追加したものを処理する
                        foreach (var temp2 in regulationIds)
                        {
                            if (!inputReg.Contains(temp2.ToString()))
                            {
                                item.Add(new Nikon.RegulationMasterInfo().GetSelectedOneItems(Convert.ToInt16(temp2)));
                                reg.Add(temp2.ToString());
                            }
                        }
                        List<V_REGULATION> tempItem = new List<V_REGULATION>();
                        tempItem = item.OrderBy(x => x.P_REG_TYPE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string regulationValue = "";
                        foreach (V_REGULATION temp in tempItem)
                        {
                            regulationValue = regulationValue + temp.REG_TYPE_ID + ",";
                        }
                        regulationValue = regulationValue.TrimEnd(',');
                        csmscommoninfo.hiddenRegulationIds = regulationValue;
                        return item.OrderBy(x => x.P_REG_TYPE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();
                    }
                }
            }
            else if (!string.IsNullOrEmpty(ChemCd)) //初期画面表示時
            {
                using (var regulationInfo = new Nikon.RegulationMasterInfo())
                {
                    List<V_REGULATION> item = new List<V_REGULATION>();
                    var ids = regulationInfo.GetIdsBelongInChem(ChemCd);
                    var idsFlag = regulationInfo.GetIdsInputFlg(ChemCd);
                    Session.Add(sessionRegulationIds, ids);

                    //手入力と分ける必要があるため
                    foreach (var lineidsflag in idsFlag)
                    {
                        if (lineidsflag.Value.ToString() == "0")
                        {
                            item.Add(new RegulationMasterInfoAuto().GetSelectedOneItems(lineidsflag.Key));
                        }
                        else
                        {
                            item.Add(new Nikon.RegulationMasterInfo().GetSelectedOneItems(lineidsflag.Key));
                        }
                    }
                    List<V_REGULATION> tempItem = new List<V_REGULATION>();
                    tempItem = item.OrderBy(x => x.P_REG_TYPE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                    string regulationValue = "";
                    foreach (V_REGULATION temp in tempItem)
                    {
                        regulationValue = regulationValue + temp.REG_TYPE_ID + ",";
                    }
                    regulationValue = regulationValue.TrimEnd(',');
                    csmscommoninfo.hiddenRegulationIds = regulationValue;

                    return item.OrderBy(x => x.P_REG_TYPE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();
                }
            }
            return new List<V_REGULATION>().AsQueryable();
        }

        //2019/03/12 TPE.Rin Add Start
        /// <summary>
        /// 消防法のSelectMethodで選択済みの消防法を取得します。
        /// </summary>
        /// <param name="FireIds">セッションに格納されている選択済みの消防法のIDを抽出します。</param>
        /// <returns>選択済みの消防法を取得します。</returns>

        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :this Method loads Selected Fire Details
        /// <summary>
        public IQueryable<V_FIRE> GetSelectedFire(string ChemCd, string mode, List<int> fireIds)
        {
            if (fireIds != null && fireIds.Count > 0)
            {
                if (Session[sessionFireIds] != null)
                {
                    if (fireFlag == true)
                    {
                        List<V_FIRE> item = new List<V_FIRE>();
                        List<string> reg = new List<string>();

                        //セッションのを入れる                  
                        List<string> selfInputReg = new List<string>();


                        string[] regArray = null;
                        if (csmscommoninfo.hiddenfire != null)
                        {
                            string exitingFireIds = csmscommoninfo.hiddenfire;
                            regArray = exitingFireIds.Split(',');
                        }


                        //法規制反映ボタン押下した際
                        //すでにGridに手入力の法規制を取得する
                        if (Session["FireServicesModel"] != null)
                        {
                            List<V_FIRE> vFire = new List<V_FIRE>();
                            vFire = (List<V_FIRE>)Session["FireServicesModel"];
                            foreach (var row in vFire)
                            {
                                if (row.INPUT_FLAG == "手入力")
                                {
                                    selfInputReg.Add(row.FIRE_ID.ToString());
                                }
                            }
                        }

                        //手入力のものはGridに表示させるため、戻り値に入れる
                        foreach (var temp in selfInputReg)
                        {

                            if (!fireIds.Contains<int>(Convert.ToInt16(temp)))
                            {
                                item.Add(new FireMasterInfo().GetSelectedOneItems(Convert.ToInt16(temp)));
                                reg.Add(temp);
                            }
                        }

                        //法規制で引きあたったもの
                        foreach (var temp2 in fireIds)
                        {
                            item.Add(new FireMasterInfoAuto().GetSelectedOneItems(Convert.ToInt16(temp2)));
                            reg.Add(temp2.ToString());
                        }

                        fireFlag = false;
                        List<V_FIRE> tempItem = new List<V_FIRE>();
                        tempItem = item.OrderBy(x => x.P_FIRE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string fireValue = "";
                        foreach (V_FIRE temp in tempItem)
                        {
                            fireValue = fireValue + temp.FIRE_ID + ",";
                        }
                        fireValue = fireValue.TrimEnd(',');
                        return item.OrderBy(x => x.P_FIRE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();

                    }
                    else if (firedel == true)
                    {
                        List<string> reg = new List<string>();
                        List<V_FIRE> item = new List<V_FIRE>();

                        foreach (var temp in fireIds)
                        {
                            reg.Add(temp.ToString());
                            //消防法Gridの値
                            foreach (var temp2 in delthengridfire)
                            {
                                //消防法GridのREG_TYPE_IDと引数のREG_TYPE_IDをチェック
                                if (temp2.FIRE_ID == temp)
                                {
                                    //Gridの内容が手入力だった場合
                                    if (temp2.INPUT_FLAG == "1")
                                    {
                                        item.Add(new FireMasterInfo().GetSelectedOneItems(temp));
                                    }
                                    else
                                    {
                                        item.Add(new FireMasterInfoAuto().GetSelectedOneItems(temp));
                                    }
                                }
                            }
                        }
                        firedel = false;


                        List<V_FIRE> tempItem = new List<V_FIRE>();
                        tempItem = item.OrderBy(x => x.P_FIRE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string fireValue = "";
                        foreach (V_FIRE temp in tempItem)
                        {
                            fireValue = fireValue + temp.FIRE_ID + ",";
                        }
                        fireValue = fireValue.TrimEnd(',');
                        csmscommoninfo.hiddenfire = fireValue;
                        return item.OrderBy(x => x.P_FIRE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();

                    }
                    else
                    {
                        //セッションのを入れる                  
                        List<string> inputReg = new List<string>();
                        List<V_FIRE> item = new List<V_FIRE>();
                        List<string> reg = new List<string>();


                        //既存のGridの内容を取得する
                        //法規制引当で引き当てた法規制が手入力で上書きされないようにする
                        if (Session["FireServicesModel"] != null)
                        {
                            List<V_FIRE> vfire = new List<V_FIRE>();
                            vfire = (List<V_FIRE>)Session["FireServicesModel"];
                            foreach (var row in vfire)
                            {
                                if (row.INPUT_FLAG == "" || row.INPUT_FLAG == null && row.FIRE_ID != 0)
                                {
                                    inputReg.Add(row.FIRE_ID.ToString());
                                }
                            }
                        }

                        //選択画面で選択したものとinputRegを比較し、同じものがあれば、上書きされないようにする
                        foreach (string temp in inputReg)
                        {
                            foreach (var ids in fireIds)
                            {
                                if (temp == ids.ToString())
                                {
                                    item.Add(new FireMasterInfoAuto().GetSelectedOneItems(Convert.ToInt16(temp)));
                                    reg.Add(temp);
                                }
                            }

                        }

                        //選択画面で追加したものを処理する
                        foreach (var temp2 in fireIds)
                        {
                            if (!inputReg.Contains(temp2.ToString()))
                            {
                                item.Add(new FireMasterInfo().GetSelectedOneItems(Convert.ToInt16(temp2)));
                                reg.Add(temp2.ToString());
                            }
                        }

                        List<V_FIRE> tempItem = new List<V_FIRE>();
                        tempItem = item.OrderBy(x => x.P_FIRE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string fireValue = "";
                        foreach (V_FIRE temp in tempItem)
                        {
                            fireValue = fireValue + temp.FIRE_ID + ",";
                        }
                        fireValue = fireValue.TrimEnd(',');
                        csmscommoninfo.hiddenfire = fireValue;
                        return item.OrderBy(x => x.P_FIRE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();

                    }
                }
            }
            else if (!string.IsNullOrEmpty(ChemCd))
            {
                using (var fireInfo = new FireMasterInfo())
                {
                    List<V_FIRE> item = new List<V_FIRE>();

                    var ids = fireInfo.GetIdsBelongInChem(ChemCd);
                    var idsFlag = fireInfo.GetIdsInputFlg(ChemCd);
                    Session.Add(sessionFireIds, ids);
                    //手入力と分ける必要があるため
                    foreach (var lineIdsflag in idsFlag)
                    {
                        if (lineIdsflag.Value.ToString() == "0")
                        {
                            item.Add(new FireMasterInfoAuto().GetSelectedOneItems(lineIdsflag.Key));
                        }
                        else
                        {
                            item.Add(new FireMasterInfo().GetSelectedOneItems(lineIdsflag.Key));
                        }
                    }

                    List<V_FIRE> tempItem = new List<V_FIRE>();
                    tempItem = item.OrderBy(x => x.P_FIRE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                    string fireValue = "";
                    foreach (V_FIRE temp in tempItem)
                    {
                        fireValue = fireValue + temp.FIRE_ID + ",";
                    }
                    fireValue = fireValue.TrimEnd(',');
                    csmscommoninfo.hiddenfire = fireValue;
                    return item.OrderBy(x => x.P_FIRE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();
                }
            }
            return new List<V_FIRE>().AsQueryable();
        }


        /// <summary>
        /// 社内ルールのSelectMethodで選択済みの社内ルールを取得します。
        /// </summary>
        /// <param name="ghsCategoryIds">セッションに格納されている選択済みの社内ルールのIDを抽出します。</param>
        /// <returns>選択済みの社内ルールを取得します。</returns>

        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :this Method loads Selected Office Details
        /// <summary>
        public IQueryable<V_OFFICE> GetSelectedOffice(string ChemCd, string mode, List<int> OfficeIds)
        {
            if (OfficeIds != null && OfficeIds.Count > 0)
            {
                if (Session[sessionOfficeIds] != null)
                {
                    if (officeFlag == true)
                    {
                        List<V_OFFICE> item = new List<V_OFFICE>();
                        List<string> reg = new List<string>();

                        //セッションのを入れる                  
                        List<string> selfInputReg = new List<string>();

                        string[] regArray = null;
                        if (csmscommoninfo.hiddenoffice != null)
                        {
                            string exitingOfficeIds = csmscommoninfo.hiddenoffice;
                            regArray = exitingOfficeIds.Split(',');
                        }

                        //法規制反映ボタン押下した際                       
                        if (Session["OfficeRulesmodel"] != null)
                        {
                            List<V_OFFICE> vOffice = new List<V_OFFICE>();
                            vOffice = (List<V_OFFICE>)Session["OfficeRulesmodel"];
                            foreach (var row in vOffice)
                            {
                                if (row.INPUT_FLAG == "手入力")
                                {
                                    selfInputReg.Add(row.OFFICE_ID.ToString());
                                }
                            }
                        }


                        //手入力のものはGridに表示させるため、戻り値に入れる
                        foreach (var temp in selfInputReg)
                        {

                            if (!OfficeIds.Contains<int>(Convert.ToInt16(temp)))
                            {
                                item.Add(new OfficeMasterInfo().GetSelectedOneItems(Convert.ToInt16(temp)));
                                reg.Add(temp);
                            }

                        }
                        //法規制で引きあたったもの
                        foreach (var temp2 in OfficeIds)
                        {
                            item.Add(new OfficeMasterInfoAuto().GetSelectedOneItems(Convert.ToInt16(temp2)));
                            reg.Add(temp2.ToString());

                        }

                        officeFlag = false;
                        List<V_OFFICE> tempItem = new List<V_OFFICE>();
                        tempItem = item.OrderBy(x => x.P_OFFICE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string officevalue = "";
                        foreach (V_OFFICE temp in tempItem)
                        {
                            officevalue = officevalue + temp.OFFICE_ID + ",";
                        }
                        officevalue = officevalue.TrimEnd(',');
                        csmscommoninfo.hiddenoffice = officevalue;
                        return item.OrderBy(x => x.P_OFFICE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();

                    }
                    else if (officedel == true)
                    {
                        List<string> reg = new List<string>();
                        List<V_OFFICE> item = new List<V_OFFICE>();
                        //引数でやってきたREG_TYPE_IDを1つずつ処理する
                        foreach (var temp in OfficeIds)
                        {
                            reg.Add(temp.ToString());
                            //消防法Gridの値
                            foreach (var temp2 in delthengridoffice)
                            {
                                //消防法GridのREG_TYPE_IDと引数のREG_TYPE_IDをチェック
                                if (temp2.OFFICE_ID == temp)
                                {
                                    //Gridの内容が手入力だった場合
                                    if (temp2.INPUT_FLAG == "1")
                                    {
                                        item.Add(new OfficeMasterInfo().GetSelectedOneItems(temp));
                                    }
                                    else
                                    {
                                        item.Add(new OfficeMasterInfoAuto().GetSelectedOneItems(temp));
                                    }
                                }
                            }

                        }
                        officedel = false;
                        List<V_OFFICE> tempItem = new List<V_OFFICE>();
                        tempItem = item.OrderBy(x => x.P_OFFICE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string officevalue = "";
                        foreach (V_OFFICE temp in tempItem)
                        {
                            officevalue = officevalue + temp.OFFICE_ID + ",";
                        }
                        officevalue = officevalue.TrimEnd(',');
                        csmscommoninfo.hiddenoffice = officevalue;
                        return item.OrderBy(x => x.P_OFFICE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();
                    }
                    else
                    {

                        List<string> inputReg = new List<string>();
                        List<V_OFFICE> item = new List<V_OFFICE>();
                        List<string> reg = new List<string>();

                        //既存のGridの内容を取得する
                        //法規制引当で引き当てた法規制が手入力で上書きされないようにする
                        if (Session["OfficeRulesmodel"] != null)
                        {
                            List<V_OFFICE> vOffice = new List<V_OFFICE>();
                            vOffice = (List<V_OFFICE>)Session["OfficeRulesmodel"];
                            foreach (var row in vOffice)
                            {
                                if (row.INPUT_FLAG == "" || row.INPUT_FLAG == null && row.OFFICE_ID != 0)
                                {
                                    inputReg.Add(row.OFFICE_ID.ToString());
                                }
                            }
                        }



                        //選択画面で選択したものとinputRegを比較し、同じものがあれば、上書きされないようにする
                        foreach (string temp in inputReg)
                        {
                            foreach (var ids in OfficeIds)
                            {
                                if (temp == ids.ToString())
                                {
                                    item.Add(new OfficeMasterInfoAuto().GetSelectedOneItems(Convert.ToInt16(temp)));
                                    reg.Add(temp);
                                }
                            }

                        }

                        //選択画面で追加したものを処理する
                        foreach (var temp2 in OfficeIds)
                        {
                            if (!inputReg.Contains(temp2.ToString()))
                            {
                                item.Add(new OfficeMasterInfo().GetSelectedOneItems(Convert.ToInt16(temp2)));
                                reg.Add(temp2.ToString());
                            }
                        }

                        List<V_OFFICE> tempItem = new List<V_OFFICE>();
                        tempItem = item.OrderBy(x => x.P_OFFICE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                        string officeValue = "";
                        foreach (V_OFFICE temp in tempItem)
                        {
                            officeValue = officeValue + temp.OFFICE_ID + ",";
                        }
                        officeValue = officeValue.TrimEnd(',');
                        csmscommoninfo.hiddenoffice = officeValue;
                        return item.OrderBy(x => x.P_OFFICE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();
                    }
                }
            }
            else if (!string.IsNullOrEmpty(ChemCd))
            {
                using (var OfficeInfo = new OfficeMasterInfo())
                {
                    List<V_OFFICE> item = new List<V_OFFICE>();
                    var ids = OfficeInfo.GetIdsBelongInChem(ChemCd);
                    var idsFlag = OfficeInfo.GetIdsInputFlg(ChemCd);
                    Session.Add(sessionOfficeIds, ids);
                    foreach (var lineidsflag in idsFlag)
                    {
                        if (lineidsflag.Value.ToString() == "0")
                        {
                            item.Add(new OfficeMasterInfoAuto().GetSelectedOneItems(lineidsflag.Key));
                        }
                        else
                        {
                            item.Add(new OfficeMasterInfo().GetSelectedOneItems(lineidsflag.Key));
                        }
                    }

                    List<V_OFFICE> tempItem = new List<V_OFFICE>();
                    tempItem = item.OrderBy(x => x.P_OFFICE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable().ToList();
                    string officeValue = "";
                    foreach (V_OFFICE temp in tempItem)
                    {
                        officeValue = officeValue + temp.OFFICE_ID + ",";
                    }
                    officeValue = officeValue.TrimEnd(',');
                    csmscommoninfo.hiddenoffice = officeValue;
                    return item.OrderBy(x => x.P_OFFICE_ID).ThenBy(x => x.SORT_ORDER).AsQueryable();
                }
            }
            return new List<V_OFFICE>().AsQueryable();
        }


        /// <summary>
        /// 商品のSelectMethodで登録済みの商品を取得します。
        /// </summary>
        /// <returns>登録済みの商品を取得します。</returns>

        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :this Method loads product Details
        /// <summary>
        public IQueryable<ChemProductModel> GetProducts(string ChemCd, string Mode)
        {

            if (!string.IsNullOrEmpty(ChemCd))
            {
                var condition = new ChemSearchModel();
                condition.CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching;
                condition.CHEM_CD = ChemCd;
                condition.CHEM_SEARCH_TARGET = (int)ChemSearchTarget.Product;
                condition.IsOnlyProduct = true;
                condition.USAGE_STATUS = (int)Classes.UsageStatusCondtionType.All;
                var d = new ChemDataInfo();
                var dataArray = d.GetSearchResult(condition).AsEnumerable();
                d.Dispose();
                var makerList = GetMakerList();
                var unitsizeList = GetUnitSizeList();

                var productArray = new List<ChemProductModel>();
                foreach (var data in dataArray)
                {
                    data.MAKER_ID = makerList.Select(x => x.Id).Contains(data.MAKER_ID) ? data.MAKER_ID : null;
                    data.UNITSIZE_ID = unitsizeList.Select(x => x.Id).Contains(data.UNITSIZE_ID) ? data.UNITSIZE_ID : null;
                    productArray.Add(new ChemProductModel(data) { Mode = mode });
                }
                //if (Mode != "3")
                //{
                Session.Add(sessionProducts, productArray.AsEnumerable());
                //}
                return productArray.AsQueryable();
            }
            var newDataArray = new List<ChemProductModel>();
            if (Mode != "3")
            {
                Session.Add(sessionProducts, newDataArray.AsEnumerable());
            }
            return newDataArray.AsQueryable();
        }

        /// <summary>
        /// 画面のモードを取得または設定します。
        /// </summary>
        protected Mode mode { get; set; }

        /// <summary>
        /// メーカーマスターからデータを取得します。
        /// </summary>
        /// <returns>メーカーマスターデータを取得します。</returns>
        public IEnumerable<CommonDataModel<int?>> GetMakerList()
        {
            switch (mode)
            {
                case Mode.New:
                case Mode.Copy:
                case Mode.Edit:
                case Mode.TemporaryNew:
                case Mode.TemporaryEdit:
                    return new MakerMasterInfo().GetSelectList();
                case Mode.View:
                case Mode.Deleted:
                case Mode.ReferenceAfterRegistration:
                case Mode.Abolition:
                case Mode.Approval:
                    return new MakerMasterInfo().GetViewList();
                default:
                    throw new ArgumentNullException();
            }
        }

        /// <summary>
        /// 容量単位マスターからデータを取得します。
        /// </summary>
        /// <returns>容量単位マスターデータを取得します。</returns>
        public IEnumerable<CommonDataModel<int?>> GetUnitSizeList()
        {
            switch (mode)
            {
                case Mode.New:
                case Mode.Copy:
                case Mode.Edit:
                case Mode.TemporaryNew:
                case Mode.TemporaryEdit:

                    return new UnitSizeMasterInfo().GetSelectList();
                case Mode.View:
                case Mode.Deleted:
                case Mode.ReferenceAfterRegistration:
                case Mode.Abolition:

                    return new UnitSizeMasterInfo().GetViewList();
                case Mode.Approval:
                    return new UnitSizeMasterInfo().GetApprovalList();
                default:
                    throw new ArgumentNullException();
            }
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 06-9-2019  
        /// Description :Loads the Contained Grid Details
        /// <summary>
        public ActionResult ZC11021(int? mode, string BusyoCheck, int? data)
        {
            if (data == null)
            {
                Session["SearchResultRows"] = null;
            }

            string username = "";
            CsmsCommonInfo cssmsCommonModel = new CsmsCommonInfo();
            if (Session["LoginUserSession"] != null)
            {
                WebCommonInfo webCommon = new WebCommonInfo();
                ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];
                username = appUser.UserName;
                TempData["username"] = username;
            }
            else
            {
                return RedirectToAction("ZC01001", "ZC010");
            }
            cssmsCommonModel = dropdownlist.ddlChemList();
            if (BusyoCheck != null)
            {
                cssmsCommonModel.hiddenbusyCheckid = BusyoCheck;
            }
            if (Session["SearchResultRows"] != null)
            {
                cssmsCommonModel.ChemCommonList = Session["SearchResultRows"] as List<CsmsCommonInfo>;
            }
            else
            {
                cssmsCommonModel.ChemCommonList = new List<CsmsCommonInfo>();
            }

            Session[SessionConst.CategoryTableName] = nameof(M_CHEM);
            cssmsCommonModel.hdnmode = mode;
            Session[SessionConst.UnRegisteredChemCD] = null;
            return View(cssmsCommonModel);
        }


        [HttpPost]
        public ActionResult ZC11021(ChemSearchModel objcssmsCommonModel)
        {
            CsmsCommonInfo cssmsCommonModel = new CsmsCommonInfo();
            cssmsCommonModel = dropdownlist.ddlChemList();
            cssmsCommonModel.ChemCommonList = Session["SearchResultRows"] as List<CsmsCommonInfo>;
            cssmsCommonModel.hiddenbusyCheckid = objcssmsCommonModel.hiddenbusyCheckid;
            if (objcssmsCommonModel.hdnmode != null)
            {
                cssmsCommonModel.hdnmode = objcssmsCommonModel.hdnmode;
                Session["hdnmode"] = mode;
            }
            else
            {
                if (Session["hdnmode"] != null)
                {
                    cssmsCommonModel.hdnmode = Convert.ToInt32(Session["hdnmode"]);
                }
            }
            return View(cssmsCommonModel);
        }


        public ActionResult ResetValues(ChemSearchModel objcssmsCommonModel)
        {
            CsmsCommonInfo cssmsCommonModel = new CsmsCommonInfo();
            Session["Contained"] = null;
            Session[sessionContaineds] = null;
            Session["SearchResultRows"] = null;

            int? mode = objcssmsCommonModel.hdnmode;
            string BusyoCheck = objcssmsCommonModel.hiddenbusyCheckid;
            return RedirectToAction("ZC11021", "ZC110", new { mode, BusyoCheck });
        }

        public List<CsmsCommonInfo> GetSearchResult(CsmsCommonInfo cssmsCommonModel)
        {

            ChemSearchModel chemSearchModel = new ChemSearchModel();
            using (var chemDataInfo = new ChemDataInfo())
            {
                cssmsCommonModel.CHEM_NAME = cssmsCommonModel.CHEM_NAME.UnityWidth();
                cssmsCommonModel.ChemCommonList = chemDataInfo.GetSearchResult(chemSearchModel).ToList();
            }

            return cssmsCommonModel.ChemCommonList;
        }

        public string AddChemCodeList(string ChemCd)
        {
            var i = 0;
            string Result = "";
            if (ChemCd != null)
            {
                var returnValue = new Dictionary<string, string>();

                foreach (var code in ChemCd.Split(','))
                {
                    returnValue.Add(nameof(M_CHEM.CHEM_CD) + i.ToString(), code.ToString());
                    i++;
                }
                Session[SessionConst.CategorySelectReturnValue] = returnValue;
                Result = "Yes";
            }
            else
            {
                Result = "No";
            }
            return Result;
        }

        public CsmsCommonInfo GetLookupValues(CsmsCommonInfo dropdownlist)
        {
            List<SelectListItem> listManufacturerName = new List<SelectListItem> { };
            List<SelectListItem> ListofUnitSize = new List<SelectListItem> { };
            new MakerMasterInfo().GetSelectList().ToList().
                    ForEach(x => listManufacturerName.Add(new SelectListItem
                    { Value = x.Id.ToString(), Text = x.Name }));


            new UnitSizeMasterInfo().GetSelectList().ToList().
                    ForEach(x => ListofUnitSize.Add(new SelectListItem
                    { Value = x.Id.ToString(), Text = x.Name }));

            dropdownlist.LookupUnitSizeMasterInfo = ListofUnitSize;
            dropdownlist.LookupManufacturerName = listManufacturerName;

            return dropdownlist;
        }

        [HttpPost]
        public string AddContainedIds(string ContainedIds, string busycheckId)
        {
            string MergedIds = null;
            int intcount = 0;
            int LedgerWorkDataInfovalue = 0;
            int LedgerDataInfovalue = 0;
            Session[SessionConst.UnRegisteredChemCD] = null;
            try
            {
                if (busycheckId != null)
                {
                    using (var groupMaster = new GroupMasterInfo())
                    {
                        var grp = groupMaster.GetParentGroups(busycheckId, true, 1);
                        //var id = ContainedIds;

                        List<string> listOfIds = new List<string>(ContainedIds.Split(','));

                        foreach (var code in listOfIds)
                        {
                            var grpSel = string.Empty;
                            foreach (var g in grp)
                            {
                                if (string.IsNullOrEmpty(grpSel))
                                {
                                    grpSel += g;
                                }
                                else
                                {
                                    grpSel += ("," + g);
                                }
                            }
                            var cond = new LedgerSearchModel() { CHEM_CD = ContainedIds, GROUP_CD_SELECTION = grpSel };
                            using (var dataInfo = new LedgerWorkDataInfo())
                            {
                                List<int> count = dataInfo.lstGetSearchResultCount(cond);

                                foreach (int entry in count)
                                {
                                    LedgerWorkDataInfovalue = entry;
                                }

                                if (LedgerWorkDataInfovalue > 0)
                                {
                                    TempData["msg"] = "<script>alert('申請中・登録済みの化学物質は選択できません。');</script>";
                                }
                            }
                            using (var dataInfo = new LedgerDataInfo())
                            {
                                List<int> count = dataInfo.lstGetSearchResultCount(cond);

                                foreach (int entry in count)
                                {
                                    LedgerDataInfovalue = entry;
                                }

                                if (LedgerDataInfovalue > 0)
                                {
                                    TempData["msg"] = "<script>alert('申請中・登録済みの化学物質は選択できません。');</script>";

                                }
                            }

                        }
                    }

                }
                if (LedgerWorkDataInfovalue > 0)
                {
                    intcount = LedgerWorkDataInfovalue;
                }
                else if (LedgerDataInfovalue > 0)
                {
                    intcount = LedgerDataInfovalue;
                }
                if (Session[sessionContaineds] != null)
                {
                    string SessionIds = (string)Session[sessionContaineds];
                    if (!string.IsNullOrEmpty(ContainedIds))
                    {
                        MergedIds = string.Join(",", ContainedIds, SessionIds);
                        Session[sessionContaineds] = MergedIds;
                    }
                }
                else
                {
                    Session[sessionContaineds] = ContainedIds;
                }

                Session[SessionConst.UnRegisteredChemCD] = ContainedIds;
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return intcount.ToString();

        }

        //[HttpPost]
        //public string Unregisterd(string ContainedIds)
        //{           


        //    return ContainedIds;


        //}



        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019  
        /// Description :This Method is to update the max and minContained Grid Data
        /// <summary>
        /// 


        public string UpdateContainedList(string MaxminContainedRate, string ChemCd, string FlowCd)
        {
            try
            {
                if (FlowCd == "2")
                {
                    if (Session["Contained"] != null)
                    {
                        CsmsCommonInfo csmscommoninfo = new CsmsCommonInfo();
                        List<ChemContainedModel> chemContainedModel = (List<ChemContainedModel>)Session["Contained"];
                        for (int i = 0; i < chemContainedModel.Count; i++)
                        {
                            if (chemContainedModel[i].UniqueRowId == Convert.ToInt32(ChemCd))
                            {
                                if (MaxminContainedRate != "")
                                {
                                    chemContainedModel[i].MAX_CONTAINED_RATE = Convert.ToDecimal(MaxminContainedRate);
                                    chemContainedModel[i].MaxContainedRate = MaxminContainedRate;
                                }
                                else
                                {
                                    chemContainedModel[i].MAX_CONTAINED_RATE = null;
                                    chemContainedModel[i].MaxContainedRate = null;
                                }
                            }
                        }
                        Session.Add("Contained", chemContainedModel);
                    }
                }
                else if (FlowCd == "1")
                {
                    if (Session["Contained"] != null)
                    {
                        CsmsCommonInfo csmscommoninfo = new CsmsCommonInfo();
                        List<ChemContainedModel> chemContainedModel = (List<ChemContainedModel>)Session["Contained"];
                        for (int i = 0; i < chemContainedModel.Count; i++)
                        {
                            if (chemContainedModel[i].UniqueRowId == Convert.ToInt32(ChemCd))
                            {
                                if (MaxminContainedRate != "")
                                {
                                    chemContainedModel[i].MIN_CONTAINED_RATE = Convert.ToDecimal(MaxminContainedRate);
                                    chemContainedModel[i].MinContainedRate = MaxminContainedRate;
                                }
                                else
                                {
                                    chemContainedModel[i].MIN_CONTAINED_RATE = null;
                                    chemContainedModel[i].MinContainedRate = null;
                                }
                            }
                        }
                        Session.Add("Contained", chemContainedModel);
                    }
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                ex.StackTrace.ToString();
            }
            catch (NullReferenceException ex)
            {
                ex.StackTrace.ToString();
            }
            catch (ArgumentNullException ex)
            {
                ex.StackTrace.ToString();
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return null;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019  
        /// Description :This Method is to get the all Contained Grid
        /// <summary>
        public string GetContainedDetails()
        {
            List<ChemContainedModel> chemContainedmodellist = new List<ChemContainedModel>();
            CsmsCommonInfo csmscommoninfo = new CsmsCommonInfo();
            string Details = "";

            try
            {
                if (Session[sessionContaineds] != null)
                {
                    int i = 1;
                    string chemContaineds = (string)Session[sessionContaineds];
                    string[] SplitContainedId = chemContaineds.Split(',');

                    foreach (var items in SplitContainedId)
                    {
                        ChemContainedModel chemcontainedmodel = new ChemContainedModel();
                        List<ChemDataInfo> chemContainedmodelnew = new List<ChemDataInfo>();
                        string MAXCONTAINEDRATE = null;
                        string MINCONTAINEDRATE = null;
                        CsmsCommonInfo csmsCommon = new CsmsCommonInfo();
                        var condition = new ChemSearchModel();
                        condition.CHEM_CD = items;
                        var d = new ChemDataInfo();
                        var dataArray = d.GetSearchResult(condition);
                        if (dataArray != null && dataArray.Count() > 0)
                        {
                            csmsCommon.ChemCommonList = dataArray.ToList();
                            chemcontainedmodel.CAS_NO = csmsCommon.ChemCommonList[0].CAS_NO;
                            chemcontainedmodel.CHEM_CD = null;
                            chemcontainedmodel.C_CHEM_CD = csmsCommon.ChemCommonList[0].CHEM_CD;
                            chemcontainedmodel.CHEM_NAME = csmsCommon.ChemCommonList[0].CHEM_NAME;
                            chemcontainedmodel.DENSITY = csmsCommon.ChemCommonList[0].DENSITY;
                            if (Session["Contained"] != null)
                            {
                                List<ChemContainedModel> chemContainedmodel = (List<ChemContainedModel>)Session["Contained"];
                                for (int k = 0; k < chemContainedmodel.Count; k++)
                                {
                                    if (items.Trim() == chemContainedmodel[k].C_CHEM_CD)
                                    {
                                        if (csmsCommon.ChemCommonList[0].MAX_CONTAINED_RATE == null)
                                        {
                                            chemcontainedmodel.MAX_CONTAINED_RATE = chemContainedmodel[k].MAX_CONTAINED_RATE;
                                            chemcontainedmodel.MaxContainedRate = chemContainedmodel[k].MAX_CONTAINED_RATE.ToString();
                                            MAXCONTAINEDRATE = chemcontainedmodel.MAX_CONTAINED_RATE.ToString();
                                        }
                                        else
                                        {
                                            MAXCONTAINEDRATE = chemcontainedmodel.MAX_CONTAINED_RATE.ToString();
                                        }
                                        if (csmsCommon.ChemCommonList[0].MIN_CONTAINED_RATE == null)
                                        {
                                            chemcontainedmodel.MIN_CONTAINED_RATE = chemContainedmodel[k].MIN_CONTAINED_RATE;
                                            chemcontainedmodel.MinContainedRate = chemContainedmodel[k].MIN_CONTAINED_RATE.ToString();
                                            MINCONTAINEDRATE = chemcontainedmodel.MIN_CONTAINED_RATE.ToString();
                                        }
                                        else
                                        {
                                            MINCONTAINEDRATE = chemcontainedmodel.MIN_CONTAINED_RATE.ToString();
                                        }
                                    }
                                }
                            }
                            chemcontainedmodel.UniqueRowId = i;
                            if (csmsCommon.ChemCommonList.Count > 0)
                            {
                                Details += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + csmsCommon.ChemCommonList[0].CHEM_CD + "></td><td style='width:280px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'>" + csmsCommon.ChemCommonList[0].CHEM_NAME + "</font></font></td><td style='width:110px;'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'></font>" + csmsCommon.ChemCommonList[0].CAS_NO + "</font></td><td style='width:40px'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'></font>" + csmsCommon.ChemCommonList[0].DENSITY + "</font></td><td><input type='text' style='width:50px;' class='halfWidth minContainedRate' value='" + MINCONTAINEDRATE + "' id='" + csmsCommon.ChemCommonList[0].CHEM_CD + "' name='minContainedRate' onchange='return MinChemContainedRate(this.value," + i + ")'></td><td><input type='text' style='width:50px;' name='maxContainedRate' value='" + MAXCONTAINEDRATE + "'  id='" + csmsCommon.ChemCommonList[0].CHEM_CD + "' class='halfWidth MaxContainedRate' onchange='return MaxChemContainedRate(this.value," + i + ")'></td></tr>";
                            }
                            chemContainedmodellist.Add(chemcontainedmodel);
                            i++;
                        }
                    }

                    csmscommoninfo.ContainedModel = chemContainedmodellist;
                    Session["Contained"] = csmscommoninfo.ContainedModel;
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                ex.StackTrace.ToString();
            }
            catch (NullReferenceException ex)
            {
                ex.StackTrace.ToString();
            }
            catch (ArgumentNullException ex)
            {
                ex.StackTrace.ToString();
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return Details;
        }



        [HttpPost]
        public string AddProductValues()
        {
            List<ChemProductModel> chemAliases = new List<ChemProductModel>();
            ChemProductModel data = new ChemProductModel();
            CsmsCommonInfo cssmsmodel = new CsmsCommonInfo();
            string Product = null;
            int productcounter = 0;
            try
            {
                cssmsmodel = GetLookupValues(cssmsmodel);
                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> chemmodel = (List<ChemProductModel>)Session["ProductDetails"];
                    productcounter = chemmodel.Count() + 1;
                    data.TempPRODUCT_SEQ = productcounter;
                    data.PRODUCT_NAME = null;
                    chemmodel.Add(data);
                    Session["ProductDetails"] = chemmodel;
                    Product += "<tr><td align='Center'><input type='checkbox' id=" + productcounter + " name='Check" + productcounter + "' align='right' class=productChekbox></td><td align='Center'><select id='ManufactureName" + productcounter + "' name='ManufactureName" + productcounter + "' class='required ManufactureName' style='width: 120px;' onchange='ProdManufactureName(this.value," + productcounter + ")'></select></td><td align='Center'><input type='text' id='ProductName' name='ProductName" + productcounter + "' class='required ProductName' style='width: 250px;' onchange='ProdutName(this.value," + productcounter + ")' onkeypress='return validateSpecialCharecters()'></td><td style='white-space: nowrap;'><input type='text' id='Capacity' name='Capacity" + productcounter + "' class='required capacity' style='width: 80px;' onchange='ProductCapacity(this.value," + productcounter + ")'>&nbsp;<select id='SiglebyteCapacity' name='SiglebyteCapacity" + productcounter + "' class='required SiglebyteCapacity' Style='width: 100px;'  onchange='Density(this.value," + productcounter + ")'></select></td><td><input type='text' id='Productbarcode' class='halfWidth Barcode' style='width: 120px;' name='Productbarcode" + productcounter + "' onchange='ProductBarcode(this.value," + productcounter + ")' onkeypress='return validateSpecialCharecters()' maxlength='30'></td><td><input type='text' id='sdslinkurl' class='halfWidth' style='width: 120px;' name='sdslinkurl" + productcounter + "' onchange='SDSURL(this.value," + productcounter + ")'></td><td><input type='text' class='halfWidth datepickerFrom' style='width: 130px;' id='datepickerFrom" + productcounter + "' name='sdsupdatedate" + productcounter + "' onchange='UPDdate(this.value," + productcounter + ")'></td>";
                }
                else
                {
                    productcounter = 1;
                    data.TempPRODUCT_SEQ = productcounter;
                    data.PRODUCT_NAME = null;
                    chemAliases.Add(data);
                    Session["ProductDetails"] = chemAliases;
                    Product += "<tr><td align='Center'><input type='checkbox' id=" + productcounter + " name='Check" + productcounter + "' align='right' class=productChekbox></td><td align='Center'><select id='ManufactureName" + productcounter + "' name='ManufactureName" + productcounter + "' class='required ManufactureName' style='width: 120px;' onchange='ProdManufactureName(this.value," + productcounter + ")'></select></td><td align='Center'><input type='text' id='ProductName' name='ProductName" + productcounter + "' class='required ProductName' style='width: 250px;' onchange='ProdutName(this.value," + productcounter + ")' onkeypress='return validateSpecialCharecters()'></td><td style='white-space: nowrap;'><input type='text' id='Capacity' name='Capacity" + productcounter + "' class='required capacity' style='width: 80px;' onchange='ProductCapacity(this.value," + productcounter + ")'>&nbsp;<select id='SiglebyteCapacity' name='SiglebyteCapacity" + productcounter + "' class='required SiglebyteCapacity' Style='width: 100px;'  onchange='Density(this.value," + productcounter + ")'></select></td><td><input type='text' id='Productbarcode' class='halfWidth Barcode' style='width: 120px;' name='Productbarcode" + productcounter + "' onchange='ProductBarcode(this.value," + productcounter + ")' onkeypress='return validateSpecialCharecters()' maxlength='30'></td><td><input type='text' id='sdslinkurl' class='halfWidth' style='width: 120px;' name='sdslinkurl" + productcounter + "' onchange='SDSURL(this.value," + productcounter + ")'></td><td><input type='text' class='halfWidth datepickerFrom' style='width: 130px;' id='datepickerFrom" + productcounter + "' name='sdsupdatedate" + productcounter + "' onchange='UPDdate(this.value," + productcounter + ")'></td>";
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return Product;
        }



        public string LoadChemContainedIDs()
        {
            var containedDetails = "";
            csmscommoninfo = new CsmsCommonInfo();
            if (Session["Contained"] != null)
            {
                List<ChemContainedModel> Chemcantained = (List<ChemContainedModel>)Session["Contained"];
                foreach (var chemContainedmodelnew in Chemcantained)
                {
                    containedDetails += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + chemContainedmodelnew.CHEM_CD + "></td><td style='width:280px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'>" + chemContainedmodelnew.CHEM_NAME + "</font></font></td><td style='width:110px;'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'></font>" + chemContainedmodelnew.CAS_NO + "</font></td><td style='width:40px'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'></font>" + chemContainedmodelnew.DENSITY + "</font></td><td><input type=number style='width:50px;' class='halfWidth numberColumn MaxContainedRate' id=" + chemContainedmodelnew.CHEM_CD + " name='MaxContainedRate'></td><td><input type=number style='width:50px;' id=" + chemContainedmodelnew.CHEM_CD + " class='halfWidth numberColumn MinContainedRate'></td></tr>";
                }
            }
            return containedDetails;
        }

        public string LoadFireIdsDetails()
        {
            csmscommoninfo = new CsmsCommonInfo();
            var Fire = "";
            try
            {
                if (Session["FireServicesModel"] != null)
                {

                    csmscommoninfo.FireModel = (List<V_FIRE>)Session["FireServicesModel"];
                    if (csmscommoninfo.FireModel != null)
                    {
                        foreach (var item in csmscommoninfo.FireModel)
                        {

                            Fire += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + item.FIRE_ID + "></td><td align='Center'><img src='" + item.IconUrl + "' style='height:32px;'/></td><td style='width:364px;'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'>" + item.FIRE_NAME + "</font></font></td><td style='width: 50px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'></font>" + item.INPUT_FLAG + "</font></td></tr>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                Fire = "";
            }
            return Fire;
        }

        public string LoadGhsDetails()
        {
            csmscommoninfo = new CsmsCommonInfo();
            var ghs = "";
            try
            {
                if (Session["GHSModel"] != null)
                {
                    csmscommoninfo.GHSModel = (List<V_GHSCAT>)Session["GHSModel"];
                    if (csmscommoninfo.GHSModel != null)
                    {
                        foreach (var item in csmscommoninfo.GHSModel)
                        {
                            ghs += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + item.GHSCAT_ID + "></td><td align='Center'><img src='" + item.IconUrl + "' style='height:32px;'/></td><td style='width:364px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'>" + item.GHSCAT_NAME + "</font></font></td><td style='width:50px;'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'></font>" + item.INPUT_FLAG + "</font></td></tr>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                ghs = "";
            }
            return ghs;
        }

        public string LoadofficeIdsDetails()
        {
            csmscommoninfo = new CsmsCommonInfo();
            var office = "";
            try
            {
                if (Session["OfficeRulesmodel"] != null)
                {
                    csmscommoninfo.OfficeGridModel = (List<V_OFFICE>)Session["OfficeRulesmodel"];
                    if (csmscommoninfo.OfficeGridModel != null)
                    {
                        foreach (var item in csmscommoninfo.OfficeGridModel)
                        {

                            office += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + item.OFFICE_ID + "></td><td align='center' style='width: 70px;'><img src='" + item.IconUrl + "' style='height: 32px;'/></td><td style='width:364px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'>" + item.OFFICE_NAME + "</font></font></td><td style='width: 50px;'><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'></font>" + item.INPUT_FLAG + "</font></td></tr>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                office = "";
            }
            return office;
        }


        public string LoadRegulationDetails()
        {
            CsmsCommonInfo csmscommoninfo = new CsmsCommonInfo();
            var reg = "";
            try
            {
                if (Session["RegulationModel"] != null)
                {
                    csmscommoninfo.RegulationModel = (List<V_REGULATION>)Session["RegulationModel"];

                    if (csmscommoninfo.RegulationModel != null)
                    {
                        foreach (var item in csmscommoninfo.RegulationModel)
                        {
                            if (item.IconUrl == null)
                            {
                                item.REG_ICON_PATH = null;
                            }

                            reg += "<tr><td align='Center'><input type='checkbox' name='Checkbox' align='right' id=" + item.REG_TYPE_ID + "></td><td align='Center'><img src='" + item.IconUrl + "' style='height:32px;'></td><td style='width: 364px;'><font style='vertical-align: inherit;'><font style='vertical-align:inherit;'>" + item.REG_TEXT + "</font></font></td><td style='width:50px;'><font style='vertical-align: inherit;'><font style='vertical-align:inherit;'></font>" + item.INPUT_FLAG + "</font></td></tr>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                reg = "";
            }
            return reg;
        }

        public string LoadProductValues()
        {
            List<ChemProductModel> chemAliases = new List<ChemProductModel>();
            string Product = null;
            try
            {
                if (Session["ProductDetails"] != null)
                {
                    csmscommoninfo.ChemProductListModel = (List<ChemProductModel>)Session["ProductDetails"];
                    if (csmscommoninfo.ChemProductListModel != null)
                    {
                        foreach (var item in csmscommoninfo.ChemProductListModel)
                        {
                            int productcounter = item.TempPRODUCT_SEQ;
                            Product += "<tr><td align='Center'><input type='checkbox' name='Check" + productcounter + "' align='right' class=productChekbox></td><td align='Center'><select id='ManufactureName' name='ManufactureName" + productcounter + "' class='required ManufactureName' style='width: 120px;' onchange='ProdManufactureName(this.value," + productcounter + ")'></select></td><td align='Center'><input type='text' id='ProductName' value=" + item.PRODUCT_NAME + " name='ProductName" + productcounter + "' class='required ProductName' style='width: 250px;' onchange='ProdutName(this.value," + productcounter + ")'></td><td style='white-space: nowrap;'><input type='text' id='Capacity' value=" + item.UNITSIZE + " name='Capacity" + productcounter + "' class='required capacity' style='width: 100px;' onchange='ProductCapacity(this.value," + productcounter + ")'>&nbsp;<select id='SiglebyteCapacity' name='SiglebyteCapacity" + productcounter + "' class='required SiglebyteCapacity' Style='width: 105px;'  onchange='Density(this.value," + productcounter + ")'></select></td><td><input type='text' id='Productbarcode' value=" + item.PRODUCT_BARCODE + " name='Productbarcode" + productcounter + "' onchange='ProductBarcode(this.value," + productcounter + ")'></td><td><input type='text' id='sdslinkurl' value=" + item.SDS_URL + " name='sdslinkurl" + productcounter + "' onchange='SDSURL(this.value," + productcounter + ")'></td><td><input type='text' id='sdsupdatedate' value=" + item.SDS_UPD_DATE + " name='sdsupdatedate" + productcounter + "' class='sdsupdatedate' onchange='UPDdate(this.value," + productcounter + ")'></td>";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return Product;
        }


        public string ValidateRegistrationDetails(string casno, string chemName)
        {
            CsmsCommonInfo cssmsmodel = new CsmsCommonInfo();
            List<string> errorMessages = new List<string>();
            string ErrorStatus = "";
            try
            {

                var sql = new StringBuilder();
                var parameters = new DynamicParameters();

                sql.AppendLine("select CHEM_NAME,DEL_FLAG from M_CHEM where CHEM_NAME= @CHEM_NAME and not(DEL_FLAG = @DEL_FLAG)");
                parameters.Add("@CHEM_NAME", chemName.Trim());
                parameters.Add("@DEL_FLAG", (int)DEL_FLAG.Temporary);
                List<ChemModel> records = DbUtil.Select<ChemModel>(sql.ToString(), parameters);
                if (records.Count > 0)
                {
                    foreach (var e in records)
                    {
                        if (Convert.ToInt64(e.DEL_FLAG) == (int)DEL_FLAG.Deleted)
                        {
                            ErrorStatus += string.Format(WarningMessages.NotUnique_RemoveData.Replace("\\n", "\n"), "化学物質名", chemName) + "\n";
                        }
                        else
                        {
                            ErrorStatus += string.Format(WarningMessages.NotUnique.Replace("\\n", "\n"), "化学物質名", chemName) + "\n";
                        }
                    }
                    ErrorStatus = string.Join(SystemConst.ScriptLinefeed, ErrorStatus);
                    return ErrorStatus;
                }

                if (Session["sessionAliases"] != null)
                {
                    string ErrorBuilder = "";
                    List<ChemAliasModel> chemaliasmodel = (List<ChemAliasModel>)Session["sessionAliases"];
                    List<ChemAliasModel> checmrecords = new List<ChemAliasModel>();
                    foreach (var itemsalias in chemaliasmodel)
                    {
                        StringBuilder sqlaliasno = new StringBuilder();
                        DynamicParameters parametersalias = new DynamicParameters();

                        sqlaliasno.AppendLine("select");
                        sqlaliasno.AppendLine(" A.CHEM_NAME");
                        sqlaliasno.AppendLine("from");
                        sqlaliasno.AppendLine(" (select CHEM_CD,CHEM_NAME ");
                        sqlaliasno.AppendLine("from");
                        sqlaliasno.AppendLine(" " + "M_CHEM_ALIAS");
                        sqlaliasno.AppendLine("where");
                        sqlaliasno.AppendLine(" not CHEM_NAME_SEQ = 0 and ");
                        sqlaliasno.AppendLine("CHEM_NAME = '" + itemsalias.CHEM_NAME + "' COLLATE Japanese_CI_AS_KS ) A ");
                        sqlaliasno.AppendLine("LEFT JOIN M_CHEM ");
                        sqlaliasno.AppendLine("ON A.CHEM_CD = M_CHEM.CHEM_CD ");
                        sqlaliasno.AppendLine("where not( M_CHEM.DEL_FLAG=" + (int)DEL_FLAG.Temporary + ") ");
                        checmrecords = DbUtil.Select<ChemAliasModel>(sqlaliasno.ToString(), parametersalias);
                        if (checmrecords.Count > 0) //return true;
                        {
                            ErrorBuilder += string.Format(WarningMessages.NotUnique, "化学物質別名", itemsalias.CHEM_NAME) + "\n";
                            ErrorStatus = ErrorBuilder;
                        }
                    }
                    if (checmrecords.Count > 0)
                    {
                        return ErrorStatus;
                    }
                }

                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> ChemProductbarcode = (List<ChemProductModel>)Session["ProductDetails"];
                    List<ChemProductModel> barcoderecords = new List<ChemProductModel>();
                    int rowNumberString = 1;
                    foreach (var Productbarcode in ChemProductbarcode)
                    {
                        if (Productbarcode.PRODUCT_BARCODE != "" && Productbarcode.PRODUCT_BARCODE != null)
                        {
                            var sqlBarcode = new StringBuilder();
                            DynamicParameters parametersbarcode = new DynamicParameters();

                            string BarcodeErrorBuilder = "";
                            sqlBarcode.AppendLine("select");
                            sqlBarcode.AppendLine("PRODUCT_BARCODE");
                            sqlBarcode.AppendLine("from");
                            sqlBarcode.AppendLine("M_CHEM_PRODUCT");
                            sqlBarcode.AppendLine("where");
                            sqlBarcode.AppendLine("not(CHEM_CD=@CHEM_CD and PRODUCT_SEQ=@PRODUCT_SEQ) and PRODUCT_BARCODE=@PRODUCT_BARCODE");
                            parametersbarcode.Add("@CHEM_CD", Productbarcode.CHEM_CD);
                            parametersbarcode.Add("@PRODUCT_SEQ", Productbarcode.PRODUCT_SEQ);
                            parametersbarcode.Add("@PRODUCT_BARCODE", Productbarcode.PRODUCT_BARCODE);
                            barcoderecords = DbUtil.Select<ChemProductModel>(sqlBarcode.ToString(), parametersbarcode);

                            if (barcoderecords.Count > 0)
                            {
                                BarcodeErrorBuilder = rowNumberString + "行目:商品バーコードの" + "[" + Productbarcode.PRODUCT_BARCODE + "]" + "は使用されています。" + "\n";
                                ErrorStatus = BarcodeErrorBuilder;
                                rowNumberString++;
                            }
                            else
                            {
                                rowNumberString++;
                            }
                        }
                    }
                    if (barcoderecords.Count > 0)
                    {
                        return ErrorStatus;
                    }
                }


                if (Session["Contained"] != null)
                {
                    List<ChemContainedModel> chemContainedmodel = (List<ChemContainedModel>)Session["Contained"];
                    int rowNumberString = 1;
                    string[] MinValue = null;
                    string MinValues = null;
                    string[] maxValue = null;
                    string maxValues = null;
                    foreach (var items in chemContainedmodel)
                    {
                        if (items.MinContainedRate != null)
                        {
                            if (items.MinContainedRate.Contains("."))
                            {
                                MinValue = items.MinContainedRate.Split('.');
                            }
                            else
                            {
                                MinValues = items.MinContainedRate;
                            }
                        }

                        if (items.MaxContainedRate != null)
                        {
                            if (items.MaxContainedRate.Contains("."))
                            {
                                maxValue = items.MaxContainedRate.Split('.');
                            }
                            else
                            {
                                maxValues = items.MaxContainedRate;
                            }
                        }

                        if (items.MAX_CONTAINED_RATE > Convert.ToDecimal(100))
                        {
                            ErrorStatus += rowNumberString + "行目:含有率上限(%)は0以上、100以下で入力してください。" + "\n";
                        }
                        if (items.MIN_CONTAINED_RATE > Convert.ToDecimal(100))
                        {
                            ErrorStatus += rowNumberString + "行目:含有率下限(%)は0以上、100以下で入力してください。" + "\n";
                        }
                        if (items.MIN_CONTAINED_RATE > items.MAX_CONTAINED_RATE)
                        {
                            ErrorStatus += rowNumberString + "行目: 含有率下限(%)は含有率上限(%)より小さい値を入力してください。" + "\n";
                        }
                        if (MinValue != null)
                        {
                            if (MinValue[0].Length > 3)
                            {
                                ErrorStatus += rowNumberString + "行目:含有率下限(%)の整数部は3桁以下で入力してください。" + "\n";
                            }
                        }
                        if (MinValue != null || MinValues != null)
                        {
                            if (MinValue != null)
                            {
                                if (MinValue[1].Length > 3)
                                {
                                    ErrorStatus += rowNumberString + "行目:含有率下限(%)の小数部は3桁以下で入力してください。" + "\n";
                                }
                            }
                            else if (MinValues.Length > 3)
                            {
                                ErrorStatus += rowNumberString + "行目:含有率下限(%)の小数部は3桁以下で入力してください。" + "\n";
                            }
                        }
                        if (maxValue != null)
                        {
                            if (maxValue[0].Length > 3)
                            {
                                ErrorStatus += rowNumberString + "行目:含有率上限(%)の整数部は3桁以下で入力してください。" + "\n";
                            }
                        }
                        if (maxValue != null || maxValues != null)
                        {
                            if (maxValue != null)
                            {
                                if (maxValue[1].Length > 3)
                                {
                                    ErrorStatus += rowNumberString + "行目:含有率上限(%)の小数部は3桁以下で入力してください。" + "\n";
                                }
                            }
                            else if (maxValues.Length > 3)
                            {
                                ErrorStatus += rowNumberString + "行目:含有率上限(%)の小数部は3桁以下で入力してください。" + "\n";
                            }
                        }
                        rowNumberString++;
                    }
                }

                StringBuilder sqlcasno = new StringBuilder();
                DynamicParameters parameterscasno = new DynamicParameters();
                sqlcasno.AppendLine("select");
                sqlcasno.AppendLine("CAS_NO");
                sqlcasno.AppendLine("from");
                sqlcasno.AppendLine("M_CHEM");
                sqlcasno.AppendLine("where");
                sqlcasno.AppendLine("CAS_NO = @CAS_NO");
                sqlcasno.AppendLine("and");
                sqlcasno.AppendLine("DEL_FLAG= @DEL_FLAG");
                parameterscasno.Add("@CAS_NO", casno);
                parameterscasno.Add("@DEL_FLAG", (int)DEL_FLAG.Undelete);
                List<ChemModel> recordscasno = DbUtil.Select<ChemModel>(sqlcasno.ToString(), parameterscasno);
                if (recordscasno.Count > 0)
                {
                    ErrorStatus = "Casno";
                    return ErrorStatus;
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return ErrorStatus;
        }

        public ActionResult DownloadSDSFile(string Chemcd)
        {
            if (Chemcd != "")
            {
                string Sql = "SELECT SDS,SDS_MIMETYPE,SDS_FILENAME FROM M_CHEM WHERE CHEM_CD=@CHEM_CD";
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@CHEM_CD", Chemcd);
                List<ChemModel> FileByte = DbUtil.Select<ChemModel>(Sql, parameters);
                MemoryStream m = new MemoryStream(FileByte[0].SDS);
                StreamReader file = new StreamReader(m);

                Response.AppendHeader("Content-Disposition",
                         string.Format("attachment;filename={0}", HttpUtility.UrlEncode(FileByte[0].SDS_FILENAME)));
                Response.ContentType = FileByte[0].SDS_MIMETYPE;
                Response.BinaryWrite(FileByte[0].SDS);
                Response.End();
            }
            return View();
        }

        public void ClearSessionValues()
        {
            Session[sessionAliases] = null;
            Session[OldsessionAliases] = null;
            Session[sessionContaineds] = null;
            Session[sessionGhsCategoryIds] = null;
            Session[sessionRegulationIds] = null;
            Session[sessionProducts] = null;
            Session[sessionFireIds] = null;
            Session[sessionOfficeIds] = null;
            Session["RegulationModel"] = null;
            Session["GHSModel"] = null;
            Session["OfficeRulesmodel"] = null;
            Session["FireServicesModel"] = null;
            Session["Contained"] = null;
            Session["sessionAliases"] = null;
            Session["ProductDetails"] = null;
        }

        public string Updatevalidation(string casno)
        {
            string ErrorStatus = null;
            string ErrorStatusSession = null;
            string AliasProduct = null;
            try
            {
                if (Session["sessionAliases"] != null)
                {
                    string ErrorBuilder = "";
                    List<ChemAliasModel> chemaliasmodel = (List<ChemAliasModel>)Session["sessionAliases"];
                    List<ChemAliasModel> Oldchemaliasmodel = (List<ChemAliasModel>)Session[OldsessionAliases];

                    foreach (var itemsalias in chemaliasmodel)
                    {
                        foreach (var olditemsalias in Oldchemaliasmodel)
                        {
                            if ((itemsalias.CHEM_NAME.Trim() == olditemsalias.CHEM_NAME.Trim()))
                            {
                                if (itemsalias.CHEM_NAME_SEQ != olditemsalias.CHEM_NAME_SEQ)
                                {
                                    ErrorStatus += string.Format(WarningMessages.NotUnique, "化学物質別名", itemsalias.CHEM_NAME) + "\n";
                                    ErrorStatusSession = "True";
                                }
                            }
                        }
                    }
                    if (ErrorStatusSession == "True")
                    {
                        return ErrorStatus;
                    }

                    foreach (var itemsalias in chemaliasmodel)
                    {
                        StringBuilder Oldsqlaliasno = new StringBuilder();
                        Oldsqlaliasno.AppendLine("select count(*) as CountVal from  M_CHEM_ALIAS where  not CHEM_NAME_SEQ = 0 and CHEM_NAME = '" + itemsalias.CHEM_NAME.Trim() + "' COLLATE Japanese_CI_AS_KS  and CHEM_CD='" + itemsalias.CHEM_CD + "' and CHEM_NAME_SEQ='" + itemsalias.CHEM_NAME_SEQ + "' ");
                        int CountVal = DbUtil.SelectSingle<int>(Oldsqlaliasno.ToString(), null);
                        if (CountVal > 0)
                        {

                        }
                        else
                        {
                            StringBuilder Oldsqlaliasno1 = new StringBuilder();
                            if (itemsalias.CHEM_CD != null)
                            {
                                Oldsqlaliasno1.AppendLine("select count(*) as CountVal from  M_CHEM_ALIAS where  not CHEM_NAME_SEQ = 0 and CHEM_NAME = '" + itemsalias.CHEM_NAME.Trim() + "' COLLATE Japanese_CI_AS_KS  and CHEM_CD='" + itemsalias.CHEM_CD + "' ");
                                int CountVal1 = DbUtil.SelectSingle<int>(Oldsqlaliasno1.ToString(), null);
                                if (CountVal1 > 0)
                                {
                                    ErrorBuilder += string.Format(WarningMessages.NotUnique, "化学物質別名", itemsalias.CHEM_NAME) + "\n";
                                    ErrorStatus += ErrorBuilder;
                                    AliasProduct = "True";
                                }
                            }
                            else
                            {
                                Oldsqlaliasno1 = new StringBuilder();
                                Oldsqlaliasno1.AppendLine("select count(*) as CountVal from  M_CHEM_ALIAS where  not CHEM_NAME_SEQ = 0 and CHEM_NAME = '" + itemsalias.CHEM_NAME.Trim() + "' COLLATE Japanese_CI_AS_KS ");
                                int CountVal1 = DbUtil.SelectSingle<int>(Oldsqlaliasno1.ToString(), null);
                                if (CountVal1 > 0)
                                {
                                    ErrorBuilder += string.Format(WarningMessages.NotUnique, "化学物質別名", itemsalias.CHEM_NAME) + "\n";
                                    ErrorStatus += ErrorBuilder;
                                    AliasProduct = "True";
                                }
                            }
                        }
                    }
                }

                if (Session["ProductDetails"] != null)
                {
                    List<ChemProductModel> ChemProductbarcode = (List<ChemProductModel>)Session["ProductDetails"];
                    List<ChemProductModel> barcoderecords = new List<ChemProductModel>();
                    int rowNumberString = 1;
                    foreach (var Productbarcode in ChemProductbarcode)
                    {
                        if (Productbarcode.PRODUCT_BARCODE != "" && Productbarcode.PRODUCT_BARCODE != null)
                        {
                            var sqlBarcode = new StringBuilder();
                            DynamicParameters parametersbarcode = new DynamicParameters();
                            string BarcodeErrorBuilder = "";
                            sqlBarcode.AppendLine("select");
                            sqlBarcode.AppendLine("PRODUCT_BARCODE");
                            sqlBarcode.AppendLine("from");
                            sqlBarcode.AppendLine("M_CHEM_PRODUCT");
                            sqlBarcode.AppendLine("where");
                            sqlBarcode.AppendLine("not(CHEM_CD=@CHEM_CD and PRODUCT_SEQ=@PRODUCT_SEQ) and PRODUCT_BARCODE=@PRODUCT_BARCODE");
                            parametersbarcode.Add("@CHEM_CD", Productbarcode.CHEM_CD);
                            parametersbarcode.Add("@PRODUCT_SEQ", Productbarcode.PRODUCT_SEQ);
                            parametersbarcode.Add("@PRODUCT_BARCODE", Productbarcode.PRODUCT_BARCODE);
                            barcoderecords = DbUtil.Select<ChemProductModel>(sqlBarcode.ToString(), parametersbarcode);

                            if (barcoderecords.Count > 0)
                            {
                                BarcodeErrorBuilder = rowNumberString + "行目:商品バーコードの" + "[" + Productbarcode.PRODUCT_BARCODE + "]" + "は使用されています。" + "\n";
                                ErrorStatus += BarcodeErrorBuilder;
                                rowNumberString++;
                                AliasProduct = "True";

                            }
                            else
                            {
                                rowNumberString++;
                            }
                        }
                    }
                }
                if (AliasProduct == "True")
                {
                    return ErrorStatus;
                }


                if (Session["Contained"] != null)
                {
                    List<ChemContainedModel> chemContainedmodel = (List<ChemContainedModel>)Session["Contained"];
                    int rowNumberString = 1;
                    string[] MinValue = null;
                    string MinValues = null;
                    string[] maxValue = null;
                    string maxValues = null;
                    foreach (var items in chemContainedmodel)
                    {
                        if (items.MinContainedRate != null)
                        {
                            if (items.MinContainedRate.Contains("."))
                            {
                                MinValue = items.MinContainedRate.Split('.');
                            }
                            else
                            {
                                MinValues = items.MinContainedRate;
                            }
                        }

                        if (items.MaxContainedRate != null)
                        {
                            if (items.MaxContainedRate.Contains("."))
                            {
                                maxValue = items.MaxContainedRate.Split('.');
                            }
                            else
                            {
                                maxValues = items.MaxContainedRate;
                            }
                        }

                        if (items.MAX_CONTAINED_RATE > Convert.ToDecimal(100))
                        {
                            ErrorStatus += rowNumberString + "行目:含有率上限(%)は0以上、100以下で入力してください。" + "\n";
                        }
                        if (items.MIN_CONTAINED_RATE > Convert.ToDecimal(100))
                        {
                            ErrorStatus += rowNumberString + "行目:含有率下限(%)は0以上、100以下で入力してください。" + "\n";
                        }
                        if (items.MIN_CONTAINED_RATE > items.MAX_CONTAINED_RATE)
                        {
                            ErrorStatus += rowNumberString + "行目: 含有率下限(%)は含有率上限(%)より小さい値を入力してください。" + "\n";
                        }
                        if (MinValue != null)
                        {
                            if (MinValue[0].Length > 3)
                            {
                                ErrorStatus += rowNumberString + "行目:含有率下限(%)の整数部は3桁以下で入力してください。" + "\n";
                            }
                        }
                        if (MinValue != null || MinValues != null)
                        {
                            if (MinValue != null)
                            {
                                if (MinValue[1].Length > 3)
                                {
                                    ErrorStatus += rowNumberString + "行目:含有率下限(%)の小数部は3桁以下で入力してください。" + "\n";
                                }
                            }
                            else if (MinValues.Length > 3)
                            {
                                ErrorStatus += rowNumberString + "行目:含有率下限(%)の小数部は3桁以下で入力してください。" + "\n";
                            }
                        }
                        if (maxValue != null)
                        {
                            if (maxValue[0].Length > 3)
                            {
                                ErrorStatus += rowNumberString + "行目:含有率上限(%)の整数部は3桁以下で入力してください。" + "\n";
                            }
                        }
                        if (maxValue != null || maxValues != null)
                        {
                            if (maxValue != null)
                            {
                                if (maxValue[1].Length > 3)
                                {
                                    ErrorStatus += rowNumberString + "行目:含有率上限(%)の小数部は3桁以下で入力してください。" + "\n";
                                }
                            }
                            else if (maxValues.Length > 3)
                            {
                                ErrorStatus += rowNumberString + "行目:含有率上限(%)の小数部は3桁以下で入力してください。" + "\n";
                            }
                        }
                        rowNumberString++;
                    }
                }


                StringBuilder sqlcasno = new StringBuilder();
                DynamicParameters parameterscasno = new DynamicParameters();
                sqlcasno.AppendLine("select");
                sqlcasno.AppendLine("CAS_NO");
                sqlcasno.AppendLine("from");
                sqlcasno.AppendLine("M_CHEM");
                sqlcasno.AppendLine("where");
                sqlcasno.AppendLine("CAS_NO = @CAS_NO");
                sqlcasno.AppendLine("and");
                sqlcasno.AppendLine("DEL_FLAG= @DEL_FLAG");

                parameterscasno.Add("@CAS_NO", casno);
                parameterscasno.Add("@DEL_FLAG", (int)DEL_FLAG.Undelete);

                List<ChemModel> recordscasno = DbUtil.Select<ChemModel>(sqlcasno.ToString(), parameterscasno);

                if (recordscasno.Count > 0)
                {
                    ErrorStatus = "Casno";
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return ErrorStatus;
        }


        public string ProductAcceptence(string ChemicalCodes)
        {
            string producterrorMessages = null;
            try
            {
                if (ChemicalCodes != null)
                {
                    producterrorMessages = ValidAcceptData(ChemicalCodes);
                }
                List<int> newlist = new List<int>();
                if (ChemicalCodes != null)
                {
                    foreach (var items in ChemicalCodes.Split(','))
                    {
                        newlist.Add(items[0]);
                    }
                }
                var count = newlist.Count();
                if (count > NikonConst.MaxAcceptNumber)
                {
                    string.Format(WarningMessages.MaxAcceptNumberOverAndExceededNumber, NikonConst.MaxAcceptNumber.ToString(), (count - NikonConst.MaxAcceptNumber).ToString());
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return producterrorMessages;
        }


        public string ValidAcceptData(string ListChemIds)
        {
            List<string> errorMessages = new List<string>();
            string alertMessages = null;
            var rowNumber = 1;
            try
            {
                if (ListChemIds != "")
                {
                    CsmsCommonInfo csmsCommonInfos = new CsmsCommonInfo();
                    csmsCommonInfos = (CsmsCommonInfo)Session["SessionList"];
                    foreach (var Chemids in ListChemIds.Split(','))
                    {
                        rowNumber = 1;
                        foreach (var item in csmsCommonInfos.ChemCommonList)
                        {
                            if (Chemids.Trim() == item.CHEM_CD)
                            {
                                if (item.MAKER_NAME_DEL_FLAG == false)
                                {
                                    errorMessages.Add(rowNumber.ToString() + "行目：" + string.Format(WarningMessages.AcceptNotPossible, "メーカー"));
                                }
                                if (item.UNITSIZE_NAME_DEL_FLAG == false)
                                {
                                    errorMessages.Add(rowNumber.ToString() + "行目：" + string.Format(WarningMessages.AcceptNotPossible, "容量単位"));
                                }
                            }
                            else
                            {
                                rowNumber++;
                            }
                        }

                        rowNumber = 1;
                        var d = new ChemDataInfo();
                        foreach (var RowCount in csmsCommonInfos.ChemCommonList)
                        {
                            if (Chemids.Trim() == RowCount.CHEM_CD)
                            {
                                UserInformation userInfo = null;
                                if (Session["LoginUserSession"] != null)
                                {
                                    userInfo = (UserInformation)Session["LoginUserSession"];
                                }

                                var isAdmin = userInfo != null ? userInfo.ADMIN_FLAG == Const.cADMIN_FLG_ADMIN : false;
                                var cond = new ChemSearchModel()
                                {
                                    LOGIN_USER_CD = userInfo.User_CD,
                                    CHEM_CD = Chemids
                                };
                                if (d.IsLedgerExists(cond) == false)
                                {
                                    errorMessages.Add(rowNumber.ToString() + "行目：" + string.Format(WarningMessages.NotFound, "管理台帳"));
                                }
                                else if (d.IsLedgerMgmtGroup(cond) == false)
                                {
                                    errorMessages.Add(rowNumber.ToString() + "行目：" + string.Format(WarningMessages.NotRegistGroup, "受入"));
                                }
                                break;
                            }
                            else
                            {
                                rowNumber++;
                            }
                        }
                        d.Dispose();
                        //rowNumber++;
                    }
                    if (errorMessages != null && errorMessages.Count > 0)
                    {
                        foreach (var items in errorMessages)
                        {
                            alertMessages += items.ToString() + "\n";
                        }

                    }
                    if (alertMessages != null)
                    {
                        alertMessages = alertMessages.TrimEnd('\n');
                    }
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }

            return alertMessages;
        }

        public string DoDeleteProcess(string ChemCd, string Chemname)
        {
            string ErrorStatus = null;
            ChemDataInfo datainfo = new ChemDataInfo();
            ChemModel chemModel = new ChemModel();
            try
            {
                if (ChemCd != "")
                {
                    chemModel.CHEM_CD = ChemCd.Trim();

                    if (datainfo.IsLedgerExists(new ChemSearchModel(chemModel)) || datainfo.IsLedgerWorkExists(new ChemSearchModel(chemModel)))
                    {
                        ErrorStatus += string.Format(WarningMessages.RegistedInLedger, chemModel.CHEM_CD, "削除") + "\n";
                    }
                    if (datainfo.IsContainExists(new ChemSearchModel(chemModel)))
                    {
                        ErrorStatus += string.Format(WarningMessages.UsedContainedError, Chemname) + "\n";
                    }
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return ErrorStatus;
        }
    }
}