﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Chem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.Stock;
using ZyCoaG.PrintLabel;

namespace ChemMgmt.Controllers
{
    [Authorize]
    [HandleError]
    public class ZC120Controller : Controller
    {
        public const string PROGRAM_ID = "ZC1020";
        LogUtil logutil = new LogUtil();

        private const string sessionPrefix = "Accept";

        ///// <summary>
        ///// 検索結果の表示件数を取得します。
        ///// </summary>
        //protected override int GetMaxSearchResultCount() => 99999;

        /// <summary>
        /// コピー数の最小値を取得します。
        /// </summary>
        private int copyNumberMinimum { get; } = 2;

        /// <summary>
        /// コピー数の最大値を取得します。
        /// </summary>
        private int copyNumberMaximum { get; } = 100;

        // GET: ZC12010       


        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : This  Method is for load the barcode acceptance page
        /// <summary>
        public ActionResult ZC12010(string Flag)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            inventoryManagement.listChemStockDetails = new List<ChemStockModel>();
            if (Flag != "1")
            {
                if (Session[nameof(ChemStockModel)] != null)
                {
                    inventoryManagement.listChemStockDetails = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                }
            }
            else
            {
                Session[nameof(ChemStockModel)] = null;
                Session.Remove(sessionPrefix + nameof(ChemSearchModel));
            }
            return View(inventoryManagement);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : This  Method is for clear the barcode acceptance page
        /// <summary>
        [HttpPost]
        public ActionResult ZC12010(InventoryManagement inventoryManagement)
        {
            if (Request.Form["Clear"] != null)
            {
                Session.Remove(nameof(ChemStockModel));
                Session.Remove(sessionPrefix + nameof(ChemSearchModel));
                return RedirectToAction("ZC12010");
            }
            return View(inventoryManagement);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : This  Method is for validate the product barcode
        /// <summary>

        public string ValidateProductBarcode(string Barcode)
        {
            string alertMessage = null;

            InventoryManagement inventoryManagement = new InventoryManagement();
            List<ChemStockModel> chemAccepts = new List<ChemStockModel>();
            try
            {
                if (Session[nameof(ChemStockModel)] != null)
                {
                    //chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                    //foreach (var items in chemAccepts)
                    //{
                    //    var data = new ChemStockModel();                      
                    //    data.setLocationList(new LocationMasterInfoInLedger(LocationClassId.StoringLedger, items.REG_NO).GetSelectList());
                    //    chemAccepts.Add(data);                        
                    //}
                    //Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                    chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                }

                var condition = new ChemSearchModel();
                if (Session[sessionPrefix + nameof(ChemSearchModel)] != null)
                {
                    condition = (ChemSearchModel)Session[sessionPrefix + nameof(ChemSearchModel)];
                }
                condition.CHEM_SEARCH_TARGET = (int)ChemSearchTarget.Product;
                condition.REFERENCE_AUTHORITY = "1";
                condition.LOGIN_USER_CD = Common.GetLogonForm().Id;

                if (!string.IsNullOrWhiteSpace(Barcode))
                {
                    condition.PRODUCT_BARCODE = Barcode;
                    condition.CHEM_CD = null;
                    var d = new ChemDataInfo();
                    var chems = d.GetSearchResult(condition).AsEnumerable();
                    d.Dispose();

                    if (chems.Count() != 1)
                    {
                        alertMessage = string.Format(WarningMessages.NotFound, "商品");
                    }
                    else if ((chemAccepts.Count + 1) > NikonConst.MaxAcceptNumber)
                    {
                        alertMessage = string.Format(WarningMessages.MaxAcceptNumberOver, NikonConst.MaxAcceptNumber.ToString());
                    }
                    else
                    {
                        condition.CHEM_CD = chems.Single().CHEM_CD;

                        string regNo = string.Empty;
                        var d2 = new ChemDataInfo();
                        var ledger = d2.IsLedgerExists(condition);
                        var group = d2.IsLedgerMgmtGroup(condition, out regNo);
                        d2.Dispose();

                        // メーカー名または容量単位が削除されている場合はエラーメッセージを表示します。
                        var errorMessages = new List<string>();
                        if (!chems.Single().MAKER_NAME_DEL_FLAG)
                        {
                            errorMessages.Add(string.Format(WarningMessages.AcceptNotPossible, "メーカー"));
                        }
                        if (!chems.Single().UNITSIZE_NAME_DEL_FLAG)
                        {
                            errorMessages.Add(string.Format(WarningMessages.AcceptNotPossible, "容量単位"));
                        }
                        // 台帳が存在しない、台帳が所属部門のものではない場合はエラーメッセージを表示します。
                        if (!ledger)
                        {
                            errorMessages.Add(string.Format(WarningMessages.NotFound, "管理台帳"));
                        }
                        if (!group)
                        {
                            errorMessages.Add(string.Format(WarningMessages.NotRegistGroup, "受入"));
                        }

                        if (errorMessages.Count > 0)
                        {
                            foreach (var items in errorMessages)
                            {
                                if (errorMessages.Count > 1)
                                {
                                    alertMessage += items + "\n";
                                }
                                else
                                {
                                    alertMessage = items.ToString();
                                }
                            }
                            if (alertMessage != null)
                            {
                                alertMessage = alertMessage.TrimEnd('\n');
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return alertMessage;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : This  Method is for gettting the product barcode values
        /// <summary>
        public ActionResult ProductBarcodeEnter(string Barcode)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            List<ChemStockModel> chemAccepts = new List<ChemStockModel>();
            try
            {
                if (Session[nameof(ChemStockModel)] != null)
                {
                    chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                }

                ChemSearchModel condition = new ChemSearchModel();
                if (Session[sessionPrefix + nameof(ChemSearchModel)] != null)
                {
                    condition = (ChemSearchModel)Session[sessionPrefix + nameof(ChemSearchModel)];
                }
                condition.CHEM_SEARCH_TARGET = (int)ChemSearchTarget.Product;
                condition.REFERENCE_AUTHORITY = Common.GetLogonForm().REFERENCE_AUTHORITY;
                condition.LOGIN_USER_CD = Common.GetLogonForm().Id;
                int ChemAcceptFlag = 0;
                if (!string.IsNullOrWhiteSpace(Barcode))
                {
                    condition.PRODUCT_BARCODE = Barcode;
                    condition.CHEM_CD = null;
                    var d = new ChemDataInfo();
                    var chems = d.GetSearchResult(condition).AsEnumerable();
                    d.Dispose();
                    condition.CHEM_CD = chems.Single().CHEM_CD;
                    string regNo = string.Empty;
                    var d2 = new ChemDataInfo();
                    var group = d2.IsLedgerMgmtGroup(condition, out regNo);
                    d2.Dispose();

                    // 商品受入一覧の情報に検索された化学物質を追加して再表示します。
                    foreach (var chem in chems)
                    {
                        chems.Single().REG_NO = regNo;

                        var chemStock = new ChemStockModel(chem);
                        chemStock.setFirstLocationList(new LocationMasterInfoInLedger(LocationClassId.StoringLedger, chemStock.REG_NO).GetSelectList());

                        var locationInfo = new LocationMasterInfo(LocationClassId.Storing, chemStock.REG_NO).GetSelectedItem(Convert.ToInt32(chemStock.LOC_ID));
                        chemStock.SetLocation(locationInfo.Single());
                        chemStock.SetFirstLocation(locationInfo.Single());
                        chemAccepts.Add(chemStock);
                        ChemAcceptFlag++;
                    }
                    Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                    inventoryManagement.listChemStockDetails = chemAccepts;
                    condition.PRODUCT_BARCODE = string.Empty;
                }
                if (chemAccepts.Count > 0) condition.IsScanned = true;
                if (chemAccepts != null && chemAccepts.Count > 0)
                {
                    if (ChemAcceptFlag > 0)
                    {
                        Session.Add(sessionPrefix + nameof(ChemSearchModel), condition);
                    }
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return PartialView("_PartialBarcodeAcceptance", inventoryManagement);
        }

        private void getInputData()
        {
            var chemAccepts = new List<ChemStockModel>();

            //foreach (GridViewRow row in ResultGrid.Rows)
            //{
            var data = new ChemStockModel();
            var classTools = new ClassTools<ChemStockModel>(data);
            //data = classTools.GetControlValues(row);
            data.setLocationList(new LocationMasterInfoInLedger(LocationClassId.StoringLedger, data.BARCODE).GetSelectList());
            chemAccepts.Add(data);
            //}
            Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());

            var condition = new ChemSearchModel();
            {
                //var classTools = new ClassTools<ChemSearchModel>(condition);
                // condition = classTools.GetControlValues(SearchFormView);
            }
            Session.Add(sessionPrefix + nameof(ChemSearchModel), condition);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : This  Method is for delete product barcode
        /// <summary>
        public ActionResult Delete(string RowId)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
            if (RowId != "")
            {
                foreach (var rowid in RowId.Split(','))
                {
                    for (int i = 0; i < chemAccepts.Count; i++)
                    {

                        if (chemAccepts[i].RowId.Trim() == rowid.Trim())
                        {
                            chemAccepts.RemoveAt(i);
                        }

                    }
                }
            }
            Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
            inventoryManagement.listChemStockDetails = chemAccepts;
            return View("_PartialBarcodeAcceptance", inventoryManagement);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : This  Method is for create the bottle number for products
        /// <summary>
        public ActionResult Numbering(string RowId)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            //DoSelectionConfirm(false, false);
            try
            {
                var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                foreach (var rowid in RowId.Split(','))
                {
                    for (int i = 0; i < chemAccepts.Count; i++)
                    {
                        if (chemAccepts[i].RowId.Trim() == rowid.Trim())
                        {
                            if (string.IsNullOrWhiteSpace(chemAccepts[i].BARCODE)) chemAccepts[i].IssueNewBarcodeAndStore();
                        }
                    }
                }
                Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                inventoryManagement.listChemStockDetails = chemAccepts;
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View("_PartialBarcodeAcceptance", inventoryManagement);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : This  Method is for validate weight 
        /// /// <summary>
        public string ValidateWeightVolumes(string grossweight, string internalvolume, string RowId)
        {
            string ValidateKey = null;
            List<ChemStockModel> chemAccepts = new List<ChemStockModel>();
            try
            {
                if (Session[nameof(ChemStockModel)] != null)
                {
                    chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                }

                var validate = new Measurement(grossweight, internalvolume);
                if (validate.ValidateMessages.Count > 0)
                {
                    foreach (var items in validate.ValidateMessages)
                    {
                        ValidateKey += items + "\n";
                    }
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return ValidateKey;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : To manage the weight volumes and add the values into list
        /// <summary>
        public ActionResult WeightManagementVolume(string grossweight, string internalvolume, string RowId)
        {
            List<ChemStockModel> chemAccepts = new List<ChemStockModel>();
            InventoryManagement inventory = new InventoryManagement();
            try
            {
                if (Session[nameof(ChemStockModel)] != null)
                {
                    chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                }
                var chemAccept = chemAccepts.Where(x => x.RowId.ToString() == RowId.Trim()).Single();
                var validate = new Measurement(grossweight, internalvolume);
                if (validate.IsValid)
                {
                    for (int i = 0; i < chemAccepts.Count; i++)
                    {
                        if (chemAccepts[i].RowId.Trim() == RowId.Trim())
                        {

                            chemAccepts[i].INI_GROSS_WEIGHT_G = validate.TotalWeight;
                            chemAccepts[i].CUR_GROSS_WEIGHT_G = validate.TotalWeight;
                            chemAccepts[i].INI_GROSS_WEIGHT_G_NAME = validate.TotalWeight.ToString() + "g";
                            chemAccepts[i].DETAIL_WEIGHT_NAME = validate.DetailWeight.ToString() + "g";
                            chemAccepts[i].DETAIL_WEIGHT = validate.DetailWeight;
                            chemAccept.CalculateBottleWeight();
                        }

                    }
                    Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                    inventory.listChemStockDetails = chemAccepts;
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View("_PartialBarcodeAcceptance", inventory);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : To manage the validation of copy details
        /// <summary>
        public string ValidateCopy(string RowId, decimal copyNumber)
        {
            string AlertMessages = "";
            InventoryManagement inventory = new InventoryManagement();
            //DoSelectionConfirm();
            try
            {
                if (string.IsNullOrWhiteSpace(copyNumber.ToString()))
                {
                    AlertMessages = string.Format(WarningMessages.NotInput, "コピー数");
                    return AlertMessages;
                }
                if (copyNumber.ToString().Length > copyNumberMaximum.ToString().Length)
                {
                    AlertMessages = string.Format(WarningMessages.MaximumLength, "コピー数", copyNumberMaximum.ToString().Length);
                    return AlertMessages;
                }

                if (!decimal.TryParse(copyNumber.ToString(), out copyNumber))
                {
                    AlertMessages = string.Format(WarningMessages.NotNumber, "コピー数");
                    return AlertMessages;
                }
                if (copyNumber < copyNumberMinimum || copyNumberMaximum < copyNumber)
                {
                    AlertMessages = string.Format(WarningMessages.ThresholdValueRangeOver, "コピー数", copyNumberMinimum.ToString(), copyNumberMaximum.ToString());
                    return AlertMessages;
                }

                var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();


                for (int i = 0; i < chemAccepts.Count; i++)
                {
                    foreach (var rowid in RowId.Split(','))
                    {
                        if (chemAccepts[i].RowId.Trim() == rowid.Trim())
                        {
                            chemAccepts[i].CHECK = true;
                        }
                    }
                }

                var count = chemAccepts.Count + (chemAccepts.Where(x => x.CHECK).Count() * (copyNumber - 1));
                chemAccepts.ConvertAll(x => x.CHECK = false);
                if (count > NikonConst.MaxAcceptNumber)
                {
                    AlertMessages = string.Format(WarningMessages.MaxAcceptNumberOverAndExceededNumber, NikonConst.MaxAcceptNumber.ToString(), (count - NikonConst.MaxAcceptNumber).ToString());
                    return AlertMessages;
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return AlertMessages;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : To manage the copy of the barcode details
        /// <summary>
        public ActionResult Copy(string RowId, decimal copyNumber)
        {
            InventoryManagement inventory = new InventoryManagement();
            //DoSelectionConfirm();
            try
            {
                var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();

                for (int i = 0; i < chemAccepts.Count; i++)
                {
                    foreach (var rowid in RowId.Split(','))
                    {
                        if (chemAccepts[i].RowId.Trim() == rowid.Trim())
                        {
                            chemAccepts[i].CHECK = true;
                        }
                    }
                }

                for (var i = chemAccepts.Count - 1; i >= 0; i--)
                {
                    if (chemAccepts[i].CHECK == false) continue;
                    var copy = chemAccepts[i].Clone();
                    copy.BARCODE = string.Empty;

                    copy.IsAccepted = false;
                    for (var c = 0; c < copyNumber - 1; c++)
                    {
                        var insert = copy.Clone();
                        insert.RowId = Guid.NewGuid().ToString();
                        chemAccepts.Insert(i + 1, insert);
                    }
                }
                chemAccepts.ConvertAll(x => x.CHECK = false);
                Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                inventory.listChemStockDetails = chemAccepts;
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View("_PartialBarcodeAcceptance", inventory);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : To manage the validation of location barcode
        /// <summary>
        public string ValidateLocationBarcodeValue(string TextValue, string RowId)
        {
            string AlertMessage = "";         
            InventoryManagement inventory = new InventoryManagement();
            try
            {
                var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();

                var barcode = TextValue;
                var rowId = RowId.Trim();
                var chemAccept = chemAccepts.Where(x => x.RowId.ToString() == rowId).Single();

                if (!string.IsNullOrWhiteSpace(barcode))
                {
                    using (var locationInfo = new LocationMasterInfo(LocationClassId.StoringLedger, chemAccept.REG_NO))
                    {
                        var locations = locationInfo.GetData(barcode, DEL_FLAG.Undelete);
                        if (locations.Count() != 1 || locationInfo.GetChildrenData(barcode, DEL_FLAG.Undelete).Count() > 0)
                        {
                            AlertMessage = string.Format(WarningMessages.NotFound, "保管場所");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return AlertMessage;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : To manage the validation of location barcode entering values
        /// <summary>
        public ActionResult LocationBarcodeEnter(string TextValue, string RowId)
        {
            InventoryManagement inventory = new InventoryManagement();
            try
            {
                var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();              
                var barcode = TextValue;
                var rowId = RowId.Trim();
                var chemAccept = chemAccepts.Where(x => x.RowId.ToString() == rowId).Single();

                if (!string.IsNullOrWhiteSpace(barcode))
                {
                    using (var locationInfo = new LocationMasterInfo(LocationClassId.StoringLedger, chemAccept.REG_NO))
                    {
                        var locations = locationInfo.GetData(barcode, DEL_FLAG.Undelete);
                        if (locations.Count() != 1 || locationInfo.GetChildrenData(barcode, DEL_FLAG.Undelete).Count() > 0)
                        {
                        }
                        else
                        {
                            chemAccept.SetLocation(locations.Single());
                            chemAccept.SetFirstLocation(locations.Single());
                            Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                        }
                    }
                }
                else
                {
                    var locationInfo = new LocationMasterInfo(LocationClassId.Storing, chemAccept.REG_NO).GetSelectedItem(Convert.ToInt32(chemAccept.LOC_ID));
                    chemAccept.SetLocation(locationInfo.Single());
                    chemAccept.SetFirstLocation(locationInfo.Single());
                    Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                }
                inventory.listChemStockDetails = chemAccepts;
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View("_PartialBarcodeAcceptance", inventory);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : To manage the location barcode values assigning into the list
        /// <summary>
        public ActionResult setLocationBarcode(string ddlLocation, string rowId)
        {
            InventoryManagement inventory = new InventoryManagement();
            try
            {
                var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                var chemAccept = chemAccepts.Where(x => x.RowId.ToString() == rowId.Trim()).Single();
                var locationInfo = new LocationMasterInfo(LocationClassId.Storing, chemAccept.REG_NO).GetSelectedItem(Convert.ToInt32(ddlLocation));
                chemAccept.SetLocation(locationInfo.Single());
                chemAccept.SetFirstLocation(locationInfo.Single());
                Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                inventory.listChemStockDetails = chemAccepts;
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View("_PartialBarcodeAcceptance", inventory);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : To accept the barcode details
        /// <summary>
        public ActionResult BarcodeAccept(string RowId)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            try
            {
                var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                inventoryManagement.listChemStockDetails = chemAccepts;
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View("_PartialBarcodeAcceptance", inventoryManagement);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : To validate the barcode acceptance
        /// <summary>
        public Tuple<string, string> ValidateBarcodeAccept(string RowId)
        {
            //getInputData();
            string WarningMessagesValue = "";
            try
            {
                var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                int intChemCount = chemAccepts.Where(x => !string.IsNullOrWhiteSpace(x.BARCODE) && !x.IsAccepted).Count();
                if (intChemCount == 0)
                {
                    WarningMessagesValue = string.Format(WarningMessages.TargetDoseNotExist, "受入");
                    return new Tuple<string, string>(WarningMessagesValue, "TargetDoseNotExist");
                }
                if (Common.GetLogonForm().User_CD != null)
                {
                    //if (chemAccepts[0].RowId == RowId.Trim())
                    //{
                    chemAccepts.ConvertAll(x => x.OWNER = Common.GetLogonForm().User_CD);
                    chemAccepts.ConvertAll(x => x.DELI_DATE = DateTime.Now);
                    chemAccepts.ConvertAll(x => x.USAGE_MEMO = x.MEMO);

                    using (var accepts = new ChemStockDataInfo())
                    {
                        accepts.Add(chemAccepts);
                        if (!accepts.IsValid)
                        {
                            WarningMessagesValue = string.Join(SystemConst.ScriptLinefeed, accepts.ErrorMessages);
                            Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                            WarningMessagesValue = WarningMessagesValue.Replace("\\n", "\n");
                            return new Tuple<string, string>(WarningMessagesValue, "NotValid");
                        }
                        else
                        {
                            chemAccepts.Where(x => !string.IsNullOrWhiteSpace(x.BARCODE) && !x.IsAccepted).ToList().ConvertAll(x => x.IsAccepted = true);
                            Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                            string infomessage = "{0}件受け入れました。";
                            WarningMessagesValue = string.Format(infomessage, Convert.ToString(intChemCount));
                            return new Tuple<string, string>(WarningMessagesValue, "IsValid");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return new Tuple<string, string>(WarningMessagesValue, "");
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 23-9-2019
        /// Description : To save the memo details
        /// <summary>

        public ActionResult SaveMemoDetails(string MemoValue, string rowId)
        {

            List<ChemStockModel> chemAccepts = new List<ChemStockModel>();
            InventoryManagement inventory = new InventoryManagement();
            try
            {
                if (Session[nameof(ChemStockModel)] != null)
                {
                    chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                }

                for (int i = 0; i < chemAccepts.Count; i++)

                {
                    if (chemAccepts[i].RowId.Trim() == rowId.Trim())
                    {
                        chemAccepts[i].MEMO = MemoValue;
                    }
                }
                Session.Add(nameof(ChemStockModel), chemAccepts.AsEnumerable());
                inventory.listChemStockDetails = chemAccepts;
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View("_PartialBarcodeAcceptance", inventory);
        }


        /// <summary>
        /// <para>バーコード印刷ボタンの処理です。</para>
        /// <para>選択した商品受入対象且つ瓶番号採番済みのバーコードを作成します。</para>
        /// </summary>
        [HttpPost]
        public string LabelOutput(string rowId)
        {
            string retval = null;
            var dataCollection = new List<LabelData>();
            try
            {
                if (Session[nameof(ChemStockModel)] != null)
                {
                    var chemAccepts = ((IEnumerable<ChemStockModel>)Session[nameof(ChemStockModel)]).ToList();
                    chemAccepts.Where(x => x.CHECK && x.IsAccepted && !string.IsNullOrWhiteSpace(x.BARCODE)).ToList().ForEach(x =>
                    {
                        dataCollection.Add(new LabelData(x.BARCODE, x.PRODUCT_NAME_JP));
                    });
                }
                if (dataCollection.Count == 0)
                {
                    retval = string.Format(WarningMessages.NotSelected, "対象");
                }
                else
                {
                    var labelPrint = new PTouchLabelPrint(LabelTypes.Bottle);
                    retval = labelPrint.CreatePrintJavaScript(dataCollection);
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return retval;
        }

    }
}
