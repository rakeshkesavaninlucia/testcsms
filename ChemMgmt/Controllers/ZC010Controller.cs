﻿using ChemMgmt.App_Start;
using ZyCoaG.Util;
using IdentityManagement.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.IO;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ZyCoaG.BaseCommon;
using ZyCoaG.Classes.util;
using Dapper;
using ZyCoaG.PrintLabel;
using ChemMgmt.Repository;
using static ZyCoaG.BaseCommon.SystemParameterValues;
using System.DirectoryServices;
using ZyCoaG.Nikon;
//using ZyCoaG.Nikon.History;
using ChemMgmt.Classes.util;
using ChemMgmt.Classes;
using ChemMgmt.Models;
using ZyCoaG.util;
using System.ComponentModel;
using ZyCoaG.Nikon.History;
using System.Web.Http.Results;

/// <summary>
/// MSCGI
/// Author: MSCGI
/// Created date: 10-7-2019
/// Description : For WebCommon Controller class named as ZC010Controller in the namespace of ZyCoaG.ZC010
/// <summary>
namespace ZyCoaG.ZC010
{

    [HandleError]
    public class ZC010Controller : Controller
    {
        // private List<UserInfo.ClsUser> getUserList = new List<UserInfo.ClsUser>();
        private LogUtil _logoututil = new LogUtil();//accessing the log class
        //private Dictionary<string, string> inputParam;

        private static SelectType selectType { get; set; }  // calling selectype from system const
        private List<string> FilterIds { get; set; } = new List<string>(); //creating list for filterids
        // For Common Popup selection screens
        CategorySelect categorySelect;
        WebCommonInfo selectKeys = new WebCommonInfo();
        WebCommonRepo repository = new WebCommonRepo();
        SystemParameters systemParameters = new SystemParameters();

        private const string ProgramId = "ZC010";
        public const string SessionNameTransaction = "ZC010_Trans";
        public const string cSESSION_NAME_InputSearch = "ZC01021_InputSearch";
        private static int searchEnabled = 0;

        private const string OUTLIST_FILENAME = "ユーザーマスター.csv";
        private const string TEMPLATE_FILENAME = "ユーザーマスターテンプレート.csv";

        private ApplicationSignInManager _signInManager;
        private bool loginLDAP = false;
        private bool loginNormal = false;
        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-7-2019
        /// Description : For User Logout
        /// <summary>
        public ActionResult Logout()
        {
            // Identity Manager will signout the user
            SignInManager.AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            Session.Abandon();
            Session.Clear();
            Session.RemoveAll();
            return RedirectToAction("ZC01001", "ZC010");
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-7-2019
        /// Description : To display the Login Page
        /// <summary>
        public ActionResult ZC01001()
        {
            //var chkTimeOut = Session.Timeout;
            //if (chkTimeOut <= 20)
            //{
            //    // set new time out to session  
            //    Session.Timeout = 60;
            //}
            setLoginData();
            return View();
        }



        /// <summary>
        /// Author: MSCGI
        /// Created date: 11-7-2019
        /// Description : To display the Dashboard.
        /// <summary>
        [Authorize(Roles = "0,1")]
        public ActionResult ZC01011()
        {
            SessionRemove();
            string[] ArryUNIT = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CONVERT_UNIT"]).Split(',');

            System.Collections.Generic.Dictionary<string, string> dic = new System.Collections.Generic.Dictionary<string, string>();

            foreach (string str in ArryUNIT)
            {
                string[] ArryKeyValue = str.Split(':');
                dic.Add(ArryKeyValue[0], ArryKeyValue[1]);
            }

            Session["CONVERT_UNIT"] = dic;
            // マスター名を設定します。
            WebCommonInfo webCommon = new WebCommonInfo();
            ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];

            string adminFlag = "";
            if (appUser != null)
            {
                if (appUser.ADMIN_FLAG != null)
                {
                    adminFlag = appUser.ADMIN_FLAG;
                }
            }
            //string[] roles = System.Web.Security.Roles.GetRolesForUser(User.Identity.Name);
            //_logoututil.OutInfoLog(ProgramId, roles.ToString());
            if (adminFlag.Equals("1"))
            {
                // display master maintenance dropdown only for admins
                try
                {
                    List<S_TAB_CONTROL> tableControls = Control.GetTableControl();
                    List<SelectListItem> groupTypeList = new List<SelectListItem> { };
                    foreach (S_TAB_CONTROL tableControl in tableControls.Where(x => x.IS_VIEW))
                    {
                        groupTypeList.Add(new SelectListItem
                        {
                            Text = tableControl.TABLE_DISPLAY_NAME,
                            Value = ".." + tableControl.MAINTENANCE_URL + "?" + QueryString.TableNameKey + "=" + tableControl.TABLE_NAME
                        });
                    }
                    webCommon.LookupMasterName = groupTypeList;
                }
                catch (Exception ex)
                {
                    _logoututil.OutErrLog(ProgramId, ex.ToString());
                }
            }

            Session.Remove(SessionNameTransaction);
            Session.Remove(cSESSION_NAME_InputSearch);
            Session.Remove(SessionConst.MasterKeys);
            return View(webCommon);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-7-2019
        /// Description : For the Login Submit Action
        /// <summary>
        [HttpPost]
        // [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<string> ZC01001(string UserName, string Password, string method)
        {
            string ret = "";
            try
            {
                if (systemParameters.AuthMethod == AuthMethod.LocalOrLdap)
                {
#if DEBUG
                    // In the debug mode use only normal login from DB
                    loginNormal = true;
                    TempData["DebugMode"] = "Debug";
#else
                    // In release mode get the Authentication method prefered by the user.
                    if(method.Equals("LDAP"))
                    {
                    loginLDAP = true;
                    }
                    else
                    {
                    loginNormal = true;
                    }
#endif
                }
                if (systemParameters.AuthMethod == AuthMethod.LocalOnly || loginNormal)
                {
                    // User SignIn. Get the user information from the DB using username submitted
                    ApplicationUser appUser = await SignInManager.UserManager.FindByNameAsync(UserName);
                    if (appUser != null)
                    {
                        // Get the and User ID and Position from UserID
                        appUser.Id = UserName;
                        appUser.UserName = repository.GetUserName(UserName);
                        appUser.UserTitle = WebCommonRepo.GetAdminFlagName(appUser.ADMIN_FLAG);

                        // check the user password
                        if (appUser.Password == Common.GetSHA1Hash(Password))
                        {
                            //password is correct 
                            SignInManager.SignIn(appUser, false, false);
                            appUser.GROUP_NAME = repository.SelectGroupName(appUser.User_CD);

                            if (appUser.ADMIN_FLAG.Equals(Const.cADMIN_FLG_ADMIN))
                            {
                                appUser.REFERENCE_AUTHORITY = Const.cREFERENCE_AUTHORITY_FLAG_ADMIN;
                            }
                            else
                            {
                                appUser.REFERENCE_AUTHORITY = Const.cREFERENCE_AUTHORITY_FLAG_NORMAL;
                            }
                            Session["LoginUserSession"] = appUser;
                            bool result = false;
                            result = repository.GetLoginUserInfo(Convert.ToInt32(appUser.ADMIN_FLAG), appUser.Id);
                            return "";
                        }
                        else
                        {
                            // password is wrong
                            // TempData["DisplayName"] = repository.DisplayMessage(); 
                            ret = Common.GetMessage("C002", "ユーザーIDとパスワード");
                            return ret;
                        }
                    }
                    else
                    {
                        // user not exists
                        //    TempData["DisplayName"] = repository.DisplayMessage();
                        ret = Common.GetMessage("C002", "ユーザーIDとパスワード");
                        return ret;
                    }
                }
                else if (systemParameters.AuthMethod == AuthMethod.LdapOnly || loginLDAP)
                {
                    // Authenticate the user from LDAP Server
                    ret = isAuthenticated(UserName, Password);
                    if (ret == "")
                    {
                        // Get the user details from the Database and create session
                        ApplicationUser appUser = await SignInManager.UserManager.FindByNameAsync(UserName);

                        // Get the and User ID and Position from UserID
                        appUser.Id = UserName;
                        appUser.UserName = repository.GetUserName(UserName);
                        appUser.UserTitle = WebCommonRepo.GetAdminFlagName(appUser.ADMIN_FLAG);

                        // Sign In the user
                        SignInManager.SignIn(appUser, false, false);
                        appUser.GROUP_NAME = repository.SelectGroupName(appUser.User_CD);

                        if (appUser.ADMIN_FLAG.Equals(Const.cADMIN_FLG_ADMIN))
                        {
                            appUser.REFERENCE_AUTHORITY = Const.cREFERENCE_AUTHORITY_FLAG_ADMIN;
                        }
                        else
                        {
                            appUser.REFERENCE_AUTHORITY = Const.cREFERENCE_AUTHORITY_FLAG_NORMAL;
                        }
                        Session["LoginUserSession"] = appUser;
                        bool result = false;
                        result = repository.GetLoginUserInfo(Convert.ToInt32(appUser.ADMIN_FLAG), appUser.Id);
                        // ログインからメニュー画面を開いたかの判定処理に使用します。
                        Session.Add(SessionConst.IsLogin, true);

                        return ret;
                    }
                    else
                    {
                        // user not exists
                        //  TempData["DisplayName"] = repository.DisplayMessage();
                        //  TempData["ErrorMsg"] = ret;
                        ret = Common.GetMessage("C002", "ユーザーIDとパスワード");
                        return ret;
                    }
                }

                // To display the Attestation for Auth Method at the login screen
                setLoginData();

            }
            catch (Exception ex)
            {
                setLoginData();
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            return ret;
        }



        private void setLoginData()
        {
            // To display the Attestation for Auth Method at the login screen
#if !DEBUG
            if (systemParameters.AuthMethod == AuthMethod.LocalOrLdap)
            {
                TempData["AuthMethod"] = "Show Options";
            }
#endif

            TempData["DisplayName"] = repository.DisplayMessage();
        }


        public ActionResult ZC01041(string StockId)
        {
            WebCommonInfo webcommoninfo = new WebCommonInfo();
            if (!string.IsNullOrWhiteSpace(StockId))
            {
                webcommoninfo.StockId = StockId;
                webcommoninfo.REFERENCE_AUTHORITY = Common.GetLogonForm().REFERENCE_AUTHORITY;
                webcommoninfo = DoSearch(StockId, null);
                if (webcommoninfo.listInventoryUsageHistory != null && webcommoninfo.listInventoryUsageHistory.Count > 0)
                {
                    webcommoninfo.Barocde = webcommoninfo.listInventoryUsageHistory[0].BARCODE;
                }
            }
            else
            {
                webcommoninfo.BottleFlow = "1";
            }
            return View(webcommoninfo);
        }

        [HttpPost]
        public ActionResult ZC01041(WebCommonInfo webcommon)
        {
            string OUTLIST_FILENAME = "使用履歴.csv";

            if (Request.Form["btnClear_Click"] != null)
            {
                if (!string.IsNullOrEmpty(webcommon.BottleFlow))
                {
                    return RedirectToAction("ZC01041", "ZC010", new { BottleFlow = webcommon.BottleFlow });
                }
                else
                {
                    return RedirectToAction("ZC01041", "ZC010");
                }
            }
            else if (Request.Form["btnOutList_Click"] != null)
            {
                string OldBarocde = null;
                if (Request.Form["hdnBarcode"] != null)
                {
                    webcommon.Barocde = Request.Form["hdnBarcode"];
                    OldBarocde = webcommon.Barocde;
                }

                webcommon = DoSearch(webcommon.StockId, webcommon.Barocde);
                var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                if (webcommon.listInventoryUsageHistory != null & webcommon.listInventoryUsageHistory.Count > 0)
                {
                    using (var csv = new StreamWriter(csvFileFullPath, false, Encoding.GetEncoding("shift_jis")))
                    {
                        csv.Write(string.Empty);
                        csv.Close();
                    }

                    repository.OutputCSV(webcommon.listInventoryUsageHistory, csvFileFullPath, false, false);
                    string replaceCsvFileName = OldBarocde + "_" + OUTLIST_FILENAME.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                    Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                }

            }
            return View(webcommon);
        }


        private WebCommonInfo DoSearch(string Id, string barcode)
        {
            WebCommonInfo wecCommon = new WebCommonInfo();
            try
            {
                wecCommon.USER_CD = Common.GetLogonForm().Id;
                DynamicParameters _params = new DynamicParameters();
                _params.Add("@BARCODE", barcode);
                _params.Add("@USER_CD", wecCommon.USER_CD);
                if (!string.IsNullOrWhiteSpace(Id))
                {
                    _params.Add("@STOCK_ID", Convert.ToInt32(Id));
                }
                else
                {
                    _params.Add("@STOCK_ID", null);
                }
                if (wecCommon.REFERENCE_AUTHORITY == Const.cREFERENCE_AUTHORITY_FLAG_NORMAL)
                {
                    if (!Common.isAcceptedBeforePhase2(barcode, Nikon.StockSearchTarget.barcode))
                    {
                        //　台帳検索
                        List<int> dtLedger = DbUtil.Select<int>(repository.GetSqlForLedger(), _params);
                        if (dtLedger.Count == 0)
                        {
                            //Common.MsgBox(this, string.Format(WarningMessages.NotFound, "管理台帳"));
                            //return;
                        }
                        //部署検索
                        List<int> dtGroup = DbUtil.Select<int>(repository.GetSqlForLedgerGroup(), _params);
                        if (dtGroup.Count == 0)
                        {
                            //Common.MsgBox(this, string.Format(WarningMessages.NotRegistGroup, "使用履歴検索"));
                            //return;
                        }
                    }
                }
                List<InventoryUsageHistory> dt = DbUtil.Select<InventoryUsageHistory>(repository.GetSql(), _params);
                wecCommon.listInventoryUsageHistory = dt;
                ///txtBarcode.Text = string.Empty;
                if (dt.Count > 0)
                {
                    //hdnBarcode.Value = dt.Rows[0]["BARCODE"].ToString();
                    // 動作順を追加
                    //dt.Columns.Add(colID.SEQ_NO, typeof(Int32));
                    //for (int i = 0; i < dt.Rows.Count; i++)
                    //{
                    //    dt.Rows[i][colID.SEQ_NO] = i + 1;
                    //}               
                    //2019/02/18 TPE.Sugimoto Add Sta
                    //long resultCount = dt.Count;
                    //Session[sessionSearchResultCount] = "件数：" + resultCount + "件";
                    //ResultCount.Text = (string)Session[sessionSearchResultCount];
                    //2019/02/18 TPE.Sugimoto Add End
                }
                //2019/02/18 TPE.Sugimoto Add Sta
                else
                {
                    //long resultCount = dt.Rows.Count;
                    //Session[sessionSearchResultCount] = "件数：" + resultCount + "件";
                    //ResultCount.Text = (string)Session[sessionSearchResultCount];
                }
                //2019/02/18 TPE.Sugimoto Add End
                //2019/02/18 TPE.Sugimoto Del Sta
                //else
                //{
                //    Common.MsgBox(this, string.Format(WarningMessages.NotFound, "瓶番号"));
                //}
                //2019/02/18 TPE.Sugimoto Del End
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return wecCommon;
        }

        public enum ucSelectionControlType
        {
            /// <summary>
            /// 場所
            /// </summary>
            Location,

            /// <summary>
            /// 保管場所
            /// </summary>
            StoringLocation,

            /// <summary>
            /// 使用場所
            /// </summary>
            UsageLocation,

            /// <summary>
            /// 法規制
            /// </summary>
            Regulation,

            /// <summary>
            /// グループユーザー
            /// </summary>
            GroupUser
        }

        /// <summary>
        /// 選択コントロールタイプを指定します。
        /// </summary>
        [Description("選択コントロールタイプを指定します。")]
        public ucSelectionControlType SelectionControlType { private get; set; }

        /// <summary>
        /// 場所クラスIDを取得します。
        /// </summary>
        private LocationClassId locationClassId
        {
            get
            {
                switch (SelectionControlType)
                {
                    case (ucSelectionControlType.StoringLocation):
                        return LocationClassId.Storing;
                    case (ucSelectionControlType.UsageLocation):
                        return LocationClassId.Usage;
                    default:
                        return LocationClassId.All;
                }
            }
        }

        public ActionResult GetBottleNumberValues(string Barcode)
        {
            WebCommonInfo webcommoninfo = new WebCommonInfo();
            if (!string.IsNullOrWhiteSpace(Barcode))
            {
                webcommoninfo = DoSearch(null, Barcode);
                webcommoninfo.Barocde = Barcode;
            }
            return PartialView("_PartialBarcodeDetails", webcommoninfo);
        }

        /// <summary>
        /// LDAP認証
        /// </summary>
        /// <returns></returns>
        private string isAuthenticated(string userName, string password)
        {
            string ret = "";
            SystemParameters systemParameters = new SystemParameters();
            // Get the LDAP details from DB
            string authLdapPath = systemParameters.LdapServerPath;
            string authUserName = string.Format(systemParameters.LdapUserName, userName);
            string authTypeName = systemParameters.LdapAuthMethod;

            AuthenticationTypes authType = AuthenticationTypes.None;
            foreach (AuthenticationTypes auth in Enum.GetValues(typeof(AuthenticationTypes)))
            {
                if (authTypeName.ToUpper() == auth.ToString().ToUpper()) authType = auth;
            }

            // Validate the User in Active Directory server
            DirectoryEntry entry = new DirectoryEntry(authLdapPath, userName, password);
            // DirectoryEntry entry = new DirectoryEntry(authLdapPath, authUserName, password, authType);
            try
            {
                //強制的に認証する
                object obj = entry.NativeObject;
            }
            catch (System.Runtime.InteropServices.COMException ex)
            {
                // ユーザーが登録されていない場合
                if (ex.ErrorCode == -2147016656)
                {
                    ret = Common.GetMessage("C002", "ユーザーIDとパスワード");
                    TempData["DisplayName"] = repository.DisplayMessage();
                }
                // パスワードを間違えた場合
                else if (ex.ErrorCode == -2147023570)
                {
                    ret = Common.GetMessage("C002", "ユーザーIDとパスワード");
                    TempData["DisplayName"] = repository.DisplayMessage();
                }
                // 上記以外のエラーの場合
                else
                {
                    //上記以外の例外エラーの場合は例外扱い
                    ret = Common.GetMessage("C002", "ユーザーIDとパスワード");
                    TempData["DisplayName"] = repository.DisplayMessage();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return ret;
        }

        /// <summary>
        /// 入力チェックを行います。（更新用）
        /// </summary>
        /// <returns>true：正常 / false：異常</returns>
        private List<string> InputCheck(WebCommonInfo user)
        {
            DataCheckBase _DataCheck = new DataCheckBase();
            List<string> errorlist = new List<string>();
            string CheckUserCD = "";
            if (user.TYPE == "New" || (user.TYPE == "Edit" && user.CALLER_IDENTITY == "New"))
            {
                // 指定コードの重複NG
                CheckUserCD = _DataCheck.CheckUserCD(user.USER_CD, true, true, true, new string[] { "txtUserCd", "0" });
                if (CheckUserCD != "")
                {
                    errorlist.Add(CheckUserCD);
                }
            }
            else
            {
                CheckUserCD = _DataCheck.CheckUserCD(user.USER_CD, true, true, false, new string[] { "txtUserCd", "0" });
                if (CheckUserCD != "")
                {
                    errorlist.Add(CheckUserCD);
                }
            }

            string CheckUserName = _DataCheck.CheckUserName(user.USER_NAME, "社員名", true, new string[] { "txtUserName", "0" });
            if (CheckUserName != "")
            {
                errorlist.Add(CheckUserName);
            }

            // 社員名（ローマ字）
            string CheckUserNameKana = _DataCheck.CheckUserNameKana(user.USER_NAME_KANA, "社員名（ローマ字）", true, new string[] { "txtUserNameKana", "0" });
            if (CheckUserNameKana != "")
            {
                errorlist.Add(CheckUserNameKana);
            }

            // 部署名
            if (string.IsNullOrEmpty(user.GROUP_CD?.Trim()))
            {
                errorlist.Add(Common.GetMessage("C001", "部署名"));

            }

            // TEL
            if (!String.IsNullOrEmpty(user.TEL))
            {
                string CheckTel = (_DataCheck.CheckTel(user.TEL, "内線番号", false, new string[] { "txtTel", "0" }));
                if (CheckTel != "")
                {
                    errorlist.Add(CheckTel);
                }
            }
            // EMail
            string CheckEMail = _DataCheck.CheckEMail(user.EMAIL, "メールアドレス", true, new string[] { "txtEmail", "0" });
            if (CheckEMail != "")
            {
                errorlist.Add(CheckEMail);
            }

            // 上長(Phase1：第一承認者)
            if (string.IsNullOrEmpty(user.APPROVER1_NAME?.Trim()))
            {
                errorlist.Add(Common.GetMessage("C001", "上長"));

            }

            // 管理者権限
            //if (string.IsNullOrEmpty(user.ADMIN_FLAGNAME?.Trim()))
            //{
            //    errorlist.Add(Common.GetMessage("C001", "管理者権限"));

            //}

            // パスワード
            string CheckPassword = _DataCheck.CheckPassword(user.TxtPassword, user.TxtPasswordCheck, false, "パスワード", new string[] { "txtPassword", "0" });
            if (CheckPassword != "")
            {
                errorlist.Add(CheckPassword);
            }

            // フロー名
            //if (string.IsNullOrEmpty(user.USAGE?.Trim()))
            //{
            //    errorlist.Add(Common.GetMessage("C001", "フロー名"));

            //}
            return errorlist;
        }
        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-7-2019
        /// Description : For the User Master Maintenance Screen View
        /// <summary>

        [Authorize(Roles = "0,1")]
        public ActionResult ZC01022(string identityFlag, string usercd, string type, string isInitialize, string callerIdentity)
        {
            WebCommonInfo webCommon = new WebCommonInfo();
            string userID = Common.GetLogonForm().Id;
            try
            {
                // check whether screen is loaded by user settings from webcommon or master maintainance
                if (identityFlag != "" || identityFlag != null)
                {
                    // assign the User Code based on Edit Different User settings from Master Maintenance by admin or Edit same user from user setting
                    if (identityFlag != null && usercd != null)
                    {
                        // Edit Different User 
                        webCommon.USER_CD = usercd;
                        webCommon.IDENTITY = "M_USER";
                        webCommon.TYPE = "Edit";
                    }
                    else if (identityFlag != null && type == "New")
                    {
                        // Request came from Master Maintenance so create new registration with empty fields
                        MasterService ms = new MasterService();

                        // get the dropdown admin flag value from common master info list based on the user code
                        // 管理者フラグ
                        List<SelectListItem> ddlAdminFlag = new List<SelectListItem> { };
                        new CommonMasterInfo(SystemCommonMasterName.GrantName).GetSelectList().ToList().
                            ForEach(x => ddlAdminFlag.Add(new SelectListItem
                            { Value = x.Id.ToString(), Text = x.Name }));

                        //webCommon = SetComboBox(webCommon);
                        webCommon.ddlAdminFlag = ddlAdminFlag;
                        // Load the Work Flow combo box
                        // フロー名
                        List<M_WORKFLOW> dtFlow = ms.SelectWorkFlow();
                        webCommon.ddlFlowCd = repository.BindLookupValues(dtFlow);

                        /*    //Need to check the below 6 lines where Drop Pattern combo is used. since it is not displayed in screen
                            string sql = null;
                            DynamicParameters param = new DynamicParameters();
                            param.Add("@USER_CD", webCommon.USER_CD);
                            // DropDownの設定
                            sql = repository.GetSqlPatternName();
                            List<M_USER_MAKER> dt = DbUtil.Select<M_USER_MAKER>(sql, param);
                            webCommon.drpPattern = BindLookupValues(dt);*/
                        webCommon.TYPE = "New";
                        Session["NewReload"] = webCommon;
                        return View(webCommon);
                    }
                }

                // for user settings in webcommon and update in master maintainence

                // check session is there or not
                if (Session[SessionNameTransaction] != null && isInitialize != "1")
                {

                    // set the session in the webcommon model class returned from preview page ZC1024
                    webCommon = (WebCommonInfo)Session[SessionNameTransaction];
                    MasterService ms = new MasterService();

                    // get the dropdown admin flag value from common master info list based on the user code
                    // 管理者フラグ
                    List<SelectListItem> ddlAdminFlag = new List<SelectListItem> { };
                    new CommonMasterInfo(SystemCommonMasterName.GrantName).GetSelectList().ToList().
                        ForEach(x => ddlAdminFlag.Add(new SelectListItem
                        { Value = x.Id.ToString(), Text = x.Name }));

                    if (identityFlag != "M_USER")
                    { // filter the admin flag if it is called from user setting
                        webCommon.ddlAdminFlag = ddlAdminFlag.Where(x => x.Value.Equals(webCommon.ADMIN_FLAG));
                    }
                    else
                    { // set all the users if it is called from master maintainance
                        webCommon.ddlAdminFlag = ddlAdminFlag;
                    }

                    // Load the Work Flow combo box
                    // フロー名
                    List<M_WORKFLOW> dtFlow = ms.SelectWorkFlow();
                    webCommon.ddlFlowCd = repository.BindLookupValues(dtFlow);

                    if (callerIdentity == "New")
                    { // Its acutally creation of new user but back button is clicked so model data has to be retained and Registration button be enabled.
                        webCommon.TYPE = "Edit";
                        webCommon.CALLER_IDENTITY = callerIdentity;
                    }
                }
                else
                {
                    // Load the user info after Login and assign values of the user in the webcommon model class
                    if (usercd != null)
                    {
                        webCommon = repository.GetUserInfo(usercd, identityFlag);
                        webCommon.TYPE = "Edit";
                        webCommon.IDENTITY = identityFlag;
                    }
                    else if (identityFlag != null && usercd == null)
                    {
                        // do nothing. 
                    }
                    else
                    {
                        webCommon = repository.GetUserInfo(userID, identityFlag);
                        //  webCommon.TYPE = "Edit";
                    }
                }
                Session[SessionConst.MasterKeys] = webCommon.USER_CD;

            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            return View(webCommon);

        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-7-2019
        /// Description : For the Login Submit Action
        /// <summary>
        // To post values from User Master Maintainance screen from User settings or from Master Maintainance
        [HttpPost]
        public ActionResult ZC01022(WebCommonInfo webCommon)
        {
            if (Request.Form["btnBack_Click"] != null)
            {
                TempData["alertMessage"] = null;
                Session.Remove("NewReload");
                //return RedirectToAction("ZC01021", "ZC010", new { userCode = webCommon.USER_CD, userName = webCommon.USER_NAME, userNameRoman = webCommon.USER_NAME_KANA, batchFLAG = webCommon.BATCH_FLAG.Equals(true) ? "1" : "0", status = webCommon.DEL_FLAG.Equals(true)?"1":"0", IsPostBack = true });
                return RedirectToAction("ZC01021", "ZC010", new { IsPostBack = true });
            }

            // Get the User ID
            string userId = (string)Session[SessionConst.MasterKeys];
            // Registration from Master Maintainance
            if (Request.Form["btn_Registration"] != null)
            {
                List<string> result = InputCheck(webCommon);

                if (result.Count > 0)
                {
                    TempData["alertMessage"] = string.Join(SystemConst.ScriptLinefeed, result);
                    // get last dropdowns and return view
                    WebCommonInfo info = (WebCommonInfo)Session["NewReload"];
                    webCommon.ddlAdminFlag = info.ddlAdminFlag;
                    webCommon.ddlFlowCd = info.ddlFlowCd;
                    return View(webCommon);
                }
                // set all the records in session
                Session[SessionNameTransaction] = webCommon;
                // show preview screen for confirmation
                return RedirectToAction("ZC01024", "ZC010", new { userid = userId, identityFlag = "M_USER", type = "New" });
            }
            else if (Request.Form["btn_Initialize"] != null)
            {
                return RedirectToAction("ZC01022", "ZC010", new { usercd = userId, isInitialize = "1", identityFlag = "M_USER" });
            }
            else
            {
                // Update Operation
                List<string> result = InputCheck(webCommon);

                if (result.Count > 0)
                {
                    TempData["alertMessage"] = string.Join(SystemConst.ScriptLinefeed, result);
                    // get last dropdowns and return view
                    WebCommonInfo info = (WebCommonInfo)Session["NewReload"];
                    webCommon.ddlAdminFlag = info.ddlAdminFlag;
                    webCommon.ddlFlowCd = info.ddlFlowCd;
                    return View(webCommon);
                }

                // set all the records in session
                Session[SessionNameTransaction] = webCommon;
                // show preview screen
                if (webCommon.IDENTITY == "M_USER")
                {
                    return RedirectToAction("ZC01024", "ZC010", new { identityFlag = "M_USER", userid = userId });
                }

                return RedirectToAction("ZC01024", "ZC010", new { userid = userId });
            }
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 17-7-2019
        /// Description : To remove the User sessions.
        /// <summary>
        public void SessionRemove()
        {
            Session.Remove(SessionConst.MaintenanceType);
            Session.Remove(SessionConst.MasterKeys);
            Session.Remove(SessionConst.IsSelfSettings);
            Session.Remove(SessionConst.CategorySelect);
            Session.Remove(SessionConst.CategorySelectReturnValue);
            Session.Remove(SessionConst.CategoryTableName);
            Session.Remove(SessionConst.WebGrid);
            Session.Remove(SessionConst.WebGridCount);
        }

        public void SessionRemoveAll()
        {
            Session.RemoveAll();
            Session.Abandon();
            Session.Clear();
        }



        /// <summary>
        /// Author: MSCGI 
        /// Created date: 17-7-2019
        /// Description : To display the User Setting Confirmation screen from USER Settings Screen or from Master Maintainance module User Master screen.
        /// Right now all the user table values are in session, which got set in previous page ZC1022
        /// <summary>
        // show preview screen of user setting Maintainance screen with corresponding values pre-filled
        [Authorize(Roles = "0,1")]
        public ActionResult ZC01024(string userid, string identityFlag, string type)
        {
            WebCommonInfo webCommonInfo = new WebCommonInfo();
            try
            {
                if (Session[SessionNameTransaction] != null)
                {
                    webCommonInfo = (WebCommonInfo)Session[SessionNameTransaction];
                }
                //if it is called by master maintainance module
                if (identityFlag == "M_USER")
                {
                    webCommonInfo.IDENTITY = "M_USER";
                }
                if (webCommonInfo.DEL_FLG == true)
                {
                    webCommonInfo.DEL_FLAG = 1;
                    webCommonInfo.DEL_FLAGNAME = "削除済";
                }
                else
                {
                    webCommonInfo.DEL_FLAG = 0;
                    webCommonInfo.DEL_FLAGNAME = "使用中";
                }

                if (webCommonInfo.BATCH_FLG == false)
                {
                    webCommonInfo.BATCH_FLAG = "2";
                    webCommonInfo.BATCH_FLAGNAME = "対象";
                }
                else if (webCommonInfo.BATCH_FLG == true)
                {
                    webCommonInfo.BATCH_FLAG = "3";
                    webCommonInfo.BATCH_FLAGNAME = "対象外";
                }

                if (webCommonInfo.FLOW_CD != "" && webCommonInfo.FLOW_CD != null)
                {
                    MasterService ms = new MasterService();
                    List<M_WORKFLOW> dtFlow = ms.SelectWorkFlow();
                    webCommonInfo.ddlFlowCd = repository.BindLookupValues(dtFlow);
                    webCommonInfo.FLOW_NAME = repository.GetMessageById(webCommonInfo.ddlFlowCd, webCommonInfo.FLOW_CD);
                }
                if (webCommonInfo.ADMIN_FLAG != null && webCommonInfo.ADMIN_FLAG != "")
                {
                    webCommonInfo.ADMIN_FLAGNAME = WebCommonRepo.GetAdminFlagName(webCommonInfo.ADMIN_FLAG);
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            return View(webCommonInfo);
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 17-7-2019
        /// Description : Update User record in DB after confirmation in the user setting.
        /// <summary>
        [HttpPost]
        public ActionResult ZC01024(WebCommonInfo webCommonInfo, string identityFlag)
        {
            if (Request.Form["btnBack_Click"] != null)
            {
                if (webCommonInfo.IDENTITY != null || webCommonInfo.USER != null)
                {
                    if (webCommonInfo.IDENTITY != null && webCommonInfo.TYPE == "New")
                    {
                        // To maintain registration button even if user goes back to previous screen, since type is edit(to maintain session model values prefilled)
                        webCommonInfo.CALLER_IDENTITY = "New";
                        return RedirectToAction("ZC01022", "ZC010", new { identityFlag = "M_USER", type = "Edit", callerIdentity = "New" });
                    }
                    return RedirectToAction("ZC01022", "ZC010", new { identityFlag = "M_USER", type = "Edit" });
                }
                else
                {
                    // from user setting at dashboard
                    return RedirectToAction("ZC01022", "ZC010", new { type = "Edit" });
                }
            }
            string sql;
            DynamicParameters param = new DynamicParameters();

            bool result = false;
            try
            {
                // check the session whether it is null or not
                if (Session[SessionNameTransaction] != null)
                {
                    // assign the session into webcommon model object
                    webCommonInfo = (WebCommonInfo)Session[SessionNameTransaction];
                    if (webCommonInfo.IDENTITY == "M_USER")
                    {
                        if (webCommonInfo.TYPE == "Edit")
                        {
                            param.Add("@LOGIN_USER_CD", Common.GetLogonForm().Id);

                            UserMasterInfo master = new UserMasterInfo();
                            M_USER before = master.GetUser(webCommonInfo.USER_CD);
                            sql = repository.GetSqlUpdate(webCommonInfo, param);
                            // update into DB table
                            result = repository.DoSql(sql, param);
                            TempData["alertMessage"] = Common.GetMessage("I002");
                            M_USER after = master.GetUser(webCommonInfo.USER_CD);

                            // once management ledger will get integrated uncomment below lines
                            LedgerWorkHistoryDataInfo workflow = new LedgerWorkHistoryDataInfo();
                            workflow.UpdateUserChange(before, after);
                        }
                        else if (webCommonInfo.TYPE == "New")
                        {
                            sql = repository.GetSqlInsert(webCommonInfo, param);
                            result = repository.DoSql(sql, param);
                            TempData["alertMessage"] = Common.GetMessage("I001");
                        }
                        else if (webCommonInfo.TYPE == "Delete")
                        {
                            // 削除時
                            sql = repository.GetSqlDelete(webCommonInfo, param);
                            result = repository.DoSql(sql, param);
                            TempData["alertMessage"] = Common.GetMessage("I003");
                        }
                    }
                    else
                    {
                        // For User settings screen
                        param.Add("@LOGIN_USER_CD", Common.GetLogonForm().Id);
                        sql = repository.GetSqlUpdate(webCommonInfo, param);
                        // update into DB table
                        result = repository.DoSql(sql, param);
                    }
                    if (result)
                    {
                        // if query execution is successful then remove the session.
                        Session.Remove(SessionNameTransaction);
                        TempData["alertMessage"] = "更新しました。";
                        Session.Remove(SessionConst.WebGridCount);
                        Session.Remove(SessionConst.WebGrid);
                        if (identityFlag != null)
                        {
                            // redirect to User Master search page
                            return RedirectToAction("ZC01021", "ZC010", new { IsPostBack = true });
                        }
                        else
                        {
                            // redirect to Dashboard
                            return RedirectToAction("ZC01011", "ZC010");
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }

            return View(webCommonInfo);
        }


        /// <summary>
        /// Author: MSCGI 
        /// Created date: 18-7-2019
        /// Description : For the WebCommon popup screens of different tables with select types of single, multiple or terminal.
        /// <summary>
        [Authorize(Roles = "0,1")]
        public ActionResult ZC01081()
        {
            bool isClear = false;
            selectType = (SelectType)Convert.ToInt32(Request.QueryString.Get(QueryString.SelectTypeKey));
            CategorySelect categorySelect;

            string tableName = Request.QueryString[QueryString.TableNameKey];
            string columnName = Request.QueryString[QueryString.ColumnNameKey];
            string rowId = Request.QueryString.Get(QueryString.RowIdKey);
            string root = Request.QueryString.Get(QueryString.RootKey);
            string filter = Request.QueryString.Get(QueryString.FilterKey);
            string isDelete = Request.QueryString.Get(QueryString.isDelete);
            string classId = Request.QueryString.Get(QueryString.ClassId);

            if (!string.IsNullOrWhiteSpace(filter)) FilterIds.AddRange(filter.Split(SystemConst.CodeDelimiter.ToCharArray()[0]));

            string keyColumnName;
            string valueColumnName;
            string iconPathColumnName;
            string sortOrderColumnName;
            string classColumnName;
            string selFlag;
            try
            {
                // セッション変数から取得します。
                if (!WebCommonRepo.IsSelectButtonTarget(tableName, columnName, out keyColumnName, out valueColumnName, out iconPathColumnName, out sortOrderColumnName, out classColumnName, out selFlag)) return View();
                categorySelect = new CategorySelect(tableName, keyColumnName, valueColumnName, columnName, iconPathColumnName, sortOrderColumnName, selFlag, isDelete, classId, root);

                if (tableName == nameof(M_REG) || tableName == "M_GHSCAT" || tableName == "M_FIRE" || tableName == "M_OFFICE")
                {
                    categorySelect.CategoryItems.Where(x => x.UpperLevel == "1" || x.UpperLevel == "2" || x.UpperLevel == "3" || x.UpperLevel == "4").ToList().ConvertAll(x => x.UpperLevel = string.Empty);
                }

                if (tableName == nameof(M_STORING_LOCATION) || tableName == nameof(M_USAGE_LOCATION))
                {
                    categorySelect.CategoryItems.Where(x => x.UpperLevel == "1" || x.UpperLevel == "2").ToList().ConvertAll(x => x.UpperLevel = string.Empty);
                }

                if (FilterIds.Count > 0)
                {
                    List<Category> list = new List<Category>();
                    categorySelect.CategoryItems.Where(x => FilterIds.Contains(x.Key)).ToList().ForEach(x =>
                    {
                        repository.FilterAndCreateCategory(categorySelect.CategoryItems, list, x);
                    });
                    categorySelect.CategoryItems = list;
                };

                // セッション変数に格納します。
                Session[SessionConst.CategoryTableName] = tableName;
                Session[SessionConst.CategoryColumnName] = columnName;
                Session[SessionConst.CategorySelect] = categorySelect;
                Session[SessionConst.RowId] = rowId;
                Session[SessionConst.ClassId] = classId;

                selectKeys.select_keys = "";

                // 画面設定を行います。
                CreateTreeView(selectType);
                categorySelect = (CategorySelect)Session[SessionConst.CategorySelect];

                if (categorySelect.CategoryItems.Count > 0)
                {
                    List<string> selectedKeys = Request.QueryString.Get(QueryString.IdKey)?.Split(SystemConst.SelectionsDelimiter.ToCharArray()).ToList();
                    if (selectedKeys == null)
                    {
                        string sessionName = Request.QueryString.Get(QueryString.SessionNameKey);
                        selectedKeys = ((string)Session[sessionName])?.Split(SystemConst.SelectionsDelimiter.ToCharArray()).ToList();
                    }

                    string tablename = (string)Session[SessionConst.CategoryTableName];
                    if (tablename == "V_GROUP_USER")
                    {
                        selectedKeys = selectedKeys.ConvertAll(x => x = SystemConst.GroupUserUserCodePrefix + x);
                    }

                    if (selectedKeys == null || isClear)
                    {
                        selectedKeys = new List<string>();
                    }
                    else
                    {
                        selectKeys.select_keys = string.Join(",", selectedKeys);
                    }

                    StringBuilder script = new StringBuilder();
                    script.Append(" var treeData = [ \r\n");
                    repository.CreateTreeTag(ref script, categorySelect.CategoryItems, "", selectType, selectedKeys);
                    script.Append("    ];  \r\n");

                    switch (selectType)
                    {
                        case SelectType.One:
                        case SelectType.OneTerminal:
                            script.Append(Common.GetScriptRadioDyatreeonSelect("tree", "select_keys"));
                            selectKeys.select_keys = script.ToString();
                            break;
                        case SelectType.Multiple:
                            script.Append(Common.GetScriptCheckBoxDyatreeonSelect("tree", "select_keys"));
                            selectKeys.select_keys = script.ToString();
                            break;
                        case SelectType.MultipleTerminal:
                            script.Append(Common.GetScriptCheckBoxDyatreeonTerminalSelect("tree", "select_keys"));
                            selectKeys.select_keys = script.ToString();
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            // return the popup view options
            return View("ZC01081", selectKeys);
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 18-7-2019
        /// Description : Create the Tree View in the popup 
        /// <summary>
        private void CreateTreeView(SelectType selectType, bool isClear = false)
        {
            categorySelect = (CategorySelect)Session[SessionConst.CategorySelect];

            if (categorySelect.CategoryItems.Count > 0)
            {
                List<string> selectedKeys = Request.QueryString.Get(QueryString.IdKey)?.Split(SystemConst.SelectionsDelimiter.ToCharArray()).ToList();
                if (selectedKeys == null)
                {
                    string sessionName = Request.QueryString.Get(QueryString.SessionNameKey);
                    selectedKeys = ((string)Session[sessionName])?.Split(SystemConst.SelectionsDelimiter.ToCharArray()).ToList();
                }

                string tableNam = (string)Session[SessionConst.CategoryTableName];
                if (tableNam == "V_GROUP_USER")
                {
                    selectedKeys = selectedKeys.ConvertAll(x => x = SystemConst.GroupUserUserCodePrefix + x);
                }

                if (selectedKeys == null || isClear)
                {
                    selectedKeys = new List<string>();
                }
                else
                {
                    selectKeys.select_keys = string.Join(",", selectedKeys);
                }

                StringBuilder script = new StringBuilder();
                script.Append(" var treeData = [ \r\n");
                repository.CreateTreeTag(ref script, categorySelect.CategoryItems, "", selectType, selectedKeys);
                script.Append("    ];  \r\n");

                switch (selectType)
                {
                    case SelectType.One:
                    case SelectType.OneTerminal:
                        script.Append(Common.GetScriptRadioDyatreeonSelect("tree", "select_keys"));
                        selectKeys.select_keys = script.ToString();
                        break;
                    case SelectType.Multiple:
                        script.Append(Common.GetScriptCheckBoxDyatreeonSelect("tree", "select_keys"));
                        selectKeys.select_keys = script.ToString();
                        break;
                    case SelectType.MultipleTerminal:
                        script.Append(Common.GetScriptCheckBoxDyatreeonTerminalSelect("tree", "select_keys"));
                        selectKeys.select_keys = script.ToString();
                        break;
                }
            }
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 18-7-2019
        /// Description : Select the choices in the popup window.
        [HttpPost]
        public bool PopupSelect(string selectedKeys)
        {
            Dictionary<string, string> returnValue = new Dictionary<string, string>();
            try
            {
                // セッション変数から取得します。
                CategorySelect categorySelect = (CategorySelect)Session[SessionConst.CategorySelect];
                // 返却値を作成します。
                List<string> values;
                if (categorySelect.TableName == "M_COMMON")
                {
                    values = selectedKeys.Split(new string[] { ";" }, StringSplitOptions.None).ToList();
                }
                else
                {
                    values = selectedKeys.Split(new string[] { "," }, StringSplitOptions.None).ToList();
                }

                string tableName = (string)Session[SessionConst.CategoryTableName];
                if (tableName == "V_GROUP_USER")
                {
                    values = values.Where(x => !string.IsNullOrEmpty(x) && x.Substring(0, 1) == SystemConst.GroupUserUserCodePrefix)
                                   .Select(x => x.Substring(SystemConst.GroupUserUserCodePrefix.Length)).ToList();
                }

                int i = 0;
                foreach (string value in values)
                {
                    if (!string.IsNullOrWhiteSpace(value))
                    {
                        switch (selectType)
                        {
                            case SelectType.One:
                            case SelectType.OneTerminal:
                                returnValue.Add(categorySelect.UpperLevelColumnName, value);
                                break;
                            case SelectType.Multiple:
                            case SelectType.MultipleTerminal:
                                if (categorySelect.TableName == "M_COMMON")
                                {
                                    returnValue.Add(categorySelect.KeyColumnName + i.ToString(), value);
                                }
                                else
                                {
                                    returnValue.Add(categorySelect.UpperLevelColumnName + i.ToString(), value);
                                }
                                break;
                        }
                        i++;
                    }
                }
                string required = Request.QueryString.Get(QueryString.IsSelectRequired);
                if (returnValue.Count == 0 && required == "True")
                {
                    OutputWarningMessage(string.Format(WarningMessages.NotSelected, "対象"));
                }

                // セッション変数に格納します。
                Session.Add(SessionConst.CategorySelectReturnValue, returnValue);
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
                return false;
            }
            return true;
        }

        /// <summary>
        /// 警告メッセージを表示します。
        /// </summary>
        /// <param name="message">メッセージ本文。</param>
        public void OutputWarningMessage(string message)
        {
            //ClientScript.RegisterStartupScript(this.GetType(), "WarningMessage",
            //    "$(window).load(function(){ alert(\"" + message + "\"); });", true);
            CreateTreeView(selectType, true);
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 24-7-2019
        /// Description : Get the Approver Name in the Right side of the Superior and Agent textbox in user Setting
        /// <summary>
        [HttpPost]
        public string TxtApproverTextChanged(string UserCode)
        {
            UserInfo.ClsUser userInfo = null;
            string returnName = "";
            repository.GetUserMasterList();
            userInfo = repository.getUserList.SingleOrDefault(x => x.USER_CD == UserCode.Trim());
            // if name is available send it or send null
            if (userInfo != null)
            {
                returnName = userInfo.USER_NAME;
            }
            return returnName;
        }

        [HttpPost]
        public string btnOpenLog_Click()
        {
            string log = null;
            return log;
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 23-8-2019
        /// Description : User Master Search Screen.
        /// <summary>
        [Authorize(Roles = "0,1")]
        public ActionResult ZC01021(bool IsPostBack = false)
        {
            Session.Remove(SessionNameTransaction);
            WebCommonInfo muser = new WebCommonInfo();

            if (!IsPostBack)
            {
                // テーブル情報を取得します。
                List<S_TAB_CONTROL> tableControls = Control.GetTableControl();
                S_TAB_CONTROL tableInformation = tableControls.Single(x => x.TABLE_NAME == "M_USER");
                List<S_TAB_COL_CONTROL> tableColumnInformations = Control.GetTableColumnControl("M_USER");

                // セッション変数に格納します。
                Session[SessionConst.TableInformation] = tableInformation;
                Session[SessionConst.TableColumnInformations] = tableColumnInformations;
            }
            else
            {
                // back button 
                Session.Add(SessionConst.IsAdmin, true);

                if (Session[cSESSION_NAME_InputSearch] != null)
                {
                    muser = (WebCommonInfo)Session[cSESSION_NAME_InputSearch];
                }
                if (muser.ADMIN_FLAG != null && muser.ADMIN_FLAG != "")
                {
                    muser.ADMIN_FLAGNAME = WebCommonRepo.GetAdminFlagName(muser.ADMIN_FLAG);
                }
                if (muser.DEL_FLAG == 1)
                {
                    muser.DEL_FLAGNAME = "使用中";
                }
                else if (muser.DEL_FLAG == 2)
                {
                    muser.DEL_FLAGNAME = "削除済";
                }

                // if only previous search was done
                if (searchEnabled == 1 && Session[SessionConst.WebGridCount] == null)
                {
                    List<WebCommonInfo> result = DoSearch(muser, true);
                }

                Session.Remove(SessionConst.CategoryColumnName);
                Session.Remove(SessionConst.CategoryTableName);
                Session.Remove(SessionConst.RowId);
            }
            // It is normal loading of User Master search
            return View(muser);
        }

        private List<WebCommonInfo> DoSearch(WebCommonInfo muser, bool postBack)
        {
            string sql = null;
            DynamicParameters _params = new DynamicParameters();
            // Sql取得
            repository.GetSql(ref sql, ref _params, muser);
            List<WebCommonInfo> users = DbUtil.Select<WebCommonInfo>(sql, _params);
            // store the search results in session and viewbag to display in view
            if (users != null)
            {
                // ViewBag.DataCount = users.Count();
                Session[SessionConst.WebGridCount] = users.Count;
                Session[SessionConst.WebGrid] = users;
            }
            //ViewBag.userSearchResult = users;
            // ソートのために検索結果をsession変数に保持しておく
            return users;
        }


        public ActionResult ZC01021_Update(string user, string usrCD, string userName, string userKana, string groupName, string grpCD, string delFlg, string batchFlg)
        {
            WebCommonInfo muser = new WebCommonInfo();
            muser.IDENTITY = "M_USER";
            muser.USER = user;
            muser.USER_CD = usrCD == "" ? null : usrCD;
            muser.USER_NAME = userName == "" ? null : userName;
            muser.USER_NAME_KANA = userKana == "" ? null : userKana;
            muser.GROUP_NAME = groupName == "" ? null : groupName;
            muser.GROUP_CD = grpCD == "" ? null : grpCD;
            muser.DEL_FLAG = Convert.ToInt32(delFlg);
            muser.BATCH_FLAG = batchFlg;
            if (muser.DEL_FLAG == 1)
            {
                muser.DEL_FLG = true;
                muser.DEL_FLAGNAME = "削除済";
            }
            else
            {
                muser.DEL_FLG = false;
                muser.DEL_FLAGNAME = "使用中";
            }

            if (muser.BATCH_FLAG == "2")
            {
                muser.BATCH_FLG = false;
                muser.BATCH_FLAGNAME = "対象";
            }
            else if (muser.BATCH_FLAG == "3")
            {
                muser.BATCH_FLG = true;
                muser.BATCH_FLAGNAME = "対象外";
            }
            Session[cSESSION_NAME_InputSearch] = muser;
            return RedirectToAction("ZC01022", "ZC010", new { identityFlag = "M_USER", usercd = user, type = "Edit", isInitialize = "", callerIdentity = "" });
        }
        /// <summary>
        /// Author: MSCGI 
        /// Created date: 23-8-2019
        /// Description : User Master Post.
        /// <summary>
        [HttpPost]
        public ActionResult ZC01021(WebCommonInfo muser)
        {

            if (Request.Form["btnOutPutList_Click"] != null)
            {
                try
                {
                    DynamicParameters _params = new DynamicParameters();
                    string sql = null;

                    // Sql取得
                    repository.GetSqlForCsv(muser, ref sql, ref _params);
                    List<WebCommonInfo> info = DbUtil.Select<WebCommonInfo>(sql, _params);

                    string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                    ChemMgmt.Classes.Extensions.GridViewExtension.OutputCSV(info, csvFileFullPath);
                    string replaceCsvFileName = OUTLIST_FILENAME.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                    Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));

                }
                catch (Exception ex)
                {
                    _logoututil.OutErrLog(ProgramId, ex.ToString());
                }
            }
            else if (Request.Form["btnTemplateOutPutList_Click"] != null)
            {
                /// <summary>
                /// <para>データテーブルのヘッダ情報をCSVファイルに出力します。 </para>
                /// </summary>
                /// <param name="dt">CSVファイルに出力する情報</param>
                /// <param name="fileFullPath">CSVファイルパス</param>

                try
                {
                    DataTable dt = new DataTable();

                    List<S_TAB_COL_CONTROL> tableColumnInformations = (List<S_TAB_COL_CONTROL>)Session[SessionConst.TableColumnInformations];
                    IEnumerable<string> templateHeaders = tableColumnInformations.Where(x => !WebCommonRepo.IsDeleteColumn(x)).Select(x => x.DisplayNameAndRequiredMark);
                    dt.Columns.Add("*状態");
                    templateHeaders.ToList().ForEach(x => dt.Columns.Add(x));
                    dt.Columns.Add("*管理者権限");
                    dt.Columns.Add("削除者");
                    dt.Columns.Add("削除日時");
                    dt.Columns.Add("バッチ更新対象外");

                    string csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                    MasterMaintenance.Common.TemplateCSV(dt, csvFileFullPath);
                    string replaceCsvFileName = TEMPLATE_FILENAME.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                    Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                }
                catch (Exception ex)
                {
                    _logoututil.OutErrLog(ProgramId, ex.ToString());
                }
            }
            else if (Request.Form["btnRegist_Click"] != null)
            {
                // set all the records in session
                muser.TYPE = "New";
                Session[cSESSION_NAME_InputSearch] = muser;
                return RedirectToAction("ZC01022", "ZC010", new { identityFlag = "M_USER", type = "New" });
            }
            else if (Request.Form["btnBack_Click"] != null)
            {
                searchEnabled = 0;
                Session.Remove(SessionConst.WebGridCount);
                Session.Remove(SessionConst.WebGrid);
                Session.Remove(cSESSION_NAME_InputSearch);
                Session.Remove(SessionConst.TableColumnInformations);
                Session.Remove(SessionConst.TableInformation);
                TempData["alertMessage"] = null;
                return RedirectToAction("ZC01011", "ZC010");
            }
            else if (Request.Form["btnSearch_Click"] != null)
            {
                try
                {

                    DoSearch(muser, true);
                    searchEnabled = 1;

                }
                catch (Exception ex)
                {
                    _logoututil.OutErrLog(ProgramId, ex.ToString());
                }
            }
            else if (Request.Form["btnClear_Click"] != null)
            {
                searchEnabled = 0;
                Session.Remove(SessionConst.WebGridCount);
                Session.Remove(SessionConst.WebGrid);
                Session.Remove(cSESSION_NAME_InputSearch);
                return RedirectToAction("ZC01021", "ZC010");
            }
            else if (Request.Form["btnInclude_Click"] != null)
            {
                string serverFileName = "";
                string fileStatus = "";
                try
                {
                    HttpPostedFileBase posted = Request.Files[0];// uploadするファイル
                    string serverPath = Server.MapPath("../upload");    // uploadするフォルダ
                    // ファイルをuploadして uploadしたファイルのパスを取得
                    serverFileName = Common.UploadFile(posted, serverPath);
                }
                catch (Exception ex)
                {
                    _logoututil.OutErrLog(ProgramId, ex.ToString());
                }

                if (serverFileName != "")
                {
                    int importedCount = 0;
                    string errMessage = "";
                    repository.DoImport(serverFileName, out importedCount, out errMessage);

                    if (importedCount > 0)
                    {
                        fileStatus += importedCount.ToString() + "件が正常に取り込まれました。<br/><br/>";
                    }

                    if (errMessage != "")
                    {
                        fileStatus += "取り込みエラーが発生しました。<br/><br/>[詳細]<br/>" + errMessage;
                    }
                }
                ViewBag.FileStatus = fileStatus;
            }
            else
            {
                // set all the records in session and update user record from Master maintenance screen
                // muser.TYPE = "New";
                muser.IDENTITY = "M_USER";
                Session[cSESSION_NAME_InputSearch] = muser;
                return RedirectToAction("ZC01022", "ZC010", new { identityFlag = "M_USER", usercd = muser.USER });
            }
            return View(muser);
        }


        /// <summary>
        /// Author: MSCGI 
        /// Created date: 23-9-2019
        /// Description : Print Single User Barcode.
        /// <summary>
        [HttpPost]
        public string BarCodeLabelPrint(string txtUserCd, string txtUserName)
        {
            string retval = "";
            try
            {
                if (string.IsNullOrWhiteSpace(txtUserCd))
                {
                    retval = string.Format(WarningMessages.NotInput, "社員コード");
                    return retval;
                }
                PTouchLabelPrint labelPrint = new PTouchLabelPrint(LabelTypes.User);
                retval = labelPrint.CreatePrintJavaScript(new LabelData(txtUserCd, txtUserName));
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
                return retval;
            }
            return retval;
        }

        /// <summary>
        /// Author: MSCGI 
        /// Created date: 23-9-2019
        /// Description : Print Users Barcode.
        /// <summary>
        [HttpPost]
        public string BarCodeLabelPrintZC01021(string userCD, string userNM, string userKANA, string groupNM, string groupCD, string delFlag, string batchFlag)
        {
            string retval = "";
            try
            {
                WebCommonInfo userinfo = new WebCommonInfo();
                userinfo.USER_CD = userCD == "" ? null : userCD;
                userinfo.USER_NAME = userNM == "" ? null : userNM;
                userinfo.USER_NAME_KANA = userKANA == "" ? null : userKANA;
                userinfo.GROUP_NAME = groupNM == "" ? null : groupNM;
                userinfo.GROUP_CD = groupCD == "" ? null : groupCD;
                userinfo.DEL_FLAG = Convert.ToInt32(delFlag);
                userinfo.BATCH_FLAG = batchFlag;
                List<WebCommonInfo> info = DoSearch(userinfo, false);

                if (info != null)
                {
                    List<ILabelData> labelDataCollection = new List<ILabelData>();

                    // 検索結果からバーコードを取得します。
                    for (int i = 0; i < info.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(info[i].USER_CD))
                        {
                            labelDataCollection.Add(new LabelData(info[i].USER_CD, info[i].USER_NAME));
                        }
                    }
                    PTouchLabelPrint labelPrint = new PTouchLabelPrint(LabelTypes.User);
                    retval = labelPrint.CreatePrintJavaScript(labelDataCollection);
                }
                else
                {
                    retval = string.Format(WarningMessages.NotInput, "社員コード");
                    return retval;
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
                return retval;
            }
            return retval;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-7-2019
        /// Description : To display the Page not Found Error
        /// <summary>
        public ActionResult HttpError404(string message)
        {
            return View("Error", message);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 13-8-2019
        /// Description : To Open the Error Log and return message
        /// <summary>
        [HttpPost]
        public string ReturnErrorLog()
        {
            StreamReader sr = null;
            string message = "";
            StringBuilder sbLog = new StringBuilder();
            int i = 0;
            try
            {
                string strFileName = Common.webSiteRoot + "\\Log\\" + DateTime.Today.ToString("yyyyMMdd") + "_ZyCoaG.Web.log";
                sr = new StreamReader(strFileName, Encoding.GetEncoding("Shift_JIS"));
                while (sr.Peek() >= 0)
                {
                    i++;
                    sbLog.Append(sr.ReadLine() + "\r\n");
                }
                message = sbLog.ToString();
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(ProgramId, ex.ToString());
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    sr = null;
                }
            }
            return message;
        }

        public ActionResult ZC01090(string Message)
        {
            M_MASTER master = new M_MASTER();
            master.Message = Message;
            ViewBag.Exception = Message;
            //TempData["DebugMode"] = Message;
            return View(master);
        }

        [HttpPost]
        public JsonResult GetLedgerRegulationIdsDetails(string GroupCd)
        {
            List<string> data = new List<string>();
            var masterService = new MasterService();
            DataTable dt = masterService.SelectVGroup(GroupCd);
            if (dt.Rows.Count > 0)
            {
                data.Add(GroupCd);
                data.Add(Convert.ToString(dt.Rows[0]["GROUP_NAME"]));
                
            }            
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}

