﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Repository;
using Dapper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.BaseCommon;
using ZyCoaG.Classes.util;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.Stock;
using ZyCoaG.PrintLabel;
using ZyCoaG.util;
using ZyCoaG.Util;

namespace ChemMgmt.Controllers
{
    [Authorize]
    [HandleError]
    public class ZC020Controller : Controller
    {
        private UserInfo.ClsUser _clsUser = new UserInfo.ClsUser();
        InventoryManagement inventoryManagement = new InventoryManagement();
        IInventoryRepo inventoryRepo = new InventoryDao();
        private const string OUTLIST_STOCK_FILENAME = "在庫検索結果.csv";
        public const string PROGRAM_ID = "ZC020";
        LogUtil logutil = new LogUtil();

        /// <summary>
        /// Author: MSCGI
        /// Created date: 14-9-2019
        /// Description : This  Method is used to search the inventory
        /// <summary>
        public ActionResult ZC02002(string sortdir, string sort, string mode)
        {
            inventoryManagement = new InventoryManagement();
            try
            {
                if ((sortdir != null && sort != null) || mode != null)
                {
                    if (Session[SessionConst.CategorySelectReturnValue] != null)
                    {
                        inventoryManagement.labelMode = mode;
                    }
                    if (Session["SearchResult"] != null)
                    {
                        inventoryManagement.labelMode = mode;
                        inventoryManagement = (InventoryManagement)Session["SearchResult"];
                    }
                }
                else
                {
                    ClearSession();
                }
                inventoryManagement.labelMode = mode;
                inventoryManagement = DropDownFill(inventoryManagement);
            }
            catch (Exception ex)
            {

                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View(inventoryManagement);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 14-9-2019
        /// Description : This  Method is used to search the inventory details
        /// <summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ZC02002(InventoryManagement inventoryModel)
        {
            try
            {
                if (Request.Form["btnSearch_STOCK"] != null)
                {
                    string LocationSelection = Request.Form["LocationSelection"];
                    string RegulationSelection = Request.Form["RegulationSelection"];
                    if (LocationSelection != null)
                    {
                        inventoryModel.hdnLocationIds = LocationSelection;
                    }
                    if (RegulationSelection != null)
                    {
                        inventoryModel.hdnRegulationIds = RegulationSelection;
                    }
                    Session["SearchResult"] = null;
                    int? maxResultCount = null;
                    //if (Session["SearchResult"] != null)
                    //{
                    //    InventoryManagement inventorymodel = new InventoryManagement();
                    //    inventorymodel = (InventoryManagement)Session["SearchResult"];
                    //    inventoryModel = inventorymodel;
                    //}
                    //else
                    //{
                        inventoryModel.listInventoryManagement = inventoryRepo.getStock(inventoryModel, maxResultCount);
                        Session["SearchResult"] = inventoryModel;
                    //}
                }
                else if (Request.Form["btnClear_STOCK"] != null)
                {
                    Session["SearchResult"] = null;
                    return RedirectToAction("ZC02002", "ZC020");
                }
                else if (Request.Form["BtnBulkChangeStoringLocation"] != null)
                {
                    string BulkChangeValue = Request.Form["BulkchangeValue"];
                    if (BulkChangeValue != null)
                    {
                        return RedirectToAction("ZC02004", new { id = BulkChangeValue });
                    }
                }
                else if (Request.Form["btnPrint_STOCK"] != null)
                {
                    if (Session["SearchResult"] != null)
                    {
                        InventoryManagement inventoryManagement = new InventoryManagement();
                        inventoryManagement = (InventoryManagement)Session["SearchResult"];
                        if (inventoryManagement.listInventoryManagement != null && inventoryManagement.listInventoryManagement.Count > 0)
                        {
                            var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            if (inventoryManagement.listInventoryManagement != null & inventoryManagement.listInventoryManagement.Count > 0)
                            {
                                using (var csv = new StreamWriter(csvFileFullPath, false, Encoding.GetEncoding("shift_jis")))
                                {
                                    csv.Write(string.Empty);
                                    csv.Close();
                                }
                            }
                            OutputCSV(inventoryManagement.listInventoryManagement, csvFileFullPath);
                            string replaceCsvFileName = OUTLIST_STOCK_FILENAME.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                        }
                    }
                    else
                    {
                        InventoryManagement inventoryManagement = new InventoryManagement();
                        inventoryManagement.listInventoryManagement = inventoryRepo.getStock(inventoryModel, null);
                        if (inventoryManagement.listInventoryManagement != null && inventoryManagement.listInventoryManagement.Count > 0)
                        {
                            var csvFileFullPath = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName());
                            if (inventoryManagement.listInventoryManagement != null & inventoryManagement.listInventoryManagement.Count > 0)
                            {
                                using (var csv = new StreamWriter(csvFileFullPath, false, Encoding.GetEncoding("shift_jis")))
                                {
                                    csv.Write(string.Empty);
                                    csv.Close();
                                }
                            }
                            OutputCSV(inventoryManagement.listInventoryManagement, csvFileFullPath);
                            string replaceCsvFileName = OUTLIST_STOCK_FILENAME.Replace(NikonConst.CsvFileNameSuffix, NikonConst.CsvFileNameSuffixPlusTimeStamp);
                            Control.FileDownload(csvFileFullPath, string.Format(replaceCsvFileName, DateTime.Now.ToString("yyyyMMddHHmmss")));
                        }
                    }
                }
                inventoryModel = DropDownFill(inventoryModel);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View(inventoryModel);
        }


        [HttpPost]
        public JsonResult MaxCountResult(InventoryManagement inventoryModel)
        {
            try
            {
                var maxSearchCount = new SystemParameters().MaxStockResultCount;
                string LocationSelection = Request.Form["LocationSelection"];
                string RegulationSelection = Request.Form["RegulationSelection"];
                if (LocationSelection != null)
                {
                    inventoryModel.hdnLocationIds = LocationSelection;
                }
                if (RegulationSelection != null)
                {
                    inventoryModel.hdnRegulationIds = RegulationSelection;
                }
                Int64 StockCount = inventoryRepo.getStockCount(inventoryModel);               
                //inventoryModel.listInventoryManagement = inventoryRepo.getStock(inventoryModel, null);               
                //Session["SearchResult"] = inventoryModel;
                List<int> result = new List<int>();
                result.Add(maxSearchCount);
                result.Add(Convert.ToInt32(StockCount));
                result.Add(maxSearchCount);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exe)
            {
                logutil.OutErrLog(PROGRAM_ID, exe.ToString());
                return Json("0", JsonRequestBehavior.AllowGet);
            }

        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 15-9-2019
        /// Description : This  Method is used to search the inventory details
        /// <summary>
        public ActionResult ZC02021(int? StockId, string isEdit)
        {
            InventoryManagement inventoryModel = new InventoryManagement();
            try
            {
                List<ViewInventoryDetails> dt = inventoryRepo.getStockInformation(StockId);
                inventoryModel.listViewInventoryDetails = dt;
                inventoryModel.viewInventory = inventoryRepo.setupResult(dt);
                inventoryModel.vGhscat = inventoryRepo.GetGhsCategory(inventoryModel.viewInventory.CAT_CD);
                inventoryModel.vFire = inventoryRepo.GetFireCategory(inventoryModel.viewInventory.CAT_CD);
                inventoryModel.vRegulation = inventoryRepo.GetRegulation(inventoryModel.viewInventory.CAT_CD);
                inventoryModel.vOffice = inventoryRepo.GetOfficeCategory(inventoryModel.viewInventory.CAT_CD);
                inventoryModel = DropDownFill(inventoryModel);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View(inventoryModel);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019
        /// Description : This  Method is used to search the inventory details
        /// <summary>
        [HttpPost]
        public ActionResult ZC02021(InventoryManagement inventory)
        {
            ChemStockModel stock = new ChemStockModel();
            try
            {
                ChemStockDataInfo chemStock = new ChemStockDataInfo();
                string USER_CD = Common.GetLogonForm().Id;
                stock.STOCK_ID = Convert.ToInt32(inventory.StockId);
                stock.BARCODE = inventory.viewInventory.BARCODE;
                stock.REG_NO = inventory.viewInventory.REG_NO;
                stock.MAKER_ID = Convert.ToInt32(inventory.viewInventory.MAKER_ID);
                stock.UNITSIZE = inventory.viewInventory.UNITSIZE;
                stock.MEMO = inventory.viewInventory.MEMO;
                stock.UNITSIZE_ID = Convert.ToInt32(inventory.viewInventory.UNITSIZE_ID);
                stock.BRW_DATE = Convert.ToDateTime(inventory.viewInventory.BRW_DATE);
                stock.FIRST_LOC_ID = 1;
                stock.BORROWER = USER_CD;
                chemStock.Save(stock);
                inventoryManagement = DropDownFill(inventory);
                return RedirectToAction("ZC02002", "ZC020", new { mode = 2 });
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View(inventoryManagement);
        }


        public string ValidateDecimalDataValues(string unitsize)
        {
            string ErrorMessages = null;
            int integerNumberLength = 8;
            int decimalNumberLength = 3;
            string displayName = "容量";
            var numberArray = unitsize.Replace("-", string.Empty).Replace(",", string.Empty).Split(".".ToCharArray());
            try
            {
                //if (Memo != "")
                //{
                //    if (Memo.Length > 1000)
                //    {
                //        ErrorMessages = string.Format(WarningMessages.MaximumLength, displayName, integerNumberLength.ToString()) + "\n";
                //        return ErrorMessages;
                //    }
                //}
                if (integerNumberLength > 0 && numberArray[0].Length > integerNumberLength)
                {
                    ErrorMessages = string.Format(WarningMessages.PositiveNumberLength, displayName, integerNumberLength.ToString()) + "\n";
                }
                if (decimalNumberLength == 0 && numberArray.Length == 2)
                {
                    ErrorMessages = string.Format(WarningMessages.PositiveNumber, displayName) + "\n";
                }
                else if (numberArray.Length == 2 && numberArray[1].Length > decimalNumberLength)
                {
                    ErrorMessages = string.Format(WarningMessages.DecimalNumberLength, displayName, decimalNumberLength.ToString()) + "\n";
                }
                else
                {
                    var dec1 = default(decimal);
                    var dec2 = default(decimal);
                    var str1 = string.Empty;
                    var str2 = string.Empty;
                    if (unitsize.Length > 28)
                    {
                        str1 = unitsize.Substring(0, 28);
                        str2 = unitsize.Substring(28);
                    }
                    else
                    {
                        str1 = unitsize;
                        str2 = "0";
                    }
                    if (!string.IsNullOrWhiteSpace(unitsize) && (!decimal.TryParse(str1, out dec1) || !decimal.TryParse(str2, out dec2)))
                    {
                        ErrorMessages= string.Format(WarningMessages.NotNumber, displayName);
                    }
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return ErrorMessages;
        }
        /// <summary>
        /// Author: MSCGI
        /// Created date: 14-9-2019
        /// Description : This  Method is used for dropdown search
        /// <summary>     
        public InventoryManagement DropDownFill(InventoryManagement inventorymanagement)
        {
            try
            {

                inventorymanagement.LookUpAcceptCondition = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.ACCEPTCONDITION];
                inventorymanagement.LookUpAmountUnitSize = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.AMOUNTUNITSIZE];
                inventorymanagement.LookUpBottleNumCondition = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.BOTTLENUMCONDITION];
                inventorymanagement.LookUpCSCodeCondition = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.CSCODECONDITION];
                inventorymanagement.LookUpCSNCondition = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.CSNCONDITION];
                inventorymanagement.LookUpMakerName = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MAKERNAME];
                inventorymanagement.LookUpMakerNameCondition = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MAKERNAMECONDITION];
                inventorymanagement.LookUpStatus = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.STATUS];
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return inventorymanagement;
        }        

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019
        /// Description : This  Method is for get maker list
        /// <summary>
        public IEnumerable<CommonDataModel<int?>> GetMakerList() => new MakerMasterInfo().GetSelectList();

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019
        /// Description : This  Method is for get unitsize list
        /// <summary>
        public IEnumerable<CommonDataModel<int?>> GetUnitSizeList() => new UnitSizeMasterInfo().GetSelectList();       

        /// <summary>
        /// Author: MSCGI
        /// Created date: 16-9-2019
        /// Description : This  Method is for add stock inventory list details
        /// <summary>
        public void AddStockInventoryList(string StockId)
        {
            Session["StockValuesList"] = StockId;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019
        /// Description : This  Method is for add stock list id into session
        /// <summary>
        public ActionResult ZC02022()
        {
            InventoryManagement inventorymanagement = new InventoryManagement();
            ClsStockList clsStockList = new ClsStockList();
            string valueslist = "";
            try
            {
                if (Session["STOCK_LIST"] != null)
                {
                    clsStockList = (ClsStockList)Session["STOCK_LIST"];
                }
                if (Session["StockValuesList"] != null)
                {
                    valueslist = (string)Session["StockValuesList"];
                }

                foreach (string stockID in valueslist.Split(','))
                {
                    clsStockList.STOCK_ID.Add(stockID);
                }

                clsStockList.STOCK_ID = (from string stockID in clsStockList.STOCK_ID
                                         where stockID != ""
                                         select stockID).Distinct().ToList();

                Session["STOCK_LIST"] = clsStockList;

                string strArryStockId = "0";
                ClsStockList _ClsStockList = (ClsStockList)Session["STOCK_LIST"];


                for (int i = 0; i < _ClsStockList.STOCK_ID.Count; i++)
                {
                    strArryStockId += "," + _ClsStockList.STOCK_ID[i];
                }

                string strSQL = SQL.SELECT_T_STOCK.Replace("@STOCK_ID", strArryStockId);
                List<ViewInventoryDetails> dt = new List<ViewInventoryDetails>();
                dt = DbUtil.Select<ViewInventoryDetails>(strSQL, null);
                inventorymanagement.listViewInventoryDetails = dt;
                Session["ListInventoryViewDetails"] = inventorymanagement.listViewInventoryDetails;
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View(inventorymanagement);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019
        /// Description : This  Method is for sql queries
        /// <summary>
        struct SQL
        {
            /// <summary>
            /// 試薬バーコードよりT_STOCKの検索
            /// </summary>
            public static string SELECT_T_STOCK
            {
                get
                {
                    string sql = string.Empty;
                    if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
                    {
                        sql += "SELECT S.*,M.*,L.*,ST.STATUS_TEXT STATUS_NAME,to_char(S.UNITSIZE) + U.UNIT_NAME AS UNIT_NAME,S.CUR_GROSS_WEIGHT_G + 'g' AS CUR_GROSS_WEIGHT,to_char(S.DELI_DATE, 'yyyy/MM/dd') DELI_DATE_S,OU.USER_NAME,G.GHSCAT_NAME,R.REG_TEXT,F.FIRECAT_NAME,COF.OFFICECAT_NAME ";
                    }
                    if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
                    {
                        sql += "SELECT S.*,M.*,L.*,ST.STATUS_TEXT STATUS_NAME,format(S.UNITSIZE,'0.#########') + U.UNIT_NAME AS UNIT_NAME,convert(nvarchar,S.CUR_GROSS_WEIGHT_G - IsNull(S.BOTTLE_WEIGHT_G, 0)) + 'g' AS CUR_GROSS_WEIGHT,convert(nvarchar,S.DELI_DATE,111) DELI_DATE_S,OU.USER_NAME,G.GHSCAT_NAME,R.REG_TEXT,F.FIRECAT_NAME,COF.OFFICECAT_NAME ";
                    }
                    sql += "FROM T_STOCK S left outer join M_UNITSIZE U on S.UNITSIZE_ID = U.UNITSIZE_ID left outer join M_MAKER M on S.MAKER_ID = M.MAKER_ID ";
                    sql += "left outer join V_LOCATION L on S.LOC_ID = L.LOC_ID left outer join T_ORDER O on S.ORDER_NO = O.ORDER_NO left outer join M_USER OU on O.ORDER_USER_CD = OU.USER_CD ";
                    sql += "left outer join V_CHEM_GHSCAT G on S.CAT_CD = G.CHEM_CD left outer join V_CHEM_REGULATION R on S.CAT_CD = R.CHEM_CD ";
                    sql += "left outer join V_CHEM_FIRE F on S.CAT_CD = F.CHEM_CD left outer join V_CHEM_OFFICE COF on S.CAT_CD = COF.CHEM_CD ";
                    sql += "left outer join M_STATUS ST on S.STATUS_ID = ST.STATUS_ID ";
                    sql += "WHERE S.STOCK_ID IN (@STOCK_ID) ";
                    return sql;
                }
            }
        }
        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019
        /// Description : This  Method is for delete stock inventory list
        /// <summary>
        public ActionResult DeleteStockInventoryList(string StockId)
        {
            InventoryManagement inventorymanagement = new InventoryManagement();
            List<ViewInventoryDetails> dt = new List<ViewInventoryDetails>();
            ClsStockList clsStockList = new ClsStockList();
            try
            {
                if (Session["ListInventoryViewDetails"] != null)
                {
                    dt = (List<ViewInventoryDetails>)Session["ListInventoryViewDetails"];
                    foreach (string stockID in StockId.Split(','))
                    {
                        for (int stockid = 0; stockid < dt.Count; stockid++)
                        {
                            if (dt[stockid].STOCK_ID == stockID.Trim().ToString())
                            {
                                dt.RemoveAt(stockid);
                            }
                        }
                    }
                    if (dt.Count() > 0)
                    {
                        List<string> newlist = new List<string>();
                        foreach (var item in dt)
                        {
                            newlist.Add(item.STOCK_ID);
                        }
                        clsStockList.STOCK_ID = newlist;
                        Session["STOCK_LIST"] = clsStockList;
                        Session["StockValuesList"] = string.Join(", ", clsStockList.STOCK_ID.ToArray());
                        inventorymanagement.listViewInventoryDetails = dt;
                        Session["ListInventoryViewDetails"] = dt;
                    }
                    else
                    {
                        inventorymanagement.listViewInventoryDetails = new List<ViewInventoryDetails>();
                        Session["STOCK_LIST"] = null;
                        Session["StockValuesList"] = null;
                        Session["ListInventoryViewDetails"] = dt;
                    }
                }
            }
            catch (IndexOutOfRangeException ex)
            {
                ex.StackTrace.ToString();
            }
            catch (NullReferenceException ex)
            {
                ex.StackTrace.ToString();
            }
            catch (ArgumentNullException ex)
            {
                ex.StackTrace.ToString();
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return PartialView("_PartialViewInventoryDetails", inventorymanagement);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019
        /// Description : This  Method is for stock post action
        /// <summary>
        public bool btnControlStockClick(string StockId)
        {
            bool status = true;
            try
            {
                if (!Common.isAcceptedBeforePhase2(StockId, StockSearchTarget.barcode))
                {
                    if (!inventoryRepo.checkGroup(StockId, StockSearchTarget.barcode))
                    {
                        status = false;
                        return status;
                    }
                }

                Session.Add(SessionConst.Barcode, StockId);

            }
            catch (ThreadAbortException)
            {
                // 何も処理しません。
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return status;
        }

        public string CheckLegderExisitsForEdit(string StockId)
        {
            string Errorstatus = null;
            try
            {
                if (!Common.isAcceptedBeforePhase2(StockId.Trim(), StockSearchTarget.stockId))
                {
                    if (!inventoryRepo.checkGroup(StockId.Trim(), StockSearchTarget.stockId))
                    {
                        Errorstatus = string.Format(WarningMessages.NotRegistGroup, "編集");
                    }
                }

            }
            catch (ThreadAbortException)
            {
                // 何も処理しません。
            }

            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return Errorstatus;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019
        /// Description : This  Method is for chemstock search values add into list session
        /// <summary>
        public ActionResult ZC02004(string id)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            ChemStockBulkChangekSearchModel condition = new ChemStockBulkChangekSearchModel();
            try
            {
                condition.LOGIN_USER_CD = Common.GetLogonForm().User_CD;
                condition.REFERENCE_AUTHORITY = Common.GetLogonForm().REFERENCE_AUTHORITY;
                if (id != "")
                {
                    var code = getSelectStock(id);
                    condition.BARCODE = string.Join(SystemConst.SelectionsDelimiter, code);
                }
                else
                {
                    condition.BARCODE = id;
                }
                ChemStockBulkChangeDataInfo chemStockData = new ChemStockBulkChangeDataInfo();
                var chemStockDataViewInfo = chemStockData.GetSearchResult(condition);
                inventoryManagement.listBulkChangeInventoryDetails = chemStockDataViewInfo.ToList();
                Session[nameof(ChemStockBulkChangekSearchModel)] = inventoryManagement.listBulkChangeInventoryDetails;
                chemStockData.Dispose();
                inventoryManagement.StockId = id;             
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return View(inventoryManagement);

        }

        private IEnumerable<string> getSelectStock(string Barcode)
        {
            var selectionCode = new List<string>();
            foreach (var items in Barcode.Split(','))
            {               
                    selectionCode.Add("'" + items + "'");                
            }
            return selectionCode;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019
        /// Description : This  Method is for get loction name
        /// <summary>
        public string getLocationName(string locId)
        {
            StringBuilder stbSql = new StringBuilder();
            try
            {
                stbSql.Append("select ");
                stbSql.Append("  LOC_NAME ");
                stbSql.Append("from ");
                stbSql.Append("  M_STORING_LOCATION ");
                stbSql.Append("where ");
                stbSql.Append(" LOC_ID= @LOC_ID ");
                stbSql.Append(" AND DEL_FLAG <> 1 ");
                stbSql.Append("order by ");
                stbSql.Append(" LOC_ID ");
                DynamicParameters _params = new DynamicParameters();
                _params.Add("LOC_ID", locId);
                List<string> locname = DbUtil.Select<string>(stbSql.ToString(), _params);
                if (locname.Count > 0)
                {
                    return locname[0].ToString();
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return null;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019
        /// Description : This  Method is for search the chem search
        /// <summary>         
        [HttpPost]
        public ActionResult ZC02004(InventoryManagement inventoryManagement)
        {
            ChemStockBulkChangeDataInfo chemStockData = new ChemStockBulkChangeDataInfo();
            ChemStockBulkChangekSearchModel condition = new ChemStockBulkChangekSearchModel();
            string LOC_ID = null;
            string loc_name = null;
            try
            {
                if (Request.Form["StoringSelection"] != null)
                {
                    LOC_ID = Request.Form["StoringSelection"];
                }
                if (Request.Form["StoringLocationName"] != null)
                {
                    loc_name = Request.Form["StoringLocationName"];
                }
                inventoryManagement.LocationID = LOC_ID;
                inventoryManagement.LocationName = loc_name;

                List<ChemStockBulkChangeModel> ChemStockBulkChange = (List<ChemStockBulkChangeModel>)Session[nameof(ChemStockBulkChangekSearchModel)];
                if (string.IsNullOrEmpty(LOC_ID))
                {
                    TempData["AlertMessage"] = string.Format(WarningMessages.NotSelected, "保管場所");
                }

                for (int i = 0; i < ChemStockBulkChange.Count; i++)
                {
                    ChemStockBulkChangeModel chemStock = new ChemStockBulkChangeModel();
                    chemStock.BORROWER = Common.GetLogonForm().User_CD;
                    chemStock.BARCODE = ChemStockBulkChange[i].BARCODE;
                    chemStock.REG_NO = ChemStockBulkChange.Single(x => x.BARCODE == ChemStockBulkChange[i].BARCODE).REG_NO;
                    if (LOC_ID != "")
                    {
                        chemStock.LOC_ID = Convert.ToInt32(LOC_ID);
                    }
                    chemStock.LOC_NAME = loc_name;
                    chemStock.FIRST_LOC_ID = ChemStockBulkChange.Single(x => x.BARCODE == ChemStockBulkChange[i].BARCODE).FIRST_LOC_ID;
                    chemStock.STOCK_ID = ChemStockBulkChange.Single(x => x.BARCODE == ChemStockBulkChange[i].BARCODE).STOCK_ID;
                    var locations = new LocationMasterInfo(LocationClassId.StoringLedger, chemStock.REG_NO);
                    if (LOC_ID != "")
                    {
                        if (locations.GetData(Convert.ToInt32(LOC_ID)) != null)
                        {
                            chemStock = DoUpdateForProcess(1, chemStock);
                            ChemStockBulkChange[i].CHANGE_RESULT = "変更済";
                            ChemStockBulkChange[i].LOC_NAME = chemStock.LOC_NAME;
                        }
                        else
                        {
                            ChemStockBulkChange[i].CHANGE_RESULT = "エラー";
                        }
                    }
                }
                inventoryManagement.listBulkChangeInventoryDetails = ChemStockBulkChange.ToList();             
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return View(inventoryManagement);
        }
        /// <summary>
        /// Author: MSCGI
        /// Created date: 18-9-2019
        /// Description : This  Method is for update the chem search values
        /// <summary>
        /// 
        public ChemStockBulkChangeModel DoUpdateForProcess(int updateType, ChemStockBulkChangeModel value)
        {
            var context = DbUtil.Open(true);
            try
            {
                using (ChemStockBulkChangeDataInfo ChemStockInfo = new ChemStockBulkChangeDataInfo() { Context = context })
                {
                    ChemStockInfo.Save(value);
                    if (ChemStockInfo.IsValid)
                    {
                        value.CHANGE_RESULT = "変更済";
                        value.LOC_NAME = value.LOC_NAME;
                    }
                    else
                    {
                        value.CHANGE_RESULT = "変更済";
                    }
                }
                //context.Commit();
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            finally
            {
                context.Rollback();
                context.Close();
            }
            return value;
            //ClearSession();
            //afterUpdateType = updateType;
        }


        [HttpPost]
        public string BarCodeLabelPrint(string Barcode)
        {
            string retval = "";
            var dataCollection = new List<ILabelData>();
            InventoryManagement inventory = new InventoryManagement();
            try
            {

                var systemParameters = new SystemParameters();
                if (Barcode != null && Barcode != "")
                {
                    if (Session["SearchResult"] != null)
                    {
                        inventory = (InventoryManagement)Session["SearchResult"];

                        foreach (var chkCount in Barcode.Split(','))
                        {
                            foreach (var invetory in inventory.listInventoryManagement)
                            {
                                if (chkCount.Trim() == invetory.STOCK_ID.Trim())
                                {
                                    var data = invetory.BARCODE;
                                    var name = invetory.PRODUCT_NAME;
                                    if (!string.IsNullOrEmpty(data?.ToString()))
                                    {
                                        dataCollection.Add(new LabelData(data, name));
                                    }
                                }
                            }
                        }
                    }
                }

                if (dataCollection.Count == 0)
                {
                    retval = string.Format(WarningMessages.NotSelected, "対象");
                }
                else
                {
                    var labelPrint = new PTouchLabelPrint(LabelTypes.Bottle);
                    retval = labelPrint.CreatePrintJavaScript(dataCollection);
                }
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
                return retval;
            }
            return retval;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 20-9-2019
        /// Description : This  Method is for generating List output
        /// <summary>
        public static void OutputCSV(List<InventoryManagement> g, string fileFullPath)
        {
            try
            {
                using (var csv = new StreamWriter(fileFullPath, true, Encoding.GetEncoding("shift_jis")))
                {
                    for (int I = 0; I < g.Count; I++)
                    {
                        if (I == 0)
                        {
                            csv.WriteLine("化学物質名" + "," + "瓶番号" + "," + "メーカー名" + "," + "容量" + "," + "容量単位" + "," + "化学物質コード" + "," + "CAS＃" + "," + "受入者" + "," + "受入者コード" + "," + "受入日" + "," + "残量（内容量）" + "," + "残量単位" + "," + "在庫ステータス" + "," + "保管場所" + "," + "法規制" + "," + "消防法" + "," + "社内ルール" + "," + "GHS分類");
                            csv.WriteLine("\"" + g[I].PRODUCT_NAME + "\"" + "," + "\"" + g[I].BARCODE + "\"" + "," + "\"" + g[I].MAKER_NAME + "\"" + "," + "\"" + g[I].UNITSIZE + "\"" + "," + "\"" + g[I].UNIT_NAME + "\"" + "," + "\"" + g[I].CAT_CD + "\"" + "," + "\"" + g[I].CAS_NO + "\"" + "," + "\"" + g[I].ORDER_USER + "\"" + "," + "\"" + g[I].USER_NAME + "\"" + "," + "\"" + g[I].DELI_DATE + "\"" + "," + "\"" + g[I].CONTENT_WEIGHT_VOLUME + "\"" + "," + "\"" + g[I].CONTENT_WEIGHT_UNITSIZE + "\"" + "," + "\"" + g[I].STATUS_NAME + "\"" + "," + "\"" + g[I].STORING_LOC_NAME + "\"" + "," + "\"" + g[I].REGULATORY + "\"" + "," + "\"" + g[I].FIRE_NAME + "\"" + "," + "\"" + g[I].OFFICE_NAME + "\"" + "," + "\"" + g[I].GHSCAT + "\"");

                        }
                        else
                        {
                            csv.WriteLine("\"" + g[I].PRODUCT_NAME + "\"" + "," + "\"" + g[I].BARCODE + "\"" + "," + "\"" + g[I].MAKER_NAME + "\"" + "," + "\"" + g[I].UNITSIZE + "\"" + "," + "\"" + g[I].UNIT_NAME + "\"" + "," + "\"" + g[I].CAT_CD + "\"" + "," + "\"" + g[I].CAS_NO + "\"" + "," + "\"" + g[I].ORDER_USER + "\"" + "," + "\"" + g[I].USER_NAME + "\"" + "," + "\"" + g[I].DELI_DATE + "\"" + "," + "\"" + g[I].CONTENT_WEIGHT_VOLUME + "\"" + "," + "\"" + g[I].CONTENT_WEIGHT_UNITSIZE + "\"" + "," + "\"" + g[I].STATUS_NAME + "\"" + "," + "\"" + g[I].STORING_LOC_NAME + "\"" + "," + "\"" + g[I].REGULATORY + "\"" + "," + "\"" + g[I].FIRE_NAME + "\"" + "," + "\"" + g[I].OFFICE_NAME + "\"" + "," + "\"" + g[I].GHSCAT + "\"");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogUtil logUtil = new LogUtil();
                ex.StackTrace.ToString();
                logUtil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
        }

        public void ClearSession()
        {
            Session["STOCK_LIST"] = null;
            Session["StockValuesList"] = null;
            Session["SearchResult"] = null;
            Session["ListInventoryViewDetails"] = null;
            Session.Remove(SessionConst.Barcode);
            Session[nameof(ChemStockBulkChangekSearchModel)] = null;
        }
    }
}