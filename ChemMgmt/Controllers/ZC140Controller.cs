﻿using ChemMgmt.Classes;
using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Flow;
using ChemMgmt.Nikon.Ledger;
using ChemMgmt.Nikon.Models;
using ChemMgmt.Repository;
using COE.Common.BaseExtension;
using IdentityManagement.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.BaseCommon;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.History;
using ZyCoaG.Util;
using ZyCoaG.Classes.util;
using System.Collections;
using ChemMgmt.Nikon.History;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Configuration;
using Dapper;
using static ChemMgmt.Models.LedgerCommonInfo;
using COE.Common;
using ZyCoaG.util;
namespace ChemMgmt.Controllers
{
    [HandleError]
    public class ZC140Controller : Controller
    {
        delegate void download(LedgerSearchModel cond, dynamic dataInfo);
        /// <summary>
        /// 保護具'sのセッション名を定義します。
        /// </summary>
        private const string sessionProtectors = "Protectors";

        /// <summary>
        /// リスク低減策のセッション名を定義します。
        /// </summary>
        private const string sessionRiskReduces = "RiskReduces";


        /// セッション変数に格納する旧安全衛生番号の名前を定義します。
        /// </summary>
        private const string sessionOldRegisterNumbers = "OldRegisterNumbers";

        private string sessionAfterUpdateType { get; } = "AfterUpdateType";

        /// <summary>
        /// セッション変数に格納する保管場所一覧の名前を定義します。
        /// </summary>
        private const string sessionStoringLocations = "StoringLocations";

        /// <summary>
        /// 適用法令のセッション名を定義します。
        /// </summary>
        private const string sessionAccesmentReg = "AccesmentReg";

        /// <summary>
        /// セッション変数に格納する法規制ID's(画面表示用)の名前を定義します。
        /// </summary>
        private const string sessionRegTypeIds = "RegTypeIds";

        /// <summary>
        /// セッション変数に格納する化学物質に紐付く法規制ID's(画面表示用でない)の名前を定義します。
        /// </summary>
        private const string sessionChemRegulations = "ChemRegulations";

        /// <summary>
        /// セッション変数に格納する法規制ID's(画面表示用)の名前を定義します。
        /// </summary>

        public const string sessionUsageLocations = "UsageLocations";

        private const string sessionRegulations = "Regulations";

        private const string sessionChemRegTypeIds = "ChemRegTypeIds";

        private const string sqlHazardous = "'第一類','第二類','第三類','第四類','第五類','第六類'";
        /// <summary>
        /// 危険物セットの名前を定義します。
        /// </summary>
        private decimal TOTAL_MARKS = 0;
        private string rowId { get; set; }
        protected Mode mode { get; set; }
        private TableType type { get; set; }

        LedgerCommonInfo legderCommonInfo = new LedgerCommonInfo();
        IMangementLedgerRepo objIMangementLedgerRepo = new MangementLedegerDao();
        private string sessionContaineds = "Containeds";
        private LogUtil _logoututil = new LogUtil();
        private const string programId = "ZC140";
        protected string SearchConditionCsv { get; set; } = string.Empty;
        protected bool IsListOutputProcess { get; set; } = false;
        protected int MaxSearchResultCount { get; set; }
        protected bool IsMaxSerchResultCountOver { get; set; } = false;
        int MaxBulkChangeCount = new SystemParameters().MaxBulkChangeCount;
        private const string SelectModels = "ZC14040-SelectModels";
        private const string ChangeValue = "ZC14040-ChangeValue";
        private string id { get; set; }
        private string regno { get; set; }
        private bool updFlg = true;

        /// <summary>
        /// 更新の種類
        /// </summary>
        public enum UpdateType
        {
            /// <summary>
            /// 登録
            /// </summary>
            Register,

            /// <summary>
            /// 更新
            /// </summary>
            Update,

            ///// <summary>
            ///// 抹消
            ///// </summary>
            //Remove,

            ///// <summary>
            ///// 抹消取消
            ///// </summary>
            //UndoRemove,

            /// <summary>
            /// 一時保存 (登録)
            /// </summary>
            TemporaryRegister,

            /// <summary>
            /// 一時保存 (更新)
            /// </summary>
            TemporaryUpdate,

            /// <summary>
            /// 廃止申請
            /// </summary>
            Abolition,

            /// <summary>
            /// 取り下げ 
            /// </summary>
            Withdraw,

            /// <summary>
            /// 取戻
            /// </summary>
            Regain,

            /// <summary>
            /// 差し戻し
            /// </summary>
            Remand,

            /// <summary>
            /// 承認
            /// </summary>
            Approval,

        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 26-11-2019  
        /// Description :Comes Back To the Dashboard
        /// <summary>
        public ActionResult SearchBack()
        {
            try
            {
                Session["SessionList"] = null;

                Session.Remove("SessionList");

            }
            catch (Exception exe)
            {
                exe.StackTrace.ToString();
            }
            return RedirectToAction("ZC01011", "ZC010");
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 12-8-2019
        /// Description : Ajax Call To get the MaxSearch Result Count
        /// <summary>

        [HttpPost]
        public JsonResult MaxCountResult(LedgerSearchModel objLedgerSearchModel)
        {
            try
            {
              
                List<LedgerCommonInfo> objLedgerCommonInfo = objIMangementLedgerRepo.searchEntity(objLedgerSearchModel).ToList();

                Session["SessionList"] = objLedgerCommonInfo;
                Session["formdata"] = objLedgerSearchModel;
                ViewBag.Result = objLedgerCommonInfo;
                int count = objLedgerCommonInfo.Count;
                int totalCount = Convert.ToInt32(Session["totalMaxSearchResultCount"]);
                int maxSearchCount = Convert.ToInt32(Session["MaxSearchCount"]);
                List<int> result = new List<int>();
                result.Add(count);
                result.Add(totalCount);
                result.Add(maxSearchCount);
                return Json(result, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
                return Json("0", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult ZC14010(string mode)
        {
            try
            {
                string adminFlag = "";
                WebCommonInfo webCommon = new WebCommonInfo();

                if (Session["LoginUserSession"] != null)
                {
                    ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];
                    adminFlag = appUser.ADMIN_FLAG;
                }
                else
                {
                    return RedirectToAction("ZC01001", "ZC010");
                }
                LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
                legderCommonInfo = new LedgerCommonInfo();
                legderCommonInfo = ddlLedgerList(legderCommonInfo);
                legderCommonInfo.ChemCommonList = new List<LedgerCommonInfo>();

                if (mode == "5")
                {
                    LedgerSearchModel objLedgerSearchModel = new LedgerSearchModel();

                    if (Session["formdata"] != null)
                    {
                        objLedgerSearchModel = (LedgerSearchModel)Session["formdata"];
                    }
                    legderCommonInfo.ChemCommonList = objIMangementLedgerRepo.searchEntity(objLedgerSearchModel).ToList();
                }
                else if (Session["SessionList"] != null)
                {
                    legderCommonInfo.ChemCommonList = (List<LedgerCommonInfo>)Session["SessionList"];
                    LedgerSearchModel objLedgerSeachmodel = (LedgerSearchModel)Session["formdata"];
                    legderCommonInfo.LEDGER_STATUS = objLedgerSeachmodel.LEDGER_STATUS;
                    legderCommonInfo.LEDGER_FLOW_ID = objLedgerSeachmodel.LEDGER_FLOW_ID;
                    legderCommonInfo.CHEM_NAME = objLedgerSeachmodel.CHEM_NAME;
                    legderCommonInfo.PRODUCT_NAME = objLedgerSeachmodel.PRODUCT_NAME;
                    legderCommonInfo.CAS_NO = objLedgerSeachmodel.CAS_NO;
                    legderCommonInfo.REG_NO = objLedgerSeachmodel.REG_NO;
                    legderCommonInfo.CHEM_CD = objLedgerSeachmodel.CHEM_CD;
                    legderCommonInfo.CHEM_OPERATION_CHIEF = objLedgerSeachmodel.CHEM_OPERATION_CHIEF;
                    legderCommonInfo.CHEM_OPERATION_CHIEF_NAME = objLedgerSeachmodel.CHEM_OPERATION_CHIEF_NAME;
                    legderCommonInfo.CHEM_NAME_CONDITION = objLedgerSeachmodel.CHEM_NAME_CONDITION;
                    legderCommonInfo.PRODUCT_NAME_CONDITION = objLedgerSeachmodel.PRODUCT_NAME_CONDITION;
                    legderCommonInfo.CAS_NO_CONDITION = objLedgerSeachmodel.CAS_NO_CONDITION;
                    legderCommonInfo.REG_NO_CONDITION = objLedgerSeachmodel.REG_NO_CONDITION;
                    legderCommonInfo.REG_COLLECTION = objLedgerSeachmodel.REG_COLLECTION;
                    legderCommonInfo.GROUP_CD_COLLECTION = objLedgerSeachmodel.GROUP_CD_COLLECTION;
                    legderCommonInfo.ACTION_LOC_COLLECTION = objLedgerSeachmodel.ACTION_LOC_COLLECTION;
                    legderCommonInfo.USAGE_LOC_COLLECTION = objLedgerSeachmodel.USAGE_LOC_COLLECTION;
                    legderCommonInfo.hiddenRegulationIds = objLedgerSeachmodel.REG_SELECTION;
                    legderCommonInfo.managementgroupIds = objLedgerSeachmodel.GROUP_CD_SELECTION;
                    legderCommonInfo.hiddenLocationIds = objLedgerSeachmodel.ACTION_LOC_SELECTION;
                    legderCommonInfo.hiddenusageIds = objLedgerSeachmodel.USAGE_LOC_SELECTION;

                    if (objLedgerSeachmodel.REG_COLLECTION != null && objLedgerSeachmodel.REG_COLLECTION.Count() > 0)
                    {
                        legderCommonInfo.lblalreadySetValue = "設定済み";
                    }

                    if (objLedgerSeachmodel.GROUP_CD_COLLECTION != null && objLedgerSeachmodel.GROUP_CD_COLLECTION.Count() > 0)
                    {
                        legderCommonInfo.lblalreadySetValue = "設定済み";
                    }

                    if (objLedgerSeachmodel.ACTION_LOC_COLLECTION != null && objLedgerSeachmodel.ACTION_LOC_COLLECTION.Count() > 0)
                    {
                        legderCommonInfo.lblalreadySetValue = "設定済み";
                    }

                    if (objLedgerSeachmodel.USAGE_LOC_COLLECTION != null && objLedgerSeachmodel.USAGE_LOC_COLLECTION.Count() > 0)
                    {
                        legderCommonInfo.lblalreadySetValue = "設定済み";
                    }
                }
                else
                {
                    legderCommonInfo.ChemCommonList = new List<LedgerCommonInfo>();
                }
                Session["MaxSearchResultCount"] = null;
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return View(legderCommonInfo);
        }

        [HttpPost]
        public ActionResult ZC14010(LedgerSearchModel objLedgerSearchModel)
        {
            LedgerCommonInfo legderCommonInfo = new LedgerCommonInfo();
            try
            {
                legderCommonInfo.ChemCommonList = (List<LedgerCommonInfo>)Session["SessionList"];
                legderCommonInfo = ddlLedgerList(legderCommonInfo);
                legderCommonInfo.CHEM_NAME_CONDITION = objLedgerSearchModel.CHEM_NAME_CONDITION;
                legderCommonInfo.CHEM_NAME = objLedgerSearchModel.CHEM_NAME;
                legderCommonInfo.PRODUCT_NAME_CONDITION = objLedgerSearchModel.PRODUCT_NAME_CONDITION;
                legderCommonInfo.PRODUCT_NAME = objLedgerSearchModel.PRODUCT_NAME;
                legderCommonInfo.CAS_NO_CONDITION = objLedgerSearchModel.CAS_NO_CONDITION;
                legderCommonInfo.CAS_NO = objLedgerSearchModel.CAS_NO;
                legderCommonInfo.REG_NO = objLedgerSearchModel.REG_NO;
                legderCommonInfo.REG_NO_CONDITION = objLedgerSearchModel.REG_NO_CONDITION;
                legderCommonInfo.CHEM_CD = objLedgerSearchModel.CHEM_CD;
                legderCommonInfo.CHEM_OPERATION_CHIEF = objLedgerSearchModel.CHEM_OPERATION_CHIEF;
                legderCommonInfo.CHEM_OPERATION_CHIEF_NAME = null;

                if (objLedgerSearchModel.CHEM_OPERATION_CHIEF != null)
                {
                    var chiefname = objIMangementLedgerRepo.getchemoerationchiefname(objLedgerSearchModel.CHEM_OPERATION_CHIEF);
                    legderCommonInfo.CHEM_OPERATION_CHIEF_NAME = chiefname;
                }
                legderCommonInfo.LEDGER_STATUS = objLedgerSearchModel.LEDGER_STATUS;
                legderCommonInfo.REG_COLLECTION = objLedgerSearchModel.REG_COLLECTION;
                legderCommonInfo.GROUP_CD_COLLECTION = objLedgerSearchModel.GROUP_CD_COLLECTION;
                legderCommonInfo.ACTION_LOC_COLLECTION = objLedgerSearchModel.ACTION_LOC_COLLECTION;
                legderCommonInfo.USAGE_LOC_COLLECTION = objLedgerSearchModel.USAGE_LOC_COLLECTION;

                if (objLedgerSearchModel.ApplicationNo != null)
                {
                    legderCommonInfo.LEDGER_FLOW_ID = Convert.ToInt32(objLedgerSearchModel.ApplicationNo.TrimStart(new Char[] { '0' }));
                }
                legderCommonInfo.hiddenRegulationIds = objLedgerSearchModel.REG_SELECTION;
                legderCommonInfo.managementgroupIds = objLedgerSearchModel.GROUP_CD_SELECTION;
                legderCommonInfo.hiddenLocationIds = objLedgerSearchModel.ACTION_LOC_SELECTION;
                legderCommonInfo.hiddenusageIds = objLedgerSearchModel.USAGE_LOC_SELECTION;

                if (objLedgerSearchModel.REG_COLLECTION != null && objLedgerSearchModel.REG_COLLECTION.Count() > 0)
                {
                    legderCommonInfo.lblalreadySetValue = "設定済み";
                }

                if (objLedgerSearchModel.GROUP_CD_COLLECTION != null && objLedgerSearchModel.GROUP_CD_COLLECTION.Count() > 0)
                {
                    legderCommonInfo.lblalreadySetValue = "設定済み";
                }

                if (objLedgerSearchModel.ACTION_LOC_COLLECTION != null && objLedgerSearchModel.ACTION_LOC_COLLECTION.Count() > 0)
                {
                    legderCommonInfo.lblalreadySetValue = "設定済み";
                }

                if (objLedgerSearchModel.USAGE_LOC_COLLECTION != null && objLedgerSearchModel.USAGE_LOC_COLLECTION.Count() > 0)
                {
                    legderCommonInfo.lblalreadySetValue = "設定済み";
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return View(legderCommonInfo);
        }

        [HttpGet]
        public ActionResult ZC14020(int? Id, int? mode, string Regno, TableType? type, int? flowids, int? flowmodes)
        {
            string adminFlag = "";
            Mode modeval = new Mode();
            if (mode != null)
            {
                modeval = (Mode)mode;
            }
            WebCommonInfo webCommon = new WebCommonInfo();
            if (Session["LoginUserSession"] != null)
            {
                ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];
                adminFlag = appUser.ADMIN_FLAG;
            }
            else
            {
                return RedirectToAction("ZC01001", "ZC010");
            }
            Session["sessionOldRegisterNumbers"] = null;
            Session[sessionRegulations] = null;
            Session["StoringLocationDetails"] = null;
            Session[sessionUsageLocations] = null;
            LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
            UserInformation user = (UserInformation)Session["LoginUserSession"];

            if (modeval == (Mode)3)
            {
                string LedgerId = Convert.ToString(Id);
                IEnumerable<string> RegistrationNo = GetSelectedLedger(LedgerId);
                Regno = null;
                if (RegistrationNo.Count() > 0)
                {
                    foreach (string Reg in RegistrationNo)
                    {
                        Regno = Reg;
                    }
                }
            }

            try
            {
                if (Regno != null)
                {
                    var condition = new LedgerSearchModel();
                    condition.REG_NO = Regno;
                    var d = new LedgerDataInfo();
                    objLedgerCommonInfo = objIMangementLedgerRepo.GetLedgerInfo(condition, type, modeval);
                    d.Dispose();

                    if (Session["sessionOldRegisterNumbers"] != null)
                    {
                        List<LedgerOldRegisterNumberModel> objLedgeroldIds = (List<LedgerOldRegisterNumberModel>)Session["sessionOldRegisterNumbers"];
                        objLedgerCommonInfo.objLedgerOldRegisterNumberModel = objLedgeroldIds;
                    }
                    else
                    {
                        var OldRegistrationnumbers = GetInputedOldRegisterNumbers(objLedgerCommonInfo.REG_NO, null, null, null);
                        objLedgerCommonInfo.objLedgerOldRegisterNumberModel = OldRegistrationnumbers;
                        Session["sessionOldRegisterNumbers"] = objLedgerCommonInfo.objLedgerOldRegisterNumberModel;
                    }

                    if (Session["StoringLocationDetails"] != null)
                    {
                        List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                        objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                    }
                    else
                    {
                        var StoringLocation = GetInputedStoringLocations(objLedgerCommonInfo.REG_NO, null, null, objLedgerCommonInfo.CHEM_CD);
                        objLedgerCommonInfo.StorageLocation = StoringLocation;
                        Session["StoringLocationDetails"] = objLedgerCommonInfo.StorageLocation;
                    }

                    if (Session[sessionUsageLocations] != null)
                    {
                        var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                        objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                    }
                    else
                    {
                        var UsageLocation = GetInputedUsageLocations(objLedgerCommonInfo.REG_NO, null, null);
                        objLedgerCommonInfo.objLedgerUsageLocationModel = UsageLocation;
                        Session[sessionUsageLocations] = objLedgerCommonInfo.objLedgerUsageLocationModel;
                    }

                    if (Session[sessionRegulations] != null)
                    {
                        List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                        objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                    }
                    else
                    {
                        var Regulation = GetRegulation(null, null, objLedgerCommonInfo.REG_NO);
                        objLedgerCommonInfo.objLedgerRegulationModel = Regulation;
                    }
                    objLedgerCommonInfo.LookupUnitSizeMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.UNITSIZEMASTERINFO];
                    objLedgerCommonInfo.LookupManufacturerName = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MANUFACTURENAME];
                    var itemvalue = objLedgerCommonInfo.LookupManufacturerName.Where(item => item.Text == objLedgerCommonInfo.MAKER_NAME).ToList();

                    if (itemvalue != null && itemvalue.Count() > 0)
                    {
                        objLedgerCommonInfo.MAKER_NAME = itemvalue[0].Value;
                    }
                    objLedgerCommonInfo.LookupReason = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.REASON];
                    var Workflow = GetWorkflows(objLedgerCommonInfo.REG_NO, null, modeval);
                    objLedgerCommonInfo.objT_LED_WORK_HISTORY = Workflow;
                    Session["Workflow"] = Workflow;

                    if (modeval == Mode.Copy)
                    {
                        objLedgerCommonInfo.REG_NO = "";
                        var regulationInfo = new Nikon.LedRegulationMasterInfo();
                        var ids = regulationInfo.LedgerGetIdsBelongInChem(objLedgerCommonInfo.CHEM_CD);
                        var items = regulationInfo.GetSelectedItems(ids).AsQueryable();
                        objLedgerCommonInfo.REQUIRED_CHEM = items.Any(x => x.WORKTIME_MGMT == 1) ? "該当" : "非該当";

                        var hazmatText = items.FirstOrDefault(x => x.REG_TEXT.IndexOf("消防法") != -1)?.REG_TEXT;
                        objLedgerCommonInfo.HAZMAT_TYPE = hazmatText ?? "非該当";

                        if (objLedgerCommonInfo.CHEM_CD == NikonConst.UnregisteredChemCd)
                        {
                            objLedgerCommonInfo.REQUIRED_CHEM = null;
                            objLedgerCommonInfo.HAZMAT_TYPE = null;
                        }
                    }
                    var btnresult = objIMangementLedgerRepo.EnablingDisblingButtons(modeval, Id, objLedgerCommonInfo.CHEM_CD, objLedgerCommonInfo.HIERARCHY, objLedgerCommonInfo.APPROVAL_USER_CD, objLedgerCommonInfo.SUBSITUTE_USER_CD, user.User_CD, user.ADMIN_FLAG);
                    objLedgerCommonInfo.btnApproval = btnresult.btnApproval;
                    objLedgerCommonInfo.btnRemand = btnresult.btnRemand;
                    objLedgerCommonInfo.btnChemRegister = btnresult.btnChemRegister;
                    objLedgerCommonInfo.btnRegain = btnresult.btnRegain;
                    objLedgerCommonInfo.btnsendback = btnresult.btnsendback;
                    objLedgerCommonInfo.SendBackVisibility = btnresult.SendBackVisibility;
                    objLedgerCommonInfo.btnRegister = btnresult.btnRegister;
                    objLedgerCommonInfo.btndeletematter = btnresult.btndeletematter;
                    objLedgerCommonInfo.btntemporarysave = btnresult.btntemporarysave;
                    objLedgerCommonInfo.btnRevocation = btnresult.btnRevocation;

                    if (modeval == Mode.Copy)
                    {
                        objLedgerCommonInfo.APPLI_CLASS = btnresult.APPLI_CLASS;
                        objLedgerCommonInfo.REASON_ID = btnresult.REASON_ID;
                        objLedgerCommonInfo.OTHER_REASON = btnresult.OTHER_REASON;
                    }

                    if (objLedgerCommonInfo.STATUS_TEXT == "使用中" || objLedgerCommonInfo.STATUS_TEXT == "廃止済み")
                    {
                        objLedgerCommonInfo.ReferenceRegistration = true;
                    }
                }
                else if (Id != null)
                {
                    if (type == TableType.History)
                    {
                        var condition = new LedgerSearchModel();
                        condition.LEDGER_FLOW_ID = Id;
                        var d = new LedgerWorkDataInfo();
                        objLedgerCommonInfo = objIMangementLedgerRepo.GetLedgerInfo(condition, type, modeval);

                        if (Session["sessionOldRegisterNumbers"] != null)
                        {
                            List<LedgerOldRegisterNumberModel> objLedgeroldIds = (List<LedgerOldRegisterNumberModel>)Session["sessionOldRegisterNumbers"];
                            objLedgerCommonInfo.objLedgerOldRegisterNumberModel = objLedgeroldIds;
                        }
                        else
                        {
                            var OldRegistrationnumbers = GetInputedOldRegisterNumbers(null, Id, modeval, objLedgerCommonInfo.TableType);
                            objLedgerCommonInfo.objLedgerOldRegisterNumberModel = OldRegistrationnumbers;
                            Session["sessionOldRegisterNumbers"] = objLedgerCommonInfo.objLedgerOldRegisterNumberModel;
                        }

                        if (Session["StoringLocationDetails"] != null)
                        {
                            List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                            objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                        }
                        else
                        {
                            var StoringLocation = GetInputedStoringLocations(null, Id, objLedgerCommonInfo.TableType, objLedgerCommonInfo.CHEM_CD);
                            objLedgerCommonInfo.StorageLocation = StoringLocation;
                            Session["StoringLocationDetails"] = objLedgerCommonInfo.StorageLocation;
                        }

                        if (Session[sessionUsageLocations] != null)
                        {
                            var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                            objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                        }
                        else
                        {
                            var UsageLocation = GetInputedUsageLocations(null, Id, objLedgerCommonInfo.TableType);
                            objLedgerCommonInfo.objLedgerUsageLocationModel = UsageLocation;
                            Session[sessionUsageLocations] = objLedgerCommonInfo.objLedgerUsageLocationModel;
                        }

                        if (Session[sessionRegulations] != null)
                        {
                            List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                            objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                        }
                        else
                        {
                            var Regulation = GetRegulation(objLedgerCommonInfo.CHEM_CD, Id, null);
                            objLedgerCommonInfo.objLedgerRegulationModel = Regulation;
                            Session[sessionRegulations] = objLedgerCommonInfo.objLedgerRegulationModel;
                        }
                        objLedgerCommonInfo.LookupUnitSizeMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.UNITSIZEMASTERINFO];
                        objLedgerCommonInfo.LookupManufacturerName = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MANUFACTURENAME];
                        var itemvalue = objLedgerCommonInfo.LookupManufacturerName.Where(item => item.Text == objLedgerCommonInfo.MAKER_NAME).ToList();

                        if (itemvalue != null && itemvalue.Count() > 0)
                        {
                            objLedgerCommonInfo.MAKER_NAME = itemvalue[0].Value;
                        }
                        objLedgerCommonInfo.LookupReason = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.REASON];
                        var Workflow = GetWorkflows(null, Id, modeval);
                        objLedgerCommonInfo.objT_LED_WORK_HISTORY = Workflow;
                        Session["Workflow"] = Workflow;

                        if (objLedgerCommonInfo.STATUS_TEXT == "使用中" || objLedgerCommonInfo.STATUS_TEXT == "廃止済み")
                        {
                            var btnresult = objIMangementLedgerRepo.EnablingDisblingButtons(modeval, Id, objLedgerCommonInfo.CHEM_CD, objLedgerCommonInfo.HIERARCHY, objLedgerCommonInfo.APPROVAL_USER_CD, objLedgerCommonInfo.SUBSITUTE_USER_CD, user.User_CD, user.ADMIN_FLAG);

                            objLedgerCommonInfo.btnApproval = btnresult.btnApproval;
                            objLedgerCommonInfo.btnRemand = btnresult.btnRemand;
                            objLedgerCommonInfo.btnChemRegister = btnresult.btnChemRegister;
                            objLedgerCommonInfo.btnRegain = btnresult.btnRegain;
                            objLedgerCommonInfo.btnsendback = btnresult.btnsendback;
                            objLedgerCommonInfo.SendBackVisibility = btnresult.SendBackVisibility;
                            objLedgerCommonInfo.btnRegister = btnresult.btnRegister;
                            objLedgerCommonInfo.btndeletematter = btnresult.btndeletematter;
                            objLedgerCommonInfo.btntemporarysave = btnresult.btntemporarysave;
                            objLedgerCommonInfo.btnRevocation = btnresult.btnRevocation;

                            if (modeval == Mode.Copy)
                            {
                                objLedgerCommonInfo.APPLI_CLASS = btnresult.APPLI_CLASS;
                                objLedgerCommonInfo.REASON_ID = btnresult.REASON_ID;
                                objLedgerCommonInfo.OTHER_REASON = btnresult.OTHER_REASON;
                            }

                            if (objLedgerCommonInfo.STATUS_TEXT == "使用中")
                            {
                                objLedgerCommonInfo.ReferenceRegistration = true;
                            }
                        }
                        else
                        {
                            var btnresult = objIMangementLedgerRepo.EnablingDisblingButtons(modeval, Id, objLedgerCommonInfo.CHEM_CD, objLedgerCommonInfo.HIERARCHY, objLedgerCommonInfo.APPROVAL_USER_CD, objLedgerCommonInfo.SUBSITUTE_USER_CD, user.User_CD, user.ADMIN_FLAG);
                            objLedgerCommonInfo.btnApproval = btnresult.btnApproval;
                            objLedgerCommonInfo.btnRemand = btnresult.btnRemand;
                            objLedgerCommonInfo.btnChemRegister = btnresult.btnChemRegister;
                            objLedgerCommonInfo.btnRegain = btnresult.btnRegain;
                            objLedgerCommonInfo.btnsendback = btnresult.btnsendback;
                            objLedgerCommonInfo.SendBackVisibility = btnresult.SendBackVisibility;
                            objLedgerCommonInfo.btnRegister = btnresult.btnRegister;
                            objLedgerCommonInfo.btndeletematter = btnresult.btndeletematter;
                            objLedgerCommonInfo.btntemporarysave = btnresult.btntemporarysave;
                            objLedgerCommonInfo.btnRevocation = btnresult.btnRevocation;

                            if (modeval == Mode.Copy)
                            {
                                objLedgerCommonInfo.APPLI_CLASS = btnresult.APPLI_CLASS;
                                objLedgerCommonInfo.REASON_ID = btnresult.REASON_ID;
                                objLedgerCommonInfo.OTHER_REASON = btnresult.OTHER_REASON;
                            }

                            if (objLedgerCommonInfo.STATUS_TEXT == "使用中")
                            {
                                objLedgerCommonInfo.ReferenceRegistration = true;
                            }
                        }
                    }
                    else if (Id != null)
                    {
                        var condition = new LedgerSearchModel();
                        condition.LEDGER_FLOW_ID = Id;
                        var d = new LedgerWorkDataInfo();
                        objLedgerCommonInfo = objIMangementLedgerRepo.GetLedgerInfo(condition, type, modeval);
                        d.Dispose();
                        objLedgerCommonInfo.Mode = modeval;
                        if (modeval == Mode.Copy)
                        {
                            objLedgerCommonInfo.REG_NO = null;
                        }
                        Session.Add(nameof(LedgerModel), objLedgerCommonInfo);

                        if (Session["sessionOldRegisterNumbers"] != null)
                        {
                            List<LedgerOldRegisterNumberModel> objLedgeroldIds = (List<LedgerOldRegisterNumberModel>)Session["sessionOldRegisterNumbers"];
                            objLedgerCommonInfo.objLedgerOldRegisterNumberModel = objLedgeroldIds;
                        }
                        else
                        {
                            var OldRegistrationnumbers = GetInputedOldRegisterNumbers(null, Id, modeval, type);
                            objLedgerCommonInfo.objLedgerOldRegisterNumberModel = OldRegistrationnumbers;
                            Session["sessionOldRegisterNumbers"] = objLedgerCommonInfo.objLedgerOldRegisterNumberModel;
                        }

                        if (Session["StoringLocationDetails"] != null)
                        {
                            List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                            objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                        }
                        else
                        {
                            var StoringLocation = GetInputedStoringLocations(null, Id, type, objLedgerCommonInfo.CHEM_CD);
                            objLedgerCommonInfo.StorageLocation = StoringLocation;
                            Session["StoringLocationDetails"] = objLedgerCommonInfo.StorageLocation;
                        }

                        if (Session[sessionUsageLocations] != null)
                        {
                            var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                            objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                        }
                        else
                        {
                            var UsageLocation = GetInputedUsageLocations(null, Id, type);
                            objLedgerCommonInfo.objLedgerUsageLocationModel = UsageLocation;
                            Session[sessionUsageLocations] = objLedgerCommonInfo.objLedgerUsageLocationModel;
                        }

                        if (Session[sessionRegulations] != null)
                        {
                            List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                            objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                        }
                        else
                        {
                            var Regulation = GetRegulation(objLedgerCommonInfo.CHEM_CD, Id, null);
                            objLedgerCommonInfo.objLedgerRegulationModel = Regulation;
                            Session[sessionRegulations] = objLedgerCommonInfo.objLedgerRegulationModel;
                        }
                        objLedgerCommonInfo.LookupUnitSizeMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.UNITSIZEMASTERINFO];
                        objLedgerCommonInfo.LookupManufacturerName = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MANUFACTURENAME];
                        var itemvalue = objLedgerCommonInfo.LookupManufacturerName.Where(item => item.Text == objLedgerCommonInfo.MAKER_NAME).ToList();

                        if (itemvalue != null && itemvalue.Count() > 0)
                        {
                            objLedgerCommonInfo.MAKER_NAME = itemvalue[0].Value;
                        }

                        objLedgerCommonInfo.LookupReason = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.REASON];

                        var Workflow = GetWorkflows(null, Id, modeval);
                        objLedgerCommonInfo.objT_LED_WORK_HISTORY = Workflow;
                        Session["Workflow"] = Workflow;

                        var btnresult = objIMangementLedgerRepo.EnablingDisblingButtons(modeval, Id, objLedgerCommonInfo.CHEM_CD, objLedgerCommonInfo.HIERARCHY, objLedgerCommonInfo.APPROVAL_USER_CD, objLedgerCommonInfo.SUBSITUTE_USER_CD, user.User_CD, user.ADMIN_FLAG);
                        objLedgerCommonInfo.btnApproval = btnresult.btnApproval;
                        objLedgerCommonInfo.btnRemand = btnresult.btnRemand;
                        objLedgerCommonInfo.btnChemRegister = btnresult.btnChemRegister;
                        objLedgerCommonInfo.btnRegain = btnresult.btnRegain;
                        objLedgerCommonInfo.btnsendback = btnresult.btnsendback;
                        objLedgerCommonInfo.SendBackVisibility = btnresult.SendBackVisibility;
                        objLedgerCommonInfo.btnRegister = btnresult.btnRegister;
                        objLedgerCommonInfo.btndeletematter = btnresult.btndeletematter;
                        objLedgerCommonInfo.btntemporarysave = btnresult.btntemporarysave;
                        objLedgerCommonInfo.btnRevocation = btnresult.btnRevocation;

                        if (modeval == Mode.Copy)
                        {
                            objLedgerCommonInfo.APPLI_CLASS = btnresult.APPLI_CLASS;
                            objLedgerCommonInfo.REASON_ID = btnresult.REASON_ID;
                            objLedgerCommonInfo.OTHER_REASON = btnresult.OTHER_REASON;
                        }

                        if (objLedgerCommonInfo.STATUS_TEXT == "使用中")
                        {
                            objLedgerCommonInfo.ReferenceRegistration = true;
                        }
                    }
                }

                if (mode == 0 && flowmodes != 0)
                {
                    var condition = new LedgerSearchModel();
                    var d = new LedgerDataInfo();
                    objLedgerCommonInfo = objIMangementLedgerRepo.GetLedgerInfo(condition, type, modeval);

                    if (Session[sessionRegulations] != null)
                    {
                        List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                        objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                    }
                    var Workflow = GetWorkflows(null, Id, modeval);
                    objLedgerCommonInfo.objT_LED_WORK_HISTORY = Workflow;
                    Session["Workflow"] = Workflow;

                    objLedgerCommonInfo.LookupUnitSizeMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.UNITSIZEMASTERINFO];

                    objLedgerCommonInfo.LookupManufacturerName = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MANUFACTURENAME];

                    if (Session["StoringLocationDetails"] != null)
                    {
                        List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                        objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                    }
                    else
                    {
                        LedgerLocationModel data = new LedgerLocationModel();
                        int StoringLocationcounter = 1;
                        List<LedgerLocationModel> ledgerlocationmodel = new List<LedgerLocationModel>();
                        data.LOC_SEQ = StoringLocationcounter;
                        data.LOC_NAME = "";
                        data.AMOUNT = "";
                        data.UNITSIZE_ID = 0;
                        ledgerlocationmodel.Add(data);
                        Session["StoringLocationDetails"] = ledgerlocationmodel;
                        Session[SessionConst.RowId] = data.RowId;
                        objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                    }
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            if (TempData.ContainsKey("regdeptmsg"))
                ViewBag.data2 = TempData["regdeptmsg"].ToString();
            if (modeval == 0)
            {
                objLedgerCommonInfo.ViewModeValue = "0";
            }
            objLedgerCommonInfo.FLOW_Mode = flowmodes;
            objLedgerCommonInfo.ADMIN_FLAG = user.ADMIN_FLAG;
            objLedgerCommonInfo.flowid = flowids;
            return View(objLedgerCommonInfo);
        }

        public void drpmakerunitsize()
        {
            try
            {
                LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
                objLedgerCommonInfo.LookupUnitSizeMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.UNITSIZEMASTERINFO];
                objLedgerCommonInfo.LookupManufacturerName = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MANUFACTURENAME];
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
        }

        public void SDS_FILENAME_Command(int id)
        {
            try
            {
                download dl = delegate (LedgerSearchModel cond, dynamic dataInfo)
                {
                    IQueryable<LedgerModel> m = dataInfo.GetSearchResult(cond);
                    var res = m.FirstOrDefault();
                    Response.AppendHeader("Content-Disposition",
                        string.Format("attachment;filename={0}", HttpUtility.UrlEncode(res.SDS_FILENAME)));
                    Response.ContentType = res.SDS_MIMETYPE;
                    Response.BinaryWrite(res.SDS);
                    Response.End();
                };
                switch (type)
                {
                    case TableType.Ledger:
                        using (var dataInfo = new LedgerDataInfo())
                        {
                            var cond = new LedgerSearchModel() { REG_NO = regno };
                            dl(cond, dataInfo);
                        }
                        break;
                    case TableType.Work:
                        using (var dataInfo = new LedgerWorkDataInfo())
                        {
                            var cond = new LedgerSearchModel() { LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(id) };
                            dl(cond, dataInfo);
                        }
                        break;
                    case TableType.History:
                        using (var dataInfo = new LedgerHistoryDataInfo())
                        {
                            var cond = new LedgerSearchModel() { LEDGER_HISTORY_ID = OrgConvert.ToNullableInt32(id) };
                            dl(cond, dataInfo);
                        }
                        break;
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
        }

        public void UNREGISTERED_CHEM_FILENAME_Command(int id)
        {
            try
            {
                download dl = delegate (LedgerSearchModel cond, dynamic dataInfo)
                {
                    IQueryable<LedgerModel> m = dataInfo.GetSearchResult(cond);
                    var res = m.FirstOrDefault();
                    Response.AppendHeader("Content-Disposition",
                        string.Format("attachment;filename={0}", HttpUtility.UrlEncode(res.UNREGISTERED_CHEM_FILENAME)));
                    Response.ContentType = res.UNREGISTERED_CHEM_MIMETYPE;
                    Response.BinaryWrite(res.UNREGISTERED_CHEM);
                    Response.End();
                };
                switch (type)
                {
                    case TableType.Ledger:
                        using (var dataInfo = new LedgerDataInfo())
                        {
                            var cond = new LedgerSearchModel() { REG_NO = regno };
                            dl(cond, dataInfo);
                        }
                        break;
                    case TableType.Work:
                        using (var dataInfo = new LedgerWorkDataInfo())
                        {
                            var cond = new LedgerSearchModel() { LEDGER_FLOW_ID = OrgConvert.ToNullableInt32(id) };
                            dl(cond, dataInfo);
                        }
                        break;
                    case TableType.History:
                        using (var dataInfo = new LedgerHistoryDataInfo())
                        {
                            var cond = new LedgerSearchModel() { LEDGER_HISTORY_ID = OrgConvert.ToNullableInt32(id) };
                            dl(cond, dataInfo);
                        }
                        break;
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
        }

        /// <summary>
        /// <para>化学物質コード入力後、検索ボタン押下時の処理です。</para>
        /// <para>化学物質マスターを検索し、該当する化学物質名を表示します。</para>
        /// </summary>
        public ActionResult PopupChemSearchClick(string ChemCd)
        {
            string Flag = "";
            string[] ArrayList = new string[] { };
            string ChemName = ""; string Errormessage = "";
            try
            {
                if (!string.IsNullOrWhiteSpace(ChemCd))
                {
                    using (var dataInfo = new ChemDataInfo())
                    {
                        var chem = dataInfo.GetSearchResult(new ChemSearchModel()
                        {
                            CHEM_CD = ChemCd,
                            CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching
                        });

                        if (chem.Count() == 1)
                        {
                            Flag = "1";
                            ChemName = chem.First().CHEM_NAME;
                            ArrayList = new string[] { Flag, ChemName };
                        }
                        else
                        {
                            Flag = "2";
                            Errormessage = string.Format(WarningMessages.NotFound, "化学物質");
                            ArrayList = new string[] { Flag, Errormessage };
                        }
                    }
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return Json(ArrayList);
        }

        public ActionResult ChemRegisterOK(string RegisterChemCd)
        {
            string[] ChemRegisterArray = new string[] { };
            string Flag = "";
            LedgerModel model = (LedgerModel)Session["LedgerModel"];
            string ChemName = ""; string Errormessage = "";
            try
            {
                if (string.IsNullOrWhiteSpace(RegisterChemCd))
                {
                    Flag = "1";
                    Errormessage = string.Format(WarningMessages.NotInput, "化学物質コード");
                    ChemRegisterArray = new string[] { Flag, Errormessage };
                    //chemRegisterOK.Enabled = false;
                    //return;
                }
                if (RegisterChemCd != NikonConst.UnregisteredChemCd)
                {
                    using (var dataInfo = new ChemDataInfo())
                    {
                        var chem = dataInfo.GetSearchResult(new ChemSearchModel()
                        {
                            CHEM_CD = RegisterChemCd,
                            CHEM_CD_CONDITION = (int)SearchConditionType.PerfectMatching
                        });
                        if (chem.Count() == 1)
                        {
                            Flag = "2";
                            ChemName = chem.First().CHEM_NAME;
                            ChemRegisterArray = new string[] { Flag, ChemName };
                        }
                        else
                        {
                            Flag = "1";
                            Errormessage = string.Format(WarningMessages.NotFound, "化学物質");
                            ChemRegisterArray = new string[] { Flag, Errormessage };
                            //chemRegisterOK.Enabled = false;
                            //return;
                        }
                    }
                    using (var groupMaster = new GroupMasterInfo())
                    {
                        var grp = groupMaster.GetParentGroups(model.GROUP_CD, true, 1);
                        var grpSel = string.Empty;
                        foreach (var g in grp)
                        {
                            if (string.IsNullOrEmpty(grpSel))
                            {
                                grpSel += g;
                            }
                            else
                            {
                                grpSel += ("," + g);
                            }
                        }
                        var cond = new LedgerSearchModel()
                        {
                            CHEM_CD = RegisterChemCd,
                            GROUP_CD_SELECTION = grpSel,
                            LEDGER_FLOW_ID_EXT = model.LEDGER_FLOW_ID,
                            REG_NO_EXT = model.REG_NO
                        };
                        using (var work = new LedgerWorkDataInfo())
                        {
                            List<int> Count = work.lstGetSearchResultCount(cond);
                            if (Count[0] > 0)
                            {
                                Flag = "1";
                                Errormessage = WarningMessages.SameChemSelect;
                                ChemRegisterArray = new string[] { Flag, Errormessage };
                                //chemRegisterOK.Enabled = false;
                                //return;
                            }
                        }
                        using (var data = new LedgerDataInfo())
                        {
                            List<int> Records = data.lstGetSearchResultCount(cond);
                            if (Records[0] > 0)
                            {
                                Flag = "1";
                                Errormessage = WarningMessages.SameChemSelect;
                                ChemRegisterArray = new string[] { Flag, Errormessage };
                            }
                        }
                    }
                }
                else
                {
                    Errormessage = WarningMessages.UnregisteredChemCd;
                    ChemRegisterArray = new string[] { Flag, Errormessage };
                }
            }
            catch (Exception exe)
            {

                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return Json(ChemRegisterArray);
        }


        public IEnumerable<T_LED_WORK_HISTORY> GetWorkflows(string regno, int? id, Mode? mode)
        {

            UserInformation loginModel = new UserInformation();
            loginModel = (UserInformation)Session["LoginUserSession"];

            var LEDGER_FLOW_ID = id;
            if (id != null)
            {
                using (var LedgerWorkHistoryDataInfo = new LedgerWorkHistoryDataInfo())
                {
                    return LedgerWorkHistoryDataInfo.GetSearchResult(new T_LED_WORK_HISTORY() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
                }
            }
            else if (regno != null)
            {
                if (mode == Mode.Copy)
                {
                    using (var LedgerWorkHistoryDataInfo = new LedgerWorkHistoryDataInfo())
                    {
                        return LedgerWorkHistoryDataInfo.GetBlankWorkflow(
                                new T_LED_WORK_HISTORY() { LEDGER_FLOW_ID = LEDGER_FLOW_ID }, loginModel.User_CD, null, mode);
                    }
                }
                using (var d = new LedgerDataInfo())
                {
                    var led = d.GetSearchResult(new LedgerSearchModel() { REG_NO = regno, LEDGER_STATUS = (int)LedgerStatusConditionType.All, });
                    var newest = led.LastOrDefault();
                    if (newest != null)
                    {
                        LEDGER_FLOW_ID = newest.RECENT_LEDGER_FLOW_ID;
                    }
                }
                using (var LedgerWorkHistoryDataInfo = new LedgerWorkHistoryDataInfo())
                {
                    var db = LedgerWorkHistoryDataInfo.GetSearchResult(new T_LED_WORK_HISTORY() { LEDGER_FLOW_ID = LEDGER_FLOW_ID });
                    if (mode == Mode.Edit || mode == Mode.Abolition)
                    {
                        var a = db.Union(LedgerWorkHistoryDataInfo.GetBlankWorkflow(
                             new T_LED_WORK_HISTORY() { LEDGER_FLOW_ID = LEDGER_FLOW_ID }, loginModel.User_CD, null, mode));
                        return a;
                    }
                    return db;
                }
            }
            else
            {
                using (var LedgerWorkHistoryDataInfo = new LedgerWorkHistoryDataInfo())
                {
                    var workflow = LedgerWorkHistoryDataInfo.GetBlankWorkflow(
                            new T_LED_WORK_HISTORY() { LEDGER_FLOW_ID = LEDGER_FLOW_ID }, loginModel.User_CD, null, mode);
                    var userDic = new UserMasterInfo().GetDictionary();
                    foreach (var flow in workflow)
                    {
                        if (!userDic.ContainsKey(flow.APPROVAL_USER_CD))
                        {
                            //OutputWarningMessage(string.Format(WarningMessages.WorkFlowUserError, flow.APPROVAL_USER_CD));
                        }
                    }
                    return workflow;
                }
            }
        }

        public List<LedgerOldRegisterNumberModel> GetInputedOldRegisterNumbers(string regno, int? id, Mode? mode, TableType? type)
        {
            if (id != null)
            {
                if (type == TableType.History)
                {
                    var condition = new LedgerOldRegisterNumberSearchModel();
                    condition.LEDGER_FLOW_ID = id;
                    var d = new LedgerHistoryOldRegisterNumberDataInfo();
                    var dataArray = d.GetSearchResult(condition).ToList();
                    d.Dispose();
                    Session.Add(sessionOldRegisterNumbers, dataArray.ToList());
                    return dataArray.ToList();
                }
                else
                {
                    var condition = new LedgerOldRegisterNumberSearchModel();
                    condition.LEDGER_FLOW_ID = id;
                    var d = new LedgerWorkOldRegisterNumberDataInfo();
                    var dataArray = d.GetSearchResult(condition).ToList();
                    d.Dispose();
                    Session.Add(sessionOldRegisterNumbers, dataArray.AsEnumerable());
                    return dataArray.ToList();
                }
            }
            else if (string.IsNullOrWhiteSpace(regno) == false)
            {
                var condition = new LedgerOldRegisterNumberSearchModel();
                condition.REG_NO = regno;
                var d = new LedgerOldRegisterNumberDataInfo();
                var dataArray = d.GetSearchResult(condition).ToList();
                d.Dispose();
                Session.Add(sessionOldRegisterNumbers, dataArray.AsEnumerable());
                return dataArray.ToList();
            }
            var newDataArray = new List<LedgerOldRegisterNumberModel>();
            Session.Add(sessionOldRegisterNumbers, newDataArray.AsEnumerable());
            return newDataArray.ToList();
        }

        public List<LedgerRegulationModel> GetRegulation(string ChemicalCode, int? id, string regno)
        {
            var tableName = (string)Session[SessionConst.CategoryTableName];
            if (tableName == nameof(M_CHEM))
            {
                // 化学物質検索後
                return getChemRegulation(ChemicalCode, 1);
            }
            else if (tableName == nameof(T_LED_WORK_USAGE_LOC))
            {
                // 管理台帳詳細 設定後
                var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                if (usageLocations != null)
                {
                    if (usageLocations.Count != 0)
                    {
                        Session.Remove(sessionRegulations);
                        var regulationInfo = new Nikon.LedRegulationMasterInfo();
                        var ids = regulationInfo.LedgerGetIdsBelongInChem(ChemicalCode);
                        string strQuery = string.Join(SystemConst.SelectionsDelimiter, ids.ToList().ConvertAll(x => x.ToString()));

                        if (strQuery != "" && strQuery != null)
                        {
                            string strIshaUsage = "";
                            foreach (var record in usageLocations)
                            {
                                if (strIshaUsage != "")
                                {
                                    strIshaUsage += ",";
                                }
                                strIshaUsage += record.ISHA_USAGE.ToString();
                            }

                            var d = new LedgerRegulationDataInfo();
                            var dataArray = d.GetRegTypeIds(strQuery, strIshaUsage).ToList();
                            d.Dispose();
                            if (dataArray.Count != 0)
                            {
                                Session.Add(sessionRegulations, dataArray.AsEnumerable());
                                setRegulationModel(ChemicalCode, dataArray);
                            }
                            return dataArray.ToList();
                        }
                    }
                }
            }
            else
            {
                // 管理台帳検索画面、案件一覧からの遷移(DBから取得)
                var searchDataArray = new List<LedgerRegulationModel>();

                if (id != null)
                {
                    var wd = new LedgerRegulationDataInfo();
                    var wdataArray = wd.GetResultFromRegulation("T_LEDGER_WORK_REGULATION", Convert.ToString(id)).ToList();
                    wd.Dispose();
                    if (wdataArray.Count != 0)
                    {
                        Session.Add(sessionRegulations, wdataArray.AsEnumerable());
                        setRegulationModel(ChemicalCode, wdataArray);
                        searchDataArray = wdataArray;
                    }
                }
                else if (string.IsNullOrWhiteSpace(regno) == false)
                {
                    var ld = new LedgerRegulationDataInfo();
                    var ldataArray = ld.GetResultFromRegulation("T_LED_REGULATION", regno).ToList();
                    ld.Dispose();
                    if (ldataArray.Count != 0)
                    {
                        Session.Add(sessionRegulations, ldataArray.AsEnumerable());
                        setRegulationModel(ChemicalCode, ldataArray);
                        searchDataArray = ldataArray;
                    }
                }
                else
                {
                    Session.Add(sessionRegulations, searchDataArray.AsEnumerable());
                }
                return searchDataArray.ToList();
            }
            var newDataArray = new List<LedgerRegulationModel>();
            Session.Add(sessionRegulations, newDataArray.AsEnumerable());
            return newDataArray.ToList();
        }

        public List<LedgerUsageLocationModel> GetInputedUsageLocations(string regno, int? id, TableType? type)
        {
            if (id != null)
            {
                if (type == TableType.History)
                {
                    var condition = new LedgerUsageLocationSearchModel();
                    condition.LEDGER_FLOW_ID = id;
                    var d = new LedgerHistoryUsageLocationDataInfo();
                    var dataArray = d.GetSearchResult(condition).ToList();
                    d.Dispose();
                    Session.Add(sessionUsageLocations, dataArray.AsEnumerable());
                    return dataArray.ToList();
                }
                else
                {
                    var condition = new LedgerUsageLocationSearchModel();
                    condition.LEDGER_FLOW_ID = id;
                    var d = new LedgerWorkUsageLocationDataInfo();
                    var dataArray = d.GetSearchResult(condition).ToList();
                    d.Dispose();
                    Session.Add(sessionUsageLocations, dataArray.AsEnumerable());
                    return dataArray.ToList();
                }
            }
            else if (string.IsNullOrWhiteSpace(regno) == false)
            {
                var condition = new LedgerUsageLocationSearchModel();
                condition.REG_NO = regno;
                var d = new LedgerUsageLocationDataInfo();
                var dataArray = d.GetSearchResult(condition).ToList();
                d.Dispose();
                Session.Add(sessionUsageLocations, dataArray.AsEnumerable());
                return dataArray.ToList();
            }
            var newDataArray = new List<LedgerUsageLocationModel>();
            Session.Add(sessionUsageLocations, newDataArray.AsEnumerable());
            return newDataArray.ToList();
        }
        public List<LedgerLocationModel> GetInputedStoringLocations(string regno, int? id, TableType? type, string CHEM_CD)
        {
            //if (Session[sessionStoringLocations] != null)
            //{
            //    return storingLocations.ToList().AsQueryable();
            //}

            List<LedgerLocationModel> dataArray = new List<LedgerLocationModel>();
            string errorMessage = "";
            if (id != null)
            {
                if (CHEM_CD == "")
                {
                    var errorMessages = new List<string>();
                    errorMessage = WarningMessages.RequiredChem;
                    //OutputWarningMessage(errorMessages);
                }

                if (type == TableType.History)
                {
                    var condition = new LedgerLocationSearchModel();
                    condition.LEDGER_FLOW_ID = id;
                    var d = new LedgerHistoryLocationDataInfo();
                    dataArray = d.GetSearchResult(condition).ToList();
                    d.Dispose();
                    Session.Add(sessionStoringLocations, dataArray);
                    //return dataArray;
                }
                else
                {
                    var condition = new LedgerLocationSearchModel();
                    condition.LEDGER_FLOW_ID = id;
                    var d = new LedgerWorkLocationDataInfo();
                    dataArray = d.GetSearchResult(condition).ToList();
                    d.Dispose();
                    Session.Add(sessionStoringLocations, dataArray);
                    //return dataArray;
                }
            }
            else if (string.IsNullOrWhiteSpace(regno) == false)
            {
                if (CHEM_CD == "")
                {
                    var errorMessages = new List<string>();
                    errorMessage = WarningMessages.RequiredChem;
                }
                else
                {
                    var condition = new LedgerLocationSearchModel();
                    condition.REG_NO = regno;
                    var d = new LedgerLocationDataInfo();
                    dataArray = d.GetSearchResult(condition).ToList();
                    d.Dispose();
                    Session.Add(sessionStoringLocations, dataArray);
                    //return dataArray;
                }
            }
            return dataArray;
        }

        [HttpPost]
        public ActionResult ZC14020(LedgerCommonInfo objLedgerCommonInfo, Mode? mode, int? Id, TableType? type, HttpPostedFileBase pdffile, HttpPostedFileBase UNREGISTERED_CHEMfile)
        {
            objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
            try
            {
                LedgerCommonInfo ledgerCommonInfo = new LedgerCommonInfo();
                if (pdffile != null && pdffile.ContentLength > 0)
                {
                    MemoryStream target = new MemoryStream();
                    pdffile.InputStream.CopyTo(target);
                    objLedgerCommonInfo.SDS = target.ToArray();
                    objLedgerCommonInfo.SDS_MIMETYPE = MimeMapping.GetMimeMapping(pdffile.ContentType);
                    objLedgerCommonInfo.SDS_FILENAME = Path.GetFileName(pdffile.FileName);
                }

                if (UNREGISTERED_CHEMfile != null && UNREGISTERED_CHEMfile.ContentLength > 0)
                {
                    MemoryStream target = new MemoryStream();
                    UNREGISTERED_CHEMfile.InputStream.CopyTo(target);
                    objLedgerCommonInfo.UNREGISTERED_CHEM = target.ToArray();
                    objLedgerCommonInfo.UNREGISTERED_CHEM_MIMETYPE = MimeMapping.GetMimeMapping(UNREGISTERED_CHEMfile.ContentType);
                    objLedgerCommonInfo.UNREGISTERED_CHEM_FILENAME = Path.GetFileName(UNREGISTERED_CHEMfile.FileName);
                }
                Session.Add(nameof(LedgerCommonInfo), objLedgerCommonInfo);
                if (Request.Form["FlowCode"] == "10")
                {
                   

                    List<string> alert = objIMangementLedgerRepo.Regain(objLedgerCommonInfo);
                    
                    if (alert.Count > 0)
                    {
                        string alertMessage = null;
                        foreach (var items in alert)
                        {
                            alertMessage += items + "\n";
                        }
                        TempData["alertmessage"] = alertMessage;
                        List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                        objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                        List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                        objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                        var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                        objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                        List<T_LED_WORK_HISTORY> workflow = (List<T_LED_WORK_HISTORY>)Session["Workflow"];
                        objLedgerCommonInfo.objT_LED_WORK_HISTORY = workflow;
                        objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
                        return View(objLedgerCommonInfo);
                    }
                    else
                    {
                        if (Session["BackUrlIds"] != null)
                        {
                            List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                            string ids = null;
                            string modes = null;
                            FlowIds = (List<Tuple<string, string>>)Session["BackUrlIds"];

                            foreach (var element in FlowIds)
                            {
                                if (objLedgerCommonInfo.LEDGER_FLOW_ID != Convert.ToInt32(element.Item1))
                                {
                                    ids = element.Item1;
                                    modes = element.Item2;
                                    FlowIds.RemoveAll(item => item.Item1 == objLedgerCommonInfo.LEDGER_FLOW_ID.ToString());
                                    break;
                                }
                            }
                            int? flowids = Convert.ToInt32(ids);
                            int flowmodes = Convert.ToInt32(modes);
                            Mode intmode = (Mode)Convert.ToInt32(modes);
                            string operation = string.Empty;
                            Session["BackUrlIds"] = FlowIds;
                            objIMangementLedgerRepo.ClearSession();
                            ModelState.Clear();
                            LedgerCommonInfo result = GetMethod(Id = Convert.ToInt32(ids), intmode, flowids, flowmodes, operation);
                            if (FlowIds.Count == 1)
                            {
                                Session["BackUrlIds"] = null;
                            }
                            return View(result);
                        }
                        else
                        {
                            return RedirectToAction("ZC14030", "ZC140");
                        }
                    }
                }
                if (Request.Form["FlowCode"] == "7")
                {
                    UserInformation loginModel = new UserInformation();
                    loginModel = (UserInformation)Session["LoginUserSession"];

                    LedgerModel ledger;
                    using (var dataInfo = new LedgerWorkDataInfo())
                    {
                        ledger = dataInfo.GetSearchResult(new LedgerSearchModel() { LEDGER_FLOW_ID = objLedgerCommonInfo.LEDGER_FLOW_ID }).Single();
                    }
                    var history = new LedgerWorkHistoryModel()
                    {
                        LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID,
                        HIERARCHY = ledger?.HIERARCHY,
                        APPROVAL_USER_CD = loginModel.User_CD,
                        RESULT = (int)WorkflowResult.Remand,
                        MEMO = objLedgerCommonInfo.RemandMemo,
                    };
                    history.MEMO = objLedgerCommonInfo.RemandMemo;
                    Session[nameof(LedgerWorkHistoryModel)] = history;
                    List<string> alert = objIMangementLedgerRepo.Remand(objLedgerCommonInfo);

                    if (alert.Count > 0)
                    {
                        string alertMessage = null;
                        foreach (var items in alert)
                        {
                            alertMessage += items + "\n";
                        }
                        TempData["alertmessage"] = alertMessage;
                        List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                        objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                        List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                        objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                        var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                        objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                        List<T_LED_WORK_HISTORY> workflow = (List<T_LED_WORK_HISTORY>)Session["Workflow"];
                        objLedgerCommonInfo.objT_LED_WORK_HISTORY = workflow;
                        objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
                        return View(objLedgerCommonInfo);
                    }
                    else
                    {
                        if (Session["BackUrlIds"] != null)
                        {
                            List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                            string ids = null;
                            string modes = null;
                            FlowIds = (List<Tuple<string, string>>)Session["BackUrlIds"];

                            foreach (var element in FlowIds)
                            {
                                if (objLedgerCommonInfo.LEDGER_FLOW_ID != Convert.ToInt32(element.Item1))
                                {
                                    ids = element.Item1;
                                    modes = element.Item2;
                                    FlowIds.RemoveAll(item => item.Item1 == objLedgerCommonInfo.LEDGER_FLOW_ID.ToString());
                                    break;
                                }
                            }
                            int? flowids = Convert.ToInt32(ids);
                            int flowmodes = Convert.ToInt32(modes);
                            Mode intmode = (Mode)Convert.ToInt32(modes);
                            string operation = string.Empty;
                            Session["BackUrlIds"] = FlowIds;
                            objIMangementLedgerRepo.ClearSession();
                            ModelState.Clear();
                            LedgerCommonInfo result = GetMethod(Id = Convert.ToInt32(ids), intmode, flowids, flowmodes, operation);
                            if (FlowIds.Count == 1)
                            {
                                Session["BackUrlIds"] = null;
                            }
                            return View(result);
                        }
                        else
                        {
                            return RedirectToAction("ZC14030", "ZC140");
                        }

                    }
                }
                else
                {
                    if (Request.Form["FlowCode"] == "1")
                    {
                        mode = (Mode)objLedgerCommonInfo.Mode;
                       
                        List<string> alert = objIMangementLedgerRepo.Register(objLedgerCommonInfo, mode);
                        
                        if (alert.Count > 0)
                        {
                            string alertMessage = null;
                            foreach (var items in alert)
                            {
                                alertMessage += items + "\n";
                            }
                            TempData["alertmessage"] = alertMessage;
                            List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                            objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                            List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                            objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                            var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                            objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                            List<T_LED_WORK_HISTORY> workflow = (List<T_LED_WORK_HISTORY>)Session["Workflow"];
                            objLedgerCommonInfo.objT_LED_WORK_HISTORY = workflow;
                            objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
                            return View(objLedgerCommonInfo);
                        }
                        else
                        {
                            if (objLedgerCommonInfo.flowid != null)
                            {
                                if (Session["BackUrlIds"] != null)
                                {
                                    List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                                    string ids = null;
                                    string modes = null;
                                    FlowIds = (List<Tuple<string, string>>)Session["BackUrlIds"];

                                    foreach (var element in FlowIds)
                                    {
                                        if (objLedgerCommonInfo.LEDGER_FLOW_ID != Convert.ToInt32(element.Item1))
                                        {
                                            ids = element.Item1;
                                            modes = element.Item2;
                                            FlowIds.RemoveAll(item => item.Item1 == objLedgerCommonInfo.LEDGER_FLOW_ID.ToString());
                                            break;
                                        }
                                    }
                                    int? flowids = Convert.ToInt32(ids);
                                    int flowmodes = Convert.ToInt32(modes);
                                    Mode intmode = (Mode)Convert.ToInt32(modes);
                                    string operation = string.Empty;
                                    Session["BackUrlIds"] = FlowIds;
                                    objIMangementLedgerRepo.ClearSession();
                                    ModelState.Clear();
                                    LedgerCommonInfo result = GetMethod(Id = Convert.ToInt32(ids), intmode, flowids, flowmodes, operation);
                                    if (FlowIds.Count == 1)
                                    {
                                        Session["BackUrlIds"] = null;
                                    }
                                    return View(result);
                                }
                                else
                                {
                                    return RedirectToAction("ZC14030", "ZC140");
                                }
                            }
                            else
                            {
                                LedgerCommonInfo result = GetMethod(objLedgerCommonInfo.LEDGER_FLOW_ID, objLedgerCommonInfo.Mode = (Mode)5, null, null, objLedgerCommonInfo.hdnOperation = "register");
                                return View(result);
                            }
                        }
                    }

                    if (Request.Form["FlowCode"] == "2")
                    {
                        mode = (Mode)objLedgerCommonInfo.Mode;
                        

                        List<string> alert = objIMangementLedgerRepo.Update(objLedgerCommonInfo, mode);
                        
                        if (alert.Count > 0)
                        {
                            string alertMessage = null;
                            foreach (var items in alert)
                            {
                                alertMessage += items + "\n";
                            }
                            TempData["alertmessage"] = alertMessage;
                            List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                            objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                            List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                            objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                            var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                            objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                            List<T_LED_WORK_HISTORY> workflow = (List<T_LED_WORK_HISTORY>)Session["Workflow"];
                            objLedgerCommonInfo.objT_LED_WORK_HISTORY = workflow;
                            objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
                            return View(objLedgerCommonInfo);
                        }
                        else
                        {
                            if (objLedgerCommonInfo.flowid != null)
                            {
                                if (Session["BackUrlIds"] != null)
                                {
                                    List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                                    string ids = null;
                                    string modes = null;
                                    FlowIds = (List<Tuple<string, string>>)Session["BackUrlIds"];

                                    foreach (var element in FlowIds)
                                    {
                                        if (objLedgerCommonInfo.LEDGER_FLOW_ID != Convert.ToInt32(element.Item1))
                                        {
                                            ids = element.Item1;
                                            modes = element.Item2;
                                            FlowIds.RemoveAll(item => item.Item1 == objLedgerCommonInfo.LEDGER_FLOW_ID.ToString());
                                            break;
                                        }
                                    }
                                    int? flowids = Convert.ToInt32(ids);
                                    int flowmodes = Convert.ToInt32(modes);
                                    Mode intmode = (Mode)Convert.ToInt32(modes);
                                    string operation = string.Empty;
                                    Session["BackUrlIds"] = FlowIds;
                                    objIMangementLedgerRepo.ClearSession();
                                    ModelState.Clear();
                                    LedgerCommonInfo result = GetMethod(Id = Convert.ToInt32(ids), intmode, flowids, flowmodes, operation);
                                    if (FlowIds.Count == 1)
                                    {
                                        Session["BackUrlIds"] = null;
                                    }
                                    return View(result);
                                }
                                else
                                {
                                    return RedirectToAction("ZC14030", "ZC140");
                                }
                            }
                            else
                            {
                                LedgerCommonInfo result = GetMethod(objLedgerCommonInfo.LEDGER_FLOW_ID, objLedgerCommonInfo.Mode = (Mode)5, null, null, objLedgerCommonInfo.hdnOperation = "update");
                                return View(result);
                            }
                        }
                    }


                    if (Request.Form["FlowCode"] == "3")
                    {
                        mode = (Mode)objLedgerCommonInfo.Mode;
                       

                        List<string> alert = objIMangementLedgerRepo.TemporaryRegister(objLedgerCommonInfo, mode);
                       

                        if (alert.Count > 0)
                        {
                            string alertMessage = null;
                            foreach (var items in alert)
                            {
                                alertMessage += items + "\n";
                            }
                            TempData["alertmessage"] = alertMessage;
                            List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                            objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                            List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                            objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                            var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                            objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                            List<T_LED_WORK_HISTORY> workflow = (List<T_LED_WORK_HISTORY>)Session["Workflow"];
                            objLedgerCommonInfo.objT_LED_WORK_HISTORY = workflow;
                            objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
                            return View(objLedgerCommonInfo);
                        }
                        else
                        {
                            if (objLedgerCommonInfo.flowid != null)
                            {
                                if (Session["BackUrlIds"] != null)
                                {
                                    List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                                    string ids = null;
                                    string modes = null;
                                    FlowIds = (List<Tuple<string, string>>)Session["BackUrlIds"];

                                    foreach (var element in FlowIds)
                                    {
                                        if (objLedgerCommonInfo.LEDGER_FLOW_ID != Convert.ToInt32(element.Item1))
                                        {
                                            ids = element.Item1;
                                            modes = element.Item2;
                                            FlowIds.RemoveAll(item => item.Item1 == objLedgerCommonInfo.LEDGER_FLOW_ID.ToString());
                                            break;
                                        }
                                    }
                                    int? flowids = Convert.ToInt32(ids);
                                    int flowmodes = Convert.ToInt32(modes);
                                    Mode intmode = (Mode)Convert.ToInt32(modes);
                                    string operation = string.Empty;
                                    Session["BackUrlIds"] = FlowIds;
                                    objIMangementLedgerRepo.ClearSession();
                                    ModelState.Clear();
                                    LedgerCommonInfo result = GetMethod(Id = Convert.ToInt32(ids), intmode, flowids, flowmodes, operation);
                                    if (FlowIds.Count == 1)
                                    {
                                        Session["BackUrlIds"] = null;
                                    }
                                    return View(result);
                                }
                                else
                                {
                                    return RedirectToAction("ZC14030", "ZC140");
                                }
                            }
                            else
                            {
                                LedgerCommonInfo result = GetMethod(objLedgerCommonInfo.LEDGER_FLOW_ID, objLedgerCommonInfo.Mode = (Mode)5, null, null, objLedgerCommonInfo.hdnOperation = "temporary");
                                return View(result);
                            }
                        }
                    }

                    if (Request.Form["FlowCode"] == "6")
                    {
                       

                        List<string> alert = objIMangementLedgerRepo.Abolish(objLedgerCommonInfo);
                        
                        string alertMessage = string.Empty;
                        if (alert.Count > 0)
                        {
                            foreach (var items in alert)
                            {
                                alertMessage += items + "\n";
                            }
                            TempData["alertmessage"] = alertMessage;
                            List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                            objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                            List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                            objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                            var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                            objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                            List<T_LED_WORK_HISTORY> workflow = (List<T_LED_WORK_HISTORY>)Session["Workflow"];
                            objLedgerCommonInfo.objT_LED_WORK_HISTORY = workflow;
                            objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
                            return View(objLedgerCommonInfo);
                        }
                        else
                        {
                            if (objLedgerCommonInfo.flowid != null)
                            {
                                if (Session["BackUrlIds"] != null)
                                {
                                    List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                                    string ids = null;
                                    string modes = null;
                                    FlowIds = (List<Tuple<string, string>>)Session["BackUrlIds"];

                                    foreach (var element in FlowIds)
                                    {
                                        if (objLedgerCommonInfo.LEDGER_FLOW_ID != Convert.ToInt32(element.Item1))
                                        {
                                            ids = element.Item1;
                                            modes = element.Item2;
                                            FlowIds.RemoveAll(item => item.Item1 == objLedgerCommonInfo.LEDGER_FLOW_ID.ToString());
                                            break;
                                        }
                                    }
                                    int? flowids = Convert.ToInt32(ids);
                                    int flowmodes = Convert.ToInt32(modes);
                                    Mode intmode = (Mode)Convert.ToInt32(modes);
                                    string operation = string.Empty;
                                    Session["BackUrlIds"] = FlowIds;
                                    objIMangementLedgerRepo.ClearSession();
                                    ModelState.Clear();
                                    LedgerCommonInfo result = GetMethod(Id = Convert.ToInt32(ids), intmode, flowids, flowmodes, operation);
                                    if (FlowIds.Count == 1)
                                    {
                                        Session["BackUrlIds"] = null;
                                    }
                                    return View(result);
                                }
                                else
                                {
                                    return RedirectToAction("ZC14030", "ZC140");
                                }

                            }
                            else
                            {
                                LedgerCommonInfo result = GetMethod(objLedgerCommonInfo.LEDGER_FLOW_ID, objLedgerCommonInfo.Mode = (Mode)5, null, null, objLedgerCommonInfo.hdnOperation = "Delete");
                                return View(result);
                            }
                        }
                    }
                    if (Request.Form["FlowCode"] == "9")
                    {
                        
                        List<string> alert = objIMangementLedgerRepo.Withdraw(objLedgerCommonInfo);
                        
                        string alertMessage = string.Empty;
                        if (alert.Count > 0)
                        {
                            foreach (var items in alert)
                            {
                                alertMessage += items + "\n";
                            }
                            TempData["alertmessage"] = alertMessage;
                            List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                            objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                            List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                            objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                            var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                            objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                            List<T_LED_WORK_HISTORY> workflow = (List<T_LED_WORK_HISTORY>)Session["Workflow"];
                            objLedgerCommonInfo.objT_LED_WORK_HISTORY = workflow;
                            objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
                            return View(objLedgerCommonInfo);
                        }
                        else
                        {
                            if (objLedgerCommonInfo.flowid != null)
                            {
                                if (Session["BackUrlIds"] != null)
                                {
                                    List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                                    string ids = null;
                                    string modes = null;
                                    FlowIds = (List<Tuple<string, string>>)Session["BackUrlIds"];

                                    foreach (var element in FlowIds)
                                    {
                                        if (objLedgerCommonInfo.LEDGER_FLOW_ID != Convert.ToInt32(element.Item1))
                                        {
                                            ids = element.Item1;
                                            modes = element.Item2;
                                            FlowIds.RemoveAll(item => item.Item1 == objLedgerCommonInfo.LEDGER_FLOW_ID.ToString());
                                            break;
                                        }
                                    }
                                    int? flowids = Convert.ToInt32(ids);
                                    int flowmodes = Convert.ToInt32(modes);
                                    Mode intmode = (Mode)Convert.ToInt32(modes);
                                    string operation = string.Empty;
                                    Session["BackUrlIds"] = FlowIds;
                                    objIMangementLedgerRepo.ClearSession();
                                    ModelState.Clear();
                                    LedgerCommonInfo result = GetMethod(Id = Convert.ToInt32(ids), intmode, flowids, flowmodes, operation);
                                    if (FlowIds.Count == 1)
                                    {
                                        Session["BackUrlIds"] = null;
                                    }
                                    return View(result);
                                }
                                else
                                {
                                    return RedirectToAction("ZC14030", "ZC140");
                                }

                            }
                            else
                            {
                                LedgerCommonInfo result = GetMethod(objLedgerCommonInfo.LEDGER_FLOW_ID, objLedgerCommonInfo.Mode = (Mode)5, null, null, objLedgerCommonInfo.hdnOperation = "Delete");
                                return View(result);
                            }
                        }
                    }
                    if (Request.Form["FlowCode"] == "4")
                    {
                        List<string> alert = DoUpdateForProcessApproval(objLedgerCommonInfo);
                        string alertMessage = string.Empty;
                        if (alert.Count > 0)
                        {
                            foreach (var items in alert)
                            {
                                alertMessage += items + "\n";
                            }
                            TempData["alertmessage"] = alertMessage;
                            List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                            objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                            List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                            objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                            var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                            objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                            List<T_LED_WORK_HISTORY> workflow = (List<T_LED_WORK_HISTORY>)Session["Workflow"];
                            objLedgerCommonInfo.objT_LED_WORK_HISTORY = workflow;
                            objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
                            return View(objLedgerCommonInfo);
                        }
                        else
                        {

                            if (Session["BackUrlIds"] != null)
                            {
                                List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                                string ids = null;
                                string modes = null;
                                FlowIds = (List<Tuple<string, string>>)Session["BackUrlIds"];

                                foreach (var element in FlowIds)
                                {
                                    if (objLedgerCommonInfo.LEDGER_FLOW_ID != Convert.ToInt32(element.Item1))
                                    {
                                        ids = element.Item1;
                                        modes = element.Item2;
                                        FlowIds.RemoveAll(item => item.Item1 == objLedgerCommonInfo.LEDGER_FLOW_ID.ToString());
                                        break;
                                    }
                                }
                                int? flowids = Convert.ToInt32(ids);
                                int flowmodes = Convert.ToInt32(modes);
                                Mode intmode = (Mode)Convert.ToInt32(modes);
                                string operation = string.Empty;
                                Session["BackUrlIds"] = FlowIds;
                                objIMangementLedgerRepo.ClearSession();
                                ModelState.Clear();
                                LedgerCommonInfo result = GetMethod(Id = Convert.ToInt32(ids), intmode, flowids, flowmodes, operation);
                                if (FlowIds.Count == 1)
                                {
                                    Session["BackUrlIds"] = null;
                                }
                                return View(result);
                            }
                            else
                            {
                                return RedirectToAction("ZC14030", "ZC140");
                            }

                        }
                    }
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return View(objLedgerCommonInfo);
        }

        public LedgerCommonInfo GetMethod(int? Id, Mode mode, int? flowids, int? flowmodes, string operation)
        {
            LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
            var condition = new LedgerSearchModel();
            UserInformation user = (UserInformation)Session["LoginUserSession"];
            condition.LEDGER_FLOW_ID = Id;
            var d = new LedgerWorkDataInfo();
            objLedgerCommonInfo = objIMangementLedgerRepo.GetLedgerInfo(condition, type, mode);
            d.Dispose();
            objLedgerCommonInfo.Mode = (Mode)mode;

            if (mode == Mode.Copy)
            {
                objLedgerCommonInfo.LEDGER_FLOW_ID = null;
            }
            Session.Add(nameof(LedgerModel), objLedgerCommonInfo);


            if (Session["sessionOldRegisterNumbers"] != null)
            {
                List<LedgerOldRegisterNumberModel> objLedgeroldIds = (List<LedgerOldRegisterNumberModel>)Session["sessionOldRegisterNumbers"];
                objLedgerCommonInfo.objLedgerOldRegisterNumberModel = objLedgeroldIds;
            }
            else
            {
                var OldRegistrationnumbers = GetInputedOldRegisterNumbers(null, Id, mode, type);
                objLedgerCommonInfo.objLedgerOldRegisterNumberModel = OldRegistrationnumbers;
                Session["sessionOldRegisterNumbers"] = objLedgerCommonInfo.objLedgerOldRegisterNumberModel;
            }

            if (Session["StoringLocationDetails"] != null)
            {
                List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
            }
            else
            {
                var StoringLocation = GetInputedStoringLocations(null, Id, type, objLedgerCommonInfo.CHEM_CD);
                objLedgerCommonInfo.StorageLocation = StoringLocation;
                Session["StoringLocationDetails"] = objLedgerCommonInfo.StorageLocation;
            }

            if (Session[sessionUsageLocations] != null)
            {
                var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
            }
            else
            {
                var UsageLocation = GetInputedUsageLocations(null, Id, type);
                objLedgerCommonInfo.objLedgerUsageLocationModel = UsageLocation;
                Session[sessionUsageLocations] = objLedgerCommonInfo.objLedgerUsageLocationModel;
            }

            if (Session[sessionRegulations] != null)
            {
                List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
            }
            else
            {
                var Regulation = GetRegulation(objLedgerCommonInfo.CHEM_CD, Id, null);
                objLedgerCommonInfo.objLedgerRegulationModel = Regulation;
                Session[sessionRegulations] = objLedgerCommonInfo.objLedgerRegulationModel;
            }


            objLedgerCommonInfo.LookupUnitSizeMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.UNITSIZEMASTERINFO];
            objLedgerCommonInfo.LookupManufacturerName = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MANUFACTURENAME];
            var itemvalue = objLedgerCommonInfo.LookupManufacturerName.Where(item => item.Text == objLedgerCommonInfo.MAKER_NAME).ToList();

            if (itemvalue != null && itemvalue.Count() > 0)
            {
                objLedgerCommonInfo.MAKER_NAME = itemvalue[0].Value;
            }
            objLedgerCommonInfo.LookupReason = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.REASON];

            var Workflow = GetWorkflows(null, Id, mode);
            objLedgerCommonInfo.objT_LED_WORK_HISTORY = Workflow;
            Session["Workflow"] = Workflow;


            var btnresult = objIMangementLedgerRepo.EnablingDisblingButtons(mode, Id, objLedgerCommonInfo.CHEM_CD, objLedgerCommonInfo.HIERARCHY, objLedgerCommonInfo.APPROVAL_USER_CD, objLedgerCommonInfo.SUBSITUTE_USER_CD, user.User_CD, user.ADMIN_FLAG);

            objLedgerCommonInfo.btnApproval = btnresult.btnApproval;
            objLedgerCommonInfo.btnRemand = btnresult.btnRemand;
            objLedgerCommonInfo.btnChemRegister = btnresult.btnChemRegister;
            objLedgerCommonInfo.btnRegain = btnresult.btnRegain;
            objLedgerCommonInfo.btnsendback = btnresult.btnsendback;
            objLedgerCommonInfo.SendBackVisibility = btnresult.SendBackVisibility;
            objLedgerCommonInfo.btnRegister = btnresult.btnRegister;
            objLedgerCommonInfo.btndeletematter = btnresult.btndeletematter;
            objLedgerCommonInfo.btntemporarysave = btnresult.btntemporarysave;
            objLedgerCommonInfo.btnRevocation = btnresult.btnRevocation;
            objLedgerCommonInfo.hdnOperation = operation;
            objLedgerCommonInfo.FLOW_Mode = flowmodes;
            objLedgerCommonInfo.flowid = flowids;
            return objLedgerCommonInfo;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 21-11-2019  
        /// Description :Comes Back To the previous page
        /// <summary>
        /// 

        public ActionResult RegistrationBackbutton()
        {

            objIMangementLedgerRepo.ClearSession();
            return RedirectToAction("ZC14010", "ZC140");
        }

        public JsonResult GetLedgerRegulationIdsDetails()
        {
            M_GROUP objgroup = new M_GROUP();
            List<string> errormessage = new List<string>();
            List<string> data = new List<string>();
            errormessage = GetLedgerRegulationDetails();

            if (Session["LedgerRegulationDetails"] != null)
            {
                objgroup = (M_GROUP)Session["LedgerRegulationDetails"];
                data.Add(objgroup.GROUP_CD);
                data.Add(objgroup.GROUP_NAME);
            }

            if (errormessage.Count > 0)
            {
                return Json(errormessage, JsonRequestBehavior.AllowGet);
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }

        public List<string> GetLedgerRegulationDetails()
        {
            var returnValue = Session[SessionConst.CategorySelectReturnValue];
            List<string> errormessage = new List<string>();
            var selectValue = (Dictionary<string, string>)returnValue;
            var group = objIMangementLedgerRepo.GroupList();
            var groupMaster = new GroupMasterInfo();
            var id = ((UserInformation)(Session["LoginUserSession"])).GROUP_CD;
            M_GROUP objgroup = new M_GROUP();
            if (id != null)
            {
                var grp = groupMaster.GetChildGroups(id, true);
                if (mode == Mode.New || mode == Mode.Edit)
                {
                    var parent = groupMaster.GetParentGroups(id, false, 1);
                    grp = grp.Concat(parent);
                }
                if (grp.Count(n => n == group.Id) > 0)
                {
                    objgroup.GROUP_CD = group.Id;
                    objgroup.GROUP_NAME = group.Name;
                }
                else
                {
                    errormessage.Add(WarningMessages.GroupSelect);
                }
            }
            if (objgroup.GROUP_CD != null && objgroup.GROUP_CD != "")
            {
                Session.Add("LedgerRegulationDetails", objgroup);
            }

            return errormessage;
        }

        public string GetLedgerstorageDetails()
        {
            string Product = null;
            try
            {
                M_STORING_LOCATION objM_STORING_LOCATION = new M_STORING_LOCATION();

                var rowId = Session[SessionConst.RowId].ToString();
                int locationId = 0;
                var returnValue = Session[SessionConst.CategorySelectReturnValue];
                var selectValue = (Dictionary<string, string>)returnValue;
                //if (selectValue.Values.Count == 0) break;
                var storageLocations = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                if (selectValue.Count() > 0)
                {
                    locationId = selectValue.Values.AsEnumerable().Single().ToInt();
                }
                var currentLocation = storageLocations.Where(x => x.RowId == rowId).Single();
                using (var locationInfo = new LocationMasterInfo(LocationClassId.Storing))
                {
                    var location = locationInfo.GetData(locationId);
                    if (location != null)
                    {
                        currentLocation.LOC_ID = location.LOC_ID;
                        currentLocation.LOC_NAME = location.LOC_NAME;
                        currentLocation.FLG_LOC_HAZARD = true;

                    }
                }

                if (Session["StoringLocationDetails"] != null)
                {
                    List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];

                    if (ledgerlocationmodel != null && ledgerlocationmodel.Count > 0)
                    {
                        for (int i = 0; i < ledgerlocationmodel.Count; i++)
                        {
                            if (ledgerlocationmodel[i].RowId == rowId)
                            {
                                ledgerlocationmodel[i].LOC_NAME = currentLocation.LOC_NAME;
                            }
                        }
                    }


                    LedgerCommonInfo LedgerCommonInfomodel = new LedgerCommonInfo();

                    LedgerCommonInfomodel = GetLookupValues(LedgerCommonInfomodel);

                    Session["StoringLocationDetails"] = ledgerlocationmodel;
                    if (ledgerlocationmodel != null && ledgerlocationmodel.Count > 0)
                    {
                        int i = 1;
                        foreach (var items in ledgerlocationmodel)
                        {
                            if (items.AMOUNT == null)
                            {
                                items.AMOUNT = "";
                            }

                            Product += "<tr><td align='Center'><input type='checkbox' id=" + items.RowId + " name='Check" + items.RowId + "' align='right' class=productChekbox value='" + items.RowId + "' onclick='return uncheckStorageheader();' ></td>" +
                                          "<td class='lbllocationid'><label id='LOC_NAME" + items.RowId + "' style='display:inline-block;width:480px;align='Left' class='lblStoragelocName' >" + items.LOC_NAME + "</label><input type='hidden' id='LOC_ID' name='LOC_ID' value='" + items.LOC_ID + "' /><input type='hidden' id='LOC_ID' name='LOC_ID' value='" + items.FLG_LOC_HAZARD + "' />" +

                                          "<input type='button' align='Right' id='StorageLocationSelect" + items.RowId + "' name ='StorageLocationSelect" + items.RowId + "' value='選択' style ='width: 50px;' class='btnstorageLocation' onclick=StorageLocationselect('" + items.RowId + "')></td>" +

                                          "<td style='white-space: nowrap;'><input type='text'  id='AMOUNT' name='AMOUNT" + items.RowId + "' value='" + items.AMOUNT + "' class='required capacity' style='width: 72px;' onchange=ProductCapacity(this.value,'" + items.RowId + "') onkeypress='return isNumberKey(event)'>&nbsp;<select id='" + items.UNITSIZE_ID + "' name='SiglebyteCapacity" + i + "' class='required SiglebyteCapacity' Style='width: 95px;' onchange=Density(this.value,'" + items.RowId + "')></select></td>";
                            i++;
                        }
                    }
                }
                return Product;
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return Product;
        }

        [HttpGet]
        public ActionResult ZC14030(string SearchTarget)
        {
            LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
            LedgerSearchModel objLedgerSearchModel = new LedgerSearchModel();
            try
            {
                string adminFlag = "";
                WebCommonInfo webCommon = new WebCommonInfo();

                if (SearchTarget == "0")
                {
                    Session["Matterlist"] = null;
                }
                if (Session["LoginUserSession"] != null)
                {
                    ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];
                    adminFlag = appUser.ADMIN_FLAG;
                }
                else
                {
                    return RedirectToAction("ZC01001", "ZC010");
                }

                if (Session["Matterlist"] != null)
                {
                    objLedgerCommonInfo = (LedgerCommonInfo)Session["Matterlist"];
                    objLedgerSearchModel.FLOW_SEARCH_TARGET = objLedgerCommonInfo.FLOW_SEARCH_TARGET;
                    objLedgerCommonInfo.ChemCommonList = objIMangementLedgerRepo.SearchExecute(objLedgerSearchModel).ToList();

                }
                else
                {
                    objLedgerSearchModel.FLOW_SEARCH_TARGET = 0;
                    List<LedgerCommonInfo> objRecords = objIMangementLedgerRepo.SearchExecute(objLedgerSearchModel).ToList();
                    objLedgerCommonInfo.ChemCommonList = objRecords;
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return View(objLedgerCommonInfo);
        }

        [HttpPost]
        public ActionResult ZC14030(LedgerSearchModel objLedgerSearchModel)
        {
            LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
            try
            {
                List<LedgerCommonInfo> objRecords = objIMangementLedgerRepo.SearchExecute(objLedgerSearchModel).ToList();
                objLedgerCommonInfo.ChemCommonList = objRecords;
                objLedgerCommonInfo.FLOW_SEARCH_TARGET = objLedgerSearchModel.FLOW_SEARCH_TARGET;
                Session["Matterlist"] = objLedgerCommonInfo;
                return View(objLedgerCommonInfo);
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return View(objLedgerCommonInfo);
        }


        [HttpPost]
        public ActionResult ClearSearch(LedgerCommonInfo searchResult)
        {
            try
            {
               
                Session["MaxSearchResultCount"] = null;
                Session["SessionList"] = null;
                
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return RedirectToAction("ZC14010", "ZC140");
        }

        [HttpPost]
        public ActionResult ListOutput(LedgerSearchModel objLedgerSearchModel)
        {
            try
            {
               

                LedgerCommonInfo legderCommonInfo = new LedgerCommonInfo();
                if (objLedgerSearchModel.ApplicationNo != null)
                {
                    objLedgerSearchModel.LEDGER_FLOW_ID = Convert.ToInt32(objLedgerSearchModel.ApplicationNo.TrimStart(new Char[] { '0' }));
                }
                objIMangementLedgerRepo.ListOutput(objLedgerSearchModel);
                legderCommonInfo.ChemCommonList = new List<LedgerCommonInfo>();
                legderCommonInfo = ddlLedgerList(legderCommonInfo);
                
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return View();
        }


        [HttpGet]
        public ActionResult ZC14021(IEnumerable<LedgerProtectorModel> objLedgerProtector, string RowId, string StatusId, string ModeValue)
        {
            LedgerUsageLocationModel ledgerUsageLocationModel = new LedgerUsageLocationModel();
            try
            {
                List<LedgerRiskReduceModel> objLedgerRisk = new List<LedgerRiskReduceModel>();
                LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();

                if (!string.IsNullOrEmpty(RowId))
                {
                    rowId = RowId;
                    ledgerUsageLocationModel = GetLedgerUsageLocationInfo(rowId);


                    if (ledgerUsageLocationModel.objLedgerProtectorModel == null)
                    {
                        List<LedgerProtectorModel> objLedger = objIMangementLedgerRepo.GetInputedProtectors(objLedgerProtector).ToList();
                        ledgerUsageLocationModel.objLedgerProtectorModel = objLedger;
                        Session[sessionProtectors] = ledgerUsageLocationModel.objLedgerProtectorModel;
                    }
                    if (ledgerUsageLocationModel.objLedgerRisk == null)
                    {
                        List<LedgerRiskReduceModel> objLedgerRiskReduce = objIMangementLedgerRepo.GetInputedRiskReduces(objLedgerRisk).ToList();
                        ledgerUsageLocationModel.objLedgerRisk = objLedgerRiskReduce;
                        Session[sessionRiskReduces] = ledgerUsageLocationModel.objLedgerRisk;
                    }
                    Session.Add(nameof(LedgerUsageLocationModel), ledgerUsageLocationModel);

                }
                else
                {
                    ClearSession();
                    List<LedgerProtectorModel> objLedger = objIMangementLedgerRepo.GetInputedProtectors(objLedgerProtector).ToList();
                    ledgerUsageLocationModel.objLedgerProtectorModel = objLedger;
                    List<LedgerRiskReduceModel> objLedgerRiskReduce = objIMangementLedgerRepo.GetInputedRiskReduces(objLedgerRisk).ToList();
                    ledgerUsageLocationModel.objLedgerRisk = objLedgerRiskReduce;
                    Session[sessionRiskReduces] = ledgerUsageLocationModel.objLedgerRisk;
                    Session[sessionProtectors] = ledgerUsageLocationModel.objLedgerProtectorModel;

                }
                ledgerUsageLocationModel = ddlUsageLocList(ledgerUsageLocationModel);
                ledgerUsageLocationModel.StatusId = StatusId;
                ledgerUsageLocationModel._Mode = Convert.ToInt32(ModeValue);
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return View(ledgerUsageLocationModel);
        }

        public LedgerUsageLocationModel GetLedgerUsageLocationInfo(string rowId)
        {
            if (!string.IsNullOrEmpty(rowId))
            {
                var usageLocations = (IEnumerable<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                var usageLocation = usageLocations.Where(x => x.RowId == rowId).SingleOrDefault();
                if (Session[sessionRiskReduces] != null)
                {
                    usageLocation.objLedgerRisk = (List<LedgerRiskReduceModel>)Session[sessionRiskReduces];
                }
                if (Session[sessionProtectors] != null)
                {
                    usageLocation.objLedgerProtectorModel = (List<LedgerProtectorModel>)Session[sessionProtectors];
                }
                usageLocation.Mode = mode;
                return usageLocation;
            }
            return new LedgerUsageLocationModel();
        }

        [HttpPost]
        public JsonResult ZC14021(LedgerUsageLocationModel usageLocation)
        {
            try
            {
                usageLocation = ddlUsageLocList(usageLocation);
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());

            }
            return Json(JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult ZC14050()
        {
            LedgerCommonInfo LedgerCommonInforesult = new LedgerCommonInfo();
            try
            {
                string adminFlag = "";
                WebCommonInfo webCommon = new WebCommonInfo();
                if (Session["LoginUserSession"] != null)
                {
                    ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];
                    adminFlag = appUser.ADMIN_FLAG;
                }
                else
                {
                    return RedirectToAction("ZC01001", "ZC010");
                }
                LedgerCommonInforesult.LedgerCommonList = (List<LedgerCommonInfo>)Session["list"];
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return View(LedgerCommonInforesult);
        }

        [HttpGet]
        public string ReferenceHistory(string ledgerflowid)
        {
            string REGNOS = null;
            string IDS = null;
            List<string> ids = new List<string>();
            List<string> regids = new List<string>();
            string[] Ledgerflowids = ledgerflowid.Split(',');
            try
            {
                for (int i = 0; i < Ledgerflowids.Length; i++)
                {
                    List<string> REGNO = GetSelectedLedger(Ledgerflowids[i]);
                    if (REGNO.Count() != 0)
                    {
                        REGNOS += REGNO[0] + ",";
                    }
                    if (REGNO.Count() == 0)
                    {
                        IDS += Ledgerflowids[i] + ",";
                    }
                }

                if (REGNOS != null)
                {
                    REGNOS = REGNOS.TrimEnd(',');
                }

                if (IDS != null)
                {
                    IDS = IDS.TrimEnd(',');
                }
                var qsId = string.Join(@"&", ids.Select(x => QueryString.IdKey + "=" + x));
                var list = new List<LedgerCommonInfo>();
                LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
                var context = DbUtil.Open(true);

                if (!string.IsNullOrEmpty(IDS))
                {
                    using (var dataInfo = new LedgerWorkDataInfo() { Context = context })
                    {
                        var data = dataInfo.GetSearchResult(new LedgerSearchModel() { LEDGER_FLOW_ID_SELECTION = IDS });
                        foreach (var d in data)
                        {
                            using (var hist = new LedgerHistoryDataInfo() { Context = context })
                            {
                                var h = hist.GetSearchResult(new LedgerSearchModel() { REG_NO = d.REG_NO });
                                list.AddRange(h);
                            }
                            list.Add(d);
                        }
                    }
                }

                if (!string.IsNullOrEmpty(REGNOS))
                {
                    using (var dataInfo = new LedgerHistoryDataInfo() { Context = context })
                    {
                        var data = dataInfo.GetSearchResult(new LedgerSearchModel() { REG_NO_SELECTION = REGNOS });
                        list.AddRange(data);
                    }
                }
                var qsNo = "";
                Session["list"] = list;
                if (REGNOS != "")
                {
                    qsNo = string.Join(@"&", regids.Select(x => QueryString.RegNoKey + "=" + x));
                }

                Response.Redirect(NikonFormPath.LedgerHistory + "?" + qsId + "&" + qsNo);
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return "";
        }

        public void RiskBeforeEvaluate_Command(LedgerModel objLedgerModel)
        {
            objIMangementLedgerRepo.RiskBeforeEvaluate(objLedgerModel, true);
        }

        [HttpPost]
        public string AddContainedIds(string ContainedIds)
        {
            try
            {
                if (Session[sessionContaineds] != null)
                {
                    string MergedIds = null;
                    string SessionIds = (string)Session[sessionContaineds];
                    if (!string.IsNullOrEmpty(ContainedIds))
                    {
                        MergedIds = string.Join(",", ContainedIds, SessionIds);
                        Session[sessionContaineds] = MergedIds;
                    }
                }
                else
                {
                    Session[sessionContaineds] = ContainedIds;
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }

            return "";
        }

        public JsonResult GetContainedDetails(string RegisterChemCd)
        {
            List<LedgerCommonInfo> chemContainedmodellist = new List<LedgerCommonInfo>();
            LedgerCommonInfo csmscommoninfo = new LedgerCommonInfo();
            List<string> objLedgerCommonInfo = new List<string>();

            try
            {
                if (Session[sessionContaineds] != null || RegisterChemCd != "")
                {
                    string[] SplitContainedId = new string[] { }; string first = "";
                    string chemContaineds = (string)Session[sessionContaineds];
                    if (chemContaineds != "" && chemContaineds != null)
                    {
                        SplitContainedId = chemContaineds.Split(',');
                        first = SplitContainedId.First();
                    }
                    else
                    {
                        first = RegisterChemCd;
                    }

                    LedgerCommonInfo chemcontainedmodel = new LedgerCommonInfo();
                    LedgerSearchModel objchemcontainedmodel = new LedgerSearchModel();
                    List<ChemDataInfo> chemContainedmodelnew = new List<ChemDataInfo>();
                    CsmsCommonInfo csmsCommon = new CsmsCommonInfo();
                    var condition = new ChemSearchModel();
                    condition.CHEM_CD = first;
                    var d = new ChemDataInfo();
                    var dataArray = d.GetSearchResult(condition);
                    objchemcontainedmodel.CHEM_CD = condition.CHEM_CD;
                    GetRegulation(condition.CHEM_CD, null, null);
                    var regulations = (List<LedgerRegulationModel>)Session[sessionRegulations];

                    var ids = new List<int>();
                    foreach (var data in regulations)
                    {
                        ids.Add(Convert.ToInt32(data.REG_TYPE_ID));
                    }
                    var regulationInfo = new Nikon.LedRegulationMasterInfo();
                    var items = regulationInfo.GetSelectedItems(ids).AsQueryable();
                    chemcontainedmodel.REQUIRED_CHEM = items.Any(x => x.WORKTIME_MGMT == 1) ? "該当" : "非該当";

                    var hazmatText = items.FirstOrDefault(x => x.REG_TEXT.IndexOf("消防法") != -1)?.REG_TEXT;
                    chemcontainedmodel.HAZMAT_TYPE = hazmatText ?? "非該当";
                    if (objchemcontainedmodel.CHEM_CD == NikonConst.UnregisteredChemCd)
                    {
                        chemcontainedmodel.REQUIRED_CHEM = null;
                        chemcontainedmodel.HAZMAT_TYPE = null;
                    }

                    if (dataArray != null && dataArray.Count() > 0)
                    {
                        csmsCommon.ChemCommonList = dataArray.ToList();
                        chemcontainedmodel.CAS_NO = csmsCommon.ChemCommonList[0].CAS_NO;
                        chemcontainedmodel.CHEM_CD = csmsCommon.ChemCommonList[0].CHEM_CD;
                        chemcontainedmodel.FIGURE_NAME = csmsCommon.ChemCommonList[0].FIGURE_NAME;
                        chemcontainedmodel.CHEM_CAT = csmsCommon.ChemCommonList[0].CHEM_CAT;
                        chemcontainedmodel.CHEM_NAME = csmsCommon.ChemCommonList[0].CHEM_NAME;
                        chemcontainedmodel.REQUIRED_CHEM = chemcontainedmodel.REQUIRED_CHEM;
                        chemcontainedmodel.HAZMAT_TYPE = chemcontainedmodel.HAZMAT_TYPE;

                        if (Session[sessionUsageLocations] != null)
                        {
                            chemcontainedmodel.hdnmode = 5;
                        }
                        chemcontainedmodel.FLG_RISK_ASSESSMENT = false;
                        objLedgerCommonInfo.Add(chemcontainedmodel.CAS_NO);
                        objLedgerCommonInfo.Add(chemcontainedmodel.CHEM_CD);
                        objLedgerCommonInfo.Add(chemcontainedmodel.FIGURE_NAME);
                        objLedgerCommonInfo.Add(chemcontainedmodel.CHEM_CAT);
                        objLedgerCommonInfo.Add(chemcontainedmodel.CHEM_NAME);
                        objLedgerCommonInfo.Add(chemcontainedmodel.REQUIRED_CHEM);
                        objLedgerCommonInfo.Add(chemcontainedmodel.HAZMAT_TYPE);
                        objLedgerCommonInfo.Add(chemcontainedmodel.FLG_RISK_ASSESSMENT.ToString());
                        objLedgerCommonInfo.Add(chemcontainedmodel.hdnmode.ToString());
                        chemContainedmodellist.Add(chemcontainedmodel);

                    }
                    csmscommoninfo.ContainedModel = chemContainedmodellist;
                    Session["Contained"] = csmscommoninfo.ContainedModel;
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return Json(objLedgerCommonInfo, JsonRequestBehavior.AllowGet);
        }

        public string GetRegulationResult()
        {
            string data = string.Empty;
            try
            {
                LedgerCommonInfo objdata = new LedgerCommonInfo();
                objdata.RegulationList = (List<LedgerRegulationModel>)Session[sessionRegulations];
                if (objdata.RegulationList.Count > 0)
                {
                    foreach (var items in objdata.RegulationList)
                    {
                        data += "<tr><td align='Center'><img src='" + items.IconUrl + "' style='height:32px;'/></td><td><font style='vertical-align:inherit;'><font style='vertical-align: inherit;'>" + items.REG_TEXT + "</font></font></td></tr>";
                    }
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return data;
        }

        [HttpPost]
        public string AddOldRegisterNumbers()
        {
            List<LedgerOldRegisterNumberModel> OldRegisterNumbers = new List<LedgerOldRegisterNumberModel>();
            string OldRegisterNumber = null;
            try
            {
                if (Session["sessionOldRegisterNumbers"] != null)
                {
                    List<LedgerOldRegisterNumberModel> chemmodel = (List<LedgerOldRegisterNumberModel>)Session["sessionOldRegisterNumbers"];
                    LedgerOldRegisterNumberModel data = new LedgerOldRegisterNumberModel();
                    data.REG_OLD_NO = null;
                    data.REG_OLD_NO_SEQ = chemmodel.Count() + 1;
                    chemmodel.Add(data);
                    Session["sessionOldRegisterNumbers"] = chemmodel;
                    OldRegisterNumber = "<tr><td align='Center'><input type='checkbox' name='OldRegisterCheckbox' align='right' id=" + data.REG_OLD_NO_SEQ + " onclick='return uncheckheader();' ></td><td align='Center'><input type='text' name='ChemAliasModel' style='Width: 100%' class='aliasName' onchange='return LedgerOldValuesOnchange(this.value," + data.REG_OLD_NO_SEQ + ");'></td></tr>";

                }
                else
                {
                    var data = new LedgerOldRegisterNumberModel();
                    data.REG_OLD_NO = null;
                    data.REG_OLD_NO_SEQ = 1;
                    OldRegisterNumbers.Add(data);
                    Session["sessionOldRegisterNumbers"] = OldRegisterNumbers;
                    OldRegisterNumber = "<tr><td align='Center'><input type='checkbox' name='OldRegisterCheckbox' align='right' id=" + data.REG_OLD_NO_SEQ + " onclick='return uncheckheader();' ></td><td align='Center'><input type='text' name='ChemAliasModel' style='Width: 100%' class='aliasName' onchange='return LedgerOldValuesOnchange(this.value," + data.REG_OLD_NO_SEQ + ");'></td></tr>";
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return OldRegisterNumber;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 09-9-2019  
        /// Description :updates alias Names
        /// <summary>
        public void UpdateLedgerOldlist(string LedgerOldName, int Rowid)
        {
            try
            {
                if (Session["sessionOldRegisterNumbers"] != null)
                {
                    List<LedgerOldRegisterNumberModel> objLedger = new List<LedgerOldRegisterNumberModel>();
                    objLedger = (List<LedgerOldRegisterNumberModel>)Session["sessionOldRegisterNumbers"];
                    for (int i = 0; i < objLedger.Count; i++)
                    {
                        if (objLedger[i].REG_OLD_NO_SEQ == Rowid)
                        {
                            objLedger[i].REG_OLD_NO = LedgerOldName;
                        }
                    }

                    Session.Add("sessionOldRegisterNumbers", objLedger);
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());

            }
        }

        public string DeleteOldRegisterNumbers(string Rowid)
        {
            string OldRegisterNumbers = "true";
            try
            {
                List<LedgerOldRegisterNumberModel> OldRegisterNumberExists = (List<LedgerOldRegisterNumberModel>)Session["sessionOldRegisterNumbers"];
                if (OldRegisterNumberExists != null && OldRegisterNumberExists.Count > 0)
                {

                    string[] values = Rowid.Split(',');
                    for (int J = 0; J < OldRegisterNumberExists.Count; J++)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (OldRegisterNumberExists[J].REG_OLD_NO_SEQ == Convert.ToInt32(values[i]))
                            {
                                OldRegisterNumberExists.Remove(OldRegisterNumberExists[J]);
                            }

                        }
                    }
                    Session["sessionOldRegisterNumbers"] = OldRegisterNumberExists;
                    OldRegisterNumbers = "true";
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
                OldRegisterNumbers = "false";
            }
            return OldRegisterNumbers;
        }

        public string ValidateAbolish(LedgerCommonInfo model)
        {
            model = objIMangementLedgerRepo.ClientsideValidation(model);

            var errorMessages = new List<string>();
            string Messages = "";
            if (model.APPLI_CLASS == (int)AppliClass.Edit || model.APPLI_CLASS == (int)AppliClass.Stopped)
            {
                if (model.REASON_ID == null)
                    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName(model, "REASON_ID")));
            }
            if (model.REASON_ID == LedgerModel.OTHER_REASON_ID && string.IsNullOrWhiteSpace(model.OTHER_REASON))
            {
                errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName(model, "OTHER_REASON")));
            }
            var stock = new ClsStock();
            if (stock.HasChemStock(model.CHEM_CD, model.REG_NO))
            {
                errorMessages.Add(string.Format(WarningMessages.HasChemStock));
            }
            if (errorMessages.Count > 0)
            {
                foreach (var Errormessage in errorMessages)
                {
                    Messages += Errormessage + "\n";
                }
            }
            return Messages;
        }


        [HttpPost]
        public string AddStoringLocationValues()
        {
            string Product = null;
            try
            {
                LedgerLocationModel data = new LedgerLocationModel();
                LedgerCommonInfo LedgerCommonInfomodel = new LedgerCommonInfo();


                LedgerCommonInfomodel = GetLookupValues(LedgerCommonInfomodel);

                if (Session["StoringLocationDetails"] != null)
                {
                    List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                    var rowId = data.RowId;
                    ledgerlocationmodel.Add(data);

                    Session["StoringLocationDetails"] = ledgerlocationmodel;

                    Product += "<tr><td align='Center'><input type='checkbox' id=" + rowId + " name='Check" + rowId + "' align='right' class=productChekbox value='" + data.RowId + "' onclick='return uncheckStorageheader();'></td>" +
                                "<td class='lbllocationid'><label id='LOC_NAME" + rowId + "' style='display:inline-block;width:480px;align='Left' class='lblStoragelocName' ></label>" +
                                "<input type='button' align='Right' id='StorageLocationSelect" + rowId + "' name ='StorageLocationSelect" + rowId + "' value='選択' style ='width: 50px;' class='btnstorageLocation' onclick=StorageLocationselect('" + rowId + "')></td>" +
                                "<td style='white-space: nowrap;'><input type='text' id='AMOUNT' name='AMOUNT" + rowId + "' value='" + data.AMOUNT + "' class='required capacity' style='width: 72px;' onchange=ProductCapacity(this.value,'" + rowId + "') " +
                                 "onkeypress='return isNumberKey(event)'>&nbsp;<select id='SiglebyteCapacity' name='SiglebyteCapacity" + rowId + "' value='" + data.UNITSIZE_ID + "' class='required SiglebyteCapacity' Style='width: 95px;' onchange=Density(this.value,'" + data.RowId + "')></select></td>";

                }
                else
                {
                    List<LedgerLocationModel> ledgerlocationmodel = new List<LedgerLocationModel>();
                    var rowId = data.RowId;
                    ledgerlocationmodel.Add(data);

                    Session["StoringLocationDetails"] = ledgerlocationmodel;

                    Product += "<tr><td align='Center'><input type='checkbox' id=" + rowId + " name='Check' align='right' class=productChekbox value='" + data.RowId + "' onclick='return uncheckStorageheader();'></td>" +
                                "<td class='lbllocationid'><label id='LOC_NAME' style='display:inline-block;width:480px;align='Left' class='lblStoragelocName' ></label>" +
                                "<input type='button' align='Right' id='StorageLocationSelect" + rowId + "' name ='StorageLocationSelect" + rowId + "' value='選択' style ='width: 50px;' class='btnstorageLocation' onclick=StorageLocationselect('" + rowId + "')></td>" +
                                "<td style='white-space: nowrap;'><input type='text' id='AMOUNT' name='AMOUNT' value='" + data.AMOUNT + "' class='required capacity' style='width: 72px;' onchange=ProductCapacity(this.value,'" + rowId + "') " +
                                 "onkeypress='return isNumberKey(event)'>&nbsp;<select id='SiglebyteCapacity' name='SiglebyteCapacity" + rowId + "' value='" + data.UNITSIZE_ID + "' class='required SiglebyteCapacity' Style='width: 95px;' onchange=Density(this.value,'" + data.RowId + "')></select></td>";
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return Product;
        }

        public LedgerCommonInfo GetLookupValues(LedgerCommonInfo dropdownlist)
        {
            dropdownlist.LookupUnitSizeMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.UNITSIZEMASTERINFO];
            return dropdownlist;
        }

        public void UpdateCapacity(string Capacity, string Rowid)
        {
            try
            {
                if (Session["StoringLocationDetails"] != null)
                {
                    List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                    for (int i = 0; i < ledgerlocationmodel.Count; i++)
                    {
                        if (ledgerlocationmodel[i].RowId == Rowid)
                        {
                            ledgerlocationmodel[i].AMOUNT = Capacity;
                        }
                    }
                    Session.Add("StoringLocationDetails", ledgerlocationmodel);
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
        }

        public void UpdateDensity(string Density, string Rowid)
        {
            try
            {
                if (Session["StoringLocationDetails"] != null)
                {
                    List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                    for (int i = 0; i < ledgerlocationmodel.Count; i++)
                    {
                        if (ledgerlocationmodel[i].RowId == Rowid)
                        {
                            ledgerlocationmodel[i].UNITSIZE_ID = Convert.ToInt32(Density);
                        }
                    }
                    Session.Add("StoringLocationDetails", ledgerlocationmodel);
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
        }

        [HttpPost]
        /// <summary>
        /// Author: MSCGI
        /// Created date: 28-8-2019  
        /// Description :this Method loads the Maker Name and Unitsize_id
        /// <summary>
        public ActionResult UnitSize()

        {
            List<SelectListItem> listofUnitSize = new List<SelectListItem> { };
            object[] ArrayList = new object[] { };
            try
            {
                new UnitSizeMasterInfo().GetSelectList().ToList().
                    ForEach(x => listofUnitSize.Add(new SelectListItem
                    { Value = x.Id.ToString(), Text = x.Name }));
                ArrayList = new object[] { listofUnitSize };
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return Json(ArrayList);
        }

        public string DeleteLedgerLocation(string Rowid)
        {
            string Alias = "true";
            try
            {
                List<LedgerLocationModel> LedgerLocationExists = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                if (LedgerLocationExists != null && LedgerLocationExists.Count > 0)
                {
                    string[] values = Rowid.Split(',');
                    for (int J = 0; J < LedgerLocationExists.Count; J++)
                    {
                        for (int i = 0; i < values.Length; i++)
                        {
                            if (LedgerLocationExists[J].RowId == values[i])
                            {
                                LedgerLocationExists.Remove(LedgerLocationExists[J]);
                            }
                        }
                    }
                    Session["StoringLocationDetails"] = LedgerLocationExists;
                    Alias = "true";
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return Alias;

        }

        [HttpGet]
        public ActionResult ZC14040()
        {
            LedgerCommonInfo legderCommonInfo = new LedgerCommonInfo();
            try
            {
                string adminFlag = "";
                WebCommonInfo webCommon = new WebCommonInfo();
                if (Session["LoginUserSession"] != null)
                {
                    ApplicationUser appUser = (ApplicationUser)Session["LoginUserSession"];
                    adminFlag = appUser.ADMIN_FLAG;
                }
                else
                {
                    return RedirectToAction("ZC01001", "ZC010");
                }

                legderCommonInfo = ddlLedgerList(legderCommonInfo);
                id = Request.QueryString[QueryString.IdKey];
                regno = Request.QueryString[QueryString.RegNoKey];
                legderCommonInfo.REG_NO = regno;
                legderCommonInfo._listLedgerModel = GetSearchResult();
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return View(legderCommonInfo);
        }

        [HttpPost]
        public ActionResult ZC14040(LedgerCommonInfo legderCommonInfo)
        {
            string message = null;
            try
            {
                legderCommonInfo.GROUP_CD = Request.Form["GROUP_SELECTION"].ToString();
                message = DoUpdateForProcess(legderCommonInfo);
                if (message != null)
                {
                    TempData["FailureMessage"] = message;
                    regno = legderCommonInfo.REG_NO;
                    legderCommonInfo._listLedgerModel = GetSearchResult();
                    legderCommonInfo = ddlLedgerList(legderCommonInfo);
                    return View(legderCommonInfo);
                }

                if (updFlg)
                {
                    message = string.Format("以下の内容で{0}しました。", "一括変更");
                    TempData["SuccessMessage"] = message;
                }
                regno = legderCommonInfo.REG_NO;
                legderCommonInfo._listLedgerModel = GetSearchResult();
                legderCommonInfo = ddlLedgerList(legderCommonInfo);
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return View(legderCommonInfo);
        }

        public List<LedgerCommonInfo> GetSearchResult()
        {
            var result = new List<LedgerCommonInfo>();
            try
            {
                using (var dataInfo = new LedgerDataInfo())
                {
                    result.AddRange(dataInfo.GetSearchResult(new LedgerSearchModel { REG_NO_SELECTION = regno, LEDGER_STATUS = (int)LedgerStatusConditionType.All, }));
                }
                Session.Add(SelectModels, result);
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return result.ToList();
        }

        public string BulkUpdate(string RegNo)
        {
            string ErrorStatus = null;
            try
            {
                var ids = GetSelectedLedgerWork(RegNo);
                if (ids.Count() > 0)
                {
                    ErrorStatus = WarningMessages.CannotBulkUpdate;
                    ErrorStatus = "false" + "|" + ErrorStatus;
                    return ErrorStatus;
                }
                var nos = GetSelectedLedger(RegNo);
                if (nos?.Count() == 0)
                    return "false" + "|" + ErrorStatus;
                if (MaxBulkChangeCount < nos?.Count())
                {
                    ErrorStatus = string.Format(WarningMessages.MaxSelectOver, MaxBulkChangeCount.ToString(), "一括変更");
                    ErrorStatus = "false" + "|" + ErrorStatus;
                    return ErrorStatus;
                }
                var qsNo = string.Join(@"&", nos.Select(x => QueryString.RegNoKey + "=" + x));
                ErrorStatus = "true" + "|" + qsNo;
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return ErrorStatus;
        }

        /// <summary>
        /// <para>検索結果から選択している管理台帳ワークを取得します。</para>
        /// <para>選択されていない場合は警告メッセージを表示します。</para>
        /// <para>複数選択不可時に複数選択されている場合は警告メッセージを表示します。</para>
        /// </summary>
        /// <returns>選択されている管理台帳の申請IDを取得します。選択されていない場合はnullを取得します。</returns>
        private IEnumerable<int> GetSelectedLedgerWork(string Id)
        {
            //var rows = ResultGrid.Rows;
            var selectionCode = new List<int>();

            if (Session["SessionList"] != null)
            {
                LedgerCommonInfo legderCommonInfo = new LedgerCommonInfo();
                legderCommonInfo.ChemCommonList = (List<LedgerCommonInfo>)Session["SessionList"];
                if (Id != null)
                {
                    string[] LedgerVal = Id.Split(',');

                    foreach (var items in legderCommonInfo.ChemCommonList)
                    {
                        foreach (var items1 in LedgerVal)
                        {
                            if (items.LEDGER_FLOW_ID.ToString() == items1.Trim())
                            {
                                var code = items.LEDGER_FLOW_ID.ToString();
                                var type = items.TableType;
                                if (string.IsNullOrWhiteSpace(code) != true && type == TableType.Work)
                                {
                                    selectionCode.Add(Convert.ToInt32(code));
                                }
                            }
                        }
                    }
                }
            }
            return selectionCode;
        }


        /// <summary>
        /// <para>検索結果から選択している管理台帳を取得します。</para>
        /// <para>選択されていない場合は警告メッセージを表示します。</para>
        /// <para>複数選択不可時に複数選択されている場合は警告メッセージを表示します。</para>
        /// </summary>
        /// <returns>選択されている管理台帳の登録番号を取得します。選択されていない場合はnullを取得します。</returns>    

        private List<string> GetSelectedLedger(string Id)
        {
            //var rows = ResultGrid.Rows;
            List<string> selectionCode = new List<string>();
            if (Session["SessionList"] != null)
            {
                LedgerCommonInfo legderCommonInfo = new LedgerCommonInfo();
                legderCommonInfo.ChemCommonList = (List<LedgerCommonInfo>)Session["SessionList"];
                if (Id != null)
                {
                    string[] LedgerVal = Id.Split(',');
                    foreach (var items in legderCommonInfo.ChemCommonList)
                    {
                        foreach (var items1 in LedgerVal)
                        {
                            if (items.LEDGER_FLOW_ID.ToString() == items1.Trim())
                            {
                                var code = items.REG_NO;
                                var type = items.TableType;
                                if (string.IsNullOrWhiteSpace(code) != true && type == TableType.Ledger)
                                {
                                    selectionCode.Add(code);
                                }
                            }
                        }
                    }
                }
            }
            return selectionCode;
        }

        /// <summary>
        /// 更新処理を行います。
        /// </summary>
        protected string DoUpdateForProcess(LedgerCommonInfo legderCommonInfo)
        {
            LedgerSearchModel condition = new LedgerSearchModel();
            List<LedgerCommonInfo> ledgerWorks = new List<LedgerCommonInfo>();
            string ErrorMessage = null;
            if (Session[ChangeValue] != null)
            {
                condition = (LedgerSearchModel)Session[ChangeValue];
            }
            if (Session[SelectModels] != null)
            {
                ledgerWorks = (List<LedgerCommonInfo>)Session[SelectModels];
            }

            updFlg = true;
            var errorMessages = new List<string>();
            foreach (var data in ledgerWorks)
            {
                var model = new LedgerUsageLocationModel();
                if (data.REG_TRANSACTION_VOLUME == null)
                {
                    updFlg = false;
                    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName((T_MGMT_LED_USAGE_LOC)model, "REG_TRANSACTION_VOLUME")));
                }

                if (data.REG_WORK_FREQUENCY == null)
                {
                    updFlg = false;
                    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName((T_MGMT_LED_USAGE_LOC)model, "REG_WORK_FREQUENCY")));
                }

                if (data.REG_DISASTER_POSSIBILITY == null)
                {
                    updFlg = false;
                    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName((T_MGMT_LED_USAGE_LOC)model, "REG_DISASTER_POSSIBILITY")));
                }

                if (!updFlg)
                {
                    errorMessages.Add("使用場所明細から登録してください。");
                }
            }
            if (errorMessages.Count > 0)
            {
                foreach (var items in errorMessages)
                {
                    ErrorMessage += items + "\n";
                }
                return ErrorMessage;
            }

            var context = DbUtil.Open(true);
            try
            {
                using (var dataInfo = new LedgerWorkDataInfo() { Context = context })
                {
                    foreach (var data in ledgerWorks)
                    {

                        if (!dataInfo.IsInfoSyncFromDB(data, true))
                        {
                            ErrorMessage = WarningMessages.AllreadyOperated;
                            return ErrorMessage;
                        }

                        if (!string.IsNullOrEmpty(legderCommonInfo.GROUP_CD))
                        {
                            data.GROUP_CD = legderCommonInfo.GROUP_CD;
                        }
                        if (legderCommonInfo.REASON != null)
                        {
                            data.REASON_ID = Convert.ToInt32(legderCommonInfo.REASON);
                        }
                        if (!string.IsNullOrEmpty(legderCommonInfo.OTHER_REASON))
                        {
                            data.OTHER_REASON = legderCommonInfo.OTHER_REASON;
                        }
                        data.FLOW_CD = Common.GetLogonForm().FLOW_CD;
                        data.TEMP_SAVED_FLAG = 0;
                        data.HIERARCHY = -1;
                        data.APPLI_CLASS = (int)AppliClass.Edit;
                        data.REG_USER_CD = Common.GetLogonForm().User_CD;
                        dataInfo.AddBulk(data);

                        using (var historyDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
                        {
                            historyDataInfo.HenkoApplication(data.LEDGER_FLOW_ID, Common.GetLogonForm().User_CD);
                        }
                        using (var work = new LedgerWorkOldRegisterNumberDataInfo() { Context = context })
                        {
                            using (var old = new LedgerOldRegisterNumberDataInfo() { Context = context })
                            {
                                var list = old.GetSearchResult(new LedgerOldRegisterNumberSearchModel() { REG_NO = data.REG_NO });
                                foreach (var item in list)
                                {
                                    item.LEDGER_FLOW_ID = data.LEDGER_FLOW_ID;
                                    work.Add((T_LED_WORK_REG_OLD_NO)item);
                                }
                            }
                        }

                        using (var work = new LedgerWorkRegulationDataInfo() { Context = context })
                        {
                            using (var old = new LedgerRegulationDataInfo() { Context = context })
                            {
                                var list = old.GetSearchResult(new LedgerRegulationSearchModel() { REG_NO = data.REG_NO });
                                foreach (var item in list)
                                {
                                    item.LEDGER_FLOW_ID = data.LEDGER_FLOW_ID;
                                    work.Add((T_LEDGER_WORK_REGULATION)item);
                                }
                            }
                        }

                        using (var work = new LedgerWorkLocationDataInfo() { Context = context })
                        {
                            using (var loc = new LedgerLocationDataInfo() { Context = context })
                            {
                                var list = loc.GetSearchResult(new LedgerLocationSearchModel() { REG_NO = data.REG_NO });
                                foreach (var item in list)
                                {
                                    item.LEDGER_FLOW_ID = data.LEDGER_FLOW_ID;
                                    work.Add((T_LED_WORK_LOC)item);
                                }
                            }
                        }
                        using (var work = new LedgerWorkUsageLocationDataInfo() { Context = context })
                        {
                            using (var usage = new LedgerUsageLocationDataInfo() { Context = context })
                            {
                                var list = usage.GetSearchResult(new LedgerUsageLocationSearchModel() { REG_NO = data.REG_NO });
                                foreach (var item in list)
                                {
                                    item.LEDGER_FLOW_ID = data.LEDGER_FLOW_ID;
                                    work.Add((T_LED_WORK_USAGE_LOC)item);

                                    if (!work.IsValid)
                                    {
                                        foreach (var items in work.ErrorMessages)
                                        {
                                            ErrorMessage += items + "\n";
                                        }
                                        return ErrorMessage;
                                    }
                                    using (var pro = new LedgerProtectorDataInfo() { Context = context })
                                    {
                                        var pros = pro.GetSearchResult(new LedgerProtectorSearchModel() { REG_NO = data.REG_NO });
                                        foreach (var p in pros)
                                        {
                                            p.LEDGER_FLOW_ID = data.LEDGER_FLOW_ID;
                                            using (var proWork = new LedgerWorkProtectorDataInfo())
                                            {
                                                proWork.Add((T_LED_WORK_PROTECTOR)p);
                                            }
                                        }
                                    }
                                    using (var acces = new LedgerAccesmentRegDataInfo() { Context = context })
                                    {
                                        var acceses = acces.GetSearchResult(new LedgerAccesmentRegSearchModel() { REG_NO = data.REG_NO });
                                        foreach (var a in acceses)
                                        {
                                            a.LEDGER_FLOW_ID = data.LEDGER_FLOW_ID;
                                            using (var accesWork = new LedgerWorkAccesmentRegDataInfo())
                                            {
                                                accesWork.Add((T_LED_WORK_ACCESMENT_REG)a);
                                            }
                                        }
                                    }
                                    using (var riskreduce = new LedgerRiskReduceDataInfo() { Context = context })
                                    {
                                        var riskreduces = riskreduce.GetSearchResult(new LedgerRiskReduceSearchModel() { REG_NO = data.REG_NO });
                                        foreach (var r in riskreduces)
                                        {
                                            r.LEDGER_FLOW_ID = data.LEDGER_FLOW_ID;
                                            using (var riskreduceWork = new LedgerWorkRiskReduceDataInfo())
                                            {
                                                riskreduceWork.Add((T_LED_WORK_RISK_REDUCE)r);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                context.Commit();
                var mail = new WorkflowMail();
                //mail.SendMail(ledgerWorks);
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
                context.Rollback();
            }
            finally
            {
                context.Close();
            }
            ClearSession();
            return ErrorMessage;
        }
        protected void ClearSession()
        {
            Session.Remove(nameof(LedgerUsageLocationModel));
            Session.Remove(SessionConst.SearchType);
            Session.Remove(SelectModels);
            Session.Remove(ChangeValue);
            Session["ChiefUserCD"] = null;
            Session["ChiefUserName"] = null;
            Session[nameof(LedgerUsageLocationModel)] = null;
            Session["ApplicableLawGridModel"] = null;
            Session[sessionRiskReduces] = null;
            Session[sessionProtectors] = null;
        }

        public string Edit(string Id)
        {
           

            string ReturnMessage = null;
            try
            {
                var regno = GetSelectedLedger(Id);

                if (regno?.Count() > 0)
                {
                    var condition = new LedgerSearchModel();
                    condition.REG_NO = regno.Single();
                    condition.REG_NO_CONDITION = (int)SearchConditionType.PerfectMatching;
                    condition.LEDGER_STATUS = (int)LedgerStatusConditionType.All;
                    var d = new LedgerDataInfo();
                    List<int> record = d.GetDelFlag(condition);

                    if (record[0] == (int)DEL_FLAG.Undelete)
                    {
                        
                        ReturnMessage = string.Format(NikonFormPath.LedgerMaintenance + "?" + QueryString.ModeKey + "=" + (int)Mode.Edit +
                                                  "&" + QueryString.RegNoKey + "=" + regno.Single());
                    }
                    else
                    {
                        ReturnMessage = string.Format(NikonFormPath.LedgerMaintenance + "?" + QueryString.ModeKey + "=" + (int)Mode.Deleted +
                                                                          "&" + QueryString.RegNoKey + "=" + regno.Single());
                    }

                }
                var flowId = GetSelectedLedgerWork(Id);
                if (flowId?.Count() > 0)
                {
                    ReturnMessage = WarningMessages.CannotEdit;
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return ReturnMessage;
        }

        public string Referenceregistration(string Id)
        {
            string ReturnMessage = null;
            try
            {
                var regno = GetSelectedLedger(Id);

                if (regno?.Count() > 0)
                {

                    ReturnMessage = string.Format(NikonFormPath.LedgerMaintenance + "?" + QueryString.ModeKey + "=" + (int)Mode.Copy +
                                                  "&" + QueryString.RegNoKey + "=" + regno.Single());

                }
                var ids = GetSelectedLedgerWork(Id);

                if (ids.Count() > 0)
                {
                    ReturnMessage = WarningMessages.CannotCopy;
                }
                else if (ids.Count() == 0 && regno.Count() == 0)
                {
                    ReturnMessage = string.Format(NikonFormPath.LedgerMaintenance + "?" + QueryString.ModeKey + "=" + (int)Mode.Copy +
                                                     "&" + QueryString.LedgerFlowIdNoKey + "=" + Id);
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return ReturnMessage;
        }

        public string Delete(string Id)
        {
            string ReturnMessage = null;
            var flowId = GetSelectedLedgerWork(Id);

            if (flowId.Count() > 0)
            {
                ReturnMessage = WarningMessages.CannotDelete;
            }
            var regno = GetSelectedLedger(Id);

            if (regno?.Count() > 0)
            {
                var condition = new LedgerSearchModel();
                condition.REG_NO = regno.Single();
                condition.REG_NO_CONDITION = (int)SearchConditionType.PerfectMatching;
                condition.LEDGER_STATUS = (int)LedgerStatusConditionType.All;
                var d = new LedgerDataInfo();
                List<int> record = d.GetDelFlag(condition);
                d.Dispose();

                if (record[0] == (int)DEL_FLAG.Undelete)
                {
                    ReturnMessage = NikonFormPath.LedgerMaintenance + "?" + QueryString.ModeKey + "=" + (int)Mode.Abolition + "&" + QueryString.RegNoKey + "=" + regno.Single();
                }
                else
                {
                    ReturnMessage = WarningMessages.CannotDelete;
                }
            }
            return ReturnMessage;
        }

        public string ValidateChangeRequest(string REASON_ID, string OTHER_REASON, string GroupCollection, string Regno)
        {
            string errorMessages = null;
            LedgerSearchModel condition = new LedgerSearchModel();
            //if (Session[ChangeValue] != null)
            //{
            //    condition = (LedgerSearchModel)Session[ChangeValue];
            //}
            //else
            //{
            if (REASON_ID != "")
            {
                condition.REASON_ID = Convert.ToInt32(REASON_ID);
            }
            if (OTHER_REASON != "")
            {
                condition.OTHER_REASON = OTHER_REASON;
            }
            if (GroupCollection != "")
            {
                condition.GROUP_CD_SELECTION = GroupCollection;
            }

            string userInfo = Common.GetLogonForm().ADMIN_FLAG;
            var isAdmin = userInfo != null ? userInfo == Const.cADMIN_FLG_ADMIN : false;
            condition.IsAdmin = isAdmin;
            condition.REG_NO_SELECTION = Regno;
            Session.Add(ChangeValue, condition);
            //}
            if (condition.REASON_ID == null)
            {
                errorMessages = string.Format(WarningMessages.Required, PropertyUtil.GetName(condition, "REASON_ID")) + "\n";
            }
            if (condition.REASON_ID == LedgerModel.OTHER_REASON_ID && string.IsNullOrWhiteSpace(condition.OTHER_REASON))
            {
                errorMessages = string.Format(WarningMessages.Required, PropertyUtil.GetName(condition, "OTHER_REASON")) + "\n";
            }

            if (condition.OTHER_REASON != null)
            {
                if (condition.OTHER_REASON.Length > 100)
                {
                    errorMessages = string.Format(WarningMessages.MaximumLength, PropertyUtil.GetName(condition, "OTHER_REASON"), 100) + "\n";
                }
            }

            return errorMessages;
        }

        [HttpPost]
        public JsonResult ValidateRegistrationDetails(LedgerCommonInfo objLedgerCommonInfo)
        {
            

            var listOfManufacturer = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MANUFACTURENAME];
            var itemvalue = listOfManufacturer.Where(item => item.Value == objLedgerCommonInfo.MAKER_NAME).ToList();

            if (itemvalue != null && itemvalue.Count() > 0)
            {
                objLedgerCommonInfo.MAKER_NAME = itemvalue[0].Text;
            }

            int updatevalue = Convert.ToInt32(objLedgerCommonInfo.hdnupdatetype);
            UpdateType updateType = (UpdateType)updatevalue;
            if (updatevalue !=0 && updatevalue != 2)
            {
                objLedgerCommonInfo = objIMangementLedgerRepo.ClientsideValidation(objLedgerCommonInfo);
            }
            

            LedgerCommonInfo cssmsmodel = new LedgerCommonInfo();
            List<string> errorMessages = new List<string>();
            List<string> alertMessages = new List<string>();
            string ErrorStatus = "";
            string AlertMessages = "";
            if (objLedgerCommonInfo.LEDGER_FLOW_ID == 0)
            {
                objLedgerCommonInfo.LEDGER_FLOW_ID = null;
            }
            var model = objLedgerCommonInfo;

            using (var dataInfo = new LedgerWorkDataInfo())
            {
                string message = dataInfo.ValidateData((T_LEDGER_WORK)model);
                if (message != null)
                {
                    errorMessages.AddRange(dataInfo.ErrorMessages);
                }

                if (model.APPLI_CLASS == (int)AppliClass.Edit || model.APPLI_CLASS == (int)AppliClass.Stopped)
                {
                    if (model.REASON_ID == null)
                        errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName(model, "REASON_ID")));
                }
                if (model.REASON_ID == LedgerModel.OTHER_REASON_ID && string.IsNullOrWhiteSpace(model.OTHER_REASON))
                {
                    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName(model, "OTHER_REASON")));
                }

                if (string.IsNullOrEmpty(model.CHEM_NAME))
                {
                    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName(model, "CHEM_NAME")));
                }
                if (model.CHEM_CD == NikonConst.UnregisteredChemCd && model.HIERARCHY == 0)
                {
                    errorMessages.Add(string.Format(WarningMessages.UnregisteredChemCd));
                }
                if (model.CHEM_CD == NikonConst.UnregisteredChemCd && model.HasUnregisteredChem == false && model.hdnUNREGISTERED_CHEM == 0)
                {
                    errorMessages.Add(string.Format(WarningMessages.Required, PropertyUtil.GetName(model, "UNREGISTERED_CHEM")));
                }

                using (var LedgerWorkHistoryDataInfo = new LedgerWorkHistoryDataInfo())
                {
                    UserInformation loginModel = new UserInformation();
                    loginModel = (UserInformation)Session["LoginUserSession"];

                    var workflow = LedgerWorkHistoryDataInfo.GetBlankWorkflow(
                            new T_LED_WORK_HISTORY() { LEDGER_FLOW_ID = model.LEDGER_FLOW_ID }, loginModel.User_CD, model.APPLI_CLASS, mode);
                    var userDic = new UserMasterInfo().GetDictionary();
                    foreach (var flow in workflow)
                    {
                        if (!userDic.ContainsKey(flow.APPROVAL_USER_CD))
                        {
                            errorMessages.Add(string.Format(WarningMessages.WorkFlowUserError, flow.APPROVAL_USER_CD));
                        }
                    }
                }

                if (model.CHEM_CD != NikonConst.UnregisteredChemCd && model.APPLI_CLASS == (int)AppliClass.New)
                {
                    using (var groupMaster = new GroupMasterInfo())
                    {
                        var grp = groupMaster.GetParentGroups(model.GROUP_CD, true, 1);
                        var grpSel = string.Empty;
                        foreach (var g in grp)
                        {
                            if (string.IsNullOrEmpty(grpSel))
                            {
                                grpSel += g;
                            }
                            else
                            {
                                grpSel += ("," + g);
                            }
                        }
                        var cond = new LedgerSearchModel()
                        {
                            CHEM_CD = model.CHEM_CD,
                            GROUP_CD_SELECTION = grpSel,
                            LEDGER_FLOW_ID_EXT = model.LEDGER_FLOW_ID,
                            REG_NO_EXT = model.REG_NO
                        };
                        using (var work = new LedgerWorkDataInfo())
                        {
                            List<int> count = work.lstGetSearchResultCount(cond);
                            foreach (var Rowcount in count)
                            {
                                if (Rowcount > 0)
                                {
                                    errorMessages.Add(WarningMessages.SameChemSelect);
                                }
                            }
                        }
                        using (var data = new LedgerDataInfo())
                        {
                            List<int> count = data.lstGetSearchResultCount(cond);
                            foreach (var Rowcount in count)
                            {
                                if (Rowcount > 0)
                                {
                                    errorMessages.Add(WarningMessages.SameChemSelect);
                                }
                            }
                        }
                    }
                }
            }

            using (var dataInfo = new LedgerWorkOldRegisterNumberDataInfo())
            {
                var oldRegisterNumbers = (IEnumerable<LedgerOldRegisterNumberModel>)Session["sessionOldRegisterNumbers"];
                if (oldRegisterNumbers == null)
                {
                    oldRegisterNumbers = new List<LedgerOldRegisterNumberModel>();
                }

                dataInfo.ValidateData(oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)).Select(x => (T_LED_WORK_REG_OLD_NO)x));
                if (dataInfo.ErrorMessages.Count > 0)
                {
                    errorMessages.AddRange(dataInfo.ErrorMessages);
                }
            }
            // 登録申請、変更申請、承認時にチェック
            bool flgConfirm = false;
            
            if ((updateType == UpdateType.Register || updateType == UpdateType.Update || updateType == UpdateType.Approval) && model.APPLI_CLASS != (int)AppliClass.Stopped)
            {
                List<LedgerUsageLocationModel> objLedgerUsageLocationModel = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                if (objLedgerUsageLocationModel == null)
                {
                    objLedgerUsageLocationModel = new List<LedgerUsageLocationModel>();
                }

                int t = 0;
                for (int i = 0; i < objLedgerUsageLocationModel.Count; i++)
                {
                    if (objLedgerUsageLocationModel[i].REG_TRANSACTION_VOLUME.ToString() == "" || objLedgerUsageLocationModel[i].REG_WORK_FREQUENCY.ToString() == "" ||
                        objLedgerUsageLocationModel[i].REG_DISASTER_POSSIBILITY.ToString() == "" || objLedgerUsageLocationModel[i].MEASURES_BEFORE_RANK.ToString() == "" ||
                        objLedgerUsageLocationModel[i].MEASURES_AFTER_RANK.ToString() == "")
                    {
                        t = i + 1;
                        errorMessages.Add(string.Format(WarningMessages.Required, t + "行目：" + "リスクアセスメント結果"));
                    }
                }

                // 化学物質に紐づく法規制(保管場所は限定前の法規制を使用するため)
                if (Session[sessionChemRegulations] == null)
                {
                    getChemRegulation(model.CHEM_CD, 0);
                }

                using (var dataInfo = new LedgerWorkLocationDataInfo())
                {
                    List<LedgerLocationModel> storingLocations = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                    var check = storingLocations.Where(x => x.LOC_ID != null).Select(x => (T_LED_WORK_LOC)x);
                    if (check.Count() > 0)
                    {
                        dataInfo.ValidateData(storingLocations.Where(x => x.LOC_ID != null).Select(x => (T_LED_WORK_LOC)x));
                        if (dataInfo.ErrorMessages.Count > 0)
                        {
                            errorMessages.AddRange(dataInfo.ErrorMessages);
                        }

                        for (int i = 0; i < storingLocations.Count(); i++)
                        {
                            int intRow = i + 1;
                            int? intLocId = OrgConvert.ToNullableInt32(storingLocations[i].LOC_ID);

                            if (intLocId == null)
                            {
                                // 保管場所の設定がされていません。
                                errorMessages.Add(intRow + "行目：" + WarningMessages.StoringLocNoDataRow);
                            }
                            else
                            {
                                if (!Convert.ToBoolean(storingLocations[i].FLG_LOC_HAZARD))
                                {

                                    // 消防法に抵触し、保管場所マスターで保管危険物のチェックが入っているものを取得
                                    var dataArray = (IEnumerable<LedgerRegulationModel>)Session[sessionChemRegulations];
                                    var dataArray2 = dataArray.AsQueryable();
                                    dataArray2 = dataArray2.Where(x => x.CLASS_ID == (int)ClassIdType.Fire);

                                    if (dataArray2.Count() > 0)
                                    {
                                        if (!ChkFireReg(intLocId, model.CHEM_CD, (int)LOCATION.StoringLocation))
                                        {
                                            // 保管場所マスターで保管可能危険物のチェックが入っていません。
                                            errorMessages.Add(intRow + "行目：" + WarningMessages.StoreableDangerousChk);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        errorMessages.Add(string.Format(WarningMessages.Required, "保管場所"));
                    }
                }

                //List<LedgerUsageLocationModel> objLedgerUsageLocationModel = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];

                if (objLedgerUsageLocationModel == null || objLedgerUsageLocationModel.Count() == 0)
                {
                    errorMessages.Add(string.Format(WarningMessages.Required, "使用場所"));
                }
                else
                {
                    for (int i = 0; i < objLedgerUsageLocationModel.Count; i++)
                    {
                        if (!Convert.ToBoolean(objLedgerCommonInfo.FLG_RISK_ASSESSMENT))
                        {
                            int intRow = i + 1;
                            // 化学物質を変更後に使用場所のリスクアセスメントが実施されていません。
                            errorMessages.Add(intRow + "行目：" + WarningMessages.RiskAssessment);
                        }
                    }
                }

                //消防法チェック
                if (errorMessages.Count == 0 && (model.CHEM_CD != NikonConst.UnregisteredChemCd))
                {
                    string regtypeid = null;
                    var varChemRegTypeIds = Session[sessionChemRegTypeIds];
                    if (varChemRegTypeIds != null)
                    {
                        string arrChemRegTypeIds = Convert.ToString(varChemRegTypeIds);

                        if (arrChemRegTypeIds != "")
                        {
                            try
                            {
                                using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["SqlServerConnectionString"].ConnectionString))
                                {
                                    SqlCommand cmd = new SqlCommand();
                                    SqlDataAdapter adp = new SqlDataAdapter();
                                    StringBuilder sbSql = new StringBuilder();
                                    DataTable records = new DataTable();

                                    conn.Open();

                                    // 消防法の法規制があるかどうか
                                    cmd.Connection = conn;
                                    cmd.CommandText = "select REG_TYPE_ID, UNITSIZE_ID from M_REGULATION where REG_TYPE_ID in (" + arrChemRegTypeIds + ") and CLASS_ID = 3 and DEL_FLAG = 0";
                                    cmd.ExecuteNonQuery();
                                    adp.SelectCommand = cmd;
                                    records = new DataTable();
                                    adp.Fill(records);

                                    if (records.Rows.Count > 0)
                                    {
                                        // DBからkgとlのIDを取得
                                        int intKg = 0;
                                        int intL = 0;

                                        cmd.Connection = conn;
                                        sbSql.Append("select UNITSIZE_ID,  ");
                                        sbSql.Append(" LOWER(UNIT_NAME) UNIT_NAME ");
                                        sbSql.AppendLine(" from");
                                        sbSql.AppendLine(" M_UNITSIZE where UNIT_NAME in('l', 'kg')");
                                        cmd.CommandText = sbSql.ToString();
                                        cmd.ExecuteNonQuery();
                                        adp = new SqlDataAdapter();
                                        adp.SelectCommand = cmd;
                                        DataTable dtUnit = new DataTable();
                                        adp.Fill(dtUnit);
                                        foreach (DataRow record in dtUnit.Rows)
                                        {
                                            if (record["UNIT_NAME"].ToString() == "kg")
                                            {
                                                intKg = Convert.ToInt16(record["UNITSIZE_ID"].ToString());
                                            }
                                            if (record["UNIT_NAME"].ToString() == "l")
                                            {
                                                intL = Convert.ToInt16(record["UNITSIZE_ID"].ToString());
                                            }
                                        }

                                        // 設定ファイルとの変換をするための一時テーブルを作成
                                        cmd.Connection = conn;
                                        cmd.CommandText = "create table #TMP_UNIT (UNIT nvarchar(10) COLLATE database_default NOT NULL, CONVERTED_UNIT nvarchar(10) COLLATE database_default NOT NULL)";
                                        cmd.ExecuteNonQuery();

                                        cmd.CommandText = "insert into #TMP_UNIT (UNIT, CONVERTED_UNIT) values('kg','kg')";
                                        cmd.ExecuteNonQuery();
                                        cmd.CommandText = "insert into #TMP_UNIT (UNIT, CONVERTED_UNIT) values('l','l')";
                                        cmd.ExecuteNonQuery();
                                        string[] ArryUNIT = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["CONVERT_UNIT"]).Split(',');
                                        foreach (string str in ArryUNIT)
                                        {
                                            string[] ArryKeyValue = str.Split(':');
                                            cmd.CommandText = "insert into #TMP_UNIT (UNIT, CONVERTED_UNIT) values('" + ArryKeyValue[0] + "','" + ArryKeyValue[1] + "')";
                                            cmd.ExecuteNonQuery();
                                        }

                                        // 単位変換一時テーブル
                                        sbSql = new StringBuilder();
                                        sbSql.Append("select tu.UNIT, mu.UNITSIZE_ID, mu2.UNITSIZE_ID CONVERTED_UNITSIZE_ID ,mu.FACTOR ");
                                        sbSql.Append("into #TMP_CONV_UNIT ");
                                        sbSql.Append("from #TMP_UNIT tu inner join M_UNITSIZE mu on tu.UNIT = mu.UNIT_NAME COLLATE database_default ");
                                        sbSql.Append("inner join M_UNITSIZE mu2 on tu.CONVERTED_UNIT = mu2.UNIT_NAME COLLATE database_default ");
                                        sbSql.Append("where mu.DEL_FLAG = 0 ");
                                        cmd = new SqlCommand();
                                        cmd.Connection = conn;
                                        cmd.CommandText = sbSql.ToString();
                                        cmd.ExecuteNonQuery();

                                        // 消防法届出一時テーブル
                                        sbSql = new StringBuilder();
                                        sbSql.Append(" SELECT V1.FIRE_ID  ,M1.FIRE_SIZE ,M1.UNITSIZE_ID, ");
                                        sbSql.Append(" SUBSTRING(V1.FIRE_NAME, 0, CHARINDEX('類/', V1.FIRE_NAME) + 1) AS REG_TEXT, ");
                                        sbSql.Append(" SUBSTRING(V1.FIRE_NAME, CHARINDEX('類/', V1.FIRE_NAME) + 2, 100) AS REG_TEXT2, ");
                                        sbSql.Append(" V1.FIRE_NAME,  V1.SORT_ORDER ");
                                        sbSql.Append(" into #TMP_HAZARDOUS ");
                                        sbSql.Append(" FROM  V_FIRE V1 LEFT JOIN M_REGULATION M1 ON V1.FIRE_ID = M1.REG_TYPE_ID ");
                                        sbSql.Append(" LEFT JOIN M_UNITSIZE U ON M1.UNITSIZE_ID = U.UNITSIZE_ID ");
                                        sbSql.Append(" WHERE V1.FIRE_NAME LIKE '%第%類%' AND M1.FIRE_SIZE IS NOT NULL AND V1.DEL_FLAG = 0 ");
                                        cmd = new SqlCommand();
                                        cmd.Connection = conn;
                                        cmd.CommandText = sbSql.ToString();
                                        cmd.ExecuteNonQuery();

                                        // 消防法を危険物種類ごとに取得
                                        sbSql = new StringBuilder();
                                        sbSql.Append("select FIRE_ID ,FIRE_SIZE ,UNITSIZE_ID,REG_TEXT, REG_TEXT2 from #TMP_HAZARDOUS ");
                                        sbSql.Append("order by ");
                                        sbSql.Append("REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(SUBSTRING(FIRE_NAME, 0, CHARINDEX('類/', FIRE_NAME) + 1), '一', 1), '二', '2'), '三', '3'), '四', '4'), '五', '5'), '六', '6') ");
                                        sbSql.Append(", FIRE_SIZE, SORT_ORDER ");
                                        cmd = new SqlCommand();
                                        cmd.Connection = conn;
                                        cmd.CommandText = sbSql.ToString();
                                        cmd.ExecuteNonQuery();
                                        adp = new SqlDataAdapter();
                                        adp.SelectCommand = cmd;
                                        DataTable dtTmpHazardous = new DataTable();
                                        adp.Fill(dtTmpHazardous);

                                        // 化学物質の密度を取得
                                        sbSql = new StringBuilder();
                                        sbSql.Append("select DENSITY from M_CHEM where CHEM_CD = '" + model.CHEM_CD + "'");
                                        cmd = new SqlCommand();
                                        cmd.Connection = conn;
                                        cmd.CommandText = sbSql.ToString();
                                        cmd.ExecuteNonQuery();
                                        adp = new SqlDataAdapter();
                                        adp.SelectCommand = cmd;
                                        DataTable recordsDensity = new DataTable();
                                        adp.Fill(recordsDensity);

                                        decimal decDensity = 0;
                                        if (recordsDensity.Rows[0]["DENSITY"].ToString() != "")
                                        {
                                            decDensity = Convert.ToDecimal(recordsDensity.Rows[0]["DENSITY"].ToString());
                                        }

                                        StringBuilder sbSqlUnit = new StringBuilder();
                                        sbSqlUnit.Append("select * from ( ");
                                        sbSqlUnit.Append(" select * from #TMP_CONV_UNIT");
                                        sbSqlUnit.Append("  union ");
                                        sbSqlUnit.Append(" select UNIT_NAME, UNITSIZE_ID, 0, FACTOR ");
                                        sbSqlUnit.Append(" from M_UNITSIZE where UNITSIZE_ID not in(select UNITSIZE_ID from #TMP_CONV_UNIT)");
                                        sbSqlUnit.Append(") UNCONV ");
                                        sbSqlUnit.Append("where UNCONV.UNITSIZE_ID = @UNITSIZE_ID");

                                        // 危険物(第一～第六類)の法規制か
                                        bool flgHazardous = ChkHazardousReg(conn, arrChemRegTypeIds);

                                        // 保管場所チェック
                                        ArrayList arrStoringLoc = new ArrayList();
                                        List<LedgerLocationModel> storingLocations = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                                        for (int i = 0; i < storingLocations.Count; i++)
                                        {
                                            bool flgNoReport = true;
                                            bool flgReport = true;
                                            bool flgOver = true;
                                            bool flgSingle = true;

                                            decimal storeLatelyAmounts = 0;
                                            int intRow = i + 1;
                                            int intLocId = Convert.ToInt16(storingLocations[i].LOC_ID);

                                            // 保管場所の単位
                                            decimal decStoreAmount = Convert.ToDecimal(storingLocations[i].AMOUNT);
                                            int intStoreUnitSizeId = Convert.ToInt16(storingLocations[i].UNITSIZE_ID);

                                            cmd = new SqlCommand();
                                            cmd.Connection = conn;
                                            cmd.CommandText = sbSqlUnit.ToString();
                                            cmd.Parameters.AddWithValue("UNITSIZE_ID", intStoreUnitSizeId);
                                            cmd.ExecuteNonQuery();
                                            adp = new SqlDataAdapter();
                                            adp.SelectCommand = cmd;
                                            DataTable dtUnitSize = new DataTable();
                                            adp.Fill(dtUnitSize);

                                            if (dtUnitSize.Rows.Count == 0)
                                            {
                                                // 容量単位が登録されていないため計算できません。
                                                errorMessages.Add(intRow + "行目：" + string.Format(WarningMessages.ReguiredUnit, "保管場所"));
                                                break;
                                            }

                                            int intConvUnit = Convert.ToInt16(dtUnitSize.Rows[0]["CONVERTED_UNITSIZE_ID"].ToString());
                                            if (intConvUnit != 0)
                                            {
                                                // 設定ファイルの単位とあわせる
                                                decimal decStoreFac = 0;
                                                if (Convert.ToInt16(dtUnitSize.Rows[0]["UNITSIZE_ID"].ToString()) != intConvUnit)
                                                {
                                                    decStoreFac = decStoreAmount * (Convert.ToDecimal(dtUnitSize.Rows[0]["FACTOR"].ToString()));
                                                }
                                                else
                                                {
                                                    decStoreFac = decStoreAmount;
                                                }

                                                foreach (DataRow record in records.Rows)
                                                {
                                                    if (record["UNITSIZE_ID"].ToString() == "")
                                                    {
                                                        // 該当法規制の管理単位IDが未登録のため計算できません。
                                                        errorMessages.Add(intRow + "行目：" + string.Format(WarningMessages.ReguiredRegUnitSizeID, "保管場所"));
                                                        break;
                                                    }
                                                    int intRegUnit = Convert.ToInt16(record["UNITSIZE_ID"].ToString());

                                                    if (intRegUnit != intConvUnit)
                                                    {
                                                        if (intRegUnit == intKg)
                                                        {
                                                            storeLatelyAmounts = decStoreFac * decDensity;
                                                        }
                                                        if (intRegUnit == intL)
                                                        {
                                                            storeLatelyAmounts = decStoreFac / decDensity;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        storeLatelyAmounts = decStoreFac;
                                                    }
                                                }

                                                // 少量危険物未満管理の保管場所か
                                                bool flgHazardousLoc = ChkHazardousLoc(conn, (int)LOCATION.StoringLocation, intLocId);
                                                if (flgHazardousLoc)
                                                {
                                                    // 少量危険物未満管理チェック
                                                    if (flgHazardous)
                                                    {
                                                        foreach (DataRow record in records.Rows)
                                                        {
                                                            bool chkOver = true;
                                                            regtypeid = record["REG_TYPE_ID"].ToString();
                                                            int result = CheckSmallHazardous(conn, regtypeid, storeLatelyAmounts, arrStoringLoc, dtTmpHazardous, (int)LOCATION.StoringLocation, intLocId, model, intKg, intL, ref chkOver);
                                                            if (!chkOver)
                                                            {
                                                                flgOver = false;
                                                            }
                                                            if (result == 1)
                                                            {
                                                                flgReport = false;
                                                            }
                                                            else if (result == 2)
                                                            {
                                                                flgSingle = false;
                                                                break;
                                                            }
                                                            else if (result == 3)
                                                            {
                                                                flgNoReport = false;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    // 届出数量チェック
                                                    foreach (DataRow record in records.Rows)
                                                    {
                                                        bool chkOver = true;
                                                        regtypeid = record["REG_TYPE_ID"].ToString();
                                                        int result = CheckHazardous(conn, regtypeid, storeLatelyAmounts, arrStoringLoc, dtTmpHazardous, (int)LOCATION.StoringLocation, intLocId, model, intKg, intL, ref chkOver);
                                                        if (!chkOver)
                                                        {
                                                            flgOver = false;
                                                        }
                                                        if (result == 1)
                                                        {
                                                            flgReport = false;
                                                        }
                                                        else if (result == 2)
                                                        {
                                                            flgSingle = false;
                                                            break;
                                                        }
                                                        else if (result == 3)
                                                        {
                                                            flgNoReport = false;
                                                            break;
                                                        }
                                                    }
                                                }

                                                if (!flgNoReport)
                                                {
                                                    // 保管場所に届出数量が登録されていません。
                                                    alertMessages.Add(intRow + "行目：" + string.Format(WarningMessages.LedgerNoReport, "保管場所"));
                                                    //alertMessages.Add("1");
                                                    flgConfirm = true;
                                                }
                                                if (!flgReport)
                                                {
                                                    if (flgHazardousLoc)
                                                    {
                                                        // 保管場所の少量危険物未満管理を超える数量が申請されています。
                                                        alertMessages.Add(intRow + "行目：" + string.Format(WarningMessages.SmallHazardousOver, "保管場所"));
                                                    }
                                                    else
                                                    {
                                                        // 保管場所の届出数量を超える数量が申請されています。
                                                        alertMessages.Add(intRow + "行目：" + string.Format(WarningMessages.LedgerReport, "保管場所"));
                                                    }
                                                    flgConfirm = true;
                                                    //alertMessages.Add("1");
                                                }
                                                if (!flgOver)
                                                {
                                                    // 保管場所の指定数量が１倍以上です。
                                                    alertMessages.Add(intRow + "行目：" + string.Format(WarningMessages.LedgerOver, "保管場所"));
                                                    flgConfirm = true;
                                                    //alertMessages.Add("1");
                                                }
                                                if (!flgSingle)
                                                {
                                                    // 保管場所が複数の消防法届出に登録されています。
                                                    errorMessages.Add(intRow + "行目：" + string.Format(WarningMessages.LedgerMulti, "保管場所"));
                                                }

                                                // 処理した行の場所IDと容量をリストで保持
                                                arrStoringLoc.Add(new DictionaryEntry(intLocId, storeLatelyAmounts));
                                            }
                                        }

                                        // 使用場所チェック
                                        ArrayList arrUsageLoc = new ArrayList();
                                        for (int i = 0; i < objLedgerUsageLocationModel.Count; i++)
                                        {
                                            bool flgNoReport = true;
                                            bool flgReport = true;
                                            bool flgOver = true;
                                            bool flgSingle = true;

                                            decimal usageLatelyAmounts = 0;
                                            int intRow = i + 1;
                                            int intLocId = Convert.ToInt16(objLedgerUsageLocationModel[i].LOC_ID);

                                            // 使用場所の単位
                                            decimal decUsageAmount = Convert.ToDecimal(objLedgerUsageLocationModel[i].AMOUNT /*USAGEAMOUNT*/);
                                            int intUsageUnitSizeId = Convert.ToInt16(objLedgerUsageLocationModel[i].UNITSIZE_ID /*USAGEUNITSIZEID*/);

                                            cmd = new SqlCommand();
                                            cmd.Connection = conn;
                                            cmd.CommandText = sbSqlUnit.ToString();
                                            cmd.Parameters.AddWithValue("UNITSIZE_ID", intUsageUnitSizeId);
                                            cmd.ExecuteNonQuery();
                                            adp = new SqlDataAdapter();
                                            adp.SelectCommand = cmd;
                                            DataTable dtUnitSize = new DataTable();
                                            adp.Fill(dtUnitSize);

                                            if (dtUnitSize.Rows.Count == 0)
                                            {
                                                // 容量単位が登録されていないため計算できません。
                                                errorMessages.Add(intRow + "行目：" + string.Format(WarningMessages.ReguiredUnit, "使用場所"));
                                                break;
                                            }

                                            int intConvUnit = Convert.ToInt16(dtUnitSize.Rows[0]["CONVERTED_UNITSIZE_ID"].ToString());
                                            if (intConvUnit != 0)
                                            {
                                                // 設定ファイルの単位とあわせる
                                                decimal decUsagaeFac = 0;
                                                if (Convert.ToInt16(dtUnitSize.Rows[0]["UNITSIZE_ID"].ToString()) != intConvUnit)
                                                {
                                                    decUsagaeFac = decUsageAmount * (Convert.ToDecimal(dtUnitSize.Rows[0]["FACTOR"].ToString()));
                                                }
                                                else
                                                {
                                                    decUsagaeFac = decUsageAmount;
                                                }

                                                foreach (DataRow record in records.Rows)
                                                {
                                                    if (record["UNITSIZE_ID"].ToString() == "")
                                                    {
                                                        // 該当法規制の管理単位IDが未登録のため計算できません。
                                                        errorMessages.Add(intRow + "行目：" + string.Format(WarningMessages.ReguiredRegUnitSizeID, "使用場所"));
                                                        break;
                                                    }
                                                    int intRegUnit = Convert.ToInt16(record["UNITSIZE_ID"].ToString());

                                                    if (intRegUnit != intConvUnit)
                                                    {
                                                        if (intRegUnit == intKg)
                                                        {
                                                            usageLatelyAmounts = decUsagaeFac * decDensity;
                                                        }
                                                        if (intRegUnit == intL)
                                                        {
                                                            usageLatelyAmounts = decUsagaeFac / decDensity;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        usageLatelyAmounts = decUsagaeFac;
                                                    }
                                                }

                                                // 少量危険物未満管理の使用場所か
                                                bool flgHazardousLoc = ChkHazardousLoc(conn, (int)LOCATION.UsageLocation, intLocId);
                                                if (flgHazardousLoc)
                                                {
                                                    // 少量危険物未満管理チェック
                                                    if (flgHazardous)
                                                    {
                                                        bool chkOver = true;
                                                        foreach (DataRow record in records.Rows)
                                                        {
                                                            regtypeid = record["REG_TYPE_ID"].ToString();
                                                            int result = CheckSmallHazardous(conn, regtypeid, usageLatelyAmounts, arrUsageLoc, dtTmpHazardous, (int)LOCATION.UsageLocation, intLocId, model, intKg, intL, ref chkOver);
                                                            if (!chkOver)
                                                            {
                                                                flgOver = false;
                                                            }
                                                            if (result == 1)
                                                            {
                                                                flgReport = false;
                                                            }
                                                            else if (result == 2)
                                                            {
                                                                flgSingle = false;
                                                                break;
                                                            }
                                                            else if (result == 3)
                                                            {
                                                                flgNoReport = false;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    // 届出数量チェック
                                                    foreach (DataRow record in records.Rows)
                                                    {
                                                        bool chkOver = true;
                                                        regtypeid = record["REG_TYPE_ID"].ToString();
                                                        int result = CheckHazardous(conn, regtypeid, usageLatelyAmounts, arrUsageLoc, dtTmpHazardous, (int)LOCATION.UsageLocation, intLocId, model, intKg, intL, ref chkOver);
                                                        if (!chkOver)
                                                        {
                                                            flgOver = false;
                                                        }
                                                        if (result == 1)
                                                        {
                                                            flgReport = false;
                                                        }
                                                        else if (result == 2)
                                                        {
                                                            flgSingle = false;
                                                            break;
                                                        }
                                                        else if (result == 3)
                                                        {
                                                            flgNoReport = false;
                                                            break;
                                                        }
                                                    }
                                                }

                                                if (!flgNoReport)
                                                {
                                                    // 使用場所に届出数量が登録されていません。
                                                    alertMessages.Add(intRow + "行目：" + string.Format(WarningMessages.LedgerNoReport, "使用場所"));
                                                    flgConfirm = true;
                                                    //alertMessages.Add("1");
                                                }
                                                if (!flgReport)
                                                {
                                                    if (flgHazardousLoc)
                                                    {
                                                        // 使用場所の少量危険物未満管理を超える数量が申請されています。
                                                        alertMessages.Add(intRow + "行目：" + string.Format(WarningMessages.SmallHazardousOver, "使用場所"));
                                                    }
                                                    else
                                                    {
                                                        // 使用場所の届出数量を超える数量が申請されています。
                                                        alertMessages.Add(intRow + "行目：" + string.Format(WarningMessages.LedgerReport, "使用場所"));
                                                    }
                                                    flgConfirm = true;
                                                }
                                                if (!flgOver)
                                                {
                                                    // 保管場所の指定数量が１倍以上です。
                                                    alertMessages.Add(intRow + "行目：" + string.Format(WarningMessages.LedgerOver, "使用場所"));
                                                    flgConfirm = true;
                                                    //alertMessages.Add("1");
                                                }
                                                if (!flgSingle)
                                                {
                                                    // 保管場所が複数の消防法届出に登録されています。
                                                    errorMessages.Add(intRow + "行目：" + string.Format(WarningMessages.LedgerMulti, "使用場所"));
                                                }

                                                // 処理した行の場所IDと容量をリストで保持
                                                arrUsageLoc.Add(new DictionaryEntry(intLocId, usageLatelyAmounts));
                                            }
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                _logoututil.OutErrLog(programId, ex.ToString());
                            }
                        }
                    }

                    if (updateType == UpdateType.Remand)
                    {
                        var model2 = (LedgerWorkHistoryModel)Session[nameof(LedgerWorkHistoryModel)];
                        using (var workHistoryDataInfo = new LedgerWorkHistoryDataInfo())
                        {
                            workHistoryDataInfo.ValidateData(model2);
                            if (workHistoryDataInfo.ErrorMessages.Count > 0)
                            {
                                errorMessages.AddRange(workHistoryDataInfo.ErrorMessages);
                            }
                        }
                    }
                }
            }

            switch (updateType)
            {
                case UpdateType.Register:
                case UpdateType.Update:
                case UpdateType.Approval:
                    if (errorMessages != null && errorMessages.Count > 0)
                    {
                        foreach (var items in errorMessages)
                        {
                            ErrorStatus += items.ToString() + "\n";
                        }
                    }
                    if (ErrorStatus != null)
                    {
                        ErrorStatus = ErrorStatus.TrimEnd('\n');
                    }
                    break;
            }
            if (alertMessages.Count > 0)
            {
                List<string> message = new List<string>();
                alertMessages.Add("回送してもよろしいですか ?");
                if (alertMessages != null && alertMessages.Count > 0)
                {
                    foreach (var items in alertMessages)
                    {
                        AlertMessages += items.ToString() + "\n";
                    }
                }
                if (AlertMessages != null)
                {
                    AlertMessages = AlertMessages.TrimEnd('\n');
                    message.Add(AlertMessages);
                    message.Add(flgConfirm.ToString());
                }
                return Json(message, JsonRequestBehavior.AllowGet);
            }
            //return ErrorStatus;
            
            return Json(ErrorStatus, JsonRequestBehavior.AllowGet);
        }

        public bool LoadUseLocationGrid(LedgerUsageLocationModel usageLocation)
        {
            try
            {
                List<LedgerUsageLocationModel> ledgermodel = new List<LedgerUsageLocationModel>();
                LedgerUsageLocationModel ledgerUsage = new LedgerUsageLocationModel();
                if (Session[nameof(LedgerUsageLocationModel)] != null)
                {
                    //usageLocation = new LedgerUsageLocationModel();
                    ledgerUsage = (LedgerUsageLocationModel)Session[nameof(LedgerUsageLocationModel)];
                }
                if (Session[sessionUsageLocations] != null)
                {
                    ledgermodel = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                }

                if (Request.Form["LOC_NAME"] != null)
                {
                    usageLocation.LOC_NAME = Request.Form["LOC_NAME"];
                    usageLocation.LOC_ID = Convert.ToInt32(Request.Form["USAGE_LOC_SELECTION"]);
                }
                if (Request.Form["MEASURES_BEFORE_RANK"] != null)
                {
                    usageLocation.MEASURES_BEFORE_RANK = Convert.ToInt32(Request.Form["MEASURES_BEFORE_RANK"]);
                }
                if (Request.Form["MEASURES_AFTER_RANK"] != null)
                {
                    usageLocation.MEASURES_AFTER_RANK = Convert.ToInt32(Request.Form["MEASURES_AFTER_RANK"]);
                }
                if (Session["ChiefUserCD"] != null)
                {
                    usageLocation.CHEM_OPERATION_CHIEF_CD = (string)Session["ChiefUserCD"];
                }
                if (Session["ChiefUserName"] != null)
                {
                    usageLocation.CHEM_OPERATION_CHIEF_NAME = (string)Session["ChiefUserName"];
                }


                usageLocation.RowId = ledgerUsage.RowId;
                usageLocation.FLG_RISK_ASSESSMENT = true;
                usageLocation.PROTECTOR_COLLECTION = (List<LedgerProtectorModel>)Session[sessionProtectors];
                usageLocation.ISHA_USAGE_TEXT = new Nikon.Ledger.IshaUsageMasterInfo().GetDictionary().GetValueToString(usageLocation.ISHA_USAGE);
                usageLocation.SUB_CONTAINER_TEXT = new CommonMasterInfo(NikonCommonMasterName.SUB_CONTAINER).GetDictionary().GetValueToString(usageLocation.SUB_CONTAINER);
                usageLocation.MEASURES_BEFORE_RANK_TEXT = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary().GetValueToString(usageLocation.MEASURES_BEFORE_RANK);
                usageLocation.MEASURES_AFTER_RANK_TEXT = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary().GetValueToString(usageLocation.MEASURES_AFTER_RANK);
                usageLocation.REG_TRANSACTION_VOLUME_TEXT = new CommonMasterInfo(NikonCommonMasterName.REG_TRANSACTION_VOLUME).GetDictionary().GetValueToString(usageLocation.REG_TRANSACTION_VOLUME);
                usageLocation.REG_WORK_FREQUENCY_TEXT = new CommonMasterInfo(NikonCommonMasterName.REG_WORK_FREQUENCY).GetDictionary().GetValueToString(usageLocation.REG_WORK_FREQUENCY);
                usageLocation.REG_DISASTER_POSSIBILITY_TEXT = new CommonMasterInfo(NikonCommonMasterName.REG_DISASTER_POSSIBILITY).GetDictionary().GetValueToString(usageLocation.REG_DISASTER_POSSIBILITY);
                usageLocation.RISK_REDUCE_COLLECTION = (List<LedgerRiskReduceModel>)Session[sessionRiskReduces];
                usageLocation.ACCESMENT_REG_COLLECTION = (List<LedgerAccesmentRegModel>)Session[sessionAccesmentReg];
                usageLocation.objLedgerRisk = (List<LedgerRiskReduceModel>)Session[sessionRiskReduces];
                usageLocation.objLedgerProtectorModel = (List<LedgerProtectorModel>)Session[sessionProtectors];

                //ledgermodel.Add(usageLocation);
                //Session.Add(sessionUsageLocations, ledgermodel.AsEnumerable());
                Session[SessionConst.CategoryTableName] = nameof(T_LED_WORK_USAGE_LOC);
                Session[SessionConst.CurrentRowId] = usageLocation;
                Session[sessionRiskReduces] = null;
                Session[sessionAccesmentReg] = null;
                Session[sessionProtectors] = null;
                Session["ChiefUserCD"] = null;
                Session["ChiefUserName"] = null;
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return true;
        }

        public string LoadUsageLocation()
        {
            string AppendGrid = null;
            try
            {
                LedgerUsageLocationModel usageLocation = new LedgerUsageLocationModel();
                if (Session[nameof(LedgerUsageLocationModel)] != null)
                {
                    usageLocation = (LedgerUsageLocationModel)Session[nameof(LedgerUsageLocationModel)];
                }

                var returnValue = Session[SessionConst.CurrentRowId];
                var current = (LedgerUsageLocationModel)returnValue;
                List<LedgerUsageLocationModel> usageLocations = new List<LedgerUsageLocationModel>();
                if (Session[sessionUsageLocations] != null)
                {
                    usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                }
                if (usageLocations.Any(x => x.RowId == current.RowId))
                {
                    var result = new List<LedgerUsageLocationModel>();
                    foreach (var usageLoc in usageLocations)
                    {
                        result.Add(usageLoc.RowId == current.RowId ? current : usageLoc);
                    }
                    Session.Add(sessionUsageLocations, result.AsEnumerable());
                }
                else
                {
                    var ledger = (LedgerModel)Session[nameof(LedgerModel)] ?? new LedgerModel();
                    var model = (LedgerUsageLocationModel)returnValue;
                    model.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;

                    model.REG_NO = ledger.REG_NO;
                    //model.PROTECTOR_COLLECTION = model.PROTECTOR_COLLECTION.Where(x => x.CHECK);
                    if (model.PROTECTOR_COLLECTION != null && model.PROTECTOR_COLLECTION.Count() > 0)
                    {
                        foreach (var item in model.PROTECTOR_COLLECTION)
                        {
                            item.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                            item.REG_NO = ledger.REG_NO;
                        }
                    }
                    if (model.RISK_REDUCE_COLLECTION != null && model.RISK_REDUCE_COLLECTION.Count() > 0)
                    {
                        foreach (var item in model.RISK_REDUCE_COLLECTION)
                        {
                            item.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                            item.REG_NO = ledger.REG_NO;
                            item.REG_USER_CD = ZyCoaG.Classes.util.Common.GetLogonForm().User_CD;
                        }
                    }

                    if (model.ACCESMENT_REG_COLLECTION != null && model.ACCESMENT_REG_COLLECTION.Count() > 0)
                    {
                        foreach (var item in model.ACCESMENT_REG_COLLECTION)
                        {
                            item.LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                            item.REG_NO = ledger.REG_NO;
                            item.REG_USER_CD = ZyCoaG.Classes.util.Common.GetLogonForm().User_CD;
                        }
                    }
                    else
                    {
                        model.ACCESMENT_REG_COLLECTION = new List<LedgerAccesmentRegModel>();
                    }
                    usageLocations.Add(model);
                    Session.Add(sessionUsageLocations, usageLocations.AsEnumerable());
                }
                if (usageLocations != null && usageLocations.Count > 0)
                {
                    int usagelocationcounter = 1;
                    string styleval = null;

                    foreach (var usageLoc in usageLocations)
                    {
                        if (usagelocationcounter != 1)
                        {
                            if (usagelocationcounter % 2 == 0)
                            {
                                styleval = "background-color:seashell";
                            }
                        }
                        AppendGrid += "<tr style='" + styleval + "'><td align='center'><input name='UsageLocationGridCheck' id='UsageLocationGrid_CHECK' type='checkbox' value=" + usageLoc.RowId + " onclick='return usagegridcheck();'></td>" +
                        "<td><a href='#' class='label' id='UsageLocationGrid_LOC_NAME" + usagelocationcounter + "' style='width:20em;display:inline-block;color:blue' value=" + usageLoc.RowId + "  onclick=EditUseLocation('" + usageLoc.RowId + "')>" + usageLoc.LOC_NAME + "</a>" +
                        "</td><td><span class='label' id='AMOUNTWITHUNITSIZE" + usagelocationcounter + "' style='width: 6em; display: inline-block;'>" + usageLoc.AMOUNTWITHUNITSIZE + "</span>" +
                        "</td><td><span class='label' id='ISHA_USAGE_TEXT" + usagelocationcounter + "' style='width: 12em; display: inline-block;'>" + usageLoc.ISHA_USAGE_TEXT + "</span>" +
                        "</td><td><span class='label' id='ISHA_OTHER_USAGE" + usagelocationcounter + "' style='width: 17em; display: inline-block;'>" + usageLoc.ISHA_OTHER_USAGE + "</span>" +
                        "</td><td><span class='label' id='CHEM_OPERATION_CHIEF_CD" + usagelocationcounter + "' style='width: 13em; display: inline-block;'>" + usageLoc.CHEM_OPERATION_CHIEF_CD + "</span>" +
                        "</td><td><span class='label' id='CHEM_OPERATION_CHIEF" + usagelocationcounter + "' style='width: 13em; display: inline-block;'>" + usageLoc.CHEM_OPERATION_CHIEF_NAME + "</span>" +
                        "</td><td><span class='label' id='SUB_CONTAINER_TEXT" + usagelocationcounter + "' style='width: 8em; display: inline-block;'>" + usageLoc.SUB_CONTAINER_TEXT + "</span>" +
                        "</td><td><span class='label' id='ENTRY_DEVICE" + usagelocationcounter + "' style='width: 8em; display: inline-block;'>" + usageLoc.ENTRY_DEVICE + "</span>" +
                        "</td><td><span class='label' id='PROTECTOR_TEXT" + usagelocationcounter + "' style='width: 32em; display: inline-block;'>" + usageLoc.PROTECTOR_TEXT + "</span>" +
                        "</td><td><span class='label' id='OTHER_PROTECTOR" + usagelocationcounter + "' style='width: 9em; display: inline-block;'>" + usageLoc.OTHER_PROTECTOR + "</span>" +
                        "</td><td><span class='label' id='PROTECTOR_COUNT" + usagelocationcounter + "' style='width: 7em; display: inline-block;'>" + usageLoc.PROTECTOR_COUNT + "</span>" +
                        "</td><td><span class='label' id='MEASURES_BEFORE_SCORE" + usagelocationcounter + "' style='width: 7em; display: inline-block;'>" + usageLoc.MEASURES_BEFORE_SCORE + "</span>" +
                        "</td><td><span class='label' id='MEASURES_BEFORE_RANK_TEXT" + usagelocationcounter + "' style='width: 8em; display: inline-block;'>" + usageLoc.MEASURES_BEFORE_RANK_TEXT + "</span>" +
                        "</td><td><span class='label' id='MEASURES_AFTER_SCORE" + usagelocationcounter + "' style='width: 7em; display: inline-block;'>" + usageLoc.MEASURES_AFTER_SCORE + "</span>" +
                        "</td><td><span class='label' id='MEASURES_AFTER_RANK_TEXT" + usagelocationcounter + "' style='width: 8em; display: inline-block;'>" + usageLoc.MEASURES_AFTER_RANK_TEXT + "</span>" +
                        "</td></tr>";
                        usagelocationcounter++;
                    }
                }
                Session[SessionConst.CategoryTableName] = null;
                Session[SessionConst.CurrentRowId] = null;
                Session[SessionConst.CategorySelectReturnValue] = null;
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return AppendGrid;
        }

        public string GetLocationName(string LocId)
        {
            V_LOCATION location = new V_LOCATION();
            var masterKeys = new Dictionary<string, string>();
            masterKeys.Add("P_LOC_ID", LocId);
            try
            {
                using (var dataInfo = new LocationMasterInfo(LocationClassId.Usage))
                {
                    if (LocId != "")
                    {
                        location = dataInfo.GetData(masterKeys.Values.AsEnumerable().Single().ToInt());
                    }
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return location.LOC_NAME;
        }


        public string BindChiefValues(string LocId)
        {
            V_LOCATION location = new V_LOCATION();
            string UserGrid = null;
            var masterKeys = new Dictionary<string, string>();
            //var DataGridChef = new Tuple<string, string>();
            string UserCD = null;
            string Username = null;
            masterKeys.Add("P_LOC_ID", LocId);

            if (masterKeys != null)
            {
                List<M_USER> griddata = SearchMChief(masterKeys);
                if (griddata != null && griddata.Count > 0)
                {
                    UserGrid += "<thead><tr align='center' style='background-color:Maroon;color:White;font-weight:bold;'>";
                    UserGrid += "<th style='width:70px'><font style='vertical-align: inherit;'><font style='vertical-align:inherit;'>NID</font></font></th>";
                    UserGrid += "<th style='width:150px'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'>氏名</font></font></th></tr></thead><tbody>";
                    foreach (var items in griddata)
                    {
                        UserGrid += "<tr style='background-color:#F0FFF0;'>";
                        UserGrid += " <td style='width:50px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'></font>" + items.USER_CD + "</font></td>";
                        UserGrid += " <td style='width:50px;'><font style='vertical-align:inherit;'><font style='vertical-align:inherit;'></font>" + items.USER_NAME + "</font></td></tr>";
                        UserCD += items.USER_CD + ";";
                        Username += items.USER_NAME + ";";
                    }
                    UserGrid += "</tbody>";
                    UserCD = UserCD.TrimEnd(';');
                    Username = Username.TrimEnd(';');
                    Session["ChiefUserCD"] = UserCD;
                    Session["ChiefUserName"] = Username;
                }
            }

            return UserGrid;
        }

        public List<M_USER> SearchMChief(Dictionary<string, string> locid)
        {
            List<M_USER> data = null;
            try
            {
                var sql = new StringBuilder();
                var parameter = new DynamicParameters();

                foreach (KeyValuePair<string, string> loccode in locid)
                {
                    sql.AppendLine("select ");
                    sql.AppendLine("C.USER_CD ");
                    sql.AppendLine(",U.USER_NAME ");
                    sql.AppendLine("from ");
                    sql.AppendLine("M_CHIEF as C");
                    sql.AppendLine(",M_USER as U");
                    sql.AppendLine("where ");
                    sql.AppendLine("C.LOC_ID = ");
                    sql.AppendLine("@" + loccode.Key);
                    sql.AppendLine(" and ");
                    sql.AppendLine("C.USER_CD = U.USER_CD ");
                    parameter.Add(loccode.Key, loccode.Value);
                }
                data = DbUtil.Select<M_USER>(sql.ToString(), parameter);
            }
            catch (Exception exe)
            {

                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return data;
        }

        [HttpPost]
        public JsonResult RiskBeforeEvaluateCommand(LedgerCommonInfo ctrlIU)
        {
            var errorMessages = new List<string>();
            LedgerUsageLocationModel model = new LedgerUsageLocationModel();
            string unregisteredledger = null;
            LedgerModel sessionledger = new LedgerModel();
            LedgerCommonInfo ledgerCommonInfo = new LedgerCommonInfo();
            List<string> resultevaluate = new List<string>();

            try
            {
                if (Session[nameof(LedgerUsageLocationModel)] != null)
                {
                    model = (LedgerUsageLocationModel)Session[nameof(LedgerUsageLocationModel)];
                }
                if (model == null)
                {
                    model = new LedgerUsageLocationModel();
                }

                if (Session[SessionConst.UnRegisteredChemCD] != null)
                {
                    unregisteredledger = (string)Session[SessionConst.UnRegisteredChemCD];
                }


                if (Session[nameof(LedgerModel)] != null)
                {
                    sessionledger = (LedgerModel)Session[nameof(LedgerModel)];
                }
                if (Session[SessionConst.UnRegisteredChemCD] != null)
                {
                    unregisteredledger = (string)Session[SessionConst.UnRegisteredChemCD];
                }
                bool flgUnreisteredChem = false;



                if (unregisteredledger == NikonConst.UnregisteredChemCd)
                {
                    flgUnreisteredChem = true;
                }
                else
                {
                    flgUnreisteredChem = false;
                }



                //if (Request.Form["FLG_UNREGISTERED_CHEM"] != null)
                //{
                //    string flagval = Request.Form["FLG_UNREGISTERED_CHEM"];
                //    if (!string.IsNullOrEmpty(flagval))
                //    {
                //        flgUnreisteredChem = true;
                //    }
                //}

                if (ctrlIU.ISHA_USAGE != null)
                {
                    List<ApplicableLawGridModel> applicableLawGridModel = updateRAList(true, sessionledger.LEDGER_FLOW_ID, ctrlIU);

                    if (applicableLawGridModel != null && applicableLawGridModel.Count > 0)
                    {

                    }
                    Session["ApplicableLawGridModel"] = applicableLawGridModel;
                }

                if (!flgUnreisteredChem)
                {
                    // ②職場の取扱量
                    int intRTV = System.Convert.ToInt32(ctrlIU.REG_TRANSACTION_VOLUME);
                    decimal valueRTV = decimal.Parse(new CommonMasterInfo(NikonCommonMasterName.REG_TRANSACTION_VOLUME_VALUE).GetDictionary().GetValueToString(intRTV));

                    // ③作業頻度
                    int intRWF = System.Convert.ToInt32(ctrlIU.REG_WORK_FREQUENCY);
                    decimal valueRWF = decimal.Parse(new CommonMasterInfo(NikonCommonMasterName.REG_WORK_FREQUENCY_VALUE).GetDictionary().GetValueToString(intRWF));

                    // ④災害発生の可能性
                    int intRDP = System.Convert.ToInt32(ctrlIU.REG_DISASTER_POSSIBILITY);
                    decimal valueRDP = decimal.Parse(new CommonMasterInfo(NikonCommonMasterName.REG_DISASTER_POSSIBILITY_VALUE).GetDictionary().GetValueToString(intRDP));

                    // アセスメント評価値:(①適用法令×②職場の取扱量×③作業頻度)+④災害発生の可能性
                    decimal dMBS = Math.Round(((TOTAL_MARKS * valueRTV * valueRWF) + valueRDP), 2, MidpointRounding.AwayFromZero);

                    ledgerCommonInfo.MEASURES_BEFORE_SCORE = dMBS;
                    ledgerCommonInfo.MEASURES_BEFORE_SCORE_UNIT = "点";

                    int intRank;
                    if (dMBS < 80)
                    {
                        intRank = 1;
                    }
                    else if (dMBS < 250)
                    {
                        intRank = 2;
                    }
                    else
                    {
                        intRank = 3;
                    }

                    ledgerCommonInfo.MEASURES_BEFORE_RANK_DISP = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary().GetValueToString(intRank);
                    ledgerCommonInfo.MEASURES_BEFORE_RANK = intRank;
                }
                else
                {
                    //ledgerCommonInfo.MEASURES_BEFORE_SCORE = null;
                    ledgerCommonInfo.MEASURES_BEFORE_RANK_DISP = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary().GetValueToString(4);
                    ledgerCommonInfo.MEASURES_BEFORE_RANK = 4;
                    //ledgerCommonInfo.MEASURES_BEFORE_SCORE_UNIT = null;
                }
                resultevaluate.Add(ledgerCommonInfo.MEASURES_BEFORE_RANK_DISP.ToString());
                resultevaluate.Add(ledgerCommonInfo.MEASURES_BEFORE_RANK.ToString());
                resultevaluate.Add(ledgerCommonInfo.MEASURES_BEFORE_SCORE.ToString());
                resultevaluate.Add(ledgerCommonInfo.MEASURES_BEFORE_SCORE_UNIT);
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return Json(resultevaluate, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// リスクアセスメント適用法令の一覧を更新します。
        /// false指定で強制的にチェック状態を解除します。
        /// </summary>
        /// <param name="CheckFLG">true:チェックする/ false:チェックしない</param
        /// <param name="ledgerflowid">フローID（申請番号）</param>
        private List<ApplicableLawGridModel> updateRAList(bool CheckFLG, int? ledgerflowid, LedgerCommonInfo ledgerCommonInfo)
        {
            //List<string> result = new List<string>();
            List<ApplicableLawGridModel> lawGridModels = new List<ApplicableLawGridModel>();
            string sessionledger = null;
            var condition = new LedgerSearchModel();
            try
            {
                condition.REG_NO = regno;
                condition.REG_NO_CONDITION = (int)SearchConditionType.PerfectMatching;
                condition.LEDGER_STATUS = (int)LedgerStatusConditionType.All;
                condition.REG_TYPE_IDS = ledgerCommonInfo.REG_TYPE_IDS;
                bool flgUnreisteredChem = false;


                if (Session[SessionConst.UnRegisteredChemCD] != null)
                {
                    sessionledger = (string)Session[SessionConst.UnRegisteredChemCD];
                }
                if (sessionledger == NikonConst.UnregisteredChemCd)
                {
                    flgUnreisteredChem = true;
                }
                else
                {
                    flgUnreisteredChem = false;
                }
                //bool flgUnreisteredChem = ledgerCommonInfo.FLG_UNREGISTERED_CHEM;
                int intIshaUsage = -1;
                if (ledgerCommonInfo.ISHA_USAGE != null)
                {
                    intIshaUsage = Convert.ToInt32(ledgerCommonInfo.ISHA_USAGE);
                }
                condition.ISHA_USAGE = intIshaUsage;

                var d = new ApplicableLawDataInfo();
                IQueryable<ApplicableLawModel> ledger = null;

                TOTAL_MARKS = 0;

                if (CheckFLG == false || flgUnreisteredChem)
                {
                    DataTable RATable = new DataTable();
                    RATable.Columns.Add("REG_TEXT_BASE", typeof(string));
                    RATable.Columns.Add("REG_TEXT_LAST", typeof(string));
                    RATable.Columns.Add("MARKS", typeof(string));


                    if (RATable.Rows.Count > 0)
                    {
                        foreach (DataRow items in RATable.Rows)
                        {
                            ApplicableLawGridModel applicableLawGridModel = new ApplicableLawGridModel();
                            applicableLawGridModel.REG_TEXT_BASE = items["REG_TEXT_BASE"].ToString();
                            applicableLawGridModel.MARKS = items["MARKS"].ToString();
                            applicableLawGridModel.REG_TEXT_LAST = items["REG_TEXT_LAST"].ToString();
                            applicableLawGridModel.RowCountHeader = "1";
                            lawGridModels.Add(applicableLawGridModel);
                        }
                    }




                    //ApplicableLawGrid.Columns.Add(new BoundField() { DataField = "REG_TEXT_BASE", HeaderText = "適用法令" });
                    //ApplicableLawGrid.Columns.Add(new BoundField() { DataField = "REG_TEXT_LAST", HeaderText = "適用法令（詳細）" });
                    //ApplicableLawGrid.Columns.Add(new BoundField() { DataField = "MARKS", HeaderText = "点数" });

                    //ApplicableLawGrid.ShowHeaderWhenEmpty = true;

                    //ApplicableLawGrid.DataSource = RATable;
                    //ApplicableLawGrid.DataBind();

                    //StringBuilder stringBuilder = new StringBuilder();
                    //StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
                    //HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
                    //ApplicableLawGrid.RenderControl(ourHtmlWriter2);

                    //result.Add(null);
                    TOTAL_MARKS = 1;
                }
                else
                {
                    ledger = d.GetSearchResult(condition);
                    DataTable RATable = new DataTable();
                    RATable.Columns.Add("MARKS", typeof(string));
                    RATable.Columns.Add("REG_TEXT_LAST", typeof(string));
                    RATable.Columns.Add("REG_TEXT_BASE", typeof(string));
                    int iMax = 0;

                    var accesmentregs = new List<LedgerAccesmentRegModel>();
                    bool flgNoApplicable = false;
                    bool flgExistRA = false;
                    foreach (var applicableLaw in ledger.ToList())
                    {
                        var data = new LedgerAccesmentRegModel();
                        var classTools = new ClassTools<LedgerAccesmentRegModel>(data);
                        data.REG_TYPE_ID = applicableLaw.REG_TYPE_ID;
                        data.REG_TEXT = applicableLaw.REG_TEXT;
                        data.CAS_NO = applicableLaw.CAS_NO;
                        data.REG_TEXT_LAST = applicableLaw.REG_TEXT_LAST;
                        data.REG_ICON_PATH = applicableLaw.REG_ICON_PATH;
                        data.WORKTIME_MGMT = applicableLaw.WORKTIME_MGMT;
                        data.LEDGER_FLOW_ID = ledgerflowid;
                        if (applicableLaw.RA_FLAG == 1)
                        {
                            flgExistRA = true;
                            accesmentregs.Add(data);
                        }

                        List<String> RAList = new List<string>(applicableLaw.REG_TEXT_BASE.Split('/'));
                        if (iMax < RAList.Count) { iMax = RAList.Count; }
                    }

                    for (int Listi = 1; Listi <= iMax; Listi++)
                    {
                        RATable.Columns.Add("REG_TEXT_BASE" + Listi.ToString(), typeof(string));
                    }

                    // RAフラグの立ったデータがない場合は該当なしの表にする
                    if (!flgExistRA)
                    {
                        var model = new ApplicableLawModel();
                        model.REG_TEXT = "該当なし/-";
                        model.REG_TEXT_BASE = "該当なし";
                        model.REG_TEXT_LAST = "-";
                        model.MARKS = 1;
                        var dataArray = new List<ApplicableLawModel>();
                        dataArray.Add(model);
                        ledger = dataArray.AsQueryable();
                        ledger.OrderBy(x => x.REG_TYPE_ID);
                        iMax = 0;

                        foreach (var applicableLaw in ledger.ToList())
                        {
                            var data = new LedgerAccesmentRegModel();
                            var classTools = new ClassTools<LedgerAccesmentRegModel>(data);
                            data.REG_TYPE_ID = applicableLaw.REG_TYPE_ID;
                            data.REG_TEXT = applicableLaw.REG_TEXT;
                            data.CAS_NO = applicableLaw.CAS_NO;
                            data.REG_TEXT_LAST = applicableLaw.REG_TEXT_LAST;
                            data.REG_ICON_PATH = applicableLaw.REG_ICON_PATH;
                            data.WORKTIME_MGMT = applicableLaw.WORKTIME_MGMT;
                            data.LEDGER_FLOW_ID = ledgerflowid;

                            List<String> RAList = new List<string>(applicableLaw.REG_TEXT_BASE.Split('/'));
                            if (iMax < RAList.Count) { iMax = RAList.Count; }
                        }

                        RATable = new DataTable();
                        RATable.Columns.Add("MARKS", typeof(string));
                        RATable.Columns.Add("REG_TEXT_LAST", typeof(string));
                        RATable.Columns.Add("REG_TEXT_BASE", typeof(string));

                        for (int Listi = 1; Listi <= iMax; Listi++)
                        {
                            RATable.Columns.Add("REG_TEXT_BASE" + Listi.ToString(), typeof(string));
                        }
                    }

                    foreach (var applicableLaw in ledger.ToList())
                    {
                        if (applicableLaw.RA_FLAG == 1 || applicableLaw.REG_TEXT_BASE == "該当なし")
                        {
                            List<String> RAList = new List<string>(applicableLaw.REG_TEXT_BASE.Split('/'));
                            DataRow dr = RATable.NewRow();
                            dr["MARKS"] = applicableLaw.MARKS;
                            dr["REG_TEXT_LAST"] = applicableLaw.REG_TEXT_LAST;
                            dr["REG_TEXT_BASE"] = RAList[0];
                            for (int iCnt = 1; iCnt < iMax; iCnt++)
                            {
                                if (iCnt < RAList.Count)
                                {
                                    dr["REG_TEXT_BASE" + iCnt] = RAList[iCnt];
                                }
                                else
                                {
                                    dr["REG_TEXT_BASE" + iCnt] = "-";
                                }
                            }

                            RATable.Rows.Add(dr);
                            TOTAL_MARKS += Convert.ToDecimal(applicableLaw.MARKS);

                            if (applicableLaw.REG_TEXT_BASE == "該当なし")
                            {
                                flgNoApplicable = true;
                            }
                        }
                    }

                    // 該当なしは管理台帳詳細に戻さない
                    if (flgNoApplicable)
                    {
                        accesmentregs = new List<LedgerAccesmentRegModel>();
                    }
                    Session.Add(sessionAccesmentReg, accesmentregs.AsEnumerable());

                    //点数合計追加
                    //DataRow Sumdr = RATable.NewRow();
                    //Sumdr["REG_TEXT_BASE"] = "点数合計";
                    //Sumdr["MARKS"] = TOTAL_MARKS;
                    // RATable.Rows.Add(Sumdr);


                    if (RATable.Rows.Count > 0)
                    {
                        foreach (DataRow items in RATable.Rows)
                        {
                            ApplicableLawGridModel applicableLawGridModel = new ApplicableLawGridModel();
                            applicableLawGridModel.REG_TEXT_BASE = items["REG_TEXT_BASE"].ToString();
                            applicableLawGridModel.MARKS = items["MARKS"].ToString();
                            applicableLawGridModel.REG_TEXT_LAST = items["REG_TEXT_LAST"].ToString();
                            applicableLawGridModel.RowCountHeader = "1";
                            lawGridModels.Add(applicableLawGridModel);
                        }
                    }

                    //ApplicableLawGrid.Columns.Add(new BoundField() { DataField = "REG_TEXT_BASE", HeaderText = "適用法令" });
                    //for (int i = 1; i < iMax; i++)
                    //{
                    //    ApplicableLawGrid.Columns.Add(new BoundField() { DataField = "REG_TEXT_BASE" + i.ToString() });
                    //}

                    //ApplicableLawGrid.Columns.Add(new BoundField() { DataField = "REG_TEXT_LAST", HeaderText = "適用法令（詳細）" });
                    //ApplicableLawGrid.Columns.Add(new BoundField() { DataField = "MARKS", HeaderText = "点数" });
                    //ApplicableLawGrid.DataSource = RATable;
                    //ApplicableLawGrid.DataBind();

                    ////Grid結合処理
                    //for (int i = 1; i < iMax; i++)
                    //{
                    //    ApplicableLawGrid.HeaderRow.Cells[i].Visible = false;
                    //}
                    //ApplicableLawGrid.HeaderRow.Cells[0].ColumnSpan = iMax;
                    //for (int ColmPos = 0; ColmPos < ApplicableLawGrid.Columns.Count - 1; ColmPos++)
                    //{
                    //    int CellJoinCnt = 1;
                    //    for (int CellPos = 1; CellPos < ApplicableLawGrid.Rows.Count - 1; CellPos++)
                    //    {
                    //        if (ApplicableLawGrid.Rows[CellPos - CellJoinCnt].Cells[ColmPos].Text == ApplicableLawGrid.Rows[CellPos].Cells[ColmPos].Text
                    //            && ApplicableLawGrid.Rows[CellPos - CellJoinCnt].Cells[ColmPos].Text != "-")
                    //        {
                    //            ApplicableLawGrid.Rows[CellPos - CellJoinCnt].Cells[ColmPos].RowSpan = CellJoinCnt + 1;
                    //            CellJoinCnt++;
                    //            ApplicableLawGrid.Rows[CellPos].Cells[ColmPos].Visible = false;
                    //        }
                    //        else
                    //        {
                    //            CellJoinCnt = 1;
                    //        }
                    //    }
                    //}
                    //for (int i = 1; i < ApplicableLawGrid.Columns.Count - 1; i++)
                    //{
                    //    ApplicableLawGrid.Rows[ApplicableLawGrid.Rows.Count - 1].Cells[i].Visible = false;
                    //}
                    //ApplicableLawGrid.Rows[ApplicableLawGrid.Rows.Count - 1].Cells[0].ColumnSpan = ApplicableLawGrid.Columns.Count - 1;
                    //ApplicableLawGrid.Rows[ApplicableLawGrid.Rows.Count - 1].Cells[0].Style.Add("text-align", "Right");
                    //ApplicableLawGrid.Rows[ApplicableLawGrid.Rows.Count - 1].Cells[0].BackColor = Color.DarkRed;
                    //ApplicableLawGrid.Rows[ApplicableLawGrid.Rows.Count - 1].Cells[0].ForeColor = Color.White;
                    //ApplicableLawGrid.Rows[ApplicableLawGrid.Rows.Count - 1].Cells[0].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");

                    //StringBuilder stringBuilder = new StringBuilder();
                    //StringWriter ourTextWriter2 = new StringWriter(stringBuilder);
                    //HtmlTextWriter ourHtmlWriter2 = new HtmlTextWriter(ourTextWriter2);
                    //ApplicableLawGrid.RenderControl(ourHtmlWriter2);
                    //result.Add(null);
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return lawGridModels;
            //return Json(result, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// リスク低減策後アセスメント評価値、対策後のリスクレベルを計算します。
        /// </summary>
        /// <param name="flg">true:エラーメッセージを出力する/ false:エラーメッセージを出力しない</param>
        [HttpPost]
        public JsonResult RiskAfterEvaluate(LedgerCommonInfo ledgerCommonInfo)
        {
            List<string> result = new List<string>();
            LedgerUsageLocationModel model = new LedgerUsageLocationModel();
            string sessionledger = null;
            try
            {
                if (Session[SessionConst.UnRegisteredChemCD] != null)
                {
                    sessionledger = (string)Session[SessionConst.UnRegisteredChemCD];
                }
                if (sessionledger == NikonConst.UnregisteredChemCd)
                {
                    ledgerCommonInfo.FLG_UNREGISTERED_CHEM = true;
                }
                else
                {
                    ledgerCommonInfo.FLG_UNREGISTERED_CHEM = false;
                }
                if (!ledgerCommonInfo.FLG_UNREGISTERED_CHEM)
                {
                    if (Session[nameof(LedgerUsageLocationModel)] != null)
                    {
                        model = (LedgerUsageLocationModel)Session[nameof(LedgerUsageLocationModel)];
                    }
                    decimal dMBS = decimal.Parse(ledgerCommonInfo.MEASURES_BEFORE_SCORE.ToString());

                    decimal totalPRIORITY = 0;
                    if (Session[sessionRiskReduces] != null)
                    {
                        List<LedgerRiskReduceModel> objLedgerRisk = new List<LedgerRiskReduceModel>();
                        objLedgerRisk = (List<LedgerRiskReduceModel>)Session[sessionRiskReduces];
                        foreach (var items in objLedgerRisk)
                        {
                            if (items.IsChecked)
                            {
                                decimal valuePRIORITY = decimal.Parse(new CommonMasterInfo(NikonCommonMasterName.RISK_REDUCE_VALUE).GetDictionary().GetValueToString(items.LED_RISK_REDUCE_TYPE));
                                totalPRIORITY += valuePRIORITY;
                            }
                        }
                    }
                    decimal dMAS = Math.Round((dMBS * (1 - totalPRIORITY)), 2, MidpointRounding.AwayFromZero);
                    ledgerCommonInfo.MEASURES_AFTER_SCORE = dMAS;
                    ledgerCommonInfo.MEASURES_AFTER_SCORE_UNIT = "点";

                    int intRank;
                    if (dMAS < 80)
                    {
                        intRank = 1;
                    }
                    else if (dMAS < 250)
                    {
                        intRank = 2;
                    }
                    else
                    {
                        intRank = 3;
                        //measuresafterrankdisp.ForeColor = System.Drawing.Color.Red;
                    }

                    ledgerCommonInfo.MEASURES_AFTER_RANK_DISP = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary().GetValueToString(intRank);
                    ledgerCommonInfo.MEASURES_AFTER_RANK = intRank;
                }
                else
                {
                    ledgerCommonInfo.MEASURES_AFTER_RANK_DISP = new CommonMasterInfo(NikonCommonMasterName.RANK).GetDictionary().GetValueToString(4);
                    ledgerCommonInfo.MEASURES_AFTER_RANK = 4;
                }
                result.Add(ledgerCommonInfo.MEASURES_AFTER_RANK_DISP.ToString());
                result.Add(ledgerCommonInfo.MEASURES_AFTER_RANK.ToString());
                result.Add(ledgerCommonInfo.MEASURES_AFTER_SCORE.ToString());
                result.Add(ledgerCommonInfo.MEASURES_AFTER_SCORE_UNIT);
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string UsageLocationDelete(string RowId)
        {
            string RowValues = null;
            List<LedgerUsageLocationModel> usageLocations = new List<LedgerUsageLocationModel>();
            try
            {
                if (Session[sessionUsageLocations] != null)
                {
                    usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                    int usagelocationcounter = 0;
                    foreach (var rowid in RowId.Split(','))
                    {
                        for (int i = 0; i < usageLocations.Count; i++)
                        {
                            if (usageLocations[i].RowId.Trim() == rowid.Trim())
                            {
                                usageLocations.RemoveAt(i);
                            }
                        }
                    }

                    string styleval = null;
                    foreach (var usageLoc in usageLocations)
                    {
                        if (usagelocationcounter % 2 == 0)
                        {
                            styleval = "style='background - color: seashell;'";
                        }
                        RowValues += "<tr style=" + styleval + "><td align='center'><input name='UsageLocationGridCheck' id='UsageLocationGrid_CHECK' type='checkbox' value=" + usageLoc.RowId + " onclick='return usagegridcheck();'></td><td>" +
                           "<a href='#' class='label' id='UsageLocationGrid_LOC_NAME" + usagelocationcounter + "' style='width: 20em; display:inline-block;' onclick=EditUseLocation('" + usageLoc.RowId + "')>" + usageLoc.LOC_NAME + "</a>" +
                           "</td><td><span class='label' id='AMOUNTWITHUNITSIZE" + usagelocationcounter + "' style='width: 6em; display: inline-block;'>" + usageLoc.AMOUNTWITHUNITSIZE + "</span>" +
                           "</td><td><span class='label' id='ISHA_USAGE_TEXT" + usagelocationcounter + "' style='width: 12em; display: inline-block;'>" + usageLoc.ISHA_USAGE_TEXT + "</span>" +
                           "</td><td><span class='label' id='ISHA_OTHER_USAGE" + usagelocationcounter + "' style='width: 17em; display: inline-block;'>" + usageLoc.ISHA_OTHER_USAGE + "</span>" +
                           "</td><td><span class='label' id='CHEM_OPERATION_CHIEF_CD" + usagelocationcounter + "' style='width: 13em; display: inline-block;'>" + usageLoc.CHEM_OPERATION_CHIEF_CD + "</span>" +
                           "</td><td><span class='label' id='CHEM_OPERATION_CHIEF" + usagelocationcounter + "' style='width: 13em; display: inline-block;'>" + usageLoc.CHEM_OPERATION_CHIEF_NAME + "</span>" +
                           "</td><td><span class='label' id='SUB_CONTAINER_TEXT" + usagelocationcounter + "' style='width: 8em; display: inline-block;'>" + usageLoc.SUB_CONTAINER_TEXT + "</span>" +
                           "</td><td><span class='label' id='ENTRY_DEVICE" + usagelocationcounter + "' style='width: 8em; display: inline-block;'>" + usageLoc.ENTRY_DEVICE + "</span>" +
                           "</td><td><span class='label' id='PROTECTOR_TEXT" + usagelocationcounter + "' style='width: 32em; display: inline-block;'>" + usageLoc.PROTECTOR_TEXT + "</span>" +
                           "</td><td><span class='label' id='OTHER_PROTECTOR" + usagelocationcounter + "' style='width: 9em; display: inline-block;'>" + usageLoc.OTHER_PROTECTOR + "</span>" +
                           "</td><td><span class='label' id='PROTECTOR_COUNT" + usagelocationcounter + "' style='width: 7em; display: inline-block;'>" + usageLoc.PROTECTOR_COUNT + "</span>" +
                           "</td><td><span class='label' id='MEASURES_BEFORE_SCORE" + usagelocationcounter + "' style='width: 7em; display: inline-block;'>" + usageLoc.MEASURES_BEFORE_SCORE + "</span>" +
                           "</td><td><span class='label' id='MEASURES_BEFORE_RANK_TEXT" + usagelocationcounter + "' style='width: 8em; display: inline-block;'>" + usageLoc.MEASURES_BEFORE_RANK_TEXT + "</span>" +
                           "</td><td><span class='label' id='MEASURES_AFTER_SCORE" + usagelocationcounter + "' style='width: 7em; display: inline-block;'>" + usageLoc.MEASURES_AFTER_SCORE + "</span>" +
                           "</td><td><span class='label' id='MEASURES_AFTER_RANK_TEXT" + usagelocationcounter + "' style='width: 8em; display: inline-block;'>" + usageLoc.MEASURES_AFTER_RANK_TEXT + "</span>" +
                           "</td></tr>";
                        usagelocationcounter++;
                    }

                }
                Session[sessionUsageLocations] = usageLocations.AsEnumerable();
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return RowValues;
        }

        public bool ProtectorGridChange(string CheckVal)
        {
            try
            {
                if (Session[sessionProtectors] != null)
                {
                    List<LedgerRiskReduceModel> objLedgerRisk = new List<LedgerRiskReduceModel>();
                    LedgerUsageLocationModel objLedgerCommonInfo = new LedgerUsageLocationModel();
                    objLedgerCommonInfo.objLedgerProtectorModel = (List<LedgerProtectorModel>)Session[sessionProtectors];

                    for (int i = 0; i < objLedgerCommonInfo.objLedgerProtectorModel.Count; i++)
                    {

                        if (objLedgerCommonInfo.objLedgerProtectorModel[i].PROTECTOR_ID == Convert.ToInt32(CheckVal.Trim()))
                        {
                            objLedgerCommonInfo.objLedgerProtectorModel[i].CHECK = true;
                        }
                    }

                    Session[sessionProtectors] = objLedgerCommonInfo.objLedgerProtectorModel;

                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return true;
        }

        public bool RiskReduceGridChange(string CheckVal)
        {
            try
            {
                if (Session[sessionRiskReduces] != null)
                {
                    List<LedgerRiskReduceModel> objLedgerRisk = new List<LedgerRiskReduceModel>();
                    LedgerUsageLocationModel objLedgerCommonInfo = new LedgerUsageLocationModel();
                    objLedgerCommonInfo.objLedgerRisk = (List<LedgerRiskReduceModel>)Session[sessionRiskReduces];

                    for (int i = 0; i < objLedgerCommonInfo.objLedgerRisk.Count; i++)
                    {

                        if (objLedgerCommonInfo.objLedgerRisk[i].LED_RISK_REDUCE_ID == Convert.ToInt32(CheckVal.Trim()))
                        {
                            objLedgerCommonInfo.objLedgerRisk[i].IsChecked = true;
                        }

                    }
                    Session[sessionRiskReduces] = objLedgerCommonInfo.objLedgerRisk;
                }
            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return true;
        }


        /// <summary>
        /// 保管可能危険物チェックを行います。
        /// </summary>
        /// <param name="locid">場所ID</param>
        /// <param name="chemcd">作成SQL文</param>
        /// <param name="loctype">区分 1:保管場所 / 2:使用場所</param>
        /// <returns>true:危険物登録あり / false:危険物登録なし</returns>
        private bool ChkFireReg(int? locid, string chemcd, int loctype)
        {
            bool ret = false;
            DbContext context = null;
            try
            {
                StringBuilder sbSql = new StringBuilder();
                Hashtable _params = new Hashtable();

                sbSql.Append("select FIRE_ID ");
                sbSql.Append(", FIRE_TYPE ");
                sbSql.Append("from V_FIRE_TYPE ");
                sbSql.Append("where ");
                sbSql.Append("FIRE_ID in (select REG_TYPE_ID from M_CHEM_REGULATION where CHEM_CD = @CHEM_CD) ");
                sbSql.Append("and DEL_FLAG = @DEL_FLAG ");

                _params.Add("CHEM_CD", chemcd);
                _params.Add("DEL_FLAG", (int)DEL_FLAG.Undelete);

                context = DbUtil.Open(true);
                DataTable records = DbUtil.Select(context, sbSql.ToString(), _params);

                string strRegIds = "";
                int i = 0;
                foreach (DataRow record in records.Rows)
                {
                    if (strRegIds != "")
                    {
                        strRegIds += ",";
                    }
                    if (record["FIRE_TYPE"].ToString() != "")
                    {
                        strRegIds += record["FIRE_ID"].ToString();
                        i++;
                    }
                }

                if (i == 0)
                {
                    ret = true;
                }
                else
                {
                    sbSql = new StringBuilder();
                    _params = new Hashtable();

                    sbSql.Append("select count(distinct VFT.FIRE_ID) CNT ");
                    sbSql.Append("from M_LOCATION ML ");
                    sbSql.Append(", V_FIRE_TYPE VFT ");
                    sbSql.Append(", V_LOCATION_HAZARD VLH ");
                    sbSql.Append("where ");
                    sbSql.Append("VFT.FIRE_TYPE = VLH.REG_TEXT_LAST and ");
                    sbSql.Append("ML.LOC_ID = VLH.LOC_ID and ");
                    sbSql.Append("VLH.LOC_ID = :LOC_ID and ");
                    sbSql.Append("ML.CLASS_ID = :CLASS_ID and ");
                    sbSql.Append("VLH.HAZARD_CHECK = :HAZARD_CHECK and ");
                    sbSql.Append("VFT.FIRE_ID in(" + strRegIds + ") ");
                    sbSql.Append("and ML.DEL_FLAG = :DEL_FLAG and VFT.DEL_FLAG = :DEL_FLAG ");
                    sbSql.Append("and VLH.DEL_FLAG = :DEL_FLAG ");

                    _params.Add("LOC_ID", locid);
                    _params.Add("CLASS_ID", loctype);
                    _params.Add("HAZARD_CHECK", (int)Check.Checked);
                    _params.Add("DEL_FLAG", (int)DEL_FLAG.Undelete);

                    records = DbUtil.Select(context, sbSql.ToString(), _params);

                    if (Convert.ToInt16(records.Rows[0]["CNT"].ToString()) == i)
                    {
                        ret = true;
                    }
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
                context.Rollback();
                throw;
            }
            finally
            {
                context.Close();
            }
            return ret;
        }

        // 化学物質に紐づく法規制を取得
        // GetRegulationからかどうか
        private List<LedgerRegulationModel> getChemRegulation(string chemcd, int type)
        {
            var regulationInfo = new Nikon.LedRegulationMasterInfo();
            var ids = regulationInfo.LedgerGetIdsBelongInChem(chemcd);
            string strQuery = string.Join(SystemConst.SelectionsDelimiter, ids.ToList().ConvertAll(x => x.ToString()));
            if (type == 1)
            {
                Session.Add(sessionRegTypeIds, strQuery);
            }
            Session.Add(sessionChemRegTypeIds, strQuery);

            if (strQuery != "" && strQuery != null)
            {
                var d = new LedgerRegulationDataInfo();
                var dataArray = d.GetSearchResultFromChem(strQuery).ToList();
                d.Dispose();
                if (type == 1)
                {
                    Session.Add(sessionRegulations, dataArray.AsEnumerable());
                    setRegulationModel(chemcd, dataArray);
                }
                Session.Add(sessionChemRegulations, dataArray.AsEnumerable());
                return dataArray.ToList();
            }
            else
            {
                var newDataArray2 = new List<LedgerRegulationModel>();
                if (type == 1)
                {
                    Session.Add(sessionRegulations, newDataArray2.AsEnumerable());
                }
                Session.Add(sessionChemRegulations, newDataArray2.AsEnumerable());
                return newDataArray2.ToList();
            }
        }
        private void setRegulationModel(string chemcd, List<LedgerRegulationModel> dataArray)
        {
            LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
            var ids = new List<int>();
            foreach (var data in dataArray)
            {
                ids.Add(Convert.ToInt32(data.REG_TYPE_ID));
            }
            var regulationInfo = new Nikon.LedRegulationMasterInfo();
            var items = regulationInfo.GetSelectedItems(ids).AsQueryable();
            objLedgerCommonInfo.REQUIRED_CHEM = items.Any(x => x.WORKTIME_MGMT == 1) ? "該当" : "非該当";

            var hazmatText = items.FirstOrDefault(x => x.REG_TEXT.IndexOf("消防法") != -1)?.REG_TEXT;
            objLedgerCommonInfo.HAZMAT_TYPE = hazmatText ?? "非該当";

            if (chemcd == NikonConst.UnregisteredChemCd)
            {
                objLedgerCommonInfo.REQUIRED_CHEM = null;
                objLedgerCommonInfo.HAZMAT_TYPE = null;
            }
            // RegTypeIdをセッションにセット(リンクで使用場所明細に飛ぶ時のため。)
            string strRegTypeId = null;
            var regulations = (List<LedgerRegulationModel>)Session[sessionRegulations];
            foreach (LedgerRegulationModel list in regulations)
            {
                if (strRegTypeId != null)
                {
                    strRegTypeId += ",";
                }
                strRegTypeId += Convert.ToInt32(list.REG_TYPE_ID);
            }
            Session.Remove(sessionRegTypeIds);
            Session.Add(sessionRegTypeIds, strRegTypeId);
        }

        /// <summary>
        /// 危険物(第一類～第六類)の法規制かをチェック
        /// </summary>
        /// <param name="conn">セッション情報</param>
        /// <param name="regtypeid">法規制種類ID</param>
        /// <returns>true：危険物 / false：危険物でない</returns>
        private bool ChkHazardousReg(SqlConnection conn, string regtypeids)
        {
            bool ret = false;
            try
            {
                SqlCommand cmd = new SqlCommand();
                StringBuilder sbSql = new StringBuilder();

                sbSql.Append("select FIRE_ID from V_FIRE_TYPE where FIRE_TYPE in (");
                sbSql.Append(sqlHazardous);
                sbSql.Append(") and DEL_FLAG = 0 and ");
                sbSql.Append(" FIRE_ID in (");
                sbSql.Append(regtypeids);
                sbSql.Append(") ");

                cmd.Connection = conn;
                cmd.CommandText = sbSql.ToString();
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataTable records = new DataTable();
                adp.Fill(records);

                if (records.Rows.Count > 0)
                {
                    ret = true;
                }


            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return ret;
        }

        /// <summary>
        /// 少危険物量未満の場所を取得
        /// </summary>
        /// <param name="conn">セッション情報</param>
        /// <param name="loctype">区分 1:保管場所 / 2:使用場所</param>
        /// <param name="locid">場所ID</param>
        /// <returns>true：少危険物量未満の場所 / false：少危険物量未満の場所でない</returns>
        private bool ChkHazardousLoc(SqlConnection conn, int loctype, int locid)
        {
            bool ret = false;
            try
            {
                StringBuilder sbSql = new StringBuilder();
                SqlCommand cmd = new SqlCommand();

                sbSql.Append("select ");
                sbSql.Append("mfl.LOC_ID, mfl.CLASS_ID ");
                sbSql.Append("from M_FIRE_LOCATION mfl, M_FIRE_REPORT mfr, M_COMMON mc ");
                sbSql.Append("where ");
                sbSql.Append("mfl.REPORT_ID = mfr.REPORT_ID ");
                sbSql.Append("and mfr.REPORT_PLACE_KEY = mc.KEY_VALUE ");
                sbSql.Append("and mc.TABLE_NAME = 'M_PLACE' and mc.DISPLAY_VALUE = '少量危険物未満' ");
                sbSql.Append("and mfl.CLASS_ID= @CLASS_ID ");
                sbSql.Append("and mfl.LOC_ID= @LOC_ID ");
                sbSql.Append("and mfr.DEL_FLAG = @DEL_FLAG ");

                cmd.Parameters.AddWithValue("CLASS_ID", loctype);
                cmd.Parameters.AddWithValue("LOC_ID", locid);
                cmd.Parameters.AddWithValue("DEL_FLAG", (int)DEL_FLAG.Undelete);

                cmd.Connection = conn;
                cmd.CommandText = sbSql.ToString();
                cmd.ExecuteNonQuery();
                SqlDataAdapter adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataTable dt = new DataTable();
                adp.Fill(dt);

                if (dt.Rows.Count > 0)
                {
                    ret = true;
                }
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }

            return ret;
        }

        /// <summary>
        /// 「少量危険物未満管理」の届出に紐づく場所が入力された管理台帳のチェック処理
        /// </summary>
        /// <param name="conn">セッション情報</param>
        /// <param name="regtypeid">法規制種類ID</param>
        /// <param name="latelyAmounts">今回申請の数量</param>
        /// <param name="arrAmounts">前行までの場所IDと容量</param>
        /// <param name="TmpHazardous">消防法を危険物種類ごとに取得したテーブル</param>
        /// <param name="loctype">区分 1:保管場所/2:使用場所</param>
        /// <param name="locId">場所ID</param>
        /// <param name="model">管理台帳クラス情報</param>
        /// <param name="unitKG">KGの単位ID</param>
        /// <param name="unitL">Lの単位ID</param>
        /// <paramref name="chkOver">指定数量1倍以上チェックフラグ</paramref>
        /// <returns>0：0.2未満 / 1：0.2超え / 2：場所が複数の消防法に登録済 / 3：届出数量の登録なし</returns>
        private int CheckSmallHazardous(SqlConnection conn, string regtypeid, decimal latelyAmounts, ArrayList arrAmounts, DataTable TmpHazardous, int loctype, int locId, LedgerModel model, int unitKG, int unitL,
            ref bool chkOver)
        {
            int ret = 1;
            decimal ttlharzards = 0;
            string locs = null;
            chkOver = true;

            // 消防法に届け出ているレポートを特定
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            StringBuilder sqlReportIds = new StringBuilder();
            sqlReportIds.Append("select REPORT_ID from M_FIRE_REPORT ");
            sqlReportIds.Append(" where REPORT_ID in( select REPORT_ID from M_FIRE_LOCATION where CLASS_ID = @CLASS_ID and LOC_ID = @LOC_ID) ");
            sqlReportIds.Append("  and DEL_FLAG =@DEL_FLAG");
            cmd.CommandText = sqlReportIds.ToString();
            cmd.Parameters.AddWithValue("CLASS_ID", loctype);
            cmd.Parameters.AddWithValue("LOC_ID", locId);
            cmd.Parameters.AddWithValue("DEL_FLAG", (int)DEL_FLAG.Undelete);
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            DataTable dtReports = new DataTable();
            adp.Fill(dtReports);
            if (dtReports.Rows.Count == 0)
            {
                ret = 3;
                return ret;
            }
            if (dtReports.Rows.Count > 1)
            {
                ret = 2;
                return ret;
            }

            // レポートに紐づく場所を取得
            cmd = new SqlCommand();
            cmd.Connection = conn;
            StringBuilder sqlLocs = new StringBuilder();
            sqlLocs.Append("select mfl.LOC_ID ");
            sqlLocs.Append("from M_FIRE_REPORT mfr ");
            sqlLocs.Append("left join M_FIRE_LOCATION mfl on mfr.REPORT_ID = mfl.REPORT_ID ");
            sqlLocs.Append("where mfl.CLASS_ID = @CLASS_ID and mfr.REPORT_ID = @REPORT_ID and mfr.DEL_FLAG = @DEL_FLAG");
            cmd.CommandText = sqlLocs.ToString();
            cmd.Parameters.AddWithValue("CLASS_ID", loctype);
            cmd.Parameters.AddWithValue("REPORT_ID", dtReports.Rows[0]["REPORT_ID"].ToString());
            cmd.Parameters.AddWithValue("DEL_FLAG", (int)DEL_FLAG.Undelete);
            cmd.ExecuteNonQuery();
            adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            DataTable dtLocs = new DataTable();
            adp.Fill(dtLocs);

            ArrayList al = new ArrayList();
            foreach (DataRow record2 in dtLocs.Rows)
            {
                if (locs != null)
                {
                    locs += ",";
                }
                locs += record2["LOC_ID"].ToString();
                al.Add(record2["LOC_ID"].ToString());
            }

            // 前行も同じレポートに含まれる場所であれば加算
            foreach (DictionaryEntry am in arrAmounts)
            {
                if (al.Contains(am.Key.ToString()))
                {
                    latelyAmounts += Convert.ToDecimal(am.Value);
                }
            }

            foreach (DataRow th in TmpHazardous.Rows)
            {
                bool flgLately = false;
                cmd = new SqlCommand();
                adp = new SqlDataAdapter();
                decimal decAmount = 0;
                decimal ttlAmount = 0;

                string hazardoustype = th["reg_text2"].ToString();

                // 今回申請が危険物にヒットしているか
                cmd.CommandText = "select FIRE_ID from V_FIRE where FIRE_ID = @FIRE_ID and FIRE_NAME like '%/" + hazardoustype + "%' and DEL_FLAG = @DEL_FLAG";
                cmd.Parameters.AddWithValue("FIRE_ID", regtypeid);
                cmd.Parameters.AddWithValue("DEL_FLAG", (int)DEL_FLAG.Undelete);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                adp.SelectCommand = cmd;
                DataTable htyperecord = new DataTable();
                adp.Fill(htyperecord);
                if (htyperecord.Rows.Count == 1)
                {
                    flgLately = true;
                }

                // フロー中数量、申請済数量の合計を取得
                cmd = new SqlCommand();
                Sql_GetAmounts(loctype, locs, model, hazardoustype, unitKG, unitL, ref cmd);
                cmd.Connection = conn;
                cmd.ExecuteNonQuery();
                adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataTable records = new DataTable();
                adp.Fill(records);

                // 数量合計
                foreach (DataRow record in records.Rows)
                {
                    decAmount = Convert.ToDecimal(record["AMOUNTS"]);
                }

                if (flgLately)
                {
                    ttlAmount = decAmount + latelyAmounts;
                }
                else
                {
                    ttlAmount = decAmount;
                }

                // 倍数(最大保管量/指定数量)
                ttlharzards += (ttlAmount / Convert.ToDecimal(th["FIRE_SIZE"].ToString()));
            }

            // 倍数が0.2未満かどうか
            if (ttlharzards < Convert.ToDecimal(0.2))
            {
                ret = 0;
            }

            // 指定数量1倍以上チェック
            cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = GetSql_OverChk(locs, ref cmd);
            cmd.ExecuteNonQuery();
            adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            DataTable dtOverchk = new DataTable();
            adp.Fill(dtOverchk);

            int cntOverchk = Convert.ToInt32(dtOverchk.Rows[0]["CNT"]);

            if (cntOverchk != 0)
            {
                if (ttlharzards / cntOverchk > 1)
                {
                    chkOver = false;
                }
            }
            return ret;
        }

        /// <summary>
        /// フロー中数量、申請済数量の合計を取得するSQL
        /// </summary>
        /// <param name="loctype">区分 1:保管場所 / 2:使用場所</param>
        /// <param name="locId">場所ID</param>
        /// <param name="model">管理台帳クラス情報</param>
        /// <param name="hazardoustype">危険物の名称</param>
        /// <param name="unitKG">KGの単位ID</param>
        /// <param name="unitL">Lの単位ID</param>
        /// <paramref name="cmd">セッション情報</paramref>
        private void Sql_GetAmounts(int loctype, string locId, LedgerModel model, string hazardoustype, int unitKG, int unitL, ref SqlCommand cmd)
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql.Append("select led.LOC_ID, sum(Convert(float, led.REG_CONV_AMOUNTS)) AMOUNTS ");
            sbSql.Append("from( ");
            sbSql.Append("  select conv.LOC_ID ");
            sbSql.Append("  ,CASE ");
            sbSql.Append("      WHEN conv.REG_UNITSIZE_ID = conv.CONVERTED_UNITSIZE_ID THEN conv.CONV_AMOUNTS ");
            sbSql.Append("      WHEN conv.REG_UNITSIZE_ID <> conv.CONVERTED_UNITSIZE_ID and conv.REG_UNITSIZE_ID = @UNIT_KG THEN conv.CONV_AMOUNTS*conv.DENSITY ");
            sbSql.Append("      WHEN conv.REG_UNITSIZE_ID <> conv.CONVERTED_UNITSIZE_ID and conv.REG_UNITSIZE_ID = @UNIT_L THEN conv.CONV_AMOUNTS/conv.DENSITY ");
            sbSql.Append("      ELSE 0 ");
            sbSql.Append("  END REG_CONV_AMOUNTS ");
            sbSql.Append("  from( ");

            sbSql.Append("    select distinct ml.REG_NO, ml.RECENT_LEDGER_FLOW_ID, tmll.LOC_ID, mr.UNITSIZE_ID REG_UNITSIZE_ID, mc.DENSITY, tcu.CONVERTED_UNITSIZE_ID ");
            sbSql.Append("    ,CASE ");
            sbSql.Append("        WHEN tcu.UNITSIZE_ID = tcu.CONVERTED_UNITSIZE_ID THEN tmll.AMOUNT ");
            sbSql.Append("        WHEN tcu.UNITSIZE_ID <> tcu.CONVERTED_UNITSIZE_ID THEN (Convert(float, tmll.AMOUNT))*(IsNull(tcu.FACTOR, 0)) ");
            sbSql.Append("        ELSE 0 ");
            sbSql.Append("    END CONV_AMOUNTS ");
            sbSql.Append("    from  ");
            if (loctype == (int)LOCATION.StoringLocation)
            {
                sbSql.Append("    T_MGMT_LED_LOC tmll inner join  T_LED_REGULATION lr on tmll.REG_NO = lr.REG_NO ");
            }
            else
            {
                sbSql.Append("    T_MGMT_LED_USAGE_LOC tmll inner join  T_LED_REGULATION lr on tmll.REG_NO = lr.REG_NO ");
            }
            sbSql.Append("    inner join V_FIRE vf on lr.REG_TYPE_ID = vf.FIRE_ID ");
            sbSql.Append("    inner join M_REGULATION mr on vf.FIRE_ID = mr.REG_TYPE_ID ");
            sbSql.Append("    inner join T_MGMT_LEDGER ml on tmll.REG_NO = ml.REG_NO ");
            sbSql.Append("    inner join M_CHEM mc on ml.CHEM_CD = mc.CHEM_CD ");
            sbSql.Append("    left join #TMP_CONV_UNIT tcu on tmll.UNITSIZE_ID = tcu.UNITSIZE_ID ");
            sbSql.Append("    left join T_LEDGER_WORK tlw on ml.REG_NO = tlw.REG_NO ");
            sbSql.Append("    where  tmll.LOC_ID in(" + locId + ") ");
            if (model.REG_NO != "")
            {
                sbSql.Append("    and tmll.REG_NO <> @REG_NO ");
            }
            sbSql.Append("    and tlw.TEMP_SAVED_FLAG = 1 and tlw.APPLI_CLASS <> 1 ");
            sbSql.Append("    and vf.FIRE_NAME like '%/" + hazardoustype + "%' ");
            sbSql.Append("    and tmll.DEL_FLAG = 0 and vf.DEL_FLAG = 0 and mr.DEL_FLAG = 0 and ml.DEL_FLAG = 0 and mc.DEL_FLAG = 0 ");
            sbSql.Append("    union     ");

            sbSql.Append("    select distinct ml.REG_NO, ml.RECENT_LEDGER_FLOW_ID, tmll.LOC_ID, mr.UNITSIZE_ID REG_UNITSIZE_ID, mc.DENSITY, tcu.CONVERTED_UNITSIZE_ID ");
            sbSql.Append("    ,CASE ");
            sbSql.Append("        WHEN tcu.UNITSIZE_ID = tcu.CONVERTED_UNITSIZE_ID THEN tmll.AMOUNT ");
            sbSql.Append("        WHEN tcu.UNITSIZE_ID <> tcu.CONVERTED_UNITSIZE_ID THEN (Convert(float, tmll.AMOUNT))*(IsNull(tcu.FACTOR, 0)) ");
            sbSql.Append("        ELSE 0 ");
            sbSql.Append("    END CONV_AMOUNTS ");
            sbSql.Append("    from  ");
            if (loctype == (int)LOCATION.StoringLocation)
            {
                sbSql.Append("    T_MGMT_LED_LOC tmll inner join  T_LED_REGULATION lr on tmll.REG_NO = lr.REG_NO ");
            }
            else
            {
                sbSql.Append("    T_MGMT_LED_USAGE_LOC tmll inner join  T_LED_REGULATION lr on tmll.REG_NO = lr.REG_NO ");
            }
            sbSql.Append("    inner join V_FIRE vf on lr.REG_TYPE_ID = vf.FIRE_ID ");
            sbSql.Append("    inner join M_REGULATION mr on vf.FIRE_ID = mr.REG_TYPE_ID ");
            sbSql.Append("    inner join T_MGMT_LEDGER ml on tmll.REG_NO = ml.REG_NO ");
            sbSql.Append("    inner join M_CHEM mc on ml.CHEM_CD = mc.CHEM_CD ");
            sbSql.Append("    left join #TMP_CONV_UNIT tcu on tmll.UNITSIZE_ID = tcu.UNITSIZE_ID ");
            sbSql.Append("    where  tmll.LOC_ID in(" + locId + ") ");
            if (model.REG_NO != "")
            {
                sbSql.Append("    and tmll.REG_NO <> @REG_NO ");
            }
            sbSql.Append("    and ml.REG_NO not in (select distinct REG_NO from T_LEDGER_WORK where REG_NO is not null) ");
            sbSql.Append("    and vf.FIRE_NAME like '%/" + hazardoustype + "%' ");
            sbSql.Append("    and tmll.DEL_FLAG = 0 and vf.DEL_FLAG = 0 and mr.DEL_FLAG = 0 and ml.DEL_FLAG = 0 and mc.DEL_FLAG = 0 ");
            sbSql.Append("    union     ");

            sbSql.Append("    select distinct tlw.REG_NO, tlwl.LEDGER_FLOW_ID, tlwl.LOC_ID, mr2.UNITSIZE_ID REG_UNITSIZE_ID, mc2.DENSITY, tcu2.CONVERTED_UNITSIZE_ID ");
            sbSql.Append("    ,CASE ");
            sbSql.Append("        WHEN tcu2.UNITSIZE_ID = tcu2.CONVERTED_UNITSIZE_ID THEN tlwl.AMOUNT ");
            sbSql.Append("        WHEN tcu2.UNITSIZE_ID <> tcu2.CONVERTED_UNITSIZE_ID THEN (Convert(float, tlwl.AMOUNT))*(IsNull(tcu2.FACTOR, 0)) ");
            sbSql.Append("        ELSE 0 ");
            sbSql.Append("    END CONV_AMOUNTS2 ");
            sbSql.Append("    from  ");
            if (loctype == (int)LOCATION.StoringLocation)
            {
                sbSql.Append("    T_LED_WORK_LOC tlwl inner join  T_LEDGER_WORK_REGULATION lwr on tlwl.LEDGER_FLOW_ID = lwr.LEDGER_FLOW_ID ");
            }
            else
            {
                sbSql.Append("    T_LED_WORK_USAGE_LOC tlwl inner join  T_LEDGER_WORK_REGULATION lwr on tlwl.LEDGER_FLOW_ID = lwr.LEDGER_FLOW_ID ");
            }
            sbSql.Append("    inner join V_FIRE vf on lwr.REG_TYPE_ID = vf.FIRE_ID ");
            sbSql.Append("    inner join M_REGULATION mr2 on vf.FIRE_ID = mr2.REG_TYPE_ID ");
            sbSql.Append("    inner join T_LEDGER_WORK tlw on tlwl.LEDGER_FLOW_ID = tlw.LEDGER_FLOW_ID ");
            sbSql.Append("    inner join M_CHEM mc2 on tlw.CHEM_CD = mc2.CHEM_CD ");
            sbSql.Append("    left join #TMP_CONV_UNIT tcu2 on tlwl.UNITSIZE_ID = tcu2.UNITSIZE_ID ");
            sbSql.Append("    where  tlwl.LOC_ID in(" + locId + ") ");
            if (model.LEDGER_FLOW_ID != null)
            {
                sbSql.Append("    and tlwl.LEDGER_FLOW_ID <> @LEDGER_FLOW_ID");
            }
            sbSql.Append("    and vf.FIRE_NAME like '%/" + hazardoustype + "%' ");
            sbSql.Append("    and tlw.TEMP_SAVED_FLAG = 0 ");
            sbSql.Append("    and tlwl.DEL_FLAG = 0 and vf.DEL_FLAG = 0 and mr2.DEL_FLAG = 0 and tlw.DEL_FLAG = 0 and mc2.DEL_FLAG = 0 ");
            sbSql.Append("  ) conv ");
            sbSql.Append(")led ");
            sbSql.Append("group by led.LOC_ID ");

            cmd.CommandText = sbSql.ToString();
            cmd.Parameters.AddWithValue("UNIT_KG", unitKG);
            cmd.Parameters.AddWithValue("UNIT_L", unitL);
            if (model.REG_NO != "")
            {
                cmd.Parameters.AddWithValue("REG_NO", model.REG_NO);
            }
            if (model.LEDGER_FLOW_ID != null)
            {
                cmd.Parameters.AddWithValue("LEDGER_FLOW_ID", model.LEDGER_FLOW_ID);
            }
        }

        /// <summary>
        /// 指定数量1倍以上チェック 検索用SQL
        /// </summary>
        /// <param name="locids">場所ID</param>
        /// <paramref name="cmd">セッション情報</paramref>
        /// <returns>true：不整合なし / false：不整合あり</returns>
        private string GetSql_OverChk(string locids, ref SqlCommand cmd)
        {
            StringBuilder sbSql = new StringBuilder();
            sbSql.Append("select count(*) CNT ");
            sbSql.Append(" from");
            sbSql.Append(" M_LOCATION ");
            sbSql.Append(" where LOC_ID in(" + locids + ")");
            sbSql.Append(" and OVER_CHECK_FLAG = 1");
            sbSql.Append(" and DEL_FLAG = @DEL_FLAG");

            cmd.Parameters.AddWithValue("DEL_FLAG", (int)DEL_FLAG.Undelete);

            return sbSql.ToString();
        }

        /// <summary>
        /// 消防法届出数量チェック
        /// </summary>
        /// <param name="conn">セッション情報</param>
        /// <param name="regtypeid">法規制種類ID</param>
        /// <param name="latelyAmounts">今回申請の数量</param>
        /// <param name="arrAmounts">前行までの場所IDと容量</param>
        /// <param name="TmpHazardous">消防法を危険物種類ごとに取得したテーブル</param>
        /// <param name="loctype">区分 1:保管場所 / 2:使用場所</param>
        /// <param name="locId">場所ID</param>
        /// <param name="model">管理台帳クラス情報</param>
        /// <param name="unitKG">KGの単位ID</param>
        /// <param name="unitL">Lの単位ID</param>
        /// <paramref name="chkOver">指定数量1倍以上チェックフラグ</paramref>
        /// <returns>0：届出数量OK / 1：届出数量アラート / 2：場所が複数の消防法に登録済 / 3：届出数量の登録なし</returns>
        private int CheckHazardous(SqlConnection conn, string regtypeid, decimal latelyAmounts, ArrayList arrAmounts, DataTable TmpHazardous, int loctype, int locId,
            LedgerModel model, int unitKG, int unitL, ref bool chkOver)
        {
            int ret = 1;
            decimal ttlharzards = 0;
            decimal decFireSize = 0;
            string locs = null;
            chkOver = true;

            // 消防法に届け出ているレポートを特定
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = conn;
            StringBuilder sqlReportIds = new StringBuilder();
            sqlReportIds.Append("select REPORT_ID from M_FIRE_REPORT ");
            sqlReportIds.Append(" where REPORT_ID in( select REPORT_ID from M_FIRE_LOCATION where CLASS_ID = @CLASS_ID and LOC_ID = @LOC_ID) ");
            sqlReportIds.Append(" and DEL_FLAG = @DEL_FLAG");
            cmd.CommandText = sqlReportIds.ToString();
            cmd.Parameters.AddWithValue("CLASS_ID", loctype);
            cmd.Parameters.AddWithValue("LOC_ID", locId);
            cmd.Parameters.AddWithValue("DEL_FLAG", (int)DEL_FLAG.Undelete);
            cmd.ExecuteNonQuery();
            SqlDataAdapter adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            DataTable dtReports = new DataTable();
            adp.Fill(dtReports);
            if (dtReports.Rows.Count == 0)
            {
                ret = 3;
                return ret;
            }
            if (dtReports.Rows.Count > 1)
            {
                ret = 2;
                return ret;
            }

            // レポートに紐づく場所を取得
            cmd = new SqlCommand();
            cmd.Connection = conn;
            StringBuilder sqlLocs = new StringBuilder();
            sqlLocs.Append("select mfl.LOC_ID ");
            sqlLocs.Append("from M_FIRE_REPORT mfr ");
            sqlLocs.Append("left join M_FIRE_LOCATION mfl on mfr.REPORT_ID = mfl.REPORT_ID ");
            sqlLocs.Append("where mfl.CLASS_ID = @CLASS_ID and mfr.REPORT_ID = @REPORT_ID and mfr.DEL_FLAG = @DEL_FLAG");
            cmd.CommandText = sqlLocs.ToString();
            cmd.Parameters.AddWithValue("CLASS_ID", loctype);
            cmd.Parameters.AddWithValue("REPORT_ID", dtReports.Rows[0]["REPORT_ID"].ToString());
            cmd.Parameters.AddWithValue("DEL_FLAG", (int)DEL_FLAG.Undelete);
            cmd.ExecuteNonQuery();
            adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            DataTable dtLocs = new DataTable();
            adp.Fill(dtLocs);

            ArrayList al = new ArrayList();
            foreach (DataRow record2 in dtLocs.Rows)
            {
                if (locs != null)
                {
                    locs += ",";
                }
                locs += record2["LOC_ID"].ToString();
                al.Add(record2["LOC_ID"].ToString());
            }

            // 前行も同じレポートに含まれる場所であれば加算
            foreach (DictionaryEntry am in arrAmounts)
            {
                if (al.Contains(am.Key.ToString()))
                {
                    latelyAmounts += Convert.ToDecimal(am.Value);
                }
            }

            foreach (DataRow th in TmpHazardous.Rows)
            {
                bool flgLately = false;
                cmd = new SqlCommand();
                adp = new SqlDataAdapter();
                decimal decAmount = 0;
                decimal ttlAmount = 0;

                string hazardoustype = th["reg_text2"].ToString();

                cmd.Connection = conn;
                cmd.CommandText = "select FIRE_ID from V_FIRE where FIRE_ID = @FIRE_ID and FIRE_NAME like '%/" + hazardoustype + "%' and DEL_FLAG =@DEL_FLAG";
                cmd.Parameters.AddWithValue("FIRE_ID", regtypeid);
                cmd.Parameters.AddWithValue("DEL_FLAG", (int)DEL_FLAG.Undelete);
                cmd.ExecuteNonQuery();
                adp.SelectCommand = cmd;
                DataTable htyperecord = new DataTable();
                adp.Fill(htyperecord);
                if (htyperecord.Rows.Count == 1)
                {
                    flgLately = true;
                }

                // フロー中数量、申請済数量の合計を取得
                cmd = new SqlCommand();
                cmd.Connection = conn;
                Sql_GetAmounts(loctype, locs, model, hazardoustype, unitKG, unitL, ref cmd);
                cmd.ExecuteNonQuery();
                adp = new SqlDataAdapter();
                adp.SelectCommand = cmd;
                DataTable records = new DataTable();
                adp.Fill(records);

                // 数量合計
                foreach (DataRow record in records.Rows)
                {
                    decAmount += Convert.ToDecimal(record["AMOUNTS"]);
                }

                if (flgLately)
                {
                    ttlAmount = decAmount + latelyAmounts;
                }
                else
                {
                    ttlAmount = decAmount;
                }

                ttlharzards += (ttlAmount / Convert.ToDecimal(th["FIRE_SIZE"].ToString()));

                if (flgLately)
                {
                    // 届出数量チェック
                    cmd = new SqlCommand();
                    cmd.Connection = conn;
                    cmd.CommandText = GetSql_FireFlow();
                    cmd.Parameters.AddWithValue("DEL_FLAG", (int)DEL_FLAG.Undelete);
                    cmd.Parameters.AddWithValue("CLASS_ID", loctype);
                    cmd.Parameters.AddWithValue("REG_TYPE_ID", th["FIRE_ID"].ToString());
                    cmd.Parameters.AddWithValue("LOC_ID", locId);
                    cmd.ExecuteNonQuery();
                    adp = new SqlDataAdapter();
                    adp.SelectCommand = cmd;
                    DataTable records2 = new DataTable();
                    adp.Fill(records2);

                    if (records2.Rows.Count == 0)
                    {
                        ret = 3;
                        return ret;
                    }
                    else
                    {
                        decFireSize = Convert.ToDecimal(records2.Rows[0]["FIRE_SIZE"].ToString());

                        // 届出数量 - (届出数量 * (閾値/100)) - 数量合計チェック
                        if (decFireSize - (decFireSize * (Convert.ToDecimal(WebConfigUtil.FireCheckThreshold) / 100)) - (ttlAmount) > 0)
                        {
                            ret = 0;
                        }
                    }
                }
            }

            // 指定数量1倍以上チェック
            cmd = new SqlCommand();
            cmd.Connection = conn;
            cmd.CommandText = GetSql_OverChk(locs, ref cmd);
            cmd.ExecuteNonQuery();
            adp = new SqlDataAdapter();
            adp.SelectCommand = cmd;
            DataTable dtOverchk = new DataTable();
            adp.Fill(dtOverchk);

            int cntOverchk = Convert.ToInt32(dtOverchk.Rows[0]["CNT"]);

            if (cntOverchk != 0)
            {
                if (ttlharzards / cntOverchk > 1)
                {
                    chkOver = false;
                }
            }

            return ret;
        }

        /// <summary>
        /// 消防法 届出数量合計の検索用SQL
        /// </summary>
        /// <returns>検索用SQL</returns>
        private string GetSql_FireFlow()
        {
            StringBuilder sbSql = new StringBuilder();

            sbSql = new StringBuilder();
            sbSql.Append("select ");
            sbSql.Append(" ISNULL(ms.FIRE_SIZE, 0) FIRE_SIZE ");
            sbSql.Append(" from");
            sbSql.Append(" M_FIRE_SIZE ms");
            sbSql.Append(" inner");
            sbSql.Append(" join M_FIRE_LOCATION ml on ms.REPORT_ID = ml.REPORT_ID");
            sbSql.Append(" inner");
            sbSql.Append(" join M_FIRE_REPORT mr on ms.REPORT_ID = mr.REPORT_ID");
            sbSql.Append(" where ");
            sbSql.Append(" mr.DEL_FLAG = @DEL_FLAG");
            sbSql.Append(" and ml.CLASS_ID = @CLASS_ID");
            sbSql.Append(" and ms.REG_TYPE_ID= @REG_TYPE_ID");
            sbSql.Append(" and ml.LOC_ID =@LOC_ID");

            return sbSql.ToString();
        }


        /// <summary>
        /// <para>表示ボタンの処理です。</para>
        /// </summary>
        public string Display(string Id)
        {
            string[] ids = Id.Split(',');
            var flowId = new List<string>();
            var modes = new List<Mode>();
            var condition = new LedgerSearchModel();
            string path = null;
            try
            {
                foreach (var items in ids)
                {
                    Id = items;
                    condition.LEDGER_FLOW_ID = Convert.ToInt32(Id);
                    LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
                    var d = new LedgerWorkDataInfo();
                    objLedgerCommonInfo = d.GetSearchResult(condition).Single();

                    var id = objLedgerCommonInfo.LEDGER_FLOW_ID;
                    var appli = (AppliClass)(objLedgerCommonInfo.APPLI_CLASS);
                    var result = objLedgerCommonInfo.BEFORE_WORKFLOW_RESULT;
                    var HIERARCHY = objLedgerCommonInfo.HIERARCHY;
                    if (appli == AppliClass.New)
                    {
                        if (result.ToString() == "")
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.New);
                        }
                        else if ((result.ToString() == ((int)WorkflowResult.Application).ToString() ||
                            result.ToString() == ((int)WorkflowResult.Approval).ToString()) && HIERARCHY != -2)
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Approval);
                        }
                        else if ((result.ToString() == ((int)WorkflowResult.Application).ToString() ||
                            result.ToString() == ((int)WorkflowResult.Approval).ToString()) && HIERARCHY == -2)
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.New);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Remand).ToString())
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.New);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Regained).ToString() && HIERARCHY == -2)
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.New);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Regained).ToString() && HIERARCHY != -2)
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Approval);
                        }
                    }
                    else if (appli == AppliClass.Edit)
                    {
                        if (result.ToString() == "")
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Edit);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Application).ToString() ||
                            result.ToString() == ((int)WorkflowResult.Approval).ToString())
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Approval);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Remand).ToString())
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Edit);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Regained).ToString() && HIERARCHY == -2)
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Edit);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Regained).ToString() && HIERARCHY != -2)
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Approval);
                        }
                    }
                    else if (appli == AppliClass.Stopped)
                    {
                        if (result.ToString() == "")
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Abolition);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Application).ToString() ||
                            result.ToString() == ((int)WorkflowResult.Approval).ToString())
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Approval);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Remand).ToString())
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Abolition);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Regained).ToString() && HIERARCHY == -2)
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Abolition);
                        }
                        else if (result.ToString() == ((int)WorkflowResult.Regained).ToString() && HIERARCHY != -2)
                        {
                            flowId.Add(id.ToString());
                            modes.Add(Mode.Approval);
                        }
                    }
                }

                path = string.Format("{0}?{1}={2}&BackUrl={3}&{4}={5}",
                    NikonFormPath.LedgerMaintenance,
                    QueryString.ModeKey, ((int)modes[0]).ToString(),
                    null,
                    QueryString.IdKey, flowId[0]);
                List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                for (int i = 0; i < flowId.Count; i++)
                {
                    path += string.Format("&flowids={0}&flowmodes={1}", flowId[i], ((int)modes[i]).ToString());
                    FlowIds.Add(new Tuple<string, string>(flowId[i], ((int)modes[i]).ToString()));
                }

                if (flowId.Count > 1)
                {
                    Session["BackUrlIds"] = FlowIds;
                }

            }
            catch (Exception ex)
            {
                _logoututil.OutErrLog(programId, ex.ToString());
            }
            return path;
        }


        public ActionResult ChemApproval(HttpPostedFileBase pdffile, HttpPostedFileBase UNREGISTERED_CHEMfile)
        {
            LedgerCommonInfo objLedgerCommonInfo = new LedgerCommonInfo();
            var ledger = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
            if (pdffile != null && pdffile.ContentLength > 0)
            {
                MemoryStream target = new MemoryStream();
                pdffile.InputStream.CopyTo(target);
                ledger.SDS = target.ToArray();
                ledger.SDS_MIMETYPE = MimeMapping.GetMimeMapping(pdffile.ContentType);
                ledger.SDS_FILENAME = pdffile.FileName;
            }

            if (UNREGISTERED_CHEMfile != null && UNREGISTERED_CHEMfile.ContentLength > 0)
            {
                MemoryStream target = new MemoryStream();
                UNREGISTERED_CHEMfile.InputStream.CopyTo(target);
                ledger.UNREGISTERED_CHEM = target.ToArray();
                ledger.UNREGISTERED_CHEM_MIMETYPE = MimeMapping.GetMimeMapping(UNREGISTERED_CHEMfile.ContentType);
                ledger.UNREGISTERED_CHEM_FILENAME = UNREGISTERED_CHEMfile.FileName;
            }
            Session.Add(nameof(LedgerCommonInfo), ledger);
            List<string> WarningMessage = DoUpdateForProcessApproval(ledger);
            if (WarningMessage.Count > 0)
            {
                foreach (var Errormsg in WarningMessage)
                {
                    WarningMessage.Add(Errormsg + "\n");
                }
                TempData["WarningMessage"] = WarningMessage;
                List<LedgerLocationModel> ledgerlocationmodel = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                objLedgerCommonInfo.StorageLocation = ledgerlocationmodel;
                List<LedgerRegulationModel> objRegulation = (List<LedgerRegulationModel>)Session[sessionRegulations];
                objLedgerCommonInfo.objLedgerRegulationModel = objRegulation;
                var usageLocations = (List<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                objLedgerCommonInfo.objLedgerUsageLocationModel = usageLocations;
                List<T_LED_WORK_HISTORY> workflow = (List<T_LED_WORK_HISTORY>)Session["Workflow"];
                objLedgerCommonInfo.objT_LED_WORK_HISTORY = workflow;
                objLedgerCommonInfo = (LedgerCommonInfo)Session[nameof(LedgerCommonInfo)];
                return View(objLedgerCommonInfo);
            }
            else
            {
                //if (objLedgerCommonInfo.flowid != null)
                //{
                if (Session["BackUrlIds"] != null)
                {
                    List<Tuple<string, string>> FlowIds = new List<Tuple<string, string>>();
                    string ids = null;
                    string modes = null;
                    FlowIds = (List<Tuple<string, string>>)Session["BackUrlIds"];

                    foreach (var element in FlowIds)
                    {
                        if (ledger.LEDGER_FLOW_ID != Convert.ToInt32(element.Item1))
                        {
                            ids = element.Item1;
                            modes = element.Item2;
                            FlowIds.RemoveAll(item => item.Item1 == ledger.LEDGER_FLOW_ID.ToString());
                            break;
                        }
                    }
                    int? flowids = Convert.ToInt32(ids);
                    int flowmodes = Convert.ToInt32(modes);
                    Mode intmode = (Mode)Convert.ToInt32(modes);
                    int Id = Convert.ToInt32(ids);
                    string operation = string.Empty;
                    Session["BackUrlIds"] = FlowIds;
                    objIMangementLedgerRepo.ClearSession();

                    LedgerCommonInfo result = GetMethod(Id, intmode, flowids, flowmodes, operation);
                    return View(result);
                }
                else
                {
                    return RedirectToAction("ZC14030", "ZC140");
                }
                //}
            }
        }

        /// <summary>
        /// 承認モード時の更新処理を行います。
        /// </summary>
        public List<string> DoUpdateForProcessApproval(LedgerCommonInfo ledger)
        {
            List<string> WarningMessage = new List<string>();
            List<string> ErrorMessages = new List<string>();
            objIMangementLedgerRepo.getInputData(ledger);

            var context = DbUtil.Open(true);
            try
            {
                using (var ledgerWorkDataInfo = new LedgerWorkDataInfo() { Context = context })
                {
                    if (!ledgerWorkDataInfo.IsInfoSyncFromDB(ledger))
                    {
                        WarningMessage.Add(WarningMessages.AllreadyOperated);
                        return WarningMessage;
                    }
                    UserInformation loginModel = new UserInformation();
                    loginModel = (UserInformation)Session["LoginUserSession"];
                    ledger.REG_USER_CD = loginModel.User_CD;

                    using (var hist = new LedgerWorkHistoryDataInfo())
                    {
                        if (!hist.CanApproval(ledger.LEDGER_FLOW_ID, loginModel.User_CD))
                        {
                            WarningMessage.Add(WarningMessages.AllreadyOperated);
                            return WarningMessage;
                        }
                    }

                    var oldRegisterNumbers = (List<LedgerOldRegisterNumberModel>)Session["sessionOldRegisterNumbers"];
                    if (oldRegisterNumbers == null)
                    {
                        oldRegisterNumbers = new List<LedgerOldRegisterNumberModel>();
                    }
                    var regulations = (List<LedgerRegulationModel>)Session[sessionRegulations];
                    if (regulations == null)
                    {
                        regulations = new List<LedgerRegulationModel>();
                    }
                    var storingLocations = (List<LedgerLocationModel>)Session["StoringLocationDetails"];
                    if (storingLocations == null)
                    {
                        storingLocations = new List<LedgerLocationModel>();
                    }
                    var usageLocationLists = (IEnumerable<LedgerUsageLocationModel>)Session[sessionUsageLocations];
                    if (usageLocationLists == null)
                    {
                        usageLocationLists = new List<LedgerUsageLocationModel>();
                    }


                    foreach (LedgerUsageLocationModel list in usageLocationLists)
                    {
                        foreach (var listColect in list.ACCESMENT_REG_COLLECTION)
                        {
                            listColect.REG_USER_CD = ledger.REG_USER_CD;
                        }
                        foreach (var listColect in list.RISK_REDUCE_COLLECTION)
                        {
                            listColect.REG_USER_CD = ledger.REG_USER_CD;
                        }
                    }
                    Session.Add(sessionUsageLocations, usageLocationLists.AsEnumerable());
                    var usageLocations = (IEnumerable<LedgerUsageLocationModel>)Session[sessionUsageLocations];

                    if (ledger.APPLI_CLASS == (int)AppliClass.Stopped)
                    {
                        foreach (var usageLocation in usageLocations)
                        {
                            //Phase3追加項目には値が入っていないため、ダミーで-1を入れる
                            if (usageLocation.REG_TRANSACTION_VOLUME == null)
                            {
                                usageLocation.REG_TRANSACTION_VOLUME = -1;
                            }
                            if (usageLocation.REG_WORK_FREQUENCY == null)
                            {
                                usageLocation.REG_WORK_FREQUENCY = -1;
                            }
                            if (usageLocation.REG_DISASTER_POSSIBILITY == null)
                            {
                                usageLocation.REG_DISASTER_POSSIBILITY = -1;
                            }
                        }
                    }

                    ledger.HIERARCHY = ledger.HIERARCHY != null ? ledger.HIERARCHY + 1 : 0;
                    if (ledger.FLOW_HIERARCHY.ToInt() > ledger.HIERARCHY)
                    {
                        ledgerWorkDataInfo.SaveWithChildren(ledger,
                            oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations.ToList(),
                            storingLocations.Where(x => x.LOC_ID != null).ToList(), usageLocations);

                        if (!ledgerWorkDataInfo.IsValid)
                        {
                            var data = ledgerWorkDataInfo.ErrorMessages;
                            WarningMessage = new List<string>(data);
                            return WarningMessage;
                        }
                    }
                    else
                    {
                        ledger.RECENT_LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID;
                        using (var dataInfo = new LedgerDataInfo() { Context = context })
                        {
                            if (ledger.APPLI_CLASS == (int)AppliClass.New)
                            {
                                dataInfo.AddWithChildren(ledger,
                                    oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                    storingLocations.Where(x => x.LOC_ID != null), usageLocations);
                            }
                            else if (ledger.APPLI_CLASS == (int)AppliClass.Stopped)
                            {
                                dataInfo.RemoveWithChildren(ledger,
                                    oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                    storingLocations.Where(x => x.LOC_ID != null), usageLocations);
                            }
                            else
                            {
                                dataInfo.SaveWithChildren(ledger,
                                     oldRegisterNumbers.Where(x => !string.IsNullOrEmpty(x.REG_OLD_NO)), regulations,
                                     storingLocations.Where(x => x.LOC_ID != null), usageLocations);
                            }
                            if (!dataInfo.IsValid)
                            {
                                var data = dataInfo.ErrorMessages;
                                WarningMessage = new List<string>(data);
                                return WarningMessage;
                            }
                        }

                        ledgerWorkDataInfo.RemoveWithChildren((T_LEDGER_WORK)ledger, true);
                        if (!ledgerWorkDataInfo.IsValid)
                        {
                            var data = ledgerWorkDataInfo.ErrorMessages;
                            WarningMessage = new List<string>(data);
                            return WarningMessage;
                        }
                    }

                    using (var workHistoryDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
                    {
                        workHistoryDataInfo.Approval(ledger.LEDGER_FLOW_ID, loginModel.User_CD);
                        if (!workHistoryDataInfo.IsValid)
                        {
                            var data = workHistoryDataInfo.ErrorMessages;
                            WarningMessage = new List<string>(data);
                            return WarningMessage;
                        }
                    }

                    if (!IsLastApprovalValid(ledger, context))
                    {
                        WarningMessage.Add(WarningMessages.ErrorToApproveLedger);
                        return WarningMessage;
                    }
                }
                context.Commit();
                var mail = new WorkflowMail();
                mail.SendMail(ledger);
                ledger.Mode = Mode.ReferenceAfterRegistration;
                ClearSession();
                afterUpdateType = UpdateType.Approval;
                Session.Add(SessionConst.IsMaintenaceFormBack, true);
                return WarningMessage;

            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
                context.Rollback();
                throw;
            }
            finally
            {
                context.Close();
            }
        }

        /// <summary>
        /// ワークフローが最終承認された時のデータに不整合がないか確認します。
        /// </summary>
        /// <returns>true：不整合なし / false:不整合あり</returns>
        private bool IsLastApprovalValid(LedgerModel ledger, DbContext context)
        {
            List<int> cnt = new List<int>();
            var condition = new LedgerSearchModel() { LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID, RECENT_LEDGER_FLOW_ID = ledger.LEDGER_FLOW_ID };

            using (var workHistoryDataInfo = new LedgerWorkHistoryDataInfo() { Context = context })
            {
                var afterRemoved = workHistoryDataInfo.GetSearchResult(new LedgerWorkHistorySearchModel() { LEDGER_FLOW_ID = condition.LEDGER_FLOW_ID });
                if (afterRemoved.Count(x => x.DEL_FLAG == 0) == 0)
                {
                    using (var WorkDataInfo = new LedgerWorkDataInfo() { Context = context })
                    {
                        cnt = WorkDataInfo.GetSearchResultCount(condition);
                        if (cnt.Count != 0) return false;
                    }

                    using (var historyDataInfo = new LedgerHistoryDataInfo() { Context = context })
                    {
                        cnt = historyDataInfo.GetSearchResultCount(condition);
                        if (cnt.Count == 0) return false;
                    }

                    using (var dataInfo = new LedgerDataInfo() { Context = context })
                    {
                        if (ledger.APPLI_CLASS == (int)AppliClass.Stopped) condition.LEDGER_STATUS = (int)LedgerStatusConditionType.Abolished;
                        cnt = dataInfo.GetSearchResultCount(condition);
                        if (cnt.Count == 0) return false;
                    }
                }
            }
            return true;
        }


        /// <summary>
        /// 更新後の更新種類を取得、格納します。
        /// </summary>
        private UpdateType afterUpdateType
        {
            get
            {
                if (Session[sessionAfterUpdateType] == null)
                {
                    return default(UpdateType);
                }
                return (UpdateType)Session[sessionAfterUpdateType];
            }
            set
            {
                Session.Add(sessionAfterUpdateType, value);
            }
        }

        /// <summary>
        /// 部署マスターからデータを取得します。
        /// </summary>
        /// <returns>部署マスターデータを取得します。</returns>
        public virtual IEnumerable<CommonDataModel<string>> GetGroupList() => new GroupMasterInfo().GetSelectList();

        public string GetGroupNameValues(string GroupCd)
        {
            string ErrorMessage = null;
            try
            {
                var group = GetGroupList().Single(x => x.Id == GroupCd);
                var groupMaster = new GroupMasterInfo();
                UserInformation usr = (UserInformation)Session["LoginUserSession"];

                //var usr = (UserInfo.clsUser)Session[Const.cSESSION_NAME_USER_INFO];
                var grp = groupMaster.GetChildGroups(usr.GROUP_CD, true);
                var parent = groupMaster.GetParentGroups(usr.GROUP_CD, false, 1);
                grp = grp.Concat(parent);
                if (grp.Count(n => n == group.Id.ToString()) == 0)
                {
                    ErrorMessage = WarningMessages.GroupSelect + "|" + "false";
                    return ErrorMessage;
                }
                ErrorMessage = string.Join(SystemConst.SelectionsDelimiter, group.Name);
            }
            catch (Exception exe)
            {
                _logoututil.OutErrLog(programId, exe.ToString());
            }
            return ErrorMessage + "|" + "true";
        }


        public LedgerCommonInfo ddlLedgerList(LedgerCommonInfo dropdownlist)
        {
            try
            {
                dropdownlist.LookupChemNameCondition = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.CHEMNAMECONDITION];
                dropdownlist.LookupProductName = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.PRODUCTNAME];
                dropdownlist.LookupCas = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.CAS];
                dropdownlist.LookupRegistrationNo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.REGISTRATIONNUMBER];
                dropdownlist.LookupState = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.STATE];
                dropdownlist.LookupReason = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.REASON];
                dropdownlist.LookupREG_TRANSACTION_VOLUME = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.TRANSACTIONVOLUME];
                dropdownlist.LookupREG_WORK_FREQUENCY = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.WORKFREQUENCY];
                dropdownlist.LookupREG_DISASTER_POSSIBILITY = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.DISASTERPOSSIBILITY];
                dropdownlist.LookupSUB_CONTAINER = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.CONTAINER];
                dropdownlist.LookupManufacturerName = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.MANUFACTURENAME];
                dropdownlist.LookupUnitSizeMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.UNITSIZEMASTERINFO];
                dropdownlist.LookupUsageMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.USAGEMASTERINFO];
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return dropdownlist;
        }

        public LedgerUsageLocationModel ddlUsageLocList(LedgerUsageLocationModel dropdownlist)
        {
            try
            {
                dropdownlist.LookupREG_TRANSACTION_VOLUME = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.TRANSACTIONVOLUME];
                dropdownlist.LookupREG_WORK_FREQUENCY = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.WORKFREQUENCY];
                dropdownlist.LookupREG_DISASTER_POSSIBILITY = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.DISASTERPOSSIBILITY];
                dropdownlist.LookupSUB_CONTAINER = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.CONTAINER];
                dropdownlist.LookupUnitSizeMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.UNITSIZEMASTERINFO];
                dropdownlist.LookupUsageMasterInfo = (IEnumerable<SelectListItem>)HttpContext.Application[Common.LookupConstants.USAGEMASTERINFO];
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }
            return dropdownlist;
        }
    }
}