﻿
using ChemMgmt.Classes;
using ChemMgmt.Classes.util;
using ChemMgmt.Models;
using ChemMgmt.Nikon;
using ChemMgmt.Nikon.Chem;
using ChemMgmt.Nikon.Ledger;
using ChemMgmt.Nikon.Models;
using COE.Common;
using Dapper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ZyCoaG;
using ZyCoaG.BaseCommon;
using ZyCoaG.Classes.util;
using ZyCoaG.Extensions;
using ZyCoaG.Nikon;
using ZyCoaG.Nikon.Ledger;
using ZyCoaG.util;
using ZyCoaG.Util;

namespace ChemMgmt.Controllers
{
    [Authorize]
    [HandleError]
    public class ZC040Controller : Controller
    {
        ChemMgmt.Classes.util.LogUtil logutil = new ChemMgmt.Classes.util.LogUtil();
        private IDictionary<int?, string> actionTypeDic { get; set; }
        private const string PROGRAM_ID = "ZC04001";
        private UserInfo.ClsUser _clsUser = new UserInfo.ClsUser();
        LogUtil LogUtil = new LogUtil();

        //重量管理対象フラグを取得
        private string[] strWeightFlag = WebConfigUtil.ManagementWeight.Split(':');

        public IEnumerable<CommonDataModel<int?>> GetUnitSizeList() => new UnitSizeMasterInfo().GetSelectList();

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to load the Loading and unloading page
        /// <summary>
        public ActionResult ZC04001(string Barcode, string ProductName, string FlowCode)
        {
            InventoryManagement inventory = new InventoryManagement();
            try
            {
                if (!string.IsNullOrEmpty(Barcode) && !string.IsNullOrEmpty(ProductName))
                {
                    inventory = SetStockData(Barcode, inventory);
                    inventory.vGhscat = GetGhsCategory(inventory.lblCatCd);
                    inventory.vFire = GetFire(inventory.lblCatCd);
                    inventory.vRegulation = GetRegulation(inventory.lblCatCd, inventory);
                    inventory.vOffice = GetOffice(inventory.lblCatCd);
                }
                inventory = DropDownFill(inventory);
                inventory.FlowCode = FlowCode;
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View(inventory);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to post the Loading and unloading page
        /// <summary>
        [HttpPost]
        public ActionResult ZC04001(InventoryManagement inventory)
        {
            InventoryManagement inventoryMgmt = new InventoryManagement();
            string HdnAllWeigtht = null;
            //string RequestType = null;
            string WorkerValue = null;
            string WorkerComment = null;
            string ExposureWorkUse = null;
            string ExposureWorkType = null;
            string UseTemperature = null;
            string WorkCommentTargetWorkerIds = null;
            string StoringLocation = null;
            string UsageLocation = null;

            //RequestType = Request.Form["ActionType"];
            WorkerValue = Request.Form["WorkerIds"];
            WorkerComment = Request.Form["WorkCommentText"];
            WorkCommentTargetWorkerIds = Request.Form["WorkCommentTargetWorkerIds"];
            HdnAllWeigtht = Request.Form["hdnTotalWeight"];
            ExposureWorkUse = Request.Form["ExposureWorkUse"];
            ExposureWorkType = Request.Form["ExposureWorkType"];
            UseTemperature = Request.Form["UseTemperature"];
            StoringLocation = Request.Form["LocationSelection"];
            UsageLocation = Request.Form["UsageLocationSelection"];
            ///Set hidden request values in modal
            inventory.txtAllWeight = HdnAllWeigtht;
            inventory.WorkComment = WorkerComment;
            inventory.WorkCommentTargetWorkerIds = WorkCommentTargetWorkerIds;
            inventory.WorkerIds = WorkerValue;
            inventory.ExposureWorkUse = ExposureWorkUse;
            inventory.ExposureWorkType = ExposureWorkType;
            inventory.UseTemperature = UseTemperature;
            inventory.StoringLocation = StoringLocation;
            inventory.LastUsageLocId = UsageLocation;

            try
            {
                if (Request.Form["btnGoods_Issue"] != null)
                {
                    inventory.txtAllWeight = HdnAllWeigtht;
                    bool status = Lend(inventory);
                    if (status)
                    {
                        TempData["SuccessMessage"] = string.Format("瓶番号：[{0}]を出庫しました。", inventory.lblBarcode);
                    }                   

                }
                else if (Request.Form["btnFirst_Add"] != null)
                {
                    inventory.txtAllWeight = HdnAllWeigtht;
                    bool status = FirstAdd(inventory);
                    if (status)
                    {
                        TempData["SuccessMessage"] = string.Format("瓶番号：[{0}]を入庫しました。", inventory.lblBarcode);
                    }                   

                }
                else if (Request.Form["btnMove"] != null)
                {
                    inventory.txtAllWeight = HdnAllWeigtht;
                    bool status = Move(inventory);
                    if (status)
                    {
                        TempData["SuccessMessage"] = string.Format(" 瓶番号：[{0}]を保管場所移動しました。", inventory.lblBarcode);
                    }                  
                }
                else if (Request.Form["btnFull"] != null)
                {
                    inventory.txtAllWeight = HdnAllWeigtht;
                    bool status = Full(inventory);
                    if (status)
                    {
                        TempData["SuccessMessage"] = string.Format(" 瓶番号：[{0}]を使切しました。", inventory.lblBarcode);
                    }                 
                }
                else if (Request.Form["btnDelete"] != null)
                {
                    inventory.txtAllWeight = HdnAllWeigtht;
                    bool status = Delete(inventory);
                    if (status)
                    {
                        TempData["SuccessMessage"] = string.Format("瓶番号：[{0}]を廃棄しました。", inventory.lblBarcode);
                    }                  
                }
                else if (Request.Form["btnRepay"] != null)
                {
                    inventory.txtAllWeight = HdnAllWeigtht;
                    bool status = Repay(inventory);
                    if (status)
                    {
                        TempData["SuccessMessage"] = string.Format(" 瓶番号： [{0}]を入庫しました。", inventory.lblBarcode);
                    }               
                }
                else if (Request.Form["ClearValues"] != null)
                {
                    if (!string.IsNullOrEmpty(inventory.FlowCode))
                    {
                        return RedirectToAction("ZC04001", "ZC040", new { FlowCode = inventory.FlowCode });
                    }
                    else
                    {
                        return RedirectToAction("ZC04001", "ZC040");
                    }
                }
                inventoryMgmt = new InventoryManagement();
                inventoryMgmt = DropDownFill(inventoryMgmt);
            }
            catch (Exception ex)
            {
                
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View(inventoryMgmt);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to load the Loading and unloading page details using barcode
        /// <summary>
        [HttpPost]
        public ActionResult BarcodeEnterPartial(InventoryManagement inventory)
        {
            try
            {
                if (Request.Form["btnClear"] != null)
                {
                    return RedirectToAction("ZC04001", "ZC040");
                }
                inventory = SetStockData(inventory.BARCODE, inventory);
                inventory.vGhscat = GetGhsCategory(inventory.lblCatCd);
                inventory.vFire = GetFire(inventory.lblCatCd);
                inventory.vRegulation = GetRegulation(inventory.lblCatCd, inventory);
                inventory.vOffice = GetOffice(inventory.lblCatCd);

                if (inventory.vRegulation != null)
                {
                    var isWorkCommentInput = inventory.vRegulation.Any(x => x.WORKTIME_MGMT == (int)WorkingTimesManagement.Target);
                    inventory.IsWorkCommentInput = isWorkCommentInput.ToString();
                    var statudId = Convert.ToInt32(inventory.hdnStatusId);
                    if (isWorkCommentInput && (statudId == (int)Const.cSTOCK_STATUS_ID.BORROW || statudId == (int)Const.cSTOCK_STATUS_ID.EMPTY))
                        inventory.IsExposureWork = inventory.vRegulation.Any(x => x.EXPOSURE_REPORT == (int)ExposureWorkReport.Target).ToString();

                }
                inventory = DropDownFill(inventory);
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return View("ZC04001", inventory);
        }



        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to load all inventory dropdown valuess
        /// <summary>
        public InventoryManagement DropDownFill(InventoryManagement inventoryManagement)
        {
            try
            {
                // LookUpIntended Values
                List<SelectListItem> listIntended = new List<SelectListItem> { };
                new IshaUsageMasterInfo().GetSelectList().ToList().
                   ForEach(x => listIntended.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventoryManagement.LookUpIntended = listIntended;

                // ExposureWorkUse Values
                List<SelectListItem> listExposureWorkUse = new List<SelectListItem> { };
                new ExposureWorkDataInfo(ExposureClassId.WorkUse).GetSelectList().ToList().
                   ForEach(x => listExposureWorkUse.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventoryManagement.LookUpExposureWorkUse = listExposureWorkUse;

                // ExposureWorkType Values
                List<SelectListItem> listExposureWorkType = new List<SelectListItem> { };
                new ExposureWorkDataInfo(ExposureClassId.WorkType).GetSelectList().ToList().
                   ForEach(x => listExposureWorkType.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventoryManagement.LookUpExposureWorkType = listExposureWorkType;

                // UseTemperature Values
                List<SelectListItem> listUseTemperature = new List<SelectListItem> { };
                new ExposureWorkDataInfo(ExposureClassId.UseTemperature).GetSelectList().ToList().
                   ForEach(x => listUseTemperature.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventoryManagement.LookUpUseTemperature = listUseTemperature;

                //UnitSixe Values
                List<SelectListItem> listAmountUnitSize = new List<SelectListItem> { };
                new UnitSizeMasterInfo().GetSelectList().ToList().
                   ForEach(x => listAmountUnitSize.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventoryManagement.LookUpAmountUnitSize = listAmountUnitSize;

                // AcceptCondition Values
                List<SelectListItem> listAcceptCondition = new List<SelectListItem> { };
                new UnitSizeMasterInfo().GetSelectList().ToList().
                   ForEach(x => listAcceptCondition.Add(new SelectListItem
                   { Value = x.Id.ToString(), Text = x.Name }));
                inventoryManagement.LookUpAcceptCondition = listAcceptCondition;

            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return inventoryManagement;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to load barcode details and assigning in to respective models
        /// <summary>
        private InventoryManagement SetStockData(string strBarcode, InventoryManagement inventorymanagement)
        {
            try
            {
                DynamicParameters _params = new DynamicParameters();
                string USER_CD = Common.GetLogonForm().Id;
                _params.Add("@BARCODE", strBarcode.ToUpper());
                _params.Add("@USER_CD", USER_CD);

                if (!Common.isAcceptedBeforePhase2(strBarcode.ToUpper(), StockSearchTarget.barcode))
                {
                    inventorymanagement.listLedgerDetails = DbUtil.Select<ViewInventoryDetails>(SQL.SELECT_T_LEDGER, _params);
                    if (inventorymanagement.listLedgerDetails.Count == 0)
                    {
                        TempData["ValidationMessage"] = string.Format(WarningMessages.NotFound, "管理台帳");
                        inventorymanagement.hdnStatusId = "9999";
                        inventorymanagement.hdnExecuteType = "0";
                        inventorymanagement.vGhscat = GetGhsCategory(inventorymanagement.listLedgerDetails[0].CAT_CD);
                        inventorymanagement.vFire = GetFire(inventorymanagement.listLedgerDetails[0].CAT_CD);
                        inventorymanagement.vRegulation = GetRegulation(inventorymanagement.listLedgerDetails[0].CAT_CD, inventorymanagement);
                        inventorymanagement.vOffice = GetOffice(inventorymanagement.listLedgerDetails[0].CAT_CD);
                        return inventorymanagement;

                    }

                    inventorymanagement.listLedgerManagementDetails = DbUtil.Select<ViewInventoryDetails>(SQL.SELECT_LEDGER_REG_GROUP, _params);
                    if (inventorymanagement.listLedgerManagementDetails.Count == 0)
                    {
                        TempData["ValidationMessage"] = string.Format(WarningMessages.NotRegistGroup, "在庫利用");
                        inventorymanagement.hdnStatusId = "9999";
                        inventorymanagement.vGhscat = GetGhsCategory(inventorymanagement.viewInventory.CAT_CD);
                        inventorymanagement.vFire = GetFire(inventorymanagement.viewInventory.CAT_CD);
                        inventorymanagement.vRegulation = GetRegulation(inventorymanagement.viewInventory.CAT_CD, inventorymanagement);
                        inventorymanagement.vOffice = GetOffice(inventorymanagement.viewInventory.CAT_CD);
                        return inventorymanagement;
                    }
                }

                inventorymanagement.listInventoryStockDetails = DbUtil.Select<ViewInventoryDetails>(SQL.SELECT_T_STOCK, _params);

                if (inventorymanagement.listInventoryStockDetails.Count > 0)
                {
                    foreach (var dr in inventorymanagement.listInventoryStockDetails)
                    {
                        inventorymanagement.hdnExecuteType = "0";
                        inventorymanagement.lblStatus = Convert.ToString(dr.STATUS_TEXT);
                        inventorymanagement.lblBarcode = Convert.ToString(dr.BARCODE);
                        inventorymanagement.lblCasNo = Convert.ToString(dr.CAS_NO);
                        inventorymanagement.lblProduct = Convert.ToString(dr.CHEM_NAME);
                        inventorymanagement.lblProduct2 = Convert.ToString(dr.CHEM_NAME);
                        inventorymanagement.lblCatCd = Convert.ToString(dr.CAT_CD);
                        inventorymanagement.lblMaker = Convert.ToString(dr.MAKER_NAME);
                        inventorymanagement.locbarcode = Convert.ToString(dr.LOC_BARCODE);

                        var LastUsageLocationId = Convert.ToString(dr.LAST_LOC_ID);
                        var OriginalLocationId = Convert.ToString(dr.LOC_ID);
                        inventorymanagement.LastUsageLocId = LastUsageLocationId;

                        List<V_LOCATION> locationUsageDic = new List<V_LOCATION>();
                        if (LastUsageLocationId != null)
                        {
                            locationUsageDic = GetLocationName(LastUsageLocationId, 2);
                            if (locationUsageDic != null && locationUsageDic.Count > 0)
                            {
                                inventorymanagement.usageLocName = locationUsageDic[0].LOC_NAME;
                                inventorymanagement.UsageLocation = locationUsageDic[0].LOC_BARCODE;
                            }
                        }
                        else
                        {
                            locationUsageDic = GetLocationName(LastUsageLocationId, 2);
                            if (locationUsageDic != null && locationUsageDic.Count > 0)
                            {
                                inventorymanagement.usageLocName = locationUsageDic[0].LOC_NAME;
                                inventorymanagement.UsageLocation = locationUsageDic[0].LOC_BARCODE;
                            }
                        }

                        List<V_LOCATION> locationDic = new List<V_LOCATION>();
                        if (OriginalLocationId != null)
                        {
                            string locbarcode = OriginalLocationId.ToString();
                            locationDic = GetLocationName(OriginalLocationId, 1);
                            if (locationDic != null && locationDic.Count > 0)
                            {
                                inventorymanagement.locName = locationDic[0].LOC_NAME;
                                inventorymanagement.StoringLocation = locationDic[0].LOC_BARCODE;
                            }
                        }
                        else
                        {
                            locationDic = GetLocationName(OriginalLocationId, 1);
                            if (locationDic != null && locationDic.Count > 0)
                            {
                                inventorymanagement.locName = locationDic[0].LOC_NAME;
                                inventorymanagement.StoringLocation = locationDic[0].LOC_BARCODE;
                            }
                        }

                        inventorymanagement.OriginalLocation = OriginalLocationId;

                        inventorymanagement.lblWeight = Convert.ToString(dr.CUR_GROSS_WEIGHT_G);
                        if (!string.IsNullOrEmpty(inventorymanagement.lblWeight))
                        {
                            inventorymanagement.lblWeight += "g";
                        }
                        inventorymanagement.lblIniWeight = Convert.ToString(dr.INI_GROSS_WEIGHT_G);
                        if (!string.IsNullOrEmpty(inventorymanagement.lblIniWeight))
                        {
                            inventorymanagement.lblIniWeight += "g";
                        }
                        inventorymanagement.lblGWeight = Convert.ToString(dr.BOTTLE_WEIGHT_G);
                        if (!string.IsNullOrEmpty(inventorymanagement.lblGWeight))
                        {
                            inventorymanagement.lblGWeight += "g";
                        }
                        if (dr.LIMIT_DATE != null)
                        {
                            inventorymanagement.lblLimit = Convert.ToDateTime(dr.LIMIT_DATE).ToString("yyyy/MM/dd");
                        }
                        inventorymanagement.lblUnitSize = OrgConvert.ToNullableDecimal(dr.UNITSIZE).ToString();
                        var s = OrgConvert.ToNullableDecimal(dr.UNITSIZE.ToString());
                        if (inventorymanagement.lblUnitSize != null && inventorymanagement.lblUnitSize != "")
                        {
                            inventorymanagement.lblUnitSize = s.Value.DecimalZeroSuppress().ToString() + Convert.ToString(dr.UNIT_NAME);
                        }
                        else
                        {
                            inventorymanagement.lblUnitSize = string.Empty;
                        }
                        inventorymanagement.hdnOriginalWeight = dr.CUR_GROSS_WEIGHT_G;
                        if (Convert.ToInt32(dr.MANAGE_WEIGHT) == (int)WeightManagement.Recommendation)
                        {
                            inventorymanagement.hdnAllWeight = dr.CUR_GROSS_WEIGHT_G;
                        }
                        inventorymanagement.hdnUnitSize = dr.UNITSIZE;
                        inventorymanagement.hdnStatusId = dr.STATUS_ID;
                        inventorymanagement.hdnManageWeight = dr.MANAGE_WEIGHT;
                        inventorymanagement.LastWeight = dr.LAST_WEIGHT;
                        inventorymanagement.LastUsageLocId = dr.LAST_LOC_ID;
                        inventorymanagement.StockId = Convert.ToString(dr.STOCK_ID);
                        var regNo = Convert.ToString(dr.REG_NO);
                        inventorymanagement.RegNo = regNo;

                        string storingLocation1 = SetLedgerStoringLocations(regNo);
                        inventorymanagement.storingFilterIds = storingLocation1;

                        string UsageLocation1 = SetLedgerUsageLocations(regNo);
                        inventorymanagement.usageFilterIds = UsageLocation1;




                        var workRecord = GetLastWorkRecord(strBarcode);
                        inventorymanagement.WorkRecordId = workRecord.WORK_RECORD_ID == default(long) ? string.Empty : workRecord.WORK_RECORD_ID.ToString();
                        inventorymanagement.LoginUserId = Common.GetLogonForm().UserName;
                        inventorymanagement.LoginUserCode = Common.GetLogonForm().Id;
                        inventorymanagement.WorkerIds = string.Empty;
                        inventorymanagement.lblWorkers = string.Empty;
                        inventorymanagement.ddlIntended = string.Empty;
                        inventorymanagement.txtIntended = string.Empty;
                        inventorymanagement.WorkCommentTargetWorkerIds = string.Empty;

                        var intendedIndex = Convert.ToString(dr.INTENDED_USE_ID);
                        if (!string.IsNullOrWhiteSpace(intendedIndex)) inventorymanagement.ddlIntended = intendedIndex;
                        if (dr.INTENDED != null)
                        {
                            inventorymanagement.txtIntended = Convert.ToString(dr.INTENDED).Trim();
                        }

                        var statusId = Convert.ToInt32(inventorymanagement.hdnStatusId);
                        if (statusId == (int)Const.cSTOCK_STATUS_ID.STOCK)
                        {
                            inventorymanagement.WorkerIds = USER_CD;
                        }
                        else if (statusId == (int)Const.cSTOCK_STATUS_ID.BORROW || statusId == (int)Const.cSTOCK_STATUS_ID.EMPTY)
                        {
                            if (workRecord.Workers.Count != 0)
                            {
                                inventorymanagement.WorkerIds = workRecord.Workers.Select(x => x.USER_CD).Aggregate((x, y) => $"{x},{y}");
                                inventorymanagement.lblWorkers = workRecord.Workers.Select(x => x.USER_NAME).Aggregate((x, y) => $"{x},{y}");
                            }
                            else
                            {
                                inventorymanagement.WorkerIds = USER_CD;
                            }
                        }

                        inventorymanagement.ExposureWorkUse = string.Empty;
                        inventorymanagement.ExposureWorkType = string.Empty;
                        inventorymanagement.UseTemperature = string.Empty;

                        if (statusId != (int)Const.cSTOCK_STATUS_ID.DISPOSE)
                        {
                            List<string> dtAbolishedLedger = DbUtil.Select<string>(SQL.SELECT_ABOLISHED_LEDGER, _params);
                            if (dtAbolishedLedger.Count > 0)
                            {
                                TempData["ValidationMessage"] = string.Format(WarningMessages.AbolishedLedger, strBarcode.ToUpper());
                                return inventorymanagement;
                            }
                        }
                    }
                }
                else
                {
                    TempData["ValidationMessage"] = string.Format(WarningMessages.NotFound, "瓶番号");
                    inventorymanagement.hdnStatusId = "9999";
                    return inventorymanagement;
                }
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return inventorymanagement;

        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to get the workers id
        /// /// <summary>
        [HttpGet]
        public string SaveWorkersId(string UserIds)
        {
            string status = "false";
            InventoryManagement inventorymanagement = new InventoryManagement();
            if (Session["LoadingUnloading"] != null)
            {
                inventorymanagement = (InventoryManagement)Session["LoadingUnloading"];
                inventorymanagement.lblWorkers = UserIds;
                Session["LoadingUnloading"] = inventorymanagement;
                status = "true";
            }
            return status;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to save workersid
        /// <summary>
        [HttpPost]
        public ActionResult SaveWorkersId(InventoryManagement inventorymanagement)
        {
            if (Session["LoadingUnloading"] != null)
            {
                inventorymanagement = (InventoryManagement)Session["LoadingUnloading"];
                inventorymanagement.lblWorkers = inventorymanagement.BARCODE_CONDITION;
                Session["LoadingUnloading"] = inventorymanagement;
            }
            return View(inventorymanagement);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is location selection control
        /// <summary>     

        public LocationClassId locationClassIds(string SelectionControlType)
        {

            if (SelectionControlType == "StoringLocation")
            {
                return LocationClassId.Storing;
            }
            else if (SelectionControlType == "UsageLocation")
            {
                return LocationClassId.Usage;
            }
            else
            {
                return LocationClassId.All;
            }

        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to get the location details using location id
        /// <summary>
        public List<V_LOCATION> GetLocationName(string locId, int usageid)
        {
            List<V_LOCATION> locname = new List<V_LOCATION>();

            if (!string.IsNullOrWhiteSpace(locId))
            {
                var sql = new StringBuilder();
                DynamicParameters _params = new DynamicParameters();
                sql.AppendLine("select");
                sql.AppendLine(" V_LOCATION.LOC_BARCODE,");
                sql.AppendLine(" LOC_NAME");
                sql.AppendLine("from");
                sql.AppendLine(" V_LOCATION");
                sql.AppendLine("where LOC_ID = @LOC_ID  AND CLASS_ID=@CLASS_ID AND DEL_FLAG=0 ");
                _params.Add("@LOC_ID", locId);
                _params.Add("@CLASS_ID", usageid);
                locname = DbUtil.Select<V_LOCATION>(sql.ToString(), _params);
            }
            return locname;
        }
        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to get the location using barcode
        /// <summary>
        public List<V_LOCATION> GetLocationNameAjax(string locId, int usageid)
        {
            List<V_LOCATION> locname = new List<V_LOCATION>();

            if (!string.IsNullOrWhiteSpace(locId))
            {
                var sql = new StringBuilder();
                DynamicParameters _params = new DynamicParameters();
                sql.AppendLine("select");
                sql.AppendLine(" V_LOCATION.LOC_BARCODE,");
                sql.AppendLine(" LOC_NAME");
                sql.AppendLine("from");
                sql.AppendLine(" V_LOCATION");
                sql.AppendLine("where");
                sql.AppendLine("LOC_ID = @LOC_BARCODE  AND CLASS_ID= @CLASS_ID AND DEL_FLAG=0 ");
                _params.Add("@LOC_BARCODE", locId);
                _params.Add("@CLASS_ID", usageid);
                locname = DbUtil.Select<V_LOCATION>(sql.ToString(), _params);
            }
            return locname;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to add the loading and unloading details
        /// <summary>
        private bool FirstAdd(InventoryManagement inventoryManagement)
        { 
            inventoryManagement.hdnExecuteType = "1";
            bool status = Execute(inventoryManagement);          
            return status;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to lend the chemicals details
        /// <summary>
        private bool Lend(InventoryManagement inventoryManagement)
        {
            bool status = false;           
            try
            {
                inventoryManagement.hdnExecuteType = "2"; //1:初回 2:貸出 3:返却 4:廃棄 5:使切 6:移管
                status = Execute(inventoryManagement);              
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return status;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to repay the chemical details
        /// <summary>
        private bool Repay(InventoryManagement inventoryManagement)
        {           
            bool status = false;           
            try
            {
                inventoryManagement.hdnExecuteType = "3"; //1:初回 2:貸出 3:返却 4:廃棄 5:使切 6:移管               
                status = Execute(inventoryManagement);             
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return status;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to move the chemicals from one storage to another location
        /// <summary>
        /// 
        private bool Move(InventoryManagement inventoryManagement)
        {
            bool status = false;                    
            try
            {
                inventoryManagement.hdnExecuteType = "6"; //1:初回 2:貸出 3:返却 4:廃棄 5:使切 6:移管
                status = Execute(inventoryManagement);               
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return status;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to delete the chemical details
        /// <summary>
        private bool Delete(InventoryManagement inventoryManagement)
        {           
            bool status = false;          
            try
            {
                inventoryManagement.hdnExecuteType = "4"; //1:初回 2:貸出 3:返却 4:廃棄 5:使切 6:移管
                status = Execute(inventoryManagement);             
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return status;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to get depletion details
        /// <summary>
        private bool Full(InventoryManagement inventoryManagement)
        {          
            bool status = false;           
            List<SelectListItem> listIntended = new List<SelectListItem> { };
            new IshaUsageMasterInfo().GetSelectList().ToList().ForEach(x => listIntended.Add(new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
            inventoryManagement.LookUpIntended = listIntended;
            try
            {
                inventoryManagement.hdnExecuteType = "5"; //1:初回 2:貸出 3:返却 4:廃棄 5:使切 6:移管
                status = Execute(inventoryManagement);              
            }
            catch (Exception ex)
            {
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return status;
        }



        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to loading and unloading details operations
        /// <summary>
        private bool Execute(InventoryManagement inventoryManagement)
        {
            DynamicParameters _params = new DynamicParameters();
            DynamicParameters _params2 = new DynamicParameters();
            ClsLocation _ClsLocation = new ClsLocation();
            string strStSQL = null; 
            bool status = false;
            decimal decBottoleWeight = 0;

            var locationDic = new ZyCoaG.LocationMasterInfo().GetDictionary();
            var storingLocationName = string.Empty;
            var usageLocationName = string.Empty;
            var workRecordId = default(long?);

            int intStatus = 0;
            try
            {
                switch (inventoryManagement.hdnExecuteType)
                {
                    case "1": //初回登録

                        intStatus = (int)Const.cSTOCK_STATUS_ID.STOCK;

                        strStSQL = "UPDATE T_STOCK SET STATUS_ID = @STATUS_ID,MEMO = @MEMO,LOC_ID = @LOC_ID,FIRST_LOC_ID = @LOC_ID ";

                        //重量測定判断
                        if (strWeightFlag[0] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                        Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                        {
                            //初期重量（風袋含む）
                            strStSQL += ",INI_GROSS_WEIGHT_G = @INI_GROSS_WEIGHT_G ";
                            _params.Add("@INI_GROSS_WEIGHT_G", inventoryManagement.txtAllWeight);

                            //現重量（風袋含む）
                            strStSQL += ",CUR_GROSS_WEIGHT_G = @CUR_GROSS_WEIGHT_G ";
                            _params.Add("@CUR_GROSS_WEIGHT_G", inventoryManagement.txtAllWeight);

                            //風袋重量（風袋含む）
                            decBottoleWeight = Convert.ToDecimal(inventoryManagement.txtAllWeight) - Convert.ToDecimal(inventoryManagement.txtUnitSize);
                            strStSQL += ",BOTTLE_WEIGHT_G = @BOTTLE_WEIGHT_G ";
                            _params.Add("@BOTTLE_WEIGHT_G", decBottoleWeight);
                        }

                        strStSQL += "WHERE STOCK_ID =@STOCK_ID ";

                        _params.Add("@STATUS_ID", intStatus);
                        _params.Add("@MEMO", inventoryManagement.txtAllWeight);
                        _params.Add("@LOC_ID", string.Join(SystemConst.SelectionsDelimiter, inventoryManagement.StoringLocation));
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);

                        DbUtil.ExecuteUpdate(strStSQL, _params);

                        _params = new DynamicParameters();

                        string UserCd = Common.GetLogonForm().User_CD;
                        _params.Add("@ACTION_TYPE_ID", Const.cACTION_HISTORY_TYPE.FIRST_ADD.ToString("D"));
                        _params.Add("@BARCODE", inventoryManagement.lblBarcode);
                        _params.Add("@USER_CD", UserCd);
                        _params.Add("@LOC_ID", Convert.ToString(_ClsLocation.LOC_ID));
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        if (inventoryManagement.StoringLocation.Count() > 0) storingLocationName = locationDic[Convert.ToInt32(inventoryManagement.StoringLocation.ToList()[0])];
                        _params.Add("@LOC_NAME", storingLocationName);
                        _params.Add("@USAGE_LOC_ID", null);
                        _params.Add("@USAGE_LOC_NAME", null);
                        _params.Add("@INTENDED", null);
                        _params.Add("@INTENDED_USE_ID", null);
                        _params.Add("@INTENDED_NM", null);
                        _params.Add("@WORKING_TIMES", null);

                        //重量測定判断
                        if (strWeightFlag[0] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                        Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", Convert.ToDecimal(inventoryManagement.txtAllWeight));
                            inventoryManagement.lblWeight = Convert.ToDecimal(inventoryManagement.txtAllWeight) + "g";
                        }
                        else
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", null);
                        }

                        _params2.Add("@USER_CD", Common.GetLogonForm().User_CD);
                        List<string> dt = DbUtil.Select<string>(SQL.SELECT_GROUP_INFO, _params2);
                        if (dt.Count > 0)
                        {
                            _params.Add("@ACTION_USER_NM", Convert.ToString(dt[0]));
                            _params.Add("@ACTION_GROUP_CD", Convert.ToString(dt[1]));
                            _params.Add("@ACTION_GROUP_NAME", Convert.ToString(dt[2]));
                        }
                        else
                        {
                            _params.Add("@ACTION_USER_NM", null);
                            _params.Add("@ACTION_GROUP_CD", null);
                            _params.Add("@ACTION_GROUP_NAME", null);
                        }
                        DbUtil.ExecuteUpdate(SQL.ACTION_HISTORY, _params);
                        status = true;

                        break;

                    case "2":

                        intStatus = (int)Const.cSTOCK_STATUS_ID.BORROW;

                        strStSQL = "UPDATE T_STOCK SET STATUS_ID = @STATUS_ID, BORROWER = @BORROWER, FIRST_LOC_ID = @LOC_ID, BRW_DATE = ";
                        if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
                        {
                            strStSQL += "SYSDATE ";
                        }
                        if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
                        {
                            strStSQL += "getdate() ";
                        }

                        //重量測定判断
                        if (strWeightFlag[1] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                        Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                        {
                            //現重量（風袋含む）
                            strStSQL += ",CUR_GROSS_WEIGHT_G = @CUR_GROSS_WEIGHT_G ";
                            _params.Add("@CUR_GROSS_WEIGHT_G", Convert.ToDecimal(inventoryManagement.txtAllWeight));

                        }

                        strStSQL += "WHERE STOCK_ID = @STOCK_ID ";

                        _params.Add("@STATUS_ID", intStatus);
                        _params.Add("@BORROWER", Common.GetLogonForm().User_CD);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        _params.Add("@LOC_ID", inventoryManagement.LastUsageLocId == "" ? null : inventoryManagement.LastUsageLocId);

                        DbUtil.ExecuteUpdate(strStSQL, _params);

                        _params = new DynamicParameters();

                        _params.Add("@ACTION_TYPE_ID", Const.cACTION_HISTORY_TYPE.BORROW.ToString("D"));
                        _params.Add("@BARCODE", inventoryManagement.lblBarcode);
                        _params.Add("@USER_CD", Common.GetLogonForm().User_CD);
                        if (inventoryManagement.txtMemo != null)
                        {
                            _params.Add("@MEMO", inventoryManagement.txtMemo.Trim());
                        }
                        else
                        {
                            _params.Add("@MEMO", inventoryManagement.txtMemo);
                        }
                        _params.Add("@LOC_ID", inventoryManagement.OriginalLocation);
                        if (!string.IsNullOrWhiteSpace(inventoryManagement.OriginalLocation)) storingLocationName = locationDic[Convert.ToInt32(inventoryManagement.OriginalLocation)];
                        _params.Add("@LOC_NAME", storingLocationName);
                        _params.Add("@USAGE_LOC_ID", Convert.ToInt32(string.Join(SystemConst.SelectionsDelimiter, inventoryManagement.LastUsageLocId)));

                        if (inventoryManagement.LastUsageLocId.Count() > 0)
                        {
                            usageLocationName = locationDic[Convert.ToInt32(inventoryManagement.LastUsageLocId.ToList()[0])];
                        }
                        _params.Add("@USAGE_LOC_NAME", usageLocationName);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        if (inventoryManagement.txtIntended != null)
                        {
                            _params.Add("@INTENDED", inventoryManagement.txtIntended.Trim());
                        }
                        else
                        {
                            _params.Add("@INTENDED", inventoryManagement.txtIntended);
                        }

                        List<SelectListItem> listIntended = new List<SelectListItem> { };
                        new IshaUsageMasterInfo().GetSelectList().ToList().ForEach(x => listIntended.Add(new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
                        inventoryManagement.LookUpIntended = listIntended;

                        _params.Add("@INTENDED_USE_ID", inventoryManagement.ddlIntended);
                        _params.Add("@INTENDED_NM", GetMessageById(inventoryManagement.LookUpIntended, inventoryManagement.ddlIntended));
                        _params.Add("@WORKING_TIMES", null);

                        if (strWeightFlag[1] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                        Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", Convert.ToDecimal(inventoryManagement.txtAllWeight));
                            inventoryManagement.lblWeight = Convert.ToDecimal(inventoryManagement.lblWeight) + "g";
                        }
                        else
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", null);
                        }

                        _params2.Add("@USER_CD", Common.GetLogonForm().Id);
                        List<M_USER> uservalues = DbUtil.Select<M_USER>(SQL.SELECT_GROUP_INFO, _params2);
                        if (uservalues.Count > 0)
                        {
                            _params.Add("@ACTION_USER_NM", Convert.ToString(uservalues[0].USER_NAME));
                            _params.Add("@ACTION_GROUP_CD", Convert.ToString(uservalues[0].GROUP_CD));
                            _params.Add("@ACTION_GROUP_NAME", Convert.ToString(uservalues[0].GROUP_NAME));
                        }
                        else
                        {
                            _params.Add("@ACTION_USER_NM", null);
                            _params.Add("@ACTION_GROUP_CD", null);
                            _params.Add("@ACTION_GROUP_NAME", null);
                        }

                        //T_ACTION_HISTORYへ追加
                        DbUtil.ExecuteUpdate(SQL.ACTION_HISTORY, _params);

                        workRecordId = InsertBorrowWorkRecord(inventoryManagement);
                        InsertWorker(inventoryManagement, workRecordId.Value);
                        status = true;
                        break;

                    case "3":

                        intStatus = (int)Const.cSTOCK_STATUS_ID.STOCK;
                        strStSQL = "UPDATE T_STOCK SET STATUS_ID = @STATUS_ID, FIRST_LOC_ID = @LOC_ID,";
                        strStSQL += "BORROWER = @BORROWER, BRW_DATE = ";
                        if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
                        {
                            strStSQL += "SYSDATE ";
                        }
                        if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
                        {
                            strStSQL += "getdate() ";
                        }

                        //重量測定判断
                        if (strWeightFlag[2] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                        Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                        {
                            //現重量（風袋含む）
                            strStSQL += ",CUR_GROSS_WEIGHT_G = @CUR_GROSS_WEIGHT_G ";
                            _params.Add("@CUR_GROSS_WEIGHT_G", Convert.ToDecimal(inventoryManagement.txtAllWeight));

                        }

                        strStSQL += "WHERE STOCK_ID = @STOCK_ID ";

                        _params.Add("@STATUS_ID", intStatus);
                        _params.Add("@BORROWER", Common.GetLogonForm().User_CD);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        _params.Add("@LOC_ID", inventoryManagement.OriginalLocation);

                        DbUtil.ExecuteUpdate(strStSQL, _params);

                        _params = new DynamicParameters();

                        _params.Add("@ACTION_TYPE_ID", Const.cACTION_HISTORY_TYPE.REPAY.ToString("D"));
                        _params.Add("@BARCODE", inventoryManagement.lblBarcode);
                        _params.Add("@USER_CD", Common.GetLogonForm().User_CD);

                        if (inventoryManagement.txtMemo != null)
                        {
                            _params.Add("@MEMO", inventoryManagement.txtMemo.Trim());
                        }
                        else
                        {
                            _params.Add("@MEMO", inventoryManagement.txtMemo);
                        }

                        _params.Add("@LOC_ID", inventoryManagement.OriginalLocation);
                        if (!string.IsNullOrWhiteSpace(inventoryManagement.OriginalLocation)) storingLocationName = locationDic[Convert.ToInt32(inventoryManagement.OriginalLocation)];
                        _params.Add("@LOC_NAME", storingLocationName);
                        _params.Add("@USAGE_LOC_ID", null);
                        _params.Add("@USAGE_LOC_NAME", null);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        _params.Add("@INTENDED", null);
                        _params.Add("@INTENDED_USE_ID", null);
                        _params.Add("@INTENDED_NM", null);

                        if (inventoryManagement.txtWorkingTimes != null)
                        {
                            _params.Add("@WORKING_TIMES", inventoryManagement.txtWorkingTimes.Trim());
                        }
                        else
                        {
                            _params.Add("@WORKING_TIMES", inventoryManagement.txtWorkingTimes);
                        }

                        //重量測定判断
                        if (strWeightFlag[2] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                        Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", Convert.ToDecimal(inventoryManagement.txtAllWeight));
                            inventoryManagement.lblWeight = Convert.ToDecimal(inventoryManagement.txtAllWeight) + "g";
                        }
                        else
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", null);
                        }


                        _params2.Add("@USER_CD", Common.GetLogonForm().User_CD);
                        List<M_USER> groupval = DbUtil.Select<M_USER>(SQL.SELECT_GROUP_INFO, _params2);
                        if (groupval.Count > 0)
                        {
                            _params.Add("@ACTION_USER_NM", groupval[0].USER_NAME == null ? "" : groupval[0].USER_NAME);
                            _params.Add("@ACTION_GROUP_CD", groupval[0].GROUP_CD == null ? "" : groupval[0].GROUP_CD);
                            _params.Add("@ACTION_GROUP_NAME", groupval[0].GROUP_NAME == null ? "" : groupval[0].GROUP_NAME);
                        }
                        else
                        {
                            _params.Add("@ACTION_USER_NM", null);
                            _params.Add("@ACTION_GROUP_CD", null);
                            _params.Add("@ACTION_GROUP_NAME", null);
                        }

                        //T_ACTION_HISTORYへ追加
                        DbUtil.ExecuteUpdate(SQL.ACTION_HISTORY, _params);

                        workRecordId = string.IsNullOrEmpty(inventoryManagement.WorkRecordId) ? null : new long?(Convert.ToInt64(inventoryManagement.WorkRecordId));

                        if (workRecordId.HasValue)
                        {
                            workRecordId = Convert.ToInt64(inventoryManagement.WorkRecordId);
                            UpdateRepayWorkRecord(inventoryManagement, workRecordId.Value);
                            DeleteWorker(inventoryManagement, workRecordId.Value);
                            InsertWorker(inventoryManagement, workRecordId.Value);
                            UpdateAccidentWorker(inventoryManagement, workRecordId.Value);
                        }
                        status = true;
                        break;

                    case "4":

                        intStatus = (int)Const.cSTOCK_STATUS_ID.DISPOSE;

                        strStSQL = "UPDATE T_STOCK SET STATUS_ID = @STATUS_ID, BORROWER = @BORROWER, FIRST_LOC_ID = @LOC_ID, BRW_DATE = ";
                        if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
                        {
                            strStSQL += "SYSDATE ";
                        }
                        if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
                        {
                            strStSQL += "getdate() ";
                        }

                        //重量測定判断
                        if (strWeightFlag[3] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                        Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                        {
                            //現重量（風袋含む）
                            strStSQL += ",CUR_GROSS_WEIGHT_G = BOTTLE_WEIGHT_G ";
                        }

                        strStSQL += "WHERE STOCK_ID = @STOCK_ID ";

                        _params.Add("@STATUS_ID", intStatus);
                        _params.Add("@BORROWER", Common.GetLogonForm().Id);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        _params.Add("@LOC_ID", null);

                        DbUtil.ExecuteUpdate(strStSQL, _params);

                        _params = new DynamicParameters();

                        _params.Add("@ACTION_TYPE_ID", Const.cACTION_HISTORY_TYPE.DISPOSE.ToString("D"));
                        _params.Add("@BARCODE", inventoryManagement.lblBarcode);
                        _params.Add("@USER_CD", Common.GetLogonForm().Id);
                        _params.Add("@MEMO", inventoryManagement.txtMemo);
                        _params.Add("@LOC_ID", null);
                        _params.Add("@LOC_NAME", null);
                        _params.Add("@USAGE_LOC_ID", null);
                        _params.Add("@USAGE_LOC_NAME", null);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        _params.Add("@INTENDED", null);
                        _params.Add("@INTENDED_USE_ID", null);
                        _params.Add("@INTENDED_NM", null);
                        if (Convert.ToInt32(inventoryManagement.hdnStatusId) == (int)Const.cSTOCK_STATUS_ID.BORROW || Convert.ToInt32(inventoryManagement.hdnStatusId) == (int)Const.cSTOCK_STATUS_ID.EMPTY)
                        {
                            _params.Add("@WORKING_TIMES", inventoryManagement.txtWorkingTimes);
                        }
                        else
                        {
                            _params.Add("@WORKING_TIMES", null);
                        }

                        //重量測定判断
                        if (strWeightFlag[3] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                        Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", Convert.ToDecimal(inventoryManagement.txtAllWeight));
                            inventoryManagement.lblWeight = Convert.ToDecimal(inventoryManagement.txtAllWeight) + "g";
                        }
                        else
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", null);
                        }

                        _params2.Add("@USER_CD", Common.GetLogonForm().Id);
                        List<M_USER> dtvalues = DbUtil.Select<M_USER>(SQL.SELECT_GROUP_INFO, _params2);
                        if (dtvalues.Count > 0)
                        {
                            _params.Add("@ACTION_USER_NM", Convert.ToString(dtvalues[0].USER_NAME));
                            _params.Add("@ACTION_GROUP_CD", Convert.ToString(dtvalues[0].GROUP_CD));
                            _params.Add("@ACTION_GROUP_NAME", Convert.ToString(dtvalues[0].GROUP_NAME));
                        }
                        else
                        {
                            _params.Add("@ACTION_USER_NM", null);
                            _params.Add("@ACTION_GROUP_CD", null);
                            _params.Add("@ACTION_GROUP_NAME", null);
                        }

                        //T_ACTION_HISTORYへ追加
                        DbUtil.ExecuteUpdate(SQL.ACTION_HISTORY, _params);
                        if (Convert.ToInt32(inventoryManagement.hdnStatusId) == (int)Const.cSTOCK_STATUS_ID.STOCK)
                        {
                            workRecordId = InsertBorrowWorkRecord(inventoryManagement);
                        }
                        else
                        {
                            workRecordId = string.IsNullOrEmpty(inventoryManagement.WorkRecordId) ? null : new long?(Convert.ToInt64(inventoryManagement.WorkRecordId));
                        }
                        if (workRecordId.HasValue)
                        {
                            UpdateRepayWorkRecord(inventoryManagement, workRecordId.Value);
                            DeleteWorker(inventoryManagement, workRecordId.Value);
                            InsertWorker(inventoryManagement, workRecordId.Value);
                            UpdateAccidentWorker(inventoryManagement, workRecordId.Value);
                        }
                        status = true;
                        break;

                    case "5":

                        intStatus = (int)Const.cSTOCK_STATUS_ID.EMPTY;
                        strStSQL = "UPDATE T_STOCK SET STATUS_ID = @STATUS_ID, CUR_GROSS_WEIGHT_G = BOTTLE_WEIGHT_G, BORROWER = @BORROWER, FIRST_LOC_ID = @LOC_ID, BRW_DATE = ";
                        if (Common.DatabaseType == ZyCoaG.DatabaseType.Oracle)
                        {
                            strStSQL += "SYSDATE ";
                        }
                        if (Common.DatabaseType == ZyCoaG.DatabaseType.SqlServer)
                        {
                            strStSQL += "getdate() ";
                        }

                        strStSQL += "WHERE STOCK_ID = @STOCK_ID ";

                        _params.Add("@STATUS_ID", intStatus);
                        _params.Add("@BORROWER", Common.GetLogonForm().User_CD);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        _params.Add("@LOC_ID", inventoryManagement.LastUsageLocId);

                        DbUtil.ExecuteUpdate(strStSQL, _params);

                        _params = new DynamicParameters();

                        _params.Add("@ACTION_TYPE_ID", Const.cACTION_HISTORY_TYPE.EMPTY.ToString("D"));
                        _params.Add("@BARCODE", inventoryManagement.lblBarcode);
                        _params.Add("@USER_CD", Common.GetLogonForm().User_CD);
                        _params.Add("@MEMO", inventoryManagement.txtMemo);
                        _params.Add("@LOC_ID", inventoryManagement.OriginalLocation);
                        if (!string.IsNullOrWhiteSpace(inventoryManagement.OriginalLocation)) storingLocationName = locationDic[Convert.ToInt32(inventoryManagement.OriginalLocation)];
                        _params.Add("@LOC_NAME", storingLocationName);
                        _params.Add("@USAGE_LOC_ID", Convert.ToInt32(inventoryManagement.LastUsageLocId));
                        if (inventoryManagement.LastUsageLocId.Count() > 0) usageLocationName = locationDic[Convert.ToInt32(inventoryManagement.LastUsageLocId.ToList()[0])];
                        _params.Add("@USAGE_LOC_NAME", usageLocationName);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        _params.Add("@INTENDED", inventoryManagement.txtIntended);
                        _params.Add("@INTENDED_USE_ID", inventoryManagement.ddlIntended);
                        _params.Add("@INTENDED_NM", GetMessageById(inventoryManagement.LookUpIntended, inventoryManagement.ddlIntended));
                        _params.Add("@WORKING_TIMES", null);

                        //重量測定判断
                        if (strWeightFlag[3] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                        Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", Convert.ToDecimal(inventoryManagement.txtAllWeight));
                            inventoryManagement.lblWeight = Convert.ToDecimal(inventoryManagement.txtAllWeight) + "g";
                        }
                        else
                        {
                            _params.Add("@CUR_GROSS_WEIGHT_G", null);
                        }

                        _params2.Add("@USER_CD", Common.GetLogonForm().User_CD);
                        List<M_USER> values = DbUtil.Select<M_USER>(SQL.SELECT_GROUP_INFO, _params2);
                        if (values.Count > 0)
                        {
                            _params.Add("@ACTION_USER_NM", Convert.ToString(values[0]));
                            _params.Add("@ACTION_GROUP_CD", Convert.ToString(values[0]));
                            _params.Add("@ACTION_GROUP_NAME", Convert.ToString(values[0]));
                        }
                        else
                        {
                            _params.Add("@ACTION_USER_NM", null);
                            _params.Add("@ACTION_GROUP_CD", null);
                            _params.Add("@ACTION_GROUP_NAME", null);
                        }

                        //T_ACTION_HISTORYへ追加
                        DbUtil.ExecuteUpdate(SQL.ACTION_HISTORY, _params);

                        workRecordId = InsertBorrowWorkRecord(inventoryManagement);
                        InsertWorker(inventoryManagement, workRecordId.Value);
                        status = true;
                        break;

                    case "6":

                        intStatus = (int)Const.cSTOCK_STATUS_ID.STOCK;
                        _params = new DynamicParameters();
                        // 保管場所と現在保有場所を取得します。
                        strStSQL = "select LOC_ID,FIRST_LOC_ID from T_STOCK where STOCK_ID = @STOCK_ID ";
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        var locationMatch = DbUtil.Select<T_STOCK>(strStSQL, _params);
                        var isFirstLocationUpdate = true;
                        foreach (var row in locationMatch)
                        {
                            // 保管場所と現在保有場所が違えば現在保有場所は更新しません。
                            if (row.LOC_ID.ToString() != row.FIRST_LOC_ID.ToString())
                            {
                                isFirstLocationUpdate = false;
                            }
                        }

                        _params = new DynamicParameters();
                        strStSQL = "UPDATE T_STOCK SET STATUS_ID = @STATUS_ID, LOC_ID =@LOC_ID, BORROWER = @BORROWER, BRW_DATE = ";
                        if (Common.DatabaseType == Classes.util.DatabaseType.Oracle)
                        {
                            strStSQL += "SYSDATE ";
                        }
                        if (Common.DatabaseType == Classes.util.DatabaseType.SqlServer)
                        {
                            strStSQL += "getdate() ";
                        }
                        if (isFirstLocationUpdate)
                        {
                            strStSQL += ",FIRST_LOC_ID = @LOC_ID ";
                        }
                        strStSQL += "WHERE STOCK_ID = @STOCK_ID ";
                        _params.Add("@STATUS_ID", intStatus);
                        _params.Add("@LOC_ID", Convert.ToInt32(string.Join(SystemConst.SelectionsDelimiter, inventoryManagement.StoringLocation)));
                        _params.Add("@BORROWER", Common.GetLogonForm().User_CD);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        DbUtil.ExecuteUpdate(strStSQL, _params);
                        _params = new DynamicParameters();
                        _params.Add("@ACTION_TYPE_ID", Const.cACTION_HISTORY_TYPE.MOVE.ToString("D"));
                        _params.Add("@BARCODE", inventoryManagement.lblBarcode);
                        _params.Add("@USER_CD", Common.GetLogonForm().User_CD);
                        _params.Add("@MEMO", inventoryManagement.txtMemo);
                        _params.Add("@LOC_ID", Convert.ToInt32(string.Join(SystemConst.SelectionsDelimiter, inventoryManagement.StoringLocation)));
                        if (inventoryManagement.StoringLocation != "")
                        {
                            storingLocationName = locationDic[Convert.ToInt32(inventoryManagement.StoringLocation)];
                        }
                        _params.Add("@LOC_NAME", storingLocationName);
                        _params.Add("@USAGE_LOC_ID", null);
                        _params.Add("@USAGE_LOC_NAME", null);
                        _params.Add("@CUR_GROSS_WEIGHT_G", null);
                        _params.Add("@STOCK_ID", inventoryManagement.StockId);
                        _params.Add("@INTENDED", null);
                        _params.Add("@INTENDED_USE_ID", null);
                        _params.Add("@INTENDED_NM", null);
                        _params.Add("@WORKING_TIMES", null);
                        _params2.Add("@USER_CD", Common.GetLogonForm().User_CD);
                        List<M_USER> dtvalue = DbUtil.Select<M_USER>(SQL.SELECT_GROUP_INFO, _params2);
                        if (dtvalue.Count > 0)
                        {
                            _params.Add("@ACTION_USER_NM", Convert.ToString(dtvalue[0].USER_NAME));
                            _params.Add("@ACTION_GROUP_CD", Convert.ToString(dtvalue[0].GROUP_CD));
                            _params.Add("@ACTION_GROUP_NAME", Convert.ToString(dtvalue[0].GROUP_NAME));
                        }
                        else
                        {
                            _params.Add("@ACTION_USER_NM", null);
                            _params.Add("@ACTION_GROUP_CD", null);
                            _params.Add("@ACTION_GROUP_NAME", null);
                        }
                        DbUtil.ExecuteUpdate(SQL.ACTION_HISTORY, _params);
                        status = true;
                        break;
                }
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return status;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to load the GHS category details
        /// <summary>
        public IEnumerable<V_GHSCAT> GetGhsCategory(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                var chemGhsCategoryInfo = new ChemRegulationDataInfo(ClassIdType.GhsCategory);
                var condition = new M_CHEM_REGULATION();
                condition.CHEM_CD = value;
                var records = chemGhsCategoryInfo.GetSearchResult(condition);
                var ids = records.Select(x => x.REG_TYPE_ID).ToList();
                GhsCategoryMasterInfo ghsCategoryInfo = new GhsCategoryMasterInfo();
                return ghsCategoryInfo.GetSelectedItems(ids).AsQueryable();
            }
            return new List<V_GHSCAT>().AsQueryable();
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to load the Regulation Details
        /// <summary>
        public IEnumerable<V_REGULATION> GetRegulation(string value, InventoryManagement inventoryMgmt)
        {
            if (!string.IsNullOrEmpty(value))
            {
                var chemRegulationInfo = new ChemRegulationDataInfo(ClassIdType.Regulation);

                M_CHEM_REGULATION condition = new M_CHEM_REGULATION();
                condition.CHEM_CD = value;
                var records = chemRegulationInfo.GetSearchResult(condition);
                var ids = records.Select(x => x.REG_TYPE_ID).ToList();
                var regulationInfo = new Classes.RegulationMasterInfo();
                var items = regulationInfo.GetSelectedItems(ids);
                var isWorkCommentInput = items.Any(x => x.WORKTIME_MGMT == (int)WorkingTimesManagement.Target);
                inventoryMgmt.IsWorkCommentInput = isWorkCommentInput.ToString();
                var statudId = Convert.ToInt32(inventoryMgmt.hdnStatusId);
                if (isWorkCommentInput && (statudId == (int)Const.cSTOCK_STATUS_ID.BORROW || statudId == (int)Const.cSTOCK_STATUS_ID.EMPTY))
                    inventoryMgmt.IsExposureWork = items.Any(x => x.EXPOSURE_REPORT == (int)ExposureWorkReport.Target).ToString();
                return regulationInfo.GetSelectedItems(ids).AsQueryable();
            }
            return new List<V_REGULATION>().AsQueryable();
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to load the Fire details
        /// <summary>
        public IEnumerable<V_FIRE> GetFire(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                var chemFireInfo = new ChemRegulationDataInfo(ClassIdType.Fire);
                var condition = new M_CHEM_REGULATION();
                condition.CHEM_CD = value;
                var records = chemFireInfo.GetSearchResult(condition);
                var ids = records.Select(x => x.REG_TYPE_ID).ToList();
                var FireInfo = new FireMasterInfo();
                return FireInfo.GetSelectedItems(ids).AsQueryable();
            }
            return new List<V_FIRE>().AsQueryable();
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to load the Office category details
        /// <summary>
        public IEnumerable<V_OFFICE> GetOffice(string value)
        {
            if (!string.IsNullOrEmpty(value))
            {
                var chemOfficeInfo = new ChemRegulationDataInfo(ClassIdType.Office);
                var condition = new M_CHEM_REGULATION();
                condition.CHEM_CD = value;
                var records = chemOfficeInfo.GetSearchResult(condition);
                var ids = records.Select(x => x.REG_TYPE_ID).ToList();
                var OfficeInfo = new OfficeMasterInfo();
                return OfficeInfo.GetSelectedItems(ids).AsQueryable();
            }
            return new List<V_OFFICE>().AsQueryable();
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to set the ledger location location details
        /// <summary>
        private string SetLedgerStoringLocations(string regNo)
        {
            var sql = $@"SELECT LOC_ID
                         FROM T_MGMT_LED_LOC
                         WHERE REG_NO = @REG_NO
                               AND DEL_FLAG = 0";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@REG_NO", regNo);

            var data = DbUtil.Select<string>(sql, parameters);

            string StoringLocationFilterIds = null;
            string StoringLocation = null;
            string FilterId = null;

            if (data.Count() == 0)
            {
                StoringLocationFilterIds = string.Empty;
                StoringLocation = StoringLocationFilterIds;
            }
            else
            {
                var locationIds = new List<string>();
                foreach (var row in data)
                {
                    locationIds.Add(row.ToString());
                }
                StoringLocationFilterIds = locationIds.Aggregate((x, y) => $"{x}{SystemConst.CodeDelimiter}{y}");
                string[] FilterIds = UpdateLedgerStoringLocations(StoringLocationFilterIds);

                FilterId = string.Join(",", FilterIds);
                StoringLocation = StoringLocationFilterIds + "," + FilterId;
            }

            //UpdateLedgerStoringLocations(StoringLocationFilterIds);
            return StoringLocationFilterIds;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to set the ledger location details
        /// <summary>
        private string[] UpdateLedgerStoringLocations(string StoringLocationFilterIds)
        {
            string[] FilterIds = null;
            if (!string.IsNullOrWhiteSpace(StoringLocationFilterIds))
            {
                FilterIds = StoringLocationFilterIds.Split(SystemConst.CodeDelimiter.ToCharArray()[0]);
            }
            return FilterIds;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to set the ledger usage location details
        /// <summary>
        private string SetLedgerUsageLocations(string regNo)
        {
            var sql = $@"SELECT LOC_ID
                         FROM T_MGMT_LED_USAGE_LOC
                         WHERE     REG_NO = @REG_NOs
                               AND DEL_FLAG = 0";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@REG_NOs", Convert.ToString(regNo));

            var data = DbUtil.Select<string>(sql, parameters);

            string UsageLocationFilterIds = null;
            string UsageLocation = null;
            string FilterId = null;
            if (data.Count() == 0)
            {
                UsageLocationFilterIds = string.Empty;
            }
            else
            {
                var locationIds = new List<string>();
                foreach (var row in data)
                {
                    locationIds.Add(row.ToString());
                }

                UsageLocationFilterIds = locationIds.Aggregate((x, y) => $"{x}{SystemConst.CodeDelimiter}{y}");
                string[] FilterIds = UpdateLedgerUsageLocations(UsageLocationFilterIds);

                FilterId = string.Join(",", FilterIds);
                UsageLocation = UsageLocationFilterIds + "," + FilterId;
            }           
            return UsageLocationFilterIds;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to update the usage location details
        /// <summary>
        private string[] UpdateLedgerUsageLocations(string UsageLocationFilterIds)
        {
            string[] FilterIds = null;
            if (!string.IsNullOrWhiteSpace(UsageLocationFilterIds))
            {
                FilterIds = UsageLocationFilterIds.Split(SystemConst.CodeDelimiter.ToCharArray()[0]);
            }
            return FilterIds;
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is used to insert the borrowerrecord details
        /// <summary>
        private long InsertBorrowWorkRecord(InventoryManagement inventoryManagement)
        {
            var insertSql = $@"INSERT INTO T_WORK_RECORD (   ACTION_ID
                                                           , ISSUE_DATE
                                                           , BARCODE
                                                           , REG_USER_CD )
                               VALUES (   (
                                            SELECT MAX(ACTION_ID)
                                            FROM T_ACTION_HISTORY
                                          )
                                        , @ISSUE_DATE
                                        , @BARCODE
                                        , @REG_USER_CD 
                                      )";
            DynamicParameters insertParameters = new DynamicParameters();
            insertParameters.Add("@ISSUE_DATE", string.Format("{0:yyyy-MM-dd 00:00:00.000}", DateTime.Now));
            insertParameters.Add("@BARCODE", Convert.ToString(inventoryManagement.lblBarcode));
            insertParameters.Add("@REG_USER_CD", Convert.ToString(Common.GetLogonForm().Id));

            DbUtil.ExecuteUpdate(insertSql, insertParameters);

            var selectSql = $@"SELECT MAX(WORK_RECORD_ID) AS MAX_RECORD_ID
                               FROM T_WORK_RECORD
                               WHERE BARCODE = @BARCODE";

            DynamicParameters selectParameters = new DynamicParameters();
            selectParameters.Add("@BARCODE", Convert.ToString(inventoryManagement.lblBarcode));

            List<string> dt = DbUtil.Select<string>(selectSql, selectParameters);
            return Convert.ToInt64(dt[0]);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to get the last work record details
        /// <summary>
        private T_WORK_RECORD GetLastWorkRecord(string strBarcode)
        {
            var sql = $@"SELECT   WORK_RECORD_ID
                                , ACTION_ID
                                , ISSUE_DATE
                                , RECEIPT_DATE
                                , BARCODE
                                , EXPOSURE_WORK_USE
                                , EXPOSURE_WORK_TYPE
                                , EXPOSURE_WORK_TEMPERATURE
                                , UNITSIZE
                                , UNITSIZE_ID
                                , NOTICES
                                , REASON_FOR_UPDATE
                         FROM T_WORK_RECORD
                         WHERE WORK_RECORD_ID = ( SELECT MAX(WORK_RECORD_ID)
                                                  FROM T_WORK_RECORD
                                                  WHERE BARCODE = @BARCODE )";
            DynamicParameters selectParameters = new DynamicParameters();
            selectParameters.Add("@BARCODE", Convert.ToString(strBarcode));

            var dt = DbUtil.Select<T_WORK_RECORD>(sql, selectParameters);
            var workRecord = new T_WORK_RECORD() { Workers = new List<M_USER>() }; ;
            if (dt.Count() == 0) return workRecord;
            var row = dt[0];
            workRecord.WORK_RECORD_ID = Convert.ToInt64(row.WORK_RECORD_ID);
            workRecord.ACTION_ID = OrgConvert.ToNullableInt32(row.ACTION_ID);
            workRecord.ISSUE_DATE = OrgConvert.ToNullableDateTime(row.ISSUE_DATE);
            workRecord.RECEIPT_DATE = OrgConvert.ToNullableDateTime(row.RECEIPT_DATE);
            workRecord.BARCODE = row.BARCODE.ToString();
            workRecord.EXPOSURE_WORK_USE = OrgConvert.ToNullableInt32(row.EXPOSURE_WORK_USE);
            workRecord.EXPOSURE_WORK_TYPE = OrgConvert.ToNullableInt32(row.EXPOSURE_WORK_TYPE);
            workRecord.EXPOSURE_WORK_TEMPERATURE = OrgConvert.ToNullableInt32(row.EXPOSURE_WORK_TEMPERATURE);
            workRecord.UNITSIZE = OrgConvert.ToNullableDecimal(row.UNITSIZE);
            workRecord.UNITSIZE_ID = OrgConvert.ToNullableInt32(row.UNITSIZE_ID);
            if (row.NOTICES != null)
            {
                workRecord.NOTICES = row.NOTICES.ToString();
            }

            if (row.REASON_FOR_UPDATE != null)
            {
                workRecord.REASON_FOR_UPDATE = row.REASON_FOR_UPDATE.ToString();
            }
            if (row.WORK_RECORD_ID != 0)
            {
                workRecord.Workers = GetWorkRecordWorkers(workRecord.WORK_RECORD_ID);
            }
            return workRecord;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to load the work record details
        /// <summary>
        private ICollection<M_USER> GetWorkRecordWorkers(long workRecordId)
        {
            var sql = $@"SELECT   M_USER.USER_CD 
                                , M_USER.USER_NAME
                         FROM            T_WORKER
                              INNER JOIN M_USER
                              ON     T_WORKER.WORKER_USER_CD = M_USER.USER_CD
                         WHERE T_WORKER.WORK_RECORD_ID = @WORK_RECORD_ID
                         ORDER BY T_WORKER.WORKER_USER_CD";
            DynamicParameters selectParameters = new DynamicParameters();
            selectParameters.Add("@WORK_RECORD_ID", workRecordId);
            var dt = DbUtil.Select<M_USER>(sql, selectParameters);
            var workers = new List<M_USER>();
            foreach (var row in dt)
            {
                var worker = new M_USER()
                {
                    USER_CD = row.USER_CD.ToString(),
                    USER_NAME = row.USER_NAME.ToString()
                };
                workers.Add(worker);
            }
            return workers;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to update the repay work record details
        /// <summary>
        private void UpdateRepayWorkRecord(InventoryManagement context, long workRecordId)
        {
            Func<Tuple<decimal?, int?>> calculateUsedAmount = () =>
            {
                var usedAmount = default(decimal?);
                var unitsize = default(int?);
                if (!string.IsNullOrWhiteSpace(context.LastWeight) && !string.IsNullOrWhiteSpace(context.txtAllWeight))
                {
                    usedAmount = Math.Abs(Convert.ToDecimal(context.LastWeight) - Convert.ToDecimal(context.txtAllWeight));
                    unitsize = GetUnitSizeList().First(y => y.Name == "g").Id;
                }
                else
                {
                    usedAmount = OrgConvert.ToNullableDecimal(context.txtAmount);
                    unitsize = string.IsNullOrWhiteSpace(context.AmountUnitSize) ? null : new int?(Convert.ToInt32(context.AmountUnitSize));
                }
                return new Tuple<decimal?, int?>(usedAmount, unitsize);
            };
            UpdateRepayWorkRecord(context, workRecordId, calculateUsedAmount);
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to update the repay work record details
        /// <summary>
        private void UpdateRepayWorkRecord(InventoryManagement context, long workRecordId, Func<Tuple<decimal?, int?>> calculateUsedAmount)
        {
            bool IsUsageAutoCalculation = (Convert.ToInt32(context.hdnManageWeight) == (int)WeightManagement.Required);
            var sql = $@"UPDATE T_WORK_RECORD
                         SET   RECEIPT_DATE = @RECEIPT_DATE
                             , EXPOSURE_WORK_USE = @EXPOSURE_WORK_USE
                             , EXPOSURE_WORK_TYPE = @EXPOSURE_WORK_TYPE
                             , EXPOSURE_WORK_TEMPERATURE = @EXPOSURE_WORK_TEMPERATURE
                             , UNITSIZE = @UNITSIZE
                             , UNITSIZE_ID = @UNITSIZE_ID
                             , NOTICES = @NOTICES
                         WHERE WORK_RECORD_ID = @WORK_RECORD_ID";

            Func<decimal?, int?> getUsedAmountUnitsize = (x) =>
            {
                if (!x.HasValue) return null;
                if (!IsUsageAutoCalculation) return Convert.ToInt32(context.AmountUnitSize);
                return GetUnitSizeList().First(y => y.Name == "g").Id;
            };

            var usedAmount = calculateUsedAmount();

            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@RECEIPT_DATE", DateTime.Today);
            parameters.Add("@EXPOSURE_WORK_USE", context.ExposureWorkUse);
            parameters.Add("@EXPOSURE_WORK_TYPE", context.ExposureWorkType);
            parameters.Add("@EXPOSURE_WORK_TEMPERATURE", context.UseTemperature);
            parameters.Add("@UNITSIZE", usedAmount.Item1);
            parameters.Add("@UNITSIZE_ID", usedAmount.Item2);
            parameters.Add("@NOTICES", context.WorkComment);
            parameters.Add("@WORK_RECORD_ID", workRecordId);
            DbUtil.ExecuteUpdate(sql, parameters);
        }
        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to insert the worker details
        /// <summary>
        private void InsertWorker(InventoryManagement inventoryManagement, long workRecordId)
        {
            var sql = $@"INSERT INTO T_WORKER (   WORK_RECORD_ID
                                                , WORKER_USER_CD
                                                , WORKER_USER_NM
                                                , WORKER_GROUP_CD
                                                , WORKER_GROUP_NAME
                                                , CLASS_ID
                                              )
                         VALUES (   @WORK_RECORD_ID
                                  , @WORKER_USER_CD
                                  , @WORKER_USER_NM
                                  , @WORKER_GROUP_CD
                                  , @WORKER_GROUP_NAME
                                  , 0
                                )";
            if (inventoryManagement.WorkerIds != null)
            {
                foreach (var id in inventoryManagement.WorkerIds.Split(',').ToArray())
                {
                    DynamicParameters _params = new DynamicParameters();
                    _params.Add("@USER_CD", id);
                    List<M_USER> dt = DbUtil.Select<M_USER>(SQL.SELECT_GROUP_INFO, _params);
                    DynamicParameters parameters = new DynamicParameters();                   
                    parameters.Add("@WORK_RECORD_ID", workRecordId);
                    parameters.Add("@WORKER_USER_CD", id);
                    if (dt.Count > 0)
                    {
                        parameters.Add("@WORKER_USER_NM", dt[0].USER_NAME);
                        parameters.Add("@WORKER_GROUP_CD", dt[0].GROUP_CD);
                        parameters.Add("@WORKER_GROUP_NAME", dt[0].GROUP_NAME);
                    }
                    else
                    {
                        parameters.Add("@WORKER_USER_NM", null);
                        parameters.Add("@WORKER_GROUP_CD", null);
                        parameters.Add("@WORKER_GROUP_NAME", null);
                    }                   
                    DbUtil.ExecuteUpdate(sql, parameters);
                }
            }
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to update the accident worker details
        /// <summary>
        private void UpdateAccidentWorker(InventoryManagement inventoryManagement, long workRecordId)
        {
            if (string.IsNullOrWhiteSpace(inventoryManagement.WorkCommentTargetWorkerIds)) return;
            var sql = $@"UPDATE T_WORKER
                         SET CLASS_ID = 1
                         WHERE     WORK_RECORD_ID = @WORK_RECORD_ID
                               AND WORKER_USER_CD = @WORKER_USER_CD
                               AND WORKER_USER_NM = @WORKER_USER_NM
                               AND WORKER_GROUP_CD = @WORKER_GROUP_CD
                               AND WORKER_GROUP_NAME = @WORKER_GROUP_NAME ";
            foreach (var id in inventoryManagement.WorkCommentTargetWorkerIds.Split(',').ToArray())
            {
                DynamicParameters _params = new DynamicParameters();
                _params.Add("@USER_CD", id);
                List<M_USER> dt = DbUtil.Select<M_USER>(SQL.SELECT_GROUP_INFO, _params);
                DynamicParameters parameters = new DynamicParameters();
                parameters.Add("@WORK_RECORD_ID", workRecordId);
                parameters.Add("@WORKER_USER_CD", id);
                if (dt.Count > 0)
                {
                    parameters.Add("@WORKER_USER_NM", Convert.ToString(dt[0].USER_NAME));
                    parameters.Add("@WORKER_GROUP_CD", Convert.ToString(dt[0].GROUP_CD));
                    parameters.Add("@WORKER_GROUP_NAME", Convert.ToString(dt[0].GROUP_NAME));
                }
                else
                {
                    parameters.Add("@WORKER_USER_NM", null);
                    parameters.Add("@WORKER_GROUP_CD", null);
                    parameters.Add("@WORKER_GROUP_NAME", null);
                }
                DbUtil.ExecuteUpdate(sql, parameters);
            }
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is used to delete worker details
        /// <summary>
        private void DeleteWorker(InventoryManagement context, long workRecordId)
        {
            var sql = $@"DELETE T_WORKER
                         WHERE WORK_RECORD_ID = @WORK_RECORD_ID";
            DynamicParameters parameters = new DynamicParameters();
            parameters.Add("@WORK_RECORD_ID", workRecordId);
            DbUtil.ExecuteUpdate(sql, parameters);
        }


        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is to get the user details through ajax calls
        /// <summary>
        public KeyValuePair<string, string> GetUserValues(string id)
        {
            KeyValuePair<string, string> ArrayList = new KeyValuePair<string, string>();
            var users = new UserMasterInfo();

            var dic = users.GetDictionary();
            if (dic.ContainsKey(id))
            {
                ArrayList = dic.Single(x => x.Key == id);            
                return ArrayList;
            }

            return ArrayList;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-9-2019
        /// Description : This  Method is used to get the lookup text values using id
        /// <summary>
        public string GetMessageById(IEnumerable<SelectListItem> items, string key)
        {
            IEnumerable<SelectListItem> ITEM = (IEnumerable<SelectListItem>)items.Where(x => x.Value.Equals(key));
            return ITEM.First(x => x.Value.Equals(key)).Text;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-10-2019
        /// Description : This  Method is to validate the lend details through ajax call
        /// <summary>
        [HttpPost]
        public string ValidateLendDetails(string RegNo, string LastUsageLocId, string ddlIntended, string txtIntended, string txtMemo, string hdnManageWeight)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            inventoryManagement.RegNo = RegNo;
            inventoryManagement.LastUsageLocId = LastUsageLocId;
            inventoryManagement.ddlIntended = ddlIntended;
            inventoryManagement.txtIntended = txtIntended;
            inventoryManagement.txtMemo = txtMemo;
            inventoryManagement.hdnManageWeight = hdnManageWeight;
            string Executestatus = "";
            try
            {
                DataCheckBase _DataCheck = new DataCheckBase();
                string[] strVal = null;
                List<SelectListItem> listIntended = new List<SelectListItem> { };
                new IshaUsageMasterInfo().GetSelectList().ToList().ForEach(x => listIntended.Add(new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
                inventoryManagement.LookUpIntended = listIntended;
                string UsageRoom = _DataCheck.CheckUsageRoom(Convert.ToString(inventoryManagement.RegNo), inventoryManagement.LastUsageLocId, LocationClassId.Usage, strVal);
                if (UsageRoom != "")
                {
                    Executestatus = UsageRoom;
                    return Executestatus;
                }

                string Intented = _DataCheck.CheckIntended(GetMessageById(inventoryManagement.LookUpIntended, inventoryManagement.ddlIntended), inventoryManagement.txtIntended, nameof(inventoryManagement.ddlIntended));
                if (Intented != "")
                {
                    Executestatus = Intented;
                    return Executestatus;
                }

                string Memo = _DataCheck.CheckMemo(inventoryManagement.txtMemo, "txtMemo");
                if (Memo != "")
                {
                    Executestatus = Memo;
                    return Executestatus;
                }

                if (strWeightFlag[1] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                {
                    Executestatus = "WeightPopUp";
                }
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return Executestatus;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-10-2019
        /// Description : This  Method is to validate the repay details through ajax call
        /// <summary>
        public string ValidateRepay(string txtWorkingTimes, string txtMemo, string IsWorkCommentInput, string hdnManageWeight)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            inventoryManagement.txtMemo = txtMemo;
            inventoryManagement.txtWorkingTimes = txtWorkingTimes;
            inventoryManagement.IsWorkCommentInput = IsWorkCommentInput;
            inventoryManagement.hdnManageWeight = hdnManageWeight;
            string Executestatus = "";
            DataCheckBase _DataCheck = new DataCheckBase();
            try
            {
                string WorkTime = _DataCheck.CheckWorkingTimes(inventoryManagement.txtWorkingTimes, inventoryManagement.IsWorkCommentInput == true.ToString(), nameof(inventoryManagement.txtWorkingTimes));
                if (WorkTime != "")
                {
                    Executestatus = WorkTime;
                    return Executestatus;
                }

                string Memo = _DataCheck.CheckMemo(inventoryManagement.txtMemo, "txtMemo");
                if (Memo != "")
                {
                    Executestatus = Memo;
                    return Executestatus;
                }

                if (strWeightFlag[2] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                {
                    return "WeightPopUp";
                }
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return Executestatus;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-10-2019
        /// Description : This  Method is to validate the moving of chemical details through ajax call
        /// <summary>
        public string ValidateMove(string StoringLocation, string txtMemo, string OriginalLocation, string UsageLocation)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            inventoryManagement.txtMemo = txtMemo;
            inventoryManagement.StoringLocation = StoringLocation;
            inventoryManagement.OriginalLocation = OriginalLocation;
            inventoryManagement.UsageLocation = UsageLocation;
            string Executestatus = "";
            string[] strVal = null;
            DataCheckBase _DataCheck = new DataCheckBase();
            try
            {

                string CheckRoom = _DataCheck.CheckRoom(inventoryManagement.StoringLocation, LocationClassId.Storing, strVal);
                if (CheckRoom != "")
                {
                    Executestatus = CheckRoom;
                    return CheckRoom;
                }

                if (inventoryManagement.StoringLocation == inventoryManagement.OriginalLocation)
                {
                    Executestatus = string.Format(WarningMessages.NotChanged, "保管場所");
                    return Executestatus;
                }

                string CheckLocation = _DataCheck.CheckLocation(inventoryManagement.UsageLocation, "使用場所コード", strVal);
                if (CheckLocation != "")
                {
                    Executestatus = CheckLocation;
                    return CheckLocation;
                }

                string CheckMemo = _DataCheck.CheckMemo(inventoryManagement.txtMemo, "txtMemo");
                if (CheckMemo != "")
                {
                    Executestatus = CheckMemo;
                }
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return Executestatus;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-10-2019
        /// Description : This  Method is to validate the depletion details through ajax call
        /// <summary>
        public string ValidateDepletion(string LastUsageLocId, string ddlIntended, string txtIntended, string txtMemo, string hdnManageWeight)
        {
            InventoryManagement inventoryManagement = new InventoryManagement();
            inventoryManagement.txtMemo = txtMemo;
            inventoryManagement.LastUsageLocId = LastUsageLocId;
            inventoryManagement.ddlIntended = ddlIntended;
            inventoryManagement.txtIntended = txtIntended;
            inventoryManagement.hdnManageWeight = hdnManageWeight;
            string Executestatus = "";
            string[] strVal = null;
            DataCheckBase _DataCheck = new DataCheckBase();
            try
            {
                List<SelectListItem> listIntended = new List<SelectListItem> { };
                new IshaUsageMasterInfo().GetSelectList().ToList().ForEach(x => listIntended.Add(new SelectListItem { Value = x.Id.ToString(), Text = x.Name }));
                inventoryManagement.LookUpIntended = listIntended;
                string CheckRoom = _DataCheck.CheckRoom(inventoryManagement.LastUsageLocId, LocationClassId.Usage, strVal);
                if (CheckRoom != "")
                {
                    Executestatus = CheckRoom;
                    return Executestatus;
                }

                string Intented = _DataCheck.CheckIntended(GetMessageById(inventoryManagement.LookUpIntended, inventoryManagement.ddlIntended), inventoryManagement.txtIntended, nameof(inventoryManagement.ddlIntended));
                if (Intented != "")
                {
                    Executestatus = Intented;
                    return Executestatus;
                }

                string Memo = _DataCheck.CheckMemo(inventoryManagement.txtMemo, "txtMemo");
                if (Memo != "")
                {
                    Executestatus = Memo;
                    return Executestatus;
                }

                if (strWeightFlag[4] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                {
                    return "WeightPopUp"; ;
                }
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return Executestatus;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-10-2019
        /// Description : This  Method is to validate the delete chemical details through ajax call
        /// <summary>
        public string ValidateDelete(string hdnStatusId, string txtWorkingTimes, string IsWorkCommentInput, string txtMemo, string hdnManageWeight)
        {
            DataCheckBase _DataCheck = new DataCheckBase();
            string Executestatus = "";
            InventoryManagement inventoryManagement = new InventoryManagement();
            inventoryManagement.txtMemo = txtMemo;
            inventoryManagement.txtWorkingTimes = txtWorkingTimes;
            inventoryManagement.IsWorkCommentInput = IsWorkCommentInput;
            inventoryManagement.hdnStatusId = hdnStatusId;
            inventoryManagement.hdnManageWeight = hdnManageWeight;
            try
            {
                if (Convert.ToInt32(inventoryManagement.hdnStatusId) == (int)Const.cSTOCK_STATUS_ID.BORROW || Convert.ToInt32(inventoryManagement.hdnStatusId) == (int)Const.cSTOCK_STATUS_ID.EMPTY)
                {
                    string CheckWorkingTimes = _DataCheck.CheckWorkingTimes(inventoryManagement.txtWorkingTimes, inventoryManagement.IsWorkCommentInput == true.ToString(), nameof(inventoryManagement.txtWorkingTimes));
                    if (CheckWorkingTimes != "")
                    {
                        Executestatus = CheckWorkingTimes;
                        return Executestatus;
                    }
                }

                string CheckMemo = _DataCheck.CheckMemo(inventoryManagement.txtMemo, "txtMemo");
                if (CheckMemo != "")
                {
                    Executestatus = CheckMemo;
                    return Executestatus;
                }

                if (strWeightFlag[3] == "1" && (Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Recommendation ||
                                                Convert.ToInt32(inventoryManagement.hdnManageWeight) == (int)WeightManagement.Required))
                {
                    return "WeightPopUp";
                }
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return Executestatus;
        }

        /// <summary>
        /// Author: MSCGI
        /// Created date: 10-10-2019
        /// Description : This  Method is to get the usage and storage location details through ajax call
        /// <summary>
        public string BarcodeEnter(string Barcode, string LocationType)
        {
            InventoryManagement inventory = new InventoryManagement();
            string TableDisplayName = "";
            string ExecuteStatus = "";
            try
            {
                if (LocationType == "Location")
                {
                    TableDisplayName = "保管場所/使用場所";
                }
                else if (LocationType == "StoringLocation")
                {
                    TableDisplayName = "保管場所";
                }
                else if (LocationType == "UsageLocation")
                {
                    TableDisplayName = "使用場所";
                }
                else if (LocationType == "UsageLocation")
                {
                    TableDisplayName = "法規制";
                }

                var barcode = Barcode.ToUpper().Trim();
                if (LocationType == "Location" || LocationType == "StoringLocation" ||
                    LocationType == "UsageLocation")
                {
                    using (var locationInfo = new LocationMasterInfo(locationClassIds(LocationType)))
                    {
                        var location = locationInfo.GetData(barcode, DEL_FLAG.Undelete).ToList();
                        //if (FilterIds != null && FilterIds.Count() > 0) location = location.Where(x => FilterIds.Contains(x.LOC_ID.ToString())).ToList();

                        if (string.IsNullOrEmpty(barcode))
                        {
                            ExecuteStatus = "" + "|" + "";
                        }
                        else if ((location.Count != 1 || locationInfo.GetChildrenData(barcode, DEL_FLAG.Undelete).Count() > 0) && LocationType != "UsageLocation")
                        {
                            ExecuteStatus = string.Format(WarningMessages.NotFound, TableDisplayName) + "|" + ""; ;
                        }
                        else if (location.Count != 1 && LocationType == "UsageLocation")
                        {
                            ExecuteStatus = string.Format(WarningMessages.NotFound, TableDisplayName) + "|" + "";
                        }
                        else if (location.Count == 1)
                        {
                            IEnumerable<string> IDs = location.Select(x => x.LOC_ID.ToString()).ToList();
                            List<V_LOCATION> locationUsageDic = new List<V_LOCATION>();
                            string locbarcode = string.Join(",", IDs.ToArray());
                            if (LocationType == "UsageLocation")
                            {
                                locationUsageDic = GetLocationNameAjax(locbarcode, 2);
                                if (locationUsageDic != null && locationUsageDic.Count > 0)
                                {
                                    inventory.usageLocName = locationUsageDic[0].LOC_NAME;
                                    inventory.UsageLocation = locationUsageDic[0].LOC_BARCODE;
                                    ExecuteStatus = inventory.usageLocName + "|" + inventory.UsageLocation;
                                }
                            }
                            List<V_LOCATION> locationDic = new List<V_LOCATION>();
                            if (LocationType == "StoringLocation")
                            {
                                locationDic = GetLocationNameAjax(locbarcode, 1);
                                if (locationDic != null && locationDic.Count > 0)
                                {
                                    inventory.locName = locationDic[0].LOC_NAME;
                                    inventory.StoringLocation = locationDic[0].LOC_BARCODE;
                                    ExecuteStatus = inventory.locName + "|" + inventory.StoringLocation;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.ToString());
            }
            return ExecuteStatus;
        }


        public string GetValuesBasedOnIds(string Ids, string SelectionType)
        {
            string LOC_BARCODE = null;
            string LOC_NAME = null;
            string ExecuteStatus = null;
            string LOC_VALUE = null;
            try
            {
                if (Ids != "")
                {
                    using (var locationInfo = new LocationMasterInfo(locationClassIds(SelectionType)))
                    {
                        var location = locationInfo.GetData(Convert.ToInt32(Ids), DEL_FLAG.Undelete);
                        LOC_VALUE = location?.LOC_ID.ToString();
                        LOC_BARCODE = location?.LOC_BARCODE;
                        LOC_NAME = location?.LOC_NAME;
                        ExecuteStatus = LOC_NAME + "|" + LOC_BARCODE;
                    }
                }
            }
            catch (Exception ex)
            {                
                logutil.OutErrLog(PROGRAM_ID, ex.StackTrace.ToString());
            }
            return ExecuteStatus;
        }

        /// <summary>
        /// 重量、容量、風袋のチェック処理
        /// </summary>
        /// <returns></returns>
        public string CheckWeight(string txtAllWeight, string lblGWeight, string hdnExecuteType, string txtUnitSize)
        {
            string ExecuteStatus = null;
            DataCheckBase _DataCheck = new DataCheckBase();
            string UnitSize;

            //入力チェック
            UnitSize = txtAllWeight;
            ExecuteStatus = _DataCheck.CheckAllWeight(ref UnitSize);
            if (ExecuteStatus != null)
            {
                return ExecuteStatus;
            }

            if (lblGWeight != "")
            {
                ExecuteStatus = _DataCheck.CheckAllBottoleWeight(txtAllWeight, lblGWeight.Replace("g", ""));
                if (ExecuteStatus != null)
                {
                    return ExecuteStatus;
                }
            }

            if (hdnExecuteType == "1")
            {
                ExecuteStatus = _DataCheck.CheckUnitSize(ref UnitSize, true);
                UnitSize = txtUnitSize;
                if (ExecuteStatus != null)
                {
                    return ExecuteStatus;
                }

                ExecuteStatus = _DataCheck.CheckBottoleWeight(txtAllWeight, txtUnitSize);
                if (ExecuteStatus != null)
                {
                    return ExecuteStatus;
                }
            }
            return ExecuteStatus;
        }
    }

    /// <summary>
    /// Author: MSCGI
    /// Created date: 10-10-2019
    /// Description : This  class is for SQL queries
    /// <summary>
    struct SQL
    {
        /// <summary>
        /// 試薬バーコードよりT_STOCKの検索
        /// </summary>
        public static string SELECT_T_STOCK
        {
            get
            {
                string sql = "SELECT * FROM T_STOCK S ";
                sql += "left outer join (select CHEM_CD, CHEM_NAME,CAS_NO from M_CHEM) C on S.CAT_CD = C.CHEM_CD "; //2018/5/11 Add CAS追加
                sql += "left outer join (select STOCK_ID,USAGE_LOC_ID LAST_LOC_ID,INTENDED_USE_ID,INTENDED from T_ACTION_HISTORY inner join M_LOCATION on T_ACTION_HISTORY.USAGE_LOC_ID = M_LOCATION.LOC_ID and M_LOCATION.DEL_FLAG = 0 where ACTION_ID in (select max(ACTION_ID) from T_ACTION_HISTORY where ACTION_TYPE_ID in (2,6) group by STOCK_ID)) A on S.STOCK_ID = A.STOCK_ID ";
                sql += "left outer join (select STOCK_ID,CUR_GROSS_WEIGHT_G LAST_WEIGHT from T_ACTION_HISTORY where ACTION_ID in (select max(ACTION_ID) from T_ACTION_HISTORY where ACTION_TYPE_ID <> 5 group by STOCK_ID)) A2 on S.STOCK_ID = A2.STOCK_ID ";
                sql += "left outer join M_UNITSIZE U on S.UNITSIZE_ID = U.UNITSIZE_ID ";
                sql += "left outer join M_MAKER M on  S.MAKER_ID = M.MAKER_ID ";
                sql += "left outer join M_STATUS ST on S.STATUS_ID = ST.STATUS_ID and ST.CLASS_ID = 2 ";
                sql += "left outer join M_LOCATION L on S.LOC_ID = L.LOC_ID ";
                sql += "WHERE S.BARCODE = @BARCODE ";
                return sql;
            }
        }

        /// <summary>
        /// バーコードよりT_LEDGERの検索
        /// </summary>
        public static string SELECT_T_LEDGER
        {
            get
            {
                string sql = "SELECT ML.REG_NO from T_MGMT_LEDGER ML ";
                sql += "left outer join T_STOCK S on ML.REG_NO = S.REG_NO ";
                sql += "WHERE S.BARCODE = @BARCODE ;";
                return sql;
            }
        }

        /// <summary>
        /// バーコードよりT_LEDGERとM_GROUPの検索
        /// </summary>
        public static string SELECT_LEDGER_REG_GROUP
        {
            get
            {
                string sql = "SELECT ML.GROUP_CD FROM T_MGMT_LEDGER ML ";
                sql += "left outer join T_STOCK S on ML.REG_NO = S.REG_NO ";
                sql += "WHERE ML.GROUP_CD in ( ";
                sql += "    (select G1.GROUP_CD from M_USER U1 left outer join M_GROUP G1 on G1.GROUP_CD = U1.GROUP_CD where U1.USER_CD = @USER_CD), ";
                sql += "    (select G2.GROUP_CD from M_GROUP G2 where G2.GROUP_CD = (select G5.P_GROUP_CD from M_GROUP G5 left outer join M_USER U5 on G5.GROUP_CD = U5.GROUP_CD where U5.USER_CD = @USER_CD)), ";
                sql += "    (select G3.GROUP_CD from M_USER U3 left outer join M_GROUP G3 on U3.PREVIOUS_GROUP_CD = G3.GROUP_CD where U3.USER_CD = @USER_CD and G3.DEL_FLAG = 1), ";
                sql += "    (select G4.GROUP_CD from M_GROUP G4 where G4.GROUP_CD = (select G6.P_GROUP_CD from M_USER U6 left outer join M_GROUP G6 on U6.PREVIOUS_GROUP_CD = G6.GROUP_CD where U6.USER_CD = @USER_CD and G6.DEL_FLAG = 1 ))";
                sql += " ) ";
                sql += "and S.BARCODE = @BARCODE;";
                return sql;
            }
        }

        /// <summary>
        /// バーコードより廃止済みT_LEDGERの検索
        /// </summary>
        public static string SELECT_ABOLISHED_LEDGER
        {
            get
            {
                string sql = "SELECT ML.REG_NO from T_MGMT_LEDGER ML ";
                sql += "left outer join T_STOCK S on ML.REG_NO = S.REG_NO ";
                sql += "WHERE ML.GROUP_CD in ( ";
                sql += "    (select G1.GROUP_CD from M_USER U1 left outer join M_GROUP G1 on G1.GROUP_CD = U1.GROUP_CD where U1.USER_CD = @USER_CD), ";
                sql += "    (select G2.GROUP_CD from M_GROUP G2 where G2.GROUP_CD = (select G5.P_GROUP_CD from M_GROUP G5 left outer join M_USER U5 on G5.GROUP_CD = U5.GROUP_CD where U5.USER_CD = @" +
                    "USER_CD)), ";
                sql += "    (select G3.GROUP_CD from M_USER U3 left outer join M_GROUP G3 on U3.PREVIOUS_GROUP_CD = G3.GROUP_CD where U3.USER_CD = @USER_CD and G3.DEL_FLAG = 1), ";
                sql += "    (select G4.GROUP_CD from M_GROUP G4 where G4.GROUP_CD = (select G6.P_GROUP_CD from M_USER U6 left outer join M_GROUP G6 on U6.PREVIOUS_GROUP_CD = G6.GROUP_CD where U6.USER_CD = @USER_CD and G6.DEL_FLAG = 1 ))";
                sql += " ) ";
                sql += "and S.BARCODE = @BARCODE ";
                sql += "and ML.APPLI_CLASS = 3; ";
                return sql;
            }
        }

        /// <summary>
        /// ACTION_HISTORY登録
        /// </summary>
        public static string ACTION_HISTORY
        {
            get
            {
                string sql = "INSERT INTO T_ACTION_HISTORY(";
                sql += "ACTION_TYPE_ID,BARCODE,USER_CD,LOC_ID,LOC_NAME,USAGE_LOC_ID,USAGE_LOC_NAME,INTENDED,WORKING_TIMES,MEMO,CUR_GROSS_WEIGHT_G,STOCK_ID,INTENDED_USE_ID,INTENDED_NM,ACTION_USER_NM,ACTION_GROUP_NAME,ACTION_GROUP_CD) ";
                sql += "VALUES (@ACTION_TYPE_ID,@BARCODE,@USER_CD,@LOC_ID,@LOC_NAME,@USAGE_LOC_ID,@USAGE_LOC_NAME,@INTENDED,@WORKING_TIMES,@MEMO,@CUR_GROSS_WEIGHT_G,@STOCK_ID,@INTENDED_USE_ID,@INTENDED_NM,@ACTION_USER_NM,@ACTION_GROUP_NAME,@ACTION_GROUP_CD)";
                return sql;
            }
        }

        /// <summary>
        /// GROUP_CD, GROUP_NAME検索
        /// </summary>
        public static string SELECT_GROUP_INFO
        {
            get
            {
                string sql = "SELECT mu.USER_NAME, mu.GROUP_CD, mg.GROUP_NAME ";
                sql += "from M_USER mu inner join V_GROUP mg ";
                sql += "on mg.GROUP_CD = mu.GROUP_CD ";
                sql += "where mu.USER_CD = @USER_CD";
                return sql;
            }
        }

    }
}