﻿/**
 * NikonIDを入力し、Enterキー押下時にNikonIDの情報を取得します。
 * @param {any} keyCode
 * @param {any} nikonIdTextBoxId
 * @param {any} addSelectID
 */
function inputNikonId(keyCode, nikonIdTextBoxId, addSelectID) {
    if (keyCode != 13) return;

    var nikonIdTextBox = $("#" + nikonIdTextBoxId);
    var inputNikonId = nikonIdTextBox.val();
    nikonIdTextBox.val("");

    if (!inputNikonId.length) return;

    addUserToSelect(inputNikonId, addSelectID);
}

/**
 * NikonIDからユーザーをSelectに復元します。
 * @param {any} nikonIds
 * @param {any} selectId
 */
function restoreUsersFromIdToSelect(nikonIds, selectId) {

    if (!nikonIds.length) return;

    var select = $("#" + selectId);
    select.children().remove();

    $.each(nikonIds.split(","), function (index, value) {
        addUserToSelect(value, selectId);
    });
}

/**
 * NikonIDからユーザーをSelectに追加します。
 * @param {any} nikonId
 * @param {any} addSelectId
 */
function addUserToSelect(nikonId, addSelectId) {
    var addSelect = $("#" + addSelectId);
    var addedNikonIds = addSelect.children();
    //if (addedNikonIds != null && addedNikonIds.toEnumerable().Any("$.val()==\"" + nikonId + "\"")) {
    //    alert("選択済みです。");
    //    return;
    //}

    $.getJSON("../ZC040/User/" + nikonId, null, function (data, status) {
        var user = data;
        if (!user.Key.length) {
            return;
        }
        var option = $("<option>").val(user.Key).text(user.Value);
        addSelect.append(option);
    });
}

/**
 * NikonIDから選択したユーザー名を取得します。
 * @param {any} nikonIds
 */
function getNikonNames(nikonIds) {

    if (!nikonIds.length) return;

    var names = [];

    $.ajaxSetup({ async: false });

    $.each(nikonIds.split(","), function (index, value) {
        $.getJSON("../ZC040/User/" + value, null, function (data, status) {
            var user = data;
            if (!user.Key.length) {
                return;
            }
            names.push(user.Value);
        });
    });

    $.ajaxSetup({ async: true });

    return names.join(",");
}

/**
 * 指定したオブジェクトへNikonIDを設定します。
 * @param {any} ids
 * @param {any} targetObjectId
 */
function setNikonIds(ids, targetObjectId) {
    var targetObject = $("#" + targetObjectId);
    targetObject.val(ids);
}

/**
 * 指定したSelectリストからNikonIDを取得します。
 * @param {any} ownerSelectId
 */
function getNikonIds(ownerSelectId) {
    return $("#" + ownerSelectId).children().toEnumerable().Select("$.val()").ToArray().join(',');
}