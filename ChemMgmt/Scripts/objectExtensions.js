﻿/**
 * Selectリストをクリアします。
 * @param {any} selectId
 */
function selectionDelete(selectId) {
    $("#" + selectId).children(":selected").remove();
}

/**
 * Selectリストから削除します。但し保護された値は削除できません。
 * @param {any} selectId
 * @param {any} protectValue
 * @param {any} protectMessage
 */
function selectionDeleteWithProtect(selectId, protectValue, protectMessage) {
    var removeNikonIds = $("#" + selectId).children(":selected");

    if (removeNikonIds != null && removeNikonIds.toEnumerable().Any("$.val()==\"" + protectValue + "\"")) {
        alert(protectMessage);
        return;
    }
    removeNikonIds.remove();
}

/**
 * Selectリストから選択されたものを別のSelectリストへ移動します。
 * @param {any} fromSelectId
 * @param {any} toSelectId
 */
function selectionMove(fromSelectId, toSelectId) {
    var selections = $("#" + fromSelectId).children(":selected");

    var toSelect = $("#" + toSelectId);

    $.each(selections, function (index, value) {
        selectAppend(toSelect, value.value, value.text);
    });

    selections.remove();
}

/**
 * Selectリストに値を追加します。
 * @param {any} targetSelect
 * @param {any} value
 * @param {any} text
 */
function selectAppend(targetSelect, value, text) {
    var option = $("<option>").val(value).text(text);
    targetSelect.append(option);
}

/**
 * ファイル選択時に自動アップロードします。
 * @param {any} fileKey
 */
function fileUpload(event, formId, guidOutputHiddenFieldId, fileNameOutputTextBoxId) {
    var fileObject = event.target;
    var form = $("#" + formId).get()[0];
    var formData = new FormData(form);
    var fileName = fileObject.files[0].name;

    formData.append("fileName", fileName);
    formData.append("HiddenFieldId", guidOutputHiddenFieldId);

    $.ajaxSetup({ async: false });

    var response = $.getJSON({
        url: "../api/FileUpload",
        method: "post",
        dataType: "json",
        data: formData,
        processData: false,
        contentType: false
    });

    $.ajaxSetup({ async: true });

    if (response.status == 200) {
        var guidHiddenField = $("#" + guidOutputHiddenFieldId);
        var fileNameTextBox = $("#" + fileNameOutputTextBoxId);
        guidHiddenField.val(response.responseJSON);
        fileNameTextBox.val(fileName);
        return;
    }

    showFileSizeOverMessage();
}