﻿//------------------------------------------------------------------------------------------------------------------
//
//
// JavaScriptの共通ファイルです。
//
//
//------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------
//関数名：
//説　明：ブラウザの戻るを無効にする。
//　　　：戻った途端に進むことで実質的に禁止します。
//引　数：
//戻り値：
//備　考：
//作　成：2011/12/01 Y.NOZAKI
//更　新：
//------------------------------------------------------------------------------------------------------------------
history.forward();

//------------------------------------------------------------------------------------------------------------------
//関数名：ClearLabelValue
//説　明：ラベルコントロールのValue値をクリアします。（Gridも可）
//引　数：lbl - ラベルコントロール
//戻り値：なし
//備　考：
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function ClearLabelValue(lbl) {
    var obj = document.getElementById(lbl);
    if (obj != null) {
        obj.innerText = '';
    }
}

//------------------------------------------------------------------------------------------------------------------
//関数名：CheckBarCode
//説　明：バーコードが入力されているテキストボックスの背景色を変更します。
//　　　：また更新フラグテキストボックスが存在する場合1を立てます。
//　　　：　バーコードが空 →　背景色　PaleTurquoise（薄青）
//引　数：UpdFlg - 更新フラグhidden Control Object
//　　　：BarCode - バーコードTextBox Control Object
//戻り値：なし
//備　考：
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function CheckBarCode(UpdFlg, BarCode,HdnBarCode) {
    if (UpdFlg != null) {
        var hdnUpdFlg = document.getElementById(UpdFlg);
        hdnUpdFlg.value = '1';
    }
    // バーコードが空の場合、チェックしない。
    var txtBarCode = document.getElementById(BarCode);
    txtBarCode.style.backgroundColor = "PaleTurquoise";

    //変更があった行をHidden項目に退避
    var hdnBarcode = document.getElementById(HdnBarCode);
    hdnBarcode.value = hdnBarcode.value + BarCode + ",";
}

//------------------------------------------------------------------------------------------------------------------
//関数名：CheckMemo
//説　明：備考欄の内容が変更された時の処理。テキストボックスの背景色PaleTurquoise（薄青）に変更します。
//　　　：また更新フラグテキストボックスが存在する場合1を立てます。
//引　数：UpdFlg - 更新フラグhidden Control Object
//　　　：Memo - 備考欄
//戻り値：なし
//備　考：
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function CheckMemo(UpdFlg, Memo, HdnMemo) {
    if (UpdFlg != null) {
        var hdnUpdFlg = document.getElementById(UpdFlg);
        hdnUpdFlg.value = '1';
    }
    // バーコードが空の場合、チェックしない。
    var txtMemo = document.getElementById(Memo);
    txtMemo.style.backgroundColor = "PaleTurquoise";

    //変更があった行をHidden項目に退避
    var hdnMemo = document.getElementById(HdnMemo);
    hdnMemo.value = hdnMemo.value + Memo + ",";
}

//------------------------------------------------------------------------------------------------------------------
//関数名：CheckUpdateMsg
//説　明：更新フラグ（隠しテキストボックスの値）が空でなければ、更新確認メッセージを出力し、結果を返します。
//引　数：UpdFlg - 更新フラグhidden Control Object
//　　　：msg - 更新確認メッセージ
//戻り値：true：OK押下 / false:キャンセル押下
//備　考：
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function CheckUpdateMsg(UpdFlg, msg) {
    var ret = true;
    var hdnUpdFlg = document.getElementById(UpdFlg);
    if (hdnUpdFlg.value != '') {
        ret = confirm(msg);
    }
    return ret;
}

//------------------------------------------------------------------------------------------------------------------
//関数名：onKeyDown_NextRowFocus
//説　明：Enterキー押下イベントであれば、指定したコントロールへカーソルを移動します。
//引　数：e - イベントオブジェクト
//　　　：nextControl - フォーカスを設定したいControlオブジェクト
//戻り値：なし
//備　考：
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function onKeyDown_NextRowFocus(e, nextControl) {
    if (e.keyCode == 13) {
        var next = document.getElementById(nextControl);
        next.focus();
        return false;
    }
}

//------------------------------------------------------------------------------------------------------------------
//関数名：onKeyDown_ConvTab
//説　明：Enterキー押下イベントであれば、Tabキー押下イベントに変換します。
//引　数：e - イベントオブジェクト
//戻り値：false - Enter処理をCancel
//備　考：
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function onKeyDown_ConvTab(e) {
    if (e.keyCode == 13) {
        e.keyCode = 9;
        return false;
    }
}

//------------------------------------------------------------------------------------------------------------------
//関数名：clientSideEdit、insertBefore、insertAfter
//説　明：RadTreeViewの操作スクリプト
//引　数：
//戻り値：
//備　考：
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function clientSideEdit(sender, args, sourceNode) {
    var destinationNode = args.get_destNode();
    if (destinationNode) {
        var firstTreeView = $find('tree');

        firstTreeView.trackChanges();
        sourceNode.get_parent().get_nodes().remove(sourceNode);

        if (args.get_dropPosition() == "over") destinationNode.get_nodes().add(sourceNode);
        if (args.get_dropPosition() == "above") insertBefore(destinationNode, sourceNode);
        if (args.get_dropPosition() == "below") insertAfter(destinationNode, sourceNode);
        destinationNode.set_expanded(true);
        firstTreeView.commitChanges();
    }
}

function insertBefore(destinationNode, sourceNode) {
    var destinationParent = destinationNode.get_parent();
    var index = destinationParent.get_nodes().indexOf(destinationNode);
    destinationParent.get_nodes().insert(index, sourceNode);
}

function insertAfter(destinationNode, sourceNode) {
    var destinationParent = destinationNode.get_parent();
    var index = destinationParent.get_nodes().indexOf(destinationNode);
    destinationParent.get_nodes().insert(index + 1, sourceNode);
}

//------------------------------------------------------------------------------------------------------------------
//関数名：ClearHiddenValue
//説　明：HiddenコントロールのValueを空にします
//引　数：ctrlHdn - Hiddenコントロール
//戻り値：なし
//備　考：
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function ClearHiddenValue(ctrlHdn) {
    var txt = document.getElementById(ctrlHdn);
    txt.value = '';
}

//------------------------------------------------------------------------------------------------------------------
//関数名：ShowModal
//説　明：指定したURLの画面をModalで表示します。
//引　数：url - Modal表示する画面URL
//　　　：height - 高さ(px)
//　　　：width - 幅(px)
//戻り値：なし
//備　考：RadTextBoxではなく、ノーマルのTextBoxの場合、ReadOnlyであると空にならないので注意
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function ShowModal(url, height, width) {

    var Style = 'dialogHeight:' + height + 'px;dialogWidth:' + width + 'px';
    showModalDialog(url, '', Style);
}

//------------------------------------------------------------------------------------------------------------------
//関数名：openWindowC
//説　明：指定したURLの画面を画面中央にwindow.openします
//引　数：url - 表示する画面URL
//　　　：height - 高さ(px)
//　　　：width - 幅(px)
//戻り値：なし
//備　考：
//作　成：
//更　新：
//------------------------------------------------------------------------------------------------------------------
function openWindowC(url, height, width) {
    x = (screen.width - width) / 2;
    y = (screen.height - height) / 2;
    subWin = window.open(url, 'openWindowC', "screenX=" + x + ",screenY=" + y + ",left=" + x + ",top=" + y + ",width=" + width + ",height=" + height + ",menubar=no,toolbar=no,scrollbars=yes");
}

// 選択確認(単一)を行う。
function SingleSelectionCheck(notSelectedWarningMessage, selectedMultipleWarning, tableID) {
    if ($('#' + tableID + ' tr td input:checkbox:checked').length == 0) {
        window.alert(notSelectedWarningMessage);
        return false;
    };
    if ($('#' + tableID + ' tr td input:checkbox:checked').length != 1) {
        window.alert(selectedMultipleWarning);
        return false;
    };
    return true;
}

// 選択確認(複数)を行う。
function MultipleSelectionCheck(warningMessage, tableID) {
    if ($('#' + tableID + ' tr td input:checkbox:checked').length == 0) {
        window.alert(warningMessage);
        return false;
    };
    return true;
}

// 確認ダイアログ表示を行う。
function ConfirmDialog(confirmMessage, buttonClientID) {
    if (window.confirm(confirmMessage)) {
        ClientButtonClick(buttonClientID);
    }
}

// 画面上のボタンをクリックする。
function ClientButtonClick(buttonClientID) {
    var button = document.getElementById(buttonClientID);
    button.click();
}
function moveCursorToEnd(id) {
    var el = document.getElementById(id)
    el.focus()
    if (typeof el.selectionStart == "number") {
        el.selectionStart = el.selectionEnd = el.value.length;
    } else if (typeof el.createTextRange != "undefined") {
        var range = el.createTextRange();
        range.collapse(false);
        range.select();
    }
}
// ファイル選択後、選択したファイル名の末尾を表示する。
function showFileNameLast(obj, valObj) {
 //   var range = document.getSelection();
   // alert(range.collapseToEnd);
    //  range.addRange(range);
 //   range.collapseToEnd();
 //   range.moveStart('character', valObj.value.length);
  //  range.select();
  //  var range = new Range();

    //range.setStart(p,start.value);
//    range.collapseToEnd();
 //   valObj.focus(); //sets focus to element
  
    var val = valObj.value; //store the value of the element
  //  valObj.value = ''; //clear the value of the element
    //  valObj.value = val; //set that value back. 
    $('#file').val(val);
    alert(val);
    // apply the selection, explained later
 //   document.getSelection().removeAllRanges();
   // document.getSelection().addRange(range);




    //alert(valObj.value.length);
    // valObj.value.length;

    //var range = obj.createRange();
    //range.moveStart('character', valObj.value.length);
    //range.select();


    //var storedSelections = [];
    //if (window.getSelection) {
    //    var currSelection = window.getSelection();
    //  //  var range = currSelection.getRangeAt(0);
    //  //  var range1 = currSelection.getRangeAt(1);
    //    //for (var i = 0; i < currSelection.rangeCount; i++) {
    //    //    storedSelections.push(currSelection.getRangeAt(rangeCount));
    //    //}

    //  //  range.('character', currSelection.rangeCount);
    //    // alert(range1);
    //    currSelection.add
    //    alert(currSelection.rangeCount);
      //  currSelection.select();
    //    currSelection.removeAllRanges();


}

// バックスペースを無効化する。
$(function () {
    $(document).keydown(function (event) {
        var keyCode = event.keyCode;
        var ctrlClick = event.ctrlKey;
        var altClick = event.altKey;
        var obj = event.target;
        if (keyCode == 8) {
            if ((obj.tagName.toUpperCase() == "INPUT" && (obj.type.toUpperCase() == "TEXT" || obj.type.toUpperCase() == "PASSWORD")) ||
                obj.tagName.toUpperCase() == "TEXTAREA") {
                if (!obj.readOnly && !obj.disabled) {
                    return true;
                }
            }
            return false;
        }
        if (altClick && (keyCode == 37 || keyCode == 39)) {
            return false;
        }
    });
});