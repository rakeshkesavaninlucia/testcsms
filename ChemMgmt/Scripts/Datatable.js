$(document).ready(function () {
    var oTable = $('#MNGLedgerWebGrid').DataTable({
        "sScrollX": "0",
        "sScrollY": "400px",
        "sScrollXInner": "140%",
        "bPaginate": false,
        "bLengthChange": false,
        "bFilter": false,
        "bSort": true,
        "bInfo": false,
        "bAutoWidth": false,
        "oLanguage": { "sZeroRecords": "", "sEmptyTable": "" }
    });
});