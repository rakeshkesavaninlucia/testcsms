﻿

//function DoPTouchLabelPrint(templateFileName, datas) {
//    var label = new ActiveXObject("bpac.Document");
//    if (label.Open("C:\\Program Files\\ChemMgmt\\" + templateFileName) != false) {

//        label.StartPrint("", 0);

//        datas.forEach(function (data) {
//            label.GetObject("Barcode").Text = data.Number;
//            label.GetObject("Number").Text = data.Number;
//            label.GetObject("Name").Text = data.Name;
//            label.PrintOut(1, 0);
//        });

//        label.Close();
//        label.EndPrint();
//    }
//    else {
//        alert("P-Touchラベルプリンターが正しくインストールされていません。");
//    }
//}

function DoPTouchLabelPrint(datas) {

    var templateFileName = datas.substring(0, datas.indexOf(","));
    var data = datas.substring(datas.indexOf(",") + 1, datas.count);
    //alert(templateFileName);
    //alert(data);
    var label = new ActiveXObject("bpac.Document");
    if (label.Open("D:\\CSMS_SOURCECODES\\CSMS - 24October\\ChemMgmt\\Template\\" + templateFileName) != false) {
        var divisor = "";
        if (templateFileName == 'User.lbx') {
            divisor = 2;
        } else if (templateFileName == 'Location.lbx') {
            divisor = 2;
        } else if (templateFileName == 'Bottle.lbx') {
            divisor = 2;
        }

        label.StartPrint("", 0);
        var count = 0;
        // datas = '[{ "Name": "test1", "Number": "dev1" },{ "Name": "test12", "Number": "dev12" }]';
        JSON.parse(data, function (key, value) {

            if (typeof (value) != "object") {
                count++;
                if (key == "Number") {
                    label.GetObject("Number").Text = value;
                    //   alert("Number: " + label.GetObject("Number").Text);
                } else if (key == "Barcode") {
                    label.GetObject("Barcode").Text = value;
                    //  alert("Barcode: " + value);
                }
                else {
                    label.GetObject("Name").Text = value;
                    //    alert("Name: " + label.GetObject("Name").Text);
                }
            }
            if (count != 0 && count % divisor == 0) {
                alert("printing");
                label.PrintOut(1, 0);
                count = 0;
            }
        });

        label.Close();
        label.EndPrint();
        //alert("P-Touch End print");
    }
    else {
        alert("P-Touchラベルプリンターが正しくインストールされていません。");
    }
}

