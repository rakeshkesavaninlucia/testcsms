﻿function block() {
    $.blockUI({ message: '<p>読み込み中...</p>' });
}
function Unblock() {
    $.unblockUI();
}
function pageLoad() {
    var mng = Sys.WebForms.PageRequestManager.getInstance();
    // 非同期ポストバックの完了時に呼び出されるイベントを定義
    mng.add_endRequest(
    function (sender, args) {
        $(function () {$.unblockUI();});
    }
    );
}
function windowResize() {
    moveTo(0, 0);
    resizeTo(screen.width, screen.height - 32);
}