﻿function showRequireMessage(itemName) {
    alert(itemName + "は必須項目です。");
}

function showMaxStringLengthMessage(itemName, maxStringLength) {
    alert(itemName + "は" + maxStringLength.toString() + "文字以下で入力してください。");
}

function showFileSizeOverMessage() {
    alert("ファイルサイズが大きいため、アップロードできません。");
}