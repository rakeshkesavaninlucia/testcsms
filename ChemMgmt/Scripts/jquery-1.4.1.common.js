﻿// 表ヘッダーのコンボボックスで全選択/全解除を行います。
$(function () {
    $('table tr th input:checkbox').click(function () {
        $(this).parent().parent().parent().find('tr td input:checkbox').attr('checked', $(this).is(':checked'));
    });
    $('table tr td input:checkbox').click(function () {
        var checkedAll = $(this).parent().parent().parent().find('tr td input:checkbox:not(:checked)').length == 0 ? true : false;
        $(this).parent().parent().parent().find('tr th input:checkbox').attr('checked', checkedAll);
    });
});