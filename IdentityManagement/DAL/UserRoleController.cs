﻿using IdentityManagement.Data;
using IdentityManagement.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityManagement.DAL
{
    public static class UserRoleController
    {
        public static int NewUserRole(string userID, string roleName)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            parameters.Add(new ParameterInfo() { ParameterName = "UserID", ParameterValue = userID });
            parameters.Add(new ParameterInfo() { ParameterName = "RoleName", ParameterValue = roleName });
            int success = SqlHelper.ExecuteQuery("NewUserRole", parameters);
            return success;
        }

        public static int DeleteUserRole(string userID, string roleName)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            parameters.Add(new ParameterInfo() { ParameterName = "UserID", ParameterValue = userID });
            parameters.Add(new ParameterInfo() { ParameterName = "RoleName", ParameterValue = roleName });
            int success = SqlHelper.ExecuteQuery("DeleteUserRole", parameters);
            return success;
        }


        public static IList<string> GetUsrRoles(string userID)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            //parameters.Add(new ParameterInfo() { ParameterName = "USER_CD", ParameterValue = userID });
            //IList<string> roles = SqlHelper.GetRecords<string>("GetUserRoles", parameters);
            parameters.Add(new ParameterInfo() { ParameterName = "User_CD", ParameterValue = userID });
            parameters.Add(new ParameterInfo() { ParameterName = "Action_Type", ParameterValue = 1 });
            IList<string> roles = SqlHelper.GetRecords<string>("GetUserDetails", parameters);
           return roles;
        }
        public static UserInformation GetUserByUsername(string userName)
        {
            List<ParameterInfo> parameters = new List<ParameterInfo>();
            parameters.Add(new ParameterInfo() { ParameterName = "Username", ParameterValue = userName });
            UserInformation oUser = SqlHelper.GetRecord<UserInformation>("GetUserByUsername", parameters);
            return oUser;
        }
    }
}
