﻿using Microsoft.AspNet.Identity;

namespace IdentityManagement.Entities
{
    public class UserInformation : IUser<string>
    {
        public string Id { get; set; }
        public string User_CD { get; set; }
        public string UserName { get; set; }
        public string UserTitle { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string ADMIN_FLAG { get; set; }
        public string FLOW_CD { get; set; }
        //public string REFERENCE_AUTHORITY { get; set; } = "1";

        public string USER_NAME_KANA { get; set; }
        public string GROUP_CD { get; set; }
        public string GROUP_NAME { get; set; }
        public string APPROVER1 { get; set; }
        public string APPROVER2 { get; set; }
        public string APPROVER1_NAME { get; set; }
        public string APPROVER2_NAME { get; set; }
        public string TEL { get; set; }
        public bool DEL_FLAG { get; set; }
        public bool BATCH_FLAG { get; set; }
        public string REFERENCE_AUTHORITY { get; set; }
    }
}
